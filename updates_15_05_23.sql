/*---15 06 23---*/
ALTER TABLE `personal` ADD `apellido_p` VARCHAR(300) NOT NULL AFTER `nombre`, ADD `apellido_m` VARCHAR(300) NOT NULL AFTER `apellido_p`;
ALTER TABLE `personal` ADD `fecha_ingreso` DATE NOT NULL AFTER `correo`, ADD `puesto` VARCHAR(300) NOT NULL AFTER `fecha_ingreso`;
ALTER TABLE `personal` ADD `direccion` VARCHAR(300) NOT NULL AFTER `celular`;
ALTER TABLE `personal` CHANGE `celular` `telefono` VARCHAR(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `personal` DROP `cedula_prof`, DROP `foto`, DROP `firma`, DROP `cedula`;

CREATE TABLE `gran_turismo`.`personal_documentos` ( `documentoId` INT(11) NOT NULL AUTO_INCREMENT , `personalId` INT(11) NOT NULL , `file` TEXT NULL , `reg_file` TIMESTAMP NOT NULL , `tipo` TINYINT(1) NOT NULL , `estatus` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`documentoId`)) ENGINE = InnoDB;
ALTER TABLE `personal_documentos` ADD FOREIGN KEY (`personalId`) REFERENCES `personal`(`personalId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `tipo`) VALUES (NULL, '1', 'Choferes', 'Choferes', 'fa fa-truck', '1')
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '4')

CREATE TABLE `gran_turismo`.`choferes` ( `choferid` INT(11) NOT NULL AUTO_INCREMENT , `nombre` VARCHAR(300) NOT NULL , `apellido_p` VARCHAR(300) NOT NULL , `apellido_m` VARCHAR(300) NOT NULL , `telefono` VARCHAR(15) NOT NULL , `direccion` VARCHAR(300) NOT NULL , `fecha_ingreso` DATE NOT NULL , `estatus` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '1 visible 0 eliminado' , `reg` TIMESTAMP NOT NULL , PRIMARY KEY (`choferid`)) ENGINE = InnoDB;

 /* ************************21-06-23********************************* */
 INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `tipo`) VALUES (NULL, '1', 'Clientes', 'Clientes', 'fa fa-users', '1');
 INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '5');

 /* *************************************************************** */
 ALTER TABLE `prospectos` ADD `razon` TEXT NOT NULL AFTER `fecha_reg`, ADD `rfc` VARCHAR(35) NOT NULL AFTER `razon`, ADD `cp` VARCHAR(5) NOT NULL AFTER `rfc`, ADD `id_regimen` INT NOT NULL AFTER `cp`, ADD `id_cfdi` INT NOT NULL AFTER `id_regimen`, ADD `direccion` TEXT NOT NULL AFTER `id_cfdi`;

 DROP TABLE IF EXISTS `pagos_cliente`;
CREATE TABLE IF NOT EXISTS `pagos_cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `porc_anticipo` int(11) NOT NULL,
  `monto_anticipo` int(11) NOT NULL,
  `porc_desc` int(11) NOT NULL,
  `identifica` text COLLATE utf8_spanish_ci NOT NULL,
  `comprobante` text COLLATE utf8_spanish_ci NOT NULL,
  `reg` datetime NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*--------------22 06 23---------------*/
CREATE TABLE `gran_turismo`.`choferes_documentos` ( `documentoId` INT(11) NOT NULL AUTO_INCREMENT , `choferId` INT(11) NOT NULL , `file` TEXT NOT NULL , `reg_file` TIMESTAMP NOT NULL , `tipo` TINYINT(1) NOT NULL COMMENT '1=comprobante, 2=identificacion,3=examedico,4=licencia' , `vigencia` DATE NOT NULL , `clase` INT(11) NOT NULL COMMENT 'clase de licencia' , `estatus` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`documentoId`)) ENGINE = InnoDB;
ALTER TABLE `choferes_documentos` ADD FOREIGN KEY (`choferId`) REFERENCES `choferes`(`choferid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `choferes` ADD `vigencia_examen` DATE NOT NULL AFTER `fecha_ingreso`, ADD `vigencia_licencia` DATE NOT NULL AFTER `vigencia_examen`, ADD `tipo_licencia` INT(11) NOT NULL AFTER `vigencia_licencia`;


/*---------------03 07 23----------------*/
CREATE TABLE `gran_turismo`.`prospectos_clientes_d` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `id_prospecto` INT(11) NOT NULL , `calle` VARCHAR(200) NOT NULL , `cp` VARCHAR(5) NOT NULL , `colonia` VARCHAR(200) NOT NULL , `ciudad_municipio` VARCHAR(200) NOT NULL , `estado` VARCHAR(200) NOT NULL , `tipo_servicio` VARCHAR(200) NOT NULL , `tipo_unidad` INT(11) NOT NULL , `cantidad_unidades` INT(11) NOT NULL, `num_pasajeros` INT(11) NOT NULL , `importe_unidad` FLOAT NOT NULL , `observaciones` TEXT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `prospectos_clientes_d` ADD FOREIGN KEY (`id_prospecto`) REFERENCES `prospectos`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `prospectos` ADD `contrato` INT NOT NULL DEFAULT '0' AFTER `tipo`;
ALTER TABLE `prospectos` ADD `calle` VARCHAR(200) NOT NULL AFTER `direccion`, ADD `colonia` VARCHAR(200) NOT NULL AFTER `calle`, ADD `estado` VARCHAR(200) NOT NULL AFTER `colonia`, ADD `ciudad` VARCHAR(200) NOT NULL AFTER `estado`;
ALTER TABLE `pagos_cliente` ADD `ine` VARCHAR(35) NOT NULL AFTER `comprobante`;



/*--unidades--*/
ALTER TABLE `unidades` ADD `fecha_reg` DATETIME NOT NULL AFTER `tipo`;
ALTER TABLE `servicio_unidades` CHANGE `estatus` `estatus` VARCHAR(1) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT '1';

/*-------06 07 23-------*/
ALTER TABLE `unidades` ADD `img` TEXT NOT NULL AFTER `tipo`;
CREATE TABLE `gran_turismo`.`unidad_prospecto` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `id_prospecto` INT(11) NOT NULL , `unidad` INT(11) NOT NULL , `cantidad` INT(11) NOT NULL , `monto` FLOAT NOT NULL , `reg` DATETIME NOT NULL , `estatus` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;

/*-------10 07 23-------*/
ALTER TABLE `unidades` ADD `poliza_seg` VARCHAR(200) NOT NULL AFTER `tipo`, ADD `vencimiento_poliza` DATE NOT NULL AFTER `poliza_seg`;

CREATE TABLE `gran_turismo`.`verificacion_unidades` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `id_unidad` INT(11) NOT NULL , `fecha` DATE NOT NULL , `tipo` INT(11) NOT NULL , `observaciones` TEXT NOT NULL , `estatus` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `verificacion_unidades` ADD FOREIGN KEY (`id_unidad`) REFERENCES `unidades`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*-------13 07 23-------*/
ALTER TABLE `prospectos` ADD `empresa` VARCHAR(250) NOT NULL AFTER `razon`;
ALTER TABLE `unidad_prospecto` ADD `cant_pasajeros` INT(11) NOT NULL AFTER `cantidad`;

/*-------17 07 23-------*/
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `tipo`) VALUES (NULL, '2', 'Contratos', 'Contratos', 'fa fa-file-o', '1');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '6');
CREATE TABLE `gran_turismo`.`contratos` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `idCliente` INT(11) NOT NULL , `lugar_origen` TEXT NOT NULL , `fecha_salida` DATE NOT NULL , `hora_salida` TIME NOT NULL , `fecha_regreso` DATE NOT NULL , `hora_regreso` TIME NOT NULL , `contrato` TINYINT(1) NOT NULL DEFAULT '0' , `fecha_reg` DATETIME NOT NULL , `estatus` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `destino_prospecto` ADD `id_contrato` INT(11) NOT NULL AFTER `id_prospecto`;
ALTER TABLE `pagos_cliente` ADD `id_contrato` INT(11) NOT NULL AFTER `id_cliente`;
ALTER TABLE `prospectos_clientes_d` ADD `id_contrato` INT(11) NOT NULL AFTER `id_prospecto`;
ALTER TABLE `unidad_prospecto` ADD `id_contrato` INT(11) NOT NULL AFTER `id_prospecto`;
ALTER TABLE `contratos` ADD `fecha_contrato` DATE NOT NULL AFTER `lugar_origen`;

/* *******18-07-23******** */
ALTER TABLE `unidades` ADD `tipo_perm` VARCHAR(1) NOT NULL AFTER `tipo`, ADD `vehiculo` TEXT NOT NULL AFTER `tipo_perm`, ADD `placas_fed` VARCHAR(35) NOT NULL AFTER `vehiculo`, ADD `serie` VARCHAR(75) NOT NULL AFTER `placas_fed`, ADD `motor` VARCHAR(75) NOT NULL AFTER `serie`;
ALTER TABLE `unidades` CHANGE `vehiculo` `vehiculo` VARCHAR(75) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL;

/* --------19 07 23--------- */
CREATE TABLE `gran_turismo`.`pagos_contrato` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `id_contrato` INT(11) NOT NULL , `referencia` VARCHAR(200) NOT NULL , `monto` FLOAT NOT NULL , `fecha` DATE NOT NULL , `reg` DATETIME NOT NULL , `id_usuario` INT(11) NOT NULL , `estatus` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `contratos` ADD `porc_anticipo` INT(11) NOT NULL AFTER `contrato`, ADD `monto_anticipo` INT(11) NOT NULL AFTER `porc_anticipo`, ADD `porc_desc` INT(11) NOT NULL AFTER `monto_anticipo`;

/* --------21 07 23--------- */
CREATE TABLE `gran_turismo`.`chofer_contrato` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `idUnidPros` INT(11) NOT NULL , `idChofer` INT(11) NOT NULL , `idContrato` INT(11) NOT NULL , `idUnidad` INT(11) NOT NULL , `estatus` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;

/* --------27 07 23--------- */
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `tipo`) VALUES (NULL, '2', 'Agenda', 'Agenda', 'fa fa-calendar', '1');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '7');

/* --------31 07 23--------- */
CREATE TABLE `gran_turismo`.`pasajeros_contrato` (`id` INT(11) NOT NULL AUTO_INCREMENT , `idUnidPros` INT NOT NULL , `pasajero` VARCHAR(255) NOT NULL , `idContrato` INT NOT NULL , `idUnidad` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

/* ------------02 08 23------------- */
ALTER TABLE `unidades` ADD `prox_ver` DATE NOT NULL AFTER `vencimiento_poliza`;

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `tipo`) VALUES (NULL, '2', 'Cotizaciones', 'Cotizaciones', 'fa fa-file-text-o', '1')
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '8')
CREATE TABLE `gran_turismo`.`cotizaciones` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `idCliente` INT(11) NOT NULL , `lugar_origen` TEXT NOT NULL , `fecha_salida` DATE NOT NULL , `hora_salida` TIME NOT NULL , `fecha_regreso` DATE NOT NULL , `hora_regreso` TIME NOT NULL , `estado` INT(11) NOT NULL , `empresa` VARCHAR(250) NOT NULL , `telefono` VARCHAR(50) NOT NULL , `correo` VARCHAR(250) NOT NULL , `medio_conoce` TINYINT(1) NOT NULL , `contrato` INT(11) NOT NULL DEFAULT '0' , `fecha_reg` DATETIME NOT NULL , `estatus` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `destino_prospecto` ADD `id_cotizacion` INT(11) NOT NULL AFTER `id_prospecto`;
ALTER TABLE `unidad_prospecto` ADD `id_cotizacion` INT(11) NOT NULL AFTER `id_prospecto`;
/*ALTER TABLE `unidad_prospecto` ADD `id_unidPros` INT(11) NOT NULL AFTER `id_contrato`;*/
ALTER TABLE `destino_prospecto` ADD `id_unidPros` INT(11) NOT NULL AFTER `id_contrato`;

/* ------------10 08 23------------- */
ALTER TABLE `contratos` ADD `estado` INT(11) NOT NULL AFTER `hora_regreso`, ADD `empresa` VARCHAR(250) NOT NULL AFTER `estado`, ADD `telefono` VARCHAR(50) NOT NULL AFTER `empresa`, ADD `correo` VARCHAR(250) NOT NULL AFTER `telefono`, ADD `medio_conoce` TINYINT(1) NOT NULL AFTER `correo`;
ALTER TABLE `contratos` ADD `id_cotizacion` INT(11) NOT NULL AFTER `idCliente`;
ALTER TABLE `contratos` ADD `cotizacion` TINYINT(1) NOT NULL DEFAULT '0' AFTER `contrato`;

/* -------------21 08 23---------------- */
ALTER TABLE `prospectos` ADD `cod_postal` VARCHAR(5) NOT NULL AFTER `correo`;

/* -------------24 08 23---------------- */
ALTER TABLE `cotizaciones` ADD `personal` INT(11) NOT NULL AFTER `estado`;
ALTER TABLE `contratos` ADD `personal` INT(11) NOT NULL AFTER `estado`;

/* --------------04 09 23----------------- */
ALTER TABLE `destino_prospecto` ADD `fecha_regreso` DATE NOT NULL AFTER `hora`, ADD `hora_regreso` TIME NOT NULL AFTER `fecha_regreso`;

/*----------------12 09 23------------------------*/
CREATE TABLE `gran_turismo`.`contrato_gastos` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `id_contrato` INT(11) NOT NULL , `tipo` TINYINT(1) NOT NULL COMMENT '1=caseta, 2=combustibles,3=otros' , `fecha` DATE NOT NULL , `descripcion` TEXT NOT NULL , `importe` FLOAT NOT NULL , `reg` DATETIME NOT NULL , `estatus` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `contrato_gastos` ADD `observaciones` TEXT NOT NULL AFTER `importe`;
ALTER TABLE `contrato_gastos` CHANGE `tipo` `tipo` TINYINT(1) NOT NULL COMMENT '0=observaciones,1=caseta, 2=combustibles,3=otros';

/* *****************14-09-23********************************* */
ALTER TABLE `prospectos` ADD `tipo_cliente` VARCHAR(1) NOT NULL AFTER `apm`;
ALTER TABLE `prospectos` CHANGE `tipo_cliente` `tipo_cliente` VARCHAR(1) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT '0';
ALTER TABLE `cotizaciones` ADD `seguimiento` VARCHAR(1) NOT NULL DEFAULT '1' COMMENT '1=creada, 2=revisada, 3=autorizada,4=expirada,5=cancelada' AFTER `contrato`;
ALTER TABLE `cotizaciones` ADD `prioridad` VARCHAR(1) NOT NULL AFTER `seguimiento`;
ALTER TABLE `cotizaciones` CHANGE `seguimiento` `seguimiento` VARCHAR(1) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT '1' COMMENT '1=creada,2=proceso, 3=revisada, 4=autorizada,5=expirada,6=cancelada,7=enviada';
ALTER TABLE `cotizaciones` ADD `observaciones` TEXT NOT NULL AFTER `prioridad`;
ALTER TABLE `cotizaciones` ADD `id_user_autoriza` INT NOT NULL AFTER `observaciones`, ADD `fecha_autoriza` DATETIME NOT NULL AFTER `id_user_autoriza`;
ALTER TABLE `cotizaciones` ADD `id_user_crea` INT NOT NULL AFTER `observaciones`;
ALTER TABLE `cotizaciones` ADD `fecha_envia` DATETIME NOT NULL AFTER `fecha_autoriza`;
/* *****************19-09-23********************************* */
ALTER TABLE `contratos` ADD `prioridad` VARCHAR(1) NOT NULL AFTER `fecha_contrato`, ADD `seguimiento` VARCHAR(1) NOT NULL DEFAULT '1' COMMENT '1=creada,2=proceso, 3=revisada, 4=autorizada,5=expirada,6=cancelada,7=enviada' AFTER `prioridad`, ADD `observaciones` TEXT NOT NULL AFTER `seguimiento`;
ALTER TABLE `contratos` ADD `liquidado` VARCHAR(1) NOT NULL DEFAULT '0' AFTER `fecha_reg`;

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `tipo`) VALUES (NULL, '1', 'Cuentas', 'Cuentas', 'fa fa-university', '1');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '9');
ALTER TABLE `pagos_contrato` ADD `id_cuenta` INT NOT NULL AFTER `id_contrato`;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gran_turismo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas`
--

DROP TABLE IF EXISTS `cuentas`;
CREATE TABLE IF NOT EXISTS `cuentas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cuenta` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `clabe` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `banco` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cuentas`
--

INSERT INTO `cuentas` (`id`, `cuenta`, `clabe`, `banco`, `estatus`) VALUES
(1, 'EFECTIVO ', 'N/A', 'N/A', '1'),
(2, '87451205', '8787418487878785', 'BBVA', '1');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


INSERT INTO `perfiles` (`perfilId`, `nombre`, `estatus`) VALUES (NULL, 'Ventas', '1'), (NULL, 'Gerencia', '1');
ALTER TABLE `cotizaciones` ADD `motivo_rechazo` TEXT NOT NULL AFTER `fecha_envia`, ADD `fecha_rechazo` DATETIME NOT NULL AFTER `motivo_rechazo`;
ALTER TABLE `cotizaciones` ADD `id_user_rechaza` INT NOT NULL AFTER `fecha_rechazo`;
ALTER TABLE `cotizaciones` ADD `es_contrato` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no,1=si' AFTER `estatus`;
ALTER TABLE `contratos` ADD `id_user_crea` INT NOT NULL AFTER `liquidado`;
ALTER TABLE `contratos` ADD `id_user_autoriza` INT NOT NULL AFTER `id_user_crea`;
ALTER TABLE `contratos` ADD `fecha_autoriza` DATETIME NOT NULL AFTER `id_user_autoriza`;
ALTER TABLE `contratos` ADD `fecha_envia` DATETIME NOT NULL AFTER `fecha_autoriza`;
ALTER TABLE `contratos` ADD `motivo_rechazo` TEXT NOT NULL AFTER `estatus`, ADD `fecha_rechazo` INT NOT NULL AFTER `motivo_rechazo`, ADD `id_user_rechaza` INT NOT NULL AFTER `fecha_rechazo`;

/*------------22/09/23---------------------------------------------------*/
CREATE TABLE `gran_turismo`.`bitacora_revisiones` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `id_contrato` INT(11) NOT NULL , `km_ini` FLOAT NOT NULL , `km_fin` FLOAT NOT NULL , `apto` TINYINT(1) NOT NULL , `revF_aceite` TINYINT(1) NOT NULL , `revF_frenos` TINYINT(1) NOT NULL , `revF_transmision` TINYINT(1) NOT NULL , `revF_anticongelante` TINYINT(1) NOT NULL , `revF_direccion` TINYINT(1) NOT NULL , `revF_neumaticos` TINYINT(1) NOT NULL , `revL_cortas` TINYINT(1) NOT NULL , `revL_direc` TINYINT(1) NOT NULL , `revL_tablero` TINYINT(1) NOT NULL , `revL_reversa` TINYINT(1) NOT NULL , `revL_largas` TINYINT(1) NOT NULL , `revL_frenos` TINYINT(1) NOT NULL , `fecha_reg` DATETIME NOT NULL , `estatus` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `gran_turismo`.`bitacora_dias` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `id_contrato` INT(11) NOT NULL , `fecha` DATE NOT NULL , `fecha_reg` DATETIME NOT NULL , `estatus` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `gran_turismo`.`bitacora_horas` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `id_contrato` INT(11) NOT NULL , `tipo` TINYINT(1) NOT NULL COMMENT '1=FueraServicio, 2=Dormido,3=Manejando,4=SinManejar' , `hora_ini` TIME NOT NULL , `hora_fin` TIME NOT NULL , `fecha_reg` DATETIME NOT NULL , `estatus` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `bitacora_horas` ADD `id_bitDia` INT(11) NOT NULL AFTER `id_contrato`;


/*-----------------28/09/23-------------------*/
ALTER TABLE `usuarios` ADD `choferId` INT(11) NOT NULL AFTER `personalId`;
INSERT INTO `perfiles` (`perfilId`, `nombre`, `estatus`) VALUES ('7', 'Chofer', '1');
ALTER TABLE `usuarios` DROP FOREIGN KEY `usuarios_ibfk_1`;
/*ALTER TABLE `usuarios` DROP INDEX `fk_usuarios_personal1_idx`;*/
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '7', '6')

/* ******************29-09-23********************/
ALTER TABLE `unidades` ADD `ctrl_vehicular` DATE NOT NULL AFTER `prox_ver`;
ALTER TABLE `servicio_unidades` ADD `importe` DECIMAL(8,2) NOT NULL AFTER `kilometraje`;
ALTER TABLE `verificacion_unidades` ADD `importe` DECIMAL(8,2) NOT NULL AFTER `tipo`;

ALTER TABLE `unidades` ADD `km_actual` DECIMAL(10,2) NOT NULL AFTER `ctrl_vehicular`;
ALTER TABLE `unidades` ADD `img_poliza` TEXT NOT NULL AFTER `img`;
ALTER TABLE `bitacora_revisiones` ADD `img_evide` TEXT NOT NULL AFTER `km_fin`;

ALTER TABLE `verificacion_unidades` ADD `img_verif` TEXT NOT NULL AFTER `observaciones`;

/* ------------29 09 23-------------- */
ALTER TABLE `contrato_gastos` ADD `cant_recibida` FLOAT NOT NULL AFTER `importe`;

/* ------------02 10 23-------------- */
ALTER TABLE `bitacora_revisiones` ADD `fecha_pdf` DATE NOT NULL AFTER `revL_frenos`;

/* ------------04 10 23-------------- */
ALTER TABLE `contratos` ADD `rel_gastos` TINYINT(1) NOT NULL DEFAULT '0' AFTER `contrato`, ADD `bit_viaje` TINYINT(1) NOT NULL DEFAULT '0' AFTER `rel_gastos`;

/* *************04-10-23********************* */
ALTER TABLE `unidades` ADD `ultimo_update` DATETIME NOT NULL AFTER `km_actual`;

INSERT INTO `menu` (`MenuId`, `Nombre`, `Icon`, `orden`) VALUES (NULL, 'Reportes', 'fa fa-bar-chart', '3');
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `tipo`) VALUES (NULL, '3', 'Edos. de Cuenta', 'Reportes', 'fa fa-money', '1');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '10');

ALTER TABLE `contratos` CHANGE `porc_anticipo` `porc_anticipo` DECIMAL(10,2) NOT NULL, CHANGE `monto_anticipo` `monto_anticipo` DECIMAL(10,2) NOT NULL, CHANGE `porc_desc` `porc_desc` DECIMAL(10,2) NOT NULL;

/* -----------------17-10-23---------------- */
ALTER TABLE `bitacora_revisiones` ADD `img_evide_f` TEXT NOT NULL AFTER `img_evide`;
ALTER TABLE `pagos_contrato` ADD `file_pago` TEXT NOT NULL AFTER `fecha`;

/* *****************19-10-23************************** */
ALTER TABLE `servicio_unidades` ADD `tipo` VARCHAR(1) NOT NULL  DEFAULT '0' AFTER `kilometraje`;
CREATE TABLE contratos_archivados ( `id` INT(11) NOT NULL AUTO_INCREMENT , `id_contrato` INT(11) NOT NULL , `file_contrato` TEXT NOT NULL , `reg` DATETIME NOT NULL , `estatus` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;

/* ****************25-10-23***************************/
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `tipo`) VALUES (NULL, '3', 'Contratos', 'Reportes/contratos', 'fa fa-line-chart ', '1');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '11');

/*-------------------08-11-23--------------------------*/
CREATE TABLE `gran_turismo`.`modulo_gastos` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `concepto` VARCHAR(300) NOT NULL , `monto` FLOAT NOT NULL , `fecha` DATE NOT NULL , `usuario` INT(11) NOT NULL , `reg` TIMESTAMP NOT NULL , `estatus` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `tipo`) VALUES (NULL, '2', 'Modulo Gastos', 'Modulo_gastos', 'fa fa-money', '1')
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '11')
/*---------09-11-23------------*/
CREATE TABLE `gran_turismo`.`cotizacion_estimados` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `id_cotizacion` INT(11) NOT NULL , `km_ini` FLOAT NOT NULL , `km_fin` FLOAT NOT NULL , `precio_gas` FLOAT NOT NULL , `casetas` FLOAT NOT NULL , `combustible` FLOAT NOT NULL , `otros` FLOAT NOT NULL , `sueldo` FLOAT NOT NULL , `utilidad` FLOAT NOT NULL , `reg` DATETIME NOT NULL , `estatus` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `cotizacion_estimados` ADD `cot_aprox` FLOAT NOT NULL AFTER `utilidad`;
/*---------10-11-23-----------*/
ALTER TABLE `unidades` ADD `rendimiento` FLOAT NOT NULL AFTER `km_actual`;
CREATE TABLE `gran_turismo`.`cotizacion_unidades` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `id_estimados` INT(11) NOT NULL , `id_cotizacion` INT(11) NOT NULL , `id_unidad` INT(11) NOT NULL , `vehiculo` VARCHAR(75) NOT NULL , `num_eco` VARCHAR(55) NOT NULL , `tipo` VARCHAR(1) NOT NULL , `reg` DATETIME NOT NULL , `estatus` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
/*---------13-11-23-----------*/
ALTER TABLE `cotizacion_unidades` ADD `cantidad` INT(11) NOT NULL AFTER `tipo`;
ALTER TABLE `cotizacion_unidades` ADD `rendimiento` FLOAT NOT NULL AFTER `cantidad`;
ALTER TABLE `cotizacion_unidades` ADD `kms_reco` FLOAT NOT NULL AFTER `rendimiento`;
ALTER TABLE `cotizacion_estimados` DROP `km_ini`, DROP `km_fin`;
ALTER TABLE `cotizacion_estimados` ADD `gasto_aprox` FLOAT NOT NULL AFTER `utilidad`;


/*----------01-12-23----------*/
CREATE TABLE `gran_turismo`.`encuesta` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `id_cliente` INT(11) NOT NULL , `id_contrato` INT(11) NOT NULL , `pregunta1` INT(11) NOT NULL , `pregunta2` INT(11) NOT NULL , `pregunta3` INT(11) NOT NULL , `pregunta4` INT(11) NOT NULL , `pregunta5` TEXT NOT NULL , `pregunta6` TEXT NOT NULL , `estatus` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
/*----------05-12-23----------*/
ALTER TABLE `contratos` ADD `token` VARCHAR(80) NOT NULL AFTER `fecha_reg`;
ALTER TABLE `contratos` ADD `encuesta` TINYINT(1) NOT NULL DEFAULT '0' AFTER `token`;
/*----------07-12-23----------*/
ALTER TABLE `encuesta` ADD `reg` DATETIME NOT NULL AFTER `pregunta6`;
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `tipo`) VALUES (NULL, '3', 'Encuesta', 'EncuestaR', 'fa fa-question-circle', '1')
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '13')
/*----------------27-12-23--------------------*/
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `tipo`) VALUES (NULL, '3', 'Corte Caja', 'Cortes_de_caja', 'fa fa-file-text-o', '1')
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '14')

/*----------------27-12-23--------------------*/
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `tipo`) VALUES (NULL, '2', 'Modulo Ingresos', 'Modulo_ingresos', 'fa fa-money', '1')
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '15')

/*----------------19-01-24--------------------*/
CREATE TABLE `gran_turismo`.`modulo_ingresos` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `concepto` VARCHAR(300) NOT NULL , `monto` FLOAT NOT NULL , `fecha` DATE NOT NULL , `responsable` INT(11) NOT NULL , `usuario` INT(11) NOT NULL , `reg` TIMESTAMP NOT NULL , `estatus` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `modulo_ingresos` CHANGE `reg` `reg` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `modulo_gastos` CHANGE `reg` `reg` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

/*-------------------30-01-24-------------------*/
ALTER TABLE `modulo_ingresos` CHANGE `responsable` `responsable` VARCHAR(250) NOT NULL;
ALTER TABLE `modulo_gastos` ADD `responsable` VARCHAR(250) NOT NULL AFTER `fecha`;


/*-------------------06-02-24-------------------*/
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '4', '1'), (NULL, '4', '2'), (NULL, '4', '3'), (NULL, '4', '4'), (NULL, '4', '5'), (NULL, '4', '6'), (NULL, '4', '7'), (NULL, '4', '8'), (NULL, '4', '9'), (NULL, '4', '10'), (NULL, '4', '11'), (NULL, '4', '12'), (NULL, '4', '13'), (NULL, '4', '14'), (NULL, '4', '15');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '2', '7'), (NULL, '2', '6'), (NULL, '2', '8'), (NULL, '2', '11'), (NULL, '2', '15');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '3', '5'), (NULL, '3', '1'), (NULL, '3', '2'), (NULL, '3', '7'), (NULL, '3', '6'), (NULL, '3', '8'), (NULL, '3', '11'), (NULL, '3', '15');

/*-------------------14-02-24-------------------*/
ALTER TABLE `contratos` ADD `estado_destino` VARCHAR(250) NOT NULL AFTER `observaciones`;
ALTER TABLE `cotizaciones` ADD `estado_destino` VARCHAR(250) NOT NULL AFTER `estado`;

/*-------------------16-02-24-------------------*/
ALTER TABLE `unidades` ADD `color` VARCHAR(11) NOT NULL AFTER `rendimiento`;


/* ******************23-02-24******************* */
ALTER TABLE `modulo_gastos` ADD `folio` VARCHAR(35) NOT NULL AFTER `responsable`, ADD `descripcion` TEXT NOT NULL AFTER `folio`;
ALTER TABLE `modulo_ingresos` ADD `folio` VARCHAR(35) NOT NULL AFTER `responsable`, ADD `descripcion` TEXT NOT NULL AFTER `folio`;
ALTER TABLE `contratos` ADD `folio` VARCHAR(35) NOT NULL AFTER `id`;




/*-------------------27-08-24---------------------*/
ALTER TABLE `contrato_gastos` ADD `id_unidad` INT(11) NOT NULL AFTER `id_contrato`;
ALTER TABLE `bitacora_revisiones` ADD `id_unidad` INT(11) NOT NULL AFTER `id_contrato`;
ALTER TABLE `bitacora_dias` ADD `id_unidad` INT(11) NOT NULL AFTER `id_contrato`;
ALTER TABLE `bitacora_horas` ADD `id_unidad` INT(11) NOT NULL AFTER `id_contrato`;
ALTER TABLE `prospectos` ADD `telefono_2` VARCHAR(50) NOT NULL AFTER `telefono`;

ALTER TABLE `unidad_prospecto` ADD `rel_gastos` INT(1) NULL DEFAULT '0' AFTER `monto`, ADD `bit_viaje` INT(1) NOT NULL DEFAULT '0' AFTER `rel_gastos`;