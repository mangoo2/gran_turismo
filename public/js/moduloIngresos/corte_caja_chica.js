var base_url = $('#base_url').val();
var count = 0;

$(document).ready(function () {
	$("#search").on("click", function () {
		load_table_movimientos();
		load_totales();
	});

	$("#export_excel").on("click", function () {
		downloadTable(1);
	});
});

function load_table_movimientos() {
	var fechaIni = $("#fecha_ini").val();
	var fechaFin = $("#fecha_fin").val();

	if (fechaIni == "") {
		fechaIni = 0;
	}
	if (fechaFin == "") {
		fechaFin = 0;
	}

	if ((fechaIni != 0 && fechaFin == 0) || (fechaIni == 0 && fechaFin != 0) || (fechaIni > fechaFin )){
		swal("Error", "Una de las fechas es incorrecta", "warning");
		return;
	}

	tabla = $("#table_data").DataTable({
		"bProcessing": true,
		"serverSide": true,
		"searching": true,
		destroy: true,
		//responsive: !0,
		"ajax": {
			"url": base_url + "Modulo_ingresos/get_list_movimientos",
			type: "post",
			data: {
				fechaIni: fechaIni,
				fechaFin: fechaFin
			},
		},
		"columns": [
			{
				"data": null,
				"render":function (row) {
					return count += 1;
				}
			},
			{ "data": "folio"},
			{
				"data": "fecha"
			},
			{
				"data": "concepto"
			},
			{
				"data": "descripcion"
			},
			{
				"data": "responsable"
			},
			{
				"data": null,
				"render": function (row) {
					var montoI = "---";
					if(row.tipo == "Ingreso"){
						montoI = new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.monto);
					}
					
					var html = '<span style="color:green;">'+montoI+'</span>';
					return html;
				}
			},
			{
				"data": null,
				"render": function (row) {
					var montoE = "---";
					if(row.tipo == "Gasto"){
						montoE = new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.monto);
					}

					var html = '<span style="color:red;"> -'+montoE+'</span>';
					return html;
				}
			},
		],
		"order": [
			[1, "desc"]
		],
		"lengthMenu": [
			[10, 25, 50],
			[10, 25, 50]
		],
		fixedColumns: true,
		language: languageTables
	});
}

function load_totales() {
	var fechaIni = $("#fecha_ini").val();
	var fechaFin = $("#fecha_fin").val();

	if (fechaIni == "") {
		fechaIni = 0;
	}
	if (fechaFin == "") {
		fechaFin = 0;
	}

	if ((fechaIni != 0 && fechaFin == 0) || (fechaIni == 0 && fechaFin != 0) || (fechaIni > fechaFin )){
		swal("Error", "Una de las fechas es incorrecta", "warning");
		return;
	}

	$.ajax({
		type: "POST",
		url: base_url + "Modulo_ingresos/get_totales",
		data: {
			fechaIni: fechaIni,
			fechaFin: fechaFin
		},
		async: false,
		success: function (result) {
			//console.log("result: " + result);
			var array = $.parseJSON(result);
			$("#cont_totales").empty();

			var html = '\
					<table class="table" style="width:100%;">\
						<tbody>\
							<tr>\
								<td scope="col">Total Ingresos:</td>\
								<td scope="col" style="text-align: right;"><b>' + new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(array.totalIngresos) + '</b></td>\
							</tr>\
							<tr>\
								<td scope="col">Total Gastos:</td>\
								<td scope="col" style="text-align: right;"><b>' + new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(array.totalEgresos) + '</b></td>\
							</tr>\
							<tr>\
								<td scope="col" style="font-size: 115%">Total Final:</td>\
								<td scope="col" style="text-align: right; font-size: 115%"><b>' + new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(array.totalFinal) + '</b></td>\
							</tr>\
							<tr>\
								<td colspan="4"> </td>\
							</tr>\
						</tbody>\
					</table>';

			$("#cont_totales").append(html);
		}
	});
}

function downloadTable(tipo){
	if(tipo == 1){
		var fechaIni = $("#fecha_ini").val();
		var fechaFin = $("#fecha_fin").val();

		if (fechaIni == "") {
			fechaIni = 0;
		}
		if (fechaFin == "") {
			fechaFin = 0;
		}

		if ((fechaIni != 0 && fechaFin == 0) || (fechaIni == 0 && fechaFin != 0) || (fechaIni > fechaFin )){
			swal("Error", "Una de las fechas es incorrecta", "warning");
			return;
		}
		
		window.open(base_url+"Modulo_ingresos/exportCorteCaja/"+fechaIni+"/"+fechaFin, '_blank');
	}
}
