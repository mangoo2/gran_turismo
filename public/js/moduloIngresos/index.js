var base_url = $('#base_url').val();

$(document).ready(function () {
	//console.log('JS');
	load_table();
	
	$("#search").on("click",function(){
		load_table();
	});
});

function load_table() {
	var fechaIni=$("#fechaIni").val();
	var fechaFin=$("#fechaFin").val();

	if ((fechaIni != 0 && fechaFin == 0) || (fechaIni == 0 && fechaFin != 0) || (fechaIni > fechaFin )){
		swal("Error", "Una de las fechas es incorrecta", "warning");
		return;
	}

	tabla = $("#table_data").DataTable({
		"bProcessing": true,
		"serverSide": true,
		"searching": true,
		destroy:true,
		//responsive: !0,
		"ajax": {
			"url": base_url + "Modulo_ingresos/get_list",
			type: "post",
			data: {fechaIni:fechaIni, fechaFin:fechaFin},
		},
		"columns": [{
				"data": "id"
			},
			{"data": "folio"},
			{
				"data": "concepto"
			},
			{
				"data": null,
				"render": function (row) {
					var html = new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.monto);
					return html;
				}
			},
			{
				"data": "fecha"
			},
			{
				"data": "responsable"
			},
			{
				"data": "Usuario"
			},
			{
				"data": null,
				"render": function (row) {
					var html = '<a title="Eliminar" onclick="delete_modulo(' + row.id + ')"><i class="fa fa-trash-o" style="font-size: 20px; cursor: pointer;"></i></a> ';
					html += ' <a title="Editar"  href="' + base_url + 'Modulo_ingresos/registro/' + row.id + '"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
					return html;
				}
			},
		],
		"order": [
			[0, "desc"]
		],
		"lengthMenu": [
			[10, 25, 50],
			[10, 25, 50]
		],
		fixedColumns: true,
		language: languageTables
	});
}

function delete_modulo(id) {
	$.confirm({
		boxWidth: '30%',
		useBootstrap: false,
		icon: 'fa fa-warning',
		title: '¡Atención!',
		content: '¿Está seguro de eliminar este registro?',
		type: 'red',
		typeAnimated: true,
		buttons: {
			confirmar: function () {
				$.ajax({
					type: 'POST',
					url: base_url + "Modulo_ingresos/delete",
					data: {
						id: id
					},
					success: function (response) {
            tabla.destroy();
						load_table();
						swal("Éxito", "Se ha eliminado correctamente", "success");
					}
				});
			},
			cancelar: function () {

			}
		}
	});
}
