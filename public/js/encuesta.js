var base_url = $('#base_url').val();
////////////////// Cuestionario
var cont = 0;
var cont2 = 1;
var valor_pregunta_1 = 0;
var valor_pregunta_2 = 0;
var valor_pregunta_3 = 0;

function siguiente() {
	//$('.preg_'+cont).css('display','none');
	//$('.preg_'+cont2).css('display','block');
	$('.preg_' + cont).hide(500);

	setTimeout(function(){
		$('.preg_' + cont2).show(500);
		cont = cont + 1;
		cont2 = cont2 + 1;
		console.log("Siguiente");
	}, 500);
	
}

function pregunta10_siguiente() {
	var pre10 = $('#pregunta10').val();
	if (pre10 != '') {
		guardar_registro();
	} else {
		Swal.fire("!Atención¡", "El campo esta vacío", "error");
	}
}

function guardar_registro() {
	var datos = 
		'id_cliente=' + $('#id_cliente').val() +
		'&id_contrato=' + $('#id_contrato').val() +
		'&pregunta1=' + $('input:radio[name=pregunta1]:checked').val() +
		'&pregunta2=' + $('input:radio[name=pregunta2]:checked').val() +
		'&pregunta3=' + $('input:radio[name=pregunta3]:checked').val() +
		'&pregunta4=' + $('input:radio[name=pregunta4]:checked').val() +
		'&pregunta5=' + $('#pregunta5').val() +
		'&pregunta6=' + $('#pregunta6').val();
		console.log("--- datos ---");
		console.log(datos);
		
	$.ajax({
		type: 'POST',
		url: base_url + 'Encuesta/insert',
		data: datos,
		statusCode: {
			404: function (data) {
				Swal.fire("Error!", "No Se encuentra el archivo!", "error");
			},
			500: function () {
				Swal.fire("Error!", "500", "error");
			}
		},
		success: function (data) {
			siguiente();
			/*
			setTimeout(function(){
			    window.location = base_url+'Encuesta';
			}, 5000);
			*/
		}
	});
}

function redireccionar() {
	window.location.href = "https://www.grandturismoexpress.com/";
}