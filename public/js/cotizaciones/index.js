var base_url = $('#base_url').val();
var tabla; var id_cli_table=0;  
var tiposUnidad = ['---', 'AUTO 4 Plazas', 'MINIVAN 10 Plazas', 'VAN14 Plazas', 'VAN 20 Plazas', 'VAN 33 Plazas', 'MIDIBUS 47 Plazas', 'AUTOBUS 47 Plazas', 'AUTOBUS 50 Plazas'];

$(document).ready(function() {
    $('#id_cliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un cliente o prospecto',
        //templateResult: formatState,
        ajax: {
            url: base_url + 'Cotizaciones/search_cliente',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemsCli = [];
                data.forEach(function(element) {
                    //console.log("element -> " + element);
                    itemsCli.push({
                        id: element.id,
                        text: element.nombre +" "+element.app +" "+element.apm,
                        //img: element.foto
                    });
                });
                return {
                    results: itemsCli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
    });
    $("#search").on("click",function(){
        loadtable();
    });
    $("#export-excel").on("click",function(){
        downloadTable(1);
    });
    $("#export-pdf").on("click",function(){
        downloadTable(2);
    });

    verificaExpira();
    //loadtable();
    $("#seguimiento_modal").on("change",function(){
        motivoRechazo();
    });

    $("#export-excel-estimado").on("click",function(){
        excelEstimado();
    });

    $("#export-excel-estimados").on("click",function(){
        excelEstimados();
    });
});

/*function formatState (state) {
    if (!state.id) { return state.text; }
    var $state = $(
        '<span ><img sytle="display: inline-block;" src="'+base_url+'public/img/'+state.img+'" /> ' + state.text + '</span>'
    );
    return $state;
}*/

function motivoRechazo(){
    var seg_mod = $("#seguimiento_modal option:selected").val();
    if(seg_mod=="6"){
        $("#cont_rechazo").show("slow");
    }else{
        $("#cont_rechazo").hide("slow");
    }
}

function verificaExpira(){
    $.ajax({
        type:'POST',
        url: base_url+"Cotizaciones/verificarExpiradas",
        success:function(response){ 
            loadtable();
        }
    });
} 

function downloadTable(tipo){
    var id_cliente=$("#id_cliente option:selected").val(); var seguimiento=$("#seguimiento option:selected").val(); var tipo_cliente=$("#tipo_cliente option:selected").val();
    var prioridad=$("#prioridad option:selected").val(); var fechai=$("#fechai").val(); var fechaf=$("#fechaf").val(); var activos=$("#activos option:selected").val();
    if(id_cliente==undefined){
        id_cliente=0;
    }
    if(fechai==""){
        fechai=0;
    }if(fechaf==""){
        fechaf=0;
    }
    window.open(base_url+"Cotizaciones/exportCotizaciones/"+id_cliente+"/"+seguimiento+"/"+tipo_cliente+"/"+prioridad+"/"+fechai+"/"+fechaf+"/"+tipo+"/"+activos, '_blank');
}

function loadtable(){
    var id_cliente=$("#id_cliente option:selected").val(); var seguimiento=$("#seguimiento option:selected").val(); var tipo_cliente=$("#tipo_cliente option:selected").val();
    var prioridad=$("#prioridad option:selected").val(); var fechai=$("#fechai").val(); var fechaf=$("#fechaf").val(); var activos=$("#activos option:selected").val();
    if(id_cliente==undefined){
        id_cliente=0;
    }
    tabla=$("#table_data").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        destroy:true,
        //responsive: !0,
        "ajax": {
            "url": base_url+"Cotizaciones/getlistado",
            type: "post",
            data: { id_cliente:id_cliente,seguimiento:seguimiento,tipo_cliente:tipo_cliente,prioridad:prioridad,fechai:fechai,fechaf:fechaf,activos:activos },
        },
        "columns": [
            {"data":"id"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';  
                    html=row.cliente;
                    return html;
                }
            },
            {"data":"fecha_reg"},
            {"data":"lugar_origen"},
            {"data":"telefono"},
            {"data":"correo"},
            {"data":"vendedor"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    if(row.prioridad=="1"){
                        html="<span class='btn btn-danger'>Alta</span>";
                    }else if(row.prioridad=="2"){
                        html="<span class='btn btn-warning'>Media</span>";
                    }else if(row.prioridad=="3"){
                        html="<span class='btn btn-info'>Baja</span>";
                    }
                    return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    if(row.seguimiento=="1"){
                        html="<span class='btn btn-light' onclick='seguimiento("+row.seguimiento+","+row.id+","+row.estatus+")'>Creada</span>";
                    }else if(row.seguimiento=="2"){
                        html="<span class='btn btn-info' onclick='seguimiento("+row.seguimiento+","+row.id+","+row.estatus+")'>En Proceso</span>";
                    }else if(row.seguimiento=="3"){
                        html="<span class='btn btn-warning' onclick='seguimiento("+row.seguimiento+","+row.id+","+row.estatus+")'>Revisada</span>";
                    }else if(row.seguimiento=="4"){
                        html="<span class='btn btn-success' onclick='seguimiento("+row.seguimiento+","+row.id+","+row.estatus+")'>Autorizada</span>";
                    }else if(row.seguimiento=="5"){
                        html="<span class='btn btn-secondary' onclick='seguimiento("+row.seguimiento+","+row.id+","+row.estatus+")'>Expirada</span>";
                    }else if(row.seguimiento=="6"){
                        html="<span class='btn btn-danger' onclick='seguimiento("+row.seguimiento+","+row.id+","+row.estatus+",\"" +row.motivo_rechazo+"\")'>Rechazada</span>";
                    }else if(row.seguimiento=="7"){
                        html="<span class='btn btn-primary' onclick='seguimiento("+row.seguimiento+","+row.id+","+row.estatus+")'>Enviada</span>";
                    }
                   
                    return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    if(row.seguimiento=="4" || row.seguimiento=="7"){
                        var html='<button style="box-shadow: 0px 15px 20px rgba(46, 229, 157, 0.4);" title="Convertir a contrato" type="button" onclick="passcont('+row.id+','+row.estatus+')"><i class="fa fa-file-text" style="font-size: 20px;"></i></a>';
                    }else{
                        var html='<button class="disabled" title="Convertir a contrato hasta ser autorizada" type="button"><i class="fa fa-file-text" style="font-size: 20px;"></i></a>';
                    }

                    html += '<button title="Datos estimados" type="button" onclick="modal_estimados('+row.id+')"><i class="fa fa-calculator" style="font-size: 20px;"></i></a>';

                    return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='<div class="form-control">';
                    if(row.estatus=="1"){
                        html+='<a target="_blank" title="Generar cotización" href="'+base_url+'Cotizaciones/cotizacion/'+row.id+'"><i class="fa fa-file-pdf-o icon_font" style="font-size: 20px;"></i></a> ';
                        if(row.seguimiento=="4"){
                            html+='<a title="Enviar por e-mail PDF" onclick="send_pdf('+row.id+','+row.idCliente+')"><i class="fa fa-envelope-o" style="font-size: 19px; cursor: pointer;"></i></a> ';
                            html+='<a title="Enviar whatsapp con link de PDF" onclick="send_WhatsApp('+row.id+','+row.telefono+')"><i class="fa fa-whatsapp" style="font-size: 20px; cursor: pointer;"></i></a> ';
                        }
                        if($("#tp").val()==1 || $("#tp").val()==4){
                            html+='<a title="Eliminar" onclick="delete_data('+row.id+',0)"><i class="fa fa-trash-o" style="font-size: 20px; cursor: pointer;"></i></a>';
                        }
                        html+='<a title="Editar" href="'+base_url+'Cotizaciones/registro/'+row.id+'"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
                    }if(row.estatus=="0"){
                       html+='<a title="Eliminar" onclick="delete_data('+row.id+',1)"><i class="fa fa-trash-o" style="font-size: 20px; cursor: pointer;"></i></a>'; 
                    }
                    html+='</div>';
                    return html;
                }
            },
        ],
        columnDefs: [
            { orderable: false, targets: 1 },
            { orderable: false, targets: 6 }
        ],
        "order": [[ 7, "asc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        fixedColumns: true,
        language: languageTables
    });
}

/* ******************************** */
function seguimiento(seguimiento,id,estatus,rechazo=0) {
    if(estatus==1){
        $("#modal_seguimiento").modal("show");
        $("#id_cont_seg").val(id);
        $("#txt_contrato").val(id);
        $("#seguimiento_modal").val(seguimiento);
        if(seguimiento==6){
            $("#cont_rechazo").show("slow");
            $("#motivo_rechazo").val(rechazo);
        }else{
            $("#cont_rechazo").hide("slow");
            $("#motivo_rechazo").val("");
        }
    }else{
        swal("Álerta", "No se puede editar seguimiento, primero se debe reactivar la cotización", "warning");
    }
}

function updateSeguimiento(){ 
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Desea cambiar el estatus de seguimiento?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Cotizaciones/updateSeguimiento",
                    data: {
                        id:$("#id_cont_seg").val(), seguimiento:$("#seguimiento_modal option:selected").val(), motivo_rechazo:$("#motivo_rechazo").val()
                    },
                    success:function(response){ 
                        $("#modal_seguimiento").modal("hide"); 
                        loadtable();
                        swal("Éxito", "Actualizado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
} 
/********************************** */

function passcont(id,estatus){
    if(estatus==1){
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: '¡Atención!',
            content: '¿Desea confirmarlo como contrato?',
            type: 'blue',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $.ajax({
                        type:'POST',
                        url: base_url+"Cotizaciones/cotizacionContrato",
                        data: {
                            id:id
                        },
                        success:function(response){  
                            loadtable();
                            swal("Éxito", "Confirmado como contrato correctamente", "success");
                        }
                    });
                },
                cancelar: function () 
                {
                    
                }
            }
        });
    }else{
        swal("Álerta", "No se puede convertir a contrato, primero se debe reactivar la cotización", "warning");
    }
}

function delete_data(id,estatus){
    if(estatus==1){
        txt='¿Está seguro de reactivar este registro?';
    }else{
        txt='¿Está seguro de eliminar este registro?';
    }
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: txt,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Cotizaciones/delete",
                    data: {
                        id:id, tabla:"cotizaciones", estatus:estatus
                    },
                    success:function(response){  
                        loadtable();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function send_pdf(idCotizacion, idCliente) {
	$.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-info',
        title: '¡Atención!',
        content: '¿Está seguro de mandar este Email?',
        type: 'green',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Cotizaciones/cotizacionMail",
                    data: {
                        id:idCotizacion
                    },
                    statusCode: {
                        404: function(data){
                            swal("Error!", "No Se encuentra el archivo", "error");
                        },
                        500: function(){
                            swal("Error!", "500", "error");
                        }
                    },
                    success:function(response){  
                        $.ajax({
							type: "POST",
							url: base_url+"Cotizaciones/sendMailPDF",
							data:{
								idCotizacion:idCotizacion,
								idCliente:idCliente,
							},
							success: function (result) {
								console.log(result);
                                swal("Éxito", "Se ha enviado correctamente", "success");
							}
						});
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
	});
}

function send_WhatsApp(id, num_tel){
    var url_pdf = base_url+"WhatsApp/cotizacion/"+id;
    window.open("https://wa.me/"+num_tel+"?text="+url_pdf+"");
}

function modal_estimados(id){
    $('#modal_estimados').modal("show");
    $('#idCoti').val(id);
    //Folio Start
    var length = 3;
    var format = (Array(length + 1).join('0') + id).slice(-length);
    var folioFormat = 'COT-'+format;
    $('#cotizacion').val(folioFormat);
    //Folio End
    get_tabla_unidades(id);
    $.ajax({
        type: 'POST',
        url: base_url+'Cotizaciones/get_data_cotizacion',
        data: {id: id},
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data){
            //console.log('<---data---> ' + data);
            const obj = JSON.parse(data);
            //console.log("obj: " + obj);
            if(obj.id == ''){
                $('#export-excel-estimado').hide();
            }else{
                $('#export-excel-estimado').show();
            }

            $('#idEstima').val(obj.id);
            $('#precio_gas').val(obj.precio_gas);
            $('#sueldo').val(obj.sueldo);
            $('#km_ini').val(obj.km_ini);
            $('#km_fin').val(obj.km_fin);
            $('#casetas').val(obj.casetas);
            $('#otros').val(obj.otros);
            $('#combustible').val(obj.combustible);
            $('#utilidad').val(obj.utilidad);
            $('#cot_aprox').val(obj.cot_aprox);
            $('#gasto_aprox').val(obj.gasto_aprox);
            $('#cliente').val(obj.cliente);
        }
    });   
}

function save_estimados(){
    var idCoti = $('#idCoti').val();
    var idEstima = $('#idEstima').val();
    var precio_gas = $('#precio_gas').val();
    var sueldo = $('#sueldo').val();
    var casetas = $('#casetas').val();
    var otros = $('#otros').val();
    var combustible = $('#combustible').val();
    var utilidad = $('#utilidad').val();
    var cot_aprox = $('#cot_aprox').val();
    var gasto_aprox = $('#gasto_aprox').val();    
    //var cliente = $('#cliente').val();

    if(precio_gas == '' || sueldo == '' || casetas == '' || otros == '' || combustible == '' || utilidad == ''){
        swal("Error", "Los campos no pueden estar vacios", "warning");
        return;
    }
    $.ajax({
        type: 'POST',
        url: base_url + 'Cotizaciones/insertEstimados',
        data: {id_cotizacion: idCoti, idEstima: idEstima, precio_gas: precio_gas, sueldo: sueldo, casetas: casetas, otros: otros, combustible: combustible, utilidad: utilidad, cot_aprox: cot_aprox, gasto_aprox:gasto_aprox},
        success: function(data) {
            //console.log('Estimados---->');
            //console.log(data);
            addUnidades(data, idCoti);
            swal("Éxito", "Guardado Correctamente", "success");
            setTimeout(function () {
                $('#modal_estimados').modal("hide")
                //window.location = base_url + 'Personal';
            }, 500);
        }
    });
}

function get_tabla_unidades(id){
	$.ajax({
        type:'POST',
		url: base_url+"Cotizaciones/get_table_units",
		data: {id : id},
		success: function (response){
            console.log("response: " + response);
			var array = $.parseJSON(response);
            $('#tbody_units').empty();
            if(array.length > 0) {
                array.forEach(function(element){
                    var tipoText = tiposUnidad[element.tipo]
                    tabla_unidades(element.id, element.id_unidad, element.vehiculo, element.num_eco, element.tipo, tipoText, element.cantidad, element.rendimiento, element.kms_reco);
                });
            }
		},
		error: function(response){
            swal("Error", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}


function tabla_unidades(id, unidad, vehiculo, num_eco, tipo, tipoText,cantidad, rendimiento, kms_reco){
	//console.log("----- Genera Row D Edicion "+idEach+" -----");
    var html = '<tr id="tr_unit_each_'+id+'" style="width: 100%">\
        <td width="25%" >\
            <input class="form-control" type="hidden" id="id" value="'+id+'" readonly>\
            <input class="form-control" type="hidden" id="id_unidad" value="'+unidad+'" readonly>\
            <input class="form-control" type="text" id="vehiculo" value="'+vehiculo+'" readonly>\
        </td>\
        <td width="10%" >\
            <input class="form-control" type="text" id="num_eco" value="'+num_eco+'" readonly>\
            <input class="form-control" type="hidden" id="rendi" value="'+rendimiento+'" readonly>\
        </td>\
        <td width="25%" >\
            <input class="form-control" type="hidden" id="tipo" value="'+tipo+'" readonly>\
            <input class="form-control" type="text" id="tipo_text" value="'+tipoText+'" readonly>\
        </td>\
        <td width="10%">\
            <input class="form-control" type="number" id="cant" value="'+cantidad+'" readonly>\
        </td>\
        <td width="20%">\
            <input class="form-control" type="number" id="kms_reco" value="'+kms_reco+'" readonly>\
        </td>\
        <td width="10%">\
            <button type="button" class="btn btn-danger" onclick="deleteUnit('+id+','+1+')" title="Eliminar unidad"><i class="fa fa-minus"></i></button>\
        </td>\
    </tr>';

    $('#tbody_units').append(html);

    //$('#tr_unidades_each_'+idEach+' #unidad_d_'+idEach).select2({width: '100%'});
    //$('#tr_unidades_each_'+idEach+' #unidad_d_'+idEach).val(unidad).trigger('change');
}

var countU = 0;
function selectUnidad(){ //id,vehiculo,num_eco,tipo
    if($('#units option:selected').val() == 0 || $('#km_reco').val() == '' ){
        swal("Error", "Seleccione unidad e ingrese kilometros recorridos", "warning");
        return;
    }
    //var id = 0;
    countU++;
    var idUni = $('#units option:selected').val();
    var vehiculo = $('#units option:selected').text();
    var num_eco = $('#units option:selected').data('eco');
    var tipo = $('#units option:selected').data('type');
    var tipoText = tiposUnidad[tipo];
    var cantidad = 1;
    var rendimiento = $('#units option:selected').data('rendi');
    var kms_reco = $('#km_reco').val();

    if(kms_reco == '' || kms_reco < 0 ){
        kms_reco = 0;
    }

    $('#units').val(0);
    $('#km_reco').val('');

    var html = '<tr id="tr_unit_'+countU+'" style="width: 100%">\
                    <td width="25%" >\
                        <input class="form-control" type="hidden" id="id" value="0" readonly>\
                        <input class="form-control" type="hidden" id="id_unidad" value="'+idUni+'" readonly>\
                        <input class="form-control" type="text" id="vehiculo" value="'+vehiculo+'" readonly>\
                    </td>\
                    <td width="10%" >\
                        <input class="form-control" type="text" id="num_eco" value="'+num_eco+'" readonly>\
                        <input class="form-control" type="hidden" id="rendi" value="'+rendimiento+'" readonly>\
                    </td>\
                    <td width="25%" >\
                        <input class="form-control" type="hidden" id="tipo" value="'+tipo+'" readonly>\
                        <input class="form-control" type="text" id="tipo_text" value="'+tipoText+'" readonly>\
                    </td>\
                    <td width="10%">\
                        <input class="form-control" type="number" id="cant" value="'+cantidad+'" readonly>\
                    </td>\
                    <td width="20%">\
                        <input class="form-control" type="number" id="kms_reco" value="'+kms_reco+'" readonly>\
                    </td>\
                    <td width="10%">\
                        <button type="button" class="btn btn-danger" onclick="deleteUnit('+countU+')" title="Eliminar unidad"><i class="fa fa-minus"></i></button>\
                    </td>\
                </tr>';

    $('#tbody_units').append(html);
    calcular_cotizacion(0);
}

function deleteUnit(row, type = 0){
    if(type > 0){
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: '¡Atención!',
            content: '¿Está seguro de eliminar esta unidad?',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $.ajax({
                        type:'POST',
                        url: base_url+"Cotizaciones/delete",
                        data: {
                            id:row, tabla:"cotizacion_unidades"
                        },
                        success:function(response){ 
    
                            $('#tr_unit_each_' + row).remove();
                            swal("Éxito", "Se ha eliminado correctamente", "success");
                        }
                    });
                },
                cancelar: function () 
                {
                    
                }
            }
        });
    }else{
        $('#tr_unit_' + row).remove();
    }
    calcular_cotizacion(0);
}

function addUnidades(id,idCotizacion){
    var DATA = [];
    var TABLAU = $("#table_units tbody > tr");//.tr_destinos
    TABLAU.each(function() {
        item = {};
        item["id_estimados"] = id;
        item["id_cotizacion"] = idCotizacion;
        item["id"] = $(this).find("input[id='id']").val();
        item["id_unidad"] = $(this).find("input[id='id_unidad']").val();
        item["vehiculo"] = $(this).find("input[id='vehiculo']").val();
        item["num_eco"] = $(this).find("input[id='num_eco']").val();
        item["tipo"] = $(this).find("input[id='tipo']").val();
        item["cantidad"] = $(this).find("input[id='cant']").val();
        item["rendimiento"] = $(this).find("input[id='rendi']").val();
        item["kms_reco"] = $(this).find("input[id='kms_reco']").val();
        DATA.push(item);
    });
    
    INFO = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    //console.log('Unidades: '+aInfo);

    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url + 'Cotizaciones/insertUnits',
        processData: false,
        contentType: false,
        success: function(data) {
            //console.log(data);
            //$('#modal_estimados').modal("hide");
        }
    });
}

function calcular_cotizacion(editTcombustible = 0){
    console.log("change");
    var costoGas = $('#precio_gas').val() != '' ? $('#precio_gas').val() : 0;
    var casetas = $('#casetas').val() != '' ? $('#casetas').val() : 0;
    var sueldo = $('#sueldo').val() != '' ? $('#sueldo').val() : 0;
    var otros = $('#otros').val() != '' ? $('#otros').val() : 0;
    var utility = $('#utilidad').val() != '' ? $('#utilidad').val() : 0;

    if( costoGas < 0 || casetas < 0 || sueldo < 0 || otros < 0 || utility < 0 ){
        swal("Error", "Verifique sus datos", "warning");
        $('#btnSaveEstima').attr('disabled',true);
        return;
    }

    $('#btnSaveEstima').attr('disabled',false);
    
    var cant = 1;
    var gasto_combustible = 0;
    var total_L_gas = 0;
    var gasto_aprox = 0;
    var cotiza_aprox = 0;

    var TABLA = $("#table_units tbody > tr");
    TABLA.each(function() {
        cant =  $(this).find("input[id='cant']").val();
        var rendimiento =  $(this).find("input[id='rendi']").val();
        if (rendimiento == 0 ){
            rendimiento = 1;
        }
        //console.log("rendimiento: " +rendimiento);
        var kms = $(this).find("input[id='kms_reco']").val();
        //console.log("kms_reco: " +kms);
        var cant_gas = (kms/rendimiento) * cant;
        //console.log("cantGas: " +cant_gas);
        total_L_gas += cant_gas;
        //console.log("TotalL: " + total_L_gas);
    });

    if(editTcombustible > 0){
        gasto_combustible = $('#combustible').val();
    }else{
        gasto_combustible = (parseFloat(total_L_gas) * parseFloat(costoGas)).toFixed(2);
        //console.log('Lgasss ' + parseFloat(total_L_gas))
        //console.log('$gasss ' + parseFloat(costoGas))
        $('#combustible').val(gasto_combustible);
    }
    //console.log("gasto_coombustible: " + gasto_combustible);
    gasto_aprox = (parseFloat(sueldo) + parseFloat(casetas) + parseFloat(otros) + parseFloat(gasto_combustible)).toFixed(2);
    $('#gasto_aprox').val(gasto_aprox);
    cotiza_aprox = parseFloat(gasto_aprox) * (parseFloat(utility)/100);//utilidad_aprox $
    $('#cot_aprox').val( (parseFloat(cotiza_aprox) + parseFloat(gasto_aprox)).toFixed(2) );
    //console.log(casetas);
}

function excelEstimado(){
    var idCot = $('#idCoti').val();
    window.open(base_url+"Cotizaciones/exportEstimado/"+idCot, '_blank');
    //window.open(base_url+"Contratos/exportPagos/"+id, '_blank');
}

function excelEstimados(){
    //console.log('estimados');

    var id_cliente=$("#id_cliente option:selected").val(); 
    var seguimiento=$("#seguimiento option:selected").val(); 
    var tipo_cliente=$("#tipo_cliente option:selected").val();
    var prioridad=$("#prioridad option:selected").val(); 
    var fechai=$("#fechai").val(); 
    var fechaf=$("#fechaf").val(); 
    var activos=$("#activos option:selected").val();

    if(id_cliente==undefined){
        id_cliente=0;
    }

    if(fechai==""){
        fechai=0;
    }if(fechaf==""){
        fechaf=0;
    }
    
    window.open(base_url+"Cotizaciones/exportEstimados/"+id_cliente+"/"+seguimiento+"/"+tipo_cliente+"/"+prioridad+"/"+fechai+"/"+fechaf+"/"+activos, '_blank');
}