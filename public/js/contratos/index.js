var base_url = $('#base_url').val();
var tabla;
var id_cli_table = 0;
var comprobanteId;
var contratoId;
var isGenerado = '';

var contu = 0;
var contCh = 0;
var perfilID = 0;

var auxCont = 0;
var auxRow = 0;
var typeCuenta = '';
var edicion = false;
var saveBtnPress = false;

$(document).ready(function () {
	perfilID = $('#id_perfil').val();

	if (perfilID == 7) {
		$('.hide-for-chofer').hide();
	}

	loadtable();
	$('#estado').select2({
		width: '100%',
		dropdownParent: $('#modal_contrato'),
	});
	$('#tipo_unidad').select2({
		width: '100%',
		dropdownParent: $('#modal_contrato'),
	});


	loadFilePago();
	loadFileContrato();

	$('#modal_comprobante').on('hidden.bs.modal', function (e) {
		$('#modal_pagos').focus();
	});


	$('#id_cliente').select2({
		width: 'resolve',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un cliente o prospecto',
		ajax: {
			url: base_url + 'Contratos/search_cliente',
			dataType: "json",
			data: function (params) {
				var query = {
					search: params.term,
					type: 'public'
				}
				return query;
			},
			processResults: function (data) {
				var itemsCli = [];
				data.forEach(function (element) {
					//console.log("element -> " + element);
					itemsCli.push({
						id: element.id,
						text: element.nombre + " " + element.app + " " + element.apm
					});
				});
				return {
					results: itemsCli
				};
			},
		}
	}).on('select2:select', function (e) {
		var data = e.params.data;
	});
	$("#search").on("click", function () {
		loadtable();
	});
	$("#export-excel").on("click", function () {
		downloadTable(1);
	});
	$("#export-pdf").on("click", function () {
		downloadTable(2);
	});

	$("#export-excel-pays").on("click", function () {
		exportPagos();
	});

	$("#seguimiento_modal").on("change", function () {
		motivoRechazo();
	});
});


function loadFilePago() {
	var img_file_p = "";
	var imgdet_p = "";
	var typePDF_p = "";

	if ($("#comp_pago_aux").val() != "" && $("#comp_pago_aux").val() != " ") {
		img_file_p = '' + base_url + "public/uploads/pagos/" + $("#comp_pago_aux").val() + '';
		ext = $("#comp_pago_aux").val().split('.');
		if (ext[1] != "pdf") {
			imgdet_p = {
				type: "image",
				url: "" + base_url + "Contratos/delete_comprobante_pago/" + $("#id_pago_aux").val(),
				caption: $("#comp_pago_aux").val()
			}
			typePDF_p = "false";
		} else {
			imgdet_p = {
				type: "pdf",
				url: base_url + "Contratos/delete_comprobante_pago/" + $("#id_pago_aux").val(),
				caption: $("#comp_pago_aux").val()
			};
			typePDF_p = "true";
		}
	}

	$('#comp_pago').fileinput({
		showCaption: false, // Ocultar titulo
		showUpload: false, // Ocultar el botón "Upload File"

		allowedFileExtensions: ["png", "jpg", "jpeg", "bmp", "pdf"],
		browseLabel: 'Seleccionar comprobante de pago',
		uploadUrl: base_url + 'Contratos/insert_comprobante_pago',
		maxFilePreviewSize: 5000,
		maxFileSize: 5000,
		elErrorContainer: '#kv-avatar-errors-1',
		msgErrorClass: 'alert alert-block alert-danger',
		initialPreview: [
			'' + img_file_p + '',
		],
		initialPreviewAsData: typePDF_p,
		initialPreviewFileType: 'image', // image is the default and can be overridden in config below
		initialPreviewConfig: [
			imgdet_p
		],
		previewFileIconSettings: {
			'png': '<i class="fa fa-file-photo-o text-warning"></i>',
			'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
			'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
			'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
			'pdf': '<i class="fa fa-pdf-o text-warning"></i>'
		},
		uploadExtraData: function (previewId, index) {
			var info = {
				id_comprobante: comprobanteId,
			};
			return info;
		}
	}).on('fileuploaded', function (event, files, extra) {
		//location.reload();
		//console.log('UPLOADEDfile');
		//$('#comp_pago_aux').val();
		swal("Éxito", "Guardado Correctamente", "success");

		setTimeout(function () {
			$("#modal_comprobante").modal("hide");
			$('#comp_pago').fileinput('clear');
			//$('#comp_pago_aux').val();
			//console.log($('#idContrato').val()+', '+$('#idcliente').val());
			//modal_pagos($('#idContrato').val(),$('#idcliente').val());

			if (auxCont == 0) {
				auxCont++;
				//console.log('AjaxTable');

				$.ajax({
					type: 'POST',
					url: base_url + 'Contratos/get_data_1_pago',
					data: {
						id: comprobanteId
					},
					statusCode: {
						404: function (data) {
							swal("Error!", "No Se encuentra el archivo", "error");
						},
						500: function () {
							swal("Error!", "500", "error");
						}
					},
					success: function (data) {
						const objeto = JSON.parse(data);
						//console.log("DATOS-> " + data);

						if (Object.keys(objeto).length > 0) {

							if (!edicion) {
								get_table_pagos(objeto.id, typeCuenta, objeto.referencia, objeto.fecha, objeto.monto, objeto.file_pago);
							} else {
								//$('#tr_pago_' + auxRow +' #btn_'+ auxRow).addClass('bg-danger');
								$('#tr_pago_' + auxRow + ' #btn_' + auxRow).attr('data-file', objeto.file_pago);
								auxRow = 0;
							}

						}

					}
				});
			}

		}, 1000);

	}).on('filedeleted', function (event, files, extra) {
		//location.reload();
		//console.log('deletedfile');
		$('#tr_pago_' + auxRow + ' #btn_' + auxRow).attr('data-file', ' ');
		auxRow = 0;
		isGenerado = '';

	}).on('fileuploaderror', function (event, files, extra) {
		//location.reload();
		//console.log('File up error');

		if (!saveBtnPress) {
			swal("Álerta", "Peso máximo (4MB.) superado! ", "warning");
			$('#comp_pago').fileinput('clear');
		} else {
			swal("Álerta", "Pago guardado correctamente, Reintente subir comprobante", "warning");
			saveBtnPress = false;
			if (auxCont == 0) {
				auxCont++;

				$.ajax({
					type: 'POST',
					url: base_url + 'Contratos/get_data_1_pago',
					data: {
						id: comprobanteId
					},
					statusCode: {
						404: function (data) {
							swal("Error!", "No Se encuentra el archivo", "error");
						},
						500: function () {
							swal("Error!", "500", "error");
						}
					},
					success: function (data) {
						const objeto = JSON.parse(data);
						//console.log("DATOS-> " + data);

						if (Object.keys(objeto).length > 0) {
							get_table_pagos(objeto.id, typeCuenta, objeto.referencia, objeto.fecha, objeto.monto, ' ');
						}

					}
				});
			}

		}
	});
}

function loadFileContrato() {
	var img_file_c = "";
	var imgdet_c = "";
	var typePDF_c = "";

	$('#comp_contrato').fileinput({
		showCaption: false, // Ocultar titulo
		showUpload: false, // Ocultar el botón "Upload File"

		allowedFileExtensions: ["png", "jpg", "jpeg", "bmp", "pdf"],
		browseLabel: 'Seleccionar contrato',
		uploadUrl: base_url + 'Contratos/insert_contrato_archivado',
		maxFilePreviewSize: 5000,
		elErrorContainer: '#kv-avatar-errors-1',
		msgErrorClass: 'alert alert-block alert-danger',
		initialPreview: [
			'' + img_file_c + '',
		],
		initialPreviewAsData: typePDF_c,
		initialPreviewFileType: 'image', // image is the default and can be overridden in config below
		initialPreviewConfig: [
			imgdet_c
		],
		previewFileIconSettings: {
			'png': '<i class="fa fa-file-photo-o text-warning"></i>',
			'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
			'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
			'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
			'pdf': '<i class="fa fa-pdf-o text-warning"></i>'
		},
		uploadExtraData: function (previewId, index) {
			var info = {
				id_contrato: contratoId,
			};
			return info;
		}
	}).on('fileuploaded', function (event, files, extra) {
		//location.reload();

		$(".sav_cont").prop("disabled", true);

		setTimeout(function () {
			$("#modal_carga_contrato").modal("hide");
			//$('#comp_contrato').fileinput('destroy');
			$('#comp_contrato').fileinput('clear');
			$(".sav_cont").prop("disabled", false);
			//addContrato();
		}, 1000);


	}).on('filedeleted', function (event, files, extra) {
		//location.reload();
	});
}


function loadFileContratoArchivado(index) {
	var img_file_con = "";
	var imgdet_con = "";
	var typePDF_con = "";

	if ($("#comp_contrato_aux_" + index).val() != "") {
		img_file_con = '' + base_url + "public/uploads/contratos/archivados/" + $("#comp_contrato_aux_" + index).val() + '';
		ext = $("#comp_contrato_aux_" + index).val().split('.');
		if (ext[1] != "pdf") {
			imgdet_con = {
				type: "image",
				url: "" + base_url + "Contratos/delete_contrato_archivado/" + $("#id_contrato_aux_" + index).val(),
				caption: $("#comp_contrato_aux_" + index).val()
			}
			typePDF_con = "false";
		} else {
			imgdet_con = {
				type: "pdf",
				url: base_url + "Contratos/delete_contrato_archivado/" + $("#id_contrato_aux_" + index).val(),
				caption: $("#comp_contrato_aux_" + index).val()
			};
			typePDF_con = "true";
		}
	}

	$('#comp_contrato_' + index).fileinput({
		showCaption: false, // Ocultar titulo
		showUpload: false, // Ocultar el botón "Upload File"
		showBrowse: false,

		allowedFileExtensions: ["png", "jpg", "jpeg", "bmp", "pdf"],
		browseLabel: 'Seleccionar contrato',
		uploadUrl: base_url + 'Contratos/insert_contrato_archivado',
		maxFilePreviewSize: 5000,
		elErrorContainer: '#kv-avatar-errors-1',
		msgErrorClass: 'alert alert-block alert-danger',
		initialPreview: [
			'' + img_file_con + '',
		],
		initialPreviewAsData: typePDF_con,
		initialPreviewFileType: 'image', // image is the default and can be overridden in config below
		initialPreviewConfig: [
			imgdet_con
		],
		previewFileIconSettings: {
			'png': '<i class="fa fa-file-photo-o text-warning"></i>',
			'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
			'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
			'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
			'pdf': '<i class="fa fa-pdf-o text-warning"></i>'
		},
		uploadExtraData: function (previewId, index) {
			var info = {
				id_contrato: contratoId,
			};
			return info;
		}
	}).on('fileuploaded', function (event, files, extra) {
		//location.reload();
	}).on('filedeleted', function (event, files, extra) {
		//location.reload();
	});
}

function downloadTable(tipo) {
	var id_cliente = $("#id_cliente option:selected").val();
	var seguimiento = $("#seguimiento option:selected").val();
	var tipo_cliente = $("#tipo_cliente option:selected").val();
	var prioridad = $("#prioridad option:selected").val();
	var fechai = $("#fechai").val();
	var fechaf = $("#fechaf").val();
	if (id_cliente == undefined) {
		id_cliente = 0;
	}
	if (fechai == "") {
		fechai = 0;
	}
	if (fechaf == "") {
		fechaf = 0;
	}
	var liquidado = $("#liquidado option:selected").val();
	window.open(base_url + "Contratos/exportContratos/" + id_cliente + "/" + seguimiento + "/" + tipo_cliente + "/" + prioridad + "/" + fechai + "/" + fechaf + "/" + tipo + "/" + liquidado, '_blank');
}

function motivoRechazo() {
	var seg_mod = $("#seguimiento_modal option:selected").val();
	if (seg_mod == "6") {
		$("#cont_rechazo").show("slow");
	} else {
		$("#cont_rechazo").hide("slow");
	}
}

function showPagado() {
	var restante_pago = $('#restante').val();
	//console.log('RESTANTE: ' + $('#restante').val());
	//console.log('RESTANTE: ' + restante_pago);

	if (restante_pago <= 0) {
		$('.showPagado').show('show');
		$('.showPago').hide('show');
		loadtable();
	} else {
		$('.showPago').show('show');
		$('.showPagado').hide('show');
	}
}

function loadtable() {
	var id_cliente = $("#id_cliente option:selected").val();
	var seguimiento = $("#seguimiento option:selected").val();
	var tipo_cliente = $("#tipo_cliente option:selected").val();
	var prioridad = $("#prioridad option:selected").val();
	var fechai = $("#fechai").val();
	var fechaf = $("#fechaf").val();
	if (id_cliente == undefined) {
		id_cliente = 0;
	}
	var liquidado = $("#liquidado option:selected").val();

	var choferID = 0;
	let url = "Contratos/getlistado";

	if (perfilID == 7) {
		url = "Contratos/getlistadoChofer";
		choferID = $('#id_chofer').val();
	}

	//console.log("url: " + url + " - " +perfilID);
	tabla = $("#table_data").DataTable({
		"bProcessing": true,
		"serverSide": true,
		"searching": true,
		destroy: true,
		//responsive: !0,
		"ajax": {
			"url": base_url + url,
			type: "post",
			data: {
				id_cliente: id_cliente,
				seguimiento: seguimiento,
				tipo_cliente: tipo_cliente,
				prioridad: prioridad,
				fechai: fechai,
				fechaf: fechaf,
				liquidado: liquidado,
				choferID: choferID
			},
		},
		"columns": [{
				"data": "id"
			},
			{
				"data": "folio"
			},
			{
				"data": null,
				"render": function (data, type, row, meta) {
					//console.log("row" + JSON.stringify(row.fecha_contrato));
					var html = '';
					html = row.cliente;
					return html;
				}
			},
			{
				"data": "fecha_reg"
			},
			{
				"data": "lugar_origen"
			},
			/*{"data": null, 
					"render": function ( data, type, row, meta ){ 
						var html='';   
						html= row.fecha_contrato; 
						return html; 
					} 
				},*/ 
			{
				"data": "telefono"
			},
			{
				"data": "correo"
			},
			{
				"data": "vendedor"
			},
			{
				"data": null,
				type: 'formatted-num',
				"render": function (url, type, row) {
					var totals = 0;
					totals = new Intl.NumberFormat('es-MX', {
						style: 'currency',
						currency: 'MXN'
					}).format(row.tot_unids);
					return totals;
				}
			},
			{
				"data": null,
				"render": function (data, type, row, meta) {
					var html = '';
					if (row.liquidado == "1") {
						html = "<span class='btn btn-success'>Pagado</span>";
					} else {
						resta = Number(row.tot_unids) - Number(row.monto_anticipo) - Number(row.porc_desc) - row.tot_pagos;
						restafin = new Intl.NumberFormat('es-MX', {
							style: 'currency',
							currency: 'MXN'
						}).format(resta);
						html = "<span class='btn btn-warning'>" + restafin + " Pendiente</span>";
					}
					return html;
				}
			},
			{
				"data": null,
				"render": function (data, type, row, meta) {
					var html = '';
					if (row.prioridad == "1") {
						html = "<span class='btn btn-danger'>Alta</span>";
					} else if (row.prioridad == "2") {
						html = "<span class='btn btn-warning'>Media</span>";
					} else if (row.prioridad == "3") {
						html = "<span class='btn btn-info'>Baja</span>";
					}
					return html;
				}
			},
			{
				"data": null,
				"render": function (data, type, row, meta) {
					var html = '';
					if (row.seguimiento == "1") {
						html = "<span class='btn btn-light' onclick='seguimiento(" + row.seguimiento + "," + row.id + "," + row.estatus + ")'>Creada</span>";
					} else if (row.seguimiento == "2") {
						html = "<span class='btn btn-info' onclick='seguimiento(" + row.seguimiento + "," + row.id + "," + row.estatus + ")'>En Proceso</span>";
					} else if (row.seguimiento == "3") {
						html = "<span class='btn btn-warning' onclick='seguimiento(" + row.seguimiento + "," + row.id + "," + row.estatus + ")'>Revisada</span>";
					} else if (row.seguimiento == "4") {
						html = "<span class='btn btn-success' onclick='seguimiento(" + row.seguimiento + "," + row.id + "," + row.estatus + ")'>Autorizada</span>";
					} else if (row.seguimiento == "5") {
						html = "<span class='btn btn-secondary' onclick='seguimiento(" + row.seguimiento + "," + row.id + "," + row.estatus + ")'>Expirada</span>";
					} else if (row.seguimiento == "6") {
						html = "<span class='btn btn-danger' onclick='seguimiento(" + row.seguimiento + "," + row.id + "," + row.estatus + ",\"" + row.motivo_rechazo + "\")'>Rechazada</span>";
					} else if (row.seguimiento == "7") {
						html = "<span class='btn btn-primary' onclick='seguimiento(" + row.seguimiento + "," + row.id + "," + row.estatus + ")'>Enviada</span>";
					}
					return html;
				}
			},
			{
				"data": null,
				"render": function (data, type, row, meta) {
					//console.log("row:" + JSON.stringify(row));
					numTel = 0;
					if (row.telefono != "") {
						numTel = row.telefono;
					}

					html = '<a class="btn btn-flat btn-primary" onclick="modal_acciones(' + row.id + ',' + row.contrato + ',' + row.cotizacion + ',' + row.id_cotizacion + ',' + row.idCliente + ',' + numTel + ',' + row.seguimiento + ',' + row.rel_gastos + ',' + row.bit_viaje + ')"><i class="fa fa-caret-square-o-down "></i></a>';
					return html;
				}
			},
			{
				"data": null,
				"render": function (data, type, row, meta) {
					var html = '<div class="form-control">';

					if (perfilID != 7) {

						if (row.contrato != 0) {
							html += '<a title="Pagos" onclick="modal_pagos(' + row.id + ',' + row.idCliente + ')"><i class="fa fa-dollar icon_font" style="font-size: 18px;color:#00319e; cursor: pointer;"></i></a>';

							html += '<a title="Enviar encuesta por e-mail" onclick="enviar_encuesta(' + row.id + ',' + row.idCliente + ', \'' + row.correo + ' \')"><i class="fa fa-question-circle" style="font-size: 19px; color:#00319e; cursor: pointer;"></i></a> ';
							html += '<a title="Enviar encuesta por whatsapp" onclick="enviar_whatsapp(' + row.id + ',' + row.idCliente + ', \'' + row.telefono + ' \')"><i class="fa fa-whatsapp" style="font-size: 19px; color:#00319e; cursor: pointer;"></i></a> ';

						}
						html += '<a title="Editar" href="' + base_url + 'Contratos/registro/' + row.id + '"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
						if ($("#tp").val() == 1 || $("#tp").val() == 4) {
							html += '<a title="Eliminar" onclick="delete_data(' + row.id + ')"><i class="fa fa-trash-o" style="font-size: 20px; cursor: pointer;"></i></a>';
						}

					}

					html += '</div>';
					return html;
				}
			},
			
			{
				"data": "fecha_contrato",
				"visible": false,
				"render": function (data, type, row) {
						return (data && data !== "00-00-0000") ? data : "";
				}
			},
			
		],
		columnDefs: [
			{
				orderable: false,
				targets: 8	
			},
			{
				targets: 14, 
				//visible: false, 
				searchable: false 
			}	
			
		],
			//"order": [ [14, "desc"] ],
			//"order": [ [10, "asc"] ],
			"order": [ [10, "asc"], [14, "desc"] ],
			//"order": [ [0, "asc"] ],
		fixedColumns: true,
		language: languageTables
	});
}



/* ******************************** */
function seguimiento(seguimiento, id, estatus, rechazo = 0) {
	if (perfilID == 7) {
		return;
	}

	if (estatus == 1) {
		$("#modal_seguimiento").modal("show");
		$("#id_cont_seg").val(id);
		$("#txt_contrato").val(id);
		$("#seguimiento_modal").val(seguimiento);
		if (seguimiento == 6) {
			$("#cont_rechazo").show("slow");
			$("#motivo_rechazo").val(rechazo);
		} else {
			$("#cont_rechazo").hide("slow");
			$("#motivo_rechazo").val("");
		}
	} else {
		swal("Álerta", "No se puede editar seguimiento, primero se debe reactivar la cotización", "warning");
	}
}

function updateSeguimiento() {
	$.confirm({
		boxWidth: '30%',
		useBootstrap: false,
		icon: 'fa fa-warning',
		title: '¡Atención!',
		content: '¿Desea cambiar el estatus de seguimiento?',
		type: 'red',
		typeAnimated: true,
		buttons: {
			confirmar: function () {
				$.ajax({
					type: 'POST',
					url: base_url + "Contratos/updateSeguimiento",
					data: {
						id: $("#id_cont_seg").val(),
						seguimiento: $("#seguimiento_modal option:selected").val(),
						motivo_rechazo: $("#motivo_rechazo").val()
					},
					success: function (response) {
						$("#modal_seguimiento").modal("hide");
						loadtable();
						swal("Éxito", "Se ha eliminado correctamente", "success");
					}
				});
			},
			cancelar: function () {

			}
		}
	});
}
/********************************** */

function delete_data(id) {
	$.confirm({
		boxWidth: '30%',
		useBootstrap: false,
		icon: 'fa fa-warning',
		title: '¡Atención!',
		content: '¿Está seguro de eliminar este registro?',
		type: 'red',
		typeAnimated: true,
		buttons: {
			confirmar: function () {
				$.ajax({
					type: 'POST',
					url: base_url + "Contratos/delete",
					data: {
						id: id,
						tabla: "contratos"
					},
					success: function (response) {
						loadtable();
						swal("Éxito", "Se ha eliminado correctamente", "success");
					}
				});
			},
			cancelar: function () {

			}
		}
	});
}

//---Unidad--------------------------------------
var cont_ant_u = 0;

function addUnidad() {
	contu++;
	clone = $("#tr_unidades").clone().attr('id', "unidades_form_" + contu);
	clone.find("input").val("");
	clone.find("button").attr("onclick", "eliminarAddU(" + contu + ")").removeClass("btn-success").addClass("btn-danger");
	clone.find("#btn_plus").removeClass("fa fa-plus").addClass("fa fa-minus");

	// Obtener todos los elementos con la clase 
	const elementos = document.querySelectorAll(".lastBtn");

	// Recorrer la lista de elementos y quitar la clase 
	for (let i = 0; i < elementos.length; i++) {
		elementos[i].classList.remove("lastBtn");
	}

	//Agregamos la clase al elemento actual
	clone.addClass("lastBtn");
	//console.log("contu: "+contu);
	if (contu == 0 || contu == 1) {
		clone.insertAfter("#tr_unidades");
		//console.log("Antes");
	} else {
		cont_ant_u = contu - 1;
		clone.insertAfter("#unidades_form_" + cont_ant_u + "");
		//console.log("Despues");
	}
	cont_ant_u = contu;
}

function eliminarAddU(row_id) {
	const miElemento = document.getElementById('unidades_form_' + row_id);
	contu--;
	cont_ant_u--;
	$('#unidades_form_' + row_id).remove();
}

function eliminarUnit(id) {
	$.confirm({
		boxWidth: '30%',
		useBootstrap: false,
		icon: 'fa fa-warning',
		title: '¡Atención!',
		content: '¿Está seguro de eliminar esta unidad?',
		type: 'red',
		typeAnimated: true,
		buttons: {
			confirmar: function () {
				$.ajax({
					type: 'POST',
					url: base_url + "Contratos/delete",
					data: {
						id: id,
						tabla: "unidad_prospecto"
					},
					statusCode: {
						404: function (data) {
							swal("Error!", "No Se encuentra el archivo", "error");
						},
						500: function () {
							swal("Error!", "500", "error");
						}
					},
					success: function (response) {
						$('#tr_each_uni_' + id).remove();
						swal("Éxito", "Se ha eliminado correctamente", "success");
					}
				});
			},
			cancelar: function () {

			}
		}
	});
}

function addFechaContrato(id, date) {
	$.ajax({
		type: 'POST',
		url: base_url + 'Contratos/updateFechaContrato',
		data: {
			id: id,
			date: date
		},
		statusCode: {
			404: function (data) {
				swal("Error!", "No Se encuentra el archivo", "error");
			},
			500: function () {
				swal("Error!", "500", "error");
			}
		},
		success: function (data) {
			//console.log(data);
		}
	});
}

function addUnidades(id) {
	var DATA = [];
	var TABLA = $("#table_unidades tbody > tr");

	TABLA.each(function () {
		item = {};
		item["id_contrato"] = id;
		item["id_prospecto"] = $('#id_prospecto').val();

		item["id"] = $(this).find("input[id*='id']").val();
		item["unidad"] = $(this).find("option:selected").val();
		item["cantidad"] = $(this).find("input[id*='cantidad']").val();
		item["cant_pasajeros"] = $(this).find("input[id*='cant_pasajeros']").val();
		item["monto"] = $(this).find("input[id*='monto']").val();
		if ($(this).find("option:selected").val() != "0") {
			DATA.push(item);
		}
	});

	INFO = new FormData();
	aInfo = JSON.stringify(DATA);
	INFO.append('data', aInfo);
	//console.log('Unidades: '+aInfo);

	$.ajax({
		data: INFO,
		type: 'POST',
		url: base_url + 'Contratos/insertUnidad',
		processData: false,
		contentType: false,
		statusCode: {
			404: function (data) {
				swal("Error!", "No Se encuentra el archivo", "error");
			},
			500: function () {
				swal("Error!", "500", "error");
			}
		},
		success: function (data) {
			//console.log(data);
			$('#cantidad').val('');
			$('#cant_pasajeros').val('');
			$('#monto').val('');
			$('#tipo_unidad').val('0').trigger('change');
		}
	});
}

function get_unidades_contrato(idCot) {
	$.ajax({
		type: 'POST',
		url: base_url + 'Cotizaciones/get_unidades_cotizacion',
		data: {
			id: idCot
		},
		statusCode: {
			404: function (data) {
				swal("Error!", "No Se encuentra el archivo", "error");
			},
			500: function () {
				swal("Error!", "500", "error");
			}
		},
		success: function (data) {
			var array = $.parseJSON(data);

			$('#tbody_units').find('tr').remove();

			if (array.length > 0) {
				array.forEach(function (element) {
					//IdUnidad, cantidadUnidad, costoUnidad
					get_table_unidad(element.id, element.cantidad, element.monto, element.cant_pasajeros, element.vehiculo);
				});
			}
		},
		error: function (response) {
			swal("Error", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
		}
	});

}

function get_table_unidad(idEach, cantidad, monto, cant_pasajeros, vehiculo) {
	var html = '<tr id="tr_unidades_each_' + idEach + '" class="tr_unidades">\
                    <td width="40%">\
                        <input type="hidden" id="id" value="' + idEach + '" readonly>\
                        <div class="form-group">\
                            <label class="col-form-label">Unidad</label>\
                            <div class="controls">\
                                <input type="text" class="form-control form-control-sm" value="' + vehiculo + '" readonly>\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="10%">\
                        <div class="form-group">\
                            <label class="col-form-label">Cantidad</label>\
                            <div class="controls">\
                                <input readonly type="number" class="form-control form-control-sm" min="0" id="cantidad" value="' + cantidad + '">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="25%">\
                        <div class="form-group">\
                            <label class="col-form-label">Monto unitario $</label>\
                            <div class="controls">\
                                <input type="text" class="form-control form-control-sm" id="monto" value="' + monto + '">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="25%" colspan="2">\
                        <div class="form-group">\
                            <label class="col-form-label">Cant. (pasajeros por unidad)</label>\
                            <div class="controls">\
                                <input type="number" class="form-control form-control-sm" min="0" id="cant_pasajeros" value="' + cant_pasajeros + '">\
                            </div>\
                        </div>\
                    </div>\
                    </td>\
                </tr>';

	$('#tbody_unidades').append(html);
}

//---------------------------------------------------->>>>>
function modal_contrato(id, contrato, cotizacion, idCot, cliente) {
	$('#modal_acciones').modal("hide");
	$('#modal_contrato').modal("show");
	$('#id_contrato').val(id);
	$('#id_prospecto').val(cliente);

	$('#tbody_unidades').find('tr.tr_unidades').remove();
	get_unidades_contrato(id, cotizacion, idCot);
	//console.log("CONTRATO " + contrato);

	if (contrato > 0) {
		//console.log("id");console.log(id);
		$.ajax({
			type: 'POST',
			url: base_url + 'Contratos/get_data_contrato',
			data: {
				id: id
			},
			statusCode: {
				404: function (data) {
					swal("Error!", "No Se encuentra el archivo", "error");
				},
				500: function () {
					swal("Error!", "500", "error");
				}
			},
			success: function (data) {
				//console.log('---data--- ' + data);
				const objeto = JSON.parse(data);
				$('#id_detalle').val(objeto.id);
				$('#calle').val(objeto.calle);
				$('#colonia').val(objeto.colonia);
				$('#cp').val(objeto.cp);
				$('#ciudad').val(objeto.ciudad_municipio);
				$('#estado').val(objeto.estado).trigger('change');
				$('#tipo_servicio').val(objeto.tipo_servicio);
				$('#fecha_contrato').val(objeto.fecha_contrato);
				$('#observaciones').val(objeto.observaciones);
			}
		});
	} else {
		$.ajax({
			type: 'POST',
			url: base_url + 'Contratos/get_fecha_contrato',
			data: {
				id: id
			},
			statusCode: {
				404: function (data) {
					swal("Error!", "No Se encuentra el archivo", "error");
				},
				500: function () {
					swal("Error!", "500", "error");
				}
			},
			success: function (data) {
				//console.log('---data--- ' + data);
				const datos = JSON.parse(data);
				$('#estado').val(datos.estado).trigger('change');

				if (datos.fecha_contrato != '0000-00-00') {
					$('#fecha_contrato').val(datos.fecha_contrato);
				}
			}
		});
	}
}

function add_form() {
	var form_register = $('#form_data');
	var error_register = $('.alert-danger', form_register);
	var success_register = $('.alert-success', form_register);
	var $validator1 = form_register.validate({
		errorElement: 'div', //default input error message container
		errorClass: 'vd_red', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules: {
			calle: {
				required: true
			},
			colonia: {
				required: true
			},
			cp: {
				required: true,
				digits: true,
				maxlength: 5
			},
			ciudad_municipio: {
				required: true
			},
			estado: {
				required: true
			},
			tipo_servicio: {
				required: true
			},
			num_pasajeros: {
				required: true,
				digits: true,
			},
			importe_unidad: {
				required: true,
				digits: true,
			},
			fecha_contrato: {
				required: true,
			}
		},
		errorPlacement: function (error, element) {
			if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
				element.parent().append(error);
			} else if (element.parent().hasClass("vd_input-wrapper")) {
				error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		},
		invalidHandler: function (event, validator) { //display error alert on form submit              
			success_register.fadeOut(500);
			error_register.fadeIn(500);
			scrollTo(form_register, -100);
		},
		highlight: function (element) { // hightlight error inputs
			$(element).addClass('vd_bd-red');
			$(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
		},
		unhighlight: function (element) { // revert the change dony by hightlight
			$(element)
				.closest('.control-group').removeClass('error'); // set error class to the control group
		},
		success: function (label, element) {
			label
				.addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
				.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
			$(element).removeClass('vd_bd-red');
		}
	});

	var $valid = $("#form_data").valid();
	if ($valid) {
		$('.btn_registro').attr('disabled', true);
		var datos = form_register.serialize();
		//console.log(datos);
		$.ajax({
			type: 'POST',
			url: base_url + 'Contratos/confirmar_contrato',
			data: datos,
			statusCode: {
				404: function (data) {
					swal("Error!", "No Se encuentra el archivo", "error");
				},
				500: function () {
					swal("Error!", "500", "error");
				}
			},
			success: function (data) {
				//console.log(data);
				addUnidades($('#id_contrato').val());
				addFechaContrato($('#id_contrato').val(), $('#fecha_contrato').val());

				swal("Éxito", "Guardado Correctamente", "success");
				$('#modal_contrato').modal('hide');
				loadtable();
			}
		});
	}
}

//--------------------------------------------------->
function modal_pagos(id, idCliente) {
	$('#modal_pagos').modal("show");
	$('#idContrato').val(id);
	$('#idcliente').val(idCliente);
	//console.log("id");console.log(id);

	$.ajax({
		type: 'POST',
		url: base_url + 'Contratos/get_data_pagos',
		data: {
			id: id,
			idCliente: idCliente
		},
		statusCode: {
			404: function (data) {
				swal("Error!", "No Se encuentra el archivo", "error");
			},
			500: function () {
				swal("Error!", "500", "error");
			}
		},
		success: function (data) {
			//console.log("data: "+data);
			const objeto = JSON.parse(data);

			$('#referencia').val(objeto.referencia);
			$('#cliente').val(objeto.cliente);
			$('#fecha').val(objeto.fecha);

			$('#descuento').val(parseFloat(objeto.descuento).toFixed(2));
			$('#anticipo').val(parseFloat(objeto.anticipo).toFixed(2));
			$('#monto_total').val(parseFloat(objeto.monto).toFixed(2));
			$('#pago_total').val(parseFloat(objeto.pago).toFixed(2));
			$('#restante').val(parseFloat(objeto.restante).toFixed(2));
			showPagado();

			var array = JSON.parse(JSON.stringify(objeto.pagos));
			//var array = $.parseJSON(objeto.pagos);

			$('#tbody_pagos').empty();

			if (array.length > 0) {
				array.forEach(function (element) {
					get_table_pagos(element.id, element.cuenta, element.referencia, element.fecha, element.monto, element.file_pago);
				});
			}
		}
	});
}

function exportPagos() {
	var id = $('#idContrato').val();
	window.open(base_url + "Contratos/exportPagos/" + id, '_blank');
}

function addPago() {
	var form_register = $('#form_pagos');
	var error_register = $('.alert-danger', form_register);
	var success_register = $('.alert-success', form_register);
	var $validator1 = form_register.validate({
		errorElement: 'div', //default input error message container
		errorClass: 'vd_red', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules: {
			cuenta: {
				required: true
			},
			referencia: {
				required: true
			},
			monto: {
				required: true
			},
			fecha: {
				required: true
			},
		},
		errorPlacement: function (error, element) {
			if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
				element.parent().append(error);
			} else if (element.parent().hasClass("vd_input-wrapper")) {
				error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		},
		invalidHandler: function (event, validator) { //display error alert on form submit              
			success_register.fadeOut(500);
			error_register.fadeIn(500);
			scrollTo(form_register, -100);
		},
		highlight: function (element) { // hightlight error inputs
			$(element).addClass('vd_bd-red');
			$(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
		},
		unhighlight: function (element) { // revert the change dony by hightlight
			$(element)
				.closest('.control-group').removeClass('error'); // set error class to the control group
		},
		success: function (label, element) {
			label
				.addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
				.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
			$(element).removeClass('vd_bd-red');
		}
	});
	var $valid = $("#form_pagos").valid();
	var restante = parseFloat($("#restante").val()).toFixed(2);
	var monto_pago = parseFloat($("#monto_pago").val()).toFixed(2);
	//console.log("restante: "+restante);
	//console.log("monto_pago: "+monto_pago);
	var idCon = $("#idContrato").val();
	var idCli = $("#idcliente").val();

	if ($valid) {
		if (Number(monto_pago) <= Number(restante)) {
			//console.log("monto_pago menor o = a restante: "+monto_pago);
			typeCuenta = $('#id_cuenta option:selected').text();
			auxCont = 0;
			saveBtnPress = true;
			var datos = form_register.serialize() + "&monto_total=" + $("#monto_total").val();


			$.ajax({
				type: 'POST',
				url: base_url + 'Contratos/insertPago',
				data: datos,
				statusCode: {
					404: function (data) {
						swal("Error!", "No Se encuentra el archivo", "error");
					},
					500: function () {
						swal("Error!", "500", "error");
					}
				},
				beforeSend: function () {
					$("#save_pay").attr("disabled", true);
				},
				success: function (data) {
					//console.log("--- data ---");
					//console.log(data.id_cuenta);

					$("#save_pay").attr("disabled", false);
					const response = JSON.parse(data);

					comprobanteId = parseInt(response.id);
					//console.log($("#comp_pago").val());

					if ($("#comp_pago").val() != "") {
						$('#comp_pago').fileinput('upload');
					} else {

						if (isGenerado != '') {
							isGenerado = '';
							//console.log('contrato: '+ $('#idContrato').val() );
							$.ajax({
								type: 'POST',
								url: base_url + 'Contratos/insertCompPago',
								data: {
									id: comprobanteId,
									idCont: $('#idContrato').val()
								},
								statusCode: {
									404: function (data) {
										swal("Error!", "No Se encuentra el archivo", "error");
									},
									500: function () {
										swal("Error!", "500", "error");
									}
								},
								success: function (data) {
									//console.log("data : "+data);
									get_table_pagos(response.id, response.cuenta, response.referencia, response.fecha, response.monto, data);
									swal("Éxito", "Guardado Correctamente", "success");
								}
							});
						} else {
							//console.log('ResponseTable');
							get_table_pagos(response.id, response.cuenta, response.referencia, response.fecha, response.monto, ' ');
							swal("Éxito", "Guardado Correctamente", "success");
						}


					}


					//get_table_pagos(response.id,response.cuenta,response.referencia,response.fecha,response.monto, response.file_pago);

					//swal("Éxito", "Guardado Correctamente", "success");
					//Limpar entradas del form
					$("#form_pagos").trigger("reset");
					$('#referencia_pago').val('');
					$('#fecha_pago').val('');
					$('#monto_pago').val('');

					$('#idContrato').val(idCon);
					$('#idcliente').val(idCli);

					$.ajax({
						type: 'POST',
						url: base_url + 'Contratos/get_data_pagos',
						data: {
							id: idCon,
							idCliente: idCli
						},
						statusCode: {
							404: function (data) {
								swal("Error!", "No Se encuentra el archivo", "error");
							},
							500: function () {
								swal("Error!", "500", "error");
							}
						},
						success: function (data) {
							//console.log("data 0 : "+data);
							const objeto = JSON.parse(data);

							$('#referencia').val(objeto.referencia);
							$('#cliente').val(objeto.cliente);
							$('#fecha').val(objeto.fecha);

							$('#descuento').val(parseFloat(objeto.descuento).toFixed(2));
							$('#anticipo').val(parseFloat(objeto.anticipo).toFixed(2));
							$('#monto_total').val(parseFloat(objeto.monto).toFixed(2));
							$('#pago_total').val(parseFloat(objeto.pago).toFixed(2));
							$('#restante').val(parseFloat(objeto.restante).toFixed(2));

							showPagado();
						}
					});

					//$('#pago_total').val( parseFloat( parseFloat($('#pago_total').val()) + parseFloat(response.monto) ).toFixed(2) );
					//$('#restante').val( parseFloat( parseFloat($('#monto_total').val()) - ( parseFloat($('#pago_total').val()) + parseFloat($('#anticipo').val()) + parseFloat($('#descuento').val()) )).toFixed(2) );
					//showPagado();
				}
			});
		} else {
			swal("Álerta!", "El pago no puede ser mayor al monto restante", "warning");
		}

	}

}

function get_table_pagos(id, cuenta, referencia, fecha, monto, file) {
	if (file == '') {
		//console.log("ID: " + id +' '+ file);
		file = ' ';
	}

	var html = '<tr id="tr_pago_' + id + '" style="width: 100%">\
                    <td width="20%">\
                        <label class="col-form-label" id="lcuenta">' + cuenta + '</label>\
                    </td>\
                    <td width="20%">\
                        <label class="col-form-label" id="lreferencia">' + referencia + '</label>\
                    </td>\
                    <td width="15%">\
                        <label class="col-form-label" id="lfecha">' + fecha + '</label>\
                    </td>\
                    <td width="20%">\
                        <label class="col-form-label" id="lmonto">' + parseFloat(monto).toFixed(2) + '</label>\
                    </td>\
                    <td width="15%">\
                        <button type="button" id="btn_' + id + '" class="btn btn-outline-primary" title="Agregar comprobante" data-file="' + file + '" onclick="addComprobante( this.getAttribute(\'data-file\'),' + id + ')"><i class="fa fa-file-image-o" aria-hidden="true"></i></button>\
                    </td>\
                    <td width="10%">\
                        <button type="button" class="btn btn-danger" title="Agregar nueva unidad" onclick="eliminarPago(' + id + ',' + monto + ')"><i id="btn_plus" class="fa fa-minus" aria-hidden="true"></i></button>\
                    </td> \
                </tr>';
	$('#tbody_pagos').append(html);
}


function eliminarPago(id, monto) {
	$.confirm({
		boxWidth: '30%',
		useBootstrap: false,
		icon: 'fa fa-warning',
		title: '¡Atención!',
		content: '¿Está seguro de eliminar este pago?',
		type: 'red',
		typeAnimated: true,
		buttons: {
			confirmar: function () {
				$.ajax({
					type: 'POST',
					url: base_url + "Contratos/delete",
					data: {
						id: id,
						tabla: "pagos_contrato"
					},
					statusCode: {
						404: function (data) {
							swal("Error!", "No Se encuentra el archivo", "error");
						},
						500: function () {
							swal("Error!", "500", "error");
						}
					},
					success: function (response) {
						$('#tr_pago_' + id).remove();
						$('#pago_total').val(parseFloat(parseFloat($('#pago_total').val()) - parseFloat(monto)).toFixed(2));
						$('#restante').val(parseFloat(parseFloat($('#monto_total').val()) - (parseFloat($('#pago_total').val()) + parseFloat($('#anticipo').val()) + parseFloat($('#descuento').val()))).toFixed(2));
						showPagado();
						swal("Éxito", "Se ha eliminado correctamente", "success");
					}
				});
			},
			cancelar: function () {

			}
		}
	});
}


function eliminarUnidad(id) {
	$.confirm({
		boxWidth: '30%',
		useBootstrap: false,
		icon: 'fa fa-warning',
		title: '¡Atención!',
		content: '¿Está seguro de eliminar esta unidad?',
		type: 'red',
		typeAnimated: true,
		buttons: {
			confirmar: function () {
				$('#tr_each_uni_' + id).remove();
				swal("Éxito", "Se ha eliminado correctamente", "success");
			},
			cancelar: function () {

			}
		}
	});
}

//---Chofer--------------------------------------
function modal_choferes(id, idCliente) {
	$('#modal_acciones').modal("hide");
	$('#modal_choferes').modal("show");
	$('#idContra').val(id);

	$.ajax({
		type: 'POST',
		url: base_url + 'Contratos/get_data_choferes',
		data: {
			id: id,
			idCliente: idCliente
		},
		statusCode: {
			404: function (data) {
				swal("Error!", "No Se encuentra el archivo", "error");
			},
			500: function () {
				swal("Error!", "500", "error");
			}
		},
		success: function (data) {
			//console.log('---data--- ' + data);
			const objeto = JSON.parse(data);
			$('#ref').val(objeto.referencia);
			$('#client').val(objeto.cliente);
			//showPagado();
			var array = JSON.parse(JSON.stringify(objeto.unidades));
			$('#tbody_choferes').empty();
			if (array.length > 0) {
				array.forEach(function (element) {
					get_table_choferes(element.id, element.vehiculo, element.cantidad, element.choferes, element.unidad);
				});
			}

		}
	});
}


function get_table_choferes(id, vehiculo, cantidad, choferes, unidad) {
	//var a_choferes = JSON.parse(JSON.stringify(choferes));
	var a_choferes = JSON.parse(JSON.stringify(choferes));
	var clonSelChofer = $("#Selchofer").clone();
	clonSelChofer.attr('id', 'sel_coferes_' + id);

	var html = '<tr id="tr_chofer_' + id + '" style="width: 100%">\
                    <td width="10%" >\
                        <input class="form-control" type="hidden" value="' + id + '">\
                        ' + cantidad + '\
                    </td>\
                    <td width="40%" >\
                        ' + vehiculo + '\
                    </td>\
                    <td width="40%" style="text-align:center">\
                    \
                        <select id="choferes_' + id + '">\
                        </select>\
                    \
                    </td>\
                    <td width="10%">\
                        <button type="button" class="btn btn-primary" title="Agregar chofer" onclick="addChofer(' + id + ',' + unidad + ')"><i class="fa fa-plus" aria-hidden="true"></i></button>\
                    </td>\
                </tr>\
                \
                <tr id="tr2_chofer_' + id + '" class="row_choferes" style="width: 100%">\
                    <td colspan="4" width="100%" class="p-0">\
                        <table width="100%">\
                            <tbody id="tbody_chofer_' + id + '">';

	if (a_choferes.length > 0) {
		a_choferes.forEach(function (element) {
			html += '<tr id="tr_2_chofer_' + element.id + '">\
                                    <td colspan="2" width="50%">\
                                        <input type="hidden" class="form-control" id="idChofCont" value="' + element.id + '">\
                                        <input class="form-control" type="hidden" id="idUnidPros" value="' + id + '">\
                                        <input class="form-control" type="hidden" id="idUnidad" value="' + unidad + '">\
                                    </td>\
                                    <td width="40%">\
                                        <input type="hidden" class="form-control" id="idChofer" value="' + element.idChofer + '" readonly>\
                                        <input type="text" class="form-control" value="' + element.nombre + ' ' + element.apellido_p + ' ' + element.apellido_m + '" readonly>\
                                    </td>\
                                    <td width="10%">\
                                        <button type="button" class="btn btn-primary" title="Eliminar chofer" onclick="eliminarChofer(' + element.id + ')"><i class="fa fa-minus" aria-hidden="true"></i></button>\
                                    </td>\
                                </tr>';
		});
	}

	html += '</tbody>\
                        </table>\
                    </td>\
                </tr>';


	$('#tbody_choferes').append(html);
	//$('#tr_chofer_'+id+' #choferes_'+element.id)
	$('#choferes_' + id).replaceWith(clonSelChofer);
}


function eliminarChofer(id) {
	$.confirm({
		boxWidth: '30%',
		useBootstrap: false,
		icon: 'fa fa-warning',
		title: '¡Atención!',
		content: '¿Está seguro de eliminar este chofer',
		type: 'red',
		typeAnimated: true,
		buttons: {
			confirmar: function () {
				$.ajax({
					type: 'POST',
					url: base_url + "Contratos/delete",
					data: {
						id: id,
						tabla: "chofer_contrato"
					},
					statusCode: {
						404: function (data) {
							swal("Error!", "No Se encuentra el archivo", "error");
						},
						500: function () {
							swal("Error!", "500", "error");
						}
					},
					success: function (response) {
						$('#tr_2_chofer_' + id).remove();
						swal("Éxito", "Se ha eliminado correctamente", "success");
					}
				});
			},
			cancelar: function () {

			}
		}
	});
}


function addChofer(id, unidad) {
	var index = $('#sel_coferes_' + id).find("option:selected").val();

	if (index == 0) {
		swal("Error", "Seleccione una opción", "error");
		return;
	}
	//Revision de choferes dobles
	let detener = false;
	$("#table_choferes tbody > tr.row_choferes tbody > tr ").each(function () {
		let idChofer = $(this).find("input[id*='idChofer']").val();

		if (idChofer == index) {
			swal("Error", "Seleccione una opción diferente", "error");
			detener = true;
			return;
		}
	});

	if (detener) {
		return; // Salir de la función
	}

	contCh++;
	var text = $('#sel_coferes_' + id).find("option:selected").text();

	/*-----------------------------------------------*/
	var html = '<tr id="tr_2a_chofer_' + contCh + '">\
                    <td colspan="2" width="50%">\
                        <input class="form-control" type="hidden" id="idChofCont" value="0">\
                        <input class="form-control" type="hidden" id="idUnidPros" value="' + id + '">\
                        <input class="form-control" type="hidden" id="idUnidad" value="' + unidad + '">\
                    </td>\
                    <td width="40%">\
                        <input type="hidden" class="form-control" id="idChofer" value="' + index + '" readonly>\
                        <input type="text" class="form-control" value="' + text + '" readonly>\
                    </td>\
                    <td width="10%">\
                        <button type="button" class="btn btn-primary" title="Eliminar chofer" onclick="eliminarAddCh(' + contCh + ')"><i class="fa fa-minus" aria-hidden="true"></i></button>\
                    </td>\
                </tr>';

	$('#tbody_chofer_' + id).append(html);
	$('#sel_coferes_' + id).val(0);
}


function eliminarAddCh(row_id) {
	contCh--;
	$('#tr_2a_chofer_' + row_id).remove();
}


function addChoferes() {
	var DATA = [];
	var TABLA = $("#table_choferes tbody > tr.row_choferes tbody > tr ");

	TABLA.each(function (index) {
		item = {};
		item["idChofCont"] = $(this).find("input[id*='idChofCont']").val();

		item["idChofer"] = $(this).find("input[id*='idChofer']").val();
		item["idContrato"] = $("#idContra").val();

		item["idUnidPros"] = $(this).find("input[id*='idUnidPros']").val();
		item["idUnidad"] = $(this).find("input[id*='idUnidad']").val();

		DATA.push(item);
	});

	INFO = new FormData();
	aInfo = JSON.stringify(DATA);
	INFO.append('data', aInfo);

	$.ajax({
		data: INFO,
		type: 'POST',
		url: base_url + 'Contratos/insertChofer',
		processData: false,
		contentType: false,
		statusCode: {
			404: function (data) {
				swal("Error!", "No Se encuentra el archivo", "error");
			},
			500: function () {
				swal("Error!", "500", "error");
			}
		},
		success: function (data) {
			//console.log(data);
			swal("Éxito", "Guardado Correctamente", "success");
			$('#modal_choferes').modal('hide');
		}
	});
}


//---Pasajeros--------------------------------------
function modal_pasajeros(id, idCliente) {
	$('#modal_acciones').modal("hide");
	$('#modal_pasajeros').modal("show");
	$('#id_contra').val(id);

	$.ajax({
		type: 'POST',
		url: base_url + 'Contratos/get_data_pasajeros',
		data: {
			id: id,
			idCliente: idCliente
		},
		statusCode: {
			404: function (data) {
				swal("Error!", "No Se encuentra el archivo", "error");
			},
			500: function () {
				swal("Error!", "500", "error");
			}
		},
		success: function (data) {
			//console.log('---data--- ' + data);
			const objeto = JSON.parse(data);
			$('#contrato_id').val(objeto.referencia);
			$('#client_id').val(objeto.cliente);

			//showPagado();

			var array = JSON.parse(JSON.stringify(objeto.unidades));

			$('#tbody_pasajeros').empty();

			if (array.length > 0) {
				array.forEach(function (element, index) {
					get_table_pasajeros(element.id, element.cant_pasajeros, element.vehiculo, element.cantidad, element.pasajeros, element.unidad);
				});
			}

		}
	});
}


function get_table_pasajeros(id, cantPasajeros, vehiculo, cantidad, pasajeros, unidad) {
	var a_pasajeros = JSON.parse(JSON.stringify(pasajeros));
	cantPasajeros = cantPasajeros * cantidad;

	var html = '<tr id="tr_pasajeros_' + id + '" style="width: 100%">\
                    <td width="10%" >\
                        <input class="form-control" type="hidden" value="' + id + '">\
                        ' + cantidad + '\
                    </td>\
                    <td width="40%" >\
                        ' + vehiculo + '\
                    </td>\
                    <td width="50%">\
                        <!--<button type="button" class="btn btn-primary" title="Agregar pasajeros"><i class="fa fa-eye"></i></button>-->\
                    </td>\
                </tr>\
                \
                <tr id="tr2_pasajeros_' + id + '" class="row_pasajeros" style="width: 100%" >\
                    <td colspan="4" width="100%" class="p-0">\
                        <table width="100%">\
                            <tbody id="tbody_pasajeros_' + id + '">';

	if (cantPasajeros > 0) {

		for (let i = 0; i < cantPasajeros; i++) {
			let idPasajeroContatro = 0;
			let pasajero = '';

			if (a_pasajeros.length > i) {
				if (a_pasajeros[i].pasajero != '') {
					pasajero = a_pasajeros[i].pasajero;
				}

				if (a_pasajeros[i].id != 0) {
					idPasajeroContatro = a_pasajeros[i].id;
				}
			}

			html += '<tr id="tr_2_pasajeros_' + i + '">\
                                    <td colspan="2" width="40%">\
                                        <input type="hidden" class="form-control" id="idPasajeroContrato" value="' + idPasajeroContatro + '">\
                                        <input class="form-control" type="hidden" id="idUnidPros" value="' + id + '">\
                                        <input class="form-control" type="hidden" id="idUnidad" value="' + unidad + '">\
                                    </td>\
                                    <td width="10%" >\
                                        # ' + (i + 1) + '\
                                    </td>\
                                    <td width="50%">\
                                        <input type="text" id="pasajero" class="form-control" value="' + pasajero + '">\
                                    </td>\
                                </tr>';
		}
	}

	html += '</tbody>\
                        </table>\
                    </td>\
                </tr>';


	$('#tbody_pasajeros').append(html);
}


function addPasajeros() {
	var DATA = [];
	var TABLA = $("#table_pasajeros tbody > tr.row_pasajeros tbody > tr ");

	TABLA.each(function (index) {
		item = {};
		item["idPasajeroContrato"] = $(this).find("input[id*='idPasajeroContrato']").val();

		//item["idChofer"] = $(this).find("input[id*='idChofer']").val();
		item["pasajero"] = $(this).find("input[id*='pasajero']").val();
		item["idContrato"] = $("#id_contra").val();

		item["idUnidPros"] = $(this).find("input[id*='idUnidPros']").val();
		item["idUnidad"] = $(this).find("input[id*='idUnidad']").val();

		if ($(this).find("input[id*='pasajero']").val() != '') {
			DATA.push(item);
		}

	});

	INFO = new FormData();
	aInfo = JSON.stringify(DATA);
	INFO.append('data', aInfo);

	$.ajax({
		data: INFO,
		type: 'POST',
		url: base_url + 'Contratos/insertPasajero',
		processData: false,
		contentType: false,
		statusCode: {
			404: function (data) {
				swal("Error!", "No Se encuentra el archivo", "error");
			},
			500: function () {
				swal("Error!", "500", "error");
			}
		},
		success: function (data) {
			//console.log(data);
			swal("Éxito", "Guardado Correctamente", "success");
			$('#modal_pasajeros').modal('hide');
		}
	});
}

function send_pdf(idContrato, idCliente) {
	$.confirm({
		boxWidth: '40%',
		useBootstrap: false,
		icon: 'fa fa-info',
		title: '¡Atención!',
		content: '¿Está seguro de mandar este Email?',
		type: 'green',
		typeAnimated: true,
		buttons: {
			confirmar: function () {
				$.ajax({
					type: 'POST',
					url: base_url + "Contratos/contratoMail",
					data: {
						id: idContrato
					},
					statusCode: {
						404: function (data) {
							swal("Error!", "No Se encuentra el archivo", "error");
						},
						500: function () {
							swal("Error!", "500", "error");
						}
					},
					success: function (response) {
						$.ajax({
							type: "POST",
							url: base_url + "Contratos/sendMailPDF",
							data: {
								idContrato: idContrato,
								idCliente: idCliente,
							},
							success: function (result) {
								//console.log(result);
								swal("Éxito", "Se ha enviado correctamente", "success");
							}
						});
					}
				});
			},
			cancelar: function () {

			}
		}
	});
}


function send_WhatsApp(id, num_tel) {
	var url_pdf = base_url + "WhatsApp/contrato/" + id;
	window.open("https://wa.me/" + num_tel + "?text=" + url_pdf + "");
}


function modal_acciones(id, contrato, cotizacion, id_cotizacion, idCliente, telefono, seguimiento, rel_gastos, bit_viaje) {
	$('#modal_acciones').modal("show");

	if (contrato == 0) {
		var html = '<a class="btn btn-flat btn-primary mt-2" onclick="modal_contrato(' + id + ',' + contrato + ',' + cotizacion + ',' + id_cotizacion + ',' + idCliente + ')"><i class=""></i>Datos de Contrato</a> ';
	} else {
		if (perfilID == 7) {
			//var html = '<a class="btn btn-flat btn-primary mt-2" target="_blank" title="Generar orden de servicio" href="'+base_url+'Contratos/orden_servicio/'+id+'"><i class="fa fa-file icon_font" style="font-size: 20px;"></i></a> ';
			var html = '<a class="btn btn-flat btn-primary mt-2" title="Generar orden de servicio" onclick="modal_selUnidad(' + id + ',\'OS\')"><i class="fa fa-file icon_font" style="font-size: 20px;"></i></a> ';

			//html += '<a class="btn btn-flat btn-primary mt-2" target="_blank" title="Relación de gastos" href="'+base_url+'Contratos/form_relacion_gastos/'+id+'"><i class="fa fa-dollar" style="font-size: 20px;"></i></a> ';
			html += '<a class="btn btn-flat btn-primary mt-2" title="Relación de gastos" onclick="modal_selUnidad(' + id + ',\'RGF\')"><i class="fa fa-dollar" style="font-size: 20px;"></i></a> ';

			if (rel_gastos != 0) {
				//html += '<a class="btn btn-flat btn-primary mt-2" target="_blank" title="Generar relación gastos" href="'+base_url+'Contratos/relacion_gastos/'+id+'"><i class="fa fa-file-o" style="font-size: 20px;"></i></a> ';
				html += '<a class="btn btn-flat btn-primary mt-2" target="_blank" title="Generar relación gastos" onclick="modal_selUnidad(' + id + ',\'RG\')"><i class="fa fa-file-o" style="font-size: 20px;"></i></a> ';
			}

			//html+= '<a class="btn btn-flat btn-primary mt-2" target="_blank" title="Bitácora de viaje" href="'+base_url+'Contratos/form_bitacora_viaje/'+id+'"><i class="fa fa-clipboard" style="font-size: 20px;"></i></a> ';
			html += '<a class="btn btn-flat btn-primary mt-2" target="_blank" title="Bitácora de viaje" onclick="modal_selUnidad(' + id + ',\'BVF\')"><i class="fa fa-clipboard" style="font-size: 20px;"></i></a> ';
			if (bit_viaje != 0) {
				//html+= '<a class="btn btn-flat btn-primary mt-2" target="_blank" title="Generar bitácora de viaje" onclick="modal_selUnidad('+id+',\'BV\')"><i class="fa fa-files-o" style="font-size: 20px;"></i></a> ';
				html += '<a class="btn btn-flat btn-primary mt-2" target="_blank" title="Generar bitácora de viaje" onclick="modal_selUnidad(' + id + ',\'BV\')"><i class="fa fa-files-o" style="font-size: 20px;"></i></a> ';
			}

		} else {
			var html = '<a class="btn btn-flat btn-primary mt-2" title="Expediente de Cliente" onclick="expediente(' + id + ',' + idCliente + ')"><i class="fa fa-user"></i><i class="fa fa-files-o"></i> </a> ';
			html += '<a class="btn btn-flat btn-primary mt-2" onclick="modal_contrato(' + id + ',' + contrato + ',' + cotizacion + ',' + id_cotizacion + ',' + idCliente + ')"><i class=""></i>Editar</a> ';
			html += '<a class="btn btn-flat btn-primary mt-2" target="_blank" title="Generar contrato" href="' + base_url + 'Contratos/contrato/' + id + '"><i class="fa fa-file-pdf-o icon_font" style="font-size: 20px;"></i></a> ';
			if (seguimiento == 4 || seguimiento == 7) {
				html += '<a class="btn btn-flat btn-primary mt-2" title="Enviar por e-mail PDF" onclick="send_pdf(' + id + ',' + idCliente + ')"><i class="fa fa-envelope-o icon_font" style="font-size: 19px;"></i></a> ';
				html += '<a class="btn btn-flat btn-primary mt-2" title="Enviar whatsapp con link de PDF" onclick="send_WhatsApp(' + id + ',' + telefono + ')"><i class="fa fa-whatsapp icon_font" style="font-size: 19px;"></i></a> ';
			}
			html += '<a class="btn btn-flat btn-primary mt-2" title="Asignar choferes" onclick="modal_choferes(' + id + ',' + idCliente + ')"><i class="fa fa-truck"></i> </a> ';

			//html+= '<a class="btn btn-flat btn-primary mt-2" target="_blank" title="Generar orden de servicio" href="'+base_url+'Contratos/orden_servicio/'+id+'"><i class="fa fa-file icon_font" style="font-size: 20px;"></i></a> ';
			html += '<a class="btn btn-flat btn-primary mt-2" title="Generar orden de servicio" onclick="modal_selUnidad(' + id + ',\'OS\')"><i class="fa fa-file icon_font" style="font-size: 20px;"></i></a> ';

			html += '<a class="btn btn-flat btn-primary mt-2" title="Asignar pasajeros" onclick="modal_pasajeros(' + id + ',' + idCliente + ')"><i class="fa fa-users"></i> </a> ';
			html += '<a class="btn btn-flat btn-primary mt-2" target="_blank" title="Generar reporte pasajeros" href="' + base_url + 'Contratos/reportePasajeros/' + id + '"><i class="fa fa-file-text-o icon_font" style="font-size: 20px;"></i></a> ';

			//html += '<a class="btn btn-flat btn-primary mt-2" target="_blank" title="Relación de gastos" href="'+base_url+'Contratos/form_relacion_gastos/'+id+'"><i class="fa fa-dollar" style="font-size: 20px;"></i></a> ';
			html += '<a class="btn btn-flat btn-primary mt-2" title="Relación de gastos" onclick="modal_selUnidad(' + id + ',\'RGF\')"><i class="fa fa-dollar" style="font-size: 20px;"></i></a> ';
			if (rel_gastos != 0) {
				//html+= '<a class="btn btn-flat btn-primary mt-2" target="_blank" title="Generar relación gastos" href="'+base_url+'Contratos/relacion_gastos/'+id+'"><i class="fa fa-file-o" style="font-size: 20px;"></i></a> ';
				html += '<a class="btn btn-flat btn-primary mt-2" target="_blank" title="Generar relación gastos" onclick="modal_selUnidad(' + id + ',\'RG\')"><i class="fa fa-file-o" style="font-size: 20px;"></i></a> ';
			}

			//html+= '<a class="btn btn-flat btn-primary mt-2" target="_blank" title="Bitácora de viaje" href="'+base_url+'Contratos/form_bitacora_viaje/'+id+'"><i class="fa fa-clipboard" style="font-size: 20px;"></i></a> ';
			html += '<a class="btn btn-flat btn-primary mt-2" target="_blank" title="Bitácora de viaje" onclick="modal_selUnidad(' + id + ',\'BVF\')"><i class="fa fa-clipboard" style="font-size: 20px;"></i></a> ';
			if (bit_viaje != 0) {
				//html+= '<a class="btn btn-flat btn-primary mt-2" target="_blank" title="Generar bitácora de viaje" href="'+base_url+'Contratos/bitacora_viaje/'+id+'"><i class="fa fa-files-o" style="font-size: 20px;"></i></a> ';
				html += '<a class="btn btn-flat btn-primary mt-2" target="_blank" title="Generar bitácora de viaje" onclick="modal_selUnidad(' + id + ',\'BV\')"><i class="fa fa-files-o" style="font-size: 20px;"></i></a> ';
			}

			html += '<a class="btn btn-flat btn-primary mt-2" target="_blank" title="Estadistícas de Contrato" href="' + base_url + 'Contratos/estadisticasView/' + id + '"><i class="fa fa-bar-chart" style="font-size: 20px;"></i></a> ';
			html += '<a class="btn btn-flat btn-primary mt-2" title="Carga de Contrato" onclick="addContrato(' + id + ')"><i class="fa fa-upload" style="font-size: 20px;"></i></a> ';
		}

	}

	$('#btns').empty();
	$('#btns').append(html);
}

function expediente(id, idCliente) {
	$('#modal_acciones').modal("hide");
	$('#modal_expediente').modal("show");

	$.ajax({
		type: 'POST',
		url: base_url + 'Contratos/get_exepedienteCli',
		data: {
			id: id,
			idCliente: idCliente
		},
		success: function (data) {
			var arr = $.parseJSON(data);
			$('#ine').val(arr.ine);
			//$('#ine_img').attr("src",base_url+"uploads/clientes/"+arr.identifica);
			//$('#comp_img').attr("src",base_url+"uploads/clientes/"+arr.comprobante);

			var identifica = arr.identifica;
			var comprobante = arr.comprobante;
			//console.log("identifica: "+identifica); 
			var img_file = "";
			var imgdet = "";
			var typePDF = "";
			if (arr.identifica != "") {
				img_file = '' + base_url + "uploads/clientes/" + identifica + '';
				ext = identifica.split('.');
				if (ext[1] != "pdf") {
					imgdet = {
						type: "image",
						caption: identifica,
						key: 1
					}
					typePDF = "false";
				} else {
					imgdet = {
						type: "pdf",
						caption: identifica,
						key: 1
					};
					typePDF = "true";
				}
			}

			$("#identifica").fileinput({
				showCaption: false,
				showUpload: false,
				elErrorContainer: '#kv-avatar-errors-1',
				msgErrorClass: 'alert alert-block alert-danger',
				initialPreview: [
					'' + img_file + '',
				],
				initialPreviewAsData: typePDF,
				initialPreviewFileType: 'image', // image is the default and can be overridden in config below
				initialPreviewConfig: [
					imgdet
				],
			});

			var img_file2 = "";
			var imgdet2 = "";
			var typePDF2 = "";
			if (comprobante != "") {
				img_file2 = '' + base_url + "uploads/clientes/" + comprobante + '';
				ext2 = comprobante.split('.');
				if (ext2[1] != "pdf") {
					imgdet2 = {
						type: "image",
						caption: comprobante,
						key: 2
					}
					typePDF2 = "false";
				} else {
					imgdet2 = {
						type: "pdf",
						caption: comprobante,
						key: 2
					};
					typePDF2 = "true";
				}
			}
			$("#comprobante").fileinput({
				showCaption: false,
				showUpload: false,
				elErrorContainer: '#kv-avatar-errors-1',
				msgErrorClass: 'alert alert-block alert-danger',
				initialPreview: [
					'' + img_file2 + '',
				],
				initialPreviewAsData: typePDF2,
				initialPreviewFileType: 'image', // image is the default and can be overridden in config below
				initialPreviewConfig: [
					imgdet2
				],

			});

			$(".kv-file-remove").hide();
		}
	});
}

function addComprobante(file = '0', id = '0') {
	var edit = 0;
	if (file != '0') {
		//console.log('Edicion');
		edit = id;
		$('#comp_pago_aux').val(file);
		$('#id_pago_aux').val(id);

		$('#btnG_hidden').show();
		$('#btnA_hidden').hide();
	} else {
		//console.log('Creacion');
		edit = 0;
		$('#comp_pago_aux').val('');
		$('#id_pago_aux').val('');

		$('#btnG_hidden').hide();
		$('#btnA_hidden').show();
	}
	$('#comp_pago').fileinput('destroy');
	auxRow = id;
	loadFilePago();
	$('#modal_comprobante').modal("show");
	$('#edit').val(edit);
}


function save_comprobante() {
	comprobanteId = parseInt($('#id_pago_aux').val());
	edicion = true;

	if ($("#comp_pago").val() != "" && contratoId != 0) {
		$('#comp_pago').fileinput('upload');
	} else {
		if (isGenerado != '') {
			//console.log('Generado');
			isGenerado = '';

			$.ajax({
				type: 'POST',
				url: base_url + 'Contratos/insertCompPago',
				data: {
					id: comprobanteId,
					idCont: $('#idContrato').val()
				},
				statusCode: {
					404: function (data) {
						swal("Error!", "No Se encuentra el archivo", "error");
					},
					500: function () {
						swal("Error!", "500", "error");
					}
				},
				success: function (data) {
					//console.log("data : "+data);
					//get_table_pagos(response.id,response.cuenta,response.referencia,response.fecha,response.monto, data);

					$('#tr_pago_' + auxRow + ' #btn_' + auxRow).attr('data-file', data);
					auxRow = 0;

					swal("Éxito", "Guardado Correctamente", "success");

					setTimeout(function () {
						$('#modal_comprobante').modal("hide");
					}, 500);
				}
			});
		}
	}
}


function addContrato(id = 0) {
	$('#modal_acciones').modal("hide");
	if (id != '0') {
		$('#id_contrato_arch').val(id);
	}

	$('#contenedor_contratos').empty();

	$.ajax({
		type: "POST",
		url: base_url + "Contratos/get_contratos_archivados",
		data: {
			idContrato: id
		},
		success: function (result) {
			//console.log("result: " + result );
			//console.log("result: " + result );

			const resultArray = JSON.parse(result);

			resultArray.forEach(function (item, index) {
				//console.log(item);
				htmlC = '<div class="mb-3 row">\
                            <label class="col-sm-12 col-form-label">' + item.file_contrato + '</label>\
                            <hr>\
                            <input class="form-control" type="hidden" id="id_contrato_aux_' + index + '" value="' + item.id + '">\
                            <input class="form-control" type="hidden" id="comp_contrato_aux_' + index + '" value="' + item.file_contrato + '">\
                            <input class="form-control" type="file" name="file" id="comp_contrato_' + index + '">\
                        </div>';



				$('#contenedor_contratos').append(htmlC);

				loadFileContratoArchivado(index);

			});

		}
	});

	$('#modal_carga_contrato').modal("show");
}

function save_contrato() {
	contratoId = parseInt($('#id_contrato_arch').val());

	if ($("#comp_contrato").val() != "" && contratoId != 0) {
		$('#comp_contrato').fileinput('upload');
	}
}

function generarCompPago() {
	var editRow = $("#edit").val();
	var idCP = $("#idContrato").val();

	//$('#idcliente').val();
	var cliente = $('#idcliente').val() != '' ? $('#idcliente').val() : 0;

	if (editRow > 0) {
		var cuenta = $('#tr_pago_' + editRow + ' #lcuenta').text();
		var tipoCuenta = cuenta == "EFECTIVO" ? 1 : 0;
		var referencia = $('#tr_pago_' + editRow + ' #lreferencia').text();
		var fecha = $('#tr_pago_' + editRow + ' #lfecha').text();
		var monto = $('#tr_pago_' + editRow + ' #lmonto').text();

	} else {
		var cuenta = $('#id_cuenta option:selected').text();
		var tipoCuenta = $('#id_cuenta option:selected').val();
		console.log(tipoCuenta);
		var referencia = $('#referencia_pago').val() != '' ? $('#referencia_pago').val() : 0;
		var fecha = $('#fecha_pago').val() != '' ? $('#fecha_pago').val() : '0000-00-00';
		var monto = $('#monto_pago').val() != '' ? $('#monto_pago').val() : 0;
	}
	//-----------
	var restante = parseFloat($("#restante").val()).toFixed(2);
	var monto_pago = parseFloat($("#monto_pago").val()).toFixed(2);

	if (referencia == 0 || fecha == '0000-00-00' || monto == 0) {
		swal("Error", "Los campos del pago no pueden estar vacios!", "warning");
		return;
	}

	if (Number(monto_pago) >= Number(restante)) {
		swal("Álerta!", "El pago no puede ser mayor al monto restante", "warning");
		return;
	}

	window.open(base_url + 'Contratos/generarCompPago/' + idCP + '/' + cuenta.trim() + '/' + tipoCuenta + '/' + referencia + '/' + fecha + '/' + monto + '/' + cliente, 'Comprobante Pago', 'width=600,height=400');

	setTimeout(function () {
		//$('#comp_contrato').click();
		//public/pdf/Comprobante_pago_


		$('#comp_pago').fileinput('destroy');
		autocargaComp('Comprobante_pago_' + idCP);

	}, 1000);
}

function autocargaComp(compPDF) {
	var img_file_p = "";
	var imgdet_p = "";
	var typePDF_p = "";

	if (compPDF != '') {
		img_file_p = base_url + 'public/pdf/' + compPDF + '.pdf';
		isGenerado = img_file_p;
		//console.log("img_file_p:  " + img_file_p);
		ext = 'pdf';
		if (ext != "pdf") {
			imgdet_p = {
				type: "image",
				url: "" + base_url + "Contratos/delete_comprobante_pago/" + $("#id_pago_aux").val(),
				caption: $("#comp_pago_aux").val()
			}
			typePDF_p = "false";
		} else {
			imgdet_p = {
				type: "pdf",
				url: base_url + "Contratos/delete_comprobante_pago/" + $("#id_pago_aux").val(),
				caption: $("#comp_pago_aux").val()
			};
			typePDF_p = "true";
		}
	}

	$('#comp_pago').fileinput({
		showCaption: false, // Ocultar titulo
		showUpload: false, // Ocultar el botón "Upload File"

		allowedFileExtensions: ["png", "jpg", "jpeg", "bmp", "pdf"],
		browseLabel: 'Seleccionar comprobante de pago',
		uploadUrl: base_url + 'Contratos/insert_comprobante_pago',
		maxFilePreviewSize: 5000,
		maxFileSize: 5000,
		elErrorContainer: '#kv-avatar-errors-1',
		msgErrorClass: 'alert alert-block alert-danger',
		initialPreview: [
			'' + img_file_p + '',
		],
		initialPreviewAsData: typePDF_p,
		initialPreviewFileType: 'image', // image is the default and can be overridden in config below
		initialPreviewConfig: [
			imgdet_p
		],
		previewFileIconSettings: {
			'png': '<i class="fa fa-file-photo-o text-warning"></i>',
			'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
			'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
			'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
			'pdf': '<i class="fa fa-pdf-o text-warning"></i>'
		},
		uploadExtraData: function (previewId, index) {
			var info = {
				id_comprobante: comprobanteId,
			};
			return info;
		}
	}).on('fileuploaded', function (event, files, extra) {
		//location.reload();
		//console.log('UPLOADEDfile_2');
		//$('#comp_pago_aux').val();
		swal("Éxito", "Guardado Correctamente", "success");

		setTimeout(function () {
			$("#modal_comprobante").modal("hide");
			$('#comp_pago').fileinput('clear');
			//$('#comp_pago_aux').val();
			//console.log($('#idContrato').val()+', '+$('#idcliente').val());
			//modal_pagos($('#idContrato').val(),$('#idcliente').val());

			if (auxCont == 0) {
				auxCont++;
				//console.log('AjaxTable');

				$.ajax({
					type: 'POST',
					url: base_url + 'Contratos/get_data_1_pago',
					data: {
						id: comprobanteId
					},
					statusCode: {
						404: function (data) {
							swal("Error!", "No Se encuentra el archivo", "error");
						},
						500: function () {
							swal("Error!", "500", "error");
						}
					},
					success: function (data) {
						const objeto = JSON.parse(data);
						//console.log("DATOS-> " + data);

						if (Object.keys(objeto).length > 0) {

							if (!edicion) {
								get_table_pagos(objeto.id, typeCuenta, objeto.referencia, objeto.fecha, objeto.monto, objeto.file_pago);
							} else {
								//$('#tr_pago_' + auxRow +' #btn_'+ auxRow).addClass('bg-danger');
								$('#tr_pago_' + auxRow + ' #btn_' + auxRow).attr('data-file', objeto.file_pago);
								auxRow = 0;
							}

						}

					}
				});
			}

		}, 1000);

	}).on('filedeleted', function (event, files, extra) {
		//location.reload();
		//console.log('deletedfile');
		$('#tr_pago_' + auxRow + ' #btn_' + auxRow).attr('data-file', ' ');
		auxRow = 0;
		isGenerado = '';
	}).on('fileuploaderror', function (event, files, extra) {
		//location.reload();
		//console.log('File up error');

		if (!saveBtnPress) {
			swal("Álerta", "Peso máximo (4MB.) superado! ", "warning");
			$('#comp_pago').fileinput('clear');
		} else {
			swal("Álerta", "Pago guardado correctamente, Reintente subir comprobante", "warning");
			saveBtnPress = false;
			if (auxCont == 0) {
				auxCont++;

				$.ajax({
					type: 'POST',
					url: base_url + 'Contratos/get_data_1_pago',
					data: {
						id: comprobanteId
					},
					statusCode: {
						404: function (data) {
							swal("Error!", "No Se encuentra el archivo", "error");
						},
						500: function () {
							swal("Error!", "500", "error");
						}
					},
					success: function (data) {
						const objeto = JSON.parse(data);
						//console.log("DATOS-> " + data);

						if (Object.keys(objeto).length > 0) {
							get_table_pagos(objeto.id, typeCuenta, objeto.referencia, objeto.fecha, objeto.monto, ' ');
						}

					}
				});
			}

		}
	});
}


function enviar_encuesta(idContrato, idCliente, correo) {
	$.confirm({
		boxWidth: '40%',
		useBootstrap: false,
		icon: 'fa fa-info',
		title: '¡Atención!',
		content: '¿Está seguro de mandar a este Email:  " ' + correo + '" ?',
		type: 'green',
		typeAnimated: true,
		buttons: {
			confirmar: function () {
				$.ajax({
					type: "POST",
					url: base_url + "Contratos/get_token",
					data: {
						idContrato: idContrato,
						idCliente: idCliente,
					},
					success: function (result) {
						console.log(result);
						$.ajax({
							type: "POST",
							url: base_url + "Contratos/enviar_encuesta",
							data: {
								idContrato: idContrato,
								idCliente: idCliente,
							},
							success: function (result) {
								console.log(result);
								swal("Éxito", "Se ha enviado correctamente", "success");
							}
						});
					}
				});
			},
			cancelar: function () {

			}
		}
	});
}

function enviar_whatsapp(idContrato, idCliente, telefono) {
	$.confirm({
		boxWidth: '40%',
		useBootstrap: false,
		icon: 'fa fa-info',
		title: '¡Atención!',
		content: '¿Está seguro de mandar a este Whatsapp: " ' + telefono + '" ?',
		type: 'green',
		typeAnimated: true,
		buttons: {
			confirmar: function () {
				$.ajax({
					type: "POST",
					url: base_url + "Contratos/send_encuesta_whatsapp",
					data: {
						idContrato: idContrato,
						idCliente: idCliente,
					},
					success: function (result) {
						//console.log(result);
						phoneNumber = telefono.replace(/\s+/g, '');

						var message = 'Hola, el grupo de Gʀᴀɴᴅ Tᴜʀɪsᴍᴏ Exᴘʀᴇss le pide por favor contestar esta encuesta de satisfacción.\n' +
							result + '\n' +
							'Saludos.';

						var url = 'https://wa.me/' + phoneNumber + '?text=' + encodeURIComponent(message);
						//console.log("url: " + url);
						window.open(url, '_blank');

					}
				});
			},
			cancelar: function () {

			}
		}
	});
}



//---------------------------->

function modal_selUnidad(id, type = "") {
	$('#modal_acciones').modal("hide");
	$('#modal_selUnidad').modal("show");
	$('#idContModUni').val(id);
	$('#typeDoc').val(type);

	$.ajax({
		type: 'POST',
		url: base_url + 'Contratos/get_data_unidad_contrato',
		data: {
			id: id
		},
		statusCode: {
			404: function (data) {
				swal("Error!", "No Se encuentra el archivo", "error");
			},
			500: function () {
				swal("Error!", "500", "error");
			}
		},
		success: function (data) {
			//console.log('---data--- ' + data);
			const objeto = JSON.parse(data);
			var dis = "";

			$('#cont').val(objeto.idContrato);
			$('#fol').val(objeto.folio);
			$('#clin').val(objeto.cliente);
			$('#existRG').val(objeto.existRG);
			$('#existBV').val(objeto.existBV);


			var array = JSON.parse(JSON.stringify(objeto.unidades));
			$('#select_container').empty();

			var sel = '\
            <select class="form-control form-control-smr" id="selUnidad" >\
                <option value="0" selected="" disabled>Seleccionar una opción</option>';


			if (array.length > 1) {
				dis = "";

				array.forEach(function (element) {
					if (type == "RG") {
						if (element.rel_gastos == 0)
							dis = "disabled";
						else
							dis = "";

					} else if (type == "BV") {
						if (element.bit_viaje == 0)
							dis = "disabled";
						else
							dis = "";
					}

					sel += '<option value="' + element.unidad + '" ' + dis + '>ECO. ' + element.num_eco + ' - ' + element.vehiculo + '</option>';
				});
			} else if (array.length == 1) {

				if (objeto.existRG > 0 || objeto.existRG > 0) {

					dis = "";

					array.forEach(function (element) {
						if (type == "RG") {
							if (element.rel_gastos == 0)
								dis = "disabled";
							else
								dis = "";

						} else if (type == "BV") {
							if (element.bit_viaje == 0)
								dis = "disabled";
							else
								dis = "";
						}

						sel += '<option value="' + element.unidad + '" ' + dis + '>ECO. ' + element.num_eco + ' - ' + element.vehiculo + '</option>';
					});

				} else {
					array.forEach(function (element) {
						if (type == "RG") {
							if (element.rel_gastos == 0)
								dis = "disabled";
							else
								dis = "";

						} else if (type == "BV") {
							if (element.bit_viaje == 0)
								dis = "disabled";
							else
								dis = "";
						}

						//console.log("dis: " + dis);
						//console.log("type" + type);

						if ((type != "RG" || type != "BV") && dis == "") {
                            sel += '<option selected value="' + element.unidad + '" ' + dis + '>ECO. ' + element.num_eco + ' - ' + element.vehiculo + '</option>';
                            select1Unidad(type, id, element.unidad);
						}else{
                            sel += '<option value="' + element.unidad + '" ' + dis + '>ECO. ' + element.num_eco + ' - ' + element.vehiculo + '</option>';
                        }

					});
				}

			}

			sel += '</select>';

			$('#select_container').html(sel);

		}
	});
}


function selectUnidad() {
	var cont = $('#idContModUni').val();
	var type = $('#typeDoc').val();
	var existRG = $('#existRG').val();
	var existBV = $('#existBV').val();
	var uni = $('#selUnidad').val();
	var cantUnis = $('#selUnidad option').length;
	var url = base_url + 'Contratos';

	if (uni == null) {
		swal("Error", "Seleccione unidad válida", "error");
		return;
	}

	switch (type) {
		case 'OS':
			url = base_url + 'Contratos/orden_servicio/' + cont + '/' + uni;
			break;

		case 'RGF':
			url = base_url + 'Contratos/form_relacion_gastos/' + cont + '/' + uni;
			break;

		case 'RG':
			url = base_url + 'Contratos/relacion_gastos/' + cont + '/' + uni;
			break;

		case 'BVF':
			url = base_url + 'Contratos/form_bitacora_viaje/' + cont + '/' + uni;
			break;

		case 'BV':
			url = base_url + 'Contratos/bitacora_viaje/' + cont + '/' + uni;
			break;

		default:
			break;
	}


	if (((type == 'RGF') && (Number(existRG) > 0)) || ((type == 'BVF')) && (Number(existBV) > 0)) {
		$.confirm({
			boxWidth: '30%',
			useBootstrap: false,
			icon: 'fa fa-warning',
			title: '¡Atención!',
			content: 'Existen datos asignados al contrato, pero no a ninguna unidad.<br>' +
				'¿Desea asignarlos a la unidad seleccionada?<br>' +
				'<strong>Si decide descartarlos, se eliminarán.</strong>',
			type: 'red',
			typeAnimated: true,
			buttons: {
				Cancelar: function () {
					console.log('Cancelado');
				},
				Descartar: function () {
					$.ajax({
						type: 'POST',
						url: base_url + "Contratos/setExist",
						data: {
							idC: cont,
							type: type,
						},
						success: function (response) {

							$('#modal_selUnidad').modal("hide");
							swal("Éxito", "Se han descartado correctamente", "success");

							setTimeout(function () {
								window.open(url, '_blank');
							}, 500);

						}
					});
				},
				Confirmar: function () {
					$.ajax({
						type: 'POST',
						url: base_url + "Contratos/setUnidad",
						data: {
							idC: cont,
							idU: uni,
							type: type,
						},
						success: function (response) {
							$('#modal_selUnidad').modal("hide");
							swal("Éxito", "Se han asignado correctamente", "success");

							setTimeout(function () {
								window.open(url, '_blank');
							}, 500);

						}
					});

				}
			}
		});
	} else if (type == 'OS') {
		if (cantUnis > 2) {
			$.confirm({
				boxWidth: '30%',
				useBootstrap: false,
				icon: 'fa fa-warning',
				title: '¡Atención!',
				content: '¿Esta unidad tendrá <bold>"IMPORTE POR COBRAR"</bold>?',
				type: 'orange',
				typeAnimated: true,
				buttons: {
					Cerrar: function () {
						console.log('Cancelado');
					},
					NO: function () {
						url = url + '/0';
						window.open(url, '_blank');
					},
					SI: function () {
						url = url + '/1';
						window.open(url, '_blank');
					}
				}
			});
		} else {
			url = url + '/1';
			window.open(url, '_blank');
		}

	} else {
		window.open(url, '_blank');
	}

}


function select1Unidad(type, contrato, unidad) {
	var url = base_url + 'Contratos';

	if (unidad == null) {
		return;
	}

	switch (type) {
		case 'OS':
			url = base_url + 'Contratos/orden_servicio/' + contrato + '/' + unidad;
			break;

		case 'RGF':
			url = base_url + 'Contratos/form_relacion_gastos/' + contrato + '/' + unidad;
			break;

		case 'RG':
			url = base_url + 'Contratos/relacion_gastos/' + contrato + '/' + unidad;
			break;

		case 'BVF':
			url = base_url + 'Contratos/form_bitacora_viaje/' + contrato + '/' + unidad;
			break;

		case 'BV':
			url = base_url + 'Contratos/bitacora_viaje/' + contrato + '/' + unidad;
			break;

		default:
			break;
	}

	window.open(url, '_blank');
}
