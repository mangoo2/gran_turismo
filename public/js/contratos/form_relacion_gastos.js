var base_url = $('#base_url').val();

var contCaseta = 0;
var contCombustible = 0;
var contOtro = 0;
var contSueldo = 0;

var idCont = $('#idContrato').val();
var idUnidad = $('#idUnidad').val();

$(document).ready(function() {
    get_tabla_casetas(idCont);
    get_tabla_combustibles(idCont);
    get_tabla_otros(idCont);
    get_tabla_sueldos(idCont);
});


function add_form(){
	var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            cant_recibida: {
                required: true,
                number: true
            },
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        invalidHandler: function(event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);
        },
        highlight: function(element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function(element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function(label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });

    var $valid = $("#form_data").valid();
    if ($valid) {
        $('.btn_registro').attr('disabled',true);

        id = $('#idObs').val();
        observaciones = $('#observaciones').val();
        cant_recibida = $('#cant_recibida').val();
        
        $.ajax({
            type:'POST',
            url: base_url + 'Contratos/insertObservaciones',
            data: {observaciones:observaciones, id:id, idContrato:idCont, idUnidad:idUnidad, cant_recibida:cant_recibida},
            success: function(data) {
                //console.log(data);
                
                //$("#idContrato").val(data);
                addCasetas(idCont);
                addCombustibles(idCont);
                addOtros(idCont);
                addSueldos(idCont);
                
                swal("Éxito", "Guardado Correctamente", "success");
                
                setTimeout(function(){ 
                    window.location = base_url+'Contratos';
                }, 1500);
                
            },
            error: function(response){
            swal("Error", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
            }
        });
    }
}



//---Caseta-------------------------------------->
function get_tabla_casetas(id){
	$.ajax({
        type:'POST',
		url: base_url+"Contratos/get_table_casetas",
		data: {
            id : id,
            idU : idUnidad,
        },
		success: function (response){
			var array = $.parseJSON(response);
            
            if(array.length > 0) {
                array.forEach(function(element){
                    tabla_casetas(element.id, element.fecha, element.descripcion, element.importe);
                });
            }
		},
		error: function(response){
            swal("Error", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}


function tabla_casetas(idEach, fecha, descripcion, importe){
	//console.log("----- Genera Row Caseta Edicion "+idEach+" -----");

    var html = '<tr id="tr_casetas_each_'+idEach+'" class="tr_casetas">\
                    <td width="20%">\
                        <input type="hidden" id="id" value="'+idEach+'">\
                        <input type="hidden" value="tr_casetas_'+idEach+'" readonly>\
                        <div class="form-group">\
                            <label class="col-form-label">Fecha</label>\
                            <div class="controls">\
                                <input type="date" id="fecha" class="form-control form-control-sm" value="'+fecha+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="50%">\
                        <div class="form-group">\
                            <label class="col-form-label">Descripción</label>\
                            <div class="controls">\
                                <input type="text" id="descripcion" class="form-control form-control-sm" value="'+descripcion+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="20%">\
                        <div class="form-group">\
                            <label class="col-form-label">Importe $</label>\
                            <div class="controls">\
                                <input type="text" id="importe" class="form-control form-control-sm" min="0" value="'+importe+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="10%">\
                        <span class="required" style="color: transparent;">dele</span>\
                        <a class="btn btn-danger" title="Eliminar Caseta" onclick="delCaseta('+idEach+')"><i class="fa fa-minus" aria-hidden="true"></i></a>\
                    </td>\
                </tr>';

    $('#tbody_casetas').append(html);
}


function addCaseta(){
    contCaseta++;

    var html = '<tr id="tr_casetas_'+contCaseta+'" class="tr_casetas">\
                    <td width="20%">\
                        <input type="hidden" id="id" value="0">\
                        <input type="hidden" value="tr_casetas_'+contCaseta+'" readonly>\
                        <div class="form-group">\
                            <label class="col-form-label">Fecha</label>\
                            <div class="controls">\
                                <input type="date" id="fecha" class="form-control form-control-sm" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="50%">\
                        <div class="form-group">\
                            <label class="col-form-label">Descripción</label>\
                            <div class="controls">\
                                <input type="text" id="descripcion" class="form-control form-control-sm" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="20%">\
                        <div class="form-group">\
                            <label class="col-form-label">Importe $</label>\
                            <div class="controls">\
                                <input type="text" id="importe" class="form-control form-control-sm" min="0" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="10%">\
                        <span style="color: transparent;">add</span>\
                        <button type="button" class="btn btn-danger" title="Eliminar caseta" onclick="eliminarAddCaseta('+contCaseta+')"><i class="fa fa-minus" aria-hidden="true"></i></button>\
                    </td>\
                </tr>';

    $('#tbody_casetas').append(html);
}


function eliminarAddCaseta(row_id) {
    $('#tr_casetas_' + row_id).remove();
}


function delCaseta(id) {
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar esta Caseta?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Contratos/delete",
                    data: {
                        id:id, tabla:"contrato_gastos"
                    },
                    success:function(response){
                        $('#tr_casetas_each_' + id).remove();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}


function addCasetas(id){
    var TABLA = $("#table_casetas tbody > tr");
    var DATA = [];

    TABLA.each(function() {
        item = {};

        if($(this).hasClass("tr_casetas")) {
            item["id_contrato"] = id;
            item["id_unidad"] = idUnidad;
            //item["id_prospecto"] = $('#idCliente').val();
            item["id"] = $(this).find("input[id='id']").val();
            item["tipo"] = '1';
            item["fecha"] = $(this).find("input[id='fecha']").val();
            item["descripcion"] = $(this).find("input[id='descripcion']").val();
            item["importe"] = $(this).find("input[id='importe']").val();


            if ($(this).find("input[id='fecha']").val() != "") {
                DATA.push(item);
            }

        }
    });
    
    INFO = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    //console.log('CASETAS: '+aInfo);

    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url + 'Contratos/insertGastos',
        processData: false,
        contentType: false,
        success: function(data) {
            //console.log(data);
        }
    });
}
//---Caseta--------------------------------------<


//---Combustible-------------------------------------->
function get_tabla_combustibles(id){
	$.ajax({
        type:'POST',
		url: base_url+"Contratos/get_table_combustibles",
		data: {
            id : id,
            idU : idUnidad,
        },
		success: function (response){
			var array = $.parseJSON(response);
            
            if(array.length > 0) {
                array.forEach(function(element){
                    tabla_combustibles(element.id, element.fecha, element.descripcion, element.importe);
                });
            }
		},
		error: function(response){
            swal("Error", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}


function tabla_combustibles(idEach, fecha, descripcion, importe){
	//console.log("----- Genera Row Combustible Edicion "+idEach+" -----");

    var html = '<tr id="tr_combustibles_each_'+idEach+'" class="tr_combustibles">\
                    <td width="20%">\
                        <input type="hidden" id="id" value="'+idEach+'">\
                        <input type="hidden" value="tr_combustibles_'+idEach+'" readonly>\
                        <div class="form-group">\
                            <label class="col-form-label">Fecha</label>\
                            <div class="controls">\
                                <input type="date" id="fecha" class="form-control form-control-sm" value="'+fecha+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="50%">\
                        <div class="form-group">\
                            <label class="col-form-label">Descripción</label>\
                            <div class="controls">\
                                <input type="text" id="descripcion" class="form-control form-control-sm" value="'+descripcion+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="20%">\
                        <div class="form-group">\
                            <label class="col-form-label">Importe $</label>\
                            <div class="controls">\
                                <input type="text" id="importe" class="form-control form-control-sm" min="0" value="'+importe+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="10%">\
                        <span class="required" style="color: transparent;">dele</span>\
                        <a class="btn btn-danger" title="Eliminar Combustible" onclick="delCombustible('+idEach+')"><i class="fa fa-minus" aria-hidden="true"></i></a>\
                    </td>\
                </tr>';

    $('#tbody_combustibles').append(html);
}


function addCombustible(){
    contCombustible++;

    var html = '<tr id="tr_combustibles_'+contCombustible+'" class="tr_combustibles">\
                    <td width="20%">\
                        <input type="hidden" id="id" value="0">\
                        <input type="hidden" value="tr_combustibles_'+contCombustible+'" readonly>\
                        <div class="form-group">\
                            <label class="col-form-label">Fecha</label>\
                            <div class="controls">\
                                <input type="date" id="fecha" class="form-control form-control-sm" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="50%">\
                        <div class="form-group">\
                            <label class="col-form-label">Descripción</label>\
                            <div class="controls">\
                                <input type="text" id="descripcion" class="form-control form-control-sm" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="20%">\
                        <div class="form-group">\
                            <label class="col-form-label">Importe $</label>\
                            <div class="controls">\
                                <input type="text" id="importe" class="form-control form-control-sm" min="0" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="10%">\
                        <span style="color: transparent;">add</span>\
                        <button type="button" class="btn btn-danger" title="Eliminar combustible" onclick="eliminarAddCombustible('+contCombustible+')"><i class="fa fa-minus" aria-hidden="true"></i></button>\
                    </td>\
                </tr>';

    $('#tbody_combustibles').append(html);
}


function eliminarAddCombustible(row_id) {
    $('#tr_combustibles_' + row_id).remove();
}


function delCombustible(id) {
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar esta Combustible?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Contratos/delete",
                    data: {
                        id:id, tabla:"contrato_gastos"
                    },
                    success:function(response){
                        $('#tr_combustibles_each_' + id).remove();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}



function addCombustibles(id){
    var TABLA = $("#table_combustibles tbody > tr");
    var DATA = [];

    TABLA.each(function() {
        item = {};

        if($(this).hasClass("tr_combustibles")) {
            item["id_contrato"] = id;
            item["id_unidad"] = idUnidad;
            //item["id_prospecto"] = $('#idCliente').val();
            item["id"] = $(this).find("input[id='id']").val();
            item["tipo"] = '2';
            item["fecha"] = $(this).find("input[id='fecha']").val();
            item["descripcion"] = $(this).find("input[id='descripcion']").val();
            item["importe"] = $(this).find("input[id='importe']").val();


            if ($(this).find("input[id='fecha']").val() != "") {
                DATA.push(item);
            }

        }
    });
    
    INFO = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    //console.log('COMBUSTIBLES: '+aInfo);

    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url + 'Contratos/insertGastos',
        processData: false,
        contentType: false,
        success: function(data) {
            //console.log(data);
        }
    });
}
//---Combustible--------------------------------------<


//---Otro-------------------------------------->
function get_tabla_otros(id){
	$.ajax({
        type:'POST',
		url: base_url+"Contratos/get_table_otros",
		data: {
            id : id,
            idU : idUnidad,
        },
		success: function (response){
			var array = $.parseJSON(response);
            
            if(array.length > 0) {
                array.forEach(function(element){
                    tabla_otros(element.id, element.fecha, element.descripcion, element.importe);
                });
            }
		},
		error: function(response){
            swal("Error", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}


function tabla_otros(idEach, fecha, descripcion, importe){
	//console.log("----- Genera Row Otro Edicion "+idEach+" -----");

    var html = '<tr id="tr_otros_each_'+idEach+'" class="tr_otros">\
                    <td width="20%">\
                        <input type="hidden" id="id" value="'+idEach+'">\
                        <input type="hidden" value="tr_otros_'+idEach+'" readonly>\
                        <div class="form-group">\
                            <label class="col-form-label">Fecha</label>\
                            <div class="controls">\
                                <input type="date" id="fecha" class="form-control form-control-sm" value="'+fecha+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="50%">\
                        <div class="form-group">\
                            <label class="col-form-label">Descripción</label>\
                            <div class="controls">\
                                <input type="text" id="descripcion" class="form-control form-control-sm" value="'+descripcion+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="20%">\
                        <div class="form-group">\
                            <label class="col-form-label">Importe $</label>\
                            <div class="controls">\
                                <input type="text" id="importe" class="form-control form-control-sm" min="0" value="'+importe+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="10%">\
                        <span class="required" style="color: transparent;">dele</span>\
                        <a class="btn btn-danger" title="Eliminar Otro" onclick="delOtro('+idEach+')"><i class="fa fa-minus" aria-hidden="true"></i></a>\
                    </td>\
                </tr>';

    $('#tbody_otros').append(html);
}


function addOtro(){
    contOtro++;

    var html = '<tr id="tr_otros_'+contOtro+'" class="tr_otros">\
                    <td width="20%">\
                        <input type="hidden" id="id" value="0">\
                        <input type="hidden" value="tr_otros_'+contOtro+'" readonly>\
                        <div class="form-group">\
                            <label class="col-form-label">Fecha</label>\
                            <div class="controls">\
                                <input type="date" id="fecha" class="form-control form-control-sm" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="50%">\
                        <div class="form-group">\
                            <label class="col-form-label">Descripción</label>\
                            <div class="controls">\
                                <input type="text" id="descripcion" class="form-control form-control-sm" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="20%">\
                        <div class="form-group">\
                            <label class="col-form-label">Importe $</label>\
                            <div class="controls">\
                                <input type="text" id="importe" class="form-control form-control-sm" min="0" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="10%">\
                        <span style="color: transparent;">add</span>\
                        <button type="button" class="btn btn-danger" title="Eliminar otro" onclick="eliminarAddOtro('+contOtro+')"><i class="fa fa-minus" aria-hidden="true"></i></button>\
                    </td>\
                </tr>';

    $('#tbody_otros').append(html);
}


function eliminarAddOtro(row_id) {
    $('#tr_otros_' + row_id).remove();
}


function delOtro(id) {
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Contratos/delete",
                    data: {
                        id:id, tabla:"contrato_gastos"
                    },
                    success:function(response){
                        $('#tr_otros_each_' + id).remove();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}


function addOtros(id){
    var TABLA = $("#table_otros tbody > tr");
    var DATA = [];

    TABLA.each(function() {
        item = {};

        if($(this).hasClass("tr_otros")) {
            item["id_contrato"] = id;
            item["id_unidad"] = idUnidad;
            //item["id_prospecto"] = $('#idCliente').val();
            item["id"] = $(this).find("input[id='id']").val();
            item["tipo"] = '3';
            item["fecha"] = $(this).find("input[id='fecha']").val();
            item["descripcion"] = $(this).find("input[id='descripcion']").val();
            item["importe"] = $(this).find("input[id='importe']").val();


            if ($(this).find("input[id='fecha']").val() != "") {
                DATA.push(item);
            }

        }
    });
    
    INFO = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    //console.log('OTROS: '+aInfo);

    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url + 'Contratos/insertGastos',
        processData: false,
        contentType: false,
        success: function(data) {
            //console.log(data);
        }
    });
}
//---Otro--------------------------------------<


//---Sueldo-------------------------------------->
function get_tabla_sueldos(id){
	$.ajax({
        type:'POST',
		url: base_url+"Contratos/get_table_sueldos",
		data: {
            id : id,
            idU : idUnidad,
        },
		success: function (response){
			var array = $.parseJSON(response);
            
            if(array.length > 0) {
                array.forEach(function(element){
                    tabla_sueldos(element.id, element.fecha, element.descripcion, element.importe);
                });
            }
		},
		error: function(response){
            swal("Error", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}


function tabla_sueldos(idEach, fecha, descripcion, importe){
	//console.log("----- Genera Row Sueldo Edicion "+idEach+" -----");

    var html = '<tr id="tr_sueldos_each_'+idEach+'" class="tr_sueldos">\
                    <td width="20%">\
                        <input type="hidden" id="id" value="'+idEach+'">\
                        <input type="hidden" value="tr_sueldos_'+idEach+'" readonly>\
                        <div class="form-group">\
                            <label class="col-form-label">Fecha</label>\
                            <div class="controls">\
                                <input type="date" id="fecha" class="form-control form-control-sm" value="'+fecha+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="50%">\
                        <div class="form-group">\
                            <label class="col-form-label">Descripción</label>\
                            <div class="controls">\
                                <input type="text" id="descripcion" class="form-control form-control-sm" value="'+descripcion+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="20%">\
                        <div class="form-group">\
                            <label class="col-form-label">Importe $</label>\
                            <div class="controls">\
                                <input type="text" id="importe" class="form-control form-control-sm" min="0" value="'+importe+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="10%">\
                        <span class="required" style="color: transparent;">dele</span>\
                        <a class="btn btn-danger" title="Eliminar Sueldo" onclick="delSueldo('+idEach+')"><i class="fa fa-minus" aria-hidden="true"></i></a>\
                    </td>\
                </tr>';

    $('#tbody_sueldos').append(html);
}


function addSueldo(){
    contSueldo++;

    var html = '<tr id="tr_sueldos_'+contSueldo+'" class="tr_sueldos">\
                    <td width="20%">\
                        <input type="hidden" id="id" value="0">\
                        <input type="hidden" value="tr_sueldos_'+contSueldo+'" readonly>\
                        <div class="form-group">\
                            <label class="col-form-label">Fecha</label>\
                            <div class="controls">\
                                <input type="date" id="fecha" class="form-control form-control-sm" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="50%">\
                        <div class="form-group">\
                            <label class="col-form-label">Descripción</label>\
                            <div class="controls">\
                                <input type="text" id="descripcion" class="form-control form-control-sm" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="20%">\
                        <div class="form-group">\
                            <label class="col-form-label">Importe $</label>\
                            <div class="controls">\
                                <input type="text" id="importe" class="form-control form-control-sm" min="0" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="10%">\
                        <span style="color: transparent;">add</span>\
                        <button type="button" class="btn btn-danger" title="Eliminar sueldo" onclick="eliminarAddSueldo('+contSueldo+')"><i class="fa fa-minus" aria-hidden="true"></i></button>\
                    </td>\
                </tr>';

    $('#tbody_sueldos').append(html);
}


function eliminarAddSueldo(row_id) {
    $('#tr_sueldos_' + row_id).remove();
}


function delSueldo(id) {
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Contratos/delete",
                    data: {
                        id:id, tabla:"contrato_gastos"
                    },
                    success:function(response){
                        $('#tr_sueldos_each_' + id).remove();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}


function addSueldos(id){
    var TABLA = $("#table_sueldos tbody > tr");
    var DATA = [];

    TABLA.each(function() {
        item = {};

        if($(this).hasClass("tr_sueldos")) {
            item["id_contrato"] = id;
            item["id_unidad"] = idUnidad;
            //item["id_prospecto"] = $('#idCliente').val();
            item["id"] = $(this).find("input[id='id']").val();
            item["tipo"] = '4';
            item["fecha"] = $(this).find("input[id='fecha']").val();
            item["descripcion"] = $(this).find("input[id='descripcion']").val();
            item["importe"] = $(this).find("input[id='importe']").val();


            if ($(this).find("input[id='fecha']").val() != "") {
                DATA.push(item);
            }

        }
    });
    
    INFO = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    //console.log('OTROS: '+aInfo);

    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url + 'Contratos/insertGastos',
        processData: false,
        contentType: false,
        success: function(data) {
            //console.log(data);
        }
    });
}
//---Sueldo--------------------------------------<
