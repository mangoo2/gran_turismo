var base_url = $('#base_url').val();

var contDestinoCot = 0;
var contUnidadCot = 0;

var idCont = $('#idContrato').val();
var firstElement = true;

$(document).ready(function() {
    $('#estado').select2({width: '100%'});
    $('#personal').select2({width: '100%'});

    $('#idCliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un cliente',
        ajax: {
            url: base_url + 'Contratos/search_cliente',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemsCli = [];
                data.forEach(function(element) {
                    //console.log("element->"+element);
                    itemsCli.push({
                        id: element.id,
                        text: element.nombre +" "+element.app +" "+element.apm,
                        estado: element.estado,
                        /*
                        origen: element.lugar_origen,
                        f_salida: element.fecha_salida,
                        h_salida: element.hora_salida,
                        f_regreso: element.fecha_regreso,
                        h_regreso: element.hora_regreso
                        */
                    });
                });
                return {
                    results: itemsCli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        //getDataItinerario(data.id);
        $('#estado').val(data.estado).trigger('change');
        
        /*
        $('#origen').val(data.origen);
        $('#f_salida').val(data.f_salida);
        $('#h_salida').val(data.h_salida);
        $('#f_regreso').val(data.f_regreso);
        $('#h_regreso').val(data.h_regreso);
        */
    });
    get_tabla_unidades(idCont);

    if(idCont > 0){
        getDates(idCont);
    }
});


function get_tabla_unidades(id){
	$.ajax({
        type:'POST',
		url: base_url+"Contratos/get_table_unidades",
		data: {id : id},
		success: function (response){
			var array = $.parseJSON(response);
            
            if(array.length > 0) {
                array.forEach(function(element){
                    tabla_unidades(element.id, element.unidad, element.cantidad, element.monto, element.destinos, firstElement, false);
                    firstElement = false;
                });
            }
		},
		error: function(response){
            swal("Error", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}


function tabla_unidades(idEach, unidad, cantidad, monto, destinos, firstElement, prospecto){
	//console.log("----- Genera Row D Edicion "+idEach+" -----");
    var a_destinos = JSON.parse(JSON.stringify(destinos));
    var clonSelect = $("#sel_unidad").clone();
    clonSelect.attr('id', 'unidad_d_'+idEach);

    var htmlT ='<tr id="tr_unidades_each_sub">\
                    <td width="100%" colspan="5">\
                        <hr>\
                        <h5>Unidades Agregadas</h5>\
                    </td>\
                </tr>';

    var html = '<tr id="tr_unidades_each_'+idEach+'" class="tr_unidades">\
                    <td width="40%">\
                        <input type="hidden" id="id" value="'+idEach+'">\
                        <input type="hidden" value="tr_unidades_'+idEach+'" readonly>\
                        <div class="form-group">\
                            <label class="col-form-label">Unidad</label>\
                            <div class="controls">\
                                <select class="form-control form-control-smr" id="unidades">\
                                </select>\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="25%">\
                        <div class="form-group">\
                            <label class="col-form-label">Cantidad</label>\
                            <div class="controls">\
                                <input readonly type="number" id="cantidad" class="form-control form-control-sm" min="0" value="'+cantidad+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="25%">\
                        <div class="form-group">\
                            <label class="col-form-label">Monto unitario $</label>\
                            <div class="controls">\
                                <input type="text" id="monto" class="form-control form-control-sm" value="'+monto+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="10%" colspan="2">\
                        <span class="required" style="color: transparent;">dele</span>\
                        <a class="btn btn-danger" title="Eliminar Unidad" onclick="delUnidadCotiza('+idEach+','+prospecto+')"><i class="fa fa-minus" aria-hidden="true"></i></a>\
                    </td>\
                </tr>\
                <!---destinos-->\
                <tr id="tr_unidades_each_space_'+idEach+'"></tr>\
                <tr id="tr_unidades_each_destinos_'+idEach+'" class="tr_destinos">\
                \
                <td colspan="4">\
                    <table id="table_destinos" width="100%">\
                    <tbody id="tbody_unidad_each_destinos_'+idEach+'">\
                        <tr id="tr_destinos" class="tr_destinos original">\
                            <td width="5%">\
                                \
                                <input type="hidden" value="tbody_uni_dest_ea_des_'+idEach+'" readonly>\
                                <input type="hidden" value="tr_destinos" readonly>\
                                \
                            </td>\
                            \
                            <td width="80%">\
                                <input type="hidden" id="id" value="0">\
                                <div class="form-group">\
                                    <label class="col-form-label">Itinerario</label>\
                                    <div class="controls">\
                                        <!--<input type="text" id="lugar" class="form-control form-control-smr" value="">-->\
                                        <textarea type="text" id="lugar" class="form-control" rows="2"></textarea>\
                                    </div>\
                                </div>\
                            </td>\
                            \
                            <!--\
                            <td width="15%">\
                                <div class="form-group">\
                                <label class="col-form-label">Fecha Salida</label>\
                                <div class="controls">\
                                    <input type="date" id="fecha" class="form-control form-control-sm" value="">\
                                </div>\
                                </div>\
                            </td>\
                            \
                            <td width="15%">\
                                <div class="form-group">\
                                    <label class="col-form-label">Hora Salida</label>\
                                    <div class="controls">\
                                        <input type="text" style="width:100%" id="hora" value="00:00">\
                                    </div>\
                                </div>\
                            </td>\
                            \
                            <td width="15%">\
                                <div class="form-group">\
                                <label class="col-form-label">Fecha Regreso</label>\
                                <div class="controls">\
                                    <input type="date" id="fecha_regreso" class="form-control form-control-sm" value="">\
                                </div>\
                                </div>\
                            </td>\
                            \
                            <td width="15%">\
                                <div class="form-group">\
                                <label class="col-form-label">Hora Regreso</label>\
                                <div class="controls">\
                                    <input type="text" style="width:100%" id="hora_regreso" value="00:00">\
                                </div>\
                                </div>\
                            </td>-->\
                            \
                            <!--<td width="15%">\
                                <div class="form-group">\
                                <label class="col-form-label">Lugar de hospedaje</label>\
                                <div class="controls">\
                                    <input type="text" id="lugar_hospeda" class="form-control form-control-sm" value="">\
                                </div>\
                                </div>\
                            </td>-->\
                            \
                            <td width="10%">\
                                <span style="color: transparent;">add</span>\
                                <button type="button" class="btn btn-primary" title="Agregar nuevo destino" onclick="addDestinoCotiza('+idEach+','+1+')"><i id="btn_plus" class="fa fa-plus" aria-hidden="true"></i></button>\
                            </td>\
                            \
                            <td width="5%"></td>\
                        </tr>\
                    </tbody>\
                </table>\
            </td>\
        </tr>';

    if(firstElement){
        $('#tbody_unidades').append(htmlT);
    }

    $('#tbody_unidades').append(html);

    $('#tr_unidades_each_'+idEach+' #unidades').replaceWith(clonSelect);
    $('#tr_unidades_each_'+idEach+' #unidad_d_'+idEach).select2({width: '100%'});
    $('#tr_unidades_each_'+idEach+' #unidad_d_'+idEach).val(unidad).trigger('change');

    $('#tbody_unidad_each_destinos_'+idEach+' #hora').timepicker({closeText:'Cerrar',okText:'Aceptar',hour24:true});
    $('#tbody_unidad_each_destinos_'+idEach+' #hora_regreso').timepicker({closeText:'Cerrar',okText:'Aceptar',hour24:true});


    if(a_destinos.length > 0) {
        a_destinos.forEach(function(element){
            tabla_destinos(idEach, element.id, element.lugar, element.fecha, element.hora, element.fecha_regreso, element.hora_regreso, element.lugar_hospeda, prospecto);
        });
    }
}


function  tabla_destinos(idRow, id_each, lugar_destino, fecha, hora, fecha_regreso, hora_regreso, lugar_hospeda, prospecto){
	//console.log("----- Genera Row U Edicion "+id_each+" -----");
    var title = '';
    if(fecha != "0000-00-00" || fecha_regreso != "0000-00-00"){
        title = ' title="Salida: '+fecha+'  '+hora+'  |  Regreso: '+fecha_regreso+'  '+hora_regreso+'" ';
    }

    var html = '<tr id="tr_destinos_each_'+id_each+'" class="tr_destinos">\
                    <td width="5%">\
                    \
                    <input type="hidden" value="tr_destinos_each_'+id_each+'" readonly>\
                    \
                    </td>\
                    <td width="80%">\
                        <input type="hidden" id="id" value='+id_each+'">\
                        <div class="form-group">\
                            <label class="col-form-label">Itinerario</label>\
                            <div class="controls">\
                                <!--<input type="text" id="lugar" class="form-control form-control-smr" value="'+lugar_destino+'">-->\
                                <textarea type="text" id="lugar" class="form-control" rows="2" '+title+' >'+lugar_destino+'</textarea>\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <!--\
                    <td width="15%">\
                        <div class="form-group">\
                            <label class="col-form-label">Fecha Salida</label>\
                            <div class="controls">\
                                <input type="date" id="fecha" class="form-control form-control-sm" value="'+fecha+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="15%">\
                        <div class="form-group">\
                            <label class="col-form-label">Hora Salida</label>\
                            <div class="controls">\
                                <input type="text" style="width:100%" id="hora" value="'+hora+'" >\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="15%">\
                        <div class="form-group">\
                            <label class="col-form-label">Fecha Regreso</label>\
                            <div class="controls">\
                                <input type="date" id="fecha_regreso" class="form-control form-control-sm" value="'+fecha_regreso+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="15%">\
                        <div class="form-group">\
                            <label class="col-form-label">Hora Regreso</label>\
                            <div class="controls">\
                                <input type="text" style="width:100%" id="hora_regreso" value="'+hora_regreso+'" >\
                            </div>\
                        </div>\
                    </td>-->\
                    \
                    <!--<td width="15%">\
                        <div class="form-group">\
                            <label class="col-form-label">Lugar de hospedaje</label>\
                            <div class="controls">\
                                <input type="text" id="lugar_hospeda" class="form-control form-control-sm" value="'+lugar_hospeda+'">\
                            </div>\
                        </div>\
                    </td>-->\
                    \
                    <td width="10%">\
                        <span class="required" style="color: transparent;">dele</span>\
                        <a class="btn btn-danger" title="Eliminar destino" onclick="delDestinoCotiza('+id_each+','+prospecto+')"><i class="fa fa-minus" aria-hidden="true"></i></a>\
                    </td>\
                    <td width="5%"></td>\
                </tr>';

    $('#tbody_unidades  #tbody_unidad_each_destinos_'+idRow).append(html);

    $('#tr_destinos_each_'+id_each+' #hora').timepicker({closeText:'Cerrar',okText:'Aceptar',hour24:true});
    $('#tr_destinos_each_'+id_each+' #hora_regreso').timepicker({closeText:'Cerrar',okText:'Aceptar',hour24:true});
}


function add_form(){
	var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            idCliente:{
                required: true
            },
            folio:{
                required: true
            },
            fecha_salida:{
                required: true
            },
            hora_salida: {
                required: true
            },
            fecha_regreso: {
                required: true
            },
            hora_regreso: {
                required: true
            },
            fecha_contrato: {
                required: true
            },
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        invalidHandler: function(event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);
        },
        highlight: function(element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function(element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function(label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form_data").valid();
    var $repeated = false;


    $('#tbody_unidades').find('#table_destinos .tr_destinos').each(function () {
        if(!$(this).hasClass("original")){
            fechaS = $(this).find('#f_salida').val();
            fechaR = $(this).find('#f_regreso').val();
            /*
            fechaS = $(this).find('#fecha').val();
            horaS = $(this).find('#hora').val();
            fechaR = $(this).find('#fecha_regreso').val();
            horaR = $(this).find('#hora_regreso').val();
            */

            /*
            if(fechaS == '' || horaS == '' || fechaR == '' || horaR == ''){
                swal("Error!", "Fechas u Horas vacias", "error");
                $repeated = true;
                return;
            }
            */

            if(fechaS > fechaR ){
                swal("Error!", "Fecha de salida no puede ser mayor a fecha de regreso", "error");
                $repeated = true;
                return;
            }
        }
    });


    if ($valid && !$repeated) {
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type: 'POST',
            url: base_url+'Contratos/guardar',
            data: datos,
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
                $("#idContrato").val(data);
                addUnidades(data);
                
                swal("Éxito", "Guardado Correctamente", "success");
                
                setTimeout(function(){ 
                    window.location = base_url+'Contratos';
                }, 1500);
            }
        });           
    }
}


function addUnidades(id){
    var capacidad = [' ',4,6,10,14,20,33,47,50];
    var DATA = [];

    var TABLAU = $("#table_unidades tbody > tr");//.tr_destinos
    var i = 0;
    var insert = false;

    TABLAU.each(function() {
        item = {};

        if($(this).hasClass("tr_unidades")) {
            item["id_contrato"] = id;
            item["id_prospecto"] = $('#idCliente').val();
            item["id"] = $(this).find("input[id*='id']").val();
            item["unidad"] = $(this).find("select[id*='unidad'] option:selected").val();
            let tipo = $(this).find("select[id*='unidad'] option:selected").data('tipo');
            item["cantidad"] = $(this).find("input[id*='cantidad']").val();
            item["monto"] = $(this).find("input[id*='monto']").val();
            if(tipo == '' || tipo > capacidad.length){
                tipo = 0;
            }
            item["cant_pasajeros"] = capacidad[tipo];
            item["DESTINOS"] = [];

            if ($(this).find("select[id*='unidad'] option:selected").val() != "0") {
                DATA.push(item);
                insert = true;
                i++;
            }

        }else if($(this).hasClass("tr_destinos")) {

            if(!insert){
                insert = false;
                return;
            }

            var TABLAD = $(this).find("#table_destinos tbody > tr");//.tr_unidades
            TABLAD.each(function() {
                itemD = {};
                itemD["id"] = $(this).find("input[id*='id']").val();
                //itemD["lugar"] = $(this).find("input[id='lugar']").val();
                itemD["lugar"] = $(this).find("textarea[id='lugar']").val();
                itemD["fecha"] = $(this).find("input[id='fecha']").val();
                itemD["hora"] = $(this).find("input[id='hora']").val();
                itemD["fecha_regreso"] = $(this).find("input[id='fecha_regreso']").val();
                itemD["hora_regreso"] = $(this).find("input[id='hora_regreso']").val();
                itemD["lugar_hospeda"] = $(this).find("input[id='lugar_hospeda']").val();

                if ($(this).find("textarea[id='lugar']").val() != "") {
                    DATA[i-1]["DESTINOS"].push(itemD);
                }
            });
            
            insert = false;
        }
    });
    
    INFO = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    //console.log('Unidades: '+aInfo);

    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url + 'Contratos/insertUnidades',
        processData: false,
        contentType: false,
        success: function(data) {
            //console.log(data);
        }
    });
}


//---Unidad--------------------------------------
function addUnidadCotiza(){
    contUnidadCot++;
    var clonSelect = $("#sel_unidad").clone();
    clonSelect.attr('id', 'unidad_u_'+contUnidadCot);

    var html = '<tr id="tr_unidades_'+contUnidadCot+'" class="tr_unidades">\
                    <td width="40%">\
                        <input type="hidden" id="id" value="0">\
                        <input type="hidden" value="tr_unidades_'+contUnidadCot+'" readonly>\
                        <div class="form-group">\
                            <label class="col-form-label">Unidad</label>\
                            <div class="controls">\
                                <select class="form-control form-control-smr" id="unidades">\
                                </select>\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="25%">\
                        <div class="form-group">\
                            <label class="col-form-label">Cantidad</label>\
                            <div class="controls">\
                                <input readonly type="number" id="cantidad" class="form-control form-control-sm" min="1" value="1">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="25%">\
                        <div class="form-group">\
                            <label class="col-form-label">Monto unitario $</label>\
                            <div class="controls">\
                                <input type="text" id="monto" class="form-control form-control-sm" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="10%" colspan="2">\
                        <span style="color: transparent;">add</span>\
                        <button type="button" class="btn btn-danger" title="Eliminar unidad" onclick="eliminarAddUnidad('+contUnidadCot+')"><i class="fa fa-minus" aria-hidden="true"></i></button>\
                    </td>\
                </tr>\
                <!---destinos-->\
                <tr id="tr_unidades_space_'+contUnidadCot+'"></tr>\
                <tr id="tr_unidades_destinos_'+contUnidadCot+'" class="tr_destinos">\
                \
                <td colspan="4">\
                    <table id="table_destinos" width="100%">\
                    <tbody id="tbody_unidad_destinos_'+contUnidadCot+'">\
                        <tr id="tr_destinos" class="tr_destinos original">\
                            <td width="5%">\
                                \
                                <input type="hidden" value="tbody_uni_dest_'+contUnidadCot+'" readonly>\
                                <input type="hidden" value="tr_destinos" readonly>\
                                \
                            </td>\
                            \
                            <td width="80%">\
                                <input type="hidden" id="id" value="0">\
                                <div class="form-group">\
                                    <label class="col-form-label">Itinerario</label>\
                                    <div class="controls">\
                                        <!--<input type="text" id="lugar" class="form-control form-control-smr" value="">-->\
                                        <textarea type="text" id="lugar" class="form-control"rows="2"></textarea>\
                                    </div>\
                                </div>\
                            </td>\
                            \
                            <!--\
                            <td width="15%">\
                                <div class="form-group">\
                                    <label class="col-form-label">Fecha Salida</label>\
                                    <div class="controls">\
                                        <input type="date" id="fecha" class="form-control form-control-sm" value="">\
                                    </div>\
                                </div>\
                            </td>\
                            \
                            <td width="15%">\
                                <div class="form-group">\
                                    <label class="col-form-label">Hora Salida</label>\
                                    <div class="controls">\
                                        <input type="text" style="width:100%" id="hora" value="00:00">\
                                    </div>\
                                </div>\
                            </td>\
                            \
                            <td width="15%">\
                                <div class="form-group">\
                                    <label class="col-form-label">Fecha Regreso</label>\
                                    <div class="controls">\
                                        <input type="date" id="fecha_regreso" class="form-control form-control-sm" value="">\
                                    </div>\
                                </div>\
                            </td>\
                            \
                            <td width="15%">\
                                <div class="form-group">\
                                    <label class="col-form-label">Hora Regreso</label>\
                                    <div class="controls">\
                                        <input type="text" style="width:100%" id="hora_regreso" value="00:00">\
                                    </div>\
                                </div>\
                            </td>-->\
                            \
                            <!--<td width="15%">\
                                <div class="form-group">\
                                    <label class="col-form-label">Lugar de hospedaje</label>\
                                    <div class="controls">\
                                        <input type="text" id="lugar_hospeda" class="form-control form-control-sm" value="">\
                                    </div>\
                                </div>\
                            </td>-->\
                            \
                            <td width="10%">\
                                <span style="color: transparent;">add</span>\
                                <button type="button" class="btn btn-primary" title="Agregar nuevo destino" onclick="addDestinoCotiza('+contUnidadCot+')"><i id="btn_plus" class="fa fa-plus" aria-hidden="true"></i></button>\
                            </td>\
                            \
                            <td width="5%"></td>\
                        </tr>\
                    </tbody>\
                </table>\
            </td>\
        </tr>';

    $('#tbody_unidades').append(html);
    $('#tr_unidades_'+contUnidadCot+' #unidades').replaceWith(clonSelect);
    $('#tr_unidades_'+contUnidadCot+' #unidad_u_'+contUnidadCot).select2({width: '100%'});
    
    $('#tbody_unidad_destinos_'+contUnidadCot+' #hora').timepicker({closeText:'Cerrar',okText:'Aceptar',hour24:true});
    $('#tbody_unidad_destinos_'+contUnidadCot+' #hora_regreso').timepicker({closeText:'Cerrar',okText:'Aceptar',hour24:true});
}


function eliminarAddUnidad(row_id) {
    //contUnidadCot--;
    $('#tr_unidades_' + row_id).remove();
    $('#tr_unidades_space_' + row_id).remove();
    $('#tr_unidades_destinos_' + row_id).remove();
}


function delUnidadCotiza(id, prospecto) {
    if(prospecto){
        $('#tr_unidades_each_' + id).remove();
        $('#tr_unidades_each_space_' + id).remove();
        $('#tr_unidades_each_destinos_' + id).remove();
        return;
    }

    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar esta Unidad?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Contratos/delete",
                    data: {
                        id:id, tabla:"unidad_prospecto"
                    },
                    success:function(response){
                        $('#tr_unidades_each_' + id).remove();
                        $('#tr_unidades_each_space_' + id).remove();
                        $('#tr_unidades_each_destinos_' + id).remove();

                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}


//---Destino--------------------------------------
function addDestinoCotiza(row = 0, each = 0){
    contDestinoCot++;
    each = each == 0 ? '':'each_';

    var html = '<tr id="tr_destinos_'+contDestinoCot+'" class="tr_destinos">\
                    <td width="5%">\
                    \
                    <input type="hidden" value="tr_destinos_'+contDestinoCot+'" readonly>\
                    \
                    </td>\
                    <td width="80%">\
                        <input type="hidden" id="id" value="0">\
                        <div class="form-group">\
                            <label class="col-form-label">Itinerario</label>\
                            <div class="controls">\
                                <!--<input type="text" id="lugar" class="form-control form-control-smr" value="">-->\
                                <textarea type="text" id="lugar" class="form-control" rows="2"></textarea>\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <!--\
                    <td width="15%">\
                        <div class="form-group">\
                            <label class="col-form-label">Fecha Salida</label>\
                            <div class="controls">\
                                <input type="date" id="fecha" class="form-control form-control-sm" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="15%">\
                        <div class="form-group">\
                            <label class="col-form-label">Hora Salida</label>\
                            <div class="controls">\
                                <input type="text" style="width:100%" id="hora" value="00:00">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    \
                    <td width="15%">\
                        <div class="form-group">\
                            <label class="col-form-label">Fecha Regreso</label>\
                            <div class="controls">\
                                <input type="date" id="fecha_regreso" class="form-control form-control-sm" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="15%">\
                        <div class="form-group">\
                            <label class="col-form-label">Hora Regreso</label>\
                            <div class="controls">\
                                <input type="text" style="width:100%" id="hora_regreso" value="00:00">\
                            </div>\
                        </div>\
                    </td>-->\
                    \
                    <!--<td width="15%">\
                        <div class="form-group">\
                            <label class="col-form-label">Lugar de hospedaje</label>\
                            <div class="controls">\
                                <input type="text" id="lugar_hospeda" class="form-control form-control-sm" value="">\
                            </div>\
                        </div>\
                    </td>-->\
                    \
                    <td width="10%">\
                        <span style="color: transparent;">add</span>\
                        <button type="button" class="btn btn-danger" title="Eliminar destino" onclick="eliminarAddDestino('+contDestinoCot+')"><i class="fa fa-minus" aria-hidden="true"></i></button>\
                    </td>\
                    <td width="5%"></td>\
                </tr>';
    
    $('#tbody_unidades  #tbody_unidad_'+each+'destinos_'+row).append(html);

    $('#tr_destinos_'+contDestinoCot+' #hora').timepicker({closeText:'Cerrar',okText:'Aceptar',hour24:true});
    $('#tr_destinos_'+contDestinoCot+' #hora_regreso').timepicker({closeText:'Cerrar',okText:'Aceptar',hour24:true});
}


function eliminarAddDestino(row_id) {
    //contDestinoCot--;
    $('#tr_destinos_' + row_id).remove();
}


function delDestinoCotiza(id, prospecto) {
    if(prospecto){
        $('#tr_destinos_each_' + id).remove();
        return;
    }
    
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este destino?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Contratos/delete",
                    data: {
                        id:id, tabla:"destino_prospecto"
                    },
                    success:function(response){ 

                        $('#tr_destinos_each_' + id).remove();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}


function getDataItinerario(id){
    $.ajax({
        type: 'POST',
        url: base_url+'Contratos/get_data_destinos',
        data: {id: id},
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data){
            //console.log(data);
            const objeto = JSON.parse(data);
            //$('#tbody_pagos').empty();
            if(objeto.length > 0) {
                objeto.forEach(element => {
                    //console.log(element);
                    tabla_unidades(element.id, element.unidad, element.cantidad, element.monto, element.destinos, false, true);
                    
                });
            }
            
        }
    });
}

function getDates(id){
    $.ajax({
        type: 'POST',
        url: base_url+'Contratos/get_dates_itinerario',
        data: {id: id},
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data){
            if(data != '0000-00-00 00:00:00hrs.'){
                swal("Atención!", "Por favor, actualice la fecha y hora de regreso. La fecha de regreso anterior más grande es: "+data, "warning");
            }
        }
    });
}