var base_url = $('#base_url').val();
var contDia = 0;
var contHora = 0;

var idCont = $('#idContrato').val();
var idUnidad = $('#idUnidad').val();

var checkedHours = true;

$(document).ready(function() {
    get_tabla_dias(idCont);

    //KM INICIAL
    var img=""; var imgdet="";
    if($("#img_aux").val()!=""){
        img=''+base_url+"public/uploads/contratos/evidenciaskm/"+$("#img_aux").val()+'';
        imgdet = {type:"image", url: ""+base_url+"Contratos/delete_img/"+$("#idBitaRev").val(), caption: $("#img_aux").val()}
    }
        
    $('#img_evide').fileinput({
        showCaption: false, // Ocultar titulo
        showUpload: false, // Ocultar el botón "Upload File"

        allowedFileExtensions: ["png", "jpg", "jpeg", "bmp"],
        browseLabel: 'Seleccionar imagen',
        uploadUrl: base_url + 'Contratos/cargar_imagen',
        maxFilePreviewSize: 5000,
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        initialPreview: [
            ''+img+'',
        ],
        initialPreviewAsData: 'false',
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
            imgdet
        ],
        previewFileIconSettings: {
            'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
        },
        uploadExtraData: function (previewId, index) {
            var info = {
                id: $("#idBitaRev").val(),
            };
            return info;
        }
    });


    //KM FINAL
    var imgF=""; var imgdetF="";
    if($("#img_aux_f").val()!=""){
        imgF=''+base_url+"public/uploads/contratos/evidenciaskm/"+$("#img_aux_f").val()+'';
        imgdetF = {type:"image", url: ""+base_url+"Contratos/delete_img_f/"+$("#idBitaRev").val(), caption: $("#img_aux_f").val()}
    }
        
    $('#img_evide_f').fileinput({
        showCaption: false, // Ocultar titulo
        showUpload: false, // Ocultar el botón "Upload File"

        allowedFileExtensions: ["png", "jpg", "jpeg", "bmp"],
        browseLabel: 'Seleccionar imagen',
        uploadUrl: base_url + 'Contratos/cargar_imagen_f',
        maxFilePreviewSize: 5000,
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        initialPreview: [
            ''+imgF+'',
        ],
        initialPreviewAsData: 'false',
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
            imgdetF
        ],
        previewFileIconSettings: {
            'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
        },
        uploadExtraData: function (previewId, index) {
            var info = {
                id: $("#idBitaRev").val(),
            };
            return info;
        }
    });



    $("#km_ini,#km_fin").on("change",function(){
        var km_ini = Number($("#km_ini").val());
        var km_fin = Number($("#km_fin").val());

        if(km_fin<km_ini){
            swal("Error!", "El km final no puede ser menor que el km inicial", "warning");
            $("#km_fin").val(1);
        }
        if(km_fin==0){
            swal("Error!", "El km final no puede ser 0", "warning");
            $("#km_fin").val(1);
        }
    })
});

function checkTime(inputElement1, order){
    if(order == 1){
        var inputElement2 = inputElement1.parentElement.parentElement.parentElement.nextElementSibling.querySelector('input[type="time"]');
    }else if(order == 2){
        var inputElement2 = inputElement1.parentElement.parentElement.parentElement.previousElementSibling.querySelector('input[type="time"]');
    }

    if(order == 1 && inputElement1.value > inputElement2.value){
        if(inputElement2.value == ''){
            return;
        }
        swal("Atención!", "Revise que sus horas sean correctas y correspondan a un solo día.\n La hora mínima permitida es 12:00 AM.", "error");
        $(inputElement1).val("00:00");
        //checkedHours = false;
        return;
    }

    if(order == 2 && inputElement2.value > inputElement1.value){
        if(inputElement2.value == ''){
            return;
        }
        swal("Atención!", "Revise que sus horas sean correctas y correspondan a un solo día.\n La hora maxíma permitida es 11:59 PM.", "error");
        //checkedHours = false;
        $(inputElement1).val("23:59");
        return;
    }
    checkedHours = true;
    //$(inputElement1).addClass('bg-danger');
    //$(inputElement2).addClass('bg-dark');
}

function add_form(){
	var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {},
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        invalidHandler: function(event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);
        },
        highlight: function(element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function(element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function(label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });

    var $valid = $("#form_data").valid();
    if ($valid && checkedHours) {
        $('.btn_registro').attr('disabled',true);

        id = $('#idObs').val();
        var datos = form_register.serialize();        
        console.log("DATOS:" + datos);

        $.ajax({
            type:'POST',
            url: base_url + 'Contratos/insert_bitacora_revisiones',
            data: datos,
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data) {
                //console.log(data);
                $("#idBitaRev").val(data);
                if($("#img_evide").val()!=""){
                    $('#img_evide').fileinput('upload');
                }

                if($("#img_evide_f").val()!=""){
                    $('#img_evide_f').fileinput('upload');
                }

                addDias(idCont,idUnidad);
                swal("Éxito", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    window.location = base_url+'Contratos';
                }, 2500);
            }
        });
    }

    if(!checkedHours){
        swal("Atención!", "Revise que sus horas sean correctas y correspondan a un solo día\n La hora mínima permitida es 12:00 AM. \n La hora maxíma permitida es 11:59 PM.", "error");
    }
}

//---Dia-------------------------------------->
function get_tabla_dias(id){
	$.ajax({
        type:'POST',
		url: base_url+"Contratos/get_table_dias",
		data: {
            id : id,
            idU : idUnidad
        },
		success: function (response){
			var array = $.parseJSON(response);
            
            if(array.length > 0) {
                array.forEach(function(element, index){
                    tabla_dias(element.id, element.fecha, element.horas, index);
                });
            }
		},
		error: function(response){
            swal("Error", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}


function tabla_dias(idEach, fecha, horas, index){
	//console.log("----- Genera Row Dia Edicion "+idEach+" -----");
    var a_horas = JSON.parse(JSON.stringify(horas));

    var html = '<tr id="tr_dias_each_'+idEach+'" class="tr_dias">\
                    <td width="10%">\
                        <input type="hidden" id="id" value="'+idEach+'">\
                        <input type="hidden" value="tr_dias_'+idEach+'" readonly>\
                        <div class="form-group">\
                            <br>\
                            <h3>Día '+(index+1)+':</h3>\
                        </div>\
                    </td>\
                    \
                    <td width="80%">\
                        <div class="form-group">\
                            <label class="col-form-label">Fecha</label>\
                            <div class="controls">\
                                <input type="date" id="fecha" class="form-control form-control-sm" value="'+fecha+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="10%">\
                        <br>\
                        <span class="required" style="color: transparent;">dele</span>\
                        <a class="btn btn-danger" title="Eliminar día" onclick="delDiaBitacora('+idEach+')"><i class="fa fa-minus" aria-hidden="true"></i></a>\
                    </td>\
                </tr>\
                <!---horas-->\
                <tr id="tr_dias_each_space_'+idEach+'"></tr>\
                <tr id="tr_dias_each_horas_'+idEach+'" class="tr_horas">\
                \
                    <td colspan="4">\
                        <table id="table_horas" width="100%">\
                            <tbody id="tbody_dia_each_horas_'+idEach+'">\
                                <tr id="tr_horas" class="tr_horas">\
                                    <td width="10%">\
                                        \
                                        <input type="hidden" value="tbody_dia_ea_hora_'+contDia+'" readonly>\
                                        <input type="hidden" value="tr_horas" readonly>\
                                        \
                                    </td>\
                                    \
                                    <td width="30%">\
                                        <input type="hidden" id="id" value="0">\
                                        <div class="form-group">\
                                            <label class="col-form-label">Tipo</label>\
                                            <div class="controls">\
                                                <select class="form-control" id="tipo">\
                                                    <option value="1">Fuera de servicio</option>\
                                                    <option value="2">Dormido</option>\
                                                    <option value="3">Manejando</option>\
                                                    <option value="4">Sin manejar</option>\
                                                </select>\
                                            </div>\
                                        </div>\
                                    </td>\
                                    \
                                    <td width="20%">\
                                        <div class="form-group">\
                                            <label class="col-form-label">Hora Inicio</label>\
                                            <div class="controls">\
                                            <input type="time" onchange="checkTime(this,1)" type="datetime-local" id="hora_ini" class="form-control form-control-sm" value="">\
                                            </div>\
                                        </div>\
                                    </td>\
                                    \
                                    <td width="20%">\
                                        <div class="form-group">\
                                            <label class="col-form-label">Hora Final</label>\
                                            <div class="controls">\
                                            <input type="time" onchange="checkTime(this,2)" type="datetime-local" id="hora_fin" class="form-control form-control-sm" value="">\
                                            </div>\
                                        </div>\
                                    </td>\
                                    \
                                    <td width="10%">\
                                        <span style="color: transparent;">add</span>\
                                        <button type="button" class="btn btn-primary" title="Agregar horas" onclick="addHoraBitacora('+idEach+','+1+')"><i id="btn_plus" class="fa fa-plus" aria-hidden="true"></i></button>\
                                    </td>\
                                    \
                                    <td width="10%"></td>\
                                </tr>\
                            </tbody>\
                        </table>\
                    </td>\
                </tr>';

    $('#tbody_dias').append(html);

    if(a_horas.length > 0) {
        a_horas.forEach(function(element){
            tabla_horas(idEach, element.id, element.tipo, element.hora_ini, element.hora_fin);
        });
    }
}


function addDiaBitacora(){
    contDia++;

    var html = '<tr id="tr_dias_'+contDia+'" class="tr_dias">\
                    <td width="10%">\
                        <input type="hidden" id="id" value="0">\
                        <input type="hidden" value="tr_dias_'+contDia+'" readonly>\
                        <div class="form-group">\
                            <br>\
                            <h3>Día:</h3>\
                        </div>\
                    </td>\
                    \
                    <td width="80%">\
                        <div class="form-group">\
                            <label class="col-form-label">Fecha</label>\
                            <div class="controls">\
                            <input type="date" id="fecha" class="form-control form-control-sm" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="10%" colspan="2">\
                        <br>\
                        <span style="color: transparent;">add</span>\
                        <button type="button" class="btn btn-danger" title="Eliminar día" onclick="eliminarAddDia('+contDia+')"><i class="fa fa-minus" aria-hidden="true"></i></button>\
                    </td>\
                </tr>\
                \
                <!---horas-->\
                <tr id="tr_dias_space_'+contDia+'"></tr>\
                <tr id="tr_dias_horas_'+contDia+'" class="tr_horas">\
                \
                    <td colspan="4">\
                        <table id="table_horas" width="100%">\
                            <tbody id="tbody_dia_horas_'+contDia+'">\
                                <tr id="tr_horas" class="tr_horas">\
                                    <td width="10%">\
                                        \
                                        <input type="hidden" value="tbody_dia_hora_'+contDia+'" readonly>\
                                        <input type="hidden" value="tr_horas" readonly>\
                                        \
                                    </td>\
                                    \
                                    <td width="30%">\
                                        <input type="hidden" id="id" value="0">\
                                        <div class="form-group">\
                                            <label class="col-form-label">Tipo</label>\
                                            <div class="controls">\
                                            <select class="form-control" id="tipo">\
                                                <option value="1">Fuera de servicio</option>\
                                                <option value="2">Dormido</option>\
                                                <option value="3">Manejando</option>\
                                                <option value="4">Sin manejar</option>\
                                            </select>\
                                            </div>\
                                        </div>\
                                    </td>\
                                    \
                                    <td width="20%">\
                                        <div class="form-group">\
                                            <label class="col-form-label">Hora Inicio</label>\
                                            <div class="controls">\
                                            <input type="time" onchange="checkTime(this,1)" id="hora_ini" class="form-control form-control-sm" value="">\
                                            </div>\
                                        </div>\
                                    </td>\
                                    \
                                    <td width="20%">\
                                        <div class="form-group">\
                                            <label class="col-form-label">Hora Final</label>\
                                            <div class="controls">\
                                            <input type="time" onchange="checkTime(this,2)" id="hora_fin" class="form-control form-control-sm" value="">\
                                            </div>\
                                        </div>\
                                    </td>\
                                    \
                                    <td width="10%">\
                                        <span style="color: transparent;">add</span>\
                                        <button type="button" class="btn btn-primary" title="Agregar horas" onclick="addHoraBitacora('+contDia+')"><i id="btn_plus" class="fa fa-plus" aria-hidden="true"></i></button>\
                                    </td>\
                                    \
                                    <td width="10%"></td>\
                                </tr>\
                            </tbody>\
                        </table>\
                    </td>\
                </tr>';

    $('#tbody_dias').append(html);
}


function eliminarAddDia(row_id) {
    $('#tr_dias_' + row_id).remove();
    $('#tr_dias_space_' + row_id).remove();
    $('#tr_dias_horas_' + row_id).remove();
}


function delDiaBitacora(id) {
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este Día?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Contratos/delete",
                    data: {
                        id:id, tabla:"bitacora_dias"
                    },
                    success:function(response){
                        $('#tr_dias_each_' + id).remove();
                        $('#tr_dias_each_space_' + id).remove();
                        $('#tr_dias_each_horas_' + id).remove();

                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}


function addDias(id, idUnidad){
    var DATA = [];

    var TABLAD = $("#table_dias tbody > tr");
    var i = 0;
    var insert = false;

    TABLAD.each(function() {
        item = {};

        if($(this).hasClass("tr_dias")) {
            item["id"] = $(this).find("input[id='id']").val();
            //item["id_prospecto"] = $('#idCliente').val();
            item["id_contrato"] = id;
            item["id_unidad"] = idUnidad;
            item["fecha"] = $(this).find("input[id='fecha']").val();
            item["HORAS"] = [];

            if ($(this).find("input[id='fecha']").val() != "") {
                DATA.push(item);
                insert = true;
                i++;
            }
        }else if($(this).hasClass("tr_horas")){

            if(!insert){
                insert = false;
                return;
            }

            var TABLAH = $(this).find("#table_horas tbody > tr");
            TABLAH.each(function() {
                itemH = {};
                itemH["id"] = $(this).find("input[id='id']").val();
                itemH["tipo"] = $(this).find("select[id='tipo'] option:selected").val();

                //console.log('TIPO: '+$(this).find("select[id='tipo'] option:selected").val());
                //console.log('TIPOH: '+itemH["tipo"]);

                itemH["hora_ini"] = $(this).find("input[id='hora_ini']").val();
                itemH["hora_fin"] = $(this).find("input[id='hora_fin']").val();

                if ($(this).find("input[id='hora_ini']").val() != "" && $(this).find("input[id='hora_fin']").val() != "") {
                    DATA[i-1]["HORAS"].push(itemH);
                }
            });
            
            insert = false;
        }
    });
    
    INFO = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    //console.log('DIAS: '+aInfo);

    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url + 'Contratos/insert_BitaDiasHoras',
        processData: false,
        contentType: false,
        success: function(data) {
            //console.log(data);
        }
    });
}
//---Dia--------------------------------------<


//---Horas--------------------------------------
function  tabla_horas(idRow, id_each, tipo, hora_ini, hora_fin){
	//console.log("----- Genera Row U Edicion "+id_each+" -----");
    
    var html = '<tr id="tr_horas_each_'+id_each+'" class="tr_horas">\
                    <td width="10%">\
                        \
                        <input type="hidden" value="tbody_horas_each_'+id_each+'" readonly>\
                        <input type="hidden" value="tr_horas" readonly>\
                        \
                    </td>\
                    \
                    <td width="30%">\
                        <input type="hidden" id="id" value="'+id_each+'">\
                        <div class="form-group">\
                            <label class="col-form-label">Tipo</label>\
                            <div class="controls">\
                                <select class="form-control" id="tipo">\
                                    <option value="1">Fuera de servicio</option>\
                                    <option value="2">Dormido</option>\
                                    <option value="3">Manejando</option>\
                                    <option value="4">Sin manejar</option>\
                                </select>\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="20%">\
                        <div class="form-group">\
                            <label class="col-form-label">Hora Inicio</label>\
                            <div class="controls">\
                            <input type="time" onchange="checkTime(this,1)" id="hora_ini" class="form-control form-control-sm" value="'+hora_ini+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="20%">\
                        <div class="form-group">\
                            <label class="col-form-label">Hora Final</label>\
                            <div class="controls">\
                            <input type="time" onchange="checkTime(this,2)" id="hora_fin" class="form-control form-control-sm" value="'+hora_fin+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="10%">\
                        <span class="required" style="color: transparent;">dele</span>\
                        <a class="btn btn-danger" title="Eliminar hora" onclick="delHoraBitacora('+id_each+')"><i class="fa fa-minus" aria-hidden="true"></i></a>\
                    </td>\
                    \
                    <td width="10%"></td>\
                </tr>';

    $('#tbody_dias  #tbody_dia_each_horas_'+idRow).append(html);
    
    $('#tr_dias_each_horas_'+idRow+' #tr_horas_each_'+id_each+' #tipo').val(tipo);
}

function addHoraBitacora(row = 0, each = 0){
    contHora++;
    each = each == 0 ? '':'each_';

    var html = '<tr id="tr_horas_'+contHora+'" class="tr_horas">\
                    <td width="10%">\
                        \
                        <input type="hidden" value="tr_horas_'+contHora+'" readonly>\
                        \
                    </td>\
                    \
                    <td width="30%">\
                        <input type="hidden" id="id" value="0">\
                        <div class="form-group">\
                            <label class="col-form-label">Tipo</label>\
                            <div class="controls">\
                            <select class="form-control" id="tipo">\
                                <option value="1">Fuera de servicio</option>\
                                <option value="2">Dormido</option>\
                                <option value="3">Manejando</option>\
                                <option value="4">Sin manejar</option>\
                            </select>\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="20%">\
                        <div class="form-group">\
                            <label class="col-form-label">Hora Inicio</label>\
                            <div class="controls">\
                            <input type="time" onchange="checkTime(this,1)" id="hora_ini" class="form-control form-control-sm" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="20%">\
                        <div class="form-group">\
                            <label class="col-form-label">Hora Final</label>\
                            <div class="controls">\
                            <input type="time" onchange="checkTime(this,2)" id="hora_fin" class="form-control form-control-sm" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="10%">\
                        <span style="color: transparent;">add</span>\
                        <button type="button" class="btn btn-danger" title="Eliminar hora" onclick="eliminarAddHora('+contHora+')"><i class="fa fa-minus" aria-hidden="true"></i></button>\
                    </td>\
                    \
                    <td width="10%"></td>\
                </tr>';

    
    $('#tbody_dias  #tbody_dia_'+each+'horas_'+row).append(html);
}


function eliminarAddHora(row_id) {
    $('#tr_horas_' + row_id).remove();
}


function delHoraBitacora(id) {
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar esta Hora?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Contratos/delete",
                    data: {
                        id:id, tabla:"bitacora_horas"
                    },
                    success:function(response){ 

                        $('#tr_horas_each_' + id).remove();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
//---Horas--------------------------------------