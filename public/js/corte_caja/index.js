var base_url = $('#base_url').val();

$(document).ready(function () {
	$('#sel_contrato').select2({
		width: '100%'
	}).on('select2:select', function (e) {
		var data = e.params.data;
	});

	$('#sel_cliente').select2({
		width: 'resolve',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un cliente',
		ajax: {
			url: base_url + 'Cortes_de_caja/search_cliente',
			dataType: "json",
			data: function (params) {
				var query = {
					search: params.term,
					type: 'public'
				}
				return query;
			},
			processResults: function (data) {
				var itemsCli = [];
				data.forEach(function (element) {
					//console.log("element -> " + element);
					itemsCli.push({
						id: element.id,
						text: element.nombre + " " + element.app + " " + element.apm
					});
				});
				return {
					results: itemsCli
				};
			},
		}
	}).on('select2:select', function (e) {
		var data = e.params.data;
	});


	$("#search").on("click", function () {
		load_table_anticipo();
		load_table_pagos();
		load_table_gastos();
		load_totales();
		load_cuentas()
	});

	$("#export_excel").on("click", function () {
		//console.log("EXCEL");
		downloadTable(1);
	});

	$("#export_pdf").on("click", function () {
		//downloadTable(2);
	});


});

function load_table_anticipo() {
	var idContrato = $("#sel_contrato option:selected").val();
	var idCliente = $("#sel_cliente option:selected").val();
	var fechaIni = $("#fecha_ini").val();
	var fechaFin = $("#fecha_fin").val();

	if (fechaIni == "") {
		fechaIni = 0;
	}
	if (fechaFin == "") {
		fechaFin = 0;
	}

	tabla = $("#table_data_anticipo").DataTable({
		"bProcessing": true,
		"serverSide": true,
		"searching": true,
		destroy: true,
		//responsive: !0,
		"ajax": {
			"url": base_url + "Cortes_de_caja/get_list_anticipo",
			type: "post",
			data: {
				idContrato: idContrato,
				idCliente: idCliente,
				fechaIni: fechaIni,
				fechaFin: fechaFin
			},
		},
		"columns": [{
				"data": "id"
			},
			{
				"data": "folio"
			},
			{
				"data": "cliente"
			},
			{
				"data": "fecha_contrato"
			},
			{
				"data": null,
				"render": function (row) {
					var html = new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.monto_anticipo);
					return html;
				}
			},
			{
				"data": null,
				"render": function (row) {
					var html = new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.porc_desc);
					return html;
				}
			},
		],
		"order": [
			[0, "asc"]
		],
		"lengthMenu": [
			[10, 25, 50],
			[10, 25, 50]
		],
		fixedColumns: true,
		language: languageTables
	});
}

function load_table_pagos() {
	var idContrato = $("#sel_contrato option:selected").val();
	var idCliente = $("#sel_cliente option:selected").val();
	var fechaIni = $("#fecha_ini").val();
	var fechaFin = $("#fecha_fin").val();

	if (fechaIni == "") {
		fechaIni = 0;
	}
	if (fechaFin == "") {
		fechaFin = 0;
	}

	tabla = $("#table_data_pagos").DataTable({
		"bProcessing": true,
		"serverSide": true,
		"searching": true,
		destroy: true,
		//responsive: !0,
		"ajax": {
			"url": base_url + "Cortes_de_caja/get_list_pagos",
			type: "post",
			data: {
				idContrato: idContrato,
				idCliente: idCliente,
				fechaIni: fechaIni,
				fechaFin: fechaFin
			},
		},
		"columns": [{
				"data": "id_contrato"
			},
			{
				"data": "folio"
			},
			{
				"data": "cliente"
			},
			{
				"data": "fecha"
			},
			{
				"data": null,
				"render": function (row) {
					var html = new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.monto);
					return html;
				}
			},
			{
				"data": "cuenta"
			},
		],
		"order": [
			[0, "asc"]
		],
		"lengthMenu": [
			[10, 25, 50],
			[10, 25, 50]
		],
		fixedColumns: true,
		language: languageTables
	});
}

function load_table_gastos() {
	var idContrato = $("#sel_contrato option:selected").val();
	var idCliente = $("#sel_cliente option:selected").val();
	var fechaIni = $("#fecha_ini").val();
	var fechaFin = $("#fecha_fin").val();

	if (fechaIni == "") {
		fechaIni = 0;
	}
	if (fechaFin == "") {
		fechaFin = 0;
	}

	tabla = $("#table_data_gastos").DataTable({
		"bProcessing": true,
		"serverSide": true,
		"searching": true,
		destroy: true,
		//responsive: !0,
		"ajax": {
			"url": base_url + "Cortes_de_caja/get_list_gastos",
			type: "post",
			data: {
				idContrato: idContrato,
				idCliente: idCliente,
				fechaIni: fechaIni,
				fechaFin: fechaFin
			},
		},
		"columns": [{
				"data": "id_contrato"
			},
			{
				"data": "folio"
			},
			{
				"data": "cliente"
			},
			{
				"data": "num_eco"
			},
			{
				"data": "vehiculo"
			},
			{
				"data": "fecha"
			},
			{
				"data": null,
				"render": function (row) {
					var html = new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.importe);
					return html;
				}
			},
			{
				"data": null,
				"render": function (row) {
					var html = '';
					switch (row.tipo) {
						case '1':
							html = "Caseta";
							break;
						case '2':
							html = "Combustible";
							break;
						case '3':
							html = "Otros";
							break;
						case '4':
							html = "Sueldos";
							break;
						default:
							html = " ";
							break;
					}

					return html;
				}
			},
		],
		"order": [
			[4, "desc"]
		],
		"lengthMenu": [
			[10, 25, 50],
			[10, 25, 50]
		],
		fixedColumns: true,
		language: languageTables
	});
}


function load_totales() {
	var idContrato = $("#sel_contrato option:selected").val();
	var idCliente = $("#sel_cliente option:selected").val();
	var fechaIni = $("#fecha_ini").val();
	var fechaFin = $("#fecha_fin").val();

	if (fechaIni == "") {
		fechaIni = 0;
	}
	if (fechaFin == "") {
		fechaFin = 0;
	}

	$.ajax({
		type: "POST",
		url: base_url + "Cortes_de_caja/get_totales",
		data: {
			idContrato: idContrato,
			idCliente: idCliente,
			fechaIni: fechaIni,
			fechaFin: fechaFin
		},
		async: false,
		success: function (result) {
			//console.log("result: " + result);
			var array = $.parseJSON(result);
			$("#cont_totales").empty();

			var html = '\
					<table class="table" style="width:100%;">\
						<tbody>\
							<tr>\
								<td scope="col">Total Anticipos:</td>\
								<td scope="col" style="text-align: right;"><b>' + new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(array.totalAnticipos) + '</b></td>\
							</tr>\
							<tr>\
								<td scope="col">Total Pagos:</td>\
								<td scope="col" style="text-align: right;"><b>' + new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(array.totalPagos) + '</b></td>\
							</tr>\
							<tr>\
								<td scope="col">Total Gastos:</td>\
								<td scope="col" style="text-align: right;"><b>' + new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(array.totalGastos) + '</b></td>\
							</tr>\
							<tr>\
								<td scope="col">Descuento:</td>\
								<td scope="col" style="text-align: right;"><b>' + new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(array.descuento) + '</b></td>\
							</tr>\
							<tr>\
								<td scope="col" style="font-size: 115%">Total Final:</td>\
								<td scope="col" style="text-align: right; font-size: 115%"><b>' + new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(array.totalFinal) + '</b></td>\
							</tr>\
							<tr>\
								<td colspan="4"> </td>\
							</tr>\
						</tbody>\
					</table>';

			$("#cont_totales").append(html);
		}
	});
}

function load_cuentas() {
	var idContrato = $("#sel_contrato option:selected").val();
	var idCliente = $("#sel_cliente option:selected").val();
	var fechaIni = $("#fecha_ini").val();
	var fechaFin = $("#fecha_fin").val();

	if (fechaIni == "") {
		fechaIni = 0;
	}
	if (fechaFin == "") {
		fechaFin = 0;
	}

	$.ajax({
		type: "POST",
		url: base_url + "Cortes_de_caja/get_cuentas",
		data: {
			idContrato: idContrato,
			idCliente: idCliente,
			fechaIni: fechaIni,
			fechaFin: fechaFin
		},
		async: false,
		success: function (result) {
			console.log("result Cuentas: " + result);
			var array = $.parseJSON(result);

			$("#cont_cuentas").empty();

			var html = '\
					<table class="table" style="width:100%;">\
						<tbody>';

			array.forEach(function (object) {

				if (object.cuenta.trim() == "EFECTIVO") {
					console.log("EFECTIVO");
					html += '<tr>\
								<td scope="col">Total Efectivo:</td>\
								<td scope="col" style="text-align: right;"><b>' + new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(object.pagos_cuenta) + '</b></td>\
							</tr>';

				} else {
					console.log("CUENTA");
					html += '<tr>\
								<td scope="col">Total Cuenta <i>' + object.cuenta + '</i>:</td>\
								<td scope="col" style="text-align: right;"><b>' + new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(object.pagos_cuenta) + '</b></td>\
							</tr>';
				}

			});

			html += '</tbody>\
					</table>';

			$("#cont_cuentas").append(html);

		}
	});
}


function downloadTable(tipo){
	var idContrato = $("#sel_contrato option:selected").val();
	var idCliente = $("#sel_cliente option:selected").val();
	var fechaIni = $("#fecha_ini").val();
	var fechaFin = $("#fecha_fin").val();

	if (fechaIni == "") {
		fechaIni = 0;
	}
	if (fechaFin == "") {
		fechaFin = 0;
	}
	
	window.open(base_url+"Cortes_de_caja/exportCorteCaja/"+idContrato+"/"+idCliente+"/"+fechaIni+"/"+fechaFin+"/"+tipo, '_blank');
}
