var base_url = $('#base_url').val();
let textPreg5 = '¿Qué recomendaría para mejorar nuestro servicio o tiene algún comentario o sugerencia para nosotros?';
let textPreg6 = 'En caso de tener alguna inconformidad con el servicio, favor de mencionarla.';

$(document).ready(function() {
    loadtable();
    
    $("#export-excel").on("click",function(){
        downloadTable(1);
    });
});

function loadtable(){
    tabla=$("#table_data").DataTable({
        //"bProcessing": true,
        //"serverSide": true,
        //"searching": true,
        //responsive: !0,
        "ajax": {
            "url": base_url+"EncuestaR/getlistado",
            type: "post",
        },
        "columns": [
            {"data":"id"},
            {"data": null,
                "render": function (row){
                    var html = "";
                    switch (row.pregunta1) {
                        case '1':
                            html = "Muy mala";
                            break;
                        case '2':
                            html = "Mala";
                            break;
                        case '3':
                            html = "Regular";
                            break;
                        case '4':
                            html = "Buena";
                            break;
                        case '5':
                            html = "Muy buena";
                            break;
                        default:
                            html = "---";
                            break;
                    }
                    return html;
                }
            },
            {"data": null,
                "render": function (row){
                    var html = "";
                    switch (row.pregunta2) {
                        case '1':
                            html = "Muy mala";
                            break;
                        case '2':
                            html = "Mala";
                            break;
                        case '3':
                            html = "Regular";
                            break;
                        case '4':
                            html = "Buena";
                            break;
                        case '5':
                            html = "Muy buena";
                            break;
                        default:
                            html = "---";
                            break;
                    }
                    return html;
                }
            },
            {"data": null,
                "render": function (row){
                    var html = "";
                    switch (row.pregunta3) {
                        case '1':
                            html = "Muy mala";
                            break;
                        case '2':
                            html = "Mala";
                            break;
                        case '3':
                            html = "Regular";
                            break;
                        case '4':
                            html = "Buena";
                            break;
                        case '5':
                            html = "Muy buena";
                            break;
                        default:
                            html = "---";
                            break;
                    }
                    return html;
                }
            },
            {"data": null,
                "render": function (row){
                    var html = "";
                    switch (row.pregunta4) {
                        case '1':
                            html = "Muy mala";
                            break;
                        case '2':
                            html = "Mala";
                            break;
                        case '3':
                            html = "Regular";
                            break;
                        case '4':
                            html = "Buena";
                            break;
                        case '5':
                            html = "Muy buena";
                            break;
                        default:
                            html = "---";
                            break;
                    }
                    return html;
                }
            },
            {"data": null,
                "render": function (row){
                    var html = '<a class="btn btn-flat btn-primary mt-2" onclick="modal_respuesta(\'' + row.pregunta5 + '\','+ 5 +')"><i class="fa fa-eye"></i></a>';
                    return html;
                }
            },
            {"data": null,
                "render": function (row){
                    var html = '<a class="btn btn-flat btn-primary mt-2" onclick="modal_respuesta(\'' + row.pregunta6 + '\','+ 6 +')"><i class="fa fa-eye"></i></a>';
                    return html;
                }
            },
            {"data": null,
                "render": function (row){
                    var html = '<a class="btn btn-flat btn-primary mt-2" onclick="modal_infoR('+row.id+')"><i class="fa fa-eye"></i></a>';
                    return html;
                }
            },
        ],
        "order": [[ 0, "asc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        fixedColumns: true,
        language: languageTables
    });
}

function modal_respuesta(resp, num){
    $('#modal_respuesta').modal("show");
    $('#myModalLabel1').text('Pregunta '+num);
    $('#quest').text('');
    if(num == 5){
        $('#quest').text(textPreg5);
    }else if(num == 6){
        $('#quest').text(textPreg6);
    }
    $('#textareaR').val(resp);
}

function modal_infoR(id){
    $('#modal_infoR').modal("show");
    $('#idEncuestaR').val(id);

    $.ajax({
        type: 'POST',
        url: base_url+'EncuestaR/get_data_encuesta',
        data: {id: id},
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data){
            //console.log('<---data---> ' + data);
            const obj = JSON.parse(data);
            //console.log("obj: " + obj);

            $('#clienteR').val(obj.cliente);
            $('#contratoR').val(obj.id_contrato);
            $('#creacionR').val(obj.reg);
        }
    });   
}

function downloadTable(tipo){
    if(tipo=1){
        window.open(base_url+"EncuestaR/exportEncuesta", '_blank');
    }
}