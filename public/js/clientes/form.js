var base_url = $('#base_url').val();
var contDestinoCot = 0;
var contUnidadCot = 0;

var idCli = $('#idcliente').val();
var firstElement = true;


$(document).ready(function() {
    $('#estado').select2({width: '100%'});

    var img_file=""; var imgdet=""; var typePDF="";
    if($("#file_aux").val()!=""){
        img_file=''+base_url+"uploads/clientes/"+$("#file_aux").val()+'';
        ext = $("#file_aux").val().split('.');
        if(ext[1]!="pdf"){ 
            imgdet = {type:"image", url: ""+base_url+"Clientes/deleteImg/"+$("#idcliente_ext").val()+"/1", caption: $("#file_aux").val(), key:1}
            typePDF = "false";  
        }else{
            imgdet = {type:"pdf", url: base_url+"Clientes/deleteImg/"+$("#idcliente_ext").val()+"/1", caption: $("#file_aux").val(), key:1};
            typePDF = "true";
        }
    }

    $("#identifica").fileinput({
        showCaption: false,
        showUpload: false,
        allowedFileExtensions: ["png","jpg","jpeg","bmp","pdf"],
        browseLabel: 'Seleccionar imagen',
        uploadUrl: base_url+'Clientes/cargaimagen',
        maxFilePreviewSize: 5000,
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        initialPreview: [
            ''+img_file+'',   
        ],
        initialPreviewAsData: typePDF,
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
            imgdet
        ],
        previewFileIconSettings: {
            'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-pdf-o text-warning"></i>'
        },
        uploadExtraData: function (previewId, index) {
        var info = {   
                    input_name:"identifica",
                    idcliente_ext:$("#idcliente_ext").val(),
                    idcliente:$("#idcliente").val()
                };
        return info;
      }
    }).on('fileuploaded', function(event, files, extra) {
      //location.reload();
    }).on('filedeleted', function(event, files, extra) {
      //location.reload();
    });
    
    var img_file2=""; var imgdet2=""; var typePDF2="";
    if($("#file_aux2").val()!=""){
        img_file2=''+base_url+"uploads/clientes/"+$("#file_aux2").val()+'';
        ext2 = $("#file_aux").val().split('.');
        if(ext2[1]!="pdf"){ 
            imgdet2 = {type:"image", url: ""+base_url+"Clientes/deleteImg/"+$("#idcliente_ext").val()+"/2", caption: $("#file_aux2").val(), key:2}
            typePDF2 = "false";  
        }else{
            imgdet2 = {type:"pdf", url: base_url+"Clientes/deleteImg/"+$("#idcliente_ext").val()+"/2", caption: $("#file_aux2").val(), key:2};
            typePDF2 = "true";
        }
    }
    $("#comprobante").fileinput({
        showCaption: false,
        showUpload: false,
        //maxFileCount: 6,
        language:'es',
        allowedFileExtensions: ["png","jpg","jpeg","bmp","pdf"],
        browseLabel: 'Seleccionar imagen',
        uploadUrl: base_url+'Clientes/cargaimagen',
        maxFilePreviewSize: 5000,
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        initialPreview: [
            ''+img_file2+'',    
        ],
        initialPreviewAsData: typePDF2,
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
            imgdet2
        ],
        previewFileIconSettings: {
            'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-pdf-o text-warning"></i>'
        },
        uploadExtraData: function (previewId, index) {
        var info = {
                    input_name:"comprobante",
                    idcliente_ext:$("#idcliente_ext").val(),
                    idcliente:$("#idcliente").val()
                };
        return info;
      }
    }).on('fileuploaded', function(event, files, extra) {

    }).on('filedeleted', function(event, files, extra) {

    });

    //get_tabla_unidades(idCli);
});



function get_tabla_unidades(id){

	$.ajax({
        type:'POST',
		url: base_url+"Clientes/get_table_unidades",
		data: {id : id},
		success: function (response){
			var array = $.parseJSON(response);
            
            if(array.length > 0) {
                array.forEach(function(element){
                    tabla_unidades(element.id, element.unidad, element.cantidad, element.monto, element.destinos, firstElement);
                    firstElement = false;
                });
            }
		},
		error: function(response){
            toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}

function tabla_unidades(idEach, unidad, cantidad, monto, destinos, firstElement){
	console.log("----- Genera Row D Edicion "+idEach+" -----");
    var a_destinos = JSON.parse(JSON.stringify(destinos));
    var clonSelect = $("#sel_unidad").clone();
    clonSelect.attr('id', 'unidad_d_'+idEach);

    var htmlT ='<tr id="tr_unidades_each_sub">\
                    <td width="100%" colspan="5">\
                        <hr>\
                        <h5>Unidades Agregadas</h5>\
                    </td>\
                </tr>';

    var html = '<tr id="tr_unidades_each_'+idEach+'" class="tr_unidades">\
                    <td width="40%">\
                        <input type="hidden" id="id" value="'+idEach+'">\
                        <input type="hidden" value="tr_unidades_'+idEach+'" readonly>\
                        <div class="form-group">\
                            <label class="col-form-label">Unidad</label>\
                            <div class="controls">\
                                <select class="form-control form-control-smr" id="unidades">\
                                </select>\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="25%">\
                        <div class="form-group">\
                            <label class="col-form-label">Cantidad</label>\
                            <div class="controls">\
                                <input type="number" id="cantidad" class="form-control form-control-sm" min="0" value="'+cantidad+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="25%">\
                        <div class="form-group">\
                            <label class="col-form-label">Monto unitario $</label>\
                            <div class="controls">\
                                <input type="text" id="monto" class="form-control form-control-sm" value="'+monto+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="10%" colspan="2">\
                        <span class="required" style="color: transparent;">dele</span>\
                        <a class="btn btn-danger" title="Eliminar Unidad" onclick="delUnidadCotiza('+idEach+')"><i class="fa fa-minus" aria-hidden="true"></i></a>\
                    </td>\
                </tr>\
                <!---destinos-->\
                <tr id="tr_unidades_each_space_'+idEach+'"></tr>\
                <tr id="tr_unidades_each_destinos_'+idEach+'" class="tr_destinos">\
                \
                <td colspan="4">\
                    <table id="table_destinos" width="100%">\
                    <tbody id="tbody_unidad_each_destinos_'+idEach+'">\
                        <tr id="tr_destinos" class="tr_destinos">\
                            <td width="10%">\
                                \
                                <input type="hidden" value="tbody_uni_dest_ea_des_'+idEach+'" readonly>\
                                <input type="hidden" value="tr_destinos" readonly>\
                                \
                            </td>\
                            \
                            <td width="20%">\
                                <input type="hidden" id="id" value="0">\
                                <div class="form-group">\
                                    <label class="col-form-label">Lugar de destino</label>\
                                    <div class="controls">\
                                        <input type="text" id="lugar" class="form-control form-control-smr" value="">\
                                    </div>\
                                </div>\
                            </td>\
                            \
                            <td width="15%">\
                                <div class="form-group">\
                                <label class="col-form-label">Fecha</label>\
                                <div class="controls">\
                                    <input type="date" id="fecha" class="form-control form-control-sm" value="">\
                                </div>\
                                </div>\
                            </td>\
                            \
                            <td width="15%">\
                                <div class="form-group">\
                                <label class="col-form-label">Hora</label>\
                                <div class="controls">\
                                    <input type="time" id="hora" class="form-control form-control-sm" value="">\
                                </div>\
                                </div>\
                            </td>\
                            \
                            <td width="20%">\
                                <div class="form-group">\
                                <label class="col-form-label">Lugar de hospedaje</label>\
                                <div class="controls">\
                                    <input type="text" id="lugar_hospeda" class="form-control form-control-sm" value="">\
                                </div>\
                                </div>\
                            </td>\
                            \
                            <td width="10%">\
                                <span style="color: transparent;">add</span>\
                                <button type="button" class="btn btn-primary" title="Agregar nuevo destino" onclick="addDestinoCotiza('+idEach+','+1+')"><i id="btn_plus" class="fa fa-plus" aria-hidden="true"></i></button>\
                            </td>\
                            \
                            <td width="10%"></td>\
                        </tr>\
                    </tbody>\
                </table>\
            </td>\
        </tr>';

    if(firstElement){
        $('#tbody_unidades').append(htmlT);
    }

    $('#tbody_unidades').append(html);

    $('#tr_unidades_each_'+idEach+' #unidades').replaceWith(clonSelect);
    $('#tr_unidades_each_'+idEach+' #unidad_d_'+idEach).select2({width: '100%'});
    $('#tr_unidades_each_'+idEach+' #unidad_d_'+idEach).val(unidad).trigger('change');


    if(a_destinos.length > 0) {
        a_destinos.forEach(function(element){
            tabla_destinos(idEach, element.id, element.lugar, element.fecha, element.hora, element.lugar_hospeda);
        });
    }
}


function  tabla_destinos(idRow, id_each, lugar_destino, fecha, hora, lugar_hospeda){
	console.log("----- Genera Row U Edicion "+id_each+" -----");

    var html = '<tr id="tr_destinos_each_'+id_each+'" class="tr_destinos">\
                    <td width="10%">\
                    \
                    <input type="hidden" value="tr_destinos_each_'+id_each+'" readonly>\
                    \
                    </td>\
                    <td width="20%">\
                        <input type="hidden" id="id" value='+id_each+'">\
                        <div class="form-group">\
                            <label class="col-form-label">Lugar de destino</label>\
                            <div class="controls">\
                                <input type="text" id="lugar" class="form-control form-control-smr" value="'+lugar_destino+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="15%">\
                        <div class="form-group">\
                            <label class="col-form-label">Fecha</label>\
                            <div class="controls">\
                                <input type="date" id="fecha" class="form-control form-control-sm" value="'+fecha+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="15%">\
                        <div class="form-group">\
                            <label class="col-form-label">Hora</label>\
                            <div class="controls">\
                                <input type="time" id="hora" class="form-control form-control-sm" value="'+hora+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="20%">\
                        <div class="form-group">\
                            <label class="col-form-label">Lugar de hospedaje</label>\
                            <div class="controls">\
                                <input type="text" id="lugar_hospeda" class="form-control form-control-sm" value="'+lugar_hospeda+'">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="10%">\
                        <span class="required" style="color: transparent;">dele</span>\
                        <a class="btn btn-danger" title="Eliminar destino" onclick="delDestinoCotiza('+id_each+')"><i class="fa fa-minus" aria-hidden="true"></i></a>\
                    </td>\
                    <td width="10%"></td>\
                </tr>';

    $('#tbody_unidades  #tbody_unidad_each_destinos_'+idRow).append(html);

}



function add_form(){
	var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre: {
                required: true
            },
            app: {
                required: true
            },
            fecha_salida:{
                required: true
            },
            hora_salida: {
                required: true
            },
            fecha_regreso: {
                required: true
            },
            hora_regreso: {
                required: true
            },
            telefono: {
                required: true
            },
            correo: {
                required: true
            },
            rfc: {
                required: false,
                rfc: true
            },
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        invalidHandler: function(event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);
        },
        highlight: function(element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function(element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function(label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form_data").valid();
    if ($valid) {
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type: 'POST',
            url: base_url+'Clientes/guardar',
            data: datos,
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
                var array_data = $.parseJSON(data);
                //console.log("id extras:"+array_data.id_ext);

                $("#idcliente_ext").val(array_data.id_ext);
                $("#idcliente").val(array_data.id_reg);
                
                //addUnidades(array_data.id_reg);
                
                if($("#identifica").val()!=""){
                    $('#identifica').fileinput('upload');
                }
                if($("#comprobante").val()!=""){
                    $('#comprobante').fileinput('upload');
                }

                swal("Éxito", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    window.location = base_url+'Clientes';
                }, 1500);
            }
        });           
    }
}



function addUnidades(id){
    var capacidad = [' ',4,6,10,14,20,33,47,50];
    var DATA = [];

    var TABLAU = $("#table_unidades tbody > tr");//.tr_destinos
    var i = 0;
    var insert = false;

    TABLAU.each(function() {
        item = {};

        if($(this).hasClass("tr_unidades")) {
            //item["id_prospecto"] = $('#id_Client').val();
            item["id_prospecto"] = id;

            item["id"] = $(this).find("input[id*='id']").val();
            item["unidad"] = $(this).find("select[id*='unidad'] option:selected").val();
            let tipo = $(this).find("select[id*='unidad'] option:selected").data('tipo');
            item["cantidad"] = $(this).find("input[id*='cantidad']").val();
            item["monto"] = $(this).find("input[id*='monto']").val();
            if(tipo == '' || tipo > capacidad.length){
                tipo = 0;
            }
            item["cant_pasajeros"] = capacidad[tipo];

            item["DESTINOS"] = [];


            if ($(this).find("select[id*='unidad'] option:selected").val() != "0") {
                DATA.push(item);
                insert = true;
                i++;
            }

        }else if($(this).hasClass("tr_destinos")) {

            if(!insert){
                insert = false;
                return;
            }

            var TABLAD = $(this).find("#table_destinos tbody > tr");//.tr_unidades
            TABLAD.each(function() {
                console.log("Entra Foreach Unidades");
                itemD = {};
                itemD["id"] = $(this).find("input[id*='id']").val();
                itemD["lugar"] = $(this).find("input[id*='lugar']").val();
                itemD["fecha"] = $(this).find("input[id*='fecha']").val();
                itemD["hora"] = $(this).find("input[id*='hora']").val();
                itemD["lugar_hospeda"] = $(this).find("input[id*='lugar_hospeda']").val();

                
                if ($(this).find("input[id*='lugar']").val() != "") {
                    DATA[i-1]["DESTINOS"].push(itemD);
                }
            });
            
            insert = false;
        }
    });
    
    INFO = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    console.log('Unidades: '+aInfo);

    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url + 'Clientes/insertUnidades',
        processData: false,
        contentType: false,
        success: function(data) {
            //console.log(data);
        }
    });
}


//---Unidad--------------------------------------
function addUnidadCotiza(){
    contUnidadCot++;
    var clonSelect = $("#sel_unidad").clone();
    clonSelect.attr('id', 'unidad_u_'+contUnidadCot);

    var html = '<tr id="tr_unidades_'+contUnidadCot+'" class="tr_unidades">\
                    <td width="40%">\
                        <input type="hidden" id="id" value="0">\
                        <input type="hidden" value="tr_unidades_'+contUnidadCot+'" readonly>\
                        <div class="form-group">\
                            <label class="col-form-label">Unidad</label>\
                            <div class="controls">\
                                <select class="form-control form-control-smr" id="unidades">\
                                </select>\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="25%">\
                        <div class="form-group">\
                            <label class="col-form-label">Cantidad</label>\
                            <div class="controls">\
                                <input type="number" id="cantidad" class="form-control form-control-sm" min="0">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="25%">\
                        <div class="form-group">\
                            <label class="col-form-label">Monto unitario $</label>\
                            <div class="controls">\
                                <input type="text" id="monto" class="form-control form-control-sm" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="10%" colspan="2">\
                        <span style="color: transparent;">add</span>\
                        <button type="button" class="btn btn-danger" title="Eliminar unidad" onclick="eliminarAddUnidad('+contUnidadCot+')"><i class="fa fa-minus" aria-hidden="true"></i></button>\
                    </td>\
                </tr>\
                <!---destinos-->\
                <tr id="tr_unidades_space_'+contUnidadCot+'"></tr>\
                <tr id="tr_unidades_destinos_'+contUnidadCot+'" class="tr_destinos">\
                \
                <td colspan="4">\
                    <table id="table_destinos" width="100%">\
                    <tbody id="tbody_unidad_destinos_'+contUnidadCot+'">\
                        <tr id="tr_destinos" class="tr_destinos">\
                            <td width="10%">\
                                \
                                <input type="hidden" value="tbody_uni_dest_'+contUnidadCot+'" readonly>\
                                <input type="hidden" value="tr_destinos" readonly>\
                                \
                            </td>\
                            \
                            <td width="20%">\
                                <input type="hidden" id="id" value="0">\
                                <div class="form-group">\
                                    <label class="col-form-label">Lugar de destino</label>\
                                    <div class="controls">\
                                        <input type="text" id="lugar" class="form-control form-control-smr" value="">\
                                    </div>\
                                </div>\
                            </td>\
                            \
                            <td width="15%">\
                                <div class="form-group">\
                                <label class="col-form-label">Fecha</label>\
                                <div class="controls">\
                                    <input type="date" id="fecha" class="form-control form-control-sm" value="">\
                                </div>\
                                </div>\
                            </td>\
                            \
                            <td width="15%">\
                                <div class="form-group">\
                                <label class="col-form-label">Hora</label>\
                                <div class="controls">\
                                    <input type="time" id="hora" class="form-control form-control-sm" value="">\
                                </div>\
                                </div>\
                            </td>\
                            \
                            <td width="20%">\
                                <div class="form-group">\
                                <label class="col-form-label">Lugar de hospedaje</label>\
                                <div class="controls">\
                                    <input type="text" id="lugar_hospeda" class="form-control form-control-sm" value="">\
                                </div>\
                                </div>\
                            </td>\
                            \
                            <td width="10%">\
                                <span style="color: transparent;">add</span>\
                                <button type="button" class="btn btn-primary" title="Agregar nuevo destino" onclick="addDestinoCotiza('+contUnidadCot+')"><i id="btn_plus" class="fa fa-plus" aria-hidden="true"></i></button>\
                            </td>\
                            \
                            <td width="10%"></td>\
                        </tr>\
                    </tbody>\
                </table>\
            </td>\
        </tr>';

    $('#tbody_unidades').append(html);
    $('#tr_unidades_'+contUnidadCot+' #unidades').replaceWith(clonSelect);
    $('#tr_unidades_'+contUnidadCot+' #unidad_u_'+contUnidadCot).select2({width: '100%'});
}

function eliminarAddUnidad(row_id) {
    //contUnidadCot--;
    $('#tr_unidades_' + row_id).remove();
    $('#tr_unidades_space_' + row_id).remove();
    $('#tr_unidades_destinos_' + row_id).remove();
}

function delUnidadCotiza(id) {
    console.log('eliminar');
    console.log(id);
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar esta Unidad?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Clientes/delete",
                    data: {
                        id:id, tabla:"unidad_prospecto"
                    },
                    success:function(response){
                        $('#tr_unidades_each_' + id).remove();
                        $('#tr_unidades_each_space_' + id).remove();
                        $('#tr_unidades_each_destinos_' + id).remove();

                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}


//---Destino--------------------------------------
function addDestinoCotiza(row = 0, each = 0){
    contDestinoCot++;
    each = each == 0 ? '':'each_';

    var html = '<tr id="tr_destinos_'+contDestinoCot+'" class="tr_destinos">\
                    <td width="10%">\
                    \
                    <input type="hidden" value="tr_destinos_'+contDestinoCot+'" readonly>\
                    \
                    </td>\
                    <td width="20%">\
                        <input type="hidden" id="id" value="0">\
                        <div class="form-group">\
                            <label class="col-form-label">Lugar de destino</label>\
                            <div class="controls">\
                                <input type="text" id="lugar" class="form-control form-control-smr" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="15%">\
                        <div class="form-group">\
                            <label class="col-form-label">Fecha</label>\
                            <div class="controls">\
                                <input type="date" id="fecha" class="form-control form-control-sm" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="15%">\
                        <div class="form-group">\
                            <label class="col-form-label">Hora</label>\
                            <div class="controls">\
                                <input type="time" id="hora" class="form-control form-control-sm" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="20%">\
                        <div class="form-group">\
                            <label class="col-form-label">Lugar de hospedaje</label>\
                            <div class="controls">\
                                <input type="text" id="lugar_hospeda" class="form-control form-control-sm" value="">\
                            </div>\
                        </div>\
                    </td>\
                    \
                    <td width="10%">\
                        <span style="color: transparent;">add</span>\
                        <button type="button" class="btn btn-danger" title="Eliminar destino" onclick="eliminarAddDestino('+contDestinoCot+')"><i class="fa fa-minus" aria-hidden="true"></i></button>\
                    </td>\
                    <td width="10%"></td>\
                </tr>';
    
    $('#tbody_unidades  #tbody_unidad_'+each+'destinos_'+row).append(html);
}


function eliminarAddDestino(row_id) {
    //contDestinoCot--;
    $('#tr_destinos_' + row_id).remove();
}

function delDestinoCotiza(id) {
    console.log('eliminar');
    console.log(id);
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este destino?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Clientes/delete",
                    data: {
                        id:id, tabla:"destino_prospecto"
                    },
                    success:function(response){ 

                        $('#tr_destinos_each_' + id).remove();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
