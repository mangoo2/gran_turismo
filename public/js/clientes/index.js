var base_url = $('#base_url').val();
var tabla; var id_cli_table=0;  

var contu = 0;

$(document).ready(function() {
    loadtable();
});

function loadtable(){
    tabla=$("#table_data").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        destroy:true,
        responsive: !0,
        "ajax": {
            "url": base_url+"Clientes/getlistado",
            type: "post",
        },
        "columns": [
            {"data":"id"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';  
                    html=row.nombre+" "+row.app+" "+row.apm;
                return html;
                }
            },
            {"data":"telefono"},
            {"data":"correo"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<a title="Eliminar" onclick="delete_data('+row.id+')"><i class="fa fa-trash-o" style="font-size: 20px; cursor: pointer;"></i></a> ';
                    html+='<a title="Editar"  href="'+base_url+'Clientes/registro/'+row.id+'"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
                    //html += '<a target="_blank" title="Generar contrato" href="'+base_url+'Clientes/contrato/'+row.id+'"><i class="fa fa-file-pdf-o icon_font" style="font-size: 20px;"></i></a>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        fixedColumns: true,
        language: languageTables
    });
}

function delete_data(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Clientes/delete",
                    data: {
                        id:id, tabla:"prospectos"
                    },
                    success:function(response){  
                        loadtable();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

//---------------------------------------------------->>>>>

function modal_contrato(id,contrato){
    $('#modal_contrato').modal("show");
    $('#id_prospecto').val(id);
    //$('#tipo_unidad').val(0);
    //$('#tipo_unidad').addClass('bg-danger');

    $('#tbody_unidades').find('tr.tr_get').remove();
    get_unidades_contrato(id);

    if(contrato > 0){
        //$('#modal_contrato').modal("show");
        console.log("id");console.log(id);
        $.ajax({
            type: 'POST',
            url: base_url+'Clientes/get_data_contrato',
            data: {id: id},
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
                console.log('---data--- ' + data);
                const objeto = JSON.parse(data);
                $('#id_detalle').val(objeto.id);
                $('#calle').val(objeto.calle);
                $('#colonia').val(objeto.colonia);
                $('#cp').val(objeto.cp);
                $('#ciudad').val(objeto.ciudad_municipio);
                $('#estado').val(objeto.estado);
                $('#tipo_servicio').val(objeto.tipo_servicio);
                $('#observaciones').val(objeto.observaciones);
            }
        });      
    }
	
}

function add_form(){
	var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            calle: {
                required: true
            },
            colonia: {
                required: true
            },
            cp: {
                required: true,
                digits: true,
                maxlength: 5
            },
            ciudad_municipio: {
                required: true
            },
            estado: {
                required: true
            },
            tipo_servicio: {
                required: true
            },
            num_pasajeros: {
                required: true,
                digits: true,
            },
            importe_unidad: {
                required: true,
                digits: true,
            },
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        invalidHandler: function(event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);
        },
        highlight: function(element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function(element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function(label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });

    var $valid = $("#form_data").valid();
    if ($valid) {
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type: 'POST',
            url: base_url+'Clientes/confirmar_contrato',
            data: datos,
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
                //console.log(data);
                addUnidades($('#id_prospecto').val());
                swal("Éxito", "Guardado Correctamente", "success");
                $('#modal_contrato').modal('hide');
                loadtable();
            }
        });           
    }
}







//---Unidad--------------------------------------
var cont_ant_u = 0;
function addUnidad(){
    contu++;
    clone = $("#tr_unidades").clone().attr('id', "unidades_form_" + contu);
    clone.find("input").val("");
    clone.find("button").attr("onclick", "eliminarAddU(" + contu + ")").removeClass("btn-success").addClass("btn-danger");
    clone.find("#btn_plus").removeClass("fa fa-plus").addClass("fa fa-minus");

    // Obtener todos los elementos con la clase 
    const elementos = document.querySelectorAll(".lastBtn");

    // Recorrer la lista de elementos y quitar la clase 
    for (let i = 0; i < elementos.length; i++) {
        elementos[i].classList.remove("lastBtn");
    }

    //Agregamos la clase al elemento actual
    clone.addClass("lastBtn");
    //console.log("contu: "+contu);
    if (contu == 0  || contu==1) {
        clone.insertAfter("#tr_unidades");
        //console.log("Antes");
    } else {
        cont_ant_u = contu - 1;
        clone.insertAfter("#unidades_form_" + cont_ant_u + "");
        //console.log("Despues");
    }
    cont_ant_u = contu;
}

function eliminarAddU(row_id) {
    const miElemento = document.getElementById('unidades_form_' + row_id);
    contu--;
    cont_ant_u--;
    $('#unidades_form_' + row_id).remove();
}

function eliminarUnit(id) {
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar esta unidad?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Clientes/delete",
                    data: {
                        id:id, tabla:"unidad_prospecto"
                    },
                    success:function(response){  
                        $('#tr_each_uni_' + id).remove();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function addUnidades(id){
    console.log("xxx");
    var DATA = [];
    var TABLA = $("#table_unidades tbody > tr");

    TABLA.each(function() {
        item = {};
        item["id_prospecto"] = id;
        item["id"] = $(this).find("input[id*='id']").val();
        item["unidad"] = $(this).find("option:selected").val();
        item["cantidad"] = $(this).find("input[id*='cantidad']").val();
        item["cant_pasajeros"] = $(this).find("input[id*='cant_pasajeros']").val();
        item["monto"] = $(this).find("input[id*='monto']").val();
        if ($(this).find("option:selected").val() != "0") {
            DATA.push(item);
        }
    });
    
    INFO = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    console.log('Unidades: '+aInfo);
    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url + 'Clientes/insertUnidad',
        processData: false,
        contentType: false,
        success: function(data) {
            //console.log(data);
            $('#cantidad').val('');
            $('#cant_pasajeros').val('');
            $('#monto').val('');
            $('#tipo_unidad').val('0');
        }
    });
}


function get_unidades_contrato(id){
    $.ajax({
        type: 'POST',
        url: base_url+'Clientes/get_unidades_contrato',
        data: {id: id},
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data){
            console.log('data::: ' + data);
            var array = $.parseJSON(data);
            
            $('#tbody_unidades').find('tr.tr_get').remove();

            if(array.length > 0) {
                array.forEach(function(element){
                //IdUnidad, cantidadUnidad, costoUnidad
                get_table_unidad(element.id, element.cantidad, element.monto, element.cant_pasajeros, element.unidad);
            });
        }
    },
    error: function(response){
        toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
    }
});
}


function get_table_unidad(id,cantidad,monto,cant_pasajeros,seleccion){
    console.log("ID: "+id);
    console.log("Cantidad: "+cantidad);
    console.log("Monto: "+monto);
    clonSelect = $("#tipo_unidad").clone();
    clonSelect.attr('id', 'unidades_'+id);

    var html='<tr id="tr_each_uni_'+id+'" class="tr_get">\
                <td width="30%">\
                    <input type="hidden" id="id" value="'+id+'">\
                    <div class="form-group">\
                        <label class="col-form-label"><br>Unidad</label>\
                        <div class="controls">\
                            <select class="form-control form-control-smr" id="unidades">\
                            </select>\
                        </div>\
                    </div>\
                </td>\
                <td width="20%">\
                    <div class="form-group">\
                        <label class="col-form-label"><br>Cantidad (unidades)</label>\
                        <div class="controls">\
                            <input type="number" id="cantidad" class="form-control form-control-sm" min="0" value="'+cantidad+'">\
                        </div>\
                    </div>\
                </td>\
                \
                <td width="20%">\
                    <div class="form-group">\
                        <label class="col-form-label">Cantidad (pasajeros por unidad)</label>\
                        <div class="controls">\
                            <input type="number" id="cant_pasajeros" class="form-control form-control-sm" min="0"  value="'+cant_pasajeros+'">\
                        </div>\
                    </div>\
                </td>\
                \
                <td width="20%">\
                    <div class="form-group">\
                        <label class="col-form-label">Importe por unidad $</label>\
                        <div class="controls">\
                            <input type="text" id="monto" class="form-control form-control-sm" value="'+monto+'">\
                        </div>\
                    </div>\
                </td>\
                <td width="10%" class="px-4">\
                    <a class="btn btn-danger" title="Eliminar contacto" onclick="eliminarUnit('+id+')"><i class="fa fa-minus" aria-hidden="true"></i></a>\
                </td>\
			</tr>';

    $('#tbody_unidades').append(html);

    $('#tr_each_uni_'+id+' #unidades').replaceWith(clonSelect);
    $('#unidades_'+id).val(seleccion);
}
