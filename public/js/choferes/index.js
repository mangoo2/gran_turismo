var base_url = $('#base_url').val();

$(document).ready(function () {
	load_table();
});

function load_table() {
	tabla = $("#table_data").DataTable({
		"bProcessing": true,
		"serverSide": true,
		"searching": true,
		responsive: !0,
		"ajax": {
			"url": base_url + "Choferes/get_list",
			type: "post",
		},
		"columns": [{
				"data": "choferId"
			},
			{
				"data": null,
				"render": function (row) {
					var html = '';
					html = row.nombre + " " + row.apellido_p + " " + row.apellido_m;
					return html;
				}
			},
			{
				"data": "telefono"
			},
			{
				"data": "fecha_ingreso"
			},
			{
				"data": null,
				"render": function (row) {
					var html = '<a title="Eliminar" onclick="delete_chofer(' + row.choferId + ')"><i class="fa fa-trash-o" style="font-size: 20px; cursor: pointer;"></i></a> ';
					html += ' <a title="Editar"  href="' + base_url + 'Choferes/registro/' + row.choferId + '"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
					return html;
				}
			},
		],
		"order": [
			[0, "asc"]
		],
		"lengthMenu": [
			[10, 25, 50],
			[10, 25, 50]
		],
		fixedColumns: true,
		language: languageTables
	});
}

function delete_chofer(id) {
	$.confirm({
		boxWidth: '30%',
		useBootstrap: false,
		icon: 'fa fa-warning',
		title: '¡Atención!',
		content: '¿Está seguro de eliminar este registro?',
		type: 'red',
		typeAnimated: true,
		buttons: {
			confirmar: function () {
				$.ajax({
					type: 'POST',
					url: base_url + "Choferes/delete",
					data: {
						id: id
					},
					success: function (response) {
            tabla.destroy();
						load_table();
						swal("Éxito", "Se ha eliminado correctamente", "success");
					}
				});
			},
			cancelar: function () {

			}
		}
	});
}
