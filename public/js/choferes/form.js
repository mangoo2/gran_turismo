var base_url = $('#base_url').val();
var choferId;

$(document).ready(function () {

	var img_file=""; var imgdet=""; var typePDF="";
    if($("#comp_aux").val()!=""){
        img_file=''+base_url+"public/uploads/choferes/documentos/"+$("#comp_aux").val()+'';
        ext = $("#comp_aux").val().split('.');
        if(ext[1]!="pdf"){ 
            imgdet = {type:"image", url: ""+base_url+"Choferes/delete_document/"+$("#idDocComp").val(), caption: $("#comp_aux").val()}
            typePDF = "false";  
        }else{
            imgdet = {type:"pdf", url: base_url+"Choferes/delete_document/"+$("#idDocComp").val(), caption: $("#comp_aux").val()};
            typePDF = "true";
        }
    }

	$('#comprobante').fileinput({
		showCaption: false, // Ocultar titulo
		showUpload: false, // Ocultar el botón "Upload File"

		allowedFileExtensions: ["png", "jpg", "jpeg", "bmp", "pdf"],
		browseLabel: 'Seleccionar comprobante de domicilio',
		uploadUrl: base_url + 'Choferes/cargar_documentos',
		maxFilePreviewSize: 5000,
		elErrorContainer: '#kv-avatar-errors-1',
		msgErrorClass: 'alert alert-block alert-danger',
		initialPreview: [
			''+img_file+'',
		],
		initialPreviewAsData: typePDF,
		initialPreviewFileType: 'image', // image is the default and can be overridden in config below
		initialPreviewConfig: [
			imgdet
		],
		previewFileIconSettings: {
			'png': '<i class="fa fa-file-photo-o text-warning"></i>',
      'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
      'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
      'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
      'pdf': '<i class="fa fa-pdf-o text-warning"></i>'
		},
		uploadExtraData: function (previewId, index) {
			var info = {
				filetipo: 1,
				id_chofer: choferId,
				vigencia_exa: 0,
				vigencia_lic: 0,
				tipo_lic: 0
			};
			return info;
		}
	}).on('fileuploaded', function(event, files, extra) {
		//location.reload();
	}).on('filedeleted', function(event, files, extra) {
		//location.reload();
		$('#comprobante').inputfile("reset");
	});

	/////////////////////////////////////////////////////////////////////////

	var img_file2=""; var imgdet2=""; var typePDF2="";
    if($("#iden_aux").val()!=""){
        img_file2=''+base_url+"public/uploads/choferes/documentos/"+$("#iden_aux").val()+'';
        ext2 = $("#iden_aux").val().split('.');
        if(ext2[1]!="pdf"){ 
            imgdet2 = {type:"image", url: ""+base_url+"Choferes/delete_document/"+$("#idDocIden").val(), caption: $("#iden_aux").val()}
            typePDF2 = "false";  
        }else{
            imgdet2 = {type:"pdf", url: base_url+"Choferes/delete_document/"+$("#idDocIden").val(), caption: $("#iden_aux").val()};
            typePDF2 = "true";
        }
    }
		
	$('#identificacion').fileinput({
		showCaption: false, // Ocultar titulo
		showUpload: false, // Ocultar el botón "Upload File"

		allowedFileExtensions: ["png", "jpg", "jpeg", "bmp", "pdf"],
		browseLabel: 'Seleccionar identificación',
		uploadUrl: base_url + 'Choferes/cargar_documentos',
		maxFilePreviewSize: 5000,
		elErrorContainer: '#kv-avatar-errors-1',
		msgErrorClass: 'alert alert-block alert-danger',
		initialPreview: [
			''+img_file2+'',
		],
		initialPreviewAsData: typePDF2,
		initialPreviewFileType: 'image', // image is the default and can be overridden in config below
		initialPreviewConfig: [
			imgdet2
		],
		previewFileIconSettings: {
			'png': '<i class="fa fa-file-photo-o text-warning"></i>',
      'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
      'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
      'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
      'pdf': '<i class="fa fa-pdf-o text-warning"></i>'
		},
		uploadExtraData: function (previewId, index) {
			var info = {
				filetipo: 2,
				id_chofer: choferId,
				vigencia_exa: 0,
				vigencia_lic: 0,
				tipo_lic: 0
			};
			return info;
		}
	}).on('fileuploaded', function(event, files, extra) {
		//location.reload();
	}).on('filedeleted', function(event, files, extra) {
		//location.reload();
	});

	
	/////////////////////////////////////////////////////////////////////////

	var img_file3=""; var imgdet3=""; var typePDF3="";
    if($("#exa_aux").val()!=""){
        img_file3=''+base_url+"public/uploads/choferes/documentos/"+$("#exa_aux").val()+'';
        ext3 = $("#exa_aux").val().split('.');
        if(ext3[1]!="pdf"){ 
            imgdet3 = {type:"image", url: ""+base_url+"Choferes/delete_document/"+$("#idDocExa").val(), caption: $("#exa_aux").val()}
            typePDF3 = "false";  
        }else{
            imgdet3 = {type:"pdf", url: base_url+"Choferes/delete_document/"+$("#idDocExa").val(), caption: $("#exa_aux").val()};
            typePDF3 = "true";
        }
    }
		
	$('#examen').fileinput({
		showCaption: false, // Ocultar titulo
		showUpload: false, // Ocultar el botón "Upload File"

		allowedFileExtensions: ["png", "jpg", "jpeg", "bmp", "pdf"],
		browseLabel: 'Seleccionar examen medico',
		uploadUrl: base_url + 'Choferes/cargar_documentos',
		maxFilePreviewSize: 5000,
		elErrorContainer: '#kv-avatar-errors-1',
		msgErrorClass: 'alert alert-block alert-danger',
		initialPreview: [
			''+img_file3+'',
		],
		initialPreviewAsData: typePDF3,
		initialPreviewFileType: 'image', // image is the default and can be overridden in config below
		initialPreviewConfig: [
			imgdet3
		],
		previewFileIconSettings: {
			'png': '<i class="fa fa-file-photo-o text-warning"></i>',
      'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
      'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
      'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
      'pdf': '<i class="fa fa-pdf-o text-warning"></i>'
		},
		uploadExtraData: function (previewId, index) {
			var info = {
				filetipo: 3,
				id_chofer: choferId,
				vigencia_exa: $("#vigencia_exa").val(),
				vigencia_lic: 0,
				tipo_lic: 0

			};
			return info;
		}
	}).on('fileuploaded', function(event, files, extra) {
		//location.reload();
	}).on('filedeleted', function(event, files, extra) {
		//location.reload();
	});

	
	/////////////////////////////////////////////////////////////////////////

	var img_file4=""; var imgdet4=""; var typePDF4="";
    if($("#lic_aux").val()!=""){
        img_file4=''+base_url+"public/uploads/choferes/documentos/"+$("#lic_aux").val()+'';
        ext4 = $("#lic_aux").val().split('.');
        if(ext4[1]!="pdf"){ 
            imgdet4 = {type:"image", url: ""+base_url+"Choferes/delete_document/"+$("#idDocLic").val(), caption: $("#lic_aux").val()}
            typePDF4 = "false";  
        }else{
            imgdet4 = {type:"pdf", url: base_url+"Choferes/delete_document/"+$("#idDocLic").val(), caption: $("#lic_aux").val()};
            typePDF4 = "true";
        }
    }
		
	$('#licencia').fileinput({
		showCaption: false, // Ocultar titulo
		showUpload: false, // Ocultar el botón "Upload File"

		allowedFileExtensions: ["png", "jpg", "jpeg", "bmp", "pdf"],
		browseLabel: 'Seleccionar licencia de manejo',
		uploadUrl: base_url + 'Choferes/cargar_documentos',
		maxFilePreviewSize: 5000,
		elErrorContainer: '#kv-avatar-errors-1',
		msgErrorClass: 'alert alert-block alert-danger',
		initialPreview: [
			''+img_file4+'',
		],
		initialPreviewAsData: typePDF4,
		initialPreviewFileType: 'image', // image is the default and can be overridden in config below
		initialPreviewConfig: [
			imgdet4
		],
		previewFileIconSettings: {
			'png': '<i class="fa fa-file-photo-o text-warning"></i>',
      'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
      'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
      'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
      'pdf': '<i class="fa fa-pdf-o text-warning"></i>'
		},
		uploadExtraData: function (previewId, index) {
			var info = {
				filetipo: 4,
				id_chofer: choferId,
				vigencia_lic: $("#vigencia_lic").val(),
				tipo_lic: $("#tipo_lic option:selected").val(),
				vigencia_exa: 0
			};
			return info;
		}
	}).on('fileuploaded', function(event, files, extra) {
		//location.reload();
		console.log("Cargo4");
	}).on('filedeleted', function(event, files, extra) {
		//location.reload();
	});

});

function save_form() {
	var form_register = $('#form_data');
	var error_register = $('.alert-danger', form_register);
	var success_register = $('.alert-success', form_register);
	var $validator1 = form_register.validate({
		errorElement: 'div', //default input error message container
		errorClass: 'vd_red', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules: {
			nombre: {
				required: true
			},
			apellido_p: {
				required: true
			},
			fecha_ingreso: {
				required: true
			},
			telefono: {
				required: true
			},
			vigencia_examen: {
				required: true
			},
			vigencia_licencia: {
				required: true
			},
			tipo_licencia: {
				required: true
			},
			usuario: {
				required: true
			},
			contrasena: {
				required: true
			},
			perfil: {
				required: true
			}
		},
		errorPlacement: function (error, element) {
			if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
				element.parent().append(error);
			} else if (element.parent().hasClass("vd_input-wrapper")) {
				error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		},
		invalidHandler: function (event, validator) { //display error alert on form submit              
			success_register.fadeOut(500);
			error_register.fadeIn(500);
			scrollTo(form_register, -100);
		},
		highlight: function (element) { // hightlight error inputs
			$(element).addClass('vd_bd-red');
			$(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
		},
		unhighlight: function (element) { // revert the change dony by hightlight
			$(element).closest('.control-group').removeClass('error'); // set error class to the control group
		},
		success: function (label, element) {
			label
				.addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
				.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
			$(element).removeClass('vd_bd-red');
		}
	});

	//////////Registro///////////
	var $valid = $("#form_data").valid();

	if ($valid) {
		$('.btn_form').attr('disabled', true);
		var datos = form_register.serialize();
		console.log("datos: " + JSON.stringify(datos));

    var idDocExa = $('#idDocExa').val();
    var idDocLic = $('#idDocLic').val();

		$.ajax({
			type: 'POST',
			url: base_url + 'Personal/check_user',
			data: {
				user: $('#usuario').val(),
				id: $('#idusuario').val()
			},
			success: function (data) {
				if (data == false) {
					//console.log("NO Existe");

					$.ajax({
						type: 'POST',
						url: base_url + 'Choferes/insert',
						data: datos,
						statusCode: {
							404: function (data) {
								swal('Error!', 'No Se encuentra el archivo');
							},
							500: function () {
								swal('Error', '500');
							}
						},
						success: function (data) {
							//console.log(data);
							choferId = parseInt(data);

							if($("#identificacion").val()!=""){
								$('#identificacion').fileinput('upload');
							}

							if($("#comprobante").val()!=""){
								$('#comprobante').fileinput('upload');
							}

							if($("#examen").val()!=""){
								$('#examen').fileinput('upload');
								idDocExa = 0;
							}

							if($("#licencia").val()!=""){
								$('#licencia').fileinput('upload');
								idDocLic = 0;
							}

							update_vigencias(idDocLic, idDocExa);

							swal("Hecho!", "Guardado Correctamente", "success");
							
							setTimeout(function () {
								window.location = base_url + 'Choferes';
							}, 2500);
							
						}
					});
				} else {

					//console.log("Existe");
					swal("Error!", "Intente nuevamente con otro usuario", "warning"); //alerts
					$('.btn_form').attr('disabled', false);
					$('.btn_form').removeClass('disabled');
				}
			}
		});
	}
}

function update_vigencias(idDocLic, idDocExa) {
	$.ajax({
		type: 'POST',
		url: base_url + 'Choferes/update_vigencias',
		data: {
			licencia: idDocLic,
			examen: idDocExa,
			vigencia_exa: $("#vigencia_exa").val(),
			vigencia_lic: $("#vigencia_lic").val(),
			tipo_lic: $("#tipo_lic option:selected").val(),
		},
		statusCode: {
			404: function (data) {
				swal('Error!', 'No Se encuentra el archivo');
			},
			500: function () {
				swal('Error', '500');
			}
		},
		success: function (data) {
			//console.log("Insert: " + data);
		}
	});
}