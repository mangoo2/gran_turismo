var base_url = $('#base_url').val();
var personalId;

$(document).ready(function () {

	var img_file=""; var imgdet=""; var typePDF="";
    if($("#comp_aux").val()!=""){
        img_file=''+base_url+"public/uploads/personal/documentos/"+$("#comp_aux").val()+'';
        ext = $("#comp_aux").val().split('.');
        if(ext[1]!="pdf"){ 
            imgdet = {type:"image", url: ""+base_url+"Personal/delete_document/"+$("#idDocComp").val(), caption: $("#comp_aux").val()}
            typePDF = "false";  
        }else{
            imgdet = {type:"pdf", url: base_url+"Personal/delete_document/"+$("#idDocComp").val(), caption: $("#comp_aux").val()};
            typePDF = "true";
        }
    }

	$('#comprobante').fileinput({
		showCaption: false, // Ocultar titulo
		showUpload: false, // Ocultar el botón "Upload File"

		allowedFileExtensions: ["png", "jpg", "jpeg", "bmp", "pdf"],
		browseLabel: 'Seleccionar comprobante de domicilio',
		uploadUrl: base_url + 'Personal/cargar_documentos',
		maxFilePreviewSize: 5000,
		elErrorContainer: '#kv-avatar-errors-1',
		msgErrorClass: 'alert alert-block alert-danger',
		initialPreview: [
			''+img_file+'',
		],
		initialPreviewAsData: typePDF,
		initialPreviewFileType: 'image', // image is the default and can be overridden in config below
		initialPreviewConfig: [
			imgdet
		],
		previewFileIconSettings: {
			'png': '<i class="fa fa-file-photo-o text-warning"></i>',
      'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
      'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
      'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
      'pdf': '<i class="fa fa-pdf-o text-warning"></i>'
		},
		uploadExtraData: function (previewId, index) {
			var info = {
				filetipo: 1,
				id_personal: personalId,
			};
			return info;
		}
	}).on('fileuploaded', function(event, files, extra) {
		//location.reload();
	}).on('filedeleted', function(event, files, extra) {
		//location.reload();
	});

	/////////////////////////////////////////////////////////////////////////

	var img_file2=""; var imgdet2=""; var typePDF2="";
    if($("#iden_aux").val()!=""){
        img_file2=''+base_url+"public/uploads/personal/documentos/"+$("#iden_aux").val()+'';
        ext2 = $("#iden_aux").val().split('.');
        if(ext2[1]!="pdf"){ 
            imgdet2 = {type:"image", url: ""+base_url+"Personal/delete_document/"+$("#idDocIden").val(), caption: $("#iden_aux").val()}
            typePDF2 = "false";  
        }else{
            imgdet2 = {type:"pdf", url: base_url+"Personal/delete_document/"+$("#idDocIden").val(), caption: $("#iden_aux").val()};
            typePDF2 = "true";
        }
    }
		
	$('#identificacion').fileinput({
		showCaption: false, // Ocultar titulo
		showUpload: false, // Ocultar el botón "Upload File"

		allowedFileExtensions: ["png", "jpg", "jpeg", "bmp", "pdf"],
		browseLabel: 'Seleccionar identificación',
		uploadUrl: base_url + 'Personal/cargar_documentos',
		maxFilePreviewSize: 5000,
		elErrorContainer: '#kv-avatar-errors-1',
		msgErrorClass: 'alert alert-block alert-danger',
		initialPreview: [
			''+img_file2+'',
		],
		initialPreviewAsData: typePDF2,
		initialPreviewFileType: 'image', // image is the default and can be overridden in config below
		initialPreviewConfig: [
			imgdet2
		],
		previewFileIconSettings: {
			'png': '<i class="fa fa-file-photo-o text-warning"></i>',
      'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
      'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
      'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
      'pdf': '<i class="fa fa-pdf-o text-warning"></i>'
		},
		uploadExtraData: function (previewId, index) {
			var info = {
				filetipo: 2,
				id_personal: personalId
			};
			return info;
		}
	}).on('fileuploaded', function(event, files, extra) {
		//location.reload();
	}).on('filedeleted', function(event, files, extra) {
		//location.reload();
	});
});

function save_form() {
	var form_register = $('#form_data');
	var error_register = $('.alert-danger', form_register);
	var success_register = $('.alert-success', form_register);
	var $validator1 = form_register.validate({
		errorElement: 'div', //default input error message container
		errorClass: 'vd_red', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules: {
			nombre: {
				required: true
			},
			apellido_p: {
				required: true
			},
			fecha_ingreso: {
				required: true
			},
			telefono: {
				required: true
			},
			usuario: {
				required: true
			},
			contrasena: {
				required: true
			}
		},
		errorPlacement: function (error, element) {
			if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
				element.parent().append(error);
			} else if (element.parent().hasClass("vd_input-wrapper")) {
				error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		},
		invalidHandler: function (event, validator) { //display error alert on form submit              
			success_register.fadeOut(500);
			error_register.fadeIn(500);
			scrollTo(form_register, -100);
		},
		highlight: function (element) { // hightlight error inputs
			$(element).addClass('vd_bd-red');
			$(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
		},
		unhighlight: function (element) { // revert the change dony by hightlight
			$(element).closest('.control-group').removeClass('error'); // set error class to the control group
		},
		success: function (label, element) {
			label
				.addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
				.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
			$(element).removeClass('vd_bd-red');
		}
	});

	//////////Registro///////////
	var $valid = $("#form_data").valid();

	if ($valid) {
		$('.btn_form').attr('disabled', true);
		var datos = form_register.serialize();
		console.log("datos: " + JSON.stringify(datos));

		$.ajax({
			type: 'POST',
			url: base_url + 'Personal/check_user',
			data: {
				user: $('#usuario').val(),
				id: $('#idusuario').val()
			},
			success: function (data) {
				if (data == false) {
					//console.log("NO Existe");
					$.ajax({
						type: 'POST',
						url: base_url + 'Personal/insert',
						data: datos,
						statusCode: {
							404: function (data) {
								toastr.error('Error!', 'No Se encuentra el archivo');
							},
							500: function () {
								toastr.error('Error', '500');
							}
						},
						success: function (data) {
							//console.log(data);
							personalId = parseInt(data);

							if($("#identificacion").val()!=""){
								$('#identificacion').fileinput('upload');
							}

							if($("#comprobante").val()!=""){
								$('#comprobante').fileinput('upload');
							}

							swal("Hecho!", "Guardado Correctamente", "success");
							
              setTimeout(function () {
                window.location = base_url + 'Personal';
              }, 1500);

						}
					});

				} else {

					//console.log("Existe");
					swal("Error!", "Intente nuevamente con otro usuario", "warning"); //alerts
					$('.btn_form').attr('disabled', false);
					$('.btn_form').removeClass('disabled');
				}
			}
		});
	}
}