var base_url = $('#base_url').val();
var tabla; var id_cli_table=0;  
$(document).ready(function() {
    loadtable();
    $("#export-excel").on("click",function(){
        if($("#fechai").val()!="" && $("#fechaf").val()!=""){
            var fi=$("#fechai").val();
            var ff=$("#fechaf").val();
            window.open(base_url+'Unidades/exportExcelAll/'+fi+'/'+ff, '_blank');
        }else{
            swal("Error", "Indique un parametro de fechas correcto", "warning");  
        }
    });
    $("#export-excel-kms").on("click",function(){
        if($("#fechai").val()!="" && $("#fechaf").val()!=""){
            var fi=$("#fechai").val();
            var ff=$("#fechaf").val();
            window.open(base_url+'Unidades/exportExcelKms/'+fi+'/'+ff, '_blank');
        }else{
            swal("Error", "Indique un parametro de fechas correcto", "warning");  
        }
    });
});

function loadtable(){
    tabla=$("#table_data").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        //responsive: !0,
        "ajax": {
            "url": base_url+"Unidades/getlistado",
            type: "post",
        },
        "columns": [
            {"data":"id"},
            {"data":"num_eco"},
            {"data":"vehiculo"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';  
                    if(row.tipo==1)
                        html="Auto (4 pasajeros)";
                    else if(row.tipo==2)
                        html="Minivan (6 pasajeros)";
                    else if(row.tipo==3)
                        html="Van (10 pasajeros)";
                    else if(row.tipo==4)
                        html="Van (14 pasajeros)";
                    else if(row.tipo==5)
                        html="Van (20 pasajeros)";
                    else if(row.tipo==4)
                        html="Midibus (33 pasajeros)";
                    else if(row.tipo==7)
                        html="Autobus (47 pasajeros)";
                    else if(row.tipo==7)
                        html="Autobus (50 pasajeros)";
                    return html;
                }
            },
            {"data":"placas"},
            {"data":"placas_fed"},
            {"data":"marca"},
            {"data":"modelo"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='<a title="Exportar Verificaciones y Servicios" onclick="export_excel('+row.id+',0)"><i class="fa fa-file-excel-o" style="font-size: 20px; cursor: pointer;"></i></a> ';
                    html+='<a title="Editar" href="'+base_url+'Unidades/registro/'+row.id+'"><i class="fa fa-edit" style="font-size: 20px;"></i></a> ';
                    html+='<a title="Eliminar" onclick="delete_data('+row.id+')"><i class="fa fa-trash-o" style="font-size: 20px; cursor: pointer;"></i></a> ';
                    
                    return html;
                }
            },
        ],
        dom: 'Blfrtip',
        buttons: ['colvis'],
        "order": [[ 0, "asc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        fixedColumns: true,
        language: languageTables
    });

    /*document.querySelectorAll('a.toggle-vis').forEach((el) => {
        el.addEventListener('click', function (e) {
            e.preventDefault();
     
            let columnIdx = e.target.getAttribute('data-column');
            let column = tabla.column(columnIdx);
     
            // Toggle the visibility
            column.visible(!column.visible());
        });
    });*/
}

function delete_data(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Unidades/delete",
                    data: {
                        id:id, tabla:"unidades"
                    },
                    success:function(response){  
                        loadtable();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function export_excel(id,type){
    window.open(base_url+'Unidades/exportExcel/'+id+'/'+type, '_blank');
}