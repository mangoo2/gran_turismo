var base_url = $('#base_url').val();
var cont1 = 0;
var contV = 0;
var unidadId;

$(document).ready(function() {

    var img_unidad=""; var imgunidet="";
    if($("#img_aux").val()!=""){
        img_unidad=''+base_url+"public/uploads/unidades/"+$("#img_aux").val()+'';
        imgunidet = {type:"image", url: ""+base_url+"Unidades/delete_img/"+$("#idcliente").val()+"/1", caption: $("#img_aux").val()}
    }
		
	$('#img').fileinput({
		showCaption: false, // Ocultar titulo
		showUpload: false, // Ocultar el botón "Upload File"
        showClose: false,
		allowedFileExtensions: ["png", "jpg", "jpeg", "bmp"],
		browseLabel: 'Seleccionar imagen',
		uploadUrl: base_url + 'Unidades/cargar_imagen',
		maxFilePreviewSize: 5000,
		elErrorContainer: '#kv-avatar-errors-1',
		msgErrorClass: 'alert alert-block alert-danger',
		initialPreview: [
			''+img_unidad+'',
		],
		initialPreviewAsData: 'false',
		initialPreviewFileType: 'image', // image is the default and can be overridden in config below
		initialPreviewConfig: [
			imgunidet
		],
		previewFileIconSettings: {
			'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
		},
		uploadExtraData: function (previewId, index) {
			var info = {
				id_unidad: unidadId,
                tipo:1
			};
			return info;
		}
	}).on('fileuploaded', function(event, files, extra) {
		//location.reload();
	}).on('filedeleted', function(event, files, extra) {
		//location.reload();
	});

    var img_poliza=""; var imgdetpoliza=""; var typePDF="";
    if($("#img_poliza_aux").val()!=""){
        img_poliza=''+base_url+"public/uploads/unidades/polizas/"+$("#img_poliza_aux").val()+'';
        ext = $("#img_poliza_aux").val().split('.');
        if(ext[1]!="pdf"){
            imgdetpoliza = {type:"image", url: ""+base_url+"Unidades/delete_img/"+$("#idcliente").val()+"/2", caption: $("#img_poliza_aux").val()}
            typePDF = "false";  
        }else{
            imgdetpoliza = {type:"pdf", url: ""+base_url+"Unidades/delete_img/"+$("#idcliente").val()+"/2", caption: $("#img_poliza_aux").val()};
            typePDF = "true";
        }
    }
        
    $('#img_poliza').fileinput({
        showCaption: false, // Ocultar titulo
        showUpload: false, // Ocultar el botón "Upload File"
        showClose: false,
        allowedFileExtensions: ["png", "jpg", "jpeg", "bmp","pdf"],
        browseLabel: 'Seleccionar imagen',
        uploadUrl: base_url + 'Unidades/cargar_imagen',
        maxFilePreviewSize: 5000,
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        initialPreview: [
            ''+img_poliza+'',
        ],
        initialPreviewAsData: typePDF,
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
            imgdetpoliza
        ],
        previewFileIconSettings: {
            'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-pdf-o text-warning"></i>'
        },
        uploadExtraData: function (previewId, index) {
            var info = {
                id_unidad: unidadId,
                tipo:2
            };
            return info;
        }
    }).on('fileuploaded', function(event, files, extra) {
        //location.reload();
    }).on('filedeleted', function(event, files, extra) {
        //location.reload();
    });

    getImgVerifica();
});

function add_form(){
	var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);

    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            num_eco: {
                required: true
            },
            placas: {
                required: true
            },
            marca:{
                required: true
            },
            modelo: {
                required: true
            },
            poliza_seg: {
                required: true
            },
            vencimiento_poliza: {
                required: true
            },
            serie: {
                required: true
            },
            motor: {
                required: true
            },
            rendimiento: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },

        invalidHandler: function(event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);
        },

        highlight: function(element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },

        unhighlight: function(element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function(label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });

    var $valid = $("#form_data").valid();
    if ($valid) {
        ////////////////////
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        //console.log("datos: " + datos);

        $.ajax({
            type: 'POST',
            url: base_url+'Unidades/guardar',
            data: datos,
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
                unidadId = parseInt(data);
                addDestinos(data);
                addVerificaciones(data);
                if($("#img").val()!=""){
                    $('#img').fileinput('upload');
                    //console.log('ID unidad: '+unidadId);
                }
                if($("#img_poliza").val()!=""){
                    $('#img_poliza').fileinput('upload');
                    //console.log('ID unidad para poliza: '+unidadId);
                }
                swal("Éxito", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    window.location = base_url+'Unidades';
                }, 1500);
            }
        });           
    }
}

function addDestinos(id){
    var DATA = [];
    var TABLA = $("#table_servs tbody > tr");

    TABLA.each(function() {
        item = {};
        item["id_unidad"] = id;
        item["id"] = $(this).find("input[id*='id']").val();
        item["fecha"] = $(this).find("input[id*='fecha']").val();
        item["kilometraje"] = $(this).find("input[id*='kilometraje']").val();
        item["tipo"] = $(this).find("select[id*='tipo'] option:selected").val();
        item["importe"] = $(this).find("input[id*='importe']").val();
        item["comentarios"] = $(this).find("textarea[id*='comentarios']").val();
        if ($(this).find("input[id*='fecha']").val() != "") {
            DATA.push(item);
        }
    });
    
    INFO = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url + 'Unidades/insertServicios',
        processData: false,
        contentType: false,
        success: function(data) {
            //console.log(data);
        }
    });
}

var cont_ant = 0;
function addServ(){
    cont1++;
    clone = $("#tr_servs").clone().attr('id', "servs_form_" + cont1);
    clone.find("input").val("");
    clone.find("textarea").val("");
    clone.find("button").attr("onclick", "eliminarAdd(" + cont1 + ")").removeClass("btn-success").addClass("btn-danger");
    clone.find("#btn_plus").removeClass("fa fa-plus").addClass("fa fa-minus");

    // Obtener todos los elementos con la clase 
    const elementos = document.querySelectorAll(".lastBtn");
    // Recorrer la lista de elementos y quitar la clase 
    for (let i = 0; i < elementos.length; i++) {
        elementos[i].classList.remove("lastBtn");
    }

    //Agregamos la clase al elemento actual
    clone.addClass("lastBtn");
    //console.log("cont1: "+cont1);
    if (cont1 == 0  || cont1==1) {
        clone.insertAfter("#tr_servs");
        //console.log("Antes");
    } else {
        cont_ant = cont1 - 1;
        clone.insertAfter("#servs_form_" + cont_ant + "");
        //console.log("Despues");
    }
    cont_ant = cont1;
}

function eliminarAdd(row_id) {
    const miElemento = document.getElementById('servs_form_' + row_id);
    cont1--;
    cont_ant--;
    $('#servs_form_' + row_id).remove();
}

function eliminarServ(row_id) {
    console.log(row_id);
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este servicio?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                console.log("function");
                $.ajax({
                    type:'POST',
                    url: base_url+"Unidades/deleteServicio",
                    data: {id:row_id},
                    success:function(response){
                        $('#tr_each_' + row_id).remove();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function eliminarDest(id) {
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este destino?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Unidades/delete",
                    data: {
                        id:id, tabla:"servicio_unidades"
                    },
                    success:function(response){  
                        $('#tr_each_' + id).remove();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

//Verificacion---------------------

function addVerificaciones(id){
    var DATA = [];
    var TABLA = $("#table_verif tbody > tr");

    TABLA.each(function() {
        item = {};
        item["id_unidad"] = id;
        item["id"] = $(this).find("input[id*='id']").val();
        item["fecha"] = $(this).find("input[id*='fecha']").val();
        item["tipo"] = $(this).find("option:selected").val();
        item["importe"] = $(this).find("input[id*='importe']").val();
        item["observaciones"] = $(this).find("textarea[id*='observaciones']").val();
        if ($(this).find("input[id*='fecha']").val() != "") {
            DATA.push(item);
        }
    });
    
    INFO = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);

    //console.log("verificaciones: " + aInfo);

    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url + 'Unidades/insertVerificaciones',
        processData: false,
        contentType: false,
        success: function(data) {
            //console.log(data);
        }
    });
}

var cont_ant_verif = 0;
function addVerif(){
    contV++;
    clone = $("#tr_verif").clone().attr('id', "verif_form_" + contV);
    clone.find("input").val("");
    clone.find("textarea").val("");
    clone.find("button").attr("onclick", "eliminarAddV(" + contV + ")").removeClass("btn-success").addClass("btn-danger");
    clone.find("#btn_plus").removeClass("fa fa-plus").addClass("fa fa-minus");

    // Obtener todos los elementos con la clase 
    const elementos = document.querySelectorAll(".lastBtn");
    // Recorrer la lista de elementos y quitar la clase 
    for (let i = 0; i < elementos.length; i++) {
        elementos[i].classList.remove("lastBtn");
    }

    //Agregamos la clase al elemento actual
    clone.addClass("lastBtn");
    //console.log("contV: "+contV);
    if (contV == 0  || contV==1) {
        clone.insertAfter("#tr_verif");
        //console.log("Antes");
    } else {
        cont_ant_verif = contV - 1;
        clone.insertAfter("#verif_form_" + cont_ant_verif + "");
        //console.log("Despues");
    }
    cont_ant_verif = contV;
}

function eliminarAddV(row_id) {
    const miElemento = document.getElementById('verif_form_' + row_id);
    contV--;
    cont_ant_verif--;
    $('#verif_form_' + row_id).remove();
}

function eliminarVerif(row_id) {
    //console.log("row_id");
    //console.log(row_id);
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar esta verificación?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                console.log("function");
                $.ajax({
                    type:'POST',
                    url: base_url+"Unidades/deleteVerificacion",
                    data: {id:row_id},
                    success:function(response){
                        $('#tr_each_verif_' + row_id).remove();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function cargarDoc(id_reg,val,id,name,size){
    filezise = size;                     
    if(filezise > 2048000) { // 512000 bytes = 500 Kb
      //console.log($('.img')[0].files[0].size);
      $(this).val('');
      swal("Error!", "El archivo supera el límite de peso permitido", "error");
    }else { //ok
        tama_perm = true; 
        var archivo = val;
        var id = id; //id con nombre de xml o pdf
        var name = name; //id solito - id de movimiento (saldo)
        extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
        extensiones_permitidas = new Array(".jpeg",".png",".jpg",".webp");
        permitida = false;
        if($('#'+id)[0].files.length > 0) {
            for (var i = 0; i < extensiones_permitidas.length; i++) {
                if (extensiones_permitidas[i] == extension) {
                    permitida = true;
                    break;
                }
            }  
            if(permitida==true && tama_perm==true){
              //console.log("tamaño permitido");
              var inputFileImage = document.getElementById(id);
              var file = inputFileImage.files[0];
              var data = new FormData();
              //id_docs = $("#id").val();
              data.append('foto',file);
              $.ajax({
                url:base_url+'Unidades/cargarImgVerificacion/'+id_reg,
                type:'POST',
                contentType:false,
                data:data,
                processData:false,
                cache:false,
                success: function(data) {
                  var array = $.parseJSON(data);
                  //console.log(array.ok);
                  if (array.ok=true) {
                    id_docs = array.id;
                    //console.log("id_docs: "+id_docs);
                    swal("Éxito", "Cargado correctamente", "success");
                  }else{
                    swal("Error!", "No Se encuentra el archivo", "error");
                  }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var data = JSON.parse(jqXHR.responseText);
                }
              });
            }else if(permitida==false){
                swal("Error!", "Tipo de archivo no permitido", "error");
            } 
        }else{
            swal("Error!", "No se a selecionado un archivo", "error");
        } 
    }
}

function getImgVerifica(){
    //console.log("getImgVerifica: ");
    var id=$("#idcliente").val();
    $.ajax({
        type: 'POST',
        url: base_url+'Unidades/get_data_verificaciones',
        data: {id: id},
        async:false,
        success: function(data){
            //console.log(data);
            const objeto = JSON.parse(data);
            //$('#tbody_pagos').empty();
            if(objeto.length > 0) {
                objeto.forEach(element => {
                    tabla_unidades(element.id, element.img_verif, element.fecha, element.tipo, element.importe,element.observaciones);
                    //imgVerificacion(element.id,element.img_verif);
                });
            }  
        }
    });
}

function tabla_unidades(id,img,fecha,tipo,importe,observaciones){
    var html=""; var sel1=""; var sel2="";
    if(tipo=="1"){
        sel1="selected";
    }else if(tipo=="2"){
        sel2="selected";
    }
    html+='<tr id="tr_each_verif_'+id+'">\
                <td width="15%">\
                    <a class="btn btn-flat btn-primary mt-2" title="Carga de evidencia" onclick="addEvidencia('+id+',\'' +img+ '\')"><i class="fa fa-upload" style="font-size: 20px;"></i></a>\
                  </div>\
                </td>\
                <td width="15%">\
                  <input type="hidden" id="id" value="'+id+'">\
                  <div class="form-group">\
                    <label class="col-form-label">Fecha de Verificación</label>\
                    <div class="controls">\
                      <input type="date" id="fecha" class="form-control form-control-smr" value="'+fecha+'">\
                    </div>\
                  </div>\
                </td>\
                <td width="15%">\
                  <div class="form-group">\
                    <label class="col-form-label">Tipo</label>\
                    <div class="controls">\
                      <select class="form-control" id="tipo">\
                        <option '+sel1+' value="1">Físico mecánica </option>\
                        <option '+sel2+' value="2">Contaminantes </option>\
                      </select>\
                    </div>\
                  </div>\
                </td>\
                <td width="15%">\
                  <div class="form-group">\
                    <label class="col-form-label">Importe</label>\
                    <div class="controls">\
                      <input type="number" id="importe" class="form-control form-control-sm" value="'+importe+'">\
                    </div>\
                  </div>\
                </td>\
                <td width="35%">\
                  <div class="form-group">\
                    <label class="col-form-label">Observaciones</label>\
                    <div class="controls">\
                      <textarea type="time" id="observaciones" class="form-control form-control-sm" value="'+observaciones+'">'+observaciones+'</textarea>\
                    </div>\
                  </div>\
                </td>\
                <td width="5%">\
                  <span class="required" style="color: transparent;">dele</span>\
                  <a class="btn btn-danger" title="Eliminar contacto" onclick="eliminarVerif('+id+')"><i class="fa fa-minus" aria-hidden="true"></i></a>\
                </td>\
            </tr>';

    
    $('#body_verif').append(html);
    //setTimeout(function(){ 
        //imgVerificacion(id,img);
    //}, 1000);
}

function imgVerificacion(id,img){
    //type="";}
    //console.log("img: "+img);
    var img2="";
    $.ajax({
        type: 'POST',
        url: base_url+'Unidades/get_data_img',
        data: {id: id},
        //data: { id:$("#idcliente").val() },
        async:false,
        success: function(data){
            img2=data;
            //console.log("data: "+data);
        }
    });
    //console.log("img2: "+img2);
    var prevx="";
    var inix="";
    var defx="";
    if(img!=""){
        prevx="true";
        inix= base_url+"public/uploads/unidades/verificaciones/"+img;
        defx= base_url+'public/uploads/unidades/verificaciones/'+img+'" ';
    }
    //console.log("id: "+id);
    //console.log("inix: "+inix);
    $("#img_verif_"+id).fileinput({
        overwriteInitial: true,
        maxFileSize: 5000,
        showClose: false,
        showCaption: false,
        showZomm: false,
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "jpeg", "webp"],
        initialPreview: [
            ''+inix+'',    
        ],
        initialPreviewAsData: 'false',
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
            {type: "image", url: base_url+"Unidades/deleteImgVerifica/"+id, caption: img, key: id},
        ],
    });
    
}

function export_excel(id,type){
    window.open(base_url+'Unidades/exportExcel/'+id+'/'+type, '_blank');
}

function addEvidencia(id, img){
    
    htmlE = '<input width="50%" onchange="cargarDoc('+id+',this.value,this.id,this.name,this.files[0].size)" class="doc '+id+'" id="img_verif_'+id+'" name="'+id+'" type="file">';

    $('#eviencia_contenedor').empty();

    $('#eviencia_contenedor').append(htmlE);
    imgVerificacion(id,img);
    $('#modal_carga_evidencia').modal("show");
}