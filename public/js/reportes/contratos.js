var base_url = $('#base_url').val();
var tabla; var id_cli_table=0;  
$(document).ready(function() {
    $('#id_cliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un cliente',
        //templateResult: formatState,
        ajax: {
            url: base_url + 'Reportes/search_cliente',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemsCli = [];
                data.forEach(function(element) {
                    itemsCli.push({
                        id: element.id,
                        text: element.nombre +" "+element.app +" "+element.apm
                    });
                });
                return {
                    results: itemsCli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
    });

    $("#export-excel").on("click",function(){
        if($("#fechai").val()!="" && $("#fechaf").val()!="")
            downloadTable();
        else
            swal("Error!", "Indique un parametro de fechas valido", "warning");
    });
});

function downloadTable(tipo){
    var id_cliente=$("#id_cliente option:selected").val(); var fechai=$("#fechai").val(); var fechaf=$("#fechaf").val();
    if(id_cliente==undefined){
        id_cliente=0;
    }
    if(fechai==""){
        fechai=0;
    }if(fechaf==""){
        fechaf=0;
    }
    window.open(base_url+"Reportes/exportReporteContra/"+id_cliente+"/"+fechai+"/"+fechaf, '_blank');
}
