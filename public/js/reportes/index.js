var base_url = $('#base_url').val();
var tabla; var id_cli_table=0;  
$(document).ready(function() {
    $('#id_cliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un cliente o prospecto',
        //templateResult: formatState,
        ajax: {
            url: base_url + 'Reportes/search_cliente',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemsCli = [];
                data.forEach(function(element) {
                    itemsCli.push({
                        id: element.id,
                        text: element.nombre +" "+element.app +" "+element.apm
                    });
                });
                return {
                    results: itemsCli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
    });
    $("#search").on("click",function(){
        loadtable();
    });
    $("#export-excel").on("click",function(){
        downloadTable();
    });
});

function downloadTable(tipo){
    var id_cliente=$("#id_cliente option:selected").val(); var fechai=$("#fechai").val(); var fechaf=$("#fechaf").val();
    if(id_cliente==undefined){
        id_cliente=0;
    }
    if(fechai==""){
        fechai=0;
    }if(fechaf==""){
        fechaf=0;
    }
    window.open(base_url+"Reportes/exportReporte/"+id_cliente+"/"+fechai+"/"+fechaf, '_blank');
}

function loadtable(){
    var id_cliente=$("#id_cliente option:selected").val(); var fechai=$("#fechai").val(); var fechaf=$("#fechaf").val();
    if(id_cliente==undefined){
        id_cliente=0;
    }
    tabla=$("#table_data").DataTable({
        destroy:true,
        "ajax": {
            "url": base_url+"Reportes/getlistaCta",
            type: "post",
            data: { id_cliente:id_cliente,fechai:fechai,fechaf:fechaf },
        },
        "columns": [
            {"data":"cliente"},
            {"data":"id"},
            {"data":"folio"},
            {"data":"fecha_reg"},
            {"data":"fecha_contrato"},
            {"data": null,type: 'formatted-num',
                "render" : function ( url, type, row) {
                    var totals=(row.tot_unids-row.tot_desc);
                    totals = new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(totals);
                    return totals;
                }
            },  
            {"data": null,type: 'formatted-num',
                "render" : function ( url, type, row) {
                    var tot_pays=parseFloat(row.tot_pays).toFixed(2); 
                    var tot_antic=parseFloat(row.tot_antic).toFixed(2);
                    /*console.log("id: "+row.id);
                    console.log("tot_pays: "+tot_pays);
                    console.log("tot_antic: "+tot_antic);*/
                    var totalp=Number(tot_pays) + Number(tot_antic);
                    totals = new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(totalp);
                    return totals;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html=''; 
                    resta=Number(row.tot_unids)-Number(row.tot_desc)-Number(row.tot_pays)-Number(row.tot_antic);
                    restafin = new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(resta);
                    html= restafin;
                    
                    return html;
                }
            },
        ],
        "order": [[ 1, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        fixedColumns: true,
        language: languageTables
    });
}
