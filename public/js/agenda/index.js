var base_url = $('#base_url').val();
var calendar;

const MESES = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
var timezones = [{
	timezoneOffset: -300, // GMT-05:00 (para America/Mexico_City)
	displayLabel: 'GMT-05:00',
	tooltip: 'Ciudad de México'
}];


$(document).ready(function () {
	initializeCalendar();
});


function initializeCalendar() {
	const Calendar = window.tui.Calendar;
	calendar = new Calendar('#calendario', {
		defaultView: 'month',
		isReadOnly: true,
		useCreationPopup: false,
		useDetailPopup: true,
		timezones: timezones,
		calendars: CalendarList,
		month: {
			startDayOfWeek: 1
		},
		week: {
			startDayOfWeek: 1
		}
	});

	calendar.on('clickMore', function (event) {
		//console.log('More events...', event);

		//Rectificar colores texto y bg
		setTimeout(function () {
			$(".tui-full-calendar-weekday-schedule-title").each(function () {
				// Obtener las clases del elemento actual
				var clases = $(this).attr('class').split(' ');

				// Verificar si tiene exclusivamente la clase específica
				if (clases.length === 1 && clases[0] === 'tui-full-calendar-weekday-schedule-title') {
					// Eliminar el atributo de estilo solo si tiene exclusivamente la clase específica
					$(this).removeAttr("style");
				}
			});
		}, 500);

	});


	$.ajax({
		url: base_url + 'Agenda/getDataAgenda',
		method: 'POST', // O el método HTTP apropiado (GET, POST, etc.)
		dataType: 'json', // Esperamos datos en formato JSON

		success: function (data) {
			//console.log("data");
			//console.log(data);
			var schedules = data.map(function (event) {
				//console.log("event :" + JSON.stringify(event));
				//console.log(event.color);
				var textColor = getContrast(event.color);
				//console.log("ID: "+event.id+" "+event.color);
				return {
					id: event.id,
					calendarId: event.id % 10, // usar un ID de calendario para agrupar eventos
					//title: '# '+event.id+' '+event.vehiculo+' - '+event.nombre+' '+event.app+' '+event.apm,
					//title: '# ' + event.id + ' - ' + event.vehiculo,
					//title: event.folio+' - '+event.vehiculo,
                    title: event.vehiculo,
					category: 'time', // Categoría de eventos (por ejemplo, 'time', 'milestone', 'task', 'allday')
					//dueDateClass: '',
					start: event.fecha_salida + 'T' + event.hora_salida,
					end: event.fecha_regreso + 'T' + event.hora_regreso,
					//location: event.lugar_origen,
					body: event.unidades,
					color: textColor, //Color texto
					bgColor: event.color, //Color Fondo
					borderColor: event.color, //Color Borde 
					customStyle: 'background-color: ' + event.color + '; color: ' + textColor,
				};
			});

			//console.log(schedules);

			calendar.clear();
			calendar.createSchedules(schedules);

			//Rectificar colores texto y bg
			setTimeout(function () {
				$(".tui-full-calendar-weekday-schedule-title").each(function () {
					// Obtener las clases del elemento actual
					var clases = $(this).attr('class').split(' ');

					// Verificar si tiene exclusivamente la clase específica
					if (clases.length === 1 && clases[0] === 'tui-full-calendar-weekday-schedule-title') {
						// Eliminar el atributo de estilo solo si tiene exclusivamente la clase específica
						$(this).removeAttr("style");
					}
				});
			}, 500);

		},
		error: function (xhr, status, error) {
			swal("Error!", "Error al obtener los eventos", "error");
		}
	});

	update_date();
}

function toggle_view(type) {
	switch (type) {
		case 'dia':
			calendar.changeView('day');
			break;

		case 'sem':
			calendar.changeView('week');
			break;

		case 'mes':
			calendar.changeView('month');
			break;

		default:
			calendar.changeView('month');
			break;
	}
}

function toggle_date(type) {
	switch (type) {
		case 1:
			calendar.next();
			update_date();
			break;

		case -1:
			calendar.prev();
			update_date();
			break;

		case 0:
			calendar.today();
			update_date();
			break;

		default:
			calendar.today();
			update_date();
			break;
	}

	//Rectificar colores texto y bg
	setTimeout(function () {
		$(".tui-full-calendar-weekday-schedule-title").each(function () {
			// Obtener las clases del elemento actual
			var clases = $(this).attr('class').split(' ');

			// Verificar si tiene exclusivamente la clase específica
			if (clases.length === 1 && clases[0] === 'tui-full-calendar-weekday-schedule-title') {
				// Eliminar el atributo de estilo solo si tiene exclusivamente la clase específica
				$(this).removeAttr("style");
			}
		});
	}, 500);
}

function update_date() {
	let date = "";
	date = MESES[calendar.getDate().getMonth()] + ' ' + calendar.getDate().getFullYear();
	$('#cal-date').text(date);
}

function getContrast(hexcolor) {
	//console.log("HEXA: "+hexcolor);
	// Convertir el color hexadecimal a RGB
	var r = parseInt(hexcolor.substr(1, 2), 16);
	var g = parseInt(hexcolor.substr(3, 2), 16);
	var b = parseInt(hexcolor.substr(5, 2), 16);

	// Calcular el valor de luminosidad (luma)
	var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b;

	// Definir el umbral de contraste (normalmente 128)
	var threshold = 128;

	// Si el color es claro, el texto debe ser oscuro y viceversa
	//return luma < threshold ? '#FFFFFF' : '#000000';
    return luma < threshold ? '#BEBEBE' : '#000000';
}
