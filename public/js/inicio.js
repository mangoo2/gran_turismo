var base_url = $('#base_url').val();

$(document).ready(function() {
  loadtable();
});

function loadtable(){
    tabla=$("#table_data").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        destroy:true,
        responsive: !0,
        "ajax": {
            "url": base_url+"Inicio/getlistado",
            type: "post",
        },
        "columns": [
            //{"data":"choferid"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var notification = '';
                    if(data.tipo == 1){
                        notification = "Licencia por vencer";
                    }else if(data.tipo == 2){
                        notification = "Examen médico por vencer";
                    }else if(data.tipo == 3){
                        notification = "Póliza de seguro por vencer";
                    }else if(data.tipo == 4){
                        notification = "Verificación cercana";
                    }

                return notification;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var type = '';
                    if(data.tipo == 1){
                        type = "Chofer";
                    }else if(data.tipo == 2){
                        type = "Chofer";
                    }else if(data.tipo == 3){
                        type = "Unidad";
                    }else if(data.tipo == 4){
                        type = "Unidad";
                    }
                return type;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    if(data.tipo == 1){
                        html = data.nombre+" "+row.apellido_p+" "+row.apellido_m;
                    }else if(data.tipo == 2){
                        html = data.nombre+" "+row.apellido_p+" "+row.apellido_m;
                    }else if(data.tipo == 3){
                        html = data.vehiculo;
                    }else if(data.tipo == 4){
                        html = data.vehiculo;
                    }
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    if(data.tipo == 1){
                        html = data.vigencia_licencia;
                    }else if(data.tipo == 2){
                        html = data.vigencia_examen;
                    }else if(data.tipo == 3){
                        html = data.vencimiento_poliza;
                    }else if(data.tipo == 4){
                        html = data.prox_ver;
                    }
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                    if(data.tipo == 1){
                        html = '<a title="Editar" href="'+base_url+'Choferes/registro/'+data.choferid+'"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
                    }else if(data.tipo == 2){
                        html = '<a title="Editar" href="'+base_url+'Choferes/registro/'+data.choferid+'"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
                    }else if(data.tipo == 3){
                        html = '<a title="Editar" href="'+base_url+'Unidades/registro/'+data.id+'"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
                    }else if(data.tipo == 4){
                        html = '<a title="Editar" href="'+base_url+'Unidades/registro/'+data.id+'"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
                    }
                return html;
                }
            },

        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        fixedColumns: true,
        language: languageTables
    });
}