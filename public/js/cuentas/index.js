var base_url = $('#base_url').val();
$(document).ready(function() {
    loadtable();
});

function loadtable(){
    tabla=$("#table_data").DataTable({
        //"bProcessing": true,
        //"serverSide": true,
        //"searching": true,
        //responsive: !0,
        "ajax": {
            "url": base_url+"Cuentas/getlistado",
            type: "post",
        },
        "columns": [
            {"data":"id"},
            {"data":"cuenta"},
            {"data":"clabe"},
            {"data":"banco"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='<a title="Eliminar" onclick="delete_data('+row.id+')"><i class="fa fa-trash-o" style="font-size: 20px; cursor: pointer;"></i></a> ';
                        html+='<a title="Editar" href="'+base_url+'Cuentas/registro/'+row.id+'"><i class="fa fa-edit" style="font-size: 20px;"></i></a> ';
                    return html;
                }
            },
        ],
        "order": [[ 0, "asc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        fixedColumns: true,
        language: languageTables
    });
}

function delete_data(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Cuentas/delete",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        loadtable();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
