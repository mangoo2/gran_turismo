var base_url = $('#base_url').val();
var tabla; var id_cli_table=0;  
$(document).ready(function() {
    loadtable();
});

function loadtable(){
    tabla=$("#table_data").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        destroy:true,
        responsive: !0,
        "ajax": {
            "url": base_url+"Prospectos/getlistado",
            type: "post",
        },
        "columns": [
            {"data":"id"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';  
                    html=row.nombre+" "+row.app+" "+row.apm;
                return html;
                }
            },
            {"data":"telefono"},
            {"data":"correo"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button" onclick="passcli('+row.id+')"><i class="fa fa-user" style="font-size: 20px;"></i></a>';
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<a title="Eliminar" onclick="delete_data('+row.id+')"><i class="fa fa-trash-o" style="font-size: 20px; cursor: pointer;"></i></a> ';
                    html+='<a title="Editar" href="'+base_url+'Prospectos/registro/'+row.id+'"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
                    //html += '<a target="_blank" title="Generar cotización" href="'+base_url+'Prospectos/cotizacion/'+row.id+'"><i class="fa fa-file-pdf-o icon_font" style="font-size: 20px;"></i></a>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        fixedColumns: true,
        language: languageTables
    });
}

function passcli(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Desea confirmarlo como cliente?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Prospectos/propectoCliente",
                    data: {
                        id:id, tabla:"prospectos", tipo:1
                    },
                    success:function(response){  
                        loadtable();
                        swal("Éxito", "Se ha confirmado como cliente correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function delete_data(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Prospectos/delete",
                    data: {
                        id:id, tabla:"prospectos"
                    },
                    success:function(response){  
                        loadtable();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}