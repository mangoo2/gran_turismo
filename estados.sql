-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-07-2023 a las 01:46:42
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `alfa_electro`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `EstadoId` int(11) NOT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `Alias` varchar(10) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`EstadoId`, `Nombre`, `Alias`, `activo`) VALUES
(1, 'AGUASCALIENTES', 'AS', 1),
(2, 'BAJA CALIFORNIA', 'BC', 1),
(3, 'BAJA CALIFORNIA SUR', 'BS', 1),
(4, 'CAMPECHE', 'CC', 1),
(5, 'COAHUILA', 'CL', 1),
(6, 'COLIMA', 'CM', 1),
(7, 'CHIAPAS', 'CS', 1),
(8, 'CHIHUAHUA', 'CH', 1),
(9, 'DISTRITO FEDERAL', 'DF', 1),
(10, 'DURANGO', 'DG', 1),
(11, 'GUANAJUATO', 'GT', 1),
(12, 'GUERRERO', 'GR', 1),
(13, 'HIDALGO', 'HG', 1),
(14, 'JALISCO', 'JC', 1),
(15, 'ESTADO DE MEXICO', 'MC', 1),
(16, 'MICHOACAN', 'MN', 1),
(17, 'MORELOS', '', 1),
(18, 'NAYARIT', 'NT', 1),
(19, 'NUEVO LEON', 'NL', 1),
(20, 'OAXACA', 'OC', 1),
(21, 'PUEBLA', 'PL', 1),
(22, 'QUERETARO', 'QT', 1),
(23, 'QUINTANA ROO', 'QR', 1),
(24, 'SAN LUIS POTOSI', 'SP', 1),
(25, 'SINALOA', 'SL', 1),
(26, 'SONORA', 'SR', 1),
(27, 'TABASCO', 'TC', 1),
(28, 'TAMAULIPAS', 'TS', 1),
(29, 'TLAXCALA', 'TL', 1),
(30, 'VERACRUZ', 'VZ', 1),
(31, 'YUCATAN', 'YN', 1),
(32, 'ZACATECAS', 'ZS', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`EstadoId`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `EstadoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
