<?php
defined('BASEPATH') or exit('No direct script access allowed');

class EncuestaR extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloSession');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloEncuestaR');
    $this->idpersonal=$this->session->userdata('idpersonal');
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
    $this->fecha_reciente = date('Y-m-d');
    if ($this->session->userdata('logeado')){
      $this->idpersonal=$this->session->userdata('idpersonal_tz');
      $this->perfilid=$this->session->userdata('perfilid_tz');
      $permiso=$this->ModeloSession->getviewpermiso($this->perfilid,9);
      if ($permiso==0) {
        redirect('Login');
      }
    }else{
      redirect('/Login');
    }
  }

  public function index(){
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('encuestaR/index');
    $this->load->view('templates/footer');
    $this->load->view('encuestaR/indexjs');
  }

  public function getlistado(){
    $params = $this->input->post();
    $datas = $this->ModeloGeneral->getselectwhere_n_consulta("encuesta",array("estatus"=>1));
    $json_data = array("data" => $datas);
    echo json_encode($json_data);
  }

  public function get_data_encuesta(){
    $id = $this->input->post('id');

    $data = $this->ModeloEncuestaR->getDataEncuesta($id);
    //log_message('error','Est 2'.json_encode($data));
    echo json_encode($data); 
  }

  public function exportEncuesta(){
    //$data['encuestas'] = $this->ModeloGeneral->getselectwhere2('encuesta',array('estatus'=>1));
    $data['encuestas'] = $this->ModeloEncuestaR->getDataEncuestaExcel();
    
    $this->load->view('reportes/excelEncuestaR',$data);
  }

}