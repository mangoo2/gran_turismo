<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloSession');
        $this->load->model('ModeloContratos');
        $this->load->model('ModeloGeneral');
        

        $this->idpersonal = $this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')) {
            $this->idpersonal = $this->session->userdata('idpersonal_tz');
            $this->perfilid = $this->session->userdata('perfilid_tz');
            $permiso = $this->ModeloSession->getviewpermiso($this->perfilid, 10);
            if ($permiso == 0) {
                redirect('Login');
            }
        }else {
            redirect('/Login');
        }
    }
	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('reportes/index');
        $this->load->view('templates/footer');
        $this->load->view('reportes/indexjs');
	}

    public function search_cliente(){
        $search = $this->input->get('search');
        $results = $this->ModeloContratos->search_cliente($search);
        echo json_encode($results);    
    }

    public function getlistaCta(){
        $params = $this->input->post();
        $datas = $this->ModeloContratos->getEdoCuenta($params);
        $json_data = array("data" => $datas);
        echo json_encode($json_data);
    }

    public function exportReporte($id_cliente,$fechai,$fechaf){  
        $params["id_cliente"] = $id_cliente;
        $params["fechai"] = $fechai;
        $params["fechaf"] = $fechaf;
        $data["edos"] = $this->ModeloContratos->getEdoCuenta($params);
        $this->load->view('reportes/excelEdosCta',$data);
    }

    /************************************ */
    public function contratos(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('reportes/contratos');
        $this->load->view('templates/footer');
        $this->load->view('reportes/contratosjs');
    }

    public function exportReporteContra($id_cliente,$fechai,$fechaf){  
        $params["id_cliente"] = $id_cliente;
        $params["fechai"] = $fechai;
        $params["fechaf"] = $fechaf;
        //$data["edos"] = $this->ModeloContratos->getEdoCuenta($params);
        $data["con"] = $this->ModeloContratos->getContratosReporte($params);

        //log_message('error','DATOS:-> '.json_encode($data["con"]));
        $this->load->view('reportes/reporteExcelContras',$data);
    }

}