<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Agenda extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloSession');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloContratos');
    $this->idpersonal=$this->session->userdata('idpersonal');
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
    $this->fecha_reciente = date('Y-m-d');
    if ($this->session->userdata('logeado')){
      $this->idpersonal=$this->session->userdata('idpersonal_tz');
      $this->perfilid=$this->session->userdata('perfilid_tz');
      $permiso=$this->ModeloSession->getviewpermiso($this->perfilid,7);
      if ($permiso==0) {
        redirect('Login');
      }
    }else{
      redirect('/Login');
    }
  }

  public function index(){
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('operaciones/agenda/index');
    $this->load->view('templates/footerCalendar');
    $this->load->view('operaciones/agenda/indexjs');
  }

  public function getDataAgenda(){
    $data = $this->ModeloContratos->getDataContratoUnidad();
    //log_message('error','DATA-> '.json_encode($data));
    //foreach ($myArray as $index => $value)
    foreach ($data as $d) {
      $destinos = $this->ModeloContratos->getDestinosContrato($d->id_UnidPros);
      //log_message('error','DATA_D-> '.$d->id.' '.json_encode($destinos->result()));

      $choferes = $this->ModeloContratos->getChoferesContrato($d->id_UnidPros);
      //log_message('error','DATA_C-> '.$d->id.' '.json_encode($choferes->result()));

      if($d->color == ''){
        $d->color = '#000000';
      }

      if($d->fecha_regreso == "0000-00-00"){
        $d->fecha_regreso = $d->fecha_salida;
      }

      
      $vehiculos = '
      <i class="fa fa-car"></i> <b>Unidad: </b>'.$d->vehiculo.' ';

      foreach ($destinos->result() as $j => $destino) {
        $vehiculos .= '<br>
          <i class="fa fa-map-marker"></i> <b>Destino '.($j+1).': </b> '.$destino->lugar.'
          ';
      }


      foreach ($choferes->result() as $k => $chofer) {
        $vehiculos .= '<br>
          <i class="fa fa-user"></i> <b>Chofer '.($k+1).': </b> '.$chofer->nombre.' '.$chofer->apellido_p.' '.$chofer->apellido_m.'
          ';
      }
      
      $d->unidades = '
        <i class="fa fa-calendar"></i> <b>Fecha de contrato: </b>'.$d->fecha_contrato.'<br>
        <i class="fa fa-file-text-o"></i> <b>Número de contrato: </b>'.$d->id.'<br>
        <i class="fa fa-file-text"></i> <b>Folio de contrato: </b>'.$d->folio.'<br>
        <i class="fa fa-user-o"></i> <b>Cliente: </b>'.$d->nombre.' '.$d->app.' '.$d->apm.'<br>
        <i class="fa fa-map-marker"></i> <b>Origen: </b>'.$d->lugar_origen.'<br>
        '.$vehiculos.'
      ';
    }
    
    log_message('error','DATA ---> '.json_encode($data));
    echo json_encode($data);
  }



  /*
  public function getDataAgenda(){
    $data = $this->ModeloContratos->getDataContratoUnidad();
    //log_message('error','DATA-> '.json_encode($data));
    //foreach ($myArray as $index => $value)
    foreach ($data as $d) {
      $destinos = $this->ModeloContratos->getDestinosContrato($d->id_UnidPros);
      //log_message('error','DATA_D-> '.$d->id.' '.json_encode($destinos->result()));

      $choferes = $this->ModeloContratos->getChoferesContrato($d->id_UnidPros);
      //log_message('error','DATA_C-> '.$d->id.' '.json_encode($choferes->result()));

      if($d->color == ''){
        $d->color = '#000000';
      }
      
      $vehiculos = '
      <i class="fa fa-car"></i> <b>Unidad: </b>'.$d->vehiculo.' ';

      $fecha_mayor = "0000-00-00"; $hora_mayor = "00:00:00";
      $fecha_menor = "0000-00-00"; $hora_menor = "00:00:00";
      foreach ($destinos->result() as $j => $destino) {
        $vehiculos .= '<br>
          <i class="fa fa-map-marker"></i> <b>Destino '.($j+1).': </b> '.$destino->lugar.'
          ';
        
        //Fecha de salida y regreso
        //log_message('error','Destino ' . json_encode($destino));
        if ($destino->fecha_regreso !== "0000-00-00") {
          $timestamp = strtotime($destino->fecha_regreso);
          
          if ($timestamp > strtotime($fecha_mayor)) {
            $fecha_mayor = $destino->fecha_regreso;
            $hora_mayor = $destino->hora_regreso;
          }
        }

        if ($destino->fecha !== "0000-00-00") {
          $timestamp = strtotime($destino->fecha);
          
          if ($timestamp > strtotime($fecha_menor)) {
            $fecha_menor = $destino->fecha;
            $hora_menor = $destino->hora;
          }
        }

      }

      //log_message('error','Fecha Mayor ' . $fecha_mayor . ' ' . $hora_mayor);
      //log_message('error','Fecha menor ' . $fecha_menor . ' ' . $hora_menor);
      $d->fecha_regreso = $fecha_mayor;
      $d->hora_regreso = $hora_mayor;

      $d->fecha_salida = $fecha_menor;
      $d->hora_salida = $hora_menor;


      foreach ($choferes->result() as $k => $chofer) {
        $vehiculos .= '<br>
          <i class="fa fa-user"></i> <b>Chofer '.($k+1).': </b> '.$chofer->nombre.' '.$chofer->apellido_p.' '.$chofer->apellido_m.'
          ';
      }
      
      $d->unidades = '
        <i class="fa fa-calendar"></i> <b>Fecha de contrato: </b>'.$d->fecha_contrato.'<br>
        <i class="fa fa-file-text-o"></i> <b>Número de contrato: </b>'.$d->id.'<br>
        <i class="fa fa-file-text"></i> <b>Folio de contrato: </b>'.$d->folio.'<br>
        <i class="fa fa-user-o"></i> <b>Cliente: </b>'.$d->nombre.' '.$d->app.' '.$d->apm.'<br>
        <i class="fa fa-map-marker"></i> <b>Origen: </b>'.$d->lugar_origen.'<br>
        '.$vehiculos.'
      ';
    }
    
    //log_message('error','DATA ---> '.json_encode($data));
    echo json_encode($data);
  }
  */

}
