<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Prospectos extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloSession');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloClientes');
    $this->idpersonal=$this->session->userdata('idpersonal');
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
    $this->fecha_reciente = date('Y-m-d');
    if ($this->session->userdata('logeado')){
      $this->idpersonal=$this->session->userdata('idpersonal_tz');
      $this->perfilid=$this->session->userdata('perfilid_tz');
      $permiso=$this->ModeloSession->getviewpermiso($this->perfilid,1);
      if ($permiso==0) {
        redirect('Login');
      }
    }else{
      redirect('/Login');
    }
  }

  public function index()
  {
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('prospectos/index');
    $this->load->view('templates/footer');
    $this->load->view('prospectos/indexjs');
  }

  public function registro($id=0)
  {
    $data["unidades"]=$this->ModeloGeneral->getselectwhere2('unidades',array('estatus'=>1));
    $data["estados"]=$this->ModeloGeneral->getselectwhere2('estados',array('activo'=>1));

    if($id==0){
      $data['tittle']='Alta';     
    }else{
      $data['tittle']='Edición'; 
      $data["p"]=$this->ModeloGeneral->getselectwhererow2('prospectos',array('id'=>$id));
      $data["des"]=$this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_prospecto'=>$id,"estatus"=>1));
      $data["uni"]=$this->ModeloGeneral->getselectwhere2('unidad_prospecto',array('id_prospecto'=>$id,"estatus"=>1));
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('prospectos/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('prospectos/formjs');
  }

  public function guardar(){
    $datos = $this->input->post();
    $id=$datos['id'];
    unset($datos['id']);
    $id_reg=0;
    if($id>0){
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,'prospectos');
      $id_reg=$id; 
    }else{
      $datos['fecha_reg']=$this->fechahoy;
      $id_reg=$this->ModeloGeneral->tabla_inserta('prospectos',$datos);
    }  
    echo $id_reg;
  }

  public function getlistado(){
    $params = $this->input->post();
    $params["tipo"]=0;
    $getdata = $this->ModeloClientes->get_result($params);
    $totaldata= $this->ModeloClientes->total_result($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }
  
  public function delete(){
    $id = $this->input->post('id');
    $tabla = $this->input->post('tabla');
    $data = array('estatus' => 0);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,$tabla);
    echo $resul;
  }

  public function propectoCliente(){
    $id = $this->input->post('id');
    $tabla = $this->input->post('tabla');
    $tipo = $this->input->post('tipo');
    $data = array('tipo' => $tipo,"tipo_cliente"=>1); //tambien como cliente normal
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,$tabla);
    echo $resul;
  }

  /*
  public function insertDestino(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    
    for ($i=0; $i<count($DATA); $i++) {
      $id_prospecto = $DATA[$i]->id_prospecto;
      $lugar = $DATA[$i]->lugar;
      $fecha = $DATA[$i]->fecha;
      $hora = $DATA[$i]->hora;
      $lugar_hospeda = $DATA[$i]->lugar_hospeda;

      $arr = array("id_prospecto"=>$id_prospecto,"lugar"=>$lugar,"fecha"=>$fecha,"hora"=>$hora,"lugar_hospeda"=>$lugar_hospeda,"reg"=>$this->fechahoy);
      if($DATA[$i]->id == 0){
        $result = $this->ModeloGeneral->tabla_inserta("destino_prospecto",$arr);
      }else{
        $result = $this->ModeloGeneral->updateCatalogo_value($arr, array('id'=>$DATA[$i]->id), "destino_prospecto");
      }
    }
    echo $datos;
  }
*/
  /* ******************************************************* */

  public function cotizacion($id = 0)
  {
    $data['prospecto'] = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id'=>$id,'estatus'=>1));
    //$data['destinos'] = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_prospecto'=>$id,'estatus'=>1));
    //$data['unidades'] = $this->ModeloClientes->getUnidadesProspecto($id);

    $unidades =  $this->ModeloClientes->getUnidadesProspecto($id);
    //$unidades = $this->ModeloGeneral->getselectwhere2('unidad_prospecto',array('id_prospecto'=>$id, 'id_prospecto !='=>0,'id_contrato'=>0, "estatus"=>1));
    $data['unidades'] = $unidades->result();

    foreach ($data['unidades'] as $dato) {
      $destinos = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_prospecto'=>$id, "id_unidPros"=>$dato->id, "estatus"=>1));
      $dato->destinos = $destinos->result();
    }

    //log_message('error', 'GET TABLE UNIDADES: ' . json_encode($data['unidades']));


    $data['personal'] = $this->ModeloGeneral->getselectwhererow2('personal',array('personalId'=>$this->session->userdata('idpersonal_tz'),'estatus'=>1));

    $length = 3;
    $format = substr(str_repeat(0, $length) . ($id), -$length);
    $data['folio'] = 'COT-' . $format;
    
    //log_message('error','DESTINOS: '.json_encode($data['destinos']));
    //log_message('error','UNIDADES: '.json_encode($data['unidades']->result()));
    $this->load->view('reportes/documento_cotizacion_prospecto', $data);
  }

  /*
  public function insertUnidad(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    
    for ($i=0; $i<count($DATA); $i++) {
      $id_prospecto = $DATA[$i]->id_prospecto;
      $unidad = $DATA[$i]->unidad;
      $cantidad = $DATA[$i]->cantidad;
      $cant_pasajeros = $DATA[$i]->cant_pasajeros;
      $monto = $DATA[$i]->monto;

      $arr = array("id_prospecto"=>$id_prospecto,"unidad"=>$unidad,"cantidad"=>$cantidad,"cant_pasajeros"=>$cant_pasajeros,"monto"=>$monto,"reg"=>$this->fechahoy);
      if($DATA[$i]->id == 0){
        $result = $this->ModeloGeneral->tabla_inserta("unidad_prospecto",$arr);
      }else{
        $result = $this->ModeloGeneral->updateCatalogo_value($arr, array('id'=>$DATA[$i]->id), "unidad_prospecto");
      }
    }
    echo $datos;
  }
  */
  public function get_table_unidades(){
    $id = $this->input->post('id');
    $unidades = $this->ModeloGeneral->getselectwhere2('unidad_prospecto',array('id_prospecto'=>$id, 'id_prospecto !='=>0,'id_contrato'=>0, "estatus"=>1));
    $data['unidades'] = $unidades->result();

    foreach ($data['unidades'] as $dato) {
      $destinos = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_prospecto'=>$id, "id_unidPros"=>$dato->id, "estatus"=>1));
      $dato->destinos = $destinos->result();
    }

    //log_message('error', 'GET TABLE UNIDADES: ' . json_encode($data['unidades']));
    echo json_encode($data['unidades']);
  }


  public function insertUnidades(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    //log_message('error','DATA Destinos: '.json_encode($DATA));
    
    for ($i=0; $i<count($DATA); $i++) {
      $id_prospecto = $DATA[$i]->id_prospecto;

      $unidad = $DATA[$i]->unidad;
      $cantidad = $DATA[$i]->cantidad;
      $cant_pasajeros = $DATA[$i]->cant_pasajeros;
      $monto = $DATA[$i]->monto;

      if($DATA[$i]->id == 0){//Nuevo
        $arr = array("id_prospecto"=>$id_prospecto, "id_cotizacion"=>0, "id_contrato"=>0, "unidad"=>$unidad, "cantidad"=>$cantidad, "cant_pasajeros"=>$cant_pasajeros, "monto"=>$monto, "reg"=>$this->fechahoy);
        $result = $this->ModeloGeneral->tabla_inserta("unidad_prospecto",$arr);

        for ($j = 0; $j<count($DATA[$i]->DESTINOS); $j++) {
          $lugar = $DATA[$i]->DESTINOS[$j]->lugar;
          $fecha = $DATA[$i]->DESTINOS[$j]->fecha;
          $hora = $DATA[$i]->DESTINOS[$j]->hora;
          $lugar_hospeda = $DATA[$i]->DESTINOS[$j]->lugar_hospeda;
          

          $arrU = array("id_prospecto"=>$id_prospecto, "id_cotizacion"=>0, "id_contrato"=>0, "id_unidPros"=>$result, "lugar"=>$lugar, "fecha"=>$fecha, "hora"=>$hora, "lugar_hospeda"=>$lugar_hospeda, "reg"=>$this->fechahoy);
          $idDestino = $this->ModeloGeneral->tabla_inserta("destino_prospecto",$arrU);
        }

      }else{//Edicion
        $arr = array("unidad"=>$unidad, "cantidad"=>$cantidad, "cant_pasajeros"=>$cant_pasajeros, "monto"=>$monto);
        $result = $this->ModeloGeneral->updateCatalogo_value($arr, array('id'=>$DATA[$i]->id), "unidad_prospecto");


        for ($j = 0; $j<count($DATA[$i]->DESTINOS); $j++) {
          $id_destino = $DATA[$i]->DESTINOS[$j]->id;
          $lugar = $DATA[$i]->DESTINOS[$j]->lugar;
          $fecha = $DATA[$i]->DESTINOS[$j]->fecha;
          $hora = $DATA[$i]->DESTINOS[$j]->hora;
          $lugar_hospeda = $DATA[$i]->DESTINOS[$j]->lugar_hospeda;

          if($id_destino == 0){
            $arrU = array("id_prospecto"=>$id_prospecto, "id_cotizacion"=>0, "id_contrato"=>0, "id_unidPros"=>$DATA[$i]->id, "lugar"=>$lugar, "fecha"=>$fecha, "hora"=>$hora, "lugar_hospeda"=>$lugar_hospeda, "reg"=>$this->fechahoy);
            $idDestino = $this->ModeloGeneral->tabla_inserta("destino_prospecto",$arrU);
          }else{
            $arrU = array("lugar"=>$lugar, "fecha"=>$fecha, "hora"=>$hora, "lugar_hospeda"=>$lugar_hospeda);
            $idDestino = $this->ModeloGeneral->updateCatalogo_value($arrU, array('id'=>$id_destino), "destino_prospecto");
          }
        }
      }
    }
    echo $result;
  }

}