<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cotizaciones extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloSession');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloCotizaciones');
    $this->idpersonal=$this->session->userdata('idpersonal');
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
    $this->fecha_reciente = date('Y-m-d');
    if ($this->session->userdata('logeado')){
      $this->idpersonal=$this->session->userdata('idpersonal_tz');
      $this->usuarioid_tz=$this->session->userdata('usuarioid_tz');
      $this->perfilid=$this->session->userdata('perfilid_tz');
      $permiso=$this->ModeloSession->getviewpermiso($this->perfilid,1);
      if ($permiso==0) {
        redirect('Login');
      }
    }else{
      redirect('/Login');
    }
  }

  public function index()
  {
    $data["unidades"] = $this->ModeloGeneral->getselectwhere2('unidades',array('estatus'=>1));
    //log_message('error','unidades'. json_encode($data["unidades"]->result()));
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('operaciones/cotizaciones/index',$data);
    $this->load->view('templates/footer');
    $this->load->view('operaciones/cotizaciones/indexjs');
  }

  public function registro($id=0)
  {
    $data["unidades"]=$this->ModeloGeneral->getselectwhere2('unidades',array('estatus'=>1));
    $data["estados"]=$this->ModeloGeneral->getselectwhere2('estados',array('activo'=>1));
    $data["personal"]=$this->ModeloGeneral->getselectwhere2('personal',array('estatus'=>1));
    $data["idpersonal"]=$this->idpersonal;
    if($id==0){
      $data['tittle']='Alta';     
    }else{
      $data['tittle']='Edición';

      $data["c"]=$this->ModeloGeneral->getselectwhererow2('cotizaciones',array('id'=>$id));
      $data["clientes"] = $this->ModeloGeneral->getselectwhererow2('prospectos',array('estatus'=>1,'id'=>$data["c"]->idCliente));
      $data["des"]=$this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_cotizacion'=>$id,"estatus"=>1));
      $data["uni"]=$this->ModeloGeneral->getselectwhere2('unidad_prospecto',array('id_prospecto'=>$id,"estatus"=>1));
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('operaciones/cotizaciones/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('operaciones/cotizaciones/formjs');
  }


  public function search_cliente(){
    $search = $this->input->get('search');
    $results = $this->ModeloCotizaciones->search_cliente($search);
    echo json_encode($results);    
  }

  public function verificarExpiradas(){  
    $getc=$this->ModeloCotizaciones->getListaCotizaExpira($this->fecha_reciente);
    foreach($getc as $c){
      $this->ModeloGeneral->updateCatalogo(array("seguimiento"=>5),'id',$c->id,'cotizaciones');
    }
  }

  public function exportCotizaciones($id_cliente,$seguimiento,$tipo_cliente,$prioridad,$fechai,$fechaf,$tipo){  
    $params["id_cliente"] = $id_cliente;
    $params["seguimiento"] = $seguimiento;
    $params["tipo_cliente"] = $tipo_cliente;
    $params["prioridad"] = $prioridad;
    $params["fechai"] = $fechai;
    $params["fechaf"] = $fechaf;
    $data["cot"] = $this->ModeloCotizaciones->getListaCotiza($params);
    if($tipo==1){
      $this->load->view('operaciones/cotizaciones/listaExcel',$data);
    }
    else{
      $this->load->view('reportes/listaCotizaciones',$data);
    }
  }

  public function get_data_destinos(){
    $id = $this->input->post('id');

    $unidades = $this->ModeloGeneral->getselectwhere2('unidad_prospecto',array('id_prospecto' => $id, 'id_contrato' => 0, 'id_cotizacion' => 0, 'estatus' => 1));
    $data['unidades'] = $unidades->result();

    foreach ($data['unidades'] as $dato) {
      $destinos = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_prospecto' => $id, 'id_contrato' => 0, 'id_cotizacion' => 0,"id_unidPros"=>$dato->id, "estatus"=>1));
      $dato->destinos = $destinos->result();
    }

    //log_message('error', 'GET TABLE destinos: ' . json_encode($data['unidades']));
    echo json_encode($data['unidades']);
  }


  public function guardar(){
    $datos = $this->input->post();
    $id=$datos['id'];
    unset($datos['id']);
    $id_reg=0;
    if($datos["seguimiento"]==6){//rechazada
      $datos['fecha_rechazo']=$this->fechahoy;
      $data["id_user_rechaza"]=$this->usuarioid_tz;
    }else{
      $datos['motivo_rechazo']="";
      $datos['fecha_rechazo']="";
      $data["id_user_rechaza"]=0;
    }
    if($id>0){
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,'cotizaciones');
      $id_reg=$id; 
    }else{
      $datos['fecha_reg']=$this->fechahoy;
      $data["id_user_crea"]=$this->usuarioid_tz;
      $id_reg=$this->ModeloGeneral->tabla_inserta('cotizaciones',$datos);
    }  
    echo $id_reg;
  }

  public function getlistado(){
    $params = $this->input->post();
    $getdata = $this->ModeloCotizaciones->get_result($params);
    $totaldata= $this->ModeloCotizaciones->total_result($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }


  public function delete(){
    $id = $this->input->post('id');
    $tabla = $this->input->post('tabla');
    $estatus = $this->input->post('estatus');
    $data = array('estatus' => $estatus);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,$tabla);
    echo $resul;
  }

  public function updateSeguimiento(){ //FALTA EDITAR EN TABLA
    $id = $this->input->post('id');
    $seguimiento = $this->input->post('seguimiento');
    $motivo_rechazo = $this->input->post('motivo_rechazo');
    $this->ModeloGeneral->updateCatalogo(array("seguimiento"=>$seguimiento,"motivo_rechazo"=>$motivo_rechazo),'id',$id,"cotizaciones");
  }
  
  public function get_table_unidades(){
    $id = $this->input->post('id');
    $unidades = $this->ModeloGeneral->getselectwhere2('unidad_prospecto',array('id_cotizacion'=>$id, 'id_cotizacion !='=>0,'id_contrato'=>0, "estatus"=>1));
    $data['unidades'] = $unidades->result();

    foreach ($data['unidades'] as $dato) {
      $destinos = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_cotizacion'=>$id, "id_unidPros"=>$dato->id, "estatus"=>1));
      $dato->destinos = $destinos->result();
    }

    //log_message('error', 'GET TABLE UNIDADES: ' . json_encode($data['unidades']));
    echo json_encode($data['unidades']);
  }


  public function insertUnidades(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    //log_message('error','DATA Destinos: '.json_encode($DATA));
    
    for ($i=0; $i<count($DATA); $i++) {
      $id_cotizacion = $DATA[$i]->id_cotizacion;
      $id_prospecto = $DATA[$i]->id_prospecto;

      $unidad = $DATA[$i]->unidad;
      $cantidad = $DATA[$i]->cantidad;
      $cant_pasajeros = $DATA[$i]->cant_pasajeros;
      $monto = $DATA[$i]->monto;

      if($DATA[$i]->id == 0){//Nuevo
        $arr = array("id_prospecto"=>$id_prospecto, "id_cotizacion"=>$id_cotizacion, "unidad"=>$unidad, "cantidad"=>$cantidad, "cant_pasajeros"=>$cant_pasajeros, "monto"=>$monto, "reg"=>$this->fechahoy);
        $result = $this->ModeloGeneral->tabla_inserta("unidad_prospecto",$arr);

        for ($j = 0; $j<count($DATA[$i]->DESTINOS); $j++) {
          $lugar = $DATA[$i]->DESTINOS[$j]->lugar;
          //$fecha = $DATA[$i]->DESTINOS[$j]->fecha;
          //$hora = $DATA[$i]->DESTINOS[$j]->hora;
          //$fecha_regreso = $DATA[$i]->DESTINOS[$j]->fecha_regreso;
          //$hora_regreso = $DATA[$i]->DESTINOS[$j]->hora_regreso;
          //$lugar_hospeda = $DATA[$i]->DESTINOS[$j]->lugar_hospeda;
          $fecha = '';
          $hora = '';
          $fecha_regreso = '';
          $hora_regreso ='';
          $lugar_hospeda = '';

          $arrU = array("id_prospecto"=>$id_prospecto, "id_cotizacion"=>$id_cotizacion, "id_unidPros"=>$result, "lugar"=>$lugar, "fecha"=>$fecha, "hora"=>$hora, "fecha_regreso"=>$fecha_regreso, "hora_regreso"=>$hora_regreso, "lugar_hospeda"=>$lugar_hospeda, "reg"=>$this->fechahoy);
          $idDestino = $this->ModeloGeneral->tabla_inserta("destino_prospecto",$arrU);
        }

      }else{//Edicion
        $arr = array("id_cotizacion"=>$id_cotizacion, "unidad"=>$unidad, "cantidad"=>$cantidad, "cant_pasajeros"=>$cant_pasajeros, "monto"=>$monto);
        $result = $this->ModeloGeneral->updateCatalogo_value($arr, array('id'=>$DATA[$i]->id), "unidad_prospecto");

        for ($j = 0; $j<count($DATA[$i]->DESTINOS); $j++) {
          $id_destino = $DATA[$i]->DESTINOS[$j]->id;
          $lugar = $DATA[$i]->DESTINOS[$j]->lugar;
          //$fecha = $DATA[$i]->DESTINOS[$j]->fecha;
          //$hora = $DATA[$i]->DESTINOS[$j]->hora;
          //$fecha_regreso = $DATA[$i]->DESTINOS[$j]->fecha_regreso;
          //$hora_regreso = $DATA[$i]->DESTINOS[$j]->hora_regreso;
          //$lugar_hospeda = $DATA[$i]->DESTINOS[$j]->lugar_hospeda;
          $fecha = '';
          $hora = '';
          $fecha_regreso = '';
          $hora_regreso ='';
          $lugar_hospeda = '';

          if($id_destino == 0){
            $arrU = array("id_prospecto"=>$id_prospecto, "id_cotizacion"=>$id_cotizacion, "id_unidPros"=>$DATA[$i]->id, "lugar"=>$lugar, "fecha"=>$fecha, "hora"=>$hora, "fecha_regreso"=>$fecha_regreso, "hora_regreso"=>$hora_regreso, "lugar_hospeda"=>$lugar_hospeda, "reg"=>$this->fechahoy);
            $idDestino = $this->ModeloGeneral->tabla_inserta("destino_prospecto",$arrU);
          }else{
            $arrU = array("id_cotizacion"=>$id_cotizacion, "lugar"=>$lugar, "fecha"=>$fecha, "hora"=>$hora, "fecha_regreso"=>$fecha_regreso, "hora_regreso"=>$hora_regreso, "lugar_hospeda"=>$lugar_hospeda);
            $idDestino = $this->ModeloGeneral->updateCatalogo_value($arrU, array('id'=>$id_destino), "destino_prospecto");
          }
        }
      }
    }
    echo $result;
  }


  public function cotizacionContrato(){
    $id = $this->input->post('id');
    $data = $this->ModeloGeneral->getselectwhererow2('cotizaciones',array('id'=>$id, 'estatus'=>1));
    //log_message('error','DATA: ' . json_encode($data));
    $this->ModeloGeneral->updateCatalogo_value(array('es_contrato'=>1), array('id'=>$id), 'cotizaciones');
    unset($data->id);
    unset($data->es_contrato);
    unset($data->id_user_crea);
    unset($data->id_user_autoriza);
    unset($data->fecha_autoriza);
    unset($data->fecha_envia);
    
    $data->id_cotizacion = $id;
    $data->cotizacion = 1;
    $data->fecha_reg = $this->fechahoy;
    $data->seguimiento = 1;
    //log_message('error','DATA Cotizacion: '.json_encode($data));
    $idCont = $this->ModeloGeneral->tabla_inserta("contratos",$data);
    //log_message('error','IdContrato: '.json_encode($idCont));

    $destinos = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_cotizacion'=>$id, "estatus"=>1));
    if(count($destinos->result()) > 0){
      foreach ($destinos->result() as $d) {
        $this->ModeloGeneral->updateCatalogo_value(array('id_contrato'=>$idCont), array('id'=>$d->id), 'destino_prospecto');
      }
    }

    $unidades = $this->ModeloGeneral->getselectwhere2('unidad_prospecto',array('id_cotizacion'=>$id, "estatus"=>1));
    if(count($unidades->result()) > 0){
      foreach ($unidades->result() as $u) {
        $this->ModeloGeneral->updateCatalogo_value(array('id_contrato'=>$idCont), array('id'=>$u->id), 'unidad_prospecto');
      }
    }
    
    echo $idCont;
  }


  public function cotizacion($id = 0)
  {
    $data['usuario'] = $this->ModeloGeneral->getselectwhererow2('personal',array('personalId'=>$this->session->userdata('idpersonal_tz'),'estatus'=>1));
    $data['fecha_hora_imp'] = $this->fechahoy;
    $data['printLabels'] = true;

    $data['cotizacion'] = $this->ModeloGeneral->getselectwhererow2('cotizaciones',array('id'=>$id,'estatus'=>1));
    
    $data['prospecto'] = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id'=>$data['cotizacion']->idCliente,'estatus'=>1));

    $unidades = $this->ModeloCotizaciones->getUnidadesCotizacion($id);
    $data['unidades'] = $unidades->result();

    foreach ($data['unidades'] as $dato) {
      $destinos = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_prospecto'=>$data['cotizacion']->idCliente, "id_unidPros"=>$dato->id, "estatus"=>1));
      $dato->destinos = $destinos->result();
    }
    //log_message('error', 'UNIDADES: '.json_encode($data['unidades']));
    //log_message('error', 'usuario: '.json_encode($data['usuario']));

    $data['personal'] = $this->ModeloGeneral->getselectwhererow2('personal',array('personalId'=>$data['cotizacion']->personal,'estatus'=>1));

    $length = 5;
    $format = substr(str_repeat(0, $length) . ($id), -$length);

    $anio = date("Y");
    $anio_2d = substr($anio, -2); //Los dos últimos dígitos

    $data['folio'] = 'GT'.$anio_2d.'-'.$format;
    
    $this->load->view('reportes/documento_cotizacion', $data);
  }


  public function cotizacionMail(){
    $id = $this->input->post('id');

    $data['usuario'] = $this->ModeloGeneral->getselectwhererow2('personal',array('personalId'=>$this->session->userdata('idpersonal_tz'),'estatus'=>1));
    $data['fecha_hora_imp'] = $this->fechahoy;
    $data['printLabels'] = true;
    $data['cotizacion'] = $this->ModeloGeneral->getselectwhererow2('cotizaciones',array('id'=>$id,'estatus'=>1));
    $data['prospecto'] = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id'=>$data['cotizacion']->idCliente,'estatus'=>1));
    $unidades = $this->ModeloCotizaciones->getUnidadesCotizacion($id);
    $data['unidades'] = $unidades->result();

    foreach ($data['unidades'] as $dato) {
      $destinos = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_prospecto'=>$data['cotizacion']->idCliente, "id_unidPros"=>$dato->id, "estatus"=>1));
      $dato->destinos = $destinos->result();
    }

    //log_message('error', 'UNIDADES: '.json_encode($data['unidades']));
    $data['personal'] = $this->ModeloGeneral->getselectwhererow2('personal',array('personalId'=>$data['cotizacion']->personal,'estatus'=>1));
    $length = 5;
    $format = substr(str_repeat(0, $length) . ($id), -$length);
    $anio = date("Y");
    $anio_2d = substr($anio, -2); //Los dos últimos dígitos
    $data['folio'] = 'GT'.$anio_2d.'-'.$format;
    $this->load->view('reportes/documento_cotizacion', $data);
  }

  function sendMailPDF(){
    $idCliente = $this->input->post('idCliente');
    $idCotizacion = $this->input->post('idCotizacion');
    $get_info = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id' => $idCliente));
    //log_message('error', 'GET MAIL: '.json_encode(gettype($get_info)));
    $nombre = $get_info->nombre." ".$get_info->app." ".$get_info->apm;

    //$empresa = $get_info->empresa; 
    $email = $get_info->correo;

    //== libreria email ==
    if($email!=""){
      $this->load->library('email');
      $config = array();
      $config['protocol'] = 'smtp';
      $config["smtp_host"] = 'grandturismo.mangoo.systems'; //'mail.mangoo.systems'
      $config["smtp_user"] = 'contacto@grandturismo.mangoo.systems'; //'contacto@mangoo.systems'
      $config["smtp_pass"] = 'xxb{-5dz767h'; //'x!fZTBrOoBGI'

      $config["smtp_port"] = 465;
      $config["smtp_crypto"] = 'ssl';
      $config['mailtype'] = "html";          
      $config['charset'] = 'utf-8'; 
      $config['wordwrap'] = TRUE;
      $config['validate'] = true;
      $config['mailtype'] = 'html';

      $this->email->initialize($config);

      $this->email->from('contacto@grandturismo.mangoo.systems'); //'contacto@mangoo.systems'
      $this->email->to($email);

      $this->email->subject('Cotizacion '.$nombre);

      //--------------------------------------------------------
      $pdf = FCPATH.'public/pdf/Cotizacion_'.$idCotizacion.'.pdf';
      $this->email->attach($pdf);
      //--------------------------------------------------------

      $this->email->message('Se adjunta Cotizacion.');
      if ($this->email->send()) {
        echo 'Email sent.';
        $this->ModeloGeneral->updateCatalogo(array("seguimiento"=>"7","fecha_envia"=>$this->fechahoy),'id',$id,'cotizaciones');
      } else {
        echo 'Error ' . $this->email->print_debugger();
      }
    } 
  }


  public function get_data_cotizacion(){
    $id = $this->input->post('id');

    $dataCli = $this->ModeloCotizaciones->getDataCliente($id);
    $dataEst = $this->ModeloCotizaciones->getDataEstimados($id);
    if($dataEst->num_rows()>0){
      $dataEst=$dataEst->row();
      $dataEst->cliente = isset($dataCli->nombre, $dataCli->app, $dataCli->apm) ? $dataCli->nombre.' '.$dataCli->app.' '.$dataCli->apm : '';
      $data = $dataEst;
      //log_message('error','Est 1'.json_encode($data));
    }else{
      $data = array(
        'id' => '',
        'km_ini' => '',
        'km_fin' => '',
        'precio_gas' => '',
        'casetas' => '',
        'combustible' => '',
        'otros' => '',
        'sueldo' => '',
        'utilidad' => '',
        'cliente' => isset($dataCli->nombre, $dataCli->app, $dataCli->apm) ? $dataCli->nombre.' '.$dataCli->app.' '.$dataCli->apm : ''
      );
      //log_message('error','Est 2'.json_encode($data));
    }
    echo json_encode($data); 
  }

  public function insertEstimados(){
    $datos = $this->input->post();
    
    $idEstima = $datos['idEstima'];
    unset($datos['idEstima']);

    log_message('error','Datos-> '.json_encode($datos));

    $id_reg=0;
    if($idEstima > 0){
      $this->ModeloGeneral->updateCatalogo($datos,'id',$idEstima,'cotizacion_estimados');
      $id_reg = $idEstima; 
    }else{
      $datos['reg']=$this->fechahoy;
      $id_reg = $this->ModeloGeneral->tabla_inserta('cotizacion_estimados',$datos);
    }
    echo $id_reg;
  }


  
  public function insertUnits(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    //log_message('error','DATA Unidades: '.json_encode($DATA));

    for ($i=0; $i<count($DATA); $i++) {
      //[{"id_estimados":"3","id_cotizacion":"17","id":"","id_unidad":"1","vehiculo":"ECO. 01 - SPRINTER MB","num_eco":"1","tipo":"1"}]
      $id = $DATA[$i]->id;
      unset($DATA[$i]->id);
      $result = 0;

      if($id == 0){//Nuevo
        $DATA[$i]->reg = $this->fechahoy;
        $this->ModeloGeneral->tabla_inserta("cotizacion_unidades", $DATA[$i]);
        $result = $id;
      }else{//Edicion
        $result = $this->ModeloGeneral->updateCatalogo_value($DATA[$i], array('id'=>$id), "cotizacion_unidades");
      }
    }
    echo $result;
  }


  public function get_table_units(){
    $idCot = $this->input->post('id');

    $unidades = $this->ModeloGeneral->getselectwhere2('cotizacion_unidades',array('id_cotizacion'=>$idCot, "estatus"=>1));
    $data['unidades'] = $unidades->result();
    //log_message('error','Unidades: '.json_encode($data['unidades']));

    echo json_encode($data['unidades']);
  }

  
  public function get_unidades_cotizacion(){
    $id = $this->input->post('id');
    $data = $this->ModeloCotizaciones->getUnidadesCotizacion($id);
    //log_message('error','DATA Unidades '.json_encode($data->result()));

    echo json_encode($data->result()); 
  }

  public function exportEstimado($idCot){
    $data["cot"] = $this->ModeloCotizaciones->getDataCotiza($idCot);
    $this->load->view('reportes/excelEstimados',$data);
  }

  
  public function exportEstimados($id_cliente,$seguimiento,$tipo_cliente,$prioridad,$fechai,$fechaf){  
    $params["id_cliente"] = $id_cliente;
    $params["seguimiento"] = $seguimiento;
    $params["tipo_cliente"] = $tipo_cliente;
    $params["prioridad"] = $prioridad;
    $params["fechai"] = $fechai;
    $params["fechaf"] = $fechaf;
    $data["cot"] = $this->ModeloCotizaciones->getDataCotiza2($params);
    
    $this->load->view('reportes/excelEstimados',$data);

  }
}