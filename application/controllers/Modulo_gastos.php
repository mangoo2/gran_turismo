<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Modulo_gastos extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloSession');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloUsuarios');
    $this->load->model('ModeloModGastos');

    $this->idpersonal = $this->session->userdata('idpersonal');
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
    $this->fecha_reciente = date('Y-m-d');
    if ($this->session->userdata('logeado')) {
      $this->idpersonal = $this->session->userdata('idpersonal_tz');
      $this->perfilid = $this->session->userdata('perfilid_tz');
      $permiso = $this->ModeloSession->getviewpermiso($this->perfilid, 1);
      if ($permiso == 0) {
        redirect('Login');
      }
    } else {
      redirect('/Login');
    }
  }

  public function index()
  {
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('moduloGastos/index');
    $this->load->view('templates/footer');
    $this->load->view('moduloGastos/index_js');
  }

  public function registro($id = 0)
  {
    $data['title_heades'] = 'Nuevo módulo de gastos';
    $data['title_save'] = 'Guardar datos';

    if ($id > 0) {
      $data['title_heades'] = 'Editar módulo de gastos';
      $data['title_save'] = 'Actualizar datos';

      $data['idMD'] = $id;
      $result = $this->ModeloGeneral->getselectwhererow2('modulo_gastos', array('id' => $id, 'estatus' => 1));
      $data['concepto'] = $result->concepto;
      $data['responsable'] = $result->responsable;
      $data['folio'] = $result->folio;
      $data['descripcion'] = $result->descripcion;
      $data['monto'] = $result->monto;
      $data['fecha'] = $result->fecha;
    } 

    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('moduloGastos/form', $data);
    $this->load->view('templates/footer');
    $this->load->view('moduloGastos/form_js');
  }

  public function insert()
  {
    $data = $this->input->post();
    $id = $data['id'];
    unset($data['id']);
    $id_reg = 0;

    if ($id > 0) {
      $this->ModeloGeneral->updateCatalogo($data, 'id', $id, 'modulo_gastos');
      $id_reg = $id;
    } else {
      $data['reg'] = $this->fechahoy;
      $data['usuario'] = $this->idpersonal;

      $id_reg = $this->ModeloGeneral->tabla_inserta('modulo_gastos', $data);
    }

    echo $id_reg;
  }


  public function get_list()
  {
    $params = $this->input->post();
    $getdata = $this->ModeloModGastos->get_result($params);
    $totaldata = $this->ModeloModGastos->total_result($params);
    $json_data = array(
      "draw"            => intval($params['draw']),
      "recordsTotal"    => intval($totaldata),
      "recordsFiltered" => intval($totaldata),
      "data"            => $getdata->result(),
      "query"           => $this->db->last_query()
    );
    echo json_encode($json_data);
  }

  public function delete()
  {
    $id = $this->input->post('id');
    $data = array('estatus' => 0);
    $result = $this->ModeloGeneral->updateCatalogo($data, 'id', $id, 'modulo_gastos');
    echo $result;
  }

}
