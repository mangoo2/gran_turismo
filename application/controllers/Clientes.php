<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Clientes extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloSession');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloClientes');
    $this->idpersonal=$this->session->userdata('idpersonal');
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
    $this->fecha_reciente = date('Y-m-d');
    if ($this->session->userdata('logeado')){
      $this->idpersonal=$this->session->userdata('idpersonal_tz');
      $this->perfilid=$this->session->userdata('perfilid_tz');
      $permiso=$this->ModeloSession->getviewpermiso($this->perfilid,5);
      if ($permiso==0) {
        redirect('Login');
      }
    }else{
      redirect('/Login');
    }
  }

  public function index()
  {
    $data["estados"]=$this->ModeloGeneral->getselectwhere2('estados',array('activo'=>1));
    $data["unidades"]=$this->ModeloGeneral->getselectwhere2('unidades',array('estatus'=>1));

    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('clientes/index',$data);
    $this->load->view('templates/footer');
    $this->load->view('clientes/indexjs');
  }

  public function registro($id=0)
  {
    $data["reg"]=$this->ModeloGeneral->getselectwhere2('f_regimenfiscal',array('activo'=>1));
    $data["cfdi"]=$this->ModeloGeneral->getselectwhere2('f_uso_cfdi',array('activo'=>1));
    $data["estados"]=$this->ModeloGeneral->getselectwhere2('estados',array('activo'=>1));
    $data["unidades"]=$this->ModeloGeneral->getselectwhere2('unidades',array('estatus'=>1));

    if($id==0){
      $data['tittle']='Alta';     
    }else{
      $data['tittle']='Edición'; 
      $data["p"]=$this->ModeloGeneral->getselectwhererow2('prospectos',array('id'=>$id));
      $data["des"]=$this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_prospecto'=>$id,'id_contrato'=>0,"estatus"=>1));
      $data["ext"]=$this->ModeloGeneral->getselectwhererow2('pagos_cliente',array('id_cliente'=>$id,"estatus"=>1));
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('clientes/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('clientes/formjs');
  }

  public function guardar(){
    $datos = $this->input->post();
    $id=$datos['id'];
    unset($datos['id']);

    $idcliente_ext=$datos["idcliente_ext"];
    $ine=$datos['ine'];
    unset($datos['idcliente_ext']);
    unset($datos['ine']);

    $id_reg=0;
    if($id>0){
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,'prospectos');
      $id_reg=$id; 
    }else{
      $datos['fecha_reg']=$this->fechahoy;
      $datos["tipo"]=1;
      $id_reg=$this->ModeloGeneral->tabla_inserta('prospectos',$datos);
    }  

    if($idcliente_ext>0){
      $this->ModeloGeneral->updateCatalogo(array("id_cliente"=>$id_reg,"ine"=>$ine),'id',$idcliente_ext,'pagos_cliente');
    }else{
      $idcliente_ext=$this->ModeloGeneral->tabla_inserta('pagos_cliente',array("id_cliente"=>$id_reg,"ine"=>$ine,"reg"=>$this->fechahoy));
    }  
    echo json_encode(array("id_reg"=>$id_reg,"id_ext"=>$idcliente_ext));
  }

  public function getlistado(){
    $params = $this->input->post();
    $params["tipo"]=1;
    $getdata = $this->ModeloClientes->get_result($params);
    $totaldata= $this->ModeloClientes->total_result($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }
  
  public function delete(){
    $id = $this->input->post('id');
    $tabla = $this->input->post('tabla');
    $data = array('estatus' => 0);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,$tabla);
    echo $resul;
  }

  public function propectoCliente(){
    $id = $this->input->post('id');
    $tabla = $this->input->post('tabla');
    $tipo = $this->input->post('tipo');
    $data = array('tipo' => $tipo);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,$tabla);
    echo $resul;
  }

  public function cargaimagen(){
    $idcliente=$_POST['idcliente'];
    $input_name=$_POST['input_name'];
    $idcliente_ext=$_POST['idcliente_ext'];
    
    $DIR_SUC=FCPATH.'uploads/clientes';
    $config['upload_path']          = $DIR_SUC;
    $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp|pdf';
    $config['max_size']             = 5000;
    $file_names=date('YmdGis');
    $config['file_name']=$file_names;       
    $output = [];

    $this->load->library('upload', $config);
    if ( ! $this->upload->do_upload($input_name)){
        $data = array('error' => $this->upload->display_errors());
        //log_message('error', json_encode($data));                    
    }else{
      $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
      $file_name = $upload_data['file_name']; //uploded file name
      $extension=$upload_data['file_ext'];    // uploded file extension
      if($idcliente_ext>0){
        $this->ModeloGeneral->updateCatalogo_value(array($input_name=>$file_name),array('id'=>$idcliente_ext),'pagos_cliente');
      }else{
        $this->ModeloGeneral->tabla_inserta('pagos_cliente',array('id_cliente'=>$idcliente,$input_name=>$file_name));
      }
      $data = array('upload_data' => $this->upload->data());
    }
    echo json_encode($output);
  }

  public function deleteImg($id,$tipo){
    if($tipo==1)
      $name="identifica";
    else
      $name="comprobante";
    
    $data = array($name=>"");
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,'pagos_cliente');
    echo $resul;
  }


  public function get_table_unidades(){
    $id = $this->input->post('id');
    $unidades = $this->ModeloGeneral->getselectwhere2('unidad_prospecto',array('id_prospecto'=>$id, 'id_prospecto !='=>0,'id_contrato'=>0, "estatus"=>1));
    $data['unidades'] = $unidades->result();

    foreach ($data['unidades'] as $dato) {
      $destinos = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_prospecto'=>$id, "id_unidPros"=>$dato->id, "estatus"=>1));
      $dato->destinos = $destinos->result();
    }

    //log_message('error', 'GET TABLE UNIDADES: ' . json_encode($data['unidades']));
    echo json_encode($data['unidades']);
  }

  
  public function insertUnidades(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    //log_message('error','DATA Destinos: '.json_encode($DATA));
    
    for ($i=0; $i<count($DATA); $i++) {
      $id_prospecto = $DATA[$i]->id_prospecto;

      $unidad = $DATA[$i]->unidad;
      $cantidad = $DATA[$i]->cantidad;
      $cant_pasajeros = $DATA[$i]->cant_pasajeros;
      $monto = $DATA[$i]->monto;

      if($DATA[$i]->id == 0){//Nuevo
        $arr = array("id_prospecto"=>$id_prospecto, "id_cotizacion"=>0, "id_contrato"=>0, "unidad"=>$unidad, "cantidad"=>$cantidad, "cant_pasajeros"=>$cant_pasajeros, "monto"=>$monto, "reg"=>$this->fechahoy);
        $result = $this->ModeloGeneral->tabla_inserta("unidad_prospecto",$arr);

        for ($j = 0; $j<count($DATA[$i]->DESTINOS); $j++) {
          $lugar = $DATA[$i]->DESTINOS[$j]->lugar;
          $fecha = $DATA[$i]->DESTINOS[$j]->fecha;
          $hora = $DATA[$i]->DESTINOS[$j]->hora;
          $lugar_hospeda = $DATA[$i]->DESTINOS[$j]->lugar_hospeda;
          

          $arrU = array("id_prospecto"=>$id_prospecto, "id_cotizacion"=>0, "id_contrato"=>0, "id_unidPros"=>$result, "lugar"=>$lugar, "fecha"=>$fecha, "hora"=>$hora, "lugar_hospeda"=>$lugar_hospeda, "reg"=>$this->fechahoy);
          $idDestino = $this->ModeloGeneral->tabla_inserta("destino_prospecto",$arrU);
        }

      }else{//Edicion
        $arr = array("unidad"=>$unidad, "cantidad"=>$cantidad, "cant_pasajeros"=>$cant_pasajeros, "monto"=>$monto);
        $result = $this->ModeloGeneral->updateCatalogo_value($arr, array('id'=>$DATA[$i]->id), "unidad_prospecto");


        for ($j = 0; $j<count($DATA[$i]->DESTINOS); $j++) {
          $id_destino = $DATA[$i]->DESTINOS[$j]->id;
          $lugar = $DATA[$i]->DESTINOS[$j]->lugar;
          $fecha = $DATA[$i]->DESTINOS[$j]->fecha;
          $hora = $DATA[$i]->DESTINOS[$j]->hora;
          $lugar_hospeda = $DATA[$i]->DESTINOS[$j]->lugar_hospeda;

          if($id_destino == 0){
            $arrU = array("id_prospecto"=>$id_prospecto, "id_cotizacion"=>0, "id_contrato"=>0, "id_unidPros"=>$DATA[$i]->id, "lugar"=>$lugar, "fecha"=>$fecha, "hora"=>$hora, "lugar_hospeda"=>$lugar_hospeda, "reg"=>$this->fechahoy);
            $idDestino = $this->ModeloGeneral->tabla_inserta("destino_prospecto",$arrU);
          }else{
            $arrU = array("lugar"=>$lugar, "fecha"=>$fecha, "hora"=>$hora, "lugar_hospeda"=>$lugar_hospeda);
            $idDestino = $this->ModeloGeneral->updateCatalogo_value($arrU, array('id'=>$id_destino), "destino_prospecto");
          }
        }
      }
    }
    echo $result;
  }

}
