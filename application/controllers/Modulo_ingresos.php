<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Modulo_ingresos extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloSession');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloUsuarios');
    $this->load->model('ModeloModIngresos');

    $this->idpersonal = $this->session->userdata('idpersonal');
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
    $this->fecha_reciente = date('Y-m-d');
    if ($this->session->userdata('logeado')) {
      $this->idpersonal = $this->session->userdata('idpersonal_tz');
      $this->perfilid = $this->session->userdata('perfilid_tz');
      $permiso = $this->ModeloSession->getviewpermiso($this->perfilid, 1);
      if ($permiso == 0) {
        redirect('Login');
      }
    } else {
      redirect('/Login');
    }
  }

  public function index()
  {
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('moduloIngresos/index');
    $this->load->view('templates/footer');
    $this->load->view('moduloIngresos/index_js');
  }

  public function registro($id = 0)
  {
    $data['title_heades'] = 'Nuevo módulo de ingresos';
    $data['title_save'] = 'Guardar datos';
    //$data['personal'] = $this->ModeloGeneral->getselectwhere2('personal',array('estatus'=>1));

    if ($id > 0) {
      $data['title_heades'] = 'Editar módulo de gastos';
      $data['title_save'] = 'Actualizar datos';

      $data['idMD'] = $id;
      $result = $this->ModeloGeneral->getselectwhererow2('modulo_ingresos', array('id' => $id, 'estatus' => 1));
      $data['concepto'] = $result->concepto;
      $data['responsable'] = $result->responsable;
      $data['folio'] = $result->folio;
      $data['descripcion'] = $result->descripcion;
      $data['monto'] = $result->monto;
      $data['fecha'] = $result->fecha;
    } 

    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('moduloIngresos/form', $data);
    $this->load->view('templates/footer');
    $this->load->view('moduloIngresos/form_js');
  }

  public function insert()
  {
    $data = $this->input->post();
    $id = $data['id'];
    unset($data['id']);
    $id_reg = 0;

    if ($id > 0) {
      $this->ModeloGeneral->updateCatalogo($data, 'id', $id, 'modulo_ingresos');
      $id_reg = $id;
    } else {
      $data['reg'] = $this->fechahoy;
      $data['usuario'] = $this->idpersonal;

      $id_reg = $this->ModeloGeneral->tabla_inserta('modulo_ingresos', $data);
    }

    echo $id_reg;
  }


  public function get_list()
  {
    $params = $this->input->post();
    $getdata = $this->ModeloModIngresos->get_result($params);
    $totaldata = $this->ModeloModIngresos->total_result($params);
    $json_data = array(
      "draw"            => intval($params['draw']),
      "recordsTotal"    => intval($totaldata),
      "recordsFiltered" => intval($totaldata),
      "data"            => $getdata->result(),
      "query"           => $this->db->last_query()
    );
    echo json_encode($json_data);
  }

  public function delete()
  {
    $id = $this->input->post('id');
    $data = array('estatus' => 0);
    $result = $this->ModeloGeneral->updateCatalogo($data, 'id', $id, 'modulo_ingresos');
    echo $result;
  }

//---Caja chica-------------------------------------------------->
  public function corteCajaChica()
  {
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('moduloIngresos/corte_caja_chica');
    $this->load->view('templates/footer');
    $this->load->view('moduloIngresos/corte_caja_chica_js');
  }

  public function get_list_movimientos()
  {

    $params = $this->input->post();
    $getdata = $this->ModeloModIngresos->get_result_cortec($params);
    $totalRecords1 = $this->ModeloModIngresos->total_result_cortec1($params);
    $totalRecords2 = $this->ModeloModIngresos->total_result_cortec2($params);


    $json_data = array(
      "draw"            => intval($params['draw']),
      "recordsTotal"    => intval($totalRecords1) + intval($totalRecords2),
      "recordsFiltered" => intval($totalRecords1) + intval($totalRecords2),
      "data"            => $getdata->result(),
      "query"           => $this->db->last_query()
    );
    log_message('error','error: '.json_encode($json_data));
    echo json_encode($json_data);
  }

  public function get_totales()
  {
    $params = $this->input->post();

    $dataIngresos = $this->ModeloModIngresos->get_total_ingresos($params);
    $dataEgresos = $this->ModeloModIngresos->get_total_egresos($params);
    $totalFinal = ($dataIngresos - $dataEgresos);

    $arrayResult = array(
      "totalIngresos" => $dataIngresos,
      "totalEgresos" => $dataEgresos,
      "totalFinal" => $totalFinal
    );

    //log_message('error', 'RESULT: ' . json_encode($arrayResult));
    echo json_encode($arrayResult);
  }

  public function exportCorteCaja($fechaIni, $fechaFin)
  {
    $params["fechaIni"] = $fechaIni;
    $params["fechaFin"] = $fechaFin;

    $data["table_movimientos"] = $this->ModeloModIngresos->get_data_cortec_excel($params);
    //log_message('error', 'TABLA MOVIMIENTOS: ' . json_encode($data["table_movimientos"]->result()));

    //---Totales-------------------
    $dataIngresos = $this->ModeloModIngresos->get_total_ingresos($params);
    $dataEgresos = $this->ModeloModIngresos->get_total_egresos($params);
    $totalFinal = ($dataIngresos - $dataEgresos);

    $dataTotales = array(
      "totalIngresos" => $dataIngresos,
      "totalEgresos" => $dataEgresos,
      "totalFinal" => $totalFinal
    );

    $data["table_totales"] = json_decode(json_encode($dataTotales));
    log_message('error', 'TABLA TOTALES: ' . json_encode($data["table_totales"]));
    //---Totales-------------------

    $this->load->view('moduloIngresos/corte_caja_chica_excel',$data);
  }

}
