<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cuentas extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloSession');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloClientes');
    $this->idpersonal=$this->session->userdata('idpersonal');
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
    $this->fecha_reciente = date('Y-m-d');
    if ($this->session->userdata('logeado')){
      $this->idpersonal=$this->session->userdata('idpersonal_tz');
      $this->perfilid=$this->session->userdata('perfilid_tz');
      $permiso=$this->ModeloSession->getviewpermiso($this->perfilid,9);
      if ($permiso==0) {
        redirect('Login');
      }
    }else{
      redirect('/Login');
    }
  }

  public function index(){
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('cuentas/index');
    $this->load->view('templates/footer');
    $this->load->view('cuentas/indexjs');
  }

  public function registro($id=0){
    if($id==0){
      $data['tittle']='Alta';     
    }else{
      $data['tittle']='Edición'; 
      $data["c"]=$this->ModeloGeneral->getselectwhererow2('cuentas',array('id'=>$id));
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('cuentas/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('cuentas/formjs');
  }

  public function guardar(){
    $datos = $this->input->post();
    $id=$datos['id'];
    unset($datos['id']);
    $id_reg=0;
    if($id>0){
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,'cuentas');
      $id_reg=$id; 
    }else{
      $id_reg=$this->ModeloGeneral->tabla_inserta('cuentas',$datos);
    }  
    echo $id_reg;
  }

  public function getlistado(){
    $params = $this->input->post();
    $datas = $this->ModeloGeneral->getselectwhere_n_consulta("cuentas",array("estatus"=>1));
    $json_data = array("data" => $datas);
    echo json_encode($json_data);
  }
  
  public function delete(){
    $id = $this->input->post('id');
    $data = array('estatus' => 0);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,"cuentas");
    echo $resul;
  }

}