<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Encuesta extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloSession');
        $this->load->model('ModeloGeneral');
        $this->load->model('ModeloContratos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
    }

    public function index($idCliente=0, $idContrato=0, $token=''){
        $data["idCli"] = $idCliente;
        $data["idCon"] = $idContrato;

        $tokenMod = str_replace('_', '%', $token);
        $tokenDeco = urldecode($tokenMod);
        
        
        $get_info = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id' => $idCliente));
        if($get_info === null){
            $nombre = '';
        }else{
            $nombre = $get_info->nombre.$get_info->app.$get_info->apm;
        }
        

        $get_cont = $this->ModeloGeneral->getselectwhererow2('contratos',array('id' => $idContrato));
        if($get_cont === null){
            $finalizada = 0;
        }else{
            $finalizada = $get_cont->encuesta;
        }

        $check = $this->ModeloContratos->check_token($nombre, $idContrato ,$tokenDeco);

        if($check > 0 && $finalizada == 0){
            $this->load->view('templates/header');
            $this->load->view('encuesta/index',$data);
            $this->load->view('templates/footerEncuesta');
            $this->load->view('encuesta/indexjs');
        }else if($check > 0 && $finalizada == 1){
            $this->load->view('templates/header');
            $this->load->view('encuesta/complete');
            $this->load->view('templates/footerEncuesta');
        }else{
            $this->load->view('templates/header');
            $this->load->view('encuesta/not_found');
            $this->load->view('templates/footerEncuesta');
        }
    }

    public function insert(){
        $datos = $this->input->post();
        $datos['reg'] = $this->fechahoy;
        $id = $this->ModeloGeneral->tabla_inserta('encuesta',$datos);
        //log_message('error','DatosEncuesta: '.json_encode($datos));
        
        if($id > 0){
            $this->ModeloGeneral->updateCatalogo(array("encuesta"=>1),'id',$datos['id_contrato'],"contratos");
        }

        echo $id;
    }
}

