<?php
defined('BASEPATH') or exit('No direct script access allowed');

class WhatsApp extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloSession');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloContratos');
    $this->load->model('ModeloCotizaciones');
    $this->idpersonal=$this->session->userdata('idpersonal');
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
    $this->fecha_reciente = date('Y-m-d');
  }


  public function cotizacion($id = 0)
  {
    //$data['usuario'] = $this->ModeloGeneral->getselectwhererow2('personal',array('personalId'=>$this->session->userdata('idpersonal_tz'),'estatus'=>1));
    $jsonData = '{
      "personalId": "0",
      "nombre": "",
      "apellido_p": "",
      "apellido_m": "",
      "telefono": "",
      "direccion": "",
      "correo": "",
      "fecha_ingreso": "",
      "puesto": "",
      "estatus": "",
      "reg": "2023-08-03 11:39:46"
    }';
    
    $data['usuario'] = json_decode($jsonData);

    //log_message('error', 'usuario: '.json_encode($data['usuario']));

    $data['fecha_hora_imp'] = '';
    $data['printLabels'] = false;

    $data['cotizacion'] = $this->ModeloGeneral->getselectwhererow2('cotizaciones',array('id'=>$id,'estatus'=>1));
    
    $data['prospecto'] = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id'=>$data['cotizacion']->idCliente,'estatus'=>1));

    $unidades = $this->ModeloCotizaciones->getUnidadesCotizacion($id);
    $data['unidades'] = $unidades->result();

    foreach ($data['unidades'] as $dato) {
      $destinos = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_prospecto'=>$data['cotizacion']->idCliente, "id_unidPros"=>$dato->id, "estatus"=>1));
      $dato->destinos = $destinos->result();
    }
    //log_message('error', 'UNIDADES: '.json_encode($data['unidades']));

    $data['personal'] = $this->ModeloGeneral->getselectwhererow2('personal',array('personalId'=>$data['cotizacion']->personal,'estatus'=>1));

    $length = 5;
    $format = substr(str_repeat(0, $length) . ($id), -$length);

    $anio = date("Y");
    $anio_2d = substr($anio, -2); //Los dos últimos dígitos

    $data['folio'] = 'GT'.$anio_2d.'-'.$format;
    
    $this->load->view('reportes/documento_cotizacion', $data);
  }

  
  public function contrato($id = 0){
    $data['contratos'] = $this->ModeloGeneral->getselectwhererow2('contratos',array('id'=>$id,'estatus'=>1));
    $data['clientes'] = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id'=>$data['contratos']->idCliente));
    $data['detalles'] = $this->ModeloGeneral->getselectwhererow2('prospectos_clientes_d',array('id_contrato'=>$id));
    $data['destinos'] = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_contrato'=>$id,'estatus'=>1));
    $data['pagos'] = $this->ModeloGeneral->getselectwhererow2('pagos_cliente',array('id_cliente'=>$data['contratos']->idCliente,'estatus'=>1));

    $data['estado_detalles'] = $this->ModeloGeneral->getselectwhererow2('estados',array('EstadoId'=>$data['detalles']->estado));
    $data['estado_detalles'] = $data['estado_detalles']->Nombre;
    
    $data['estado_clientes'] = $this->ModeloGeneral->getselectwhererow2('estados',array('EstadoId'=>$data['clientes']->estado));
    $data['estado_clientes'] = $data['estado_clientes']->Nombre;

    $data['unidades'] = $this->ModeloContratos->getUnidadesContrato($id);
    //log_message('error','Unidad:'.json_encode($data['unidades']->result()));
    $data['personal'] = $this->ModeloGeneral->getselectwhererow2('personal',array('personalId'=>$data['contratos']->personal,'estatus'=>1));

    $data['folio'] = $id;

    $length = 3;
    $data['folio_cot'] = '--';
    $idCot = $this->ModeloContratos->getIdCotizacion($id);
    if($idCot->cotizacion != 0){
      $format = substr(str_repeat(0, $length) . ($idCot->id_cotizacion), -$length);
      $data['folio_cot'] = 'COT-' . $format;
    }
    //log_message('error','Cotizacion-> '.json_encode($data['folio']));

    $pagos = $this->ModeloGeneral->getselectwhere2('pagos_contrato',array('id_contrato'=>$id,"estatus"=>1));
    $pago = $pagos->result();
    $data['pago'] = 0;

    foreach ($pago as $pag) {
      $data['pago'] = $data['pago'] + $pag->monto;
    }
    //log_message('error','PAGOS-> '.json_encode($data['pagos']));
    $this->load->view('reportes/documento_contrato', $data);
  }


}
