<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Unidades extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloSession');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloUnidades');
    $this->idpersonal=$this->session->userdata('idpersonal');
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
    $this->fecha_reciente = date('Y-m-d');
    if ($this->session->userdata('logeado')){
      $this->idpersonal=$this->session->userdata('idpersonal_tz');
      $this->perfilid=$this->session->userdata('perfilid_tz');
      $permiso=$this->ModeloSession->getviewpermiso($this->perfilid,2);
      if ($permiso==0) {
        redirect('Login');
      }
    }else{
      redirect('/Login');
    }
  }

  public function index()
  {
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('unidades/index');
    $this->load->view('templates/footer');
    $this->load->view('unidades/indexjs');
  }

  public function registro($id=0)
  {
    if($id==0){
      $data['tittle']='Alta';     
    }else{
      $data['tittle']='Edición'; 
      $data["u"]=$this->ModeloGeneral->getselectwhererow2('unidades',array('id'=>$id));
      $data["serv"]=$this->ModeloGeneral->getselectwhere2Order('servicio_unidades',array("id_unidad"=>$id,'estatus'=>1),'fecha');
      $data["verif"]=$this->ModeloGeneral->getselectwhere2Order('verificacion_unidades',array("id_unidad"=>$id,'estatus'=>1),'fecha');
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('unidades/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('unidades/formjs');
  }

  public function guardar(){
    $datos = $this->input->post();
    $id=$datos['id'];
    unset($datos['id']);
    $id_reg=0;
    if($id > 0){
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,'unidades');
      $id_reg=$id; 
    }else{
      $datos['fecha_reg']=$this->fechahoy;
      $id_reg=$this->ModeloGeneral->tabla_inserta('unidades',$datos);
    }  
    echo $id_reg;
  }

  public function getlistado(){
    $params = $this->input->post();
    $params["tipo"] = 0;
    $getdata = $this->ModeloUnidades->get_result($params);
    $totaldata= $this->ModeloUnidades->total_result($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }
  
  public function delete(){
    $id = $this->input->post('id');
    $tabla = $this->input->post('tabla');
    $data = array('estatus' => 0);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,$tabla);
    echo $resul;
  }

  public function insertServicios(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);

    //log_message('error',"Servicios ".json_encode($DATA));
    
    for ($i=0; $i<count($DATA); $i++) {
      $id_unidad = $DATA[$i]->id_unidad;
      $fecha = $DATA[$i]->fecha;
      $kilometraje = $DATA[$i]->kilometraje;
      $tipo = $DATA[$i]->tipo;
      $importe = $DATA[$i]->importe;
      $comentarios = $DATA[$i]->comentarios;

      $arr = array("id_unidad"=>$id_unidad,"fecha"=>$fecha,"kilometraje"=>$kilometraje,"tipo"=>$tipo,"importe"=>$importe,"comentarios"=>$comentarios);
      if($DATA[$i]->id == 0){
        $result = $this->ModeloGeneral->tabla_inserta("servicio_unidades",$arr);
      }else{
        $result = $this->ModeloGeneral->updateCatalogo_value($arr, array('id'=>$DATA[$i]->id), "servicio_unidades");
      }
    }
    echo $datos;
  }


  public function deleteServicio(){
    $id = $this->input->post('id');
    $this->ModeloGeneral->updateCatalogo(array('estatus' => 0),'id',$id,'servicio_unidades');
    echo $id;
  }

  function cargar_imagen(){
    $unidad = $_POST['id_unidad'];
    $tipo = $_POST['tipo'];
    //$input_name = 'img';
    if($tipo==1){
      $DIR_SUC = FCPATH . 'public/uploads/unidades';
      $name="img";
      $input_name = 'img';
    }else{
      $DIR_SUC = FCPATH . 'public/uploads/unidades/polizas';
      $name="img_poliza";
      $input_name = 'img_poliza';
    }
    //log_message('error', "tipo: " . $tipo);
    $config['upload_path']     = $DIR_SUC;
    $config['allowed_types']   = 'gif|jpg|png|jpeg|bmp|pdf';
    $config['max_size']        = 5000;
    $file_names = 'u'.$unidad.'_'.date('YmdGis');
    $config['file_name'] = $file_names;
    $output = [];

    $this->load->library('upload', $config);
    if (!$this->upload->do_upload($input_name)) {
      $data = array('error' => $this->upload->display_errors());
      //log_message('error', "DOC: " . json_encode($data));
    } else {
      $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
      $file_name = $upload_data['file_name']; //uploded file name
      $extension = $upload_data['file_ext'];    // uploded file extension
      $this->ModeloGeneral->updateCatalogo_value(array($name=>$file_name),array('id'=>$unidad),'unidades');
      $data = array('upload_data' => $this->upload->data());
    }
    echo json_encode($output);
  }

  function delete_img($id,$tipo){
    if($tipo==1){
      $name="img";
    }else{
      $name="img_poliza";
    }
    $result = $this->ModeloGeneral->updateCatalogo_value(array($name=>''),array('id'=> $id),'unidades');
    echo $id;
  }

  public function insertVerificaciones(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    //log_message('error',"Verificacion".json_encode($DATA));
    
    for ($i=0; $i<count($DATA); $i++) {
      $id_unidad = $DATA[$i]->id_unidad;
      $fecha = $DATA[$i]->fecha;
      $tipo = $DATA[$i]->tipo;
      $importe = $DATA[$i]->importe;
      $observaciones = $DATA[$i]->observaciones;

      $arr = array("id_unidad"=>$id_unidad,"fecha"=>$fecha,"tipo"=>$tipo,"importe"=>$importe,"observaciones"=>$observaciones);
      if($DATA[$i]->id == 0){
        $result = $this->ModeloGeneral->tabla_inserta("verificacion_unidades",$arr);
      }else{
        $result = $this->ModeloGeneral->updateCatalogo_value($arr, array('id'=>$DATA[$i]->id), "verificacion_unidades");
      }
    }
    echo $datos;
  }

  public function deleteVerificacion(){
    $id = $this->input->post('id');
    $this->ModeloGeneral->updateCatalogo(array('estatus' => 0),'id',$id,'verificacion_unidades');
    echo $id;
  }

  function cargarImgVerificacion($id){
    $upload_folder ='public/uploads/unidades/verificaciones';
    $comp_name="img_verif";

    $nombre_archivo = $_FILES['foto']['name'];
    $tipo_archivo = $_FILES['foto']['type'];
    $tamano_archivo = $_FILES['foto']['size'];
    $tmp_archivo = $_FILES['foto']['tmp_name'];
    //$archivador = $upload_folder . '/' . $nombre_archivo;
    $fecha=date('Ymd_His');
    $nombre_archivo=str_replace(' ', '_', $nombre_archivo);
    $nombre_archivo=str_replace('-', '_', $nombre_archivo);
    //$nombre_archivo=$this->eliminar_acentos($nombre_archivo);

    $newfile=$fecha.'_'.$nombre_archivo;        
    $archivador = $upload_folder . '/'.$newfile;
    
    if (!move_uploaded_file($tmp_archivo, $archivador)) {
        $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
    }else{
      $this->ModeloGeneral->updateCatalogo(array($comp_name=>$newfile),'id',$id,"verificacion_unidades");
      //$this->redimensionar($upload_folder,$newfile);
      $return = Array('id'=>$id,'ok'=>TRUE,"name"=>$newfile);
    }
    echo json_encode($return);
  }

  function redimensionar($src,$name){
    $imagen = $src; //Imagen original
    $imagenNueva = FCPATH.'public/uploads/unidades/verificaciones/redimen/'.$name; //Nueva imagen
    //$nAncho = 105; //Nuevo ancho
    //$nAlto = 75;  //Nuevo alto
    //log_message('error','name: '. $name);
    //log_message('error','imagenNueva: '. $imagenNueva);
    //Creamos una nueva imagen a partir del fichero inicial
    /*if(mb_strtolower(pathinfo($name,PATHINFO_EXTENSION))=="jpg" || mb_strtolower(pathinfo($name,PATHINFO_EXTENSION))=="jpeg" || mb_strtolower(pathinfo($name,PATHINFO_EXTENSION))=="webp"){
        $imagen = imagecreatefromjpeg($imagen); 
    }else if(mb_strtolower(pathinfo($name,PATHINFO_EXTENSION))=="png"){
      $imagen = imagecreatefrompng($imagen); 
    }*/
    if(strtolower(pathinfo($name,PATHINFO_EXTENSION))=="jpg" || strtolower(pathinfo($name,PATHINFO_EXTENSION))=="jpeg" || strtolower(pathinfo($name,PATHINFO_EXTENSION))=="webp"){
        $imagen = imagecreatefromjpeg($imagen); 
    }else if(strtolower(pathinfo($name,PATHINFO_EXTENSION))=="png"){
      $imagen = imagecreatefrompng($imagen); 
    }

    //Obtenemos el tamaño 
    $x = imagesx($imagen);
    $y = imagesy($imagen);
    
    $nAncho = 260;
    $nAlto = 180;
    // Crear una nueva imagen, copia y cambia el tamaño de la imagen
    $img = imagecreatetruecolor($nAncho, $nAlto);
    imagecopyresized($img, $imagen, 0, 0, 0, 0, $nAncho, $nAlto, $x, $y);
    
    //Creamos el archivo jpg
    imagejpeg($img, $imagenNueva);
    return $name;
}

  public function get_data_verificaciones(){
    $id = $this->input->post('id');
    $unidades = $this->ModeloGeneral->getselectwhere2('verificacion_unidades',array('id_unidad'=>$id,'estatus'=>1));
    $data['unidades'] = $unidades->result();
    echo json_encode($data['unidades']);
  }

  public function get_data_img(){
    $id = $this->input->post('id');
    $img="";
    $unidades = $this->ModeloGeneral->getselectwhere2('verificacion_unidades',array('id'=>$id,'estatus'=>1));
    foreach($unidades->result() as $u){
      $img = $u->img_verif;
    }
    echo $img;
  }

  public function exportExcel($id=0, $type=0){
    $tipos = array('---','Físico mecánica','Contaminantes');
    $tipo_s = array('---','Mantenimiento Preventivo','Mantenimiento Correctivo');
    $uni = "";
    $serv = "";
    $verf = "";
    $tot_ser=0; $tot_verif=0;
    if($id > 0){
      $unidad = $this->ModeloGeneral->getselectwhererow2('unidades',array('id'=>$id));
      $servicios = $this->ModeloGeneral->getselectwhere2Order('servicio_unidades',array("id_unidad"=>$id,'estatus'=>1),'fecha');
      $verificaciones = $this->ModeloGeneral->getselectwhere2Order('verificacion_unidades',array("id_unidad"=>$id,'estatus'=>1),'fecha');
      //log_message('error','Servicios: '. json_encode($servicios->result()));
      //log_message('error','Verificacion: '. json_encode($verificaciones->result()));
      //log_message('error','Unidad: '. json_encode($unidad));

      $uni = "<tr>
              <td>".$unidad->num_eco."</td>
              <td>".$unidad->vehiculo."</td>
              <td>".$unidad->modelo."</td>
              <td>".$unidad->placas."</td>
              <td>".$unidad->placas_fed."</td>
              <td>".$unidad->serie."</td>
              <td>".$unidad->motor."</td>
              <td>".$unidad->prox_ver."</td>
              <td>".$unidad->km_actual."</td>
          </tr>";
      $cont=0;
      foreach ($servicios->result() as $servicio) {
        $cont++;
        $serv .= "<tr>
              <td>".$cont."</td>
              <td>".$servicio->fecha."</td>
              <td>".$servicio->kilometraje."</td>
              <td>".$tipo_s[$servicio->tipo]."</td>
              <td>".number_format($servicio->importe)."</td>
              <td>".$servicio->comentarios."</td>
          </tr>";
        $tot_ser=$tot_ser+$servicio->importe;
      }
      $cont2=0;
      foreach ($verificaciones->result() as $verificacion) {
        $cont2++;
        $img = "";
        if($verificacion->img_verif!=""){
          $uri=base_url().'public/uploads/unidades/verificaciones/'.$verificacion->img_verif;
          $img_med=$this->redimensionar($uri,$verificacion->img_verif);
          $img='<img src="'.base_url().'public/uploads/unidades/verificaciones/redimen/'.$img_med.'">';
        }
        $verf .= "<tr>
              <td>".$cont2."</td>
              <td colspan='4' style='align-items: center; justify-content: center'><p class='pspaces'></p>
                <table>
                  <tr> <td colspan='4' width='100%'></td></tr>
                  <tr>
                    <td colspan='3' width='80%'>".$img."</td>
                    <td width='20%'></td>
                  </tr>
                </table><br><br><br><br><br><br><br><br><br>
              </td>
              <td>".$verificacion->fecha."</td>
              <td>".$tipos[$verificacion->tipo]."</td>
              <td>".number_format($verificacion->importe)."</td>
              <td>".$verificacion->observaciones."</td>
          </tr>";
        $tot_verif=$tot_verif+$verificacion->importe;
      }
    }

    $data["uni"] = $uni;
    $data["serv"] = $serv;
    $data["verf"] = $verf;
    $data["type"] = $type;
    $data["tot_ser"] = $tot_ser;
    $data["tot_verif"] = $tot_verif;

    $this->load->view('reportes/documento_serv_verif',$data);
  }

  public function exportExcelAll($fi,$ff){
    $data["fi"]=$fi;
    $data["ff"]=$ff;
    $this->load->view('reportes/documento_serv_verif_all',$data);
  }
  
  public function exportExcelKms($fi,$ff){
    $data["fi"]=$fi;
    $data["ff"]=$ff;
    $this->load->view('reportes/documento_kms_contratos',$data);
  }

  function deleteImgVerifica($id){
    $result = $this->ModeloGeneral->updateCatalogo_value(array('img_verif'=>''),array('id'=> $id),'verificacion_unidades');
    echo $id;
  }
}

