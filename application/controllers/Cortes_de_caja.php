<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cortes_de_caja extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloSession');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloUsuarios');
    $this->load->model('ModeloCorteCaja');

    $this->idpersonal = $this->session->userdata('idpersonal');
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
    $this->fecha_reciente = date('Y-m-d');
    if ($this->session->userdata('logeado')) {
      $this->idpersonal = $this->session->userdata('idpersonal_tz');
      $this->perfilid = $this->session->userdata('perfilid_tz');
      $permiso = $this->ModeloSession->getviewpermiso($this->perfilid, 1);
      if ($permiso == 0) {
        redirect('Login');
      }
    } else {
      redirect('/Login');
    }
  }

  public function index()
  {
    $data['contratos'] = $this->ModeloGeneral->getselectwhere2('contratos', array('estatus' => 1));
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('corte_caja/index', $data);
    $this->load->view('templates/footer');
    $this->load->view('corte_caja/index_js');
  }

  public function get_list_anticipo()
  {
    $params = $this->input->post();
    $getdata = $this->ModeloCorteCaja->get_result_anticipo($params);
    $totaldata = $this->ModeloCorteCaja->total_result_anticipo($params);
    $json_data = array(
      "draw"            => intval($params['draw']),
      "recordsTotal"    => intval($totaldata),
      "recordsFiltered" => intval($totaldata),
      "data"            => $getdata->result(),
      "query"           => $this->db->last_query()
    );
    echo json_encode($json_data);
  }

  public function get_list_pagos()
  {
    $params = $this->input->post();
    $getdata = $this->ModeloCorteCaja->get_result_pagos($params);
    $totaldata = $this->ModeloCorteCaja->total_result_pagos($params);
    $json_data = array(
      "draw"            => intval($params['draw']),
      "recordsTotal"    => intval($totaldata),
      "recordsFiltered" => intval($totaldata),
      "data"            => $getdata->result(),
      "query"           => $this->db->last_query()
    );
    echo json_encode($json_data);
  }


  public function get_list_gastos()
  {
    $params = $this->input->post();
    $getdata = $this->ModeloCorteCaja->get_result_gastos($params);
    $totaldata = $this->ModeloCorteCaja->total_result_gastos($params);
    $json_data = array(
      "draw"            => intval($params['draw']),
      "recordsTotal"    => intval($totaldata),
      "recordsFiltered" => intval($totaldata),
      "data"            => $getdata->result(),
      "query"           => $this->db->last_query()
    );
    echo json_encode($json_data);
  }

  public function search_cliente()
  {
    $search = $this->input->get('search');
    $results = $this->ModeloCorteCaja->search_cliente($search);
    echo json_encode($results);
  }

  public function get_cuentas()
  {
    $params = $this->input->post();
    $arrayResult = array();

    $dataAnticipos = $this->ModeloCorteCaja->get_anticipos_totales_corte_contrato($params);
    $dataPagosEfectivo = $this->ModeloCorteCaja->get_pagos_efectivo_corte_contrato($params);
    $dataPagosCuentas = $this->ModeloCorteCaja->get_pagos_cuentas_corte_contrato($params);

    //$anticipo = array("pagos_cuenta"=>$dataAnticipos->anticipo, "cuenta"=>"ANTICIPO");
    //$efectivo = array("pagos_cuenta"=>$dataPagosEfectivo->pagos_efectivo, "cuenta"=>"EFECTIVO");
    $efectivo = array("pagos_cuenta" => $dataPagosEfectivo->pagos_efectivo + $dataAnticipos->anticipo, "cuenta" => "EFECTIVO");

    //array_unshift($dataPagos, $anticipo);
    array_unshift($dataPagosCuentas, $efectivo);

    //log_message('error', 'dataAnticipos-> ' . json_encode($dataAnticipos));
    //log_message('error', 'DATA_PagosE-> ' . json_encode($dataPagosEfectivo));
    //log_message('error', 'DATA_PagosC-> ' . json_encode($dataPagosCuentas));
    //log_message('error', 'DATA_Result-> ' . json_encode($arrayResult));
    echo json_encode($dataPagosCuentas);
  }

  public function get_totales()
  {
    $params = $this->input->post();

    $dataAnticipos = 0;
    $dataPagos = 0;
    $dataGastos = 0;
    $totalFinal = 0;
    $cliente = '';
    $arrayResult = array();

    //Total global
    $dataAnticipos = $this->ModeloCorteCaja->get_anticipos_totales_corte_contrato($params);
    $dataPagos = $this->ModeloCorteCaja->get_pagos_totales_corte_contrato($params);
    $dataGastos = $this->ModeloCorteCaja->get_gastos_totales_corte_contrato($params);
    $totalFinal = ($dataAnticipos->anticipo + $dataPagos->pagos) - ($dataAnticipos->descuento + $dataGastos->gastos);

    $arrayResult = array(
      "contrato" => $params['idContrato'],
      "descuento" => $dataAnticipos->descuento,
      "totalAnticipos" => $dataAnticipos->anticipo,
      "totalPagos" => $dataPagos->pagos,
      "totalGastos" => $dataGastos->gastos,
      "totalFinal" => $totalFinal,
      "totalEfectivo" => $dataPagos->pagos_efectivo
    );

    //log_message('error', 'dataAnticipos-> ' . json_encode($dataAnticipos));
    //log_message('error', 'dataPagos-> ' . json_encode($dataPagos));
    //log_message('error', 'dataGastos-> ' . json_encode($dataGastos));

    /*
    if($params["idContrato"] != "0") {
      //Unico contrato
      $dataAnticipos = $this->ModeloCorteCaja->get_anticipos_totales_corte_contrato($params);
      $dataPagos = $this->ModeloCorteCaja->get_pagos_totales_corte_contrato($params);
      $dataGastos = $this->ModeloCorteCaja->get_gastos_totales_corte_contrato($params);
      $totalFinal = ($dataAnticipos->anticipo + $dataPagos->pagos)-($dataAnticipos->descuento + $dataGastos->gastos);
      $cliente = $this->ModeloCorteCaja->get_clientes_from_contrato($params["idContrato"]);
      log_message('error','CLIENTE-> '. json_encode($cliente));

      $arrayR = array(
        "contrato" => $params['idContrato'],
        "cliente" => $cliente,
        "descuento" => $dataAnticipos->descuento,
        "totalAnticipos" => $dataAnticipos->anticipo, 
        "totalPagos" => $dataPagos->pagos, 
        "totalGastos" => $dataGastos->gastos, 
        "totalFinal" => $totalFinal
      );

      array_push($arrayResult, $arrayR);

    }else{
      //Multiples contrato
      $arrayR = array();
      $getContratos = $this->ModeloCorteCaja->get_contratos_corte($params);
      log_message('error','CONTRATOS-> '. json_encode($getContratos));
      

      foreach ($getContratos as $contrato) {
        log_message('error','CONTRATO-> '. json_encode($contrato));
        $params['idContrato'] = $contrato->id;

        $dataAnticipos = $this->ModeloCorteCaja->get_anticipos_totales_corte_contrato($params);
        $dataPagos = $this->ModeloCorteCaja->get_pagos_totales_corte_contrato($params);
        $dataGastos = $this->ModeloCorteCaja->get_gastos_totales_corte_contrato($params);
        $totalFinal = ($dataAnticipos->anticipo + $dataPagos->pagos)-($dataAnticipos->descuento + $dataGastos->gastos);
        $cliente = $this->ModeloCorteCaja->get_clientes_from_contrato($params['idContrato']);
        log_message('error','CLIENTE-> '. json_encode($cliente));

        $arrayR = array(
          "contrato" => $params['idContrato'],
          "cliente" => $cliente,
          "descuento" => $dataAnticipos->descuento,
          "totalAnticipos" => $dataAnticipos->anticipo, 
          "totalPagos" => $dataPagos->pagos, 
          "totalGastos" => $dataGastos->gastos, 
          "totalFinal" => $totalFinal
        );

        array_push($arrayResult, $arrayR);
      }
    }
    */

    //log_message('error', 'RESULT-> ' . json_encode($arrayResult));
    echo json_encode($arrayResult);
  }

  public function exportCorteCaja($idContrato, $idCliente, $fechaIni, $fechaFin, $tipo)
  {
    $params["idContrato"] = $idContrato;
    $params["idCliente"] = $idCliente;
    $params["fechaIni"] = $fechaIni;
    $params["fechaFin"] = $fechaFin;

    $data["table_anticipo"] = $this->ModeloCorteCaja->get_table_anticipo($params);
    //log_message('error', 'TABLA ANTICIPO: ' . json_encode($data["table_anticipo"]));

    $data["table_pagos"] = $this->ModeloCorteCaja->get_table_pagos($params);
    ///log_message('error', 'TABLA PAGOS: ' . json_encode($data["table_pagos"]));

    $data["table_gastos"] = $this->ModeloCorteCaja->get_table_gastos($params);
    //log_message('error', 'TABLA GASTOS: ' . json_encode($data["table_gastos"]));


    //Totales
    $dataAnticipos = $this->ModeloCorteCaja->get_anticipos_totales_corte_contrato($params);
    $dataPagos = $this->ModeloCorteCaja->get_pagos_totales_corte_contrato($params);
    $dataGastos = $this->ModeloCorteCaja->get_gastos_totales_corte_contrato($params);
    $totalFinal = ($dataAnticipos->anticipo + $dataPagos->pagos) - ($dataAnticipos->descuento + $dataGastos->gastos);

    $datatotales = array(
      "descuento" => $dataAnticipos->descuento,
      "totalAnticipos" => $dataAnticipos->anticipo,
      "totalPagos" => $dataPagos->pagos,
      "totalGastos" => $dataGastos->gastos,
      "totalFinal" => $totalFinal,
      "totalEfectivo" => $dataPagos->pagos_efectivo
    );

    $data["table_totales"] = json_decode(json_encode($datatotales));
    //log_message('error', 'TABLA TOTALES: ' . json_encode($data["table_totales"]));
    //Totales

    //Cuentas
    $dataAnticipos = $this->ModeloCorteCaja->get_anticipos_totales_corte_contrato($params);
    $dataPagosEfectivo = $this->ModeloCorteCaja->get_pagos_efectivo_corte_contrato($params);
    $dataPagosCuentas = $this->ModeloCorteCaja->get_pagos_cuentas_corte_contrato($params);

    $efectivo = array("pagos_cuenta" => $dataPagosEfectivo->pagos_efectivo + $dataAnticipos->anticipo, "cuenta" => "EFECTIVO");

    array_unshift($dataPagosCuentas, $efectivo);

    $data["table_cuentas"] = json_decode(json_encode($dataPagosCuentas));
    //log_message('error', 'TABLA CUENTAS: ' . json_encode($data["table_cuentas"]));
    //Cuentas

    
    //Tipo: 1 = Excel
    if ($tipo == 1) {
      $this->load->view('corte_caja/corte_caja_excel',$data);
    }
  }
}
