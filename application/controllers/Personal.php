<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Personal extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloSession');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloPersonal');
    $this->load->model('ModeloUsuarios');

    $this->idpersonal = $this->session->userdata('idpersonal');
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
    $this->fecha_reciente = date('Y-m-d');
    if ($this->session->userdata('logeado')) {
      $this->idpersonal = $this->session->userdata('idpersonal_tz');
      $this->perfilid = $this->session->userdata('perfilid_tz');
      $permiso = $this->ModeloSession->getviewpermiso($this->perfilid, 1);
      if ($permiso == 0) {
        redirect('Login');
      }
    } else {
      redirect('/Login');
    }
  }

  public function index()
  {
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('personal/index');
    $this->load->view('templates/footer');
    $this->load->view('personal/index_js');
  }

  public function registro($id = 0)
  {
    $data['perfiles'] = $this->ModeloGeneral->getCatalogo('perfiles', 'estatus = 1');

    $data['title_heades'] = 'Nuevo personal';
    $data['title_save'] = 'Guardar datos';

    if ($id > 0) {
      $data['title_heades'] = 'Editar personal';
      $data['title_save'] = 'Actualizar datos';

      $data['idP'] = $id;
      $result = $this->ModeloGeneral->getselectwhererow2('personal', array('personalId' => $id));
      $data['nombre'] = $result->nombre;
      $data['apellido_p'] = $result->apellido_p;
      $data['apellido_m'] = $result->apellido_m;
      $data['fecha_ingreso'] = $result->fecha_ingreso;
      $data['puesto'] = $result->puesto;
      $data['telefono'] = $result->telefono;
      $data['direccion'] = $result->direccion;

      $resultUser = $this->ModeloGeneral->getselectwhererow2('usuarios', array('personalId' => $id));
      $data['idusuario'] = $resultUser->UsuarioID;
      $data['perfilId'] = $resultUser->perfilId;
      $data['usuario'] = $resultUser->Usuario;
      $data['contrasena'] = 'xxxx';

      $resultDoc1 = $this->ModeloGeneral->getselectwhere2Order2('personal_documentos',array('personalId'=>$id, 'tipo'=>1),'documentoId','DESC');
      $resultDoc1 = $resultDoc1->row();
      if($resultDoc1 != null){
        if($resultDoc1->estatus > 0){
          $data['comprobante'] = $resultDoc1->file;
          $data['idDComp'] = $resultDoc1->documentoId;
        }
      }
      
      $resultDoc2 = $this->ModeloGeneral->getselectwhere2Order2('personal_documentos',array('personalId'=>$id, 'tipo'=>2),'documentoId','DESC');
      $resultDoc2 = $resultDoc2->row();
      if($resultDoc2 != null){
        if($resultDoc2->estatus > 0){
          $data['identificacion'] = $resultDoc2->file;
          $data['idDIden'] = $resultDoc2->documentoId;
        }
      }
    }

    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('personal/form', $data);
    $this->load->view('templates/footer');
    $this->load->view('personal/form_js');
  }

  public function insert()
  {
    $data = $this->input->post();
    //log_message('error', 'Personal-data: ' . json_encode($data));
    $id = $data['id'];
    unset($data['id']);
    $id_reg = 0;
    //Session---
    $idusuario = $data['idusuario'];
    unset($data['idusuario']);

    $usuario = $data['usuario'];
    unset($data['usuario']);

    $contrasena = $data['contrasena'];
    unset($data['contrasena']);

    $perfil = $data['perfilId'];
    unset($data['perfilId']);

    //Session---
    if ($id > 0) {
      $this->ModeloGeneral->updateCatalogo($data, 'personalId', $id, 'personal');
      $id_reg = $id;
    } else {
      $id_reg = $this->ModeloGeneral->tabla_inserta('personal', $data);
    }

    //Session---
    if ($idusuario > 0) {
      //$this->ModeloUsuarios->usuarios_update($idusuario, $usuario, $contrasena, $perfil, $id_reg);
      $this->ModeloUsuarios->usuariosPersonal_update($idusuario, $usuario, $contrasena, $perfil, $id_reg);
    } else {
      $this->ModeloUsuarios->usuarios_insert($usuario, $contrasena, $perfil, $id_reg, 0);
    }
    //Session---

    echo $id_reg;
  }


  public function get_list()
  {
    $params = $this->input->post();
    $getdata = $this->ModeloPersonal->get_result($params);
    $totaldata = $this->ModeloPersonal->total_result($params);
    $json_data = array(
      "draw"            => intval($params['draw']),
      "recordsTotal"    => intval($totaldata),
      "recordsFiltered" => intval($totaldata),
      "data"            => $getdata->result(),
      "query"           => $this->db->last_query()
    );
    echo json_encode($json_data);
  }

  public function delete()
  {
    $id = $this->input->post('id');
    $data = array('estatus' => 0);
    $result = $this->ModeloGeneral->updateCatalogo($data, 'personalId', $id, 'personal');
    echo $result;
  }

  public function check_user()
  {
    $currentUsu = $this->input->post("user");
    $currentId = $this->input->post("id");
    $user = $this->ModeloUsuarios->get_existing_user($currentUsu);
    $exists = false;

    if ($user != NULL) {
      foreach ($user as $u) {
        if ($u != $currentId) {
          $exists = true;
        }
      }
    }
    echo $exists;
  }

  function cargar_documentos()
  {
    $filetipo = $_POST['filetipo'];
    $personal = $_POST['id_personal'];

    $input_name = 'file';
    $DIR_SUC = FCPATH . 'public/uploads/personal/documentos';
    $config['upload_path']     = $DIR_SUC;
    $config['allowed_types']   = 'gif|jpg|png|jpeg|bmp|pdf';
    $config['max_size']        = 5000;
    $file_names = 'p'.$personal.'_'.'t'.$filetipo.'_'.date('YmdGis');
    $config['file_name'] = $file_names;
    $output = [];

    $this->load->library('upload', $config);
    if (!$this->upload->do_upload($input_name)) {
      $data = array('error' => $this->upload->display_errors());
      //log_message('error', "DOC: " . json_encode($data));
    } else {
      $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
      $file_name = $upload_data['file_name']; //uploded file name
      $extension = $upload_data['file_ext'];    // uploded file extension

      $this->ModeloGeneral->tabla_inserta('personal_documentos', array('personalId'=> $personal, 'file' => $file_name, 'reg_file' => date("Y-m-d H:i:s"), 'tipo' => $filetipo));

      $data = array('upload_data' => $this->upload->data());
    }
    echo json_encode($output);
  }


  function delete_document($id)
  {
    $result = $this->ModeloGeneral->updateCatalogo_value(array('estatus'=>0),array('documentoId'=> $id),'personal_documentos');
    echo $id;
  }

}
