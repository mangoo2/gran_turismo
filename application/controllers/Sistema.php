<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }
	public function index(){
        $idperf=$this->session->userdata('perfilid_tz');
		$this->load->view('login/index'); 

        /*
        log_message('error', 'View: '.json_encode($idperf));
        log_message('error', 'Log: '.json_encode($this->session->userdata('logeado')));
        log_message('error', 'Info: '.json_encode($this->session->userdata()));
        log_message('error', 'ID_perf: '.$idperf);
        */
        
		if (!isset($idperf)) {
            $perfilview=0;
            redirect('/Login');
        }else{
            $perfilview=$idperf;
        }

       	if ($idperf==1) {
        	redirect('/Inicio');
        }
        /*else{
        	redirect('/Login');
        }*/

        if ($idperf >= 1 && $idperf!=7) {
            redirect('/Inicio');
        }

        if ($idperf==7) {
        	redirect('/Contratos');
        }else{
        	redirect('/Login');
        }

	}
}
