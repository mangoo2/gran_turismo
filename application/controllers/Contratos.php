<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Contratos extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloSession');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloContratos');
    $this->load->model('ModeloUnidades');
    $this->idpersonal=$this->session->userdata('idpersonal');
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
    $this->fecha_reciente = date('Y-m-d');
    if ($this->session->userdata('logeado')){
      $this->idpersonal=$this->session->userdata('idpersonal_tz');
      $this->perfilid=$this->session->userdata('perfilid_tz');
      $permiso=$this->ModeloSession->getviewpermiso($this->perfilid,6);

      if ($permiso==0) {
        redirect('Login');
      }
    }else{
      redirect('/Login');
    }
  }

  public function index()
  {
    $data["estados"]=$this->ModeloGeneral->getselectwhere2('estados',array('activo'=>1));
    $data["cuentas"]=$this->ModeloGeneral->getselectwhere2('cuentas',array('estatus'=>1));
    $data["unidades"]=$this->ModeloGeneral->getselectwhere2('unidades',array('estatus'=>1));
    $data["choferes"]=$this->ModeloGeneral->getselectwhere2('choferes',array('estatus'=>1));
    $data["perfilId"] = $this->session->userdata('perfilid_tz');
    $data["choferId"] = $this->session->userdata('idpersonal_tz');

    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('operaciones/contratos/index',$data);
    $this->load->view('templates/footer');
    $this->load->view('operaciones/contratos/indexjs');
  }

  public function registro($id=0)
  {
    $data["unidades"]=$this->ModeloGeneral->getselectwhere2('unidades',array('estatus'=>1));
    $data["estados"]=$this->ModeloGeneral->getselectwhere2('estados',array('activo'=>1));
    $data["personal"]=$this->ModeloGeneral->getselectwhere2('personal',array('estatus'=>1));
    $data["reg"]=$this->ModeloGeneral->getselectwhere2('f_regimenfiscal',array('activo'=>1));
    $data["cfdi"]=$this->ModeloGeneral->getselectwhere2('f_uso_cfdi',array('activo'=>1));

    if($id==0){
      $data['tittle']='Alta';
    }else{
      $data['tittle']='Edición'; 
      $data["c"]=$this->ModeloGeneral->getselectwhererow2('contratos',array('id'=>$id));
      $data["clientes"] = $this->ModeloGeneral->getselectwhererow2('prospectos',array('estatus'=>1,'id'=>$data["c"]->idCliente));
      $data["des"] = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_contrato'=>$id,"estatus"=>1));
      //$data["ext"] = $this->ModeloGeneral->getselectwhererow2('pagos_cliente',array('id_contrato'=>$id,"estatus"=>1));
    }

    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('operaciones/contratos/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('operaciones/contratos/formjs');
  }

  public function guardar(){
    $datos = $this->input->post();
    $id=$datos['id'];
    unset($datos['id']);
    $id_reg=0;
    if($id>0){
      $datos["liquidado"]=0;
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,'contratos');
      $id_reg = $id; 
    }else{
      $datos['fecha_reg']=$this->fechahoy;
      $id_reg = $this->ModeloGeneral->tabla_inserta('contratos',$datos);
    }
    echo $id_reg;
  }

  public function getlistado(){
    $params = $this->input->post();
    $getdata = $this->ModeloContratos->get_result($params);
    $totaldata= $this->ModeloContratos->total_result($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }

  public function getlistadoChofer(){
    $params = $this->input->post();
    $getdata = $this->ModeloContratos->get_result_chofer($params);
    $totaldata= $this->ModeloContratos->total_result_chofer($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }

  public function exportPagos($id){
    $data["id"]=$id;
    $data["cont"]=$this->ModeloContratos->getDatosContrato($id);
    //$data["pago"]=$this->ModeloGeneral->getselectwhere2('pagos_contrato',array('id_contrato'=>$id,"estatus"=>1));
    $data["pago"]=$this->ModeloContratos->getPagosContrato($id);
    $this->load->view('operaciones/contratos/pagosExcel',$data);
  }

  public function exportContratos($id_cliente,$seguimiento,$tipo_cliente,$prioridad,$fechai,$fechaf,$tipo,$liquidado){  
    $params["id_cliente"] = $id_cliente;
    $params["seguimiento"] = $seguimiento;
    $params["tipo_cliente"] = $tipo_cliente;
    $params["prioridad"] = $prioridad;
    $params["fechai"] = $fechai;
    $params["fechaf"] = $fechaf;
    $params["liquidado"] = $liquidado;
    $data["con"] = $this->ModeloContratos->getContratosLista($params);
    if($tipo==1){
      $this->load->view('operaciones/contratos/listaExcel',$data);
    }
    else{
      $this->load->view('reportes/listaContratos',$data);
    }
  }

  public function estadisticasView($id){
    $data["id"]=$id;
    $data["cont"]=$this->ModeloContratos->getDatosContrato($id);
    $data["pago"]=$this->ModeloContratos->getPagosContrato($id);
    $data["unidades"] = $this->ModeloGeneral->getselectwhere2('unidad_prospecto',array('id_contrato'=>$id,"estatus"=>1));
    //$data["des"] = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_contrato'=>$id,"estatus"=>1));
    //$data["gastos"] = $this->ModeloGeneral->getselectwhere2Order2('contrato_gastos',array('id_contrato'=>$id,"tipo >"=>"0","estatus"=>1),'id_unidad','asc');
    $data["gastos"] = $this->ModeloContratos->getGastosUnidadesContrato($id);

    //$data["kms"] = $this->ModeloGeneral->getselectwhere2('bitacora_revisiones',array('id_contrato'=>$id,"estatus"=>1));
    $data["kms"] = $this->ModeloContratos->getKMSUnidadesContrato($id);
    //log_message('error','KMS: '.json_encode($data["kms"]->result()));
    $data["kms"] = $data["kms"]->result();
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('operaciones/contratos/estadisticas_view',$data);
    $this->load->view('templates/footer');
  }

  public function estadisticas($id){
    $data["id"]=$id;
    $data["cont"]=$this->ModeloContratos->getDatosContrato($id);
    $data["pago"]=$this->ModeloContratos->getPagosContrato($id);
    $data["unidades"] = $this->ModeloGeneral->getselectwhere2('unidad_prospecto',array('id_contrato'=>$id,"estatus"=>1));
    //$data["des"] = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_contrato'=>$id,"estatus"=>1));
    //$data["gastos"] = $this->ModeloGeneral->getselectwhere2('contrato_gastos',array('id_contrato'=>$id,"tipo >"=>"0","estatus"=>1));
    $data["gastos"] = $this->ModeloContratos->getGastosUnidadesContrato($id);
    /*
    $data["kms"] = $this->ModeloGeneral->getselectwhere2('bitacora_revisiones',array('id_contrato'=>$id,"estatus"=>1));
    $data["kms"] = $data["kms"]->row();
    */
    $data["kms"] = $this->ModeloContratos->getKMSUnidadesContrato($id);
    $data["kms"] = $data["kms"]->result();
    
    $this->load->view('operaciones/contratos/estadisticas',$data);
  }

  public function search_cliente(){
    $search = $this->input->get('search');
    $results = $this->ModeloContratos->search_cliente($search);
    echo json_encode($results);    
  }

  public function updateSeguimiento(){
    $id = $this->input->post('id');
    $seguimiento = $this->input->post('seguimiento');
    $motivo_rechazo = $this->input->post('motivo_rechazo');
    $this->ModeloGeneral->updateCatalogo(array("seguimiento"=>$seguimiento,"motivo_rechazo"=>$motivo_rechazo),'id',$id,"contratos");
  }

  public function delete(){
    $id = $this->input->post('id');
    $tabla = $this->input->post('tabla');
    $data = array('estatus' => 0);

    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,$tabla);
    if($tabla=="pagos_contrato"){
      $getic = $this->ModeloGeneral->getselectwhererow2("pagos_contrato",array("id"=>$id));
      $this->ModeloGeneral->updateCatalogo(array("liquidado"=>0),'id',$getic->id_contrato,"contratos");
    }
    echo $resul;
  }


  public function get_table_unidades(){
    $id = $this->input->post('id');
    $idCot = $this->ModeloContratos->getIdCotizacion($id);
    //log_message('error','IDCOT: '.json_encode($idCot));

    if($idCot->cotizacion == 1){
      $unidades = $this->ModeloGeneral->getselectwhere2('unidad_prospecto',array('id_cotizacion'=>$idCot->id_cotizacion, 'id_cotizacion !='=>0, "estatus"=>1));
      $data['unidades'] = $unidades->result();


      foreach ($data['unidades'] as $dato) {
        $destinos = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_cotizacion'=>$idCot->id_cotizacion, "id_unidPros"=>$dato->id, "estatus"=>1));
        $dato->destinos = $destinos->result();
      }
      //log_message('error', 'GET TABLE UNIDADES: ' . json_encode($data['unidades']));

    }else{
      $unidades = $this->ModeloGeneral->getselectwhere2('unidad_prospecto',array('id_contrato'=>$id, 'id_contrato !='=>0, "estatus"=>1));
      $data['unidades'] = $unidades->result();


      foreach ($data['unidades'] as $dato) {
        $destinos = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_contrato'=>$id, "id_unidPros"=>$dato->id, "estatus"=>1));
        $dato->destinos = $destinos->result();
      }

      //log_message('error', 'GET TABLE UNIDADES: ' . json_encode($data['unidades']));
    }

    //log_message('error', 'GET TABLE UNIDADES: ' . json_encode($data['unidades']));
    echo json_encode($data['unidades']);
  }


  public function insertUnidades(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    //log_message('error','DATA Destinos: '.json_encode($DATA));
    
    for ($i=0; $i<count($DATA); $i++) {
      $id_contrato = $DATA[$i]->id_contrato;
      $id_prospecto = $DATA[$i]->id_prospecto;

      $unidad = $DATA[$i]->unidad;
      $cantidad = $DATA[$i]->cantidad;
      $cant_pasajeros = $DATA[$i]->cant_pasajeros;
      $monto = $DATA[$i]->monto;

      if($DATA[$i]->id == 0){//Nuevo
        $arr = array("id_prospecto"=>$id_prospecto, "id_contrato"=>$id_contrato, "unidad"=>$unidad, "cantidad"=>$cantidad, "cant_pasajeros"=>$cant_pasajeros, "monto"=>$monto, "reg"=>$this->fechahoy);
        $result = $this->ModeloGeneral->tabla_inserta("unidad_prospecto",$arr);

        for ($j = 0; $j<count($DATA[$i]->DESTINOS); $j++) {
          $lugar = $DATA[$i]->DESTINOS[$j]->lugar;
          //$fecha = $DATA[$i]->DESTINOS[$j]->fecha;
          //$hora = $DATA[$i]->DESTINOS[$j]->hora;
          //$fecha_regreso = $DATA[$i]->DESTINOS[$j]->fecha_regreso;
          //$hora_regreso = $DATA[$i]->DESTINOS[$j]->hora_regreso;
          //$lugar_hospeda = $DATA[$i]->DESTINOS[$j]->lugar_hospeda;
          $fecha = '';
          $hora = '';
          $fecha_regreso = '';
          $hora_regreso ='';
          $lugar_hospeda = '';

          $arrU = array("id_prospecto"=>$id_prospecto, "id_contrato"=>$id_contrato, "id_unidPros"=>$result, "lugar"=>$lugar, "fecha"=>$fecha, "hora"=>$hora, "fecha_regreso"=>$fecha_regreso, "hora_regreso"=>$hora_regreso, "lugar_hospeda"=>$lugar_hospeda, "reg"=>$this->fechahoy);
          $idDestino = $this->ModeloGeneral->tabla_inserta("destino_prospecto",$arrU);
        }

      }else{//Edicion
        $arr = array("id_contrato"=>$id_contrato, "unidad"=>$unidad, "cantidad"=>$cantidad, "cant_pasajeros"=>$cant_pasajeros, "monto"=>$monto);
        $result = $this->ModeloGeneral->updateCatalogo_value($arr, array('id'=>$DATA[$i]->id), "unidad_prospecto");


        for ($j = 0; $j<count($DATA[$i]->DESTINOS); $j++) {
          $id_destino = $DATA[$i]->DESTINOS[$j]->id;
          $lugar = $DATA[$i]->DESTINOS[$j]->lugar;
          //$fecha = $DATA[$i]->DESTINOS[$j]->fecha;
          //$hora = $DATA[$i]->DESTINOS[$j]->hora;
          //$fecha_regreso = $DATA[$i]->DESTINOS[$j]->fecha_regreso;
          //$hora_regreso = $DATA[$i]->DESTINOS[$j]->hora_regreso;
          //$lugar_hospeda = $DATA[$i]->DESTINOS[$j]->lugar_hospeda;
          $fecha = '';
          $hora = '';
          $fecha_regreso = '';
          $hora_regreso ='';
          $lugar_hospeda = '';

          if($id_destino == 0){
            $arrU = array("id_prospecto"=>$id_prospecto, "id_contrato"=>$id_contrato, "id_unidPros"=>$DATA[$i]->id, "lugar"=>$lugar, "fecha"=>$fecha, "hora"=>$hora, "fecha_regreso"=>$fecha_regreso, "hora_regreso"=>$hora_regreso, "lugar_hospeda"=>$lugar_hospeda, "reg"=>$this->fechahoy);
            $idDestino = $this->ModeloGeneral->tabla_inserta("destino_prospecto",$arrU);
          }else{
            $arrU = array("id_contrato"=>$id_contrato, "lugar"=>$lugar, "fecha"=>$fecha, "hora"=>$hora, "fecha_regreso"=>$fecha_regreso, "hora_regreso"=>$hora_regreso, "lugar_hospeda"=>$lugar_hospeda);
            $idDestino = $this->ModeloGeneral->updateCatalogo_value($arrU, array('id'=>$id_destino), "destino_prospecto");
          }
        }
      }

      //Era cotizacion?
      $idCot = $this->ModeloContratos->getIdCotizacion($DATA[0]->id_contrato);
      if($idCot->cotizacion == 1){
        $this->ModeloGeneral->updateCatalogo_value(array('cotizacion'=>2), array('id'=>$DATA[0]->id_contrato), "contratos");
      }

    }

    echo $result;
  }


  public function confirmar_contrato(){
    $datos = $this->input->post();
    $id_contrato = $datos['id_contrato'];
    $id_detalle = $datos['id_detalle'];
    unset($datos['id_detalle']);
    unset($datos['fecha_contrato']);
    //log_message('error','Confirmar Contrato: '.json_encode($datos));

    if($id_detalle > 0){
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id_detalle,'prospectos_clientes_d');
      $id_reg = $id_detalle;
    }else{
      $id_reg = $this->ModeloGeneral->tabla_inserta('prospectos_clientes_d',$datos);
      $this->ModeloGeneral->updateCatalogo(array('contrato'=>1),'id',$id_contrato,'contratos');
    }

    echo $id_reg; 
  }


  public function updateFechaContrato(){
    $data = $this->input->post();
    //log_message('error','Up Contrato Fecha: '.json_encode($data));

    $id = $data['id'];
    $date = $data['date'];

    $id_reg = 0;

    if($id > 0){
      $this->ModeloGeneral->updateCatalogo(array('fecha_contrato' => $date),'id',$id,'contratos');
      $id_reg = $id; 
    }

    echo $id_reg;
  }


  public function get_data_contrato(){
    $id = $this->input->post('id');
    $data = $this->ModeloGeneral->getselectwhererow2('prospectos_clientes_d',array('id_contrato' => $id));
    $datos = $this->ModeloGeneral->getselectwhererow2('contratos',array('id' => $id));
    $data->fecha_contrato = $datos->fecha_contrato;
    
    echo json_encode($data); 
  }


  public function get_fecha_contrato(){
    $id = $this->input->post('id');
    $datos = $this->ModeloGeneral->getselectwhererow2('contratos',array('id' => $id));
    //log_message('error','Datos_fecha: '.json_encode($datos));
    echo json_encode($datos);
  }

//Unidades en Modal--------------->
  public function insertUnidad(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    
    for ($i=0; $i<count($DATA); $i++) {
      $id_contrato = $DATA[$i]->id_contrato;
      $id_prospecto = $DATA[$i]->id_prospecto;

      $cantidad = $DATA[$i]->cantidad;
      $cant_pasajeros = $DATA[$i]->cant_pasajeros;
      $monto = $DATA[$i]->monto;

      if($DATA[$i]->id > 0){
        $arr = array("id_prospecto"=>$id_prospecto,"id_contrato"=>$id_contrato,"cantidad"=>$cantidad,"cant_pasajeros"=>$cant_pasajeros,"monto"=>$monto);
        $result = $this->ModeloGeneral->updateCatalogo_value($arr, array('id'=>$DATA[$i]->id), "unidad_prospecto");

        $destinos = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_unidPros' => $DATA[$i]->id, 'estatus' => 1));
        //log_message('error','RESULT '.json_encode($destinos->result()));

        foreach ($destinos->result() as $dest) {
          //log_message('error','RESULT DEST'.json_encode($dest));
          
          if($dest->id > 0){
            $this->ModeloGeneral->updateCatalogo_value(array("id_contrato"=>$id_contrato), array('id'=>$dest->id), "destino_prospecto");
          }
        }
      }
    }
    echo $datos;
  }


  public function get_unidades_contrato(){
    $id = $this->input->post('id');
    $data = $this->ModeloContratos->getUnidadesContrato($id);

    echo json_encode($data->result()); 
  }


  public function contrato($id = 0){
    $data['contratos'] = $this->ModeloGeneral->getselectwhererow2('contratos',array('id'=>$id,'estatus'=>1));
    $data['clientes'] = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id'=>$data['contratos']->idCliente));
    $data['detalles'] = $this->ModeloGeneral->getselectwhererow2('prospectos_clientes_d',array('id_contrato'=>$id));
    $data['destinos'] = $this->ModeloContratos->getItinerarioContrato($id);
    $data['pagos'] = $this->ModeloGeneral->getselectwhererow2('pagos_cliente',array('id_cliente'=>$data['contratos']->idCliente,'estatus'=>1));

    $data['estado_detalles'] = $this->ModeloGeneral->getselectwhererow2('estados',array('EstadoId'=>$data['detalles']->estado));
    $data['estado_detalles'] = $data['estado_detalles']->Nombre;
    
    $data['estado_clientes'] = $this->ModeloGeneral->getselectwhererow2('estados',array('EstadoId'=>$data['clientes']->estado));
    $data['estado_clientes'] = $data['estado_clientes']->Nombre;

    $data['unidades'] = $this->ModeloContratos->getUnidadesContrato($id);
    //log_message('error','Unidad:'.json_encode($data['unidades']->result()));

    $data['personal'] = $this->ModeloGeneral->getselectwhererow2('personal',array('personalId'=>$data['contratos']->personal,'estatus'=>1));

    $data['idC'] = $id;
    //$data['folio'] = $id;

    $length = 3;
    $data['folio_cot'] = '--';
    $idCot = $this->ModeloContratos->getIdCotizacion($id);
    if($idCot->cotizacion != 0){
      $format = substr(str_repeat(0, $length) . ($idCot->id_cotizacion), -$length);
      $data['folio_cot'] = 'COT-' . $format;
    }

    //log_message('error','Cotizacion-> '.json_encode($data['folio']));

    $pagos = $this->ModeloGeneral->getselectwhere2('pagos_contrato',array('id_contrato'=>$id,"estatus"=>1));
    $pago = $pagos->result();
    $data['pago'] = 0;

    foreach ($pago as $pag) {
      $data['pago'] = $data['pago'] + $pag->monto;
    }

    //log_message('error','PAGOS-> '.json_encode($data['pagos']));
    //log_message('error','Destinos ' .json_encode($data['destinos']));

    $this->load->view('reportes/documento_contrato', $data);
  }


  
  public function contratoMail(){
    $id = $this->input->post('id');

    $data['contratos'] = $this->ModeloGeneral->getselectwhererow2('contratos',array('id'=>$id,'estatus'=>1));
    $data['clientes'] = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id'=>$data['contratos']->idCliente));
    $data['detalles'] = $this->ModeloGeneral->getselectwhererow2('prospectos_clientes_d',array('id_contrato'=>$id));
    $data['destinos'] = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_contrato'=>$id,'estatus'=>1));
    $data['pagos'] = $this->ModeloGeneral->getselectwhererow2('pagos_cliente',array('id_cliente'=>$data['contratos']->idCliente,'estatus'=>1));

    $data['estado_detalles'] = $this->ModeloGeneral->getselectwhererow2('estados',array('EstadoId'=>$data['detalles']->estado));
    $data['estado_detalles'] = $data['estado_detalles']->Nombre;
    
    $data['estado_clientes'] = $this->ModeloGeneral->getselectwhererow2('estados',array('EstadoId'=>$data['clientes']->estado));
    $data['estado_clientes'] = $data['estado_clientes']->Nombre;

    $data['unidades'] = $this->ModeloContratos->getUnidadesContrato($id);
    //log_message('error','Unidad:'.json_encode($data['unidades']->result()));

    $data['personal'] = $this->ModeloGeneral->getselectwhererow2('personal',array('personalId'=>$data['contratos']->personal,'estatus'=>1));

    $data['idC'] = $id;
    //$data['folio'] = $id;

    $length = 3;
    $data['folio_cot'] = '--';
    $idCot = $this->ModeloContratos->getIdCotizacion($id);
    if($idCot->cotizacion != 0){
      $format = substr(str_repeat(0, $length) . ($idCot->id_cotizacion), -$length);
      $data['folio_cot'] = 'COT-' . $format;
    }

    //log_message('error','Cotizacion-> '.json_encode($data['folio']));

    $pagos = $this->ModeloGeneral->getselectwhere2('pagos_contrato',array('id_contrato'=>$id,"estatus"=>1));
    $pago = $pagos->result();
    $data['pago'] = 0;

    foreach ($pago as $pag) {
      $data['pago'] = $data['pago'] + $pag->monto;
    }

    //log_message('error','PAGOS-> '.json_encode($data['pagos']));

    $this->load->view('reportes/documento_contrato', $data);
  }


  function sendMailPDF(){
    $idCliente = $this->input->post('idCliente');
    $idContrato = $this->input->post('idContrato');
    $get_info = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id' => $idCliente));
    //log_message('error', 'GET MAIL: '.json_encode($get_info));
    $nombre = $get_info->nombre." ".$get_info->app." ".$get_info->apm;
    /*
    $nombre = '';
    
    if(isset($get_info->nombre)){
      $nombre .= $get_info->nombre; 
    } 
    if(isset($get_info->app)){
      $nombre .= " ".$get_info->app; 
    } 
    if(isset($get_info->apm)){
      $nombre .= " ".$get_info->apm; 
    } 
    */

    //$empresa = $get_info->empresa; 
    $email = $get_info->correo;

    //== libreria email ==
    if($email!=""){
      $this->load->library('email');
      $config = array();
      $config['protocol'] = 'smtp';
      $config["smtp_host"] = 'grandturismo.mangoo.systems'; //'mail.mangoo.systems'
      $config["smtp_user"] = 'contacto@grandturismo.mangoo.systems'; //'contacto@mangoo.systems'
      $config["smtp_pass"] = 'xxb{-5dz767h'; //'x!fZTBrOoBGI'

      $config["smtp_port"] = 465;
      $config["smtp_crypto"] = 'ssl';
      $config['mailtype'] = "html";          
      $config['charset'] = 'utf-8'; 
      $config['wordwrap'] = TRUE;
      $config['validate'] = true;
      $config['mailtype'] = 'html';
      $this->email->initialize($config);

      $this->email->from('contacto@grandturismo.mangoo.systems'); //'contacto@mangoo.systems'
      $this->email->to($email);
      $this->email->subject('Contrato '.$nombre);
      //--------------------------------------------------------
      $pdf = FCPATH.'public/pdf/Contrato_'.$idContrato.'.pdf';
      //log_message('error', 'GET MAIL: '.json_encode($pdf));
      $this->email->attach($pdf);
      //--------------------------------------------------------
      $this->email->message('Se adjunta Contrato.');

      /*
      $this->email->send();
      
      if($this->email->send()) {
        echo 'Email sent.';
      } else {
        show_error($this->email->print_debugger());
      }
      */
      
      if ($this->email->send()) {
        echo 'Email sent.';
      } else {
        echo 'Error ' . $this->email->print_debugger();
      }

    } 
  }

  //----------------------------
  public function get_data_pagos(){
    $datos = $this->input->post();
    $id = $datos['id'];
    $idCliente = $datos['idCliente'];
    $dataContrato = $this->ModeloGeneral->getselectwhererow2('contratos',array('id' => $id));

    $data['referencia'] = $id;
    $data['fecha'] = $dataContrato->fecha_contrato;
    $data['descuento'] = $dataContrato->porc_desc;
    $data['anticipo'] = $dataContrato->monto_anticipo;

    $data['cliente'] = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id'=>$idCliente));
    $data['cliente'] = $data['cliente']->nombre.' '.$data['cliente']->app.' '.$data['cliente']->apm;

    $montos = $this->ModeloGeneral->getselectwhere2('unidad_prospecto',array('id_contrato'=>$id,"estatus"=>1));
    $montos = $montos->result();
    $data['monto'] = 0;
    foreach ($montos as $monto) {
      $data['monto'] = $data['monto'] + ($monto->cantidad * $monto->monto);
    }

    /*-----------------------------*/
    /*$pagos = $this->ModeloGeneral->getselectwhere2('pagos_contrato',array('id_contrato'=>$id,"estatus"=>1));
    $data['pagos'] = $pagos->result();*/
    $data["pagos"]=$this->ModeloContratos->getPagosContrato($id);

    $data['pago'] = 0;
    foreach ($data["pagos"] as $pag) {
      $data['pago'] = $data['pago'] + $pag->monto;
    }

    $data['restante'] = $data['monto'] - ($data['pago'] + $data['descuento'] + $data['anticipo']) ;
    echo json_encode($data); 
  }

  public function get_data_1_pago(){
    $datos = $this->input->post();
    $id = $datos['id'];
    //$idContrato = $datos['idContrato'];
    $data = $this->ModeloGeneral->getselectwhererow2('pagos_contrato',array('id'=>$id , 'estatus'=>1));
    
    echo json_encode($data); 
  }
  

  public function insertPago(){
    $datos = $this->input->post();
    $id_contrato = $this->input->post("id_contrato");
    //log_message('error', 'id_contrato: ' . $id_contrato);
    $datos['reg'] = $this->fechahoy;
    $datos['id_usuario'] = $this->idpersonal;
    $monto_total = $datos["monto_total"];
    unset($datos["monto_total"]);
    $id_reg = 0;
    $id_reg = $this->ModeloGeneral->tabla_inserta('pagos_contrato',$datos); 
    $datos['id'] = $id_reg;

    $pago_contrato=0;
    //$pagos = $this->ModeloGeneral->getselectwhere2('pagos_contrato',array('id_contrato'=>$id_contrato,"estatus"=>1));
    $pagos=$this->ModeloContratos->getPagosContrato($id_contrato);
    foreach($pagos as $p){
      $pago_contrato=$pago_contrato+$p->monto;
      $datos["cuenta"]=$p->cuenta;
    }
    if($monto_total==$pago_contrato){
      $this->ModeloGeneral->updateCatalogo_value(array("liquidado"=>1), array('id'=>$id_contrato), "contratos");
    }
    echo json_encode($datos);
  }


  public function get_data_destinos(){
    $id = $this->input->post('id');

    $unidades = $this->ModeloGeneral->getselectwhere2('unidad_prospecto',array('id_prospecto' => $id, 'id_contrato' => 0, 'id_cotizacion' => 0,  'estatus' => 1));
    $data['unidades'] = $unidades->result();

    foreach ($data['unidades'] as $dato) {
      $destinos = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_prospecto' => $id, 'id_contrato' => 0, 'id_cotizacion' => 0, "id_unidPros"=>$dato->id, "estatus"=>1));
      $dato->destinos = $destinos->result();
    }

    //log_message('error', 'GET TABLE destinos: ' . json_encode($data['unidades']));
    echo json_encode($data['unidades']);
  }


  public function get_unidades_cotizacion(){
    $id = $this->input->post('id');
    $data = $this->ModeloContratos->getUnidadesCotizacion($id);
    //log_message('error','DATA Unidades '.json_encode($data->result()));

    echo json_encode($data->result()); 
  }


  public function get_unidades_cliente(){
    $id = $this->input->post('id');

    $data = $this->ModeloContratos->getUnidadesProspecto($id);
    ////log_message('error','DATA Unidades '.json_encode($data->result()));

    echo json_encode($data->result()); 
  }


  //----------------------------
  public function get_data_choferes(){
    $datos = $this->input->post();
    $id = $datos['id'];
    $idCliente = $datos['idCliente'];
    $unidades = $this->ModeloContratos->getUnidadesContrato($id);
    $data['unidades'] = $unidades->result();

    foreach ($data['unidades'] as $dato) {
      $choferes = $this->ModeloContratos->getChoferesContrato($dato->id);
      $dato->choferes = $choferes->result();
  }
    //log_message('error','UNIDADES:' . json_encode($data['unidades']));

    $data['referencia'] = $id;

    $data['cliente'] = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id'=>$idCliente));
    $data['cliente'] = $data['cliente']->nombre.' '.$data['cliente']->app.' '.$data['cliente']->apm;


    echo json_encode($data); 
  }


  public function insertChofer(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    $result = 0;
    
    for ($i=0; $i<count($DATA); $i++) {

      $idChofCont = $DATA[$i]->idChofCont;

      $idUnidPros = $DATA[$i]->idUnidPros;
      $idChofer = $DATA[$i]->idChofer;
      $idContrato = $DATA[$i]->idContrato;
      $idUnidad = $DATA[$i]->idUnidad;
      
      $array = array("idUnidPros"=>$idUnidPros, "idChofer"=>$idChofer, "idContrato"=>$idContrato, "idUnidad"=>$idUnidad);

      if($idChofCont == 0){
        $result = $this->ModeloGeneral->tabla_inserta("chofer_contrato",$array);
      }/*else{
        $result = $this->ModeloGeneral->updateCatalogo_value($array, array('id'=>$idChofCont), "chofer_contrato");
      }*/
    }

    echo $result;
  }

  public function get_data_pasajeros(){
    $datos = $this->input->post();
    $id = $datos['id'];
    $idCliente = $datos['idCliente'];
    $unidades = $this->ModeloContratos->getUnidadesContrato($id);
    $data['unidades'] = $unidades->result();

    
    foreach ($data['unidades'] as $dato) {
      $pasajeros = $this->ModeloContratos->getPasajerosContrato($dato->id);
      $dato->pasajeros = $pasajeros->result();
    }
    //log_message('error','UNIDADES:' . json_encode($data['unidades']));

    $data['referencia'] = $id;
    $data['cliente'] = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id'=>$idCliente));
    $data['cliente'] = $data['cliente']->nombre.' '.$data['cliente']->app.' '.$data['cliente']->apm;
    //log_message('error','UNIDADES:' . json_encode($data['unidades']));
    
    echo json_encode($data); 
  }


  public function insertPasajero(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    $result = 0;
    
    for ($i=0; $i<count($DATA); $i++) {

      $idPasajeroContrato = $DATA[$i]->idPasajeroContrato;

      $idUnidPros = $DATA[$i]->idUnidPros;
      $pasajero = $DATA[$i]->pasajero;
      $idContrato = $DATA[$i]->idContrato;
      $idUnidad = $DATA[$i]->idUnidad;
      
      $array = array("idUnidPros"=>$idUnidPros, "pasajero"=>$pasajero, "idContrato"=>$idContrato, "idUnidad"=>$idUnidad);

      if($idPasajeroContrato == 0){
        $result = $this->ModeloGeneral->tabla_inserta("pasajeros_contrato",$array);
      }else{
        $result = $this->ModeloGeneral->updateCatalogo_value($array, array('id'=>$idPasajeroContrato), "pasajeros_contrato");
      }
    }

    echo $result;
  }



  public function reportePasajeros($id = 0){
    $data['idC'] = $id;
    //$data['folio'] = $id;
    $data['contratos'] = $this->ModeloGeneral->getselectwhererow2('contratos',array('id'=>$id));
    $data['prospecto'] = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id'=>$data['contratos']->idCliente));
    $unidades = $this->ModeloContratos->getUnidadesContrato($id);
    $data['unidades'] = $unidades->result();

    foreach ($data['unidades'] as $unidad) {
      //log_message('error','UNIDAD: '.json_encode($unidad));
      $pasajeros = $this->ModeloContratos->getPasajerosContrato($unidad->id);
      $unidad->pasajeros = $pasajeros->result();
    }

    $this->load->view('reportes/documento_reporte_pasajeros', $data);
  }


  public function get_personal(){
    $personal = $this->ModeloGeneral->getselectwhererow2('personal',array('personalId'=>$this->session->userdata('idpersonal_tz'),'estatus'=>1));

    $data = $personal->nombre." ".$personal->apellido_p." ".$personal->apellido_m;

    echo $data;
  }


    public function orden_servicio($id = 0, $unidad = 0, $porCobrar = 1){
      $data['id_cont'] = $id;
      //$data['folio_cont'] = $id;
      $data['id_unidad'] = $unidad;
      $data['por_cobrar'] = $porCobrar;
  
      $length = 3;
      $data['folio_coti'] = '--';
      $idCot = $this->ModeloContratos->getIdCotizacion($id);
      if($idCot->cotizacion != 0){
        $format = substr(str_repeat(0, $length) . ($idCot->id_cotizacion), -$length);
        $data['folio_coti'] = 'COT-' . $format;
      }
      
      $data['usuario'] = $this->ModeloGeneral->getselectwhererow2('personal',array('personalId'=>$this->session->userdata('idpersonal_tz'),'estatus'=>1));
      $data['fecha_hora_imp'] = $this->fechahoy;
  
      $data['contratos'] = $this->ModeloGeneral->getselectwhererow2('contratos',array('id'=>$id,'estatus'=>1));
      $data['clientes'] = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id'=>$data['contratos']->idCliente));

      $data['destinos'] = $this->ModeloContratos->getItinerarioContratoUnidad($id,$unidad);
      $data['detalles'] = $this->ModeloGeneral->getselectwhererow2('prospectos_clientes_d',array('id_contrato'=>$id));
  
      $data['estado_contrato'] = $this->ModeloGeneral->getselectwhererow2('estados',array('EstadoId'=>$data['contratos']->estado));
      $data['estado_contrato'] = $data['estado_contrato']->Nombre;

      $data['unidadAsig'] = $this->ModeloContratos->getUnidadContrato($id,$unidad);
      $data['choferAsig'] = $this->ModeloContratos->getChoferesUnidad($id,$unidad);
      
      $data['montoTotal'] = $this->ModeloContratos->getMontoTotalContrato($id);
      $data['totalPagos'] = $this->ModeloContratos->getPagoTotalContrato($id);

  
      $this->load->view('reportes/documento_orden_servicio', $data);
    }

  public function get_exepedienteCli(){
    $idCliente = $this->input->post("idCliente");
    $data = $this->ModeloGeneral->getselectwhere2("pagos_cliente",array("id_cliente"=>$idCliente));
    $data = $data->row();
    //$arraydata = array('id'=>$id,"monto"=>$monto,'ok'=>'ok','id_clientec_transac'=>$idcct);
    echo json_encode($data); 
  }

  public function form_relacion_gastos($id = 0, $unidad = 0)
  {
    $data["c"]=$this->ModeloGeneral->getselectwhererow2('contratos',array('id'=>$id));
    $data["clientes"] = $this->ModeloGeneral->getselectwhererow2('prospectos',array('tipo'=>1,'estatus'=>1,'id'=>$data["c"]->idCliente));
    $data["u"]=$this->ModeloGeneral->getselectwhererow2('unidad_prospecto',array('id_contrato'=>$id,'unidad'=>$unidad,'estatus'=>1));
    $data["obs"]=$this->ModeloGeneral->getselectwhererow2('contrato_gastos',array('id_contrato'=>$id,'id_unidad'=>$unidad, 'tipo'=>0, 'estatus'=>1));

    //log_message('error', 'UNID: ' . json_encode($data["u"]));
    
    //log_message('error', 'GET OBS: ' . json_encode($data["obs"]));
    //log_message('error', 'old: ' . json_encode($old));

    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('operaciones/contratos/form_relacion_gastos', $data);
    $this->load->view('templates/footer');
    $this->load->view('operaciones/contratos/formjs_relacion_gastos');
  }

/*
  public function relacion_gastos($id = 0){ 
    $data['folio_cont'] = $id;

    $length = 3;
    $data['folio_coti'] = '--';
    $idCot = $this->ModeloContratos->getIdCotizacion($id);
    if($idCot->cotizacion != 0){
      $format = substr(str_repeat(0, $length) . ($idCot->id_cotizacion), -$length);
      $data['folio_coti'] = 'COT-' . $format;
    }
    
    $data['usuario'] = $this->ModeloGeneral->getselectwhererow2('personal',array('personalId'=>$this->session->userdata('idpersonal_tz'),'estatus'=>1));
    $data['fecha_hora_imp'] = $this->fechahoy;

    $data['contratos'] = $this->ModeloGeneral->getselectwhererow2('contratos',array('id'=>$id,'estatus'=>1));
    $data['clientes'] = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id'=>$data['contratos']->idCliente));
    $data['destinos'] = $this->ModeloContratos->getItinerarioContrato($id);
    $data['detalles'] = $this->ModeloGeneral->getselectwhererow2('prospectos_clientes_d',array('id_contrato'=>$id));

    $data['estado_contrato'] = $this->ModeloGeneral->getselectwhererow2('estados',array('EstadoId'=>$data['contratos']->estado));
    $data['estado_contrato'] = $data['estado_contrato']->Nombre;

    $data['unidades'] = $this->ModeloContratos->getUnidadesContrato($id);
    $data['choferes'] = $this->ModeloContratos->getChoferesUnidades($id);

    $data['casetas'] = $this->ModeloGeneral->getselectwhere2('contrato_gastos',array('id_contrato'=>$id, 'tipo'=>1, 'estatus'=>1));
    $data['combustibles'] = $this->ModeloGeneral->getselectwhere2('contrato_gastos',array('id_contrato'=>$id, 'tipo'=>2, 'estatus'=>1));
    $data['otros'] = $this->ModeloGeneral->getselectwhere2('contrato_gastos',array('id_contrato'=>$id, 'tipo'=>3, 'estatus'=>1));
    $data['sueldos'] = $this->ModeloGeneral->getselectwhere2('contrato_gastos',array('id_contrato'=>$id, 'tipo'=>4, 'estatus'=>1));
    $data['obs'] = $this->ModeloGeneral->getselectwhererow2('contrato_gastos',array('id_contrato'=>$id, 'tipo'=>0, 'estatus'=>1));
    
    $data['personal'] = $this->ModeloGeneral->getselectwhererow2('personal',array('personalId'=>$data['contratos']->personal,'estatus'=>1));


    $pagos = $this->ModeloGeneral->getselectwhere2('pagos_contrato',array('id_contrato'=>$id,"estatus"=>1));
    $pago = $pagos->result();
    $data['pago'] = 0;

    foreach ($pago as $pag) {
      $data['pago'] = $data['pago'] + $pag->monto;
    }

    $data['pagoFecha'] = $this->ModeloGeneral->getselectwhererow2('pagos_contrato',array('id_contrato'=>$id,"estatus"=>1));

    //log_message('error', 'Unidades: '. json_encode($data['unidades']->result()));
    log_message('error', 'Choferes: '. json_encode($data['choferes']));

    $this->load->view('reportes/documento_relacion_gastos', $data);
  }
    */

    public function relacion_gastos($id = 0, $unidad = 0){ 
      $data['id_cont'] = $id;
      //$data['folio_cont'] = $id;
      $data['id_unidad'] = $unidad;
  
      $length = 3;
      $data['folio_coti'] = '--';
      $idCot = $this->ModeloContratos->getIdCotizacion($id);
      if($idCot->cotizacion != 0){
        $format = substr(str_repeat(0, $length) . ($idCot->id_cotizacion), -$length);
        $data['folio_coti'] = 'COT-' . $format;
      }
      
      $data['usuario'] = $this->ModeloGeneral->getselectwhererow2('personal',array('personalId'=>$this->session->userdata('idpersonal_tz'),'estatus'=>1));
      $data['fecha_hora_imp'] = $this->fechahoy;
  
      $data['contratos'] = $this->ModeloGeneral->getselectwhererow2('contratos',array('id'=>$id,'estatus'=>1));
      $data['clientes'] = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id'=>$data['contratos']->idCliente));

      $data['unidadAsig'] = $this->ModeloContratos->getUnidadContrato($id,$unidad);
      $data['choferAsig'] = $this->ModeloContratos->getChoferesUnidad($id,$unidad);

      $data['casetas'] = $this->ModeloGeneral->getselectwhere2('contrato_gastos',array('id_contrato'=>$id, 'id_unidad'=>$unidad, 'tipo'=>1, 'estatus'=>1));
      $data['combustibles'] = $this->ModeloGeneral->getselectwhere2('contrato_gastos',array('id_contrato'=>$id, 'id_unidad'=>$unidad, 'tipo'=>2, 'estatus'=>1));
      $data['otros'] = $this->ModeloGeneral->getselectwhere2('contrato_gastos',array('id_contrato'=>$id, 'id_unidad'=>$unidad, 'tipo'=>3, 'estatus'=>1));
      $data['sueldos'] = $this->ModeloGeneral->getselectwhere2('contrato_gastos',array('id_contrato'=>$id, 'id_unidad'=>$unidad, 'tipo'=>4, 'estatus'=>1));
      $data['obs'] = $this->ModeloGeneral->getselectwhererow2('contrato_gastos',array('id_contrato'=>$id, 'id_unidad'=>$unidad, 'tipo'=>0, 'estatus'=>1));
      
      $data['personal'] = $this->ModeloGeneral->getselectwhererow2('personal',array('personalId'=>$data['contratos']->personal,'estatus'=>1));
  
      $data['montoTotal'] = $this->ModeloContratos->getMontoTotalContrato($id);
      $data['totalPagos'] = $this->ModeloContratos->getPagoTotalContrato($id);

  
      $this->load->view('reportes/documento_relacion_gastos', $data);
    }


  public function form_bitacora_viaje($id=0, $unidad=0)
  {
    $data["c"] = $this->ModeloGeneral->getselectwhererow2('contratos',array('id'=>$id));
    $data["u"] = $this->ModeloGeneral->getselectwhererow2('unidad_prospecto',array('id_contrato'=>$id,'unidad'=>$unidad,'estatus'=>1));
    $data["clientes"] = $this->ModeloGeneral->getselectwhererow2('prospectos',array('tipo'=>1,'estatus'=>1,'id'=>$data["c"]->idCliente));
    $data["bit_rev"] = $this->ModeloGeneral->getselectwhererow2('bitacora_revisiones',array('id_contrato'=>$id, 'id_unidad'=>$unidad));

    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('operaciones/contratos/form_bitacora_viaje', $data);
    $this->load->view('templates/footer');
    $this->load->view('operaciones/contratos/formjs_bitacora_viaje');
  }

  public function bitacora_viaje($id = 0, $unidad = 0){

    log_message('error','Unidad: '.json_encode($unidad));

    $data['id_cont'] = $id;
    //$data['folio_cont'] = $id;
    $data['id_unidad'] = $unidad;

    $length = 3;
    $data['folio_coti'] = '--';
    $idCot = $this->ModeloContratos->getIdCotizacion($id);
    if($idCot->cotizacion != 0){
      $format = substr(str_repeat(0, $length) . ($idCot->id_cotizacion), -$length);
      $data['folio_coti'] = 'COT-' . $format;
    }
    
    $data['usuario'] = $this->ModeloGeneral->getselectwhererow2('personal',array('personalId'=>$this->session->userdata('idpersonal_tz'),'estatus'=>1));
    $data['fecha_hora_imp'] = $this->fechahoy;

    $data['contratos'] = $this->ModeloGeneral->getselectwhererow2('contratos',array('id'=>$id,'estatus'=>1));
    //$data['clientes'] = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id'=>$data['contratos']->idCliente));
    //$data['destinos'] = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_contrato'=>$id,'estatus'=>1));

    $data['unidadAsig'] = $this->ModeloContratos->getUnidadContrato($id,$unidad);
    $data['choferAsig'] = $this->ModeloContratos->getChoferesUnidad($id,$unidad);

    $data['personal'] = $this->ModeloGeneral->getselectwhererow2('personal',array('personalId'=>$data['contratos']->personal,'estatus'=>1));


    $data['bitacora_rev'] = $this->ModeloGeneral->getselectwhererow2('bitacora_revisiones',array('id_contrato'=>$id,'id_unidad'=>$unidad,'estatus'=>1));
    //log_message('error','bitacora_rev: '.json_encode($data['bitacora_rev']->fecha_pdf));
    if($data['bitacora_rev']->fecha_pdf == "0000-00-00"){
      $this->ModeloGeneral->updateCatalogo(array('fecha_pdf'=>$this->fechahoy),'id_contrato',$id,'bitacora_revisiones');
      $data['fecha_pdf'] = $this->fechahoy;

    }else{
      $data['fecha_pdf'] = $data['bitacora_rev']->fecha_pdf;
    }

    //Dias-------------------
    $dias = $this->ModeloGeneral->getselectwhere2('bitacora_dias',array('id_contrato' => $id, 'id_unidad' => $unidad, 'estatus' => 1));
    $data['dias'] = $dias->result();

    foreach ($data['dias'] as $dia) {
      $horas = $this->ModeloGeneral->getselectwhere2('bitacora_horas',array('id_contrato' => $id, 'id_unidad' => $unidad, 'id_bitDia'=>$dia->id, 'estatus'=>1));
      $dia->horas = $horas->result();
    }
    //log_message('error','DIAS: '.json_encode($data['dias']));
    //Dias-------------------

/*

    $unidades = $this->ModeloGeneral->getselectwhere2('unidad_prospecto',array('id_contrato'=>$id,'estatus'=>1));
    
    //log_message('error','Contratos: '.json_encode($data['contratos']));
    //log_message('error','UNIDADES: '.json_encode($unidades->result()));

    $data['destinos'] = array();
    foreach($unidades->result() as $unidad){
      $dest = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_contrato'=>$id, 'id_unidPros'=>$unidad->id, 'estatus'=>1));
      array_push($data['destinos'], $dest->result() );
    }

    //log_message('error','DEST: '.json_encode( $dest->result() ));
    //log_message('error','DESTINO: '.json_encode($data['destinos']));
    
*/

    $this->load->view('reportes/documento_bitacora_viaje', $data);
  }


  public function insertObservaciones(){
    $observaciones = $this->input->post('observaciones');
    $id = $this->input->post('id');
    $id_contrato = $this->input->post('idContrato');
    $id_unidad = $this->input->post('idUnidad');
    $cant_recibida = $this->input->post('cant_recibida');

    if($id == 0){//Nuevo
      $arr = array("observaciones"=>$observaciones, "tipo"=>'0', "observaciones"=>$observaciones, "id_contrato"=>$id_contrato, "id_unidad"=>$id_unidad, "cant_recibida"=>$cant_recibida, "reg"=>$this->fechahoy);
      $result = $this->ModeloGeneral->tabla_inserta("contrato_gastos",$arr);

    }else{//Edicion
      $arr = array("observaciones"=>$observaciones, "cant_recibida"=>$cant_recibida);
      $result = $this->ModeloGeneral->updateCatalogo_value($arr, array('id'=>$id), "contrato_gastos");
    }

    $this->ModeloGeneral->updateCatalogo_value(array('rel_gastos'=>1), array('id'=>$id_contrato), "contratos");
    $this->ModeloGeneral->updateCatalogo_value(array('rel_gastos'=>1), array('id_contrato'=>$id_contrato,'unidad'=>$id_unidad), "unidad_prospecto");

    echo $result;
  }


  public function insertGastos(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    log_message('error','DATA S: '.json_encode($DATA));
    
    for ($i=0; $i<count($DATA); $i++) {
      $id_contrato = $DATA[$i]->id_contrato;
      $id_unidad = $DATA[$i]->id_unidad;
      //$id_prospecto = $DATA[$i]->id_prospecto;
      $tipo = $DATA[$i]->tipo;
      $fecha = $DATA[$i]->fecha;
      $descripcion = $DATA[$i]->descripcion;
      $importe = $DATA[$i]->importe;

      if($DATA[$i]->id == 0){//Nuevo
        $arr = array("id_contrato"=>$id_contrato,"id_unidad"=>$id_unidad,"tipo"=>$tipo,"fecha"=>$fecha,"descripcion"=>$descripcion,"observaciones"=>' ', "importe"=>$importe, "reg"=>$this->fechahoy);
        $result = $this->ModeloGeneral->tabla_inserta("contrato_gastos",$arr);

      }else{//Edicion
        $arr = array("fecha"=>$fecha, "descripcion"=>$descripcion, "importe"=>$importe);
        $result = $this->ModeloGeneral->updateCatalogo_value($arr, array('id'=>$DATA[$i]->id), "contrato_gastos");

      }
    }

    echo $result;
  }


  public function get_table_casetas(){
    $idCont = $this->input->post('id');
    $idUnid = $this->input->post('idU');
  
    $casetas = $this->ModeloGeneral->getselectwhere2('contrato_gastos',array('id_contrato'=>$idCont, 'id_unidad'=>$idUnid, 'tipo'=>1, 'estatus'=>1));
    
    $data['casetas'] = $casetas->result();
    //log_message('error', 'GET TABLE CASETAS: ' . json_encode($data['casetas']));

    echo json_encode($data['casetas']);
  }

  public function get_table_combustibles(){
    $idCont = $this->input->post('id');
    $idUnid = $this->input->post('idU');

    $combustibles = $this->ModeloGeneral->getselectwhere2('contrato_gastos',array('id_contrato'=>$idCont, 'id_unidad'=>$idUnid, 'tipo'=>2, 'estatus'=>1));
    
    
    $data['combustibles'] = $combustibles->result();
    //log_message('error', 'GET TABLE COMBUSTIBLES: ' . json_encode($data['combustibles']));

    echo json_encode($data['combustibles'] );
  }

  public function get_table_otros(){
    $idCont = $this->input->post('id');
    $idUnid = $this->input->post('idU');
    
    $otros = $this->ModeloGeneral->getselectwhere2('contrato_gastos',array('id_contrato'=>$idCont, 'id_unidad'=>$idUnid, 'tipo'=>3, 'estatus'=>1));
    
    $data['otros'] = $otros->result();
    //log_message('error', 'GET TABLE OTROS: ' . json_encode($data['otros']));

    echo json_encode($data['otros']);
  }

  public function get_table_sueldos(){
    $idCont = $this->input->post('id');
    $idUnid = $this->input->post('idU');
    
    $sueldos = $this->ModeloGeneral->getselectwhere2('contrato_gastos',array('id_contrato'=>$idCont, 'id_unidad'=>$idUnid, 'tipo'=>4, 'estatus'=>1));
    
    $data['sueldos'] = $sueldos->result();
    //log_message('error', 'GET TABLE SUELDOS: ' . json_encode($data['sueldos']));

    echo json_encode($data['sueldos']);
  }


  public function insert_bitacora_revisiones(){
    $datos = $this->input->post();
    $id = $datos['id'];

    $id_reg=0;

    $data['km_ini'] = isset($datos['km_ini']) ? $datos['km_ini'] : '0';
    $data['km_fin'] = isset($datos['km_fin']) ? $datos['km_fin'] : '0';

    $data['apto'] = isset($datos['apto']) ? $datos['apto'] : '0';
    
    $data['revF_aceite'] = isset($datos['revF_aceite']) ? '1' : '0';
    $data['revF_frenos'] = isset($datos['revF_frenos']) ? '1' : '0';
    $data['revF_transmision'] = isset($datos['revF_transmision']) ? '1' : '0';
    $data['revF_anticongelante'] = isset($datos['revF_anticongelante']) ? '1' : '0';
    $data['revF_direccion'] = isset($datos['revF_direccion']) ? '1' : '0';
    $data['revF_neumaticos'] = isset($datos['revF_neumaticos']) ? '1' : '0';

    $data['revL_cortas'] = isset($datos['revL_cortas']) ? '1' : '0';
    $data['revL_direc'] = isset($datos['revL_direc']) ? '1' : '0';
    $data['revL_tablero'] = isset($datos['revL_tablero']) ? '1' : '0';
    $data['revL_reversa'] = isset($datos['revL_reversa']) ? '1' : '0';
    $data['revL_largas'] = isset($datos['revL_largas']) ? '1' : '0';
    $data['revL_frenos'] = isset($datos['revL_frenos']) ? '1' : '0';

    //log_message('error','DATOS: '.json_encode($datos));
    //log_message('error','DATA: '.json_encode($data));
    


    if($id > 0){
      $getbr = $this->ModeloGeneral->getselectwhererow2("bitacora_revisiones",array("id"=>$id));
      log_message('error','Getbr: '.json_encode($getbr));
      $kmi_reg = $getbr->km_ini;
      $kmf_reg = $getbr->km_fin;
      if($kmf_reg<$data['km_fin']){
        $difkm = $datos['km_fin'] - $kmf_reg;
        $this->ModeloUnidades->updateKmUnidad($datos['id_unidad'],"+",$difkm,$this->fechahoy); 
        /*
        $getu=$this->ModeloGeneral->getselectwhere2('unidad_prospecto',array('id_contrato'=>$datos["id_contrato"],"estatus"=>1));
        foreach ($getu->result() as $u) {
          //$this->ModeloGeneral->updateCatalogo(array("km_actual=km_actual+"=>$difkm,"ultimo_update"=>$this->fechahoy),'id',$u->unidad,'unidades');
          $this->ModeloUnidades->updateKmUnidad($u->unidad,"+",$difkm,$this->fechahoy); 
        }
        */
      }else{
        $difkm = $kmf_reg - $datos['km_fin'];
        $this->ModeloUnidades->updateKmUnidad($datos['id_unidad'],"-",$difkm,$this->fechahoy);  
        /*
        $getu=$this->ModeloGeneral->getselectwhere2('unidad_prospecto',array('id_contrato'=>$datos["id_contrato"],"estatus"=>1));
        foreach ($getu->result() as $u) {
          $this->ModeloUnidades->updateKmUnidad($u->unidad,"-",$difkm,$this->fechahoy);  
        }
          */
      }

      $this->ModeloGeneral->updateCatalogo($data,'id',$id,'bitacora_revisiones');
      $id_reg = $id; 

    }else{
      //Nuevo
      $data['id'] = $id;
      $data['id_contrato'] = $datos['id_contrato'];
      $data['id_unidad'] = $datos['id_unidad'];
      $data['fecha_reg']=$this->fechahoy;
      $kmf = $data['km_fin'] - $data['km_ini'];
    
      log_message('error','DATA: '.json_encode($data));

      $id_reg = $this->ModeloGeneral->tabla_inserta('bitacora_revisiones',$data);
      $this->ModeloUnidades->updateKmUnidad($datos['id_unidad'],"+",$kmf,$this->fechahoy); 
    }

    $this->ModeloGeneral->updateCatalogo_value(array('bit_viaje'=>1), array('id'=>$datos['id_contrato']), "contratos");
    $this->ModeloGeneral->updateCatalogo_value(array('bit_viaje'=>1), array('id_contrato'=>$datos['id_contrato'],'unidad'=>$datos['id_unidad']), "unidad_prospecto");

    echo $id_reg;

  }


  public function insert_BitaDiasHoras(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    $result = '';
    
    for ($i=0; $i<count($DATA); $i++) {
      $id_contrato = $DATA[$i]->id_contrato;
      $id_unidad = $DATA[$i]->id_unidad;
      //$id_prospecto = $DATA[$i]->id_prospecto;
      $fecha = $DATA[$i]->fecha;

      if($DATA[$i]->id == 0){//Nuevo
        $array_dias = array("id_contrato"=>$id_contrato,"id_unidad"=>$id_unidad, "fecha"=>$fecha, "fecha_reg"=>$this->fechahoy);
        $result = $this->ModeloGeneral->tabla_inserta("bitacora_dias",$array_dias);

        for ($j = 0; $j<count($DATA[$i]->HORAS); $j++) {
          $tipo = $DATA[$i]->HORAS[$j]->tipo;
          $hora_ini = $DATA[$i]->HORAS[$j]->hora_ini;
          $hora_fin = $DATA[$i]->HORAS[$j]->hora_fin;          

          $array_horas = array("id_contrato"=>$id_contrato, "id_unidad"=>$id_unidad, "id_bitDia"=>$result, "tipo"=>$tipo, "hora_ini"=>$hora_ini, "hora_fin"=>$hora_fin, "fecha_reg"=>$this->fechahoy);
          $idDestino = $this->ModeloGeneral->tabla_inserta("bitacora_horas",$array_horas);
        }

      }else{//Edicion
        $array_dias = array("fecha"=>$fecha);
        $result = $this->ModeloGeneral->updateCatalogo_value($array_dias, array('id'=>$DATA[$i]->id), "bitacora_dias");


        for ($j = 0; $j<count($DATA[$i]->HORAS); $j++) {
          
          $id_hora = $DATA[$i]->HORAS[$j]->id;
          $tipo = $DATA[$i]->HORAS[$j]->tipo;
          $hora_ini = $DATA[$i]->HORAS[$j]->hora_ini;
          $hora_fin = $DATA[$i]->HORAS[$j]->hora_fin;  

          if($id_hora == 0){
            $array_horas = array("id_contrato"=>$id_contrato, "id_unidad"=>$id_unidad, "id_bitDia"=>$DATA[$i]->id, "tipo"=>$tipo, "hora_ini"=>$hora_ini, "hora_fin"=>$hora_fin, "fecha_reg"=>$this->fechahoy);
            $idDestino = $this->ModeloGeneral->tabla_inserta("bitacora_horas",$array_horas);
          }else{
            $array_horas = array("tipo"=>$tipo, "hora_ini"=>$hora_ini, "hora_fin"=>$hora_fin);
            $idDestino = $this->ModeloGeneral->updateCatalogo_value($array_horas, array('id'=>$id_hora), "bitacora_horas");
          }
        }

      }
    }

    echo $result;
  }

  
  public function get_table_dias(){
    $id = $this->input->post('id');
    $idUnid = $this->input->post('idU');
    $dias = $this->ModeloGeneral->getselectwhere2('bitacora_dias',array('id_contrato'=>$id, 'id_unidad'=>$idUnid, 'estatus'=>1));
    $data['dias'] = $dias->result();
    foreach ($data['dias'] as $dia) {
      $horas = $this->ModeloGeneral->getselectwhere2('bitacora_horas',array('id_contrato'=>$id, 'id_unidad'=>$idUnid, 'id_bitDia'=>$dia->id, 'estatus'=>1));
      $dia->horas = $horas->result();
    }
    //log_message('error', 'GET TABLE DIAS: ' . json_encode($data['dias']));

    echo json_encode($data['dias']);
  }

  function cargar_imagen(){
    $id = $_POST['id'];
    $DIR_SUC = FCPATH . 'public/uploads/contratos/evidenciaskm';
    $name="img_evide";
    $input_name = 'img_evide';

    $config['upload_path']    = $DIR_SUC;
    $config['allowed_types']  = 'gif|jpg|png|jpeg|bmp';
    $config['max_size']       = 5000;
    $file_names = 'contrato_'.$id;
    $config['file_name'] = $file_names;
    $output = [];

    $this->load->library('upload', $config);
    if (!$this->upload->do_upload($input_name)) {
      $data = array('error' => $this->upload->display_errors());
      //log_message('error', "DOC: " . json_encode($data));
    }else {
      $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
      $file_name = $upload_data['file_name']; //uploded file name
      $extension = $upload_data['file_ext'];    // uploded file extension
      $this->ModeloGeneral->updateCatalogo_value(array($name=>$file_name),array('id'=>$id),'bitacora_revisiones');
      $data = array('upload_data' => $this->upload->data());
    }
    echo json_encode($output);
  }

  function cargar_imagen_f(){
    $id = $_POST['id'];
    $DIR_SUC = FCPATH . 'public/uploads/contratos/evidenciaskm';
    $name="img_evide_f";
    $input_name = 'img_evide_f';

    $config['upload_path']    = $DIR_SUC;
    $config['allowed_types']  = 'gif|jpg|png|jpeg|bmp';
    $config['max_size']       = 5000;
    $file_names = 'contrato_'.$id;
    $config['file_name'] = $file_names;
    $output = [];

    $this->load->library('upload', $config);
    if (!$this->upload->do_upload($input_name)) {
      $data = array('error' => $this->upload->display_errors());
      //log_message('error', "DOC: " . json_encode($data));
    }else {
      $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
      $file_name = $upload_data['file_name']; //uploded file name
      $extension = $upload_data['file_ext'];    // uploded file extension
      $this->ModeloGeneral->updateCatalogo_value(array($name=>$file_name),array('id'=>$id),'bitacora_revisiones');
      $data = array('upload_data' => $this->upload->data());
    }
    echo json_encode($output);
  }

  function delete_img($id){
    $name="img_evide";
    $result = $this->ModeloGeneral->updateCatalogo_value(array($name=>''),array('id'=> $id),'bitacora_revisiones');
    echo $id;
  }

  function delete_img_f($id){
    $name="img_evide_f";
    $result = $this->ModeloGeneral->updateCatalogo_value(array($name=>''),array('id'=> $id),'bitacora_revisiones');
    echo $id;
  }


  function insert_comprobante_pago()
  {
    $id_pago = $_POST['id_comprobante'];

    $input_name = 'file';
    $DIR_SUC = FCPATH . 'public/uploads/pagos';
    $config['upload_path']     = $DIR_SUC;
    $config['allowed_types']   = 'gif|jpg|png|jpeg|bmp|pdf';
    $config['max_size']        = 5000;
    $file_names = 'pago_'.$id_pago.'_'.date('YmdGis');
    $config['file_name'] = $file_names;
    $output = [];


    $this->load->library('upload', $config);
    if (!$this->upload->do_upload($input_name)) {
      $data = array('error' => $this->upload->display_errors());
      //log_message('error', "DOC: " . json_encode($data));
    } else {
      $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
      $file_name = $upload_data['file_name']; //uploded file name
      $extension = $upload_data['file_ext'];    // uploded file extension

      log_message('error', "DOC: " . json_encode($file_name) . ' - ' . json_encode($id_pago));
      //$this->ModeloGeneral->tabla_inserta('pagos_contrato', array('file_pago' => $file_name));
      $this->ModeloGeneral->updateCatalogo(array('file_pago' => $file_name),'id',$id_pago,'pagos_contrato');

      $data = array('upload_data' => $this->upload->data());
    }
    echo json_encode($output);
  }

  function delete_comprobante_pago($id)
  {
    $result = $this->ModeloGeneral->updateCatalogo(array('file_pago' => ''),'id',$id,'pagos_contrato');
    echo $id;
  }

  function insert_contrato_archivado(){
    $id_contrato = $_POST['id_contrato'];

    $input_name = 'file';
    $DIR_SUC = FCPATH . 'public/uploads/contratos/archivados';
    $config['upload_path']     = $DIR_SUC;
    $config['allowed_types']   = 'gif|jpg|png|jpeg|bmp|pdf';
    $config['max_size']        = 5000;
    $file_names = 'contrato_'.$id_contrato.'_'.date('YmdGis');
    $config['file_name'] = $file_names;
    $output = [];

    $this->load->library('upload', $config);
    if (!$this->upload->do_upload($input_name)) {
      $data = array('error' => $this->upload->display_errors());
      //log_message('error', "DOC: " . json_encode($data));
    } else {
      $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
      $file_name = $upload_data['file_name']; //uploded file name
      $extension = $upload_data['file_ext'];    // uploded file extension

      $this->ModeloGeneral->tabla_inserta('contratos_archivados', array('id_contrato'=> $id_contrato, 'file_contrato' => $file_name, 'reg' =>$this->fechahoy));
      //$this->ModeloGeneral->updateCatalogo(array('file_pago' => $file_name),'id',$id_pago,'pagos_contrato');

      $data = array('upload_data' => $this->upload->data());
    }
    echo json_encode($output);
  }

  function get_contratos_archivados(){

    $id = $_POST['idContrato'];

    $data["archived"] = $this->ModeloGeneral->getselectwhere2('contratos_archivados',array('id_contrato'=>$id, 'estatus'=>1));
    log_message('error', 'GET ID: ' . json_encode($id));
    log_message('error', 'GET contratos A: ' . json_encode($data["archived"]->result()));
    echo json_encode($data["archived"]->result());
  }

  function delete_contrato_archivado($id)
  {
    $result = $this->ModeloGeneral->updateCatalogo(array('estatus' => 0),'id',$id,'contratos_archivados');
    echo $id;
  }

  function generarCompPago($idCP,$cuenta,$tipoCuenta,$referencia,$fecha,$monto,$cliente){//+idCP+'/'+cuenta+'/'+tipoCuenta+'/'+referencia+'/'+fecha+'/'+monto
    $data['id'] = $idCP;
    $data['cuenta'] = urldecode($cuenta);
    //log_message('error','cuenta--> ' . json_encode($cuenta));
    //log_message('error','cuentaD--> ' . json_encode($data['cuenta']) );
    $data['tipoCuenta'] = $tipoCuenta;
    $data['referencia']= $referencia == 0 ? '---' : $referencia;
    $data['fecha'] = $fecha;
    $data['monto'] = $monto;

    $data['cliente'] = '';
    if ($cliente > 0){
      $data['cliente'] = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id'=>$cliente, 'estatus'=>1));
    }
    $data['usuario'] = $this->ModeloGeneral->getselectwhererow2('personal',array('personalId'=>$this->session->userdata('idpersonal_tz'),'estatus'=>1));
    $data['contrato'] = $this->ModeloGeneral->getselectwhererow2('contratos',array('id'=>$idCP, 'estatus'=>1));
    $data['pagos'] = $this->ModeloGeneral->getselectwhere2('pagos_contrato',array('id_contrato'=>$idCP, 'estatus'=>1));
    $data['costos'] = $this->ModeloGeneral->getselectwhere2('unidad_prospecto',array('id_contrato'=>$idCP,'estatus'=>1));
    /*
    log_message('error','Contrato:--> ' . json_encode($data['contrato']) );
    log_message('error','Pagos:--> ' . json_encode($data['pagos']->result()) );
    log_message('error','Usuario:--> ' . json_encode($data['usuario']));
    log_message('error','Usuario:--> ' . json_encode($data['usuario']));
    */
    //$data['datapago'] = $this->ModeloGeneral->getselectwhere2('contratos_archivados',array('id_contrato'=>$id, 'estatus'=>1));
    $this->load->view('reportes/documento_comprobante_pago', $data);
  }



  public function insertCompPago(){
    $id_pago = $this->input->post('id');
    $id_contrato = $this->input->post('idCont');
    $id_reg = ' ';

    $file_name = 'pago_'.$id_pago.'_'.date('YmdGis').'.pdf';

    $actualUbi = FCPATH . 'public/pdf/Comprobante_pago_'.$id_contrato.'.pdf';
    $nuevaUbi = FCPATH . 'public/uploads/pagos/'.$file_name;

    rename($actualUbi , $nuevaUbi);

    if($id_pago > 0){
      $this->ModeloGeneral->updateCatalogo(array('file_pago' => $file_name),'id',$id_pago,'pagos_contrato');
      $id_reg = $file_name; 
    }

    echo $id_reg;
  }

  function get_token(){
    $idCliente = $this->input->post('idCliente');
    $idContrato = $this->input->post('idContrato');
    $data = $this->ModeloGeneral->getselectwhererow2('contratos',array('estatus'=>1,'id'=>$idContrato));

    if($data->encuesta > 0){
      echo $idContrato;
      return;
    };

    $get_info = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id' => $idCliente));
    $nombre = $get_info->nombre.$get_info->app.$get_info->apm;
    $pass = $nombre.'mangoo'.$idContrato;

    $this->ModeloContratos->insert_token($idContrato,$pass);

    echo $idContrato;
  }


  function enviar_encuesta(){
    $idCliente = $this->input->post('idCliente');
    $idContrato = $this->input->post('idContrato');

    $get_info = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id' => $idCliente));
    $get_cont = $this->ModeloGeneral->getselectwhererow2('contratos',array('id' => $idContrato));
    //log_message('error', 'GET MAIL: '.json_encode(gettype($get_info)));
    $nombre = $get_info->nombre." ".$get_info->app." ".$get_info->apm;
    $email = $get_info->correo;

    $token = urlencode($get_cont->token);
    $token_cod = str_replace('%', '_', $token);

    //== libreria email ==
    if($email!=""){
      $this->load->library('email');
      $config = array();
      $config['protocol'] = 'smtp';
      $config["smtp_host"] = 'grandturismo.mangoo.systems'; //'mail.mangoo.systems'
      $config["smtp_user"] = 'contacto@grandturismo.mangoo.systems'; //'contacto@mangoo.systems'
      $config["smtp_pass"] = 'xxb{-5dz767h'; //'x!fZTBrOoBGI'

      $config["smtp_port"] = 465;
      $config["smtp_crypto"] = 'ssl';
      $config['mailtype'] = "html";          
      $config['charset'] = 'utf-8'; 
      $config['wordwrap'] = TRUE;
      $config['validate'] = true;
      $config['mailtype'] = 'html';

      $this->email->initialize($config);

      $this->email->from('contacto@grandturismo.mangoo.systems'); //'contacto@mangoo.systems'
      $this->email->to($email);

      $this->email->subject('Encuesta '.$nombre);

      //log_message('error','mail ' . $email);
      log_message('error', site_url('Encuesta/index/'.$idCliente.'/'.$idContrato.'/'.$token_cod.''));

      //--------------------------------------------------------
      //$pdf = FCPATH.'public/pdf/Contrato_'.$idContrato.'.pdf';
      //$this->email->attach($pdf);
      //--------------------------------------------------------

      $this->email->message("Hola, el grupo de <b>Gʀᴀɴᴅ Tᴜʀɪsᴍᴏ Exᴘʀᴇss</b> le pide por favor contestar esta <a href='".site_url('Encuesta/index/'.$idCliente.'/'.$idContrato.'/'.$token_cod)."'>encuesta de satisfaccion.</a> <br>Saludos.");
      if ($this->email->send()) {
        echo 'Email sent.';
      } else {
        echo 'Error ' . $this->email->print_debugger();
      }
    } 
  }

  function send_encuesta_whatsapp(){
    $idCliente = $this->input->post('idCliente');
    $idContrato = $this->input->post('idContrato');
    $data = $this->ModeloGeneral->getselectwhererow2('contratos',array('estatus'=>1,'id'=>$idContrato));
    $out = array();

    if($data->encuesta > 0){
      echo "Encuesta finalizada."; 
      return;
    };

    $get_info = $this->ModeloGeneral->getselectwhererow2('prospectos',array('id' => $idCliente));
    //log_message('error','GET INFO: '.json_encode($get_info));
    $nombre = $get_info->nombre.$get_info->app.$get_info->apm;
    $pass = $nombre.'mangoo'.$idContrato;

    $this->ModeloContratos->insert_token($idContrato,$pass);

    $get_cont = $this->ModeloGeneral->getselectwhererow2('contratos',array('id' => $idContrato));
    $token = urlencode($get_cont->token);
    $token_cod = str_replace('%', '_', $token);
    
    log_message('error', site_url('Encuesta/index/'.$idCliente.'/'.$idContrato.'/'.$token_cod.''));

    echo site_url('Encuesta/index/'.$idCliente.'/'.$idContrato.'/'.$token_cod.'');

  }




  //----------------------------------->
  public function get_data_unidad_contrato(){
    $id = $this->input->post('id');

    $unidades = $this->ModeloContratos->getUnidadesContrato($id);
    $data['unidades'] = $unidades->result();
    //log_message('error', 'UNIDADES POR CONTRATO '.$id.': '.json_encode($data['unidades']));

    $datos = $this->ModeloContratos->dataContratoCliente($id);
    //log_message('error', 'DATOS: '.json_encode($datos));

    $existRG = $this->ModeloContratos->existRG($id);
    //log_message('error', 'existRG: '.json_encode($existRG));

    $existBV = $this->ModeloContratos->existBV($id);
    //log_message('error', 'existBV: '.json_encode($existBV));

    $data['cliente'] = $datos->cliente;
    $data['folio'] = $datos->folio;
    $data['idContrato'] = $datos->idContrato;
    $data['existRG'] = $existRG;
    $data['existBV'] = $existBV;

    //log_message('error', 'DATA: '.json_encode($data));
    echo json_encode($data);

  }



  public function setExist(){
    $idC = $this->input->post('idC');
    $typeF = $this->input->post('type');

    if ($typeF == 'RGF'){
      $idC = $this->ModeloContratos->setExistRG($idC);

    }else if($typeF == 'BVF'){
      $idC = $this->ModeloContratos->setExistBV($idC);
    }
    
    echo $idC;
  }

  public function setUnidad(){
    $idC = $this->input->post('idC');
    $idU = $this->input->post('idU');
    $typeF = $this->input->post('type');

    if ($typeF == 'RGF'){
      $idC = $this->ModeloContratos->setUnidadRG($idC, $idU);
    }else if($typeF == 'BVF'){
      $idC = $this->ModeloContratos->setUnidadBV($idC, $idU);
    }

    echo $idC;
  }

  public function get_dates_itinerario(){
    $idC = $this->input->post('id');
    $msj = "";
    $dates = $this->ModeloContratos->getDatesItinerario($idC);
    //log_message('error','DATES: '.json_encode($dates));
    
    if($dates){
        $msj = $dates->fecha_regreso." ".$dates->hora_regreso."hrs.";
    }
    
    echo $msj;
  }

}