<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloInicio');

        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid_tz');
            $this->idpersonal=$this->session->userdata('idpersonal_tz');
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d');
    }
	public function index(){

		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('inicio');
        $this->load->view('templates/footer');
        $this->load->view('iniciojs');
	}

    
    public function getlistado(){
        /*
            $notiVigenciaLicChofer = $this->ModeloSession->get_vigencia_lic_chofer();
            $notiVigenciaExaChofer = $this->ModeloSession->get_vigencia_exa_chofer();
            $notiVigenciaPolizaUnidad = $this->ModeloSession->get_vigencia_poliza_unidad();
            $notiVerifUnidad = $this->ModeloSession->get_verificacion_unidad();
        */

        $params = $this->input->post();

        //notificaciones Vigencia Licencia Chofer (1)
        $getdata_LicChofer = $this->ModeloInicio->get_result_LicChofer($params);
        $totaldata_LicChofer = $this->ModeloInicio->total_result_LicChofer($params); 

        //notificaciones Vigencia Examen Medico Chofer (2)
        $getdata_ExaChofer = $this->ModeloInicio->get_result_ExaChofer($params);
        $totaldata_ExaChofer = $this->ModeloInicio->total_result_ExaChofer($params); 

        //notificaciones Vigencia Poliza Unidad (3)
        $getdata_PoliUnidad = $this->ModeloInicio->get_result_PoliUnidad($params);
        $totaldata_PoliUnidad = $this->ModeloInicio->total_result_PoliUnidad($params);

        //notificaciones Verificación de unidad cercana (4)
        $getdata_VerifUnidad = $this->ModeloInicio->get_result_VerifUnidad($params);
        $totaldata_VerifUnidad = $this->ModeloInicio->total_result_VerifUnidad($params);

        //Combinacion Notificaciones
        $combinedData = array_merge($getdata_LicChofer->result(), $getdata_ExaChofer->result(), $getdata_PoliUnidad->result(), $getdata_VerifUnidad->result());

        /*
        //Ordenamiento del arreglo
        usort($combinedData, function ($a, $b) {
            if ($a->choferid == $b->choferid) {
                return 0;
            }
            return ($a->choferid > $b->choferid) ? -1 : 1;
        });
        */
        
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata_LicChofer)+intval($totaldata_ExaChofer)+intval($totaldata_PoliUnidad)+intval($totaldata_VerifUnidad),
            "recordsFiltered" => intval($totaldata_LicChofer)+intval($totaldata_ExaChofer)+intval($totaldata_PoliUnidad)+intval($totaldata_VerifUnidad),
            "data"            => $combinedData,
            "query"           =>$this->db->last_query()   
        );

        echo json_encode($json_data);
    }
    

}