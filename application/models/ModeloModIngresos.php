<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloModIngresos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_result($params){
        $columns = array( 
            0=>'mi.id',
            1=>'mi.concepto',
            2=>'mi.monto',
            3=>'mi.fecha',
            4=>'mi.responsable',
            5=>'u.Usuario',
            6=>'mi.folio',
            7=>'mi.descripcion'
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('modulo_ingresos mi');
        $this->db->join('usuarios u', 'mi.usuario = u.UsuarioID', 'left');
        /*$this->db->join('personal p', 'mi.responsable = p.personalId', 'left');*/
        $this->db->where('mi.estatus',1);
        
        if($params["fechaIni"] != "" && $params["fechaFin"] != ""){
            $this->db->where('mi.fecha BETWEEN '.'"'.$params["fechaIni"].'"'.' AND '.'"'.$params["fechaFin"].'"');
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }


    public function total_result($params){
        $columns = array( 
            0=>'mi.id',
            1=>'mi.concepto',
            2=>'mi.monto',
            3=>'mi.fecha',
            4=>'mi.responsable',
            5=>'u.Usuario',
            6=>'mi.folio',
            7=>'mi.descripcion'
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('modulo_ingresos mi');
        /*$this->db->join('usuarios u', 'mi.usuario = u.UsuarioID', 'left');*/
        $this->db->join('personal p', 'mi.responsable = p.personalId', 'left');
        $this->db->where('mi.estatus',1);

        if($params["fechaIni"] != "" && $params["fechaFin"] != ""){
            $this->db->where('mi.fecha BETWEEN '.'"'.$params["fechaIni"].'"'.' AND '.'"'.$params["fechaFin"].'"');
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }


//---Caja chica-------------------------------------------------->

    function get_result_cortec($params){
        $columns1 = array( 
            0=>'id',
            1=>'fecha',
            2=>'concepto',
            3=>'"Ingreso" as tipo',
            4=>'responsable',
            5=>'monto',
            6=>'ui.Usuario',
            7=>'folio',
            8=>'descripcion',
        );

        $columns1s = array( 
            0=>'id',
            1=>'fecha',
            2=>'concepto',
            3=>'responsable',
            4=>'monto',
            5=>'ui.Usuario',
            6=>'folio',
            7=>'descripcion'
        );

        $select="";
        foreach ($columns1 as $c) {
            $select.="$c, ";
        }

        $this->db->select($select);
        $this->db->from('modulo_ingresos');
        $this->db->join('usuarios ui', 'modulo_ingresos.usuario = ui.UsuarioID', 'left');
        $this->db->where('modulo_ingresos.estatus',1);
        
        if($params["fechaIni"] != 0 && $params["fechaFin"] != 0){
            $this->db->where('fecha BETWEEN '.'"'.$params["fechaIni"].'"'.' AND '.'"'.$params["fechaFin"].'"');
        }

        $query1 = $this->db->get_compiled_select(); 

        // Segunda parte de la consulta
        $columns2 = array( 
            0=>'id',
            1=>'fecha',
            2=>'concepto',
            3=>'"Gasto" as tipo',
            4=>'responsable',
            5=>'monto',
            6=>'ug.Usuario',
            7=>'folio',
            8=>'descripcion',
        );
        $columns2s= array( 
            0=>'id',
            1=>'fecha',
            2=>'concepto',
            3=>'responsable',
            4=>'monto',
            5=>'ug.Usuario',
            6=>'folio',
            7=>'descripcion',
        );

        $select = "";
        foreach ($columns2 as $c) {
            $select .= "$c, ";
        }

        $this->db->select($select);
        $this->db->from('modulo_gastos');
        $this->db->join('usuarios ug', 'modulo_gastos.usuario = ug.UsuarioID', 'left');
        $this->db->where('modulo_gastos.estatus', 1);

        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('fecha BETWEEN ' . '"' . $params["fechaIni"] . '"' . ' AND ' . '"' . $params["fechaFin"] . '"');
        }


        if (!empty($params['search']['value'])) {
            $search = $params['search']['value'];
            $this->db->group_start();
            foreach ($columns1s as $c) {
                $this->db->or_like($c, $search);
            }
            $this->db->group_end();
        }

        $this->db->order_by($columns1[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);

        /*
        $query=$this->db->get();
        return $query;
        */

        $query2 = $this->db->get_compiled_select(); 
        $union_query = $query1." UNION ".$query2;
        return $this->db->query($union_query);
    }


    public function total_result_cortec1($params){
        $columns = array( 
            0=>'id',
            1=>'fecha',
            2=>'concepto',
            //3=>'"Ingreso" tipo',
            4=>'responsable',
            5=>'monto',
            6=>'ui.Usuario',
            7=>'folio',
            8=>'descripcion'
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('modulo_ingresos');
        $this->db->join('usuarios ui', 'modulo_ingresos.usuario = ui.UsuarioID', 'left');
        $this->db->where('modulo_ingresos.estatus',1);
        
        if($params["fechaIni"] != 0 && $params["fechaFin"] != 0){
            $this->db->where('fecha BETWEEN '.'"'.$params["fechaIni"].'"'.' AND '.'"'.$params["fechaFin"].'"');
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  

        $query=$this->db->get();
        return $query->row()->total;
    }

    public function total_result_cortec2($params){
        $columns = array( 
            0=>'id',
            1=>'fecha',
            2=>'concepto',
            //3=>'"Gasto" tipo',
            4=>'responsable',
            5=>'monto',
            6=>'ug.Usuario',
            7=>'folio',
            8=>'descripcion'
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('modulo_gastos');
        $this->db->join('usuarios ug', 'modulo_gastos.usuario = ug.UsuarioID', 'left');
        $this->db->where('modulo_gastos.estatus', 1);

        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('fecha BETWEEN ' . '"' . $params["fechaIni"] . '"' . ' AND ' . '"' . $params["fechaFin"] . '"');
        }


        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  

        $query=$this->db->get();
        return $query->row()->total;
    }

    public function get_total_ingresos($params){
        $this->db->select('IFNULL(SUM(monto), 0) AS ingresos');
        $this->db->from('modulo_ingresos');
        $this->db->where('estatus', 1);

        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('fecha BETWEEN ' . '"' . $params["fechaIni"] . '"' . ' AND ' . '"' . $params["fechaFin"] . '"');
        }
        
        $query = $this->db->get();
        return $query->row()->ingresos;
    }

    public function get_total_egresos($params){
        $this->db->select('IFNULL(SUM(monto), 0) AS egresos');
        $this->db->from('modulo_gastos');
        $this->db->where('estatus', 1);

        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('fecha BETWEEN ' . '"' . $params["fechaIni"] . '"' . ' AND ' . '"' . $params["fechaFin"] . '"');
        }
        
        $query = $this->db->get();
        return $query->row()->egresos;
    }

    function get_data_cortec_excel($params){
        $this->db->select('id,folio, fecha, concepto, descripcion, "Ingreso" as tipo, responsable, monto, ui.Usuario');
        $this->db->from('modulo_ingresos');
        $this->db->join('usuarios ui', 'modulo_ingresos.usuario = ui.UsuarioID', 'left');
        $this->db->where('modulo_ingresos.estatus',1);
        
        if($params["fechaIni"] != 0 && $params["fechaFin"] != 0){
            $this->db->where('fecha BETWEEN '.'"'.$params["fechaIni"].'"'.' AND '.'"'.$params["fechaFin"].'"');
        }

        $query1 = $this->db->get_compiled_select(); 

        // Segunda parte de la consulta
        $this->db->select('id,folio, fecha, concepto, descripcion, "Gasto" as tipo, responsable, monto, ug.Usuario');
        $this->db->from('modulo_gastos');
        $this->db->join('usuarios ug', 'modulo_gastos.usuario = ug.UsuarioID', 'left');
        $this->db->where('modulo_gastos.estatus', 1);

        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('fecha BETWEEN ' . '"' . $params["fechaIni"] . '"' . ' AND ' . '"' . $params["fechaFin"] . '"');
        }

        $this->db->order_by('fecha desc');

        $query2 = $this->db->get_compiled_select(); 
        $union_query = $query1." UNION ".$query2;
        return $this->db->query($union_query);
    }

}
