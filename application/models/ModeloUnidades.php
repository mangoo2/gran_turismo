<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloUnidades extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_result($params){
        $columns = array( 
            0=>'id',
            1=>'num_eco',
            2=>'vehiculo',
            3=>'tipo',
            4=>'placas',
            5=>'placas_fed',
            6=>'marca',
            7=>'modelo'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('unidades');
        //$this->db->where("tipo",$params["tipo"]);
        $this->db->where("estatus",1);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_result($params){
        $columns = array( 
            0=>'id',
            1=>'num_eco',
            2=>'vehiculo',
            3=>'tipo',
            4=>'placas',
            5=>'placas_fed',
            6=>'marca',
            7=>'modelo'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('unidades');
        //$this->db->where("tipo",$params["tipo"]);
        $this->db->where('estatus',1);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function updateKmUnidad($id,$opera,$km,$update){
        /*$this->db->set("km_actual=km_actual".$opera." ".$km.", ultimo_update='$update' ");
        $this->db->where("id",$id);
        $this->db->update("unidades");*/
        $sql = "UPDATE unidades 
        SET km_actual=km_actual $opera $km, ultimo_update = '$update' WHERE id = $id";
        $query = $this->db->query($sql);
    }

    public function getUnidadesServReporte($id,$fi,$ff){
        $this->db->select('su.*');
        $this->db->from('servicio_unidades su');
        $this->db->where('id_unidad',$id);
        $this->db->where('estatus',1);
        $this->db->where('fecha BETWEEN "'.$fi.'" AND "'.$ff.'"');
        $this->db->order_by("fecha","asc");
        $query=$this->db->get();
        return $query->result();
    }

    public function getUnidadesVeriReporte($id,$fi,$ff){
        $this->db->select('vu.*, u.num_eco');
        $this->db->from('verificacion_unidades vu');
        $this->db->join("unidades u","u.id=vu.id_unidad");
        $this->db->where('vu.id_unidad',$id);
        $this->db->where('vu.estatus',1);
        $this->db->where('vu.fecha BETWEEN "'.$fi.'" AND "'.$ff.'"');
        $this->db->order_by("vu.fecha","asc");
        $query=$this->db->get();
        return $query->result();
    }

    public function getKmsReporte($fi,$ff){
        $this->db->select('u.num_eco,u.vehiculo, br.km_ini, br.km_fin, con.fecha_salida, con.fecha_regreso, con.folio, up.id_contrato, dp.lugar');
        $this->db->from('unidad_prospecto up');
        $this->db->join('unidades u','u.id=up.unidad and u.estatus=1');
        $this->db->join('destino_prospecto dp','dp.id_contrato=up.id_contrato and dp.estatus=1');
        $this->db->join('contratos con','con.id = up.id_contrato and con.estatus=1');
        $this->db->join('bitacora_revisiones br','br.id_contrato=up.id_contrato and br.estatus=1','left');
        $this->db->where('up.id_contrato >',0);
        $this->db->where('up.estatus',1);
        //$this->db->where('(dp.fecha BETWEEN "'.$fi.'" AND "'.$ff.'" or dp.fecha_regreso BETWEEN "'.$fi.'" AND "'.$ff.'" )');
        $this->db->where('(con.fecha_salida BETWEEN "'.$fi.'" AND "'.$ff.'" or con.fecha_regreso BETWEEN "'.$fi.'" AND "'.$ff.'" )');
        //$this->db->or_where('dp.fecha_regreso BETWEEN "'.$fi.'" AND "'.$ff.'"');
        $this->db->group_by("u.id");
        $this->db->order_by("fecha_salida","asc");
        $query=$this->db->get();
        return $query->result();
    }
}