<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloPersonal extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_result($params){
        $columns = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'p.apellido_p',
            3=>'p.apellido_m',
            4=>'p.telefono',
            5=>'p.fecha_ingreso',
            6=>'p.puesto',
            7=>'u.Usuario',
            8=>'pf.nombre AS perfil'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('personal p');
        $this->db->where('p.estatus',1);
        $this->db->join('usuarios AS u', 'p.personalId = u.personalId', 'left');
        $this->db->join('perfiles AS pf', 'u.perfilId = pf.perfilId', 'left');

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }


    public function total_result($params){
        $columns = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'p.apellido_p',
            3=>'p.apellido_m',
            4=>'p.telefono',
            5=>'p.fecha_ingreso',
            6=>'p.puesto',
            7=>'u.Usuario',
            8=>'pf.nombre AS perfil'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('personal p');
        $this->db->where('p.estatus',1);
        $this->db->join('usuarios AS u', 'p.personalId = u.personalId', 'left');
        $this->db->join('perfiles AS pf', 'u.perfilId = pf.perfilId', 'left');

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
}
