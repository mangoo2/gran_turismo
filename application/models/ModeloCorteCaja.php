<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModeloCorteCaja extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }


    function get_result_anticipo($params)
    {
        $columns = array(
            0 => 'cont.id',
            1 => 'cont.fecha_contrato',
            2 => 'cont.monto_anticipo',
            3 => 'cont.porc_desc',
            4 => 'concat(clien.nombre," ",clien.app," ",clien.apm) AS cliente',
            5 => 'cont.folio',

        );
        $columns2 = array(
            0 => 'cont.id',
            1 => 'cont.fecha_contrato',
            2 => 'cont.monto_anticipo',
            3 => 'cont.porc_desc',
            4 => 'concat(clien.nombre," ",clien.app," ",clien.apm) AS cliente',
            5 => 'cont.folio',
        );

        $select = "";
        foreach ($columns as $c) {
            $select .= "$c, ";
        }
        $this->db->select($select);
        $this->db->from('contratos cont');
        $this->db->join('prospectos clien', 'cont.idCliente = clien.id', 'left');
        $this->db->where('cont.estatus', 1);
        $this->db->where('cont.contrato', 1);

        /*---------------------*/
        $this->db->group_start();
        $this->db->where('cont.monto_anticipo !=', 0);
        $this->db->or_where('cont.porc_desc !=', 0);
        $this->db->group_end();
        /*---------------------*/


        if ($params["idContrato"] != "0") {
            $this->db->where("cont.id", $params["idContrato"]);
        }
        if ($params["idCliente"] != "0") {
            $this->db->where("cont.idCliente", $params["idCliente"]);
        }
        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('cont.fecha_contrato BETWEEN ' . '"' . $params["fechaIni"] . '"' . ' AND ' . '"' . $params["fechaFin"] . '"');
        }
        $this->db->order_by('cont.fecha_contrato', "DESC");

        if (!empty($params['search']['value'])) {
            $search = $params['search']['value'];
            $this->db->group_start();
            foreach ($columns2 as $c) {
                $this->db->or_like($c, $search);
            }
            $this->db->group_end();
        }
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'], $params['start']);
        $query = $this->db->get();
        return $query;
    }

    public function total_result_anticipo($params)
    {
        $columns2 = array(
            0 => 'cont.id',
            1 => 'cont.fecha_contrato',
            2 => 'cont.monto_anticipo',
            3 => 'cont.porc_desc',
            4 => 'concat(clien.nombre," ",clien.app," ",clien.apm) AS cliente',
            5 => 'cont.folio',
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('contratos cont');
        $this->db->join('prospectos clien', 'cont.idCliente = clien.id', 'left');
        $this->db->where('cont.estatus', 1);
        $this->db->where('cont.contrato', 1);

        /*---------------------*/
        $this->db->group_start();
        $this->db->where('cont.monto_anticipo !=', 0);
        $this->db->or_where('cont.porc_desc !=', 0);
        $this->db->group_end();
        /*---------------------*/

        if ($params["idContrato"] != "0") {
            $this->db->where("cont.id", $params["idContrato"]);
        }
        if ($params["idCliente"] != "0") {
            $this->db->where("cont.idCliente", $params["idCliente"]);
        }
        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('cont.fecha_contrato BETWEEN ' . '"' . $params["fechaIni"] . '"' . ' AND ' . '"' . $params["fechaFin"] . '"');
        }

        if (!empty($params['search']['value'])) {
            $search = $params['search']['value'];
            $this->db->group_start();
            foreach ($columns2 as $c) {
                $this->db->or_like($c, $search);
            }
            $this->db->group_end();
        }
        $query = $this->db->get();
        return $query->row()->total;
    }


    function get_result_pagos($params)
    {
        $columns = array(
            0 => 'pcont.id_contrato',
            1 => 'pcont.fecha',
            2 => 'pcont.monto',
            3 => 'cuen.cuenta',
            4 => 'concat(clien.nombre," ",clien.app," ",clien.apm) AS cliente',
            5 => 'cont.folio',

        );
        $columns2 = array(
            0 => 'pcont.id_contrato',
            1 => 'pcont.fecha',
            2 => 'pcont.monto',
            3 => 'cuen.cuenta',
            4 => 'concat(clien.nombre," ",clien.app," ",clien.apm) AS cliente',
            5 => 'cont.folio',
        );

        $select = "";
        foreach ($columns as $c) {
            $select .= "$c, ";
        }
        $this->db->select($select);
        $this->db->from('pagos_contrato pcont');
        $this->db->join('cuentas cuen', 'pcont.id_cuenta = cuen.id', 'left');
        $this->db->join('contratos cont', 'pcont.id_contrato = cont.id', 'left');
        $this->db->join('prospectos clien', 'cont.idCliente = clien.id', 'left');
        $this->db->where('pcont.estatus', 1);
        $this->db->where('cont.contrato', 1);
        $this->db->where('cont.estatus', 1);

        if ($params["idContrato"] != "0") {
            $this->db->where("pcont.id_contrato", $params["idContrato"]);
        }
        if ($params["idCliente"] != "0") {
            $this->db->where("cont.idCliente", $params["idCliente"]);
        }
        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('pcont.fecha BETWEEN ' . '"' . $params["fechaIni"] . '"' . ' AND ' . '"' . $params["fechaFin"] . '"');
        }
        $this->db->order_by('pcont.fecha', "DESC");

        if (!empty($params['search']['value'])) {
            $search = $params['search']['value'];
            $this->db->group_start();
            foreach ($columns2 as $c) {
                $this->db->or_like($c, $search);
            }
            $this->db->group_end();
        }
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'], $params['start']);
        $query = $this->db->get();
        return $query;
    }

    public function total_result_pagos($params)
    {
        $columns2 = array(
            0 => 'pcont.id_contrato',
            1 => 'pcont.fecha',
            2 => 'pcont.monto',
            3 => 'cuen.cuenta',
            4 => 'concat(clien.nombre," ",clien.app," ",clien.apm) AS cliente',
            5 => 'cont.folio',
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('pagos_contrato pcont');
        $this->db->join('cuentas cuen', 'pcont.id_cuenta = cuen.id', 'left');
        $this->db->join('contratos cont', 'pcont.id_contrato = cont.id', 'left');
        $this->db->join('prospectos clien', 'cont.idCliente = clien.id', 'left');
        $this->db->where('pcont.estatus', 1);
        $this->db->where('cont.contrato', 1);
        $this->db->where('cont.estatus', 1);

        if ($params["idContrato"] != "0") {
            $this->db->where("pcont.id_contrato", $params["idContrato"]);
        }
        if ($params["idCliente"] != "0") {
            $this->db->where("cont.idCliente", $params["idCliente"]);
        }
        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('pcont.fecha BETWEEN ' . '"' . $params["fechaIni"] . '"' . ' AND ' . '"' . $params["fechaFin"] . '"');
        }

        if (!empty($params['search']['value'])) {
            $search = $params['search']['value'];
            $this->db->group_start();
            foreach ($columns2 as $c) {
                $this->db->or_like($c, $search);
            }
            $this->db->group_end();
        }
        $query = $this->db->get();
        return $query->row()->total;
    }


    function get_result_gastos($params)
    {
        $columns = array(
            0 => 'cgast.id_contrato',
            1 => 'concat(clien.nombre," ",clien.app," ",clien.apm) AS cliente',
            2 => 'IFNULL(unidades.num_eco, "--SIN UNIDAD--") AS num_eco',
            3 => 'IFNULL(unidades.vehiculo, "--SIN UNIDAD--") AS vehiculo',
            4 => 'cgast.fecha',
            5 => 'cgast.importe',
            6 => 'cgast.tipo',
            7 => 'cont.folio',
        );

        $columns2 = array(
            0 => 'cgast.id_contrato',
            1 => 'clien.nombre',
            2 => 'unidades.num_eco',
            3 => 'unidades.vehiculo',
            4 => 'cgast.fecha',
            5 => 'cgast.importe',
            6 => 'cgast.tipo',
            7 => 'cont.folio',
        );

        $select = "";
        foreach ($columns as $c) {
            $select .= "$c, ";
        }

        $this->db->select($select);
        $this->db->from('contrato_gastos cgast');
        $this->db->join('contratos cont', 'cgast.id_contrato = cont.id', 'left');
        $this->db->join('prospectos clien', 'cont.idCliente = clien.id', 'left');
        $this->db->join('unidades', 'cgast.id_unidad = unidades.id','left');
        $this->db->where('cgast.estatus', 1);
        $this->db->where('cont.contrato', 1);
        $this->db->where('cont.estatus', 1);
        $this->db->where('cgast.tipo !=', 0);

        if ($params["idContrato"] != "0") {
            $this->db->where("cgast.id_contrato", $params["idContrato"]);
        }
        if ($params["idCliente"] != "0") {
            $this->db->where("cont.idCliente", $params["idCliente"]);
        }
        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('cgast.fecha BETWEEN ' . '"' . $params["fechaIni"] . '"' . ' AND ' . '"' . $params["fechaFin"] . '"');
        }
        //$this->db->order_by('cgast.fecha', "DESC");

        if (!empty($params['search']['value'])) {
            $search = $params['search']['value'];
            $this->db->group_start();
            foreach ($columns2 as $c) {
                $this->db->or_like($c, $search);
            }
            $this->db->group_end();
        }
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'], $params['start']);
        $query = $this->db->get();
        return $query;
    }

    public function total_result_gastos($params)
    {
        $columns2 = array(
            0 => 'cgast.id_contrato',
            1 => 'clien.nombre',
            2 => 'unidades.num_eco',
            3 => 'unidades.vehiculo',
            4 => 'cgast.fecha',
            5 => 'cgast.importe',
            6 => 'cgast.tipo',
            7 => 'cont.folio',
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('contrato_gastos cgast');
        $this->db->join('contratos cont', 'cgast.id_contrato = cont.id', 'left');
        $this->db->join('prospectos clien', 'cont.idCliente = clien.id', 'left');
        $this->db->join('unidades', 'cgast.id_unidad = unidades.id', 'left');
        $this->db->where('cgast.estatus', 1);
        $this->db->where('cont.contrato', 1);
        $this->db->where('cont.estatus', 1);
        $this->db->where('cgast.tipo !=', 0);

        if ($params["idContrato"] != "0") {
            $this->db->where("cgast.id_contrato", $params["idContrato"]);
        }
        if ($params["idCliente"] != "0") {
            $this->db->where("cont.idCliente", $params["idCliente"]);
        }
        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('cgast.fecha BETWEEN ' . '"' . $params["fechaIni"] . '"' . ' AND ' . '"' . $params["fechaFin"] . '"');
        }

        if (!empty($params['search']['value'])) {
            $search = $params['search']['value'];
            $this->db->group_start();
            foreach ($columns2 as $c) {
                $this->db->or_like($c, $search);
            }
            $this->db->group_end();
        }
        $query = $this->db->get();
        return $query->row()->total;
    }


    public function search_cliente($search)
    {
        /*
        $strq = "SELECT cli.id, cli.nombre, cli.app, cli.apm
                FROM prospectos AS cli
                WHERE cli.estatus = 1 
                AND cli.tipo = 1 
                AND (cli.nombre LIKE '%$search%' OR cli.app LIKE '%$search%' OR cli.apm LIKE '%$search%')";
        $query = $this->db->query($strq);
        return $query->result();
        */

        $this->db->select('cli.id, cli.nombre, cli.app, cli.apm');
        $this->db->from('prospectos AS cli');
        $this->db->where('cli.estatus', 1);
        $this->db->where('cli.tipo', 1);

        /*---------------------*/
        $this->db->group_start();
        $this->db->like('cli.nombre', $search);
        $this->db->or_like('cli.app', $search);
        $this->db->or_like('cli.apm', $search);
        $this->db->group_end();
        /*---------------------*/

        $query = $this->db->get();
        //$this->db->close();
        return $query->result();
    }



    public function get_contratos_corte($params)
    {
        $this->db->select('cont.id');
        $this->db->from('contratos cont');
        $this->db->where('cont.estatus', 1);
        $this->db->where('cont.contrato', 1);

        if ($params["idContrato"] != "0") {
            $this->db->where("cont.id", $params["idContrato"]);
        }
        if ($params["idCliente"] != "0") {
            $this->db->where("cont.idCliente", $params["idCliente"]);
        }
        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('cont.fecha_contrato BETWEEN ' . '"' . $params["fechaIni"] . '"' . ' AND ' . '"' . $params["fechaFin"] . '"');
        }
        $query = $this->db->get();
        return $query->result();
        /*
        $results = $query->result_array();
        
        $idArray = array_map(function($row) {
            return (int)$row['id'];
        }, $results);

        return json_encode($idArray);
        */
    }


    public function get_anticipos_totales_corte_contrato($params)
    {
        $this->db->select('IFNULL(SUM(cont.monto_anticipo), 0) AS anticipo, IFNULL(SUM(cont.porc_desc), 0) AS descuento');
        $this->db->from('contratos cont');
        $this->db->join('prospectos clien', 'cont.idCliente = clien.id', 'left');
        $this->db->where('cont.estatus', 1);
        $this->db->where('cont.contrato', 1);

        if ($params["idContrato"] != "0") {
            $this->db->where("cont.id", $params["idContrato"]);
        }
        if ($params["idCliente"] != "0") {
            $this->db->where("cont.idCliente", $params["idCliente"]);
        }
        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('cont.fecha_contrato BETWEEN ' . '"' . $params["fechaIni"] . '"' . ' AND ' . '"' . $params["fechaFin"] . '"');
        }
        $query = $this->db->get();
        //return $query->result();
        return $query->row();
        //return $query->row()->gastos;
    }

    public function get_pagos_totales_corte_contrato($params)
    {
        $this->db->select('IFNULL(SUM(pcont.monto), 0) AS pagos, 
        IFNULL(SUM(CASE WHEN id_cuenta = 1 THEN monto ELSE 0 END), 0) AS pagos_efectivo,
        IFNULL(SUM(CASE WHEN id_cuenta > 1 THEN monto ELSE 0 END), 0) AS pagos_cuenta');
        $this->db->from('pagos_contrato pcont');
        $this->db->join('cuentas cuen', 'pcont.id_cuenta = cuen.id', 'left');
        $this->db->join('contratos cont', 'pcont.id_contrato = cont.id', 'left');
        $this->db->join('prospectos clien', 'cont.idCliente = clien.id', 'left');
        $this->db->where('pcont.estatus', 1);
        $this->db->where('cont.contrato', 1);
        $this->db->where('cont.estatus', 1);

        if ($params["idContrato"] != "0") {
            $this->db->where("pcont.id_contrato", $params["idContrato"]);
        }
        if ($params["idCliente"] != "0") {
            $this->db->where("cont.idCliente", $params["idCliente"]);
        }
        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('pcont.fecha BETWEEN ' . '"' . $params["fechaIni"] . '"' . ' AND ' . '"' . $params["fechaFin"] . '"');
        }
        $query = $this->db->get();
        //return $query->result();
        return $query->row();
        //return $query->row()->pagos;
    }

    public function get_gastos_totales_corte_contrato($params)
    {
        $this->db->select('IFNULL(SUM(cgast.importe), 0) AS gastos');
        $this->db->from('contrato_gastos cgast');
        $this->db->join('contratos cont', 'cgast.id_contrato = cont.id', 'left');
        $this->db->join('prospectos clien', 'cont.idCliente = clien.id', 'left');
        $this->db->where('cgast.estatus', 1);
        //  $this->db->where('cgast.id_unidad >', 0);
        $this->db->where('cont.contrato', 1);
        $this->db->where('cont.estatus', 1);
        $this->db->where('cgast.tipo !=', 0);

        if ($params["idContrato"] != "0") {
            $this->db->where("cgast.id_contrato", $params["idContrato"]);
        }
        if ($params["idCliente"] != "0") {
            $this->db->where("cont.idCliente", $params["idCliente"]);
        }
        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('cgast.fecha BETWEEN ' . '"' . $params["fechaIni"] . '"' . ' AND ' . '"' . $params["fechaFin"] . '"');
        }
        $query = $this->db->get();
        //return $query->result();
        return $query->row();
        //return $query->row()->gastos;
    }


    public function get_clientes_from_contrato($id)
    {
        $this->db->select('CONCAT(cli.nombre," ",cli.app," ",cli.apm) AS cliente');
        $this->db->from('contratos con');
        $this->db->join('prospectos cli', 'con.idCliente = cli.id', 'left');
        $this->db->where('con.estatus', 1);
        $this->db->where('cli.estatus', 1);
        $this->db->where('con.id', $id);
        $query = $this->db->get();
        return $query->row()->cliente;
    }



    public function get_pagos_efectivo_corte_contrato($params)
    {
        $this->db->select('IFNULL(SUM(CASE WHEN id_cuenta = 1 THEN monto ELSE 0 END), 0) AS pagos_efectivo');
        $this->db->from('pagos_contrato pcont');
        $this->db->join('cuentas cuen', 'pcont.id_cuenta = cuen.id', 'left');
        $this->db->join('contratos cont', 'pcont.id_contrato = cont.id', 'left');
        $this->db->join('prospectos clien', 'cont.idCliente = clien.id', 'left');
        $this->db->where('pcont.estatus', 1);
        $this->db->where('cont.contrato', 1);
        $this->db->where('cont.estatus', 1);

        if ($params["idContrato"] != "0") {
            $this->db->where("pcont.id_contrato", $params["idContrato"]);
        }
        if ($params["idCliente"] != "0") {
            $this->db->where("cont.idCliente", $params["idCliente"]);
        }
        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('pcont.fecha BETWEEN ' . '"' . $params["fechaIni"] . '"' . ' AND ' . '"' . $params["fechaFin"] . '"');
        }
        $query = $this->db->get();
        return $query->row();
    }

    public function get_pagos_cuentas_corte_contrato($params)
    {
        $this->db->select('IFNULL(SUM(CASE WHEN id_cuenta > 1 THEN monto ELSE 0 END), 0) AS pagos_cuenta, cuen.cuenta');
        $this->db->from('pagos_contrato pcont');
        $this->db->join('cuentas cuen', 'pcont.id_cuenta = cuen.id', 'left');
        $this->db->join('contratos cont', 'pcont.id_contrato = cont.id', 'left');
        $this->db->join('prospectos clien', 'cont.idCliente = clien.id', 'left');
        $this->db->where('pcont.estatus', 1);
        $this->db->where('pcont.id_cuenta >', 1);
        $this->db->where('cont.contrato', 1);
        $this->db->where('cont.estatus', 1);

        if ($params["idContrato"] != "0") {
            $this->db->where("pcont.id_contrato", $params["idContrato"]);
        }
        if ($params["idCliente"] != "0") {
            $this->db->where("cont.idCliente", $params["idCliente"]);
        }
        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('pcont.fecha BETWEEN ' . '"' . $params["fechaIni"] . '"' . ' AND ' . '"' . $params["fechaFin"] . '"');
        }
        
        $this->db->group_by('id_cuenta');

        $query = $this->db->get();
        return $query->result();
        //return $query->row();
        //return $query->row()->pagos;
    }


    function get_table_anticipo($params)
    {
        $this->db->select('cont.id, cont.folio, cont.fecha_contrato, cont.monto_anticipo, cont.porc_desc, concat(clien.nombre," ",clien.app," ",clien.apm) AS cliente');
        $this->db->from('contratos cont');
        $this->db->join('prospectos clien', 'cont.idCliente = clien.id', 'left');
        $this->db->where('cont.estatus', 1);
        $this->db->where('cont.contrato', 1);

        /*---------------------*/
        $this->db->group_start();
        $this->db->where('cont.monto_anticipo !=', 0);
        $this->db->or_where('cont.porc_desc !=', 0);
        $this->db->group_end();
        /*---------------------*/

        if ($params["idContrato"] != "0") {
            $this->db->where("cont.id", $params["idContrato"]);
        }
        if ($params["idCliente"] != "0") {
            $this->db->where("cont.idCliente", $params["idCliente"]);
        }
        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('cont.fecha_contrato BETWEEN ' . '"' . $params["fechaIni"] . '"' . ' AND ' . '"' . $params["fechaFin"] . '"');
        }
        $this->db->order_by('cont.fecha_contrato', "DESC");

        $query = $this->db->get();
        return $query->result();
    }

    function get_table_pagos($params)
    {
        $this->db->select('pcont.id_contrato, pcont.fecha, pcont.monto, cuen.cuenta, concat(clien.nombre," ",clien.app," ",clien.apm) AS cliente, cont.folio');
        $this->db->from('pagos_contrato pcont');
        $this->db->join('cuentas cuen', 'pcont.id_cuenta = cuen.id', 'left');
        $this->db->join('contratos cont', 'pcont.id_contrato = cont.id', 'left');
        $this->db->join('prospectos clien', 'cont.idCliente = clien.id', 'left');
        $this->db->where('pcont.estatus', 1);
        $this->db->where('cont.contrato', 1);
        $this->db->where('cont.estatus', 1);

        if ($params["idContrato"] != "0") {
            $this->db->where("pcont.id_contrato", $params["idContrato"]);
        }
        if ($params["idCliente"] != "0") {
            $this->db->where("cont.idCliente", $params["idCliente"]);
        }
        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('pcont.fecha BETWEEN ' . '"' . $params["fechaIni"] . '"' . ' AND ' . '"' . $params["fechaFin"] . '"');
        }
        $this->db->order_by('pcont.fecha', "DESC");

        $query = $this->db->get();
        return $query->result();
    }


    function get_table_gastos($params)
    {
        $this->db->select('cgast.id_contrato, cgast.fecha, cgast.importe, cgast.tipo, concat(clien.nombre," ",clien.app," ",clien.apm) AS cliente, IFNULL(unidades.vehiculo, "--SIN UNIDAD--") AS vehiculo, IFNULL(unidades.num_eco, "--SIN UNIDAD--") AS num_eco, cont.folio');
        $this->db->from('contrato_gastos cgast');
        $this->db->join('contratos cont', 'cgast.id_contrato = cont.id', 'left');
        $this->db->join('prospectos clien', 'cont.idCliente = clien.id', 'left');
        $this->db->join('unidades', 'cgast.id_unidad = unidades.id', 'left');
        $this->db->where('cgast.estatus', 1);
        $this->db->where('cont.contrato', 1);
        $this->db->where('cont.estatus', 1);
        $this->db->where('cgast.tipo !=', 0);

        if ($params["idContrato"] != "0") {
            $this->db->where("cgast.id_contrato", $params["idContrato"]);
        }
        if ($params["idCliente"] != "0") {
            $this->db->where("cont.idCliente", $params["idCliente"]);
        }
        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('cgast.fecha BETWEEN ' . '"' . $params["fechaIni"] . '"' . ' AND ' . '"' . $params["fechaFin"] . '"');
        }
        $this->db->order_by('cgast.fecha', "DESC");

        $query = $this->db->get();
        return $query->result();
    }


}
