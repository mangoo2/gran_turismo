<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloSession extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function login($usu,$pass) {
        $strq = "SELECT usu.UsuarioID,per.personalId,per.nombre, usu.perfilId, usu.contrasena,perf.nombre AS perfil_nombre
                FROM usuarios AS usu
                INNER JOIN perfiles AS perf ON perf.perfilId = usu.perfilId
                INNER JOIN personal AS per ON per.personalId = usu.personalId
                WHERE usu.estatus = 1 AND perf.estatus = 1 AND usu.Usuario = '$usu'";
        $count = 0;
        $passwo =0;
        $query = $this->db->query($strq);
        
        foreach ($query->result() as $row) {
            $passwo =$row->contrasena;
            $id = $row->UsuarioID;
            $nom =$row->nombre;
            $perfil = $row->perfilId; 
            $idpersonal = $row->personalId;  
            $perfil_nombre = $row->perfil_nombre;

            $verificar = password_verify($pass,$passwo);
            if ($verificar) {
                $data = array(
                        'logeado' => true,
                        'usuarioid_tz' => $id,
                        'usuario_tz' => $nom,
                        'perfilid_tz'=>$perfil,
                        'idpersonal_tz'=>$idpersonal,
                        'perfil_nombre'=>$perfil_nombre,
                    );
                $this->session->set_userdata($data);
                $count=1;
            }
        } 


        if($count == 0){
            $strq = "SELECT usu.UsuarioID,per.choferid AS personalId,per.nombre, usu.perfilId, usu.contrasena,perf.nombre AS perfil_nombre
                FROM usuarios AS usu
                INNER JOIN perfiles AS perf ON perf.perfilId = usu.perfilId
                INNER JOIN choferes AS per ON per.choferid = usu.choferId
                WHERE usu.estatus = 1 AND perf.estatus = 1 AND usu.Usuario = '$usu'";
            $count = 0;
            $passwo =0;
            $query = $this->db->query($strq);

            
            foreach ($query->result() as $row) {
                $passwo =$row->contrasena;
                $id = $row->UsuarioID;
                $nom =$row->nombre;
                $perfil = $row->perfilId; 
                $idpersonal = $row->personalId;  
                $perfil_nombre = $row->perfil_nombre;

                $verificar = password_verify($pass,$passwo);
                if ($verificar) {
                    $data = array(
                            'logeado' => true,
                            'usuarioid_tz' => $id,
                            'usuario_tz' => $nom,
                            'perfilid_tz'=>$perfil,
                            'idpersonal_tz'=>$idpersonal,
                            'perfil_nombre'=>$perfil_nombre,
                        );
                    $this->session->set_userdata($data);
                    $count=1;
                }
            } 
        }


        
        echo $count;
    }

    public function menus($perfil){
        $strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='$perfil' ORDER BY men.orden ASC";
        $query = $this->db->query($strq);
        return $query;
    }
    
    public function submenus($perfil,$menu,$tipo){
        $strq ="SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='$perfil' AND menus.tipo='$tipo' and menus.MenuId='$menu' ORDER BY menus.Nombre ASC";
        $query = $this->db->query($strq);
        return $query;
    }   

    function getviewpermiso($perfil,$modulo){
        $strq = "SELECT COUNT(*) as total FROM perfiles_detalles WHERE perfilId=$perfil AND MenusubId=$modulo";
        //log_message('error', $strq);
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    } 
    
    /*-------------------------------------------------------*/
    function get_vigencia_lic_chofer(){
        $sql = "SELECT *
        FROM choferes AS c
        WHERE c.estatus = 1
        AND c.vigencia_licencia != 0000-00-00
        AND c.vigencia_licencia < DATE_ADD(CURDATE(), INTERVAL 3 MONTH) + INTERVAL 1 DAY";

        $query = $this->db->query($sql);
        return $query;
    }

    function get_vigencia_exa_chofer(){
        $sql = "SELECT *
        FROM choferes AS c
        WHERE c.estatus = 1
        AND c.vigencia_examen != 0000-00-00
        AND c.vigencia_examen < DATE_ADD(CURDATE(), INTERVAL 3 MONTH) + INTERVAL 1 DAY";

        $query = $this->db->query($sql);
        return $query;
    }

    function get_vigencia_poliza_unidad(){
        $sql = "SELECT *
        FROM unidades AS u
        WHERE u.estatus = 1 
        AND u.vencimiento_poliza != 0000-00-00
        AND u.vencimiento_poliza < DATE_ADD(CURDATE(), INTERVAL 3 MONTH) + INTERVAL 1 DAY";

        $query = $this->db->query($sql);
        return $query;
    }

    function get_verificacion_unidad(){
        $sql = "SELECT *
        FROM unidades AS u
        WHERE u.estatus = 1 
        AND u.prox_ver != 0000-00-00
        AND u.prox_ver < DATE_ADD(CURDATE(), INTERVAL 3 MONTH) + INTERVAL 1 DAY";

        $query = $this->db->query($sql);
        return $query;
    }

}