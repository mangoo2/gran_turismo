<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloEncuestaR extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getDataEncuesta($id){
        $this->db->select('en.reg, en.id_contrato, CONCAT( cli.nombre, " ", cli.app, " ", cli.apm) AS cliente');
        $this->db->from('encuesta en');
        $this->db->join('prospectos cli','en.id_cliente = cli.id','left');
        $this->db->where('en.id',$id);
        $this->db->where('en.estatus',1);
        $query=$this->db->get();
        return $query->row();
    }

    public function getDataEncuestaExcel(){
        $this->db->select('en.*, CONCAT( cli.nombre, " ", cli.app, " ", cli.apm) AS cliente');
        $this->db->from('encuesta en');
        $this->db->join('prospectos cli','en.id_cliente = cli.id','left');
        $this->db->where('en.estatus',1);
        $query=$this->db->get();
        return $query->result();
    }

}