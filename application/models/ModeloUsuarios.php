<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModeloUsuarios extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function usuarios_insert($usu, $pass, $perf, $perfP, $perfC)
    {
        $pass = password_hash($pass, PASSWORD_BCRYPT);
        $strq = "INSERT INTO usuarios (perfilId, personalId, choferId, Usuario, contrasena) VALUES ('$perf','$perfP','$perfC','$usu','$pass')";
        $this->db->query($strq);
    }

    public function usuarios_update($id, $usu, $pass, $perf, $perfP, $perfC)
    {

        if ($pass == 'xxxx') {
            $strq = "UPDATE usuarios SET perfilId='$perf', personalId='$perfP', choferId='$perfC',Usuario='$usu' WHERE UsuarioID='$id'";
        } else {
            $pass = password_hash($pass, PASSWORD_BCRYPT);
            $strq = "UPDATE usuarios SET perfilId='$perf', personalId='$perfP', choferId='$perfC',Usuario='$usu', contrasena='$pass' WHERE UsuarioID='$id'";
        }

        $this->db->query($strq);
    }

    public function usuariosPersonal_update($id, $usu, $pass, $perf, $perfP)
    {

        if ($pass == 'xxxx') {
            $strq = "UPDATE usuarios SET perfilId='$perf', personalId='$perfP', Usuario='$usu' WHERE UsuarioID='$id'";
        } else {
            $pass = password_hash($pass, PASSWORD_BCRYPT);
            $strq = "UPDATE usuarios SET perfilId='$perf', personalId='$perfP', Usuario='$usu', contrasena='$pass' WHERE UsuarioID='$id'";
        }

        $this->db->query($strq);
    }

    public function usuariosChofer_update($id, $usu, $pass, $perf, $perfC)
    {

        if ($pass == 'xxxx') {
            $strq = "UPDATE usuarios SET perfilId='$perf', choferId='$perfC', Usuario='$usu' WHERE UsuarioID='$id'";
        } else {
            $pass = password_hash($pass, PASSWORD_BCRYPT);
            $strq = "UPDATE usuarios SET perfilId='$perf', choferId='$perfC', Usuario='$usu', contrasena='$pass' WHERE UsuarioID='$id'";
        }

        $this->db->query($strq);
    }

    public function get_existing_user($usu)
    {
        $sql = "SELECT UsuarioID FROM usuarios AS u
        LEFT JOIN personal AS p ON u.personalId = p.personalId
        WHERE u.Usuario = '$usu'
        AND p.estatus != 0 ";

        $query = $this->db->query($sql);
        return $query->row();
    }
}
