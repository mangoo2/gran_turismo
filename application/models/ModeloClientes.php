<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloClientes extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_result($params){
        $columns = array( 
            0=>'id',
            1=>'nombre',
            2=>'app',
            3=>'apm',
            4=>'lugar_origen',
            5=>'telefono',
            6=>'correo',
            7=>'fecha_salida',
            8=>'hora_salida',
            9=>'contrato',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('prospectos p');
        $this->db->where("tipo",$params["tipo"]);
        $this->db->where("estatus",1);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_result($params){
        $columns = array( 
            0=>'id',
            1=>'nombre',
            2=>'app',
            3=>'apm',
            4=>'lugar_origen',
            5=>'telefono',
            6=>'correo',
            7=>'fecha_salida',
            8=>'hora_salida',
            9=>'contrato',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('prospectos c');
        $this->db->where("tipo",$params["tipo"]);
        $this->db->where('estatus',1);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function getUnidadesProspecto($id){
        $this->db->select('u.*,up.*');
        $this->db->from('unidad_prospecto AS up');
        $this->db->join('unidades AS u','up.unidad = u.id','left');
        $this->db->where('up.estatus',1);
        $this->db->where('id_prospecto',$id);
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }

}