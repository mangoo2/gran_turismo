<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloContratos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_result($params){
        $columns = array( 
            0=>'con.id',
            1=>'con.folio',
            2=>'concat(cli.nombre," ",cli.app," ",cli.apm) as cliente',
            3=>'con.fecha_reg',
            4=>'con.lugar_origen',
            //4=>'cot.hora_salida',
            5=>'cli.telefono',
            6=>'cli.correo',
            7=>'concat(p.nombre," ",p.apellido_p) as vendedor',
            8=>'(select sum(monto) FROM unidad_prospecto WHERE id_contrato=con.id AND id_contrato!=0 AND estatus=1) as tot_unids',
            9=>'liquidado',
            10=>'prioridad',
            11=>'seguimiento',
            12=>'con.contrato',
            13=>'cli.id AS idCliente',
            14=>'con.fecha_contrato',
            15=>'con.id_cotizacion',
            16 =>'con.cotizacion',
            17=>'con.monto_anticipo',
            18=>'porc_desc',
            19=>'(select sum(monto) FROM pagos_contrato WHERE id_contrato=con.id AND estatus=1) as tot_pagos',
            20=>'con.estatus',
            21=>'con.motivo_rechazo',
            22=>'con.rel_gastos',
            23=>'con.bit_viaje',
        );
        $columns2 = array( 
            0=>'con.id',
            1=>'con.folio',
            2=>'cli.nombre',
            //2=>'concat(cli.nombre," ",cli.app," ",cli.apm)',
            3=>'con.fecha_reg',
            4=>'con.lugar_origen',
            //4=>'cot.hora_salida',
            5=>'cli.telefono',
            6=>'cli.correo',
            7=>'p.nombre',
            //7=>'concat(p.nombre," ",p.apellido_p)',
            8=>'(select sum(monto) FROM unidad_prospecto WHERE id_contrato=con.id AND id_contrato!=0 AND estatus=1)',
            9=>'liquidado',
            10=>'prioridad',
            11=>'seguimiento',
            12=>'con.contrato',
            13=>'cli.id',
            14=>'con.fecha_contrato',
            15=>'con.id_cotizacion',
            16 =>'con.cotizacion',
            17=>'con.rel_gastos',
            19=>'con.bit_viaje',
            21=>'cli.app',
            22=>'cli.apm',
            23=>'p.apellido_p',
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('contratos con');
        $this->db->join('prospectos cli', 'con.idCliente = cli.id', 'left');
        $this->db->join('personal p', 'con.personal = p.personalId', 'left');
        //$this->db->where('cli.tipo',1);
        $this->db->where('con.estatus',1);

        if($params["id_cliente"]!="0"){
            $this->db->where("idCliente",$params["id_cliente"]);
        }
        if($params["seguimiento"]!="0"){
            $this->db->where("seguimiento",$params["seguimiento"]);
        }
        if($params["tipo_cliente"]!="0"){
            $this->db->where("tipo_cliente",$params["tipo_cliente"]);
        }
        if($params["prioridad"]!="0"){
            $this->db->where("prioridad",$params["prioridad"]);
        }
        if($params["fechai"]!="" && $params["fechaf"]!=""){
            $this->db->where('con.fecha_contrato BETWEEN '.'"'.$params["fechai"].'"'.' AND '.'"'.$params["fechaf"].'"');
        }
        if($params["liquidado"]!="2"){
            $this->db->where("liquidado",$params["liquidado"]);
        }
        //$this->db->order_by('con.prioridad',"ASC");
        //$this->db->order_by('con.fecha_contrato',"DESC");
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   

        //$this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        foreach ($params['order'] as $order) {
            $columnIndex = $order['column'];
            $direction = $order['dir'];
            $this->db->order_by($columns2[$columnIndex], $direction);
        }

        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }


    public function total_result($params){
        $columns2 = array( 
            0=>'con.id',
            1=>'con.folio',
            2=>'cli.nombre',
            //2=>'concat(cli.nombre," ",cli.app," ",cli.apm)',
            3=>'con.fecha_reg',
            4=>'con.lugar_origen',
            //4=>'cot.hora_salida',
            5=>'cli.telefono',
            6=>'cli.correo',
            7=>'p.nombre',
            //7=>'concat(p.nombre," ",p.apellido_p)',
            8=>'(select sum(monto) FROM unidad_prospecto WHERE id_contrato=con.id AND id_contrato!=0 AND estatus=1)',
            9=>'liquidado',
            10=>'prioridad',
            11=>'seguimiento',
            12=>'con.contrato',
            13=>'cli.id',
            14=>'con.fecha_contrato',
            15=>'con.id_cotizacion',
            16 =>'con.cotizacion',
            17=>'con.rel_gastos',
            19=>'con.bit_viaje',
            21=>'cli.app',
            22=>'cli.apm',
            23=>'p.apellido_p',
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('contratos con');
        $this->db->join('prospectos cli', 'con.idCliente = cli.id', 'left');
        $this->db->join('personal p', 'con.personal = p.personalId', 'left');
        //$this->db->where('cli.tipo',1);
        $this->db->where('con.estatus',1);
        if($params["id_cliente"]!="0"){
            $this->db->where("idCliente",$params["id_cliente"]);
        }
        if($params["seguimiento"]!="0"){
            $this->db->where("seguimiento",$params["seguimiento"]);
        }
        if($params["tipo_cliente"]!="0"){
            $this->db->where("tipo_cliente",$params["tipo_cliente"]);
        }
        if($params["prioridad"]!="0"){
            $this->db->where("prioridad",$params["prioridad"]);
        }
        if($params["fechai"]!="" && $params["fechaf"]!=""){
            $this->db->where('con.fecha_contrato BETWEEN '.'"'.$params["fechai"].'"'.' AND '.'"'.$params["fechaf"].'"');
        }
        if($params["liquidado"]!="2"){
            $this->db->where("liquidado",$params["liquidado"]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function getIdCotizacion($id){
        $this->db->select('id_cotizacion, cotizacion');
        $this->db->from('contratos');
        $this->db->where('estatus',1);
        $this->db->where('id',$id);
        $query=$this->db->get();
        return $query->row();
    }

    public function getUnidadContrato($idContrato,$idUnidad){
        $this->db->select('u.*,up.*');
        $this->db->from('unidad_prospecto AS up');
        $this->db->join('unidades AS u','up.unidad = u.id','left');
        $this->db->where('up.estatus',1);
        $this->db->where('up.id_contrato',$idContrato);
        $this->db->where('up.unidad',$idUnidad);
        $query=$this->db->get();
        return $query->row();
    }

    public function getUnidadesChoferesContrato($id){
        $this->db->select('u.*,up.*, concat(c.nombre," ",c.apellido_p," ",apellido_m) as chofer');
        $this->db->from('unidad_prospecto AS up');
        $this->db->join('unidades AS u','up.unidad = u.id','left');
        $this->db->join('chofer_contrato cc','cc.idContrato=up.id_contrato and cc.idUnidad=up.unidad','left');
        $this->db->join('choferes c','c.choferid=cc.idChofer','left');
        $this->db->where('up.estatus',1);
        $this->db->where('id_contrato',$id);
        $query=$this->db->get();
        return $query;
    }

    public function getUnidadesContrato($idContrato){
        $this->db->select('u.*,up.*');
        $this->db->from('unidad_prospecto AS up');
        $this->db->join('unidades AS u','up.unidad = u.id','left');
        $this->db->where('up.estatus',1);
        $this->db->where('id_contrato',$idContrato);
        $query=$this->db->get();
        return $query;
    }

    public function getUnidadesCotizacion($id){
        $this->db->select('u.*,up.*');
        $this->db->from('unidad_prospecto AS up');
        $this->db->join('unidades AS u','up.unidad = u.id','left');
        $this->db->where('up.estatus',1);
        $this->db->where('up.id_cotizacion',$id);
        //$this->db->where('up.id_contrato',0);
        $query=$this->db->get();
        return $query;
    }

    public function getUnidadesEstadisticas($id){
        $this->db->select('u.*,up.*');
        $this->db->from('unidad_prospecto AS up');
        $this->db->join('unidades AS u','up.unidad = u.id','left');
        $this->db->where('up.estatus',1);
        $this->db->where('up.id_contrato',$id);
        $query=$this->db->get();
        return $query->result();
    }


    public function getUnidadesProspecto($id){
        $this->db->select('u.*,up.*');
        $this->db->from('unidad_prospecto AS up');
        $this->db->join('unidades AS u','up.unidad = u.id','left');
        $this->db->where('up.estatus',1);
        $this->db->where('up.id_prospecto',$id);
        $this->db->where('up.id_contrato',0);
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }


    public function search_cliente($search) {
        $strq = "SELECT id, nombre, app, apm, lugar_origen, fecha_salida, hora_salida, fecha_regreso, hora_regreso, estado
                FROM prospectos
                WHERE estatus = 1 AND tipo = 1 AND (nombre LIKE '%$search%' OR app LIKE '%$search%' OR apm LIKE '%$search%')";
        $query = $this->db->query($strq);
        return $query->result();
    }


    public function getChoferesContrato($id){
        $this->db->select('cc.*,c.*');
        $this->db->from('chofer_contrato AS cc');
        $this->db->join('choferes AS c','cc.idChofer = c.choferid','left');
        $this->db->where('cc.estatus',1);
        $this->db->where('cc.idUnidPros',$id);
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }


    public function getDataContrato(){
        $this->db->select('c.id, c.fecha_contrato, c.fecha_salida, c.hora_salida, c.fecha_regreso, c.hora_regreso, c.lugar_origen, cli.nombre, cli.app, cli.apm');
        $this->db->from('contratos AS c');
        $this->db->join('prospectos AS cli','c.idCliente = cli.id','left');
        $this->db->where('c.estatus',1);
        $this->db->where('c.contrato',1);
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }


    public function getDataContratoUnidad(){
        $this->db->select('c.id, IFNULL(c.folio, "") as folio, c.fecha_contrato, c.fecha_salida, c.hora_salida, c.fecha_regreso, c.hora_regreso, c.lugar_origen, cli.nombre, cli.app, cli.apm, up.id AS id_UnidPros , u.vehiculo, u.color, u.num_eco');
        $this->db->from('contratos AS c');
        $this->db->join('prospectos AS cli','c.idCliente = cli.id','left');
        $this->db->join('unidad_prospecto AS up','c.id = up.id_contrato','left');
        $this->db->join('unidades AS u','up.unidad = u.id','left');
        $this->db->where('c.estatus',1);
        $this->db->where('c.contrato',1);
        $this->db->where('up.estatus',1);
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    public function getChoferesUnidad($idContrato,$idUnidad){
        $this->db->select('cc.idUnidPros, c.nombre, c.apellido_p, c.apellido_m, c.telefono, u.vehiculo, u.marca, u.modelo, u.placas, c.tipo_licencia,c.vigencia_licencia,c.vigencia_examen,c.choferid,u.num_eco,u.tipo');
        $this->db->from('chofer_contrato AS cc');
        $this->db->join('choferes AS c','cc.idChofer = c.choferid','left');
        $this->db->join('unidad_prospecto AS up', 'up.unidad = cc.idUnidad');
        $this->db->join('unidades AS u','cc.idUnidad = u.id','left');
        $this->db->where('cc.estatus',1);
        $this->db->where('cc.idContrato',$idContrato);
        $this->db->where('up.estatus', 1);
        $this->db->where('up.id_contrato',$idContrato);
        $this->db->where('cc.idunidad',$idUnidad);
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    public function getChoferesUnidadDestino($idContrato,$idUnidad){
        $this->db->select('cc.idUnidPros, c.nombre, c.apellido_p, c.apellido_m, c.telefono, u.vehiculo, u.marca, u.modelo, u.placas, c.tipo_licencia,c.vigencia_licencia,c.vigencia_examen,c.choferid,u.num_eco,u.tipo');
        $this->db->from('chofer_contrato AS cc');
        $this->db->join('choferes AS c','cc.idChofer = c.choferid','left');
        $this->db->join('unidad_prospecto AS up', 'up.unidad = cc.idUnidad');
        $this->db->join('unidades AS u','cc.idUnidad = u.id','left');
        $this->db->where('cc.estatus',1);
        $this->db->where('cc.idContrato',$idContrato);
        $this->db->where('up.estatus', 1);
        $this->db->where('up.id_contrato',$idContrato);
        $this->db->where('cc.idUnidPros',$idUnidad);
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    public function getChoferesUnidades($id){
        $this->db->select('cc.idUnidPros, c.nombre, c.apellido_p, c.apellido_m, c.telefono, u.vehiculo, u.marca, u.modelo, u.placas, c.tipo_licencia,c.vigencia_licencia,c.vigencia_examen,c.choferid,u.num_eco,u.tipo');
        $this->db->from('chofer_contrato AS cc');
        $this->db->join('choferes AS c','cc.idChofer = c.choferid','left');
        $this->db->join('unidad_prospecto AS up', 'up.unidad = cc.idUnidad');
        $this->db->join('unidades AS u','cc.idUnidad = u.id','left');
        $this->db->where('cc.estatus',1);
        $this->db->where('cc.idContrato',$id);
        $this->db->where('up.estatus', 1);
        $this->db->where('up.id_contrato',$id);
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    
    public function getPasajerosContrato($id){
        $this->db->select('pc.*');
        $this->db->from('pasajeros_contrato AS pc');
        $this->db->where('pc.idUnidPros',$id);
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }

    public function getDestinosContrato($id){
        $this->db->select('dp.lugar, dp.fecha, dp.hora, dp.lugar_hospeda, dp.fecha_regreso, dp.hora_regreso');
        $this->db->from('destino_prospecto AS dp');
        $this->db->where('dp.id_UnidPros',$id);
        $this->db->where('dp.estatus',1);
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }

    function getContratosLista($params){
        $columns = array( 
            0=>'con.id',
            1=>'concat(cli.nombre," ",cli.app," ",cli.apm) as cliente',
            2=>'con.fecha_reg',
            3=>'con.lugar_origen',
            //4=>'cot.hora_salida',
            4=>'cli.telefono',
            5=>'cli.correo',
            6=>'concat(p.nombre," ",p.apellido_p) as vendedor',
            7=>'(select sum(monto) FROM unidad_prospecto WHERE id_contrato=con.id AND id_contrato!=0 AND estatus=1) as tot_unids',
            8=>'liquidado',
            9=>'prioridad',
            10=>'seguimiento',
            11=>'con.contrato',
            12=>'cli.id AS idCliente',
            13=>'con.contrato',
            14=>'con.fecha_contrato',
            15=>'con.id_cotizacion',
            16=>'con.cotizacion',
            17=>'con.monto_anticipo',
            18=>'porc_desc',
            19=>'cli.tipo_cliente',
            20=>'cli.tipo',
            21=>'con.observaciones',
            22=>'con.fecha_salida',
            23=>'con.fecha_regreso',
            24=>'con.empresa',
            25=>"(select sum(monto) FROM pagos_contrato WHERE id_contrato=con.id AND estatus=1) as tot_pagos",
            26=>'con.folio',
            27=>'cli.telefono_2'
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('contratos con');
        $this->db->join('prospectos cli', 'con.idCliente = cli.id', 'left');
        $this->db->join('personal p', 'p.personalId=con.personal');
        //$this->db->where('cli.tipo',1);
        $this->db->where('con.estatus',1);

        if($params["id_cliente"]!="0"){
            $this->db->where("idCliente",$params["id_cliente"]);
        }
        if($params["seguimiento"]!="0"){
            $this->db->where("seguimiento",$params["seguimiento"]);
        }
        if($params["tipo_cliente"]!="0"){
            $this->db->where("tipo_cliente",$params["tipo_cliente"]);
        }
        if($params["prioridad"]!="0"){
            $this->db->where("prioridad",$params["prioridad"]);
        }
        if($params["fechai"]!="0" && $params["fechaf"]!="0"){
            $this->db->where('con.fecha_contrato BETWEEN '.'"'.$params["fechai"].'"'.' AND '.'"'.$params["fechaf"].'"');
        }
        if($params["liquidado"]!="2"){
            $this->db->where("liquidado",$params["liquidado"]);
        }
        $this->db->order_by('con.prioridad',"ASC");
        $this->db->order_by('con.fecha_contrato',"DESC");

        $query=$this->db->get();
        return $query->result();
    }

    public function getDatosContrato($id){
        $this->db->select('c.id, c.folio, c.fecha_contrato, c.fecha_salida, c.hora_salida, c.fecha_regreso, c.hora_regreso, c.lugar_origen, c.monto_anticipo,c.porc_desc,c.prioridad,c.seguimiento,c.fecha_reg,
            cli.telefono,cli.correo, concat(cli.nombre," ",cli.app," ",cli.apm) as cliente, cli.tipo, cli.tipo_cliente,
            concat(p.nombre," ",p.apellido_p) as vendedor, c.observaciones,
            (select sum(monto) FROM unidad_prospecto WHERE id_contrato=c.id AND id_contrato!=0 AND estatus=1) as tot_unids');
        $this->db->from('contratos c');
        $this->db->join('prospectos AS cli','c.idCliente = cli.id','left');
        $this->db->join('personal p', 'p.personalId=c.personal');
        $this->db->where('c.id',$id);
        $query=$this->db->get();
        return $query->row();
    }

    public function getPagosContrato($id){
        $this->db->select('pc.*, c.cuenta');
        $this->db->from('pagos_contrato pc');
        $this->db->join('cuentas c','c.id=pc.id_cuenta','left');
        $this->db->where('pc.id_contrato',$id);
        $this->db->where('pc.estatus',1);
        $query=$this->db->get();
        return $query->result();
    }

    public function getChoferesEstadisticas($id){
        $this->db->select('cc.*,c.*');
        $this->db->from('chofer_contrato AS cc');
        $this->db->join('choferes AS c','cc.idChofer = c.choferid','left');
        $this->db->where('cc.estatus',1);
        $this->db->where('cc.idContrato',$id);
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }

    public function getChoferesUnidadEstadisticas($idC, $idU){
        $this->db->select('cc.*,c.*');
        $this->db->from('chofer_contrato AS cc');
        $this->db->join('choferes AS c','cc.idChofer = c.choferid','left');
        $this->db->where('cc.estatus',1);
        $this->db->where('cc.idContrato',$idC);
        $this->db->where('cc.idUnidad',$idU);
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }


    function get_result_chofer($params){
        $columns = array( 
            0=>'con.id',
            1=>'concat(cli.nombre," ",cli.app," ",cli.apm) as cliente',
            2=>'con.fecha_reg',
            3=>'con.lugar_origen',
            //4=>'cot.hora_salida',
            4=>'cli.telefono',
            5=>'cli.correo',
            6=>'concat(p.nombre," ",p.apellido_p) as vendedor',
            7=>'(select sum(monto) FROM unidad_prospecto WHERE id_contrato=con.id AND id_contrato!=0 AND estatus=1) as tot_unids',
            8=>'liquidado',
            9=>'prioridad',
            10=>'seguimiento',
            11=>'con.contrato',
            12=>'cli.id AS idCliente',
            13=>'con.contrato',
            14=>'con.fecha_contrato',
            15=>'con.id_cotizacion',
            16 =>'con.cotizacion',
            17=>'con.monto_anticipo',
            18=>'porc_desc',
            19=>'(select sum(monto) FROM pagos_contrato WHERE id_contrato=con.id AND estatus=1) as tot_pagos',
            20=>'con.estatus',
            21=>'con.motivo_rechazo',
            22=>'con.rel_gastos',
            23=>'con.bit_viaje',
            24=>'con.folio',
        );
        $columns2 = array( 
            0=>'con.id',
            1=>'concat(cli.nombre," ",cli.app," ",cli.apm)',
            2=>'con.fecha_reg',
            3=>'con.lugar_origen',
            //4=>'cot.hora_salida',
            4=>'cli.telefono',
            5=>'cli.correo',
            6=>'concat(p.nombre," ",p.apellido_p)',
            7=>'(select sum(monto) FROM unidad_prospecto WHERE id_contrato=con.id AND id_contrato!=0 AND estatus=1)',
            8=>'prioridad',
            9=>'seguimiento',
            10=>'con.contrato',
            11=>'cli.id',
            12=>'con.contrato',
            13=>'con.fecha_contrato',
            14=>'con.id_cotizacion',
            15 =>'con.cotizacion',
            16=>'con.rel_gastos',
            17=>'con.bit_viaje',
            18=>'con.folio',
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('contratos con');
        $this->db->join('prospectos cli', 'con.idCliente = cli.id', 'left');
        $this->db->join('personal p', 'p.personalId=con.personal');
        $this->db->join('chofer_contrato chco', 'con.id = chco.idContrato', 'left');
        //$this->db->where('cli.tipo',1);
        $this->db->where('con.estatus',1);
        $this->db->where('chco.estatus',1);
        $this->db->where('chco.idChofer',$params["choferID"]);

        if($params["id_cliente"]!="0"){
            $this->db->where("idCliente",$params["id_cliente"]);
        }
        if($params["seguimiento"]!="0"){
            $this->db->where("seguimiento",$params["seguimiento"]);
        }
        if($params["tipo_cliente"]!="0"){
            $this->db->where("tipo_cliente",$params["tipo_cliente"]);
        }
        if($params["prioridad"]!="0"){
            $this->db->where("prioridad",$params["prioridad"]);
        }
        if($params["fechai"]!="" && $params["fechaf"]!=""){
            $this->db->where('con.fecha_contrato BETWEEN '.'"'.$params["fechai"].'"'.' AND '.'"'.$params["fechaf"].'"');
        }
        if($params["liquidado"]!="2"){
            $this->db->where("liquidado",$params["liquidado"]);
        }
        $this->db->order_by('con.prioridad',"asc");
        $this->db->order_by('con.fecha_contrato',"DESC");
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }


    public function total_result_chofer($params){
        $columns2 = array( 
            0=>'con.id',
            1=>'concat(cli.nombre," ",cli.app," ",cli.apm)',
            2=>'con.fecha_reg',
            3=>'con.lugar_origen',
            //4=>'cot.hora_salida',
            4=>'cli.telefono',
            5=>'cli.correo',
            6=>'concat(p.nombre," ",p.apellido_p)',
            7=>'(select sum(monto) FROM unidad_prospecto WHERE id_contrato=con.id AND id_contrato!=0 AND estatus=1)',
            8=>'prioridad',
            9=>'seguimiento',
            10=>'con.contrato',
            11=>'cli.id',
            12=>'con.contrato',
            13=>'con.fecha_contrato',
            14=>'con.id_cotizacion',
            15 =>'con.cotizacion',
            16=>'con.rel_gastos',
            17=>'con.bit_viaje',
            18=>'con.folio',    
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('contratos con');
        $this->db->join('prospectos cli', 'con.idCliente = cli.id', 'left');
        $this->db->join('personal p', 'p.personalId=con.personal');
        $this->db->join('chofer_contrato chco', 'con.id = chco.idContrato', 'left');
        //$this->db->where('cli.tipo',1);
        $this->db->where('con.estatus',1);
        $this->db->where('chco.estatus',1);
        $this->db->where('chco.idChofer',$params["choferID"]);

        if($params["id_cliente"]!="0"){
            $this->db->where("idCliente",$params["id_cliente"]);
        }
        if($params["seguimiento"]!="0"){
            $this->db->where("seguimiento",$params["seguimiento"]);
        }
        if($params["tipo_cliente"]!="0"){
            $this->db->where("tipo_cliente",$params["tipo_cliente"]);
        }
        if($params["prioridad"]!="0"){
            $this->db->where("prioridad",$params["prioridad"]);
        }
        if($params["fechai"]!="" && $params["fechaf"]!=""){
            $this->db->where('con.fecha_contrato BETWEEN '.'"'.$params["fechai"].'"'.' AND '.'"'.$params["fechaf"].'"');
        }
        if($params["liquidado"]!="2"){
            $this->db->where("liquidado",$params["liquidado"]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function getItinerarioContrato($id){
        $this->db->select('dp.*');
        $this->db->from('destino_prospecto AS dp');
        $this->db->join('unidad_prospecto AS up', 'up.id = dp.id_unidPros','left');
        $this->db->where('dp.estatus',1);
        $this->db->where('dp.id_contrato',$id);
        $this->db->where('up.estatus', 1);
        $this->db->where('up.id_contrato',$id);
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    public function getItinerarioContratoUnidad($idContrato, $idUnidad){
        $this->db->select('dp.*');
        $this->db->from('destino_prospecto AS dp');
        $this->db->join('unidad_prospecto AS up', 'up.id = dp.id_unidPros','left');
        $this->db->where('dp.estatus',1);
        $this->db->where('dp.id_contrato',$idContrato);
        $this->db->where('up.estatus', 1);
        $this->db->where('up.id_contrato',$idContrato);
        $this->db->where('up.unidad',$idUnidad);
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    public function getEdoCuenta($params){
        $this->db->select('c.id, c.folio, c.fecha_contrato, c.fecha_reg, concat(cli.nombre," ",cli.app," ",cli.apm) as cliente,
                monto_anticipo as tot_antic, porc_desc as tot_desc,
                IFNULL((select sum(monto) FROM unidad_prospecto WHERE id_contrato=c.id AND estatus=1),0) as tot_unids,
                IFNULL((select sum(monto) FROM pagos_contrato WHERE id_contrato=c.id AND estatus=1),0) as tot_pays');
        $this->db->from('contratos c');
        $this->db->join('prospectos cli', 'c.idCliente=cli.id');
        $this->db->where('c.seguimiento!=',6);
        if($params["id_cliente"]!="0"){
            $this->db->where('c.idCliente',$params["id_cliente"]);
        }
        if($params["fechai"]!="" && $params["fechaf"]!="" && $params["fechai"]!="0" && $params["fechaf"]!="0"){
            $this->db->where('c.fecha_contrato BETWEEN '.'"'.$params["fechai"].'"'.' AND '.'"'.$params["fechaf"].'"');
        }
        $query=$this->db->get();
        return $query->result();
    }

    function getContratosReporte($params){
        $columns = array( 
            0=>'con.id',
            1=>'concat(cli.nombre," ",cli.app," ",cli.apm) as cliente',
            2=>'con.fecha_reg',
            3=>'con.lugar_origen',
            4=>'cli.telefono',
            5=>'cli.correo',
            6=>'concat(p.nombre," ",p.apellido_p) as vendedor',
            7=>'(select sum(monto) FROM unidad_prospecto WHERE id_contrato=con.id AND id_contrato!=0 AND estatus=1) as tot_unids',
            8=>'liquidado',
            9=>'prioridad',
            10=>'seguimiento',
            11=>'con.contrato',
            12=>'cli.id AS idCliente',
            13=>'con.contrato',
            14=>'con.fecha_contrato',
            15=>'con.id_cotizacion',
            16=>'con.cotizacion',
            17=>'con.monto_anticipo',
            18=>'porc_desc',
            19=>'cli.tipo_cliente',
            20=>'cli.tipo',
            21=>'con.observaciones',
            22=>'con.fecha_contrato',
            23=>'con.empresa',
            24=>'cli.rfc',
            25=>'cli.cod_postal',
            26=>'cli.calle',
            27=>'cli.ciudad',
            28=>'e.Nombre as estado',
            29=>"(select sum(monto) FROM pagos_contrato WHERE id_contrato=con.id AND estatus=1) as tot_pagos",
            30=>'cli.telefono_2',
            31=>'con.fecha_salida',
            32=>'con.hora_salida',
            33=>'con.fecha_regreso',
            34=>'con.hora_regreso',
            35=>'con.folio',
            //36=>"GROUP_CONCAT(cg.importe SEPARATOR '<br>') as gastos,"
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('contratos con');
        $this->db->join('prospectos cli', 'con.idCliente = cli.id', 'left');
        $this->db->join('personal p', 'p.personalId=con.personal');
        $this->db->join('estados e', 'e.EstadoId=cli.estado',"left");
        //$this->db->join('contrato_gastos cg', 'cg.id_contrato=con.id',"left");
        
        //$this->db->where('cli.tipo',1);
        $this->db->where('con.estatus',1);

        if($params["id_cliente"]!="0"){
            $this->db->where("idCliente",$params["id_cliente"]);
        }
        if($params["fechai"]!="0" && $params["fechaf"]!="0"){
            $this->db->where('con.fecha_contrato BETWEEN '.'"'.$params["fechai"].'"'.' AND '.'"'.$params["fechaf"].'"');
        }
        
        $this->db->order_by('con.prioridad',"asc");
        $this->db->order_by('con.fecha_contrato',"DESC");

        $query=$this->db->get();
        return $query->result();
    }


    public function insert_token($id,$pass)
    {
        $token = password_hash($pass, PASSWORD_BCRYPT);
        //$data = array('token'=>$token,'encuesta'=>1);
        $data = array('token'=>$token);

        $this->db->set($data);
        $this->db->where('id', $id);
        $this->db->update('contratos');
        return $id;
    }

    //check_token($nombre, $idContrato ,$tokenDeco)
    public function check_token($nombre, $idContrato ,$token)
    {
        $pass = $nombre.'mangoo'.$idContrato;
        $verificar = password_verify($pass, $token);

        if ($verificar) {
            //echo "¡La contraseña es correcta!";
            return 1;
        } else {
            //echo "¡La contraseña es incorrecta!";
            return 0;
        }

    }


    public function dataContratoCliente($id){
        $this->db->select('cont.folio, cont.id as idContrato, CONCAT(cli.nombre," ",cli.app," ",cli.apm)as cliente');
        $this->db->from('contratos cont');
        $this->db->join('prospectos cli','cont.idCliente = cli.id','left');
        $this->db->where('cont.estatus',1);
        $this->db->where('cont.id',$id);
        $query=$this->db->get();
        return $query->row();
    }


    public function existRG($id){
        $this->db->where('id_contrato', $id);
        $this->db->where('id_unidad', 0);
        $this->db->where('estatus', 1);
        $query = $this->db->count_all_results('contrato_gastos');
        return $query;
    }

    public function existBV($id){
        $this->db->where('id_contrato', $id);
        $this->db->where('id_unidad', 0);
        $this->db->where('estatus', 1);
        $query = $this->db->count_all_results('bitacora_revisiones');
        return $query;
    }

    public function setExistRG($id){
        $data = array('estatus' => 0);

        $this->db->set($data);
        $this->db->where('id_contrato', $id);
        $this->db->where('id_unidad', 0);

        $this->db->update('contrato_gastos');
        return $id;
    }

    public function setExistBV($id){
        $data = array('estatus' => 0);
    
        // Iniciar transacción
        $this->db->trans_start();
    
        // Actualizar en la tabla 'bitacora_revisiones'
        $this->db->set($data);
        $this->db->where('id_contrato', $id);
        $this->db->where('id_unidad', 0);
        $this->db->update('bitacora_revisiones');
    
        // Actualizar en la tabla 'bitacora_dias'
        $this->db->set($data);
        $this->db->where('id_contrato', $id);
        $this->db->where('id_unidad', 0);
        $this->db->update('bitacora_dias');
    
        // Actualizar en la tabla 'bitacora_horas'
        $this->db->set($data);
        $this->db->where('id_contrato', $id);
        $this->db->where('id_unidad', 0);
        $this->db->update('bitacora_horas');
    
        // Completar transacción
        $this->db->trans_complete();
    
        // Verificar si la transacción fue exitosa
        if ($this->db->trans_status() === FALSE) {
            // Si hay un error, hacer rollback
            return false;
        } else {
            // Si todo fue bien, devolver el id
            return $id;
        }
    }

    public function setUnidadRG($idC, $idU){
        $data = array('id_unidad' => $idU);

        $this->db->set($data);
        $this->db->where('id_contrato', $idC);
        $this->db->where('id_unidad', 0);

        $this->db->update('contrato_gastos');
        return $idC;
    }


    public function setUnidadBV($idC, $idU){
        $data = array('id_unidad' => $idU);
    
        // Iniciar transacción
        $this->db->trans_start();
    
        // Actualizar en la tabla 'bitacora_revisiones'
        $this->db->set($data);
        $this->db->where('id_contrato', $idC);
        $this->db->where('id_unidad', 0);
        $this->db->update('bitacora_revisiones');
    
        // Actualizar en la tabla 'bitacora_dias'
        $this->db->set($data);
        $this->db->where('id_contrato', $idC);
        $this->db->where('id_unidad', 0);
        $this->db->update('bitacora_dias');
    
        // Actualizar en la tabla 'bitacora_horas'
        $this->db->set($data);
        $this->db->where('id_contrato', $idC);
        $this->db->where('id_unidad', 0);
        $this->db->update('bitacora_horas');
    
        // Completar transacción
        $this->db->trans_complete();
    
        // Verificar si la transacción fue exitosa
        if ($this->db->trans_status() === FALSE) {
            // Si hay un error, hacer rollback
            return false;
        } else {
            // Si todo fue bien, devolver el id
            return $idC;
        }
    }



    public function getGastosUnidadesContrato($idC){
        $this->db->select('cg.*,u.num_eco,u.vehiculo');
        $this->db->from('contrato_gastos AS cg');
        $this->db->join('unidades AS u','cg.id_unidad = u.id','left');
        $this->db->where('cg.id_contrato',$idC);
        $this->db->where('cg.tipo >',0);
        $this->db->where('cg.estatus',1);
        $this->db->order_by('id_unidad', 'ASC');
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }


    public function getKMSUnidadesContrato($idC){
        $this->db->select('br.*,u.num_eco,u.vehiculo');
        $this->db->from('bitacora_revisiones AS br');
        $this->db->join('unidades AS u','br.id_unidad = u.id','left');
        $this->db->where('br.id_contrato',$idC);
        $this->db->where('br.estatus',1);
        $this->db->order_by('id_unidad', 'ASC');
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }

    public function getMontoTotalContrato($id){
        $this->db->select('IFNULL(SUM(monto * cantidad), 0) AS total_montos');
        $this->db->from('unidad_prospecto');
        $this->db->where('estatus', 1);
        $this->db->where('id_contrato', $id);
        $query = $this->db->get();
        //$this->db->close();
        return $query->row()->total_montos;
    }

    public function getPagoTotalContrato($id){
        $this->db->select('IFNULL(SUM(monto), 0) AS total_pagos');
        $this->db->from('pagos_contrato');
        $this->db->where('estatus', 1);
        $this->db->where('id_contrato', $id);
        $query = $this->db->get();
        //$this->db->close();
        return $query->row()->total_pagos;
    }

    public function getDatesItinerario($idC) {
        $this->db->select('id, fecha, hora, fecha_regreso, hora_regreso');
        $this->db->from('destino_prospecto');
        $this->db->where('estatus', 1);
        $this->db->where('id_contrato', $idC);
        $this->db->order_by('fecha_regreso', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }

}