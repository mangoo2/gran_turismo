<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloInicio extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    //notificaciones Vigencia Licencia Chofer (1)
    function get_result_LicChofer($params){
        $columns = array( 
            0=>'choferid',
            1=>'nombre',
            2=>'apellido_p',
            3=>'apellido_m',
            4=>'vigencia_licencia',
            5=>'1 AS tipo'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from("choferes c");
        $this->db->where("c.estatus",1);
        $this->db->where("c.vigencia_licencia != '0000-00-00'");
        $this->db->where("c.vigencia_licencia < DATE_ADD(CURDATE(), INTERVAL 3 MONTH) + INTERVAL 1 DAY");

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_result_LicChofer($params){
        $columns = array( 
            0=>'choferid',
            1=>'nombre',
            2=>'apellido_p',
            3=>'apellido_m',
            4=>'vigencia_licencia',
            5=>'1 AS tipo'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from("choferes c");
        $this->db->where("c.estatus",1);
        $this->db->where("c.vigencia_licencia != '0000-00-00'");
        $this->db->where("c.vigencia_licencia < DATE_ADD(CURDATE(), INTERVAL 3 MONTH) + INTERVAL 1 DAY");

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }


    //notificaciones Vigencia Examen Medico Chofer (2)
    function get_result_ExaChofer($params){
        $columns = array( 
            0=>'choferid',
            1=>'nombre',
            2=>'apellido_p',
            3=>'apellido_m',
            4=>'vigencia_examen',
            5=>'2 AS tipo'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from("choferes c");
        $this->db->where("c.estatus",1);
        $this->db->where("c.vigencia_examen != '0000-00-00'");
        $this->db->where("c.vigencia_examen < DATE_ADD(CURDATE(), INTERVAL 3 MONTH) + INTERVAL 1 DAY");

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_result_ExaChofer($params){
        $columns = array( 
            0=>'choferid',
            1=>'nombre',
            2=>'apellido_p',
            3=>'apellido_m',
            4=>'vigencia_examen',
            5=>'2 AS tipo'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from("choferes c");
        $this->db->where("c.estatus",1);
        $this->db->where("c.vigencia_examen != '0000-00-00'");
        $this->db->where("c.vigencia_examen < DATE_ADD(CURDATE(), INTERVAL 3 MONTH) + INTERVAL 1 DAY");

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }


    //notificaciones Vigencia Poliza Unidad (3)
    function get_result_PoliUnidad($params){
        $columns = array( 
            0=>'id',
            1=>'vehiculo',
            2=>'vencimiento_poliza',
            3=>'3 AS tipo'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from("unidades u");
        $this->db->where("u.estatus", 1);
        $this->db->where("u.vencimiento_poliza != '0000-00-00'");
        $this->db->where("u.vencimiento_poliza < DATE_ADD(CURDATE(), INTERVAL 3 MONTH) + INTERVAL 1 DAY");

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_result_PoliUnidad($params){
        $columns = array( 
            0=>'id',
            1=>'vehiculo',
            2=>'vencimiento_poliza',
            3=>'3 AS tipo'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from("unidades u");
        $this->db->where("u.estatus", 1);
        $this->db->where("u.vencimiento_poliza != '0000-00-00'");
        $this->db->where("u.vencimiento_poliza < DATE_ADD(CURDATE(), INTERVAL 3 MONTH) + INTERVAL 1 DAY");

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }


    //notificaciones Verificación de unidad cercana (4)
    function get_result_VerifUnidad($params){
        $columns = array( 
            0=>'id',
            1=>'vehiculo',
            2=>'prox_ver',
            3=>'4 AS tipo'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from("unidades u");
        $this->db->where("u.estatus", 1);
        $this->db->where("u.prox_ver != '0000-00-00'");
        $this->db->where("u.prox_ver < DATE_ADD(CURDATE(), INTERVAL 3 MONTH) + INTERVAL 1 DAY");

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_result_VerifUnidad($params){
        $columns = array( 
            0=>'id',
            1=>'vehiculo',
            2=>'prox_ver',
            3=>'4 AS tipo'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from("unidades u");
        $this->db->where("u.estatus", 1);
        $this->db->where("u.prox_ver != '0000-00-00'");
        $this->db->where("u.prox_ver < DATE_ADD(CURDATE(), INTERVAL 3 MONTH) + INTERVAL 1 DAY");

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

}