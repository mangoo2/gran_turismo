<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloModGastos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_result($params){
        $columns = array( 
            0=>'mg.id',
            1=>'mg.concepto',
            2=>'mg.monto',
            3=>'mg.fecha',
            4=>'mg.responsable',
            5=>'u.Usuario',
            6=>'mg.folio',
            7=>'mg.descripcion'
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('modulo_gastos mg');
        $this->db->join('usuarios u', 'mg.usuario = u.UsuarioID', 'left');
        $this->db->where('mg.estatus',1);

        if($params["fechaIni"] != "" && $params["fechaFin"] != ""){
            $this->db->where('mg.fecha BETWEEN '.'"'.$params["fechaIni"].'"'.' AND '.'"'.$params["fechaFin"].'"');
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }


    public function total_result($params){
        $columns = array( 
            0=>'mg.id',
            1=>'mg.concepto',
            2=>'mg.monto',
            3=>'mg.fecha',
            4=>'mg.responsable',
            5=>'u.Usuario',
            6=>'mg.folio',
            7=>'mg.descripcion'
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('modulo_gastos mg');
        $this->db->join('usuarios u', 'mg.usuario = u.UsuarioID', 'left');
        $this->db->where('mg.estatus',1);

        if($params["fechaIni"] != "" && $params["fechaFin"] != ""){
            $this->db->where('mg.fecha BETWEEN '.'"'.$params["fechaIni"].'"'.' AND '.'"'.$params["fechaFin"].'"');
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
}
