<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCotizaciones extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_result($params){
        $columns = array( 
            0=>'cot.id',
            1=>'concat(cli.nombre," ",cli.app," ",cli.apm) as cliente',
            2=>'cot.fecha_reg',
            3=>'cot.lugar_origen',
            //4=>'cot.hora_salida',
            4=>'cli.telefono',
            5=>'cli.correo',
            6=>'concat(p.nombre," ",p.apellido_p) as vendedor',
            7=>'prioridad',
            8=>'seguimiento',
            9=>'cot.contrato',
            10=>'cli.id AS idCliente',  
            11=>'cot.estatus',
            12=>'cot.motivo_rechazo'          
        );
        $columns2 = array( 
            0=>'cot.id',
            1=>'concat(cli.nombre," ",cli.app," ",cli.apm)',
            2=>'cot.fecha_reg',
            3=>'cot.lugar_origen',
            //4=>'cot.hora_salida',
            4=>'cli.telefono',
            5=>'cli.correo',
            6=>'concat(p.nombre," ",p.apellido_p)',
            7=>'prioridad',
            8=>'seguimiento',
            9=>'cot.contrato',
            10=>'cli.id',            
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('cotizaciones cot');
        $this->db->join('prospectos cli', 'cot.idCliente = cli.id', 'left');
        $this->db->join('personal p', 'p.personalId=cot.personal');
        //$this->db->where('cli.tipo',1);
        $this->db->where('cot.es_contrato',0);
        $this->db->where('cot.estatus',$params["activos"]);
        if($params["id_cliente"]!="0"){
            $this->db->where("idCliente",$params["id_cliente"]);
        }
        if($params["seguimiento"]!="0"){
            $this->db->where("seguimiento",$params["seguimiento"]);
        }
        if($params["tipo_cliente"]!="0"){
            $this->db->where("tipo_cliente",$params["tipo_cliente"]);
        }
        if($params["prioridad"]!="0"){
            $this->db->where("prioridad",$params["prioridad"]);
        }
        if($params["fechai"]!="" && $params["fechaf"]!=""){
            $this->db->where('cot.fecha_salida BETWEEN '.'"'.$params["fechai"].'"'.' AND '.'"'.$params["fechaf"].'"');
        }
        //$this->db->order_by("cot.fecha_salida","DESC");
        //$this->db->order_by("cot.prioridad","ASC");

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_result($params){
        $columns = array( 
            0=>'cot.id',
            1=>'concat(cli.nombre," ",cli.app," ",cli.apm) as cliente',
            2=>'cot.fecha_reg',
            3=>'cot.lugar_origen',
            //4=>'cot.hora_salida',
            4=>'cli.telefono',
            5=>'cli.correo',
            6=>'concat(p.nombre," ",p.apellido_p) as vendedor',
            7=>'prioridad',
            8=>'seguimiento',
            9=>'cot.contrato',
            10=>'cli.id AS idCliente', 
            11=>'cot.estatus'           
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('cotizaciones cot');
        $this->db->join('prospectos cli', 'cot.idCliente = cli.id', 'left');
        //$this->db->where('cli.tipo',1);
        $this->db->where('cot.es_contrato',0);
        $this->db->where('cot.estatus',$params["activos"]);
        if($params["id_cliente"]!="0"){
            $this->db->where("idCliente",$params["id_cliente"]);
        }
        if($params["seguimiento"]!="0"){
            $this->db->where("seguimiento",$params["seguimiento"]);
        }
        if($params["tipo_cliente"]!="0"){
            $this->db->where("tipo_cliente",$params["tipo_cliente"]);
        }
        if($params["prioridad"]!="0"){
            $this->db->where("prioridad",$params["prioridad"]);
        }
        if($params["fechai"]!="" && $params["fechaf"]!=""){
            $this->db->where('cot.fecha_salida BETWEEN '.'"'.$params["fechai"].'"'.' AND '.'"'.$params["fechaf"].'"');
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function getUnidadesCotizacion($id){
        $this->db->select('u.*,up.*');
        $this->db->from('unidad_prospecto AS up');
        $this->db->join('unidades AS u','up.unidad = u.id','left');
        $this->db->where('up.estatus',1);
        $this->db->where('up.id_cotizacion',$id);
        //$this->db->where('up.id_contrato',0);
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }

    public function getUnidadesProspecto($id){
        $this->db->select('u.*,up.*');
        $this->db->from('unidad_prospecto AS up');
        $this->db->join('unidades AS u','up.unidad = u.id','left');
        $this->db->where('up.estatus',1);
        $this->db->where('id_prospecto',$id);
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }

    public function search_cliente($search) {
        $strq = "SELECT id, nombre, app, apm, lugar_origen, estado, empresa, fecha_salida, hora_salida, fecha_regreso, hora_regreso, telefono, correo, medio_conoce, 'logo_color.png' as foto
                FROM prospectos
                WHERE estatus = 1 AND (nombre LIKE '%$search%' OR app LIKE '%$search%' OR apm LIKE '%$search%')";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function getListaCotiza($params){
        $this->db->select("cot.*, concat(cli.nombre,' ',cli.app,' ',cli.apm) as cliente,cli.telefono,cli.telefono_2,cli.correo,cli.tipo_cliente,cli.tipo,
            concat(p.nombre,' ',p.apellido_p) as vendedor,
            (select sum(round(monto,2)) from unidad_prospecto WHERE id_cotizacion=cot.id AND id_cotizacion!=0 AND id_contrato=0 AND estatus= 1) as tot_monto");
        $this->db->from('cotizaciones cot');
        $this->db->join('prospectos cli', 'cot.idCliente = cli.id', 'left');
        $this->db->join('personal p', 'p.personalId=cot.personal');
        //$this->db->where('cli.tipo',1);
        $this->db->where('cot.estatus',1);
        $this->db->where('cot.es_contrato',0);

        if($params["id_cliente"]!="0"){
            $this->db->where("idCliente",$params["id_cliente"]);
        }
        if($params["seguimiento"]!="0"){
            $this->db->where("seguimiento",$params["seguimiento"]);
        }
        if($params["tipo_cliente"]!="0"){
            $this->db->where("tipo_cliente",$params["tipo_cliente"]);
        }
        if($params["prioridad"]!="0"){
            $this->db->where("prioridad",$params["prioridad"]);
        }
        if($params["fechai"]!="0" && $params["fechaf"]!="0"){
            $this->db->where('cot.fecha_salida BETWEEN '.'"'.$params["fechai"].'"'.' AND '.'"'.$params["fechaf"].'"');
        }
        //$this->db->order_by("cot.fecha_salida","DESC");
        $this->db->order_by("cot.prioridad","ASC");

        $query=$this->db->get();
        return $query->result();
    }

    public function getListaCotizaExpira($date){
        $this->db->select('c.*, TIMESTAMPDIFF(MINUTE, fecha_reg, now()) as dif');
        $this->db->from('cotizaciones c');
        $this->db->where('estatus',1);
        $this->db->where('seguimiento',1);
        $this->db->where('timestampdiff(MINUTE, fecha_reg, now()) >=180');
        $this->db->where('timestampdiff(MINUTE, fecha_reg, now()) <=1440');
        $query=$this->db->get();
        return $query->result();
    }

    public function getDataCliente($id){
        $this->db->select('p.nombre,p.app,p.apm');
        
        $this->db->from('cotizaciones c');
        $this->db->join('prospectos p', 'c.idCliente = p.id');

        $this->db->where('c.id', $id);
        $this->db->where('c.estatus',1);
        $query=$this->db->get();
        return $query->row();
    }

    public function getDataEstimados($id){
        $this->db->select('ce.*, "" AS cliente');
        $this->db->from('cotizacion_estimados ce');
        $this->db->join('cotizaciones c', 'ce.id_cotizacion = c.id', 'left');
        $this->db->where('ce.id_cotizacion', $id);
        $this->db->where('ce.estatus',1);
        $this->db->where('c.estatus',1);
        $query=$this->db->get();
        return $query;
    }

    function getDataCotiza($idCot){//(select sum(round(monto,2)) from unidad_prospecto WHERE id_cotizacion=cot.id AND id_cotizacion!=0 AND id_contrato=0 AND estatus= 1) as tot_monto,
        $this->db->select("cot.*, concat(cli.nombre,' ',cli.app,' ',cli.apm) as cliente,cli.telefono,cli.correo,cli.tipo_cliente,cli.tipo,cli.rfc,cli.calle,cli.cod_postal,cli.ciudad,
            concat(p.nombre,' ',p.apellido_p) as vendedor,
            (select sum(monto) from unidad_prospecto WHERE id_cotizacion=cot.id AND id_cotizacion!=0 AND id_contrato=0 AND estatus= 1) as tot_unids");
        $this->db->from('cotizaciones cot');
        $this->db->join('prospectos cli', 'cot.idCliente = cli.id', 'left');
        $this->db->join('personal p', 'p.personalId=cot.personal');
        $this->db->where('cot.id',$idCot);
        $this->db->where('cot.estatus',1);
        $this->db->order_by("cot.prioridad","ASC");

        $query=$this->db->get();
        return $query->result();
    }

    public function getIdUnidades($id){
        $this->db->select('up.unidad');
        $this->db->from('unidad_prospecto AS up');
        $this->db->where('up.estatus',1);
        $this->db->where('up.id_cotizacion',$id);
        //$this->db->where('up.id_contrato',0);
        $query=$this->db->get();
        //$this->db->close();

        $result_array = array();

        foreach ($query->result_array() as $row) {
            $result_array[] = $row['unidad'];
        }

        return $result_array;
    }


    public function getDataDestinos($id){
        $this->db->select('dp.*, up.unidad');
        
        $this->db->from('destino_prospecto dp');
        $this->db->join('unidad_prospecto up', 'dp.id_unidPros = up.id', 'left');

        $this->db->where('dp.id_cotizacion', $id);
        $this->db->where('dp.estatus',1);
        $this->db->where('up.estatus',1);
        $query=$this->db->get();
        return $query->result();
    }

    function getDataCotiza2($params){
        $this->db->select("cot.*, concat(cli.nombre,' ',cli.app,' ',cli.apm) as cliente,cli.telefono,cli.correo,cli.tipo_cliente,cli.tipo,cli.rfc,cli.calle,cli.cod_postal,cli.ciudad,
            concat(p.nombre,' ',p.apellido_p) as vendedor,
            (select sum(monto) from unidad_prospecto WHERE id_cotizacion=cot.id AND id_cotizacion!=0 AND id_contrato=0 AND estatus= 1) as tot_unids");
        $this->db->from('cotizaciones cot');
        $this->db->join('prospectos cli', 'cot.idCliente = cli.id', 'left');
        $this->db->join('personal p', 'p.personalId=cot.personal');
        //$this->db->where('cli.tipo',1);
        $this->db->where('cot.estatus',1);
        $this->db->where('cot.es_contrato',0);

        if($params["id_cliente"]!="0"){
            $this->db->where("idCliente",$params["id_cliente"]);
        }
        if($params["seguimiento"]!="0"){
            $this->db->where("seguimiento",$params["seguimiento"]);
        }
        if($params["tipo_cliente"]!="0"){
            $this->db->where("tipo_cliente",$params["tipo_cliente"]);
        }
        if($params["prioridad"]!="0"){
            $this->db->where("prioridad",$params["prioridad"]);
        }
        if($params["fechai"]!="0" && $params["fechaf"]!="0"){
            $this->db->where('cot.fecha_salida BETWEEN '.'"'.$params["fechai"].'"'.' AND '.'"'.$params["fechaf"].'"');
        }
        //$this->db->order_by("cot.fecha_salida","DESC");
        $this->db->order_by("cot.prioridad","ASC");

        $query=$this->db->get();
        return $query->result();
    }
}