<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloChoferes extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_result($params){
        $columns = array( 
            0=>'c.choferId',
            1=>'c.nombre',
            2=>'c.apellido_p',
            3=>'c.apellido_m',
            4=>'c.telefono',
            5=>'c.fecha_ingreso'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('choferes c');
        $this->db->where('c.estatus',1);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }


    public function total_result($params){
        $columns = array( 
            0=>'c.choferId',
            1=>'c.nombre',
            2=>'c.apellido_p',
            3=>'c.apellido_m',
            4=>'c.telefono',
            5=>'c.fecha_ingreso'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('choferes c');
        $this->db->where('c.estatus',1);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function get_existing_user($usu)
    {
        $sql = "SELECT UsuarioID FROM usuarios AS u
        LEFT JOIN personal AS p ON u.personalId = p.personalId
        WHERE u.Usuario = '$usu'
        AND p.estatus != 0 ";

        $query = $this->db->query($sql);
        return $query->row();
    }


}
