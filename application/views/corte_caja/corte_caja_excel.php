<?php
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=corte_caja".date('Ymd Gis').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th colspan="6">
                <h4>Anticipos a contrato</h4>
            </th>
        </tr>
        <tr>
            <th scope="col">Contrato</th>
            <th scope="col">Folio</th>
            <th scope="col">Cliente</th>
            <th scope="col">Fecha registro</th>
            <th scope="col">Anticipo</th>
            <th scope="col">Descuento</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($table_anticipo as $t_a) {
            echo '<tr>
                    <td >' . $t_a->id . '</td>
                    <td >' . $t_a->folio . '</td>
                    <td >' . $t_a->cliente . '</td>
                    <td >' . $t_a->fecha_contrato . '</td>
                    <td >' . number_format($t_a->monto_anticipo, 2) . '</td>
                    <td >' . number_format($t_a->porc_desc, 2) . '</td>
                </tr>';
        }
        ?>
    </tbody>
</table>
<br>

<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th colspan="6">
                <h4>Pagos a contrato</h4>
            </th>
        </tr>
        <tr>
            <th scope="col">Contrato</th>
            <th scope="col">Folio</th>
            <th scope="col">Cliente</th>
            <th scope="col">Fecha registro</th>
            <th scope="col">Pago</th>
            <th scope="col">Cuenta</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($table_pagos as $t_p) {
            echo '<tr>
                    <td >' . $t_p->id_contrato . '</td>
                    <td >' . $t_p->folio . '</td>
                    <td >' . $t_p->cliente . '</td>
                    <td >' . $t_p->fecha . '</td>
                    <td >' . number_format($t_p->monto, 2) . '</td>
                    <td >' . $t_p->cuenta . '</td>
                </tr>';
        }
        ?>
    </tbody>
</table>
<br>

<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th colspan="8">
                <h4>Gastos de contrato</h4>
            </th>
        </tr>
        <tr>
            <th scope="col">Contrato</th>
            <th scope="col">Folio</th>
            <th scope="col">Cliente</th>
            <th scope="col">Num Eco.</th>
            <th scope="col">Unidad</th>
            <th scope="col">Fecha registro</th>
            <th scope="col">Monto</th>
            <th scope="col">Tipo</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($table_gastos as $t_g) {
            $tipoGasto = " ";
            switch ($t_g->tipo) {
                case '1':
                    $tipoGasto = "Caseta";
                    break;
                case '2':
                    $tipoGasto = "Combustible";
                    break;
                case '3':
                    $tipoGasto = "Otros";
                    break;
                case '4':
                    $tipoGasto = "Sueldos";
                    break;
                default:
                    $tipoGasto = " ";
                    break;
            }

            echo '<tr>
                    <td >' . $t_g->id_contrato . '</td>
                    <td >' . $t_g->folio . '</td>
                    <td >' . $t_g->cliente . '</td>
                    <td >' . $t_g->num_eco . '</td>
                    <td >' . $t_g->vehiculo . '</td>
                    <td >' . $t_g->fecha . '</td>
                    <td >' . number_format($t_g->importe, 2) . '</td>
                    <td >' . $tipoGasto . '</td>
                </tr>';
        }
        ?>
    </tbody>
</table>
<br>


<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th colspan="2">
                <h4>Montos Totales</h4>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php
        echo '
                <tr>
                    <td>Total Anticipos:</td>
                    <td style="text-align: right;"><b>$ ' . number_format($table_totales->totalAnticipos, 2) . '</b></td>
                </tr>
                <tr>
                    <td>Total Pagos:</td>
                    <td style="text-align: right;"><b>$ ' . number_format($table_totales->totalPagos, 2) . '</b></td>
                </tr>
                <tr>
                    <td>Total Gastos:</td>
                    <td style="text-align: right;"><b>$ ' . number_format($table_totales->totalGastos, 2) . '</b></td>
                </tr>
                <tr>
                    <td>Descuento:</td>
                    <td style="text-align: right;"><b>$ ' . number_format($table_totales->descuento, 2) . '</b></td>
                </tr>
                <tr>
                    <td style="font-size: 115%">Total Final:</td>
                    <td style="text-align: right; font-size: 115%"><b>$ ' . number_format($table_totales->totalFinal, 2) . '</b></td>
                </tr>
                ';
        ?>
    </tbody>
</table>
<br>


<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th colspan="2">
                <h4>Montos Cuentas</h4>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php
        $html = '';
        foreach ($table_cuentas as $t_c) {
            log_message('error', 'TABLA CUENTAS:JS: ' . json_encode($t_c));

            if ($t_c->cuenta == "EFECTIVO") {
                $html .= '<tr>
                            <td scope="col">Total Efectivo:</td>
                            <td scope="col" style="text-align: right;"><b>$ ' . number_format($t_c->pagos_cuenta, 2) . '</b></td>
                        </tr>';

            } else {
                $html .= '<tr>
                            <td scope="col">Total Cuenta <i>' . $t_c->cuenta . '</i>:</td>
                            <td scope="col" style="text-align: right;"><b>$ ' . number_format($t_c->pagos_cuenta, 2) . '</b></td>
                        </tr>';
            }

        }
        echo $html
        ?>
    </tbody>
</table>
<br>