<div class="container-fluid">
	<div class="page-header">
		<div class="row">
			<div class="col-sm-6">
				<h3>Corte de Caja</h3>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="mb-4 row">
							<div class="col-sm-2">
								<label class="col-sm-6 col-form-label">Contrato:</label>
								<select class="form-control " id="sel_contrato">
									<option disabled selected value="0">Buscar un contrato</option>
									<?php foreach ($contratos->result() as $c) {
										echo '<option value="'.$c->id.'">'.$c->id.'</option>';
									} ?>
								</select>
							</div>
							<div class="col-sm-2">
								<label class="col-sm-6 col-form-label">Cliente:</label>
								<select class="form-control " id="sel_cliente">
									<option disabled selected value="0">Buscar cliente</option>
								</select>
							</div>
							<div class="col-sm-2">
								<label class="col-sm-6 col-form-label">Desde:</label>
								<input type="date" id="fecha_ini" class="form-control">
							</div>
							<div class="col-sm-2">
								<label class="col-sm-6 col-form-label">Hasta:</label>
								<input type="date" id="fecha_fin" class="form-control">
							</div>
							<div class="col-md-1 pt-2">
								<label class="col-sm-2 col-form-label" style="margin-top: 11px;"></label>
								<button id="search" type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
							</div>
							<div class="col-md-1 pt-2">
								<label class="col-sm-2 col-form-label" style="margin-top: 11px;"></label>
								<button title="Exportar a Excel" id="export_excel" type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i></button>
							</div>
							<div class="col-md-1 pt-2" style="display: none;">
								<label class="col-sm-2 col-form-label" style="margin-top: 11px;"></label>
								<button title="Exportar a PDF" id="export_pdf" type="button" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i></button>
							</div>
						</div>

						<div class="col-12 mb-3">
							<div class="table-responsive">
								<hr>
								<h4>Anticipos a contrato</h4>
								<table class="table" id="table_data_anticipo">
									<thead>
										<tr>
											<th scope="col">Contrato</th>
											<th scope="col">Folio</th>
											<th scope="col">Cliente</th>
											<th scope="col">Fecha registro</th>
											<th scope="col">Anticipo</th>
											<th scope="col">Descuento</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>

						<div class="col-12 mb-3">
							<div class="table-responsive">
								<hr>
								<h4>Pagos a contrato</h4>
								<table class="table" id="table_data_pagos">
									<thead>
										<tr>
											<th scope="col">Contrato</th>
											<th scope="col">Folio</th>
											<th scope="col">Cliente</th>
											<th scope="col">Fecha registro</th>
											<th scope="col">Pago</th>
											<th scope="col">Cuenta</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>

						<div class="col-12 mb-3">
							<div class="table-responsive">
								<hr>
								<h4>Gastos de contrato</h4>
								<table class="table" id="table_data_gastos">
									<thead>
										<tr>
											<th scope="col">Contrato</th>
											<th scope="col">Folio</th>
											<th scope="col">Cliente</th>
											<th scope="col">Num. Eco.</th>
											<th scope="col">Unidad</th>
											<th scope="col">Fecha registro</th>
											<th scope="col">Monto</th>
											<th scope="col">Tipo</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>

						<div class="col-6 mb-3">
							<div class="table-responsive">
								<hr>
								<h4>Montos Totales</h4>
								<div id="cont_totales">
									<table class="table" id="table_totales" style="width:100%;">
										<tbody>
											<tr>
												<td scope="col">Total Anticipos:</td>
												<td scope="col" style="text-align: right;"><b>$ ---</b></td>
											</tr>
											<tr>											
												<td scope="col">Total Pagos:</td>
												<td scope="col" style="text-align: right;"><b>$ ---</b></td>
											</tr>
											<tr>
												<td scope="col">Total Gastos:</td>
												<td scope="col" style="text-align: right;"><b>$ ---</b></td>
											</tr>
											<tr>
												<td scope="col">Descuento:</td>
												<td scope="col" style="text-align: right;"><b>$ ---</b></td>
											</tr>
											<tr>
												<td scope="col" style="font-size: 115%">Total Final:</td>
												<td scope="col" style="text-align: right; font-size: 115%"><b>$ ---</b></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>

						<div class="col-6 mb-3">
							<div class="table-responsive">
								<hr>
								<h4>Montos Cuentas</h4>
								<div id="cont_cuentas">
									<table class="table" id="cont_cuentas" style="width:100%;">
										<tbody>
											<tr>
												<td scope="col">Total Efectivo:</td>
												<td scope="col" style="text-align: right;"><b>$ ---</b></td>
											</tr>
											<tr>											
												<td scope="col">Total Cuentas:</td>
												<td scope="col" style="text-align: right;"><b>$ ---</b></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>