<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=lista_cotizaciones".date('Ymd Gis').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Cliente</th>
            <th scope="col">Tipo Cliente</th>
            <th scope="col">Fecha registro</th>
            <th scope="col">Lugar origen</th>
            <th scope="col">Fecha salida</th>
            <th scope="col">Unidad(es)</th>
            <th scope="col">Destino</th>
            <th scope="col">No. de días</th>
            <th scope="col">Teléfono</th>
            <th scope="col">Teléfono 2</th>
            <th scope="col">Email</th>
            <th scope="col">Total</th>
            <th scope="col">Vendedor</th>
            <th scope="col">Prioridad</th>
            <th scope="col">Estatus</th>
            <th scope="col">Obervaciones</th>
        </tr>
    </thead>
    <tbody>
    	<?php
        foreach ($cot as $c) {
            $tipo="";
            $unid = $this->ModeloCotizaciones->getUnidadesCotizacion($c->id);

            $tabUnidades = '<table border="1"><tbody>';
            $tabDestinos = '<table border="1"><tbody>';

            foreach($unid->result() as $u){
                $dest = $this->ModeloGeneral->getselectwhere2('destino_prospecto', array('id_cotizacion' => $c->id, 'id_unidPros' => $u->id, "estatus" => 1));

                $rowspan = $unid->num_rows() > 1 ? $unid->num_rows() : 1;

                $tabUnidades .= '
                    <tr style="vertical-align: middle;">
                        <td rowspan="' . $rowspan . '">
                            ' . $u->vehiculo . '
                        </td>
                    </tr>
                ';

                $destino = '';
                foreach ($dest->result() as $key => $d) {
                    $destino .= $d->lugar;
                    if ($key < ($dest->num_rows() - 1)) {
                        $destino .= ' | ';
                    }
                }

                $tabDestinos .= '
                    <tr style="vertical-align: middle;">
                        <td rowspan="' . $rowspan . '">
                            ' . $destino . '
                        </td>
                    </tr>
                ';

                if ($rowspan > 1) {
                    for ($i = 0; $i < ($rowspan - 1); $i++) {
                        $tabUnidades .= '<tr></tr>';
                        $tabDestinos .= '<tr></tr>';
                    }
                }
            }

            $tabUnidades .= '</tbody></table>';
            $tabDestinos .= '</tbody></table>';

            if($c->tipo=="0"){
                $tipo="Prospecto";
            }
            if($c->tipo_cliente=="1"){
                $tipo="Potencial";
            }else if($c->tipo_cliente=="2"){
                $tipo="Ocacional";
            }else if($c->tipo_cliente=="3"){
                $tipo="Estandar";
            }else if($c->tipo_cliente=="4"){
                $tipo="Bronce";
            }else if($c->tipo_cliente=="5"){
                $tipo="Oro";
            }else if($c->tipo_cliente=="6"){
                $tipo="Diamante";
            }

            $prioridad="";
            if($c->prioridad=="1"){
                $prioridad="<span class='btn btn-danger'>Alta</span>";
            }else if($c->prioridad=="2"){
                $prioridad="<span class='btn btn-warning'>Media</span>";
            }else if($c->prioridad=="3"){
                $prioridad="<span class='btn btn-info'>Baja</span>";
            }

            if($c->seguimiento=="1"){
                $seguimiento="<span class='btn btn-light'>Creada</span>";
            }else if($c->seguimiento=="2"){
                $seguimiento="<span class='btn btn-info'>En proceso</span>";
            }else if($c->seguimiento=="3"){
                $seguimiento="<span class='btn btn-warning'>Revisada</span>";
            }else if($c->seguimiento=="4"){
                $seguimiento="<span class='btn btn-success'>Autorizada</span>";
            }else if($c->seguimiento=="5"){
                $seguimiento="<span class='btn btn-secondary'>Expirada</span>";
            }else if($c->seguimiento=="6"){
                $seguimiento="<span class='btn btn-danger'>Rechazada</span>";
            }else if($c->seguimiento=="7"){
                $seguimiento="<span class='btn btn-primary'>Enviada</span>";
            }

            $fecha_s = $c->fecha_salida;
            $sal = new DateTime($fecha_s);
            $fecha_r = $c->fecha_regreso;
            $reg = new DateTime($fecha_r);

            $diferencia = $sal->diff($reg);
            if ($diferencia->format('%a') == 0) {
                $tot_dias = 1;
            } else {
                $tot_dias = intval($diferencia->format('%a'));
            }


            echo '
            <tr style="vertical-align: middle;">
                <td >'.$c->id.'</td>
                <td >'.$c->cliente.'</td>
                <td >'.$tipo.'</td>
                <td >'.$c->fecha_reg.'</td>
                <td >'.$c->lugar_origen.'</td>
                <td >'.$c->fecha_salida.'</td>
                <td >'.$tabUnidades.'</td>
                <td >'.$tabDestinos.'</td>
                <td >'.$tot_dias.'</td>
                <td >'.$c->telefono.'</td>
                <td >'.$c->telefono_2.'</td>
                <td >'.$c->correo.'</td>
                <td >'.$c->tot_monto.'</td>
                <td >'.$c->vendedor.'</td>
                <td >'.$prioridad.'</td>
                <td >'.$seguimiento.'</td>
                <td >'.$c->observaciones.'</td>
            </tr>';
        }
        ?>
    </tbody>
</table>
