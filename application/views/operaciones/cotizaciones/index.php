<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Listado de cotizaciones</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="mb-3 row">
              <div class="col-sm-4">
                <label class="col-sm-6 col-form-label">Cliente</label>
                <select class="form-control" id="id_cliente">
                </select>
              </div>
              <div class="col-sm-2">
                <label class="col-sm-6 col-form-label">Estatus</label>
                <select class="form-control" id="seguimiento">
                  <option value="0">Todos</option>
                  <option value="1">Creada</option>
                  <option value="2">En proceso</option>
                  <option value="3">Revisada</option>
                  <option value="4">Autorizada</option>
                  <option value="5">Expirada</option>
                  <option value="6">Rechazada</option>
                  <option value="7">Enviada</option>
                </select>
              </div>
              <div class="col-sm-3">
                <label class="col-sm-6 col-form-label">Tipo Cliente</label>
                <select class="form-control" id="tipo_cliente">
                  <option value="0">Todos</option>
                  <option value="1">Potencial</option>
                  <option value="2">Ocacional</option>
                  <option value="3">Estandar</option>
                  <option value="4">Bronce</option>
                  <option value="5">Oro</option>
                  <option value="6">Diamante</option>
                </select>
              </div>
              <div class="col-sm-3">
                <label class="col-sm-6 col-form-label">Prioridad</label>
                <select class="form-control" id="prioridad">
                  <option value="0">Todos</option>
                  <option value="1">Alta</option>
                  <option value="2">Media</option>
                  <option value="3">Baja</option>
                </select>
              </div>
              <div class="col-sm-2">
                <label class="col-sm-6 col-form-label">Desde:</label>
                <input type="date" id="fechai" class="form-control">
              </div>
              <div class="col-sm-2">
                <label class="col-sm-6 col-form-label">Hasta:</label>
                <input type="date" id="fechaf" class="form-control">
              </div>
              <div class="col-sm-2">
                <label class="col-sm-6 col-form-label">Activos</label>
                <select class="form-control" id="activos">
                  <option value="1">Activos</option>
                  <option value="0">Eliminados</option>
                </select>
              </div>

              <div class="col-md-1">
                <label class="col-sm-2 col-form-label" style="margin-top: 20px;"></label>
                <button id="search" type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
              </div>
              <div class="col-md-1">
                <label class="col-sm-2 col-form-label" style="margin-top: 20px;"></label>
                <button title="Exportar a Excel" id="export-excel" type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i></button>
              </div>
              <div class="col-md-1">
                <label class="col-sm-2 col-form-label" style="margin-top: 20px;"></label>
                <button title="Exportar a PDF" id="export-pdf" type="button" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i></button>
              </div>

              <div class="col-md-3">
                <label class="col-sm-6 col-form-label" style="margin-top: 20px;"></label>
                <button title="Exportar estimados a Excel" id="export-excel-estimados" type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i> Estimados</button>
              </div>

            </div>
            <div class="col-md-3">
              <a class="btn btn-primary" href="<?php echo base_url() ?>Cotizaciones/registro">Nuevo</a><br><br>
            </div>

            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table" id="table_data">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Cliente</th>
                      <th scope="col">Fecha registro</th>
                      <th scope="col">Lugar origen</th>
                      <th scope="col">Teléfono</th>
                      <th scope="col">Email</th>
                      <th scope="col">Vendedor</th>
                      <th scope="col">Prioridad</th>
                      <th scope="col">Estatus</th>
                      <th scope="col">Acciones</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!------------------------------------------------>

<div class="modal fade" id="modal_seguimiento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Seguimiento de cotización</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <!---->
              <form class="form" method="post" role="form" id="form_segs">
                <div class="card-body">
                  <div class="mb-3 form-control">
                    <input class="form-control" type="hidden" id="id_cont_seg" value="0">
                    <label class="col-sm-4 col-form-label">Cotización:</label>
                    <div class="col-sm-2">
                      <input class="form-control" type="text" id="txt_contrato" readonly>
                    </div>
                  </div>
                  <div class="col-sm-6 form-control">
                    <label class="col-sm-6 col-form-label">Estatus:</label>
                    <select class="form-control" id="seguimiento_modal">
                      <option value="0">Todos</option>
                      <option value="1">Creada</option>
                      <option value="2">En proceso</option>
                      <option value="3">Revisada</option>
                      <option value="4">Autorizada</option>
                      <option value="5">Expirada</option>
                      <option value="6">Rechazada</option>
                      <option value="7">Enviada</option>
                    </select>
                  </div>
                  <div class="col-sm-6 form-control" id="cont_rechazo" style="display:none">
                    <label class="col-sm-12 col-form-label">Motivo de rechazo</label>
                    <div class="col-sm-12">
                      <textarea rows="4" value="" class="form-control" type="text" id="motivo_rechazo"></textarea>
                    </div>
                  </div>
                </div>
              </form>
              <!---->
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cerrar</button>
        <button class="btn btn-primary" type="button" onclick="updateSeguimiento()">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<!------------------------------------------------>

<div class="modal fade" id="modal_estimados" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Datos estimados</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <!---->
              <form class="form" method="post" role="form" id="form_segs">
                <div class="card-body">
                  
                  <div class="mb-3 row">
                    <input class="form-control" type="hidden" id="idCoti"  value="0">
                    <input class="form-control" type="hidden" id="idEstima"  value="0">
                    
                    <label class="col-sm-2 col-form-label">Cotización:</label>
                    <div class="col-sm-4">
                      <input class="form-control" type="text" id="cotizacion" readonly>
                    </div>

                    
                    <div class="col-sm-6 d-flex justify-content-end">
                      <button class="btn btn-success" type="button" id="export-excel-estimado"><i class="fa fa-file-excel-o"></i> Exportar</button>
                    </div>

                    <label class="col-sm-2 col-form-label">Cliente:</label>
                    <div class="col-sm-4">
                      <input class="form-control" type="text" id="cliente" readonly>
                    </div>
                  </div>

                  <div class="mb-3 row">
                    <div class="col-sm-6">
                      <label class="col-form-label">Costo del combustible:</label>
                      <input class="form-control" type="number" id="precio_gas" onchange="calcular_cotizacion()">
                    </div>

                    <div class="col-sm-6">
                      <label class="col-form-label">Sueldo del chofer:</label>
                      <input class="form-control" type="number" id="sueldo" onchange="calcular_cotizacion()">
                    </div>
                  </div>

                  <div class="mb-3 row">
                    <div class="col-sm-6">
                      <label class="col-form-label">Total de Casetas:</label>
                      <input class="form-control" type="number" id="casetas" onchange="calcular_cotizacion()">
                    </div>

                    <div class="col-sm-6">
                      <label class="col-form-label">Total de Otros:</label>
                      <input class="form-control" type="number" id="otros" onchange="calcular_cotizacion()">
                    </div>
                  </div>

                  <div class="mb-3 row">
                    <div class="mb-3 row">
                      <hr>
                      <label class="col-form-label col-sm-12">Unidades:</label>
                      <select class="form-control" id="units" style="width: 70%;">
                        <option value="0" selected="" disabled="">Seleccionar una opción</option>
                        <?php foreach ($unidades->result() as $u) {
                          echo '<option value="'.$u->id.'" data-eco="'.$u->num_eco.'" data-type="'.$u->tipo.'" data-rendi="'.$u->rendimiento.'">'.$u->vehiculo.'</option>';
                        } ?>
                      </select>
                      <input class="form-control" type="number" id="km_reco" style="width:20%;" placeholder="Kilometros recorridos">
                      <button type="button" class="btn btn-primary" style="width:10%;" title="Agregar unidad" onclick="selectUnidad()"><i id="btn_plus" class="fa fa-plus" aria-hidden="true"></i></button>


                      <table id="table_units" width="100%" class="table table-striped ">
                        <thead id="thead_units">
                          <tr style="width: 100%">
                            <td width="25%">
                              <label class="col-form-label">Unidad</label>
                            </td>
                            <td width="10%">
                              <label class="col-form-label">Número económico</label>
                            </td>
                            <td width="25%">
                              <label class="col-form-label">Tipo</label>
                            </td>
                            <td width="10%">
                              <label class="col-form-label">Cantidad</label>
                            </td>
                            <td width="20%">
                              <label class="col-form-label">KMs recorridos</label>
                            </td>
                            <td width="10%">
                              <label class="col-form-label">Eliminar</label>
                            </td>
                          </tr>
                        </thead>
                        
                        <tbody id="tbody_units">
                        </tbody>
                      </table>
                    </div>

                    <div class="col-sm-12">
                      <label class="col-form-label">Total Combustible:</label>
                      <input class="form-control" type="number" id="combustible"  onchange="calcular_cotizacion(1)">
                    </div>
                  
                  </div>

                  <div class="mb-3 mt-3 row">
                    <div class="col-sm-6">
                      <label class="col-form-label">Utilidad %:</label>
                      <input class="form-control" type="number" id="utilidad" onchange="calcular_cotizacion()">
                    </div>
                    
                    <div class="col-sm-6">
                      <label class="col-form-label">Gastos aproximados:</label>
                      <input class="form-control" type="number" id="gasto_aprox" readonly>
                    </div>
                  </div>

                  <div class="mb-3 mt-3 row">
                    <hr>
                    <div class="col-sm-12">
                      <label class="col-form-label">Cotización aproximada:</label>
                      <input class="form-control" type="number" id="cot_aprox" readonly>
                    </div>
                  </div>
                  
                </div>
              </form>
              <!---->
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cerrar</button>
        <button class="btn btn-primary" type="button" id="btnSaveEstima" onclick="save_estimados()">Aceptar</button>
      </div>
    </div>
  </div>
</div>
