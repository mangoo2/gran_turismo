<style type="text/css">
  .option-view:hover {
    cursor: pointer;
  }

  .tui-full-calendar-popup-detail-date{
    display: none;
  }

  /* Personalizar el tamaño de la ventana emergente */
  .tui-full-calendar-month-more {
    /*width: 100% !important; */
    height: 115% !important;
    /*background-color: red !important;*/
  }

</style>

<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Agenda de Contratos</h3>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-2">
              <button class="btn btn-primary" type="button" onclick="toggle_date(0)">Hoy</button>
            </div>

            <div class="col-5" style="text-align: center;">
              <h4 id="cal-date">Abril 2023</h4>
            </div>

            <div class="col-3">
              <button class="btn btn-primary" id="dropdownMenu-calendarType" type="button" data-bs-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
                <span>Vista de calendario </span><i class="fa fa-angle-down"></i>
              </button>
              <ul class="dropdown-menu">
                <li class="option-view">&nbsp;&nbsp;<a class="dropdown-menu-title" onclick="toggle_view('dia')"><i class="fa fa-bars"></i> Día</a></li>
                <li class="option-view">&nbsp;&nbsp;<a class="dropdown-menu-title" onclick="toggle_view('sem')"><i class="fa fa-th-large"></i> Semana</a></li>
                <li class="option-view">&nbsp;&nbsp;<a class="dropdown-menu-title" onclick="toggle_view('mes')"><i class="fa fa-th"></i> Mes</a></li>
              </ul>
            </div>

            <div class="col-2">
              <button class="btn btn-primary " type="button" onclick="toggle_date(-1)"><i class="fa fa-angle-left"></i></button>
              <button class="btn btn-primary " type="button" onclick="toggle_date(1)"><i class="fa fa-angle-right"></i></button>
            </div>
          </div>

          <div class="row">
            <div class="col-12">
                <div class="pt-3" id="calendario"></div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>