<style type="text/css">
	.kv-file-upload, .fileinput-remove-button, .close, .fileinput-remove {
		display: none !important;
	}

  #selUnidad option:disabled {
    color: #A9A9A9; /* Un tono de gris más oscuro */
    background-color: #f0f0f0; /* Fondo más claro para que contraste */
    opacity: 0.6; /* Hacer que se vea más apagado */
  }

</style>

<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Listado de Contratos</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="mb-3 row">
              <div class="col-sm-4 hide-for-chofer">
                <label class="col-sm-6 col-form-label">Cliente</label>
                <input class="form-control" type="hidden" id="id_perfil" value="<?php if(isset($perfilId)) echo $perfilId; ?>" readonly>
                <input class="form-control" type="hidden" id="id_chofer" value="<?php if(isset($choferId)) echo $choferId; ?>" readonly>
                <select class="form-control " id="id_cliente">
                </select>
              </div>
              <div class="col-sm-2 hide-for-chofer">
                <label class="col-sm-6 col-form-label">Estatus</label>
                <select class="form-control" id="seguimiento">
                  <option value="0">Todos</option>
                  <option value="1">Creada</option>
                  <option value="2">En proceso</option>
                  <option value="3">Revisada</option>
                  <option value="4">Autorizada</option>
                  <option value="5">Expirada</option>
                  <option value="6">Rechazada</option>
                  <option value="7">Enviada</option>
                </select>
              </div>
              <div class="col-sm-3 hide-for-chofer">
                <label class="col-sm-6 col-form-label">Tipo Cliente</label>
                <select class="form-control" id="tipo_cliente">
                  <option value="0">Todos</option>
                  <option value="1">Potencial</option>
                  <option value="2">Ocacional</option>
                  <option value="3">Estandar</option>
                  <option value="4">Bronce</option>
                  <option value="5">Oro</option>
                  <option value="6">Diamante</option>
                </select>
              </div>
              <div class="col-sm-3 hide-for-chofer">
                <label class="col-sm-6 col-form-label">Prioridad</label>
                <select class="form-control" id="prioridad">
                  <option value="0">Todos</option>
                  <option value="1">Alta</option>
                  <option value="2">Media</option>
                  <option value="3">Baja</option>
                </select>
              </div>
              <div class="col-sm-3 hide-for-chofer">
                <label class="col-sm-6 col-form-label">Desde:</label>
                <input type="date" id="fechai" class="form-control">
              </div>
              <div class="col-sm-3 hide-for-chofer">
                <label class="col-sm-6 col-form-label">Hasta:</label>
                <input type="date" id="fechaf" class="form-control">
              </div>
              <div class="col-sm-3 hide-for-chofer">
                <label class="col-sm-6 col-form-label">Pagado</label>
                <select class="form-control" id="liquidado">
                  <option value="2">Todos</option>
                  <option value="1">Liquidado</option>
                  <option value="0">Pendiente</option>
                </select>
              </div>
              <div class="col-md-1 hide-for-chofer">
                <label class="col-sm-2 col-form-label" style="margin-top: 11px;"></label>
                <button id="search" type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
              </div>
              <div class="col-md-1 hide-for-chofer">
                <label class="col-sm-2 col-form-label" style="margin-top: 11px;"></label>
                <button title="Exportar a Excel" id="export-excel" type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i></button>
              </div>
              <div class="col-md-1 hide-for-chofer">
                <label class="col-sm-2 col-form-label" style="margin-top: 11px;"></label>
                <button title="Exportar a PDF" id="export-pdf" type="button" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i></button>
              </div>

            </div>
            <div class="col-3 hide-for-chofer">
              <a class="btn btn-primary" href="<?php echo base_url() ?>Contratos/registro">Nuevo</a><br><br>
            </div>
            <div class="col-12">
              <div class="table-responsive">
                <table class="table" id="table_data">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Folio</th>
                      <th scope="col">Cliente</th>
                      <th scope="col">Fecha registro</th>
                      <th scope="col">Lugar origen</th>
                      <!--<th scope="col">Fecha de contrato</th>-->
                      <th scope="col">Teléfono</th>
                      <th scope="col">Email</th>
                      <th scope="col">Vendedor</th>
                      <th scope="col">Total</th>
                      <th scope="col">Pagado</th>
                      <th scope="col">Prioridad</th>
                      <th scope="col">Estatus</th>
                      <th scope="col">Acciones de contrato</th>
                      <th></th>
                      <th scope="col">Fecha de contrato</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!----------------->
<div class="modal fade" id="modal_contrato" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Detalles de contrato</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <!---->
              <form class="form" method="post" role="form" id="form_data">
                <div class="card-body">
                  <div class="mb-3 row">
                    <div class="col-md-4">
                      <input class="form-control" type="hidden" id="id_contrato" name="id_contrato">
                    </div>
                    <div class="col-md-4">
                      <input class="form-control" type="hidden" id="id_detalle" name="id_detalle" value="0">
                    </div>
                    <div class="col-md-4">
                      <input class="form-control" type="hidden" id="id_prospecto" name="id_prospecto" value="0">
                    </div>
                  </div>

                  <div class="row">
                    <div class="col">
                      <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Calle y numero</label>
                        <div class="col-sm-10">
                          <input class="form-control" type="text" name="calle" id="calle">
                        </div>
                      </div>

                          <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Colonia</label>
                            <div class="col-sm-4">
                              <input class="form-control" type="text" name="colonia" id="colonia">
                            </div>

                            <label class="col-sm-2 col-form-label">Codigo Postal</label>
                            <div class="col-sm-4">
                              <input class="form-control" type="text" pattern="^[0-9]{0,10}$" maxlength="5" minlength="5" name="cp" id="cp">
                            </div>
                          </div>
                          
                          <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Ciudad o municipio</label>
                            <div class="col-sm-4">
                              <input class="form-control" type="text" name="ciudad_municipio" id="ciudad">
                            </div>

                            <label class="col-sm-2 col-form-label">Estado</label>
                            <div class="col-sm-4">
                              <select class="form-control" name="estado" id="estado">
                                <?php foreach ($estados->result() as $e) {
                                  echo '<option value="'.$e->EstadoId.'">'.$e->Nombre.'</option>';
                                } ?>
                                
                              </select>
                            </div>
                          </div>

                          <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Tipo de servicio</label>
                            <div class="col-sm-4">
                              <select class="form-control" name="tipo_servicio" id="tipo_servicio">
                                <option value="1">Turistico</option>
                              </select>
                            </div>

                            <label class="col-sm-2 col-form-label">Fecha de contrato</label>
                            <div class="col-sm-4">
                              <input class="form-control" type="date" name="fecha_contrato" id="fecha_contrato">
                            </div>
                          </div>
                          

                          <!----- UNIDADES ----->
                          <div class="col-md-12 mt-3">
                            <hr>
                            <h4 class="form-section">Datos de Unidades</h4>
                            <table id="table_unidades" width="100%">
                              <tbody id="tbody_unidades">
                              </tbody>
                            </table>
                          </div>
                          <!----- UNIDADES ----->
                          

                          <hr>
                          <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Observaciones</label>
                            <div class="col-sm-10">
                              <textarea rows="4" class="form-control" type="text" name="observaciones" id="observaciones"></textarea>
                            </div>
                          </div>

                          <div style="display:none;">
                            <select class="form-control form-control-smr" id="tipo_unidad_c" >
                              <option disabled selected value="0">Selecciona una opción</option>
                              <?php foreach ($unidades->result() as $u) {
                                echo '<option value="'.$u->id.'">'.$u->vehiculo.'</option>';
                              } ?>
                            </select>
                          </div>

                        </div>
                      </div>
                    </div>
                  </form>
                  <!---->
                </div>
              </div>
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cerrar</button>
                <button class="btn btn-primary" type="button" onclick="add_form()">Guardar</button>
                <!--<button type="button" class="btn btn-outline-primary">Save changes</button>-->
            </div>
        </div>
    </div>
</div>

<!------------------------------------------------>

<div class="modal fade" id="modal_pagos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Detalles de pago</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <!---->
              <form class="form" method="post" role="form" id="form_pagos">
                <div class="card-body">

                  <div class="mb-3 row">
                    <input class="form-control" type="hidden" id="idContrato" name="id_contrato" value="0">
                    <input class="form-control" type="hidden" id="idcliente" value="0">
                    <label class="col-sm-2 col-form-label">Contrato:</label>
                    <div class="col-sm-4">
                      <input class="form-control" type="text" id="referencia" readonly>
                    </div>

                    <label class="col-sm-2 col-form-label">Fecha:</label>
                    <div class="col-sm-4">
                      <input class="form-control" type="date" id="fecha" readonly>
                    </div>
                  </div>

                  <div class="mb-3 row">
                    <label class="col-sm-2 col-form-label">Cliente:</label>
                    <div class="col-sm-10">
                      <input class="form-control" type="text" id="cliente" readonly>
                    </div>
                  </div>

                  <div class="mb-3 row">
                    <label class="col-sm-2 col-form-label">Descuento $:</label>
                    <div class="col-sm-4">
                      <input class="form-control" type="text"  id="descuento" readonly>
                    </div>

                    <label class="col-sm-2 col-form-label">Anticipo $:</label>
                    <div class="col-sm-4">
                      <input class="form-control" type="text" id="anticipo" readonly>
                    </div>
                  </div>

                  <div class="mb-3 row">
                    <label class="col-sm-2 col-form-label">Monto contrato $:</label>
                    <div class="col-sm-4">
                      <input class="form-control" type="text"  id="monto_total" readonly>
                    </div>

                    <label class="col-sm-2 col-form-label">Total pagos $:</label>
                    <div class="col-sm-4">
                      <input class="form-control" type="text" id="pago_total" readonly>
                    </div>
                  </div>

                  <div class="mb-3 row">
                    <div class="col-sm-6">
                    </div>

                    <label class="col-sm-2 col-form-label">Restante $:</label>
                    <div class="col-sm-4">
                      <input class="form-control" type="text" id="restante" readonly>
                    </div>
                  </div>
                  <hr>
                  <div class="mb-3 row showPago">
                    <div class="mb-3 row">
                      <label class="col-sm-2 col-form-label">Cuenta</label>
                      <label class="col-sm-3 col-form-label">Referencia:</label>
                      <label class="col-sm-2 col-form-label">Fecha:</label>
                      <label class="col-sm-2 col-form-label">Monto $:</label>
                      <label class="col-sm-2 col-form-label">Comprobante:</label>
                    </div>
                    
                    <div class="mb-3 row">
                      <div class="col-sm-2">
                        <select class="form-control" name="id_cuenta" id="id_cuenta">
                          <?php foreach ($cuentas->result() as $c) {
                            echo '<option value="'.$c->id.'">'.$c->cuenta.'</option>';
                          } ?>
                        </select>
                      </div>
                      
                      <div class="col-sm-3">
                        <input class="form-control" type="text" name="referencia" id="referencia_pago">
                      </div>
                      <div class="col-sm-2">
                        <input class="form-control" type="date" name="fecha" id="fecha_pago">
                      </div>
                      <div class="col-sm-2">
                        <input class="form-control" type="text" name="monto" id="monto_pago">
                      </div>

                      <div class="col-sm-2">
                        <button type="button" id="add_comp" class="btn btn-outline-primary" title="Agregar comprobante" onclick="addComprobante()"><i class="fa fa-file-image-o" aria-hidden="true"></i></button>
                      </div>

                      <div class="col-sm-1">
                        <button type="button" id="save_pay" class="btn btn-primary" title="Agregar pago" onclick="addPago()"><i class="fa fa-save" aria-hidden="true"></i></button>
                      </div>
                    </div>
                  </div>

                  <div class="mb-3 row showPagado" style="text-align: center; display:none;">
                    <h5>-------- Contrato pagado --------</h5>
                  </div>

                  <hr>
                  <div class="mb-3 row">
                    <div class="row col-md-12">
                      <div class="col-md-3">
                        <h5 class="form-section">Historial de pagos</h5>
                      </div>
                      <div class="col-md-4">
                        <label class="col-sm-2 col-form-label" style="margin-top: 9px;"></label>
                        <button title="Exportar a Excel" id="export-excel-pays" type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i></button>
                      </div>
                    </div>

                    <table id="table_pagos" width="100%" class="table table-striped ">
                      <thead id="thead_pagos">
                        <tr style="width: 100%">
                          <td width="20%">
                            <label class="col-form-label">Cuenta</label>
                          </td>
                          <td width="20%">
                            <label class="col-form-label">Referencia</label>
                          </td>
                          <td width="15%">
                            <label class="col-form-label">Fecha</label>
                          </td>
                          <td width="20%">
                            <label class="col-form-label">Monto $</label>
                          </td>
                          <td width="15%">
                            <label class="col-form-label">Comprobante</label>
                          </td>
                          <td width="10%">
                            <label class="col-form-label">Eliminar</label>
                          </td> 
                        </tr>
                      </thead>
                      <tbody id="tbody_pagos">
                      </tbody>
                    </table>
                  </div>
                </div>
              </form>
              <!---->
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!------------------------------------------------>

<div class="modal fade" id="modal_choferes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Asignación de choferes</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <!---->
              <form class="form" method="post" role="form" id="form_choferes">
                <div class="card-body">
                  <div class="mb-3 row">
                    <input class="form-control" type="hidden" id="idContra" name="id_contrato" value="0">
                    <label class="col-sm-2 col-form-label">Contrato:</label>
                    <div class="col-sm-4">
                      <input class="form-control" type="text" id="ref" readonly>
                    </div>
                  </div>
                  <div class="mb-3 row">
                    <label class="col-sm-2 col-form-label">Cliente:</label>
                    <div class="col-sm-10">
                      <input class="form-control" type="text" id="client" readonly>
                    </div>
                  </div>
                  <hr>
                  <div class="mb-3 row">
                    <table id="table_choferes" width="100%" class="table table-striped ">
                      
                      <thead id="thead_choferes">
                        <tr style="width: 100%">
                          <td width="10%">
                            <label class="col-form-label">Cant.</label>
                          </td>
                          <td width="40%">
                            <label class="col-form-label">Unidad</label>
                          </td>
                          <td width="40%">
                            <label class="col-form-label">Chofer asignado</label>
                          </td>
                          <td width="10%">
                            <label class="col-form-label">Acciones</label>
                          </td>
                        </tr>
                      </thead>
                      
                      <tbody id="tbody_choferes">
                      </tbody>
                    </table>
                  </div>

                  <div style="display:none;">
                    <select class="form-control form-control-smr" id="Selchofer" >
                      <option value="0" selected="" disabled="">Seleccionar una opción</option>
                      <?php foreach ($choferes->result() as $c) {
                        echo '<option value="'.$c->choferid.'">'.$c->nombre.' '.$c->apellido_p.' '.$c->apellido_m.'</option>';
                      } ?>
                    </select>
                  </div>
                </div>
              </form>
              <!---->
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cerrar</button>
        <button class="btn btn-primary" type="button" onclick="addChoferes()">Guardar</button>
      </div>
    </div>
  </div>
</div>

<!------------------------------------------------>

<div class="modal fade" id="modal_pasajeros" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Asignación de pasajeros</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <!---->
              <form class="form" method="post" role="form" id="form_pasajeros">
                <div class="card-body">

                  <div class="mb-3 row">
                    <input class="form-control" type="hidden" id="id_contra" value="0">
                    
                    <label class="col-sm-2 col-form-label">Contrato:</label>
                    <div class="col-sm-4">
                      <input class="form-control" type="text" id="contrato_id" readonly>
                    </div>

                  </div>

                  <div class="mb-3 row">
                    <label class="col-sm-2 col-form-label">Cliente:</label>
                    <div class="col-sm-10">
                      <input class="form-control" type="text" id="client_id" readonly>
                    </div>
                  </div>

                  <hr>
                  <div class="mb-3 row">
                    <table id="table_pasajeros" width="100%" class="table table-striped ">
                      
                      <thead id="thead_pasajeros">
                        <tr style="width: 100%">
                          <td width="10%">
                            <label class="col-form-label">Cant.</label>
                          </td>
                          <td width="40%">
                            <label class="col-form-label">Unidad</label>
                          </td>
                          <td width="50%">
                            <label class="col-form-label">Pasajeros asignados</label>
                          </td>
                        </tr>
                      </thead>
                      
                      <tbody id="tbody_pasajeros">
                      </tbody>
                    </table>
                  </div>
                </div>
              </form>
              <!---->
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cerrar</button>
        <button class="btn btn-primary" type="button" onclick="addPasajeros()">Guardar</button>
      </div>
    </div>
  </div>
</div>

<!------------------------------------------------>

<div class="modal fade" id="modal_acciones" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Acciones de contrato</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <!---->
              <div class="card-body" id="btns">

              </div>
              <!---->
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_seguimiento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Seguimiento de contrato</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <!---->
              <form class="form" method="post" role="form" id="form_segs">
                <div class="card-body">
                  <div class="mb-3 form-control">
                    <input class="form-control" type="hidden" id="id_cont_seg" value="0">
                    <label class="col-sm-4 col-form-label">Contrato:</label>
                    <div class="col-sm-2">
                      <input class="form-control" type="text" id="txt_contrato" readonly>
                    </div>
                  </div>
                  <div class="col-sm-6 form-control">
                    <label class="col-sm-6 col-form-label">Estatus:</label>
                    <select class="form-control" id="seguimiento_modal">
                      <option value="0">Todos</option>
                      <option value="1">Creada</option>
                      <option value="2">En proceso</option>
                      <option value="3">Revisada</option>
                      <option value="4">Autorizada</option>
                      <option value="5">Expirada</option>
                      <option value="6">Rechazada</option>
                      <option value="7">Enviada</option>
                    </select>
                  </div>
                  <div class="col-sm-6 form-control" id="cont_rechazo" style="display:none">
                    <label class="col-sm-12 col-form-label">Motivo de rechazo</label>
                    <div class="col-sm-12">
                      <textarea rows="4" value="" class="form-control" type="text" id="motivo_rechazo"></textarea>
                    </div>
                  </div>
                </div>
              </form>
              <!---->
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cerrar</button>
        <button class="btn btn-primary" type="button" onclick="updateSeguimiento()">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_expediente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Expediente de Cliente</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <!---->
              <div class="card-body">
                <div class="mb-3 row">
                  <label class="col-sm-1 col-form-label">INE</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" id="ine" value="">
                  </div>
                </div>
                <div class="mb-3 row ">
                  <div class="col-md-12 form-group d-flex">
                    <div class="col-md-4 mx-auto">
                      <h4>Identificación Oficial</h4><hr>
                      <!--<img class="img" height="175px" id="ine_img" src="">-->
                      <input class="form-control" type="file" id="identifica" readonly>
                    </div>
                  
                    <div class="col-md-4 mx-auto">
                      <h4>Comprobante de Domicilio</h4><hr>
                      <!--<img class="img" height="175px" id="comp_img" src="">-->
                      <input class="form-control" type="file" id="comprobante" readonly>
                    </div>
                  </div>
                </div>

              </div>
              <!---->
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<!------------------------------------------------>

<div class="modal fade" id="modal_comprobante" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Cargar Comprobante</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <!---->
              <div class="card-body">
                <div class="mb-3 row">
                  <input class="form-control" type="hidden" id="edit">
                  <label class="col-sm-12 col-form-label">Generar comprobante</label>
                  <button type="button" class="btn btn-flat btn-primary" target="_blank" title="Generar comprobante" onclick="generarCompPago()" ><i class="fa fa-file-o icon_font" style="font-size: 20px;"></i> Generar Comprobante</button>
                </div>

                <hr>
                
                <div class="mb-3 row">
                  <label class="col-sm-4 col-form-label">Subir archivo</label>
                  <input class="form-control" type="hidden" id="id_pago_aux" value="">
                  <input class="form-control" type="hidden" id="comp_pago_aux" value="">
                  <input class="form-control" type="file" name="file" id="comp_pago">
                </div>
              </div>
              <!---->
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="btnG_hidden" onclick="save_comprobante()">Guardar</button>
        <button type="button" class="btn btn-primary" id="btnA_hidden" data-bs-dismiss="modal">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<!------------------------------------------------>

<div class="modal fade" id="modal_carga_contrato" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Cargar Contrato</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <!---->
              <div class="card-body">
                <div class="mb-3 row">
                  <label class="col-sm-4 col-form-label">Subir archivo</label>
                  <hr>
                  <input class="form-control" type="hidden" id="id_contrato_arch" value="">
                  <!--<input class="form-control" type="text" id="comp_contrato_aux" value="">-->
                  <input class="form-control" type="file" name="file" id="comp_contrato">

                  <button class="btn btn-primary sav_cont" style="margin-top: 5px; " type="button" onclick="save_contrato()">Guardar</button>
                </div>

                <hr style="background-color:gray;">

                <div class="mb-3 row" id="contenedor_contratos"></div>
              </div>
              <!---->
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cerrar</button>
        <button class="btn btn-primary sav_cont" type="button" onclick="save_contrato()">Guardar</button>
      </div>
    </div>
  </div>
</div>

<!------------------------------------------------>

<div class="modal fade" id="modal_selUnidad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Selección de unidad</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <!---->
              <form class="form" method="post" role="form" id="form_choferes">
                <div class="card-body">
                  <div class="mb-3 row">
                    <input class="form-control" type="hidden" id="idContModUni" name="id_contrato" value="0">
                    <input class="form-control" type="hidden" id="typeDoc" value="">
                    <input class="form-control" type="hidden" id="existRG" value="0">
                    <input class="form-control" type="hidden" id="existBV" value="0">
                    <label class="col-sm-2 col-form-label">Folio:</label>
                    <div class="col-sm-4">
                      <input class="form-control" type="text" id="fol" readonly>
                    </div>

                    <label class="col-sm-2 col-form-label">Contrato:</label>
                    <div class="col-sm-4">
                      <input class="form-control" type="text" id="cont" readonly>
                    </div>
                  </div>
                  <div class="mb-3 row">
                    <label class="col-sm-2 col-form-label">Cliente:</label>
                    <div class="col-sm-10">
                      <input class="form-control" type="text" id="clin" readonly>
                    </div>
                  </div>

                  <div class="mb-3 row">
                    <label class="col-sm-2 col-form-label">Unidad:</label>
                    <div class="col-sm-10" id="select_container">
                    </div>
                  </div>

                </div>
              </form>
              <!---->
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cerrar</button>
        <button class="btn btn-primary" type="button" onclick="selectUnidad()">Confirmar</button>
      </div>
    </div>
  </div>
</div>