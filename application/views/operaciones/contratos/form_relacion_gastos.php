<style type="text/css">
  .kv-file-upload,
  .fileinput-remove-button {
    display: none;
  }
</style>

<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Relación de gastos</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <!---->
        <form class="form" method="post" role="form" id="form_data">
          <input type="hidden" name="id" id="idContrato" value="<?php if (isset($c)) echo $c->id; else echo "0"; ?>"  readonly>
          <input type="hidden" id="idUnidad" value="<?php if (isset($u)) echo $u->unidad; else echo "0"; ?>" readonly>
          
          <div class="card-body">
            <div class="row">
              <div class="col">

                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Contrato:</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" value="<?php if (isset($c)) echo $c->id; ?>" readonly>
                  </div>
                </div>

                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Cliente:</label>
                  <div class="col-sm-10">
                    <input class="form-control" type="text" value="<?php if (isset($c) && $c->idCliente != 0) echo $clientes->nombre . " " . $clientes->app . " " . $clientes->apm; ?>" readonly>
                  </div>
                </div>

                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Cantidad recibida:</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="number" name="cant_recibida" id="cant_recibida" value="<?php if (isset($c) && isset($obs)) echo $obs->cant_recibida; ?>">
                  </div>
                </div>
              </div>

              <!--Casetas-------------------------------------------------------------------------->
              <div class="col-md-12 mt-5">
                <h4 class="form-section">Casetas</h4>

                <hr>
                <table id="table_casetas" width="100%" class="table table-striped">
                  <tbody id="tbody_casetas">

                    <tr id="tr_casetas" class="tr_casetas">
                      <td width="20%">
                        <input type="hidden" id="id" value="0">
                        <div class="form-group">
                          <label class="col-form-label">Fecha</label>
                          <div class="controls">
                            <input type="date" id="fecha" class="form-control form-control-sm" value="">
                          </div>
                        </div>
                      </td>

                      <td width="50%">
                        <div class="form-group">
                          <label class="col-form-label">Descripción</label>
                          <div class="controls">
                            <input type="text" id="descripcion" class="form-control form-control-sm" value="">
                          </div>
                        </div>
                      </td>

                      <td width="20%">
                        <div class="form-group">
                          <label class="col-form-label">Importe $</label>
                          <div class="controls">
                            <input type="text" id="importe" class="form-control form-control-sm" min="0" value="">
                          </div>
                        </div>
                      </td>

                      <td width="10%">
                        <span style="color: transparent;">add</span>
                        <button type="button" class="btn btn-primary mt-4" title="Agregar nueva caseta" onclick="addCaseta()"><i id="btn_plus" class="fa fa-plus" aria-hidden="true"></i></button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!--Casetas-------------------------------------------------------------------------->

              <!--Combustibles-------------------------------------------------------------------------->
              <div class="col-md-12 mt-5">
                <h4 class="form-section">Combustibles</h4>

                <hr>
                <table id="table_combustibles" width="100%" class="table table-striped">
                  <tbody id="tbody_combustibles">

                    <tr id="tr_combustibles" class="tr_combustibles">
                      <td width="20%">
                        <input type="hidden" id="id" value="0">
                        <div class="form-group">
                          <label class="col-form-label">Fecha</label>
                          <div class="controls">
                            <input type="date" id="fecha" class="form-control form-control-sm" value="">
                          </div>
                        </div>
                      </td>

                      <td width="50%">
                        <div class="form-group">
                          <label class="col-form-label">Descripción</label>
                          <div class="controls">
                            <input type="text" id="descripcion" class="form-control form-control-sm" value="">
                          </div>
                        </div>
                      </td>

                      <td width="20%">
                        <div class="form-group">
                          <label class="col-form-label">Importe $</label>
                          <div class="controls">
                            <input type="text" id="importe" class="form-control form-control-sm" min="0" value="">
                          </div>
                        </div>
                      </td>

                      <td width="10%">
                        <span style="color: transparent;">add</span>
                        <button type="button" class="btn btn-primary mt-4" title="Agregar nuevo combustible" onclick="addCombustible()"><i id="btn_plus" class="fa fa-plus" aria-hidden="true"></i></button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!--Combustibles-------------------------------------------------------------------------->

              <!--Otros-------------------------------------------------------------------------->
              <div class="col-md-12 mt-5">
                <h4 class="form-section">Otros</h4>

                <hr>
                <table id="table_otros" width="100%" class="table table-striped">
                  <tbody id="tbody_otros">

                    <tr id="tr_otros" class="tr_otros">
                      <td width="20%">
                        <input type="hidden" id="id" value="0">
                        <div class="form-group">
                          <label class="col-form-label">Fecha</label>
                          <div class="controls">
                            <input type="date" id="fecha" class="form-control form-control-sm" value="">
                          </div>
                        </div>
                      </td>

                      <td width="50%">
                        <div class="form-group">
                          <label class="col-form-label">Descripción</label>
                          <div class="controls">
                            <input type="text" id="descripcion" class="form-control form-control-sm" value="">
                          </div>
                        </div>
                      </td>

                      <td width="20%">
                        <div class="form-group">
                          <label class="col-form-label">Importe $</label>
                          <div class="controls">
                            <input type="text" id="importe" class="form-control form-control-sm" min="0" value="">
                          </div>
                        </div>
                      </td>

                      <td width="10%">
                        <span style="color: transparent;">add</span>
                        <button type="button" class="btn btn-primary mt-4" title="Agregar nuevo otro" onclick="addOtro()"><i id="btn_plus" class="fa fa-plus" aria-hidden="true"></i></button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!--Otros-------------------------------------------------------------------------->

              <!--Sueldos-------------------------------------------------------------------------->
              <div class="col-md-12 mt-5">
                <h4 class="form-section">Sueldos</h4>

                <hr>
                <table id="table_sueldos" width="100%" class="table table-striped">
                  <tbody id="tbody_sueldos">

                    <tr id="tr_sueldos" class="tr_sueldos">
                      <td width="20%">
                        <input type="hidden" id="id" value="0">
                        <div class="form-group">
                          <label class="col-form-label">Fecha</label>
                          <div class="controls">
                            <input type="date" id="fecha" class="form-control form-control-sm" value="">
                          </div>
                        </div>
                      </td>

                      <td width="50%">
                        <div class="form-group">
                          <label class="col-form-label">Descripción</label>
                          <div class="controls">
                            <input type="text" id="descripcion" class="form-control form-control-sm" value="">
                          </div>
                        </div>
                      </td>

                      <td width="20%">
                        <div class="form-group">
                          <label class="col-form-label">Importe $</label>
                          <div class="controls">
                            <input type="text" id="importe" class="form-control form-control-sm" min="0" value="">
                          </div>
                        </div>
                      </td>

                      <td width="10%">
                        <span style="color: transparent;">add</span>
                        <button type="button" class="btn btn-primary mt-4" title="Agregar nuevo sueldo" onclick="addSueldo()"><i id="btn_plus" class="fa fa-plus" aria-hidden="true"></i></button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!--Sueldos-------------------------------------------------------------------------->

              <div class="col-md-12 mt-5">
                <h4 class="form-section">Observaciones</h4>
                <input type="hidden" id="idObs" value="<?php if (isset($c) && isset($obs)) echo $obs->id;
                                                        else echo '0'; ?>">
                <div class="col-sm-10">
                  <textarea rows="4" class="form-control" type="text" id="observaciones"><?php if (isset($c) && isset($obs)) echo $obs->observaciones; ?></textarea>
                </div>
              </div>

            </div>
          </div>
      </div>
      </form>
      <div class="card-footer">
        <div class="col-sm-9">
          <button class="btn btn-primary" type="button" onclick="add_form()">Guardar datos</button>
          <a href="<?php echo base_url() ?>Contratos" class="btn btn-light">Regresar</a>
        </div>
      </div>
      <!---->
    </div>
  </div>
</div>