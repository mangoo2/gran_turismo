<style type="text/css">
	.kv-file-upload, .fileinput-remove-button {
		display: none;
	}
</style>

<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Bitácora de viaje</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <!---->
        <form class="form" method="post" role="form" id="form_data">
          <input type="hidden" name="id_contrato" id="idContrato" value="<?php if (isset($c)) echo $c->id; else echo "0"; ?>" readonly>
          <input type="hidden" name="id" id="idBitaRev" value="<?php if (isset($bit_rev)) echo $bit_rev->id; else echo "0"; ?>" readonly>
          <input type="hidden" name="id_unidad" id="idUnidad" value="<?php if (isset($u)) echo $u->unidad; else echo "0"; ?>" readonly>
          <div class="card-body">
            <div class="row">
              <div class="col">
                
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Contrato:</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" value="<?php if(isset($c)) echo $c->id; ?>" readonly>
                  </div>
                </div>

                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Cliente:</label>
                  <div class="col-sm-10">
                    <input class="form-control" type="text"  value="<?php if(isset($c) && $c->idCliente != 0) echo $clientes->nombre." ".$clientes->app." ".$clientes->apm; ?>" readonly>
                  </div>
                </div>

                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Kilometraje inicial</label>
                  <div class="col-sm-4">
                    <input class="form-control" id="km_ini" name="km_ini" type="number"  value="<?php if(isset($bit_rev)) echo $bit_rev->km_ini; ?>">
                  </div>

                  <label class="col-sm-2 col-form-label">Kilometraje final</label>
                  <div class="col-sm-4">
                    <input class="form-control" id="km_fin" name="km_fin" type="number"  value="<?php if(isset($bit_rev)) echo $bit_rev->km_fin; ?>">
                  </div>
                </div>
                
                <div class="mb-3 row ">
                  <div class="col-md-4 mx-auto">
                    <label class="col-sm-12 col-form-label">Evidencia de KM Inicial</label>
                    <input class="form-control" type="hidden" id="img_aux" value="<?php if(isset($bit_rev)) echo $bit_rev->img_evide; ?>">
                    <input class="form-control" type="file" name="img_evide" id="img_evide">
                  </div>

                  <div class="col-md-4 mx-auto">
                    <label class="col-sm-12 col-form-label">Evidencia de KM Final</label>
                    <input class="form-control" type="hidden" id="img_aux_f" value="<?php if(isset($bit_rev)) echo $bit_rev->img_evide_f; ?>">
                    <input class="form-control" type="file" name="img_evide_f" id="img_evide_f">
                  </div>

                </div>

                <div class="mb-3 row">
                  <label class="col-sm-6 col-form-label">¿Me siento apto para realizar el viaje que me ha sido encomendado de forma segura?</label>
                  <div class="col-sm-6 mt-2">
                    <label>
                      <input type="radio" name="apto" value="1" <?php if(isset($bit_rev) && $bit_rev->apto=="1") echo "checked"; ?>> SI
                    </label>
                    &nbsp;
                    <label>
                        <input type="radio" name="apto" value="0" <?php if(isset($bit_rev) && $bit_rev->apto=="0") echo "checked"; ?>> NO
                    </label>
                  </div>
                </div>

                <div class="mb-4 row">
                  <label class="col-sm-12 col-form-label">Revisión de niveles de fluidos</label>
                  <div class="col-sm-3 mt-2">
                    <span> <input type="checkbox" name="revF_aceite" <?php if(isset($bit_rev) && $bit_rev->revF_aceite=="1") echo "checked"; ?>> Aceite de motor &nbsp;</span>
                  </div>

                  <div class="col-sm-3 mt-2">
                    <span> <input type="checkbox" name="revF_frenos" <?php if(isset($bit_rev) && $bit_rev->revF_frenos=="1") echo "checked"; ?>> Líquido de frenos &nbsp;</span>
                  </div>

                  <div class="col-sm-3 mt-2">
                    <span> <input type="checkbox" name="revF_neumaticos" <?php if(isset($bit_rev) && $bit_rev->revF_neumaticos=="1") echo "checked"; ?>> Presión de aire de neumáticos &nbsp;</span>
                  </div>

                  <div class="col-sm-3 mt-2">
                    <span> <input type="checkbox" name="revF_anticongelante" <?php if(isset($bit_rev) && $bit_rev->revF_anticongelante=="1") echo "checked"; ?>> Nivel de anticongelante en radiador &nbsp;</span>
                  </div>

                  <div class="col-sm-12 mt-2" style="display:none;">
                    <span> <input type="checkbox" name="revF_transmision" <?php if(isset($bit_rev) && $bit_rev->revF_transmision=="1") echo "checked"; ?>> Liquido de transmisión &nbsp;</span>
                    <br>
                    <span> <input type="checkbox" name="revF_direccion" <?php if(isset($bit_rev) && $bit_rev->revF_direccion=="1") echo "checked"; ?>> Liquido de dirección &nbsp;</span>
                  </div>
                </div>



                <?php
/*
                <div class="mb-4 row">
                  <label class="col-sm-12 col-form-label">Revisión de niveles de fluidos</label>
                  <div class="col-sm-4 mt-2">
                    <span> <input type="checkbox" name="revF_aceite" <?php if(isset($bit_rev) && $bit_rev->revF_aceite=="1") echo "checked"; ?>> Aceite de motor &nbsp;</span>
                    <br>
                    <span> <input type="checkbox" name="revF_frenos" <?php if(isset($bit_rev) && $bit_rev->revF_frenos=="1") echo "checked"; ?>> Líquido de frenos &nbsp;</span>
                  </div>

                  <div class="col-sm-4 mt-2">
                    <span> <input type="checkbox" name="revF_transmision" <?php if(isset($bit_rev) && $bit_rev->revF_transmision=="1") echo "checked"; ?>> Liquido de transmisión &nbsp;</span>
                    <br>
                    <span> <input type="checkbox" name="revF_anticongelante" <?php if(isset($bit_rev) && $bit_rev->revF_anticongelante=="1") echo "checked"; ?>> Nivel de anticongelante en radiador &nbsp;</span>
                  </div>

                  <div class="col-sm-4 mt-2">
                    <span> <input type="checkbox" name="revF_direccion" <?php if(isset($bit_rev) && $bit_rev->revF_direccion=="1") echo "checked"; ?>> Liquido de dirección &nbsp;</span>
                    <br>
                    <span> <input type="checkbox" name="revF_neumaticos" <?php if(isset($bit_rev) && $bit_rev->revF_neumaticos=="1") echo "checked"; ?>> Presión de aire de neumáticos &nbsp;</span>
                  </div>
                </div>

              */
              ?>



                <div class="mb-4 row">
                  <label class="col-sm-12 col-form-label">Revisión de luces</label>
                  <div class="col-sm-4 mt-2">
                    <span> <input type="checkbox" name="revL_cortas" <?php if(isset($bit_rev) && $bit_rev->revL_cortas=="1") echo "checked"; ?>> Luces cortas “cuartos” (traseras y delanteras) &nbsp; </span>
                    <br>
                    <span> <input type="checkbox" name="revL_direc" <?php if(isset($bit_rev) && $bit_rev->revL_direc=="1") echo "checked"; ?>> Direccionales e intermitentes &nbsp; </span>
                  </div>

                  <div class="col-sm-4 mt-2">
                    <span> <input type="checkbox" name="revL_tablero" <?php if(isset($bit_rev) && $bit_rev->revL_tablero=="1") echo "checked"; ?>> Luces del tablero &nbsp; </span>
                    <br>
                    <span> <input type="checkbox" name="revL_reversa" <?php if(isset($bit_rev) && $bit_rev->revL_reversa=="1") echo "checked"; ?>> Reversa &nbsp; </span>
                  </div>
                  
                  <div class="col-sm-4 mt-2">
                    <span> <input type="checkbox" name="revL_largas" <?php if(isset($bit_rev) && $bit_rev->revL_largas=="1") echo "checked"; ?>> Luces largas (altas y bajas) &nbsp; </span>
                    <br>
                    <span> <input type="checkbox" name="revL_frenos" <?php if(isset($bit_rev) && $bit_rev->revL_frenos=="1") echo "checked"; ?>> Rev. Frenos &nbsp; </span>
                  </div>
              </div>

              <!-------------------------------------------------------------------------->
              <div class="col-md-12 mt-5">
                  <h4 class="form-section">Días</h4>

                  <hr>
                  <table id="table_dias" width="100%" class="table table-striped">
                    <tbody id="tbody_dias">

                      <tr id="tr_dias" class="tr_dias">
                        <td width="10%">
                          <input type="hidden" id="id" value="0">
                          <div class="form-group">
                            <br>
                            <h3>Día:</h3>
                          </div>
                        </td>

                        <td width="80%">
                          <div class="form-group">
                            <label class="col-form-label">Fecha</label>
                            <div class="controls">
                              <input type="date" id="fecha" class="form-control form-control-sm">
                            </div>
                          </div>
                        </td>

                        <td width="10%" colspan="2">
                          <br>
                          <span style="color: transparent;">add</span>
                          <button type="button" class="btn btn-primary" title="Agregar día" onclick="addDiaBitacora()"><i id="btn_plus" class="fa fa-plus" aria-hidden="true"></i></button>
                        </td>
                      </tr>

                      <!---horas-->
                      <tr id="tr_dias_space"></tr>
                      <tr id="tr_dias_horas" class="tr_horas">
                        <td colspan="4">
                          <table id="table_horas" width="100%">
                            <tbody id="tbody_dia_horas_0">

                              <tr id="tr_dias">
                                <td width="10%"></td>

                                <td width="30%">
                                  <input type="hidden" id="id" value="0">
                                  <div class="form-group">
                                    <label class="col-form-label">Tipo</label>
                                    <div class="controls">
                                      <select class="form-control" id="tipo">
                                        <option value="1">Fuera de servicio</option>
                                        <option value="2">Dormido</option>
                                        <option value="3">Manejando</option>
                                        <option value="4">Sin manejar</option>
                                      </select>
                                    </div>
                                  </div>
                                </td>

                                <td width="20%">
                                  <div class="form-group">
                                    <label class="col-form-label">Hora Inicio</label>
                                    <div class="controls">
                                      <input type="time" id="hora_ini" onchange="checkTime(this,1)" class="form-control form-control-sm" value="">
                                    </div>
                                  </div>
                                </td>

                                <td width="20%">
                                  <div class="form-group">
                                    <label class="col-form-label">Hora Final</label>
                                    <div class="controls">
                                      <input type="time" id="hora_fin" onchange="checkTime(this,2)" class="form-control form-control-sm" value="">
                                    </div>
                                  </div>
                                </td>

                                <td width="10%">
                                  <span style="color: transparent;">add</span>
                                  <button type="button" class="btn btn-primary" title="Agregar horas" onclick="addHoraBitacora()"><i id="btn_plus" class="fa fa-plus" aria-hidden="true"></i></button>
                                </td>

                                <td width="10%"></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!-------------------------------------------------------------------------->

            </div>
          </div>
      </div>
      </form>
      <div class="card-footer">
        <div class="col-sm-9">
          <button class="btn btn-primary" type="button" onclick="add_form()">Guardar datos</button>
          <a href="<?php echo base_url() ?>Contratos" class="btn btn-light">Regresar</a>
        </div>
      </div>
      <!---->
    </div>
  </div>
</div>
</div>