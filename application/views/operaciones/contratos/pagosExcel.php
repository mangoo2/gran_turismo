<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=pagos_contrato".$id."_".date('YmdGis').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th scope="col">Cliente:</th>
            <td><?php echo $cont->cliente; ?></td>
        </tr>
        <tr>
            <th scope="col">Contrato:</th>
            <td><?php echo $id; ?></td>
        </tr>
        <tr>
            <th scope="col">Fecha Contrato:</th>
            <td><?php echo $cont->fecha_contrato; ?></td>
        </tr>
        <tr>
            <th scope="col">Lugar Origen:</th>
            <td><?php echo $cont->lugar_origen; ?></td>
        </tr>
        <tr>
            <th scope="col">Monto contrato:</th>
            <td><?php echo number_format($cont->tot_unids,2); ?></td>
        </tr>
        <tr>
            <th scope="col">Monto anticipo:</th>
            <td><?php echo number_format($cont->monto_anticipo,2); ?></td>
        </tr>
        <tr>
            <th scope="col">Monto descuento:</th>
            <td><?php echo number_format($cont->porc_desc,2); ?></td>
        </tr>
        <tr>
            <th scope="col">Monto restante:</th>
            <td><?php echo number_format($cont->tot_unids-$cont->monto_anticipo-$cont->porc_desc,2); ?></td>
        </tr>
    </thead>
</table>
<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th scope="col"></th>
        </tr>
    </thead>
</table>
<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr style="text-align: center;"><th colspan="5">PAGOS REALIZADOS AL CONTRATO <?php echo $id; ?></th></tr>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Referencia</th>
            <th scope="col">Fecha</th>
            <th scope="col">Cuenta</th>
            <th scope="col">Monto</th>
        </tr>
    </thead>
    <tbody>
    	<?php $i=0; $total=0;
        foreach ($pago as $p) {
            $i++;
            $dia = date("Y-m-d", strtotime($p->fecha));
            $dia_num = date("d", strtotime($p->fecha));
            $mes = date("m", strtotime($p->fecha));
            $anio = date("Y", strtotime($p->fecha));
            $total=$total+$p->monto;
            echo '
            <tr>
              <td >'.$i.'</td>
              <td >'.$p->referencia.'</td>
              <td >'.saber_dia($dia).' '.$dia_num.' de '.nameMes($mes).' de '.$anio.'</td>
              <td >'.$p->cuenta.'</td>
              <td style="text-align:center;">'.number_format($p->monto,2).'</td>
              
            </tr>';
        }
        ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="3"></td>
            <td style="text-align:right; font-weight: bold;">Total:</td>
            <td style="text-align:center; font-weight: bold;"><?php echo number_format($total,2); ?></td>
        </tr>
    </tfoot>
</table>
<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr><th colspan="2"></th></tr>
        <tr>
            <td width="85%" style="text-align:right; font-weight: bold;">Monto restante:</td>
            <td width="15%" style="text-align:center; font-weight: bold;"><?php echo number_format($cont->tot_unids-$cont->monto_anticipo-$cont->porc_desc,2); ?></td>
        </tr>
        <tr>
            <td width="85%" style="text-align:right; font-weight: bold;">Total pagos:</td>
            <td width="15%" style="text-align:center; font-weight: bold;"><?php echo number_format($total,2); ?></td>
        </tr>
        <tr>
            <td width="85%" style="text-align:right; font-weight: bold;">Restante final:</td>
            <td width="15%" style="text-align:center; font-weight: bold;"><?php echo number_format(($cont->tot_unids-$cont->monto_anticipo-$cont->porc_desc)-$total,2); ?></td>
        </tr>
    </thead>
</table>
<?php 
function nameMes($m){
    $mes="Enero";
    switch ($m) {
      case 2: $mes="Febrero"; break;
      case 3: $mes="Marzo"; break;
      case 4: $mes="Abril"; break;
      case 5: $mes="Mayo"; break;
      case 6: $mes="Junio"; break;
      case 7: $mes="Julio"; break;
      case 8: $mes="Agosto"; break;
      case 9: $mes="Septiembre"; break;
      case 10: $mes="Octubre"; break;
      case 11: $mes="Noviembre"; break;
      case 12: $mes="Diciembre"; break;
    }
    return $mes;
  }

function saber_dia($name) {
    $dias = array('','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo');
    $fecha = $dias[date('N', strtotime($name))];
    return $fecha;
}
?>