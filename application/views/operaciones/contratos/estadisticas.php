<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=estadisticas_contrato".$id.".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th scope="col">Cliente:</th>
            <td><?php echo $cont->cliente; ?></td>
        </tr>
        <tr>
            <th scope="col">Contrato:</th>
            <td><?php echo $id; ?></td>
        </tr>
        <tr>
            <th scope="col">Folio:</th>
            <td><?php echo $cont->folio; ?></td>    
        </tr>
        <tr>
            <th scope="col">Fecha Contrato:</th>
            <td><?php echo $cont->fecha_contrato; ?></td>
        </tr>
        <tr>
            <th scope="col">Lugar Origen:</th>
            <td><?php echo $cont->lugar_origen; ?></td>
        </tr>
        <tr>
            <th scope="col">Monto contrato:</th>
            <td><?php echo number_format($cont->tot_unids,2); ?></td>
        </tr>
        <tr>
            <th scope="col">Monto anticipo:</th>
            <td><?php echo number_format($cont->monto_anticipo,2); ?></td>
        </tr>
        <tr>
            <th scope="col">Monto descuento:</th>
            <td><?php echo number_format($cont->porc_desc,2); ?></td>
        </tr>
        <tr>
            <th scope="col">Monto restante:</th>
            <td><?php echo number_format($cont->tot_unids-$cont->monto_anticipo-$cont->porc_desc,2); ?></td>
        </tr>
    </thead>
</table>
<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th scope="col"></th>
        </tr>
    </thead>
</table>
<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr style="text-align: center;"><th colspan="5">PAGOS REALIZADOS AL CONTRATO <?php echo $id; ?></th></tr>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Referencia</th>
            <th scope="col">Fecha de Pago</th>
            <th scope="col">Cuenta</th>
            <th scope="col">Monto</th>
        </tr>
    </thead>
    <tbody>
        <?php $i=0; $total=0;
        foreach ($pago as $p) {
            $i++;
            $dia = date("Y-m-d", strtotime($p->fecha));
            $dia_num = date("d", strtotime($p->fecha));
            $mes = date("m", strtotime($p->fecha));
            $anio = date("Y", strtotime($p->fecha));
            $total=$total+$p->monto;
            echo '
            <tr>
              <td >'.$i.'</td>
              <td >'.$p->referencia.'</td>
              <td >'.saber_dia($dia).' '.$dia_num.' de '.nameMes($mes).' de '.$anio.'</td>
              <td >'.$p->cuenta.'</td>
              <td style="text-align:center;">'.number_format($p->monto,2).'</td>
              
            </tr>';
        }
        ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="3"></td>
            <td style="text-align:right; font-weight: bold;">Total:</td>
            <td style="text-align:center; font-weight: bold;"><?php echo number_format($total,2); ?></td>
        </tr>
    </tfoot>
</table>
<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr><th colspan="2"></th></tr>
        <tr>
            <td width="85%" style="text-align:right; font-weight: bold;">Monto restante:</td>
            <td width="15%" style="text-align:center; font-weight: bold;"><?php echo number_format($cont->tot_unids-$cont->monto_anticipo-$cont->porc_desc,2); ?></td>
        </tr>
        <tr>
            <td width="85%" style="text-align:right; font-weight: bold;">Total pagos:</td>
            <td width="15%" style="text-align:center; font-weight: bold;"><?php echo number_format($total,2); ?></td>
        </tr>
        <tr>
            <td width="85%" style="text-align:right; font-weight: bold;">Restante final:</td>
            <td width="15%" style="text-align:center; font-weight: bold;"><?php echo number_format(($cont->tot_unids-$cont->monto_anticipo-$cont->porc_desc)-$total,2); ?></td>
        </tr>
    </thead>
</table>
<br>
<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <!--<th scope="col">Cliente</th>
            <th scope="col">Tipo Cliente</th>
            <th scope="col">Fecha registro</th>
            <th scope="col">Lugar origen</th>-->
            <th nowrap>Unidad(es)</th>
            <th nowrap>Cantidad</th>
            <th nowrap>Destino</th>
            <th nowrap>Fecha salida</th>
            <th nowrap>Hora salida</th>
            <th nowrap>Fecha regreso</th>
            <th nowrap>Hora regreso</th>
            <th nowrap>No. de días</th>
            <th nowrap>Teléfono</th>
            <th nowrap>Email</th>
            <th nowrap>Total</th>
            <th nowrap>Vendedor</th>
            <th nowrap>Prioridad</th>
            <th nowrap>Estatus</th>
            <th nowrap>Operador Asignado</th>
            <th nowrap>Obervaciones</th>
        </tr>
    </thead>
    <tbody>
    <?php
        $unidad=""; $cant_uni=""; $tipo="";
        $unid = $this->ModeloContratos->getUnidadesEstadisticas($id);
        
        /*if($cont->tipo_cliente=="1"){
            $tipo="Potencial";
        }else if($cont->tipo_cliente=="2"){
            $tipo="Ocacional";
        }else if($cont->tipo_cliente=="3"){
            $tipo="Estandar";
        }else if($cont->tipo_cliente=="4"){
            $tipo="Bronce";
        }else if($cont->tipo_cliente=="5"){
            $tipo="Oro";
        }else if($cont->tipo_cliente=="6"){
            $tipo="Diamante";
        }*/

        //---------
        $fecha_s = $cont->fecha_salida;
        $hora_s = $cont->hora_salida;
        $sal = new DateTime($fecha_s);
        $fecha_r = $cont->fecha_regreso;
        $hora_r = $cont->hora_regreso;
        $reg = new DateTime($fecha_r);

        $diferencia = $sal->diff($reg);
        if ($diferencia->format('%a') == 0) {
            $dias = 1;
        } else {
            $dias = intval($diferencia->format('%a'));
        }
        //---------

        $prioridad="";
        if($cont->prioridad=="1"){
            $prioridad="<span class='btn btn-danger'>Alta</span>";
        }else if($cont->prioridad=="2"){
            $prioridad="<span class='btn btn-warning'>Media</span>";
        }else if($cont->prioridad=="3"){
            $prioridad="<span class='btn btn-info'>Baja</span>";
        }

        $seguimiento="";
        if($cont->seguimiento=="1"){
            $seguimiento="<span class='btn btn-light'>Creada</span>";
        }else if($cont->seguimiento=="2"){
            $seguimiento="<span class='btn btn-info'>En proceso</span>";
        }else if($cont->seguimiento=="3"){
            $seguimiento="<span class='btn btn-warning'>Revisada</span>";
        }else if($cont->seguimiento=="4"){
            $seguimiento="<span class='btn btn-success'>Autorizada</span>";
        }else if($cont->seguimiento=="5"){
            $seguimiento="<span class='btn btn-secondary'>Expirada</span>";
        }else if($cont->seguimiento=="6"){
            $seguimiento="<span class='btn btn-danger'>Rechazada</span>";
        }else if($cont->seguimiento=="7"){
            $seguimiento="<span class='btn btn-primary'>Enviada</span>";
        }

        
        foreach($unid as $u){
            //--------------------------------------------------------------------------
            $dest = $this->ModeloGeneral->getselectwhere2('destino_prospecto',array('id_contrato'=>$id, 'id_unidPros'=>$u->id, "estatus"=>1));
            $choferes = $this->ModeloContratos->getChoferesUnidadEstadisticas($id, $u->unidad);

            $destinoTab = '<table class="table" border="1" width="100%"><tbody>';
            $operadores = '';

            foreach($dest->result() as $d){
                $destinoTab .= '<tr><td nowrap>'.$d->lugar.'</td></tr>';
            }
            
            $destinoTab .= '</tbody></table>';

            foreach($choferes->result() as $in => $ch){
            $operadores .= ($in + 1).') '.$ch->nombre.' '.$ch->apellido_p.' '.$ch->apellido_m.'<br>';
            }
            
//--------------------------------------------------------------------------

            echo '
            <tr style="vertical-align: middle;">
            <td nowrap>'.$u->vehiculo.'</td>
            
            <td style="text-align: center;">'.$u->cantidad.'</td>

            <td>'.$destinoTab.'</td>

            <td>'.$fecha_s.'</td>

            <td>'.$hora_s.'</td>

            <td>'.$fecha_r.'</td>

            <td>'.$hora_r.'</td>

            <td>'.$dias.'</td>

            <td>'.$cont->telefono.'</td>

            <td>'.$cont->correo.'</td>

            <td>$'.number_format($cont->tot_unids,2).'</td>

            <td>'.$cont->vendedor.'</td>

            <td>'.$prioridad.'</td>

            <td>'.$seguimiento.'</td>

            <td nowrap>'.$operadores.'</td>

            <td>'.$cont->observaciones.'</td>
            </tr>';
        }
        
    ?>
    </tbody>
</table>

<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th scope="col"></th>
        </tr>
    </thead>
</table>
<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th colspan="6">GASTOS REGISTRADOS AL CONTRATO</th>
        </tr>
        <tr>
            <th scope="col">Núm Eco.</th>
            <th scope="col">Unidad</th>
            <th scope="col">Tipo de Gasto</th>
            <th scope="col">Fecha</th>
            <th scope="col">Monto</th>
            <th scope="col">Descripción</th>
            <!--<th scope="col">Observaciones</th>-->
        </tr>
    </thead>
    <tbody>
        <?php $tipo_gasto=""; $total_gastos=0;
        foreach($gastos->result() as $key => $g){
            $vehiculo = $g->vehiculo != null ? $g->vehiculo : "---SIN UNIDAD---";
            $num_eco = $g->num_eco != null ? $g->num_eco : "---";

            if($g->tipo=="1"){
                $tipo_gasto="Caseta(s)";
            }else if($g->tipo=="2"){
                $tipo_gasto="Combustible";
            }else if($g->tipo=="3"){
                $tipo_gasto="Otros";
            }else if($g->tipo=="4"){
                $tipo_gasto="Saldos";
            }

            echo '
            <tr>
                <td >'.$num_eco.'</td>
                <td >'.$vehiculo.'</td>
                <td >'.$tipo_gasto.'</td>
                <td >'.$g->fecha.'</td>
                <td >'.number_format($g->importe,2).'</td>
                <td >'.$g->descripcion.'</td>
                <!--<td >'.$g->observaciones.'</td>-->
            </tr>';
            $total_gastos=$total_gastos+$g->importe;
        }
        
        ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="1"></td>
            <td><b>Total:</b></td>
            <td colspan="2"></td>
            <td><b><?php echo number_format($total_gastos,2); ?></b></td>
            <td></td>
        </tr>
    </tfoot>
</table>

<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th scope="col"></th>
        </tr>
    </thead>
</table>
<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th colspan="5">DATOS DE KILOMETRAJE</th>
        </tr>
        <tr>
            <th scope="col">Núm Eco.</th>
            <th scope="col">Unidad</th>
            <th scope="col">KM Inicial</th>
            <th scope="col">KM Final</th>
            <th scope="col">KMS Recorridos</th>
        </tr>
    </thead>
    <tbody>
        <?php $kmIni=0; $kmFin=0; $kmTotal=0;
        if(isset($kms)){
            foreach($kms as $km){
                $vehiculo = $km->vehiculo != null ? $km->vehiculo : "---SIN UNIDAD---";
                $num_eco = $km->num_eco != null ? $km->num_eco : "---";

                $kmsIni = $km->km_ini != null ? $km->km_ini : 0 ;
                $kmsFin = $km->km_fin != null ? $km->km_fin : 0 ;
                $kmsTotal = $km->km_fin - $km->km_ini;

                echo '
                <tr>
                    <td >'.($num_eco).'</td>
                    <td >'.($vehiculo).'</td>
                    <td >'.($kmsIni).'</td>
                    <td >'.($kmsFin).'</td>
                    <td >'.($kmsTotal).'</td>
                </tr>';

            }
        }else{
            echo '
            <tr>
                <td> --- </td>
                <td> --- </td>
                <td> --- </td>
                <td> --- </td>
                <td> --- </td>
            </tr>';
        }
        
        ?>
    </tbody>
</table>

<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th scope="col"></th>
        </tr>
    </thead>
</table>
<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th colspan="3">UTILIDAD DE CONTRATO</th>
        </tr>
        <tr>
            <th scope="col">Total de Contrato</th>
            <th scope="col">Total de Gastos</th>
            <th scope="col">Utilidad</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        echo '
        <tr>
          <td >'.number_format($cont->tot_unids,2).'</td>
          <td >'.number_format($total_gastos,2).'</td>
          <td >'.number_format($cont->tot_unids-$total_gastos,2).'</td>
        </tr>';
        ?>
    </tbody>
</table>
<?php 
function nameMes($m){
    $mes="Enero";
    switch ($m) {
      case 2: $mes="Febrero"; break;
      case 3: $mes="Marzo"; break;
      case 4: $mes="Abril"; break;
      case 5: $mes="Mayo"; break;
      case 6: $mes="Junio"; break;
      case 7: $mes="Julio"; break;
      case 8: $mes="Agosto"; break;
      case 9: $mes="Septiembre"; break;
      case 10: $mes="Octubre"; break;
      case 11: $mes="Noviembre"; break;
      case 12: $mes="Diciembre"; break;
    }
    return $mes;
  }

function saber_dia($name) {
    $dias = array('','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo');
    $fecha = $dias[date('N', strtotime($name))];
    return $fecha;
}
?>