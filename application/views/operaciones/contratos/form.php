<style type="text/css">
	.kv-file-upload, .fileinput-remove-button {
		display: none;
	}
</style>

<!-- ------------------ -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/easyui/css/easyui.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/easyui/css/icon.css">
<!-- ------------------ -->

<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-12">
        <?php if(!isset($c) || $c->id_cotizacion==0){ ?>
          <h3><?php echo $tittle; ?> de contrato</h3>
        <?php } else{ echo "<h3>".$tittle." contrato <br>Cotización: ".$c->id_cotizacion."</h3>"; } ?>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <!---->
        <form class="form" method="post" role="form" id="form_data">
          <input type="hidden" name="id" id="idContrato" value="<?php if(isset($c)) echo $c->id; else echo "0"; ?>">
          <div class="card-body">
            <div class="row">
              <div class="col">

                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Cliente</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="idCliente" id="idCliente">
                      <?php if(isset($c) && $c->idCliente != 0){
                        echo '<option value="'.$c->idCliente.'">'.$clientes->nombre." ".$clientes->app." ".$clientes->apm.'</option>';
                      } ?>; 
                    </select>
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Folio de contrato</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" id="folio" name="folio" value="<?php if(isset($c)) echo $c->folio; ?>">
                  </div>
                </div>

                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Lugar origen</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" id="origen" name="lugar_origen" value="<?php if(isset($c)) echo $c->lugar_origen; ?>">
                  </div>

                  <label class="col-sm-2 col-form-label">Personal encargado</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="personal" id="personal">
                      <?php foreach ($personal->result() as $p) {
                        if(isset($c) && $p->personalId == $c->personal)
                          $sel="selected";
                        else
                          $sel="";

                        echo '<option '.$sel.' value="'.$p->personalId.'">'.$p->nombre.' '.$p->apellido_p.' '.$p->apellido_m.'</option>';
                      } ?>
                    </select>
                  </div>
                </div>

                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Empresa</label>
                    <div class="col-sm-4">
                      <input class="form-control" type="text" id="empresa" name="empresa" value="<?php if(isset($c)) echo $c->empresa; ?>">
                    </div>
                
                  <label class="col-sm-2 col-form-label">Estado</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="estado" id="estado">
                      <?php foreach ($estados->result() as $e) {
                        if(isset($c) && $e->EstadoId == $c->estado)
                          $sel="selected";
                        else
                          $sel="";

                        echo '<option '.$sel.' value="'.$e->EstadoId.'">'.$e->Nombre.'</option>';
                      } ?>
                    </select>
                  </div>
                </div>
                
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Fecha salida</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="date" id="f_salida" name="fecha_salida" value="<?php if(isset($c)) echo $c->fecha_salida; ?>">
                  </div>

                  <label class="col-sm-2 col-form-label">Hora salida</label>
                  <div class="col-sm-4">
                    <!--<input class="form-control" type="time" id="h_salida" name="hora_salida" value="<?php if(isset($c)) echo $c->hora_salida; ?>">-->
                    <input type="text" id="h_salida" name="hora_salida" value="<?php if(isset($c)) echo $c->hora_salida;?>" style="width:100%" class="easyui-timepicker" data-options="closeText:'Cerrar',okText:'Aceptar',hour24:true">
                  </div>
                </div>

                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Fecha regreso</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="date" id="f_regreso" name="fecha_regreso" value="<?php if(isset($c)) echo $c->fecha_regreso; ?>">
                  </div>

                  <label class="col-sm-2 col-form-label">Hora regreso</label>
                  <div class="col-sm-4">
                    <!--<input class="form-control" type="time" id="h_regreso" name="hora_regreso" value="<?php if(isset($c)) echo $c->hora_regreso; ?>">-->
                    <input type="text" id="h_regreso" name="hora_regreso" style="width:100%" value="<?php if(isset($c)) echo $c->hora_regreso; ?>" class="easyui-timepicker" data-options="closeText:'Cerrar',okText:'Aceptar',hour24:true">
                  </div>
                </div>



                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Fecha de contrato</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="date" name="fecha_contrato" value="<?php if(isset($c)) echo $c->fecha_contrato; ?>">
                  </div>
                  <label class="col-sm-2 col-form-label">Priodidad</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="prioridad" id="prioridad">
                      <option <?php if(isset($c) && $c->prioridad=="1") echo "selected"; ?> value="1">Alta</option>
                      <option <?php if(isset($c) && $c->prioridad=="2") echo "selected"; ?> value="2">Media</option>
                      <option <?php if(isset($c) && $c->prioridad=="3") echo "selected"; ?> value="3">Baja</option>
                    </select>
                  </div>
                </div>
                

                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Seguimiento</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="seguimiento" id="seguimiento">
                      <option <?php if(isset($c) && $c->seguimiento=="1") echo "selected"; ?> value="1">Creada</option>
                      <option <?php if(isset($c) && $c->seguimiento=="2") echo "selected"; ?> value="2">En proceso</option>
                      <option <?php if(isset($c) && $c->seguimiento=="3") echo "selected"; ?> value="3">Revisada</option>
                      <option <?php if(isset($c) && $c->seguimiento=="4") echo "selected"; ?> value="4">Autorizada</option>
                      <option <?php if(isset($c) && $c->seguimiento=="5") echo "selected"; ?> value="5">Expirada</option>
                      <option <?php if(isset($c) && $c->seguimiento=="6") echo "selected"; ?> value="6">Rechazada</option>
                      <option <?php if(isset($c) && $c->seguimiento=="7") echo "selected"; ?> value="7">Enviada</option>
                    </select>
                  </div>

                  <label class="col-sm-2 col-form-label">Estado destino</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" id="estado_destino" name="estado_destino" value="<?php if(isset($c)) echo $c->estado_destino; ?>">
                  </div>
                </div>

                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Observaciones</label>
                  <div class="col-sm-10">
                    <textarea rows="4" value="<?php if(isset($c)) echo $c->observaciones; ?>" class="form-control" type="text" name="observaciones" id="observaciones"><?php if(isset($c)) echo $c->observaciones; ?></textarea>
                  </div>
                </div>

                <!--
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Fecha salida</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="date" id="f_salida" name="fecha_salida" value="<?php if(isset($c)) echo $c->fecha_salida; ?>">
                  </div>
                  <label class="col-sm-2 col-form-label">Hora salida</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="time" id="h_salida" name="hora_salida" value="<?php if(isset($c)) echo $c->hora_salida; ?>">
                  </div>
                </div>-->
              
                <!-------------------------------------------------------------------------->
                <div class="col-md-12 mt-5">
                  <h4 class="form-section">Datos de Itinerario</h4>

                  <hr>
                  <h5 class="form-section">Nueva Unidad - Destinos</h5>
                  <table id="table_unidades" width="100%" class="table-striped">
                    <tbody id="tbody_unidades">
                      <tr id="tr_unidades" class="tr_unidades">
                        <td width="40%">
                          <input type="hidden" id="id" value="0">
                          <div class="form-group">
                            <label class="col-form-label">Unidad</label>

                            <div class="controls">
                              <select class="form-control form-control-smr" id="unidad">
                                <option disabled selected value="0">Selecciona una opción</option>
                                <?php foreach ($unidades->result() as $u) {
                                  echo '<option data-tipo="'.$u->tipo.'" value="'.$u->id.'">'.$u->vehiculo.'</option>';
                                } ?>
                              </select>
                            </div>
                          </div>
                        </td>

                        <td width="25%">
                          <div class="form-group">
                            <label class="col-form-label">Cantidad</label>
                            <div class="controls">
                              <input readonly type="number" id="cantidad" class="form-control form-control-sm" min="1" max="1" value="1">
                            </div>
                          </div>
                        </td>

                        <td width="25%">
                          <div class="form-group">
                            <label class="col-form-label">Monto unitario $</label>
                            <div class="controls">
                              <input type="text" id="monto" class="form-control form-control-sm" value="">
                            </div>
                          </div>
                        </td>

                        <td width="10%" colspan="2">
                          <span style="color: transparent;">add</span>
                          <button type="button" class="btn btn-primary" title="Agregar nueva unidad" onclick="addUnidadCotiza()"><i id="btn_plus" class="fa fa-plus" aria-hidden="true"></i></button>
                        </td>
                      </tr>

                      <!---destinos-->
                      <tr id="tr_unidades_space"></tr>
                      <tr id="tr_unidades_destinos" class="tr_destinos">
                        <td colspan="4">
                          <table id="table_destinos" width="100%">
                            <tbody id="tbody_unidad_destinos_0">

                              <tr id="tr_unidades">
                                <td width="5%"></td>

                                <td width="80%">
                                  <input type="hidden" id="id" value="0">
                                  <div class="form-group">
                                    <label class="col-form-label">Itinerario</label>
                                    <div class="controls">
                                      <!--<input type="text" id="lugar" class="form-control form-control-smr" value="">-->
                                      <textarea type="text" id="lugar" class="form-control" rows="2" ></textarea>
                                    </div>
                                  </div>
                                </td>
                                
                                <!--
                                <td width="15%">
                                  <div class="form-group">
                                    <label class="col-form-label">Fecha Salida </label>
                                    <div class="controls">
                                      <input type="date" id="fecha" class="form-control form-control-sm" value="">
                                    </div>
                                  </div>
                                </td>

                                <td width="15%">
                                  <div class="form-group">
                                    <label class="col-form-label">Hora Salida</label>
                                    <div class="controls">
                                      <input type="text" id="hora" value="00:00" style="width:100%" class="easyui-timepicker" data-options="closeText:'Cerrar',okText:'Aceptar',hour24:true">
                                    </div>
                                  </div>
                                </td>

                                <td width="15%">
                                  <div class="form-group">
                                    <label class="col-form-label">Fecha Regreso</label>
                                    <div class="controls">
                                      <input type="date" id="fecha_regreso" class="form-control form-control-sm" value="">
                                    </div>
                                  </div>
                                </td>

                                <td width="15%">
                                  <div class="form-group">
                                    <label class="col-form-label">Hora Regreso</label>
                                    <div class="controls">
                                      <input type="text" id="hora_regreso" style="width:100%" value="00:00" class="easyui-timepicker" data-options="closeText:'Cerrar',okText:'Aceptar',hour24:true">
                                    </div>
                                  </div>
                                </td>
                                -->

                                <!--
                                <td width="15%">
                                  <div class="form-group">
                                    <label class="col-form-label">Lugar de hospedaje</label>
                                    <div class="controls">
                                      <input type="text" id="lugar_hospeda" class="form-control form-control-sm" value="">
                                    </div>
                                  </div>
                                </td>
                                -->

                                <td width="10%">
                                  <span style="color: transparent;">add</span>
                                  <button type="button" class="btn btn-primary" title="Agregar nuevo destino" onclick="addDestinoCotiza()"><i id="btn_plus" class="fa fa-plus" aria-hidden="true"></i></button>
                                </td>

                                <td width="5%"></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!-------------------------------------------------------------------------->
                <hr>
                <!-------------------------------------------------------------------------->
                <div class="col-md-12 mt-5">
                  <h4 class="form-section">Datos Extras</h4>
                </div>
                <hr>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Anticipo %</label>
                  <div class="col-sm-2">
                    <input placeholder="%" class="form-control" type="number" name="porc_anticipo" value="<?php if(isset($c)) echo $c->porc_anticipo; ?>">
                  </div>
                  <label class="col-sm-2 col-form-label">Anticipo $</label>
                  <div class="col-sm-2">
                    <input placeholder="$" class="form-control" type="number" name="monto_anticipo" value="<?php if(isset($c)) echo $c->monto_anticipo; ?>">
                  </div>
                  <label class="col-sm-2 col-form-label">Descuento $</label>
                  <div class="col-sm-2">
                    <input placeholder="$" class="form-control" type="number" name="porc_desc" value="<?php if(isset($c)) echo $c->porc_desc; ?>">
                  </div>
                </div>
                <!---->


                <div class="controls" style="display:none;">
                  <select class="form-control form-control-smr" id="sel_unidad">
                    <option disabled selected value="0">Selecciona una opción</option>
                    <?php foreach ($unidades->result() as $u) {
                      echo '<option data-tipo="'.$u->tipo.'" value="'.$u->id.'">'.$u->vehiculo.'</option>';
                      } ?>
                  </select>
                </div>

                
              </div>
            </div>
          </div>
        </form>  
          <div class="card-footer">
            <div class="col-sm-9">
              <button class="btn btn-primary" type="button" onclick="add_form()">Guardar datos</button>
              <a href="<?php echo base_url()?>Contratos" class="btn btn-light">Regresar</a>
            </div>
          </div>
        <!---->
      </div>
    </div>
  </div>
</div>