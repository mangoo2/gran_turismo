<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Listado de cuentas</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-12">
              <a class="btn btn-primary" href="<?php echo base_url() ?>Cuentas/registro">Nueva</a><br><br>
            </div>
            <div class="col-12">
              <div class="table-responsive">
                <table class="table" id="table_data">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Cuenta</th>
                      <th scope="col">CLABE</th>
                      <th scope="col">Banco</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>