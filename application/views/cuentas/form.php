<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3><?php echo $tittle; ?> de cuenta</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <!---->
        <form class="form" method="post" role="form" id="form_data">
          <input type="hidden" name="id" id="id" value="<?php if(isset($c)) echo $c->id; else echo "0"; ?>">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Cuenta</label>
                  <div class="col-sm-10">
                    <input class="form-control" type="text" name="cuenta" value="<?php if(isset($c)) echo $c->cuenta; ?>">
                  </div>
                </div>

                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">CLABE</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" name="clabe" value="<?php if(isset($c)) echo $c->clabe; ?>">
                  </div>
                  <label class="col-sm-2 col-form-label">Banco</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" name="banco" value="<?php if(isset($c)) echo $c->banco; ?>">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>  
          <div class="card-footer">
            <div class="col-sm-9">
              <button class="btn btn-primary" type="button" onclick="add_form()" id="save">Guardar datos</button>
              <a href="<?php echo base_url()?>Cuentas" class="btn btn-light">Regresar</a>
            </div>
          </div>
        <!---->
      </div>
    </div>
  </div>
</div>