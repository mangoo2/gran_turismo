<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3><?php echo $tittle; ?> prospecto</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <!---->
        <form class="form" method="post" role="form" id="form_data">
          <input type="hidden" name="id" id="idcliente" value="<?php if(isset($p)) echo $p->id; else echo "0"; ?>">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Nombre(s)</label>
                  <div class="col-sm-10">
                    <input class="form-control" type="text" name="nombre" value="<?php if(isset($p)) echo $p->nombre; ?>">
                  </div>
                </div>

                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Apellido Paterno</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" name="app" value="<?php if(isset($p)) echo $p->app; ?>">
                  </div>
                  <label class="col-sm-2 col-form-label">Apellido Materno</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" name="apm" value="<?php if(isset($p)) echo $p->apm; ?>">
                  </div>
                </div>

                <!--
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Lugar origen</label>
                  <div class="col-sm-10">
                    <input class="form-control" type="text" name="lugar_origen" value="<?php if(isset($p)) echo $p->lugar_origen; ?>">
                  </div>
                </div>
                -->

                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Empresa</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" name="empresa" value="<?php if(isset($p)) echo $p->empresa; ?>">
                  </div>

                  <label class="col-sm-2 col-form-label">Estado</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="estado" id="estado">
                      <?php foreach ($estados->result() as $e) {
                        if(isset($p) && $e->EstadoId == $p->estado)
                          $sel="selected";
                        else
                          $sel="";

                        echo '<option '.$sel.' value="'.$e->EstadoId.'">'.$e->Nombre.'</option>';
                      } ?>
                    </select>
                  </div>
                </div>

                <!--
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Fecha salida</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="date" name="fecha_salida" value="<?php if(isset($p)) echo $p->fecha_salida; ?>">
                  </div>
                  <label class="col-sm-2 col-form-label">Hora salida</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="time" name="hora_salida" value="<?php if(isset($p)) echo $p->hora_salida; ?>">
                  </div>
                </div>
                -->


                <!-------------------------------------------------------------------------->
                <!--
                <div class="col-md-12 mt-5">
                  <h4 class="form-section">Datos de Itinerario</h4>

                  <hr>
                  <h5 class="form-section">Nueva Unidad - Destinos</h5>
                  <table id="table_unidades" width="100%" class="table table-striped">
                    <tbody id="tbody_unidades" >

                      <tr id="tr_unidades" class="tr_unidades">
                        <td width="40%">
                          <input type="hidden" id="id" value="0">
                          <div class="form-group">
                            <label class="col-form-label">Unidad</label>

                            <div class="controls">
                              <select class="form-control form-control-smr" id="unidad">
                                <option disabled selected value="0">Selecciona una opción</option>
                                <?php foreach ($unidades->result() as $u) {
                                  echo '<option data-tipo="'.$u->tipo.'" value="'.$u->id.'">'.$u->vehiculo.'</option>';
                                } ?>
                              </select>
                            </div>
                          </div>
                        </td>

                        <td width="25%">
                          <div class="form-group">
                            <label class="col-form-label">Cantidad</label>
                            <div class="controls">
                              <input type="number" id="cantidad" class="form-control form-control-sm" min="0">
                            </div>
                          </div>
                        </td>

                        <td width="25%">
                          <div class="form-group">
                            <label class="col-form-label">Monto unitario $</label>
                            <div class="controls">
                              <input type="text" id="monto" class="form-control form-control-sm" value="">
                            </div>
                          </div>
                        </td>

                        <td width="10%" colspan="2">
                          <span style="color: transparent;">add</span>
                          <button type="button" class="btn btn-primary" title="Agregar nueva unidad" onclick="addUnidadCotiza()"><i id="btn_plus" class="fa fa-plus" aria-hidden="true"></i></button>
                        </td>
                      </tr>
                -->

                      <!---destinos-->
                <!--
                      <tr id="tr_unidades_space"></tr>
                      <tr id="tr_unidades_destinos" class="tr_destinos">
                        <td colspan="4">
                          <table id="table_destinos" width="100%">
                            <tbody id="tbody_unidad_destinos_0">

                              <tr id="tr_unidades">
                                <td width="10%"></td>

                                <td width="20%">
                                  <input type="hidden" id="id" value="0">
                                  <div class="form-group">
                                    <label class="col-form-label">Lugar de destino</label>
                                    <div class="controls">
                                      <input type="text" id="lugar" class="form-control form-control-smr" value="">
                                    </div>
                                  </div>
                                </td>

                                <td width="15%">
                                  <div class="form-group">
                                    <label class="col-form-label">Fecha</label>
                                    <div class="controls">
                                      <input type="date" id="fecha" class="form-control form-control-sm" value="">
                                    </div>
                                  </div>
                                </td>

                                <td width="15%">
                                  <div class="form-group">
                                    <label class="col-form-label">Hora</label>
                                    <div class="controls">
                                      <input type="time" id="hora" class="form-control form-control-sm" value="">
                                    </div>
                                  </div>
                                </td>

                                <td width="20%">
                                  <div class="form-group">
                                    <label class="col-form-label">Lugar de hospedaje</label>
                                    <div class="controls">
                                      <input type="text" id="lugar_hospeda" class="form-control form-control-sm" value="">
                                    </div>
                                  </div>
                                </td>

                                <td width="10%">
                                  <span style="color: transparent;">add</span>
                                  <button type="button" class="btn btn-primary" title="Agregar nuevo destino" onclick="addDestinoCotiza()"><i id="btn_plus" class="fa fa-plus" aria-hidden="true"></i></button>
                                </td>

                                <td width="10%"></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                -->
                <!-------------------------------------------------------------------->
                

                <!--
                <hr>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Fecha regreso</label>
                  <div class="col-sm-3">
                    <input class="form-control" type="date" name="fecha_regreso" value="<?php if(isset($p)) echo $p->fecha_regreso; ?>">
                  </div>
                  <label class="col-sm-2 col-form-label">Hora regreso</label>
                  <div class="col-sm-3">
                    <input class="form-control" type="time" name="hora_regreso" value="<?php if(isset($p)) echo $p->hora_regreso; ?>">
                  </div>
                </div>
                -->

                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Teléfono</label>
                  <div class="col-sm-4">
                    <input class="form-control" maxlength="12" type="text" pattern="\d*" name="telefono" value="<?php if(isset($p)) echo $p->telefono; ?>">
                  </div>
                  <label class="col-sm-2 col-form-label">Correo</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="email" name="correo" value="<?php if(isset($p)) echo $p->correo; ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Seleccione Medio</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="medio_conoce" id="medio_conoce">
                      <option <?php if(isset($p) && $p->medio_conoce=="1") echo "selected"; ?> value="1">Recomendación</option>
                      <option <?php if(isset($p) && $p->medio_conoce=="2") echo "selected"; ?> value="2">Whatsapp</option>
                      <option <?php if(isset($p) && $p->medio_conoce=="3") echo "selected"; ?> value="3">Facebook</option>
                      <option <?php if(isset($p) && $p->medio_conoce=="4") echo "selected"; ?> value="4">Instagram</option>
                      <option <?php if(isset($p) && $p->medio_conoce=="5") echo "selected"; ?> value="5">Telefónica</option>
                      <option <?php if(isset($p) && $p->medio_conoce=="6") echo "selected"; ?> value="6">Personal</option>
                      <option <?php if(isset($p) && $p->medio_conoce=="7") echo "selected"; ?> value="7">Email</option>
                      <option <?php if(isset($p) && $p->medio_conoce=="8") echo "selected"; ?> value="8">Otro</option>
                    </select>
                  </div>

                  <label class="col-sm-2 col-form-label">Tipo de cliente</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="tipo_cliente" id="tipo_cliente">
                      <option <?php if(isset($p) && $p->tipo_cliente=="1") echo "selected"; ?> value="1">Potencial</option>
                      <option <?php if(isset($p) && $p->tipo_cliente=="2") echo "selected"; ?> value="2">Ocacional</option>
                      <option <?php if(isset($p) && $p->tipo_cliente=="3") echo "selected"; ?> value="3">Estandar</option>
                      <option <?php if(isset($p) && $p->tipo_cliente=="4") echo "selected"; ?> value="4">Bronce</option>
                      <option <?php if(isset($p) && $p->tipo_cliente=="5") echo "selected"; ?> value="5">Oro</option>
                      <option <?php if(isset($p) && $p->tipo_cliente=="6") echo "selected"; ?> value="6">Diamante</option>
                    </select>
                  </div>
                </div>

                <div class="controls" style="display:none;">
                  <select class="form-control form-control-smr" id="sel_unidad">
                    <option disabled selected value="0">Selecciona una opción</option>
                    <?php foreach ($unidades->result() as $u) {
                      echo '<option data-tipo="'.$u->tipo.'" value="'.$u->id.'">'.$u->vehiculo.'</option>';
                      } ?>
                  </select>
                </div>


                <div class="col-md-12 mt-5">
                  <h4 class="form-section">Dirección</h4>
                </div>
                <hr>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Calle y numero</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" name="calle" value="<?php if(isset($p)) echo $p->calle; ?>">
                  </div>

                  <label class="col-sm-2 col-form-label">Colonia</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" name="colonia" value="<?php if(isset($p)) echo $p->colonia; ?>">
                  </div>
                </div>

                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Ciudad o municipio</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" name="ciudad" value="<?php if(isset($p)) echo $p->ciudad; ?>">
                  </div>

                  <label class="col-sm-2 col-form-label">Estado</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="estado" id="estado">
                      <?php foreach ($estados->result() as $e) {
                        if(isset($p) && $e->EstadoId == $p->estado)
                          $sel="selected";
                        else
                          $sel="";

                        echo '<option '.$sel.' value="'.$e->EstadoId.'">'.$e->Nombre.'</option>';
                      } ?>
                      
                    </select>
                  </div>
                </div>

                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Código Postal</label>
                  <div class="col-sm-4">
                    <input class="form-control" pattern='\d*' maxlength="5" type="text" name="cod_postal" value="<?php if(isset($p)) echo $p->cod_postal; ?>">
                  </div>
                </div>

                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Teléfono</label>
                  <div class="col-sm-4">
                    <input class="form-control" maxlength="12" type="text" pattern="\d*" name="telefono" value="<?php if(isset($p)) echo $p->telefono; ?>">
                  </div>
                  <label class="col-sm-2 col-form-label">Correo</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="email" name="correo" value="<?php if(isset($p)) echo $p->correo; ?>">
                  </div>
                </div>

              </div>
            </div>
          </div>
        </form>  
          <div class="card-footer">
            <div class="col-sm-9">
              <button class="btn btn-primary" type="button" onclick="add_form()">Guardar datos</button>
              <a href="<?php echo base_url()?>Prospectos" class="btn btn-light">Regresar</a>
            </div>
          </div>
        <!---->
      </div>
    </div>
  </div>
</div>