<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Listado de Clientes</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-12">
              <a class="btn btn-primary" href="<?php echo base_url() ?>Clientes/registro">Nuevo</a><br><br>
            </div>
            <div class="col-12">
              <div class="table-responsive">
                <table class="table" id="table_data">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Nombre</th>
                      <th scope="col">Teléfono</th>
                      <th scope="col">Email</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal_contrato" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Detalles de contrato</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <!---->
              <form class="form" method="post" role="form" id="form_data">
                <div class="card-body">
                  <div class="mb-3 row">
                    <div class="col-md-6">
                      <input class="form-control" type="hidden" id="id_prospecto" name="id_prospecto">
                    </div>
                    <div class="col-md-6">
                      <input class="form-control" type="hidden" id="id_detalle" name="id_detalle" value="0">
                    </div>
                  </div>

                  <div class="row">
                    <div class="col">
                      <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Calle y numero</label>
                        <div class="col-sm-10">
                          <input class="form-control" type="text" name="calle" id="calle">
                        </div>
                      </div>

                          <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Colonia</label>
                            <div class="col-sm-4">
                              <input class="form-control" type="text" name="colonia" id="colonia">
                            </div>

                            <label class="col-sm-2 col-form-label">Codigo Postal</label>
                            <div class="col-sm-4">
                              <input class="form-control" type="text" pattern="^[0-9]{0,10}$" maxlength="5" minlength="5" name="cp" id="cp">
                            </div>
                          </div>
                          
                          <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Ciudad o municipio</label>
                            <div class="col-sm-4">
                              <input class="form-control" type="text" name="ciudad_municipio" id="ciudad">
                            </div>

                            <label class="col-sm-2 col-form-label">Estado</label>
                            <div class="col-sm-4">
                              <select class="form-control" name="estado" id="estado">
                                <?php foreach ($estados->result() as $e) {
                                  echo '<option value="'.$e->EstadoId.'">'.$e->Nombre.'</option>';
                                } ?>
                                
                              </select>
                            </div>
                          </div>

                          <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Tipo de servicio</label>
                            <div class="col-sm-4">
                              <select class="form-control" name="tipo_servicio" id="tipo_servicio">
                                <option value="1">Turistico</option>
                              </select>
                            </div>

                            <!--
                            <label class="col-sm-2 col-form-label">Tipo de unidad</label>
                            <div class="col-sm-4">
                              <select class="form-control" name="tipo_unidad" id="unidad">
                              </select>
                            </div>
                            -->
                          </div>
                          
                          <!---
                          <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label"># de unidades</label>
                            <div class="col-sm-2">
                              <input class="form-control" type="text" pattern="\d*" name="cantidad_unidades" id="cantidad_unidades">
                            </div>

                            <label class="col-sm-2 col-form-label"># de pasajeros por unidad</label>
                            <div class="col-sm-2">
                              <input class="form-control" type="text" pattern="\d*" name="num_pasajeros" id="num_pasajeros">
                            </div>

                            <label class="col-sm-2 col-form-label">Importe por unidad</label>
                            <div class="col-sm-2">
                              <input class="form-control" type="text" pattern="\d*" name="importe_unidad" id="importe_unidad">
                            </div>
                          </div>
                          --->

                          <!----- UNIDADES ----->

                          <div class="col-md-12 mt-3">
                            <hr>
                            <h4 class="form-section">Datos de Unidades</h4>
                            <table id="table_unidades" width="100%">
                              <tbody id="tbody_unidades">
                                <tr id="tr_unidades">
                                  <td width="30%">
                                    <input type="hidden" id="id" value="0">

                                    <div class="form-group">
                                      <label class="col-form-label"><br>Unidad</label>
                                      <div class="controls">
                                        <select class="form-control form-control-smr" id="tipo_unidad">
                                          <option disabled selected value="0">Selecciona una opción</option>
                                          <?php foreach ($unidades->result() as $u) {
                                            echo '<option value="'.$u->id.'">'.$u->marca.' - '.$u->modelo.' - '.$u->placas.'</option>';
                                          } ?>
                                        </select>
                                      </div>
                                    </div>
                                  </td>
                                  
                                  <td width="20%">
                                    <div class="form-group">
                                      <label class="col-form-label"><br>Cantidad (unidades)</label>
                                      <div class="controls">
                                        <input type="number" id="cantidad" class="form-control form-control-sm" min="0">
                                      </div>
                                    </div>
                                  </td>

                                  <td width="20%">
                                    <div class="form-group">
                                      <label class="col-form-label">Cantidad (pasajeros por unidad)</label>
                                      <div class="controls">
                                        <input type="number" id="cant_pasajeros" class="form-control form-control-sm" min="0">
                                      </div>
                                    </div>
                                  </td>

                                  <td width="20%">
                                    <div class="form-group">
                                      <label class="col-form-label"><br>Importe por unidad $</label>
                                      <div class="controls">
                                        <input type="text" id="monto" class="form-control form-control-sm" value="">
                                      </div>
                                    </div>
                                  </td>

                                  <td width="10%">
                                    <span style="color: transparent;">add</span>
                                    <button type="button" class="btn btn-primary" title="Agregar nueva unidad" onclick="addUnidad()"><i id="btn_plus" class="fa fa-plus" aria-hidden="true"></i></button>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>


                          <!----- UNIDADES ----->
                          
                          <hr>
                          <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Observaciones</label>
                            <div class="col-sm-10">
                              <textarea rows="4" class="form-control" type="text" name="observaciones" id="observaciones"></textarea>
                            </div>
                          </div>

                          
                        </div>
                      </div>
                    </div>
                  </form>
                  <!---->
                </div>
              </div>
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cerrar</button>
                <button class="btn btn-primary" type="button" onclick="add_form()">Guardar</button>
                <!--<button type="button" class="btn btn-outline-primary">Save changes</button>-->
            </div>
        </div>
    </div>
</div>