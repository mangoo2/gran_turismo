<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->helper('url');
$perfilid=$this->session->userdata('perfilid_tz');
if (!isset($perfilid)) {
  $perfil=0;
}else{
  $perfil=$perfilid;   
}

if(!isset($perfilid)){
    ?>
    <script>
        document.location="<?php echo base_url(); ?>Login"
    </script>
    <?php
}

$menu=$this->ModeloSession->menus($perfil); ?>

<div class="page-wrapper compact-wrapper" id="pageWrapper">
  <!-- Page Header Start-->
  <div class="page-main-header">
    <div class="main-header-right row m-0">
      <div class="main-header-left">
        <div class="logo-wrapper"><a href="<?php echo base_url(); ?>"><img width="70px" class="img-fluid" src="<?php echo base_url(); ?>public/img/logo_color.png" alt=""></a></div>
        <div class="dark-logo-wrapper"><a href="<?php echo base_url(); ?>"><img width="70px" class="img-fluid" src="<?php echo base_url(); ?>public/img/logo_color.png" alt=""></a></div>
        <div class="toggle-sidebar"><i class="status_toggle middle" data-feather="align-center" id="sidebar-toggle"></i></div>
      </div>

      <div class="nav-right col pull-right right-menu p-0">
        <ul class="nav-menus">
          <li><a class="text-dark" href="#!" onclick="javascript:toggleFullScreen()"><i data-feather="maximize"></i></a></li>
          
          <li class="onhover-dropdown">
            <?php 
              $notiVigenciaLicChofer = $this->ModeloSession->get_vigencia_lic_chofer();
              $notiVigenciaExaChofer = $this->ModeloSession->get_vigencia_exa_chofer();
              $notiVigenciaPolizaUnidad = $this->ModeloSession->get_vigencia_poliza_unidad();
              $notiVerifUnidad = $this->ModeloSession->get_verificacion_unidad();

              $numNotifications = $notiVigenciaLicChofer->num_rows() + $notiVigenciaExaChofer->num_rows() + $notiVigenciaPolizaUnidad->num_rows() + $notiVerifUnidad->num_rows();
            ?>
            <div class="<?php echo $numNotifications > 0 ? 'notification-box' : '' ?>">
              <i data-feather="bell"></i>
              <?php echo $numNotifications > 0 ? '<span class="dot-animated"></span>' : '' ?>
            </div>
          
            <ul class="notification-dropdown onhover-show-div noti_scroll">
              <li>
                <p class="f-w-700 mb-0 text-dark">Notificaciones  <span class="pull-right badge badge-primary badge-pill"><?php echo $numNotifications; ?></span></p>
              </li>
              <?php foreach ($notiVigenciaLicChofer->result() as $item) { ?>
                <li class="noti-danger">
                  <div class="media" ><span class="notification-bg bg-light-danger"><i data-feather="file-text"> </i></span>
                    <a href="<?php echo base_url().'Choferes/registro/';?><?php echo $item->choferid;?>">
                      <div class="media-body">
                        <p>Licencia por vencer</p>
                        <p><?php echo $item->vigencia_licencia?></p>
                        <span>Chofer: <?php echo $item->nombre." ".$item->apellido_p." ".$item->apellido_m;?></span>
                      </div>
                    </a>
                  </div>
                </li>
              <?php }?> 

              <?php foreach ($notiVigenciaExaChofer->result() as $item) { ?>
                <li class="noti-danger">
                  <div class="media"><span class="notification-bg bg-light-danger"><i data-feather="file-text"> </i></span>
                    <a href="<?php echo base_url().'Choferes/registro/';?><?php echo $item->choferid;?>">
                      <div class="media-body">
                        <p>Examen medico por vencer</p>
                        <p><?php echo $item->vigencia_examen?></p>
                        <span>Chofer: <?php echo $item->nombre." ".$item->apellido_p." ".$item->apellido_m;?></span>
                      </div>
                    </a>
                  </div>
                </li>
              <?php }?> 

              <?php foreach ($notiVigenciaPolizaUnidad->result() as $item) { ?>
                <li class="noti-danger">
                  <div class="media"><span class="notification-bg bg-light-danger"><i data-feather="truck"> </i></span>
                    <a href="<?php echo base_url().'Unidades/registro/';?><?php echo $item->id;?>">
                      <div class="media-body">
                        <p>Póliza de seguro por vencer</p>
                        <p><?php echo $item->vencimiento_poliza?></p>
                        <span>Unidad: <?php echo $item->vehiculo;?></span>
                      </div>
                    </a>
                  </div>
                </li>
              <?php }?> 

              <?php foreach ($notiVerifUnidad->result() as $item) { ?>
                <li class="noti-danger">
                  <div class="media"><span class="notification-bg bg-light-danger"><i data-feather="truck"> </i></span>
                    <a href="<?php echo base_url().'Unidades/registro/';?><?php echo $item->id;?>">
                      <div class="media-body">
                        <p>Verificación cercana</p>
                        <p><?php echo $item->prox_ver?></p>
                        <span>Unidad: <?php echo $item->vehiculo;?></span>
                      </div>
                    </a>
                  </div>
                </li>
              <?php }?> 

              <!--
              <li class="noti-primary">
                <div class="media"><span class="notification-bg bg-light-primary"><i data-feather="activity"> </i></span>
                  <div class="media-body">
                    <p>Delivery processing </p><span>10 minutes ago</span>
                  </div>
                </div>
              </li>
              <li class="noti-secondary">
                <div class="media"><span class="notification-bg bg-light-secondary"><i data-feather="check-circle"> </i></span>
                  <div class="media-body">
                    <p>Order Complete</p><span>1 hour ago</span>
                  </div>
                </div>
              </li>
              <li class="noti-success">
                <div class="media"><span class="notification-bg bg-light-success"><i data-feather="file-text"> </i></span>
                  <div class="media-body">
                    <p>Tickets Generated</p><span>3 hour ago</span>
                  </div>
                </div>
              </li>
              <li class="noti-danger">
                <div class="media"><span class="notification-bg bg-light-danger"><i data-feather="user-check"> </i></span>
                  <div class="media-body">
                    <p>Delivery Complete</p><span>6 hour ago</span>
                  </div>
                </div>
              </li>
              -->
            </ul>
          </li>

          <li class="onhover-dropdown p-0">
            <button class="btn btn-primary-light" type="button"><a href="<?php echo base_url(); ?>Login/exitlogin"><i data-feather="log-out"></i>Salir</a></button>
          </li>
        </ul>
      </div>
      <div class="d-lg-none mobile-toggle pull-right w-auto"><i data-feather="more-horizontal"></i></div>
    </div>
  </div>
  <!-- Page Header Ends -->


  <div class="page-body-wrapper sidebar-icon">
    <!-- Page Sidebar Start-->
    <header class="main-nav">
      <div class="sidebar-user text-center">
        <!--<a class="setting-primary" href="javascript:void(0)"><i data-feather="settings"></i></a>-->
        <img class="img-90 rounded-circle" src="<?php echo base_url(); ?>assets/images/dashboard/1.png" alt="">
        <!--<div class="badge-bottom">
          <span class="badge badge-primary">New</span>
        </div>-->
        <a href="user-profile.html">
          <h6 class="mt-3 f-14 f-w-600">Bienvenido de nuevo</h6>
        </a>
        <p class="mb-0 font-roboto"><?php echo $this->session->userdata('usuario_tz'); ?></p>
        <ul>
          <!--<li><span><span class="counter">19.8</span>k</span>
            <p>Follow</p>
          </li>
          <li><span>2 year</span>
            <p>Experince</p>
          </li>
          <li><span><span class="counter">95.2</span>k</span>
            <p>Follower </p>
          </li>
        </ul>-->
      </div>
      <nav>
        <div class="main-navbar">
          <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
          <div id="mainnav">           
            <ul class="nav-menu custom-scrollbar">
              <li class="back-btn">
                <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
              </li>
              <?php foreach ($menu->result() as $item){ ?>
                <li class="sidebar-main-title">
                  <div>
                    <h6><?php echo $item->Nombre; ?></h6>
                  </div>
                </li>
                <?php 
                $menu =  $item->MenuId;
                $menusub = $this->ModeloSession->submenus($perfil,$menu,1);
                foreach ($menusub->result() as $datos) { ?>
                  <li><a class="nav-link menu-title link-nav" href="<?php echo base_url(); ?><?php echo $datos->Pagina; ?>"><i data-feather="<?php echo $datos->Icon; ?>"></i><span><?php echo $datos->Nombre; ?></span></a></li>
                <?php }
              } ?>
            </ul>
          </div>
          <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
        </div>
      </nav>
    </header>
    <!-- Page Sidebar Ends-->

    <div class="page-body">