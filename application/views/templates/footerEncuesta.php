</div>
    </div>
    <!-- latest jquery-->
    <script src="<?php echo base_url(); ?>assets/js/jquery-3.5.1.min.js"></script>
    <!-- feather icon js-->
    <script src="<?php echo base_url(); ?>assets/js/icons/feather-icon/feather.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/icons/feather-icon/feather-icon.js"></script>
    <!-- Sidebar jquery-->
    <script src="<?php echo base_url(); ?>assets/js/sidebar-menu.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/config.js"></script>
    <!-- Bootstrap js-->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
    <!-- Plugins JS start-->
    <script src="<?php echo base_url(); ?>assets/js/chart/chartist/chartist.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/chart/chartist/chartist-plugin-tooltip.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/chart/knob/knob.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/chart/knob/knob-chart.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/chart/apex-chart/apex-chart.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/chart/apex-chart/stock-prices.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/prism/prism.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/clipboard/clipboard.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/counter/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/counter/jquery.counterup.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/counter/counter-custom.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/custom-card/custom-card.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/notify/bootstrap-notify.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vector-map/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vector-map/map/jquery-jvectormap-world-mill-en.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vector-map/map/jquery-jvectormap-us-aea-en.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vector-map/map/jquery-jvectormap-uk-mill-en.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vector-map/map/jquery-jvectormap-au-mill.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vector-map/map/jquery-jvectormap-chicago-mill-en.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vector-map/map/jquery-jvectormap-in-mill.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vector-map/map/jquery-jvectormap-asia-mill.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/dashboard/default.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/notify/index.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datepicker/date-picker/datepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datepicker/date-picker/datepicker.en.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datepicker/date-picker/datepicker.custom.js"></script>
    
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="<?php echo base_url(); ?>assets/js/script.js"></script>
    <!--<script src="<?php echo base_url(); ?>assets/js/theme-customizer/customizer.js"></script>-->
    <!-- login js-->
    <script src="<?php echo base_url(); ?>assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datatable/datatables/datatable.custom.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datatable/datatable-extension/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>plugins/confirm/jquery-confirm.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/jquery-validation/jquery.validate.js"></script>
    <script src="<?php echo base_url(); ?>plugins/alert/sweetalert.js"></script>
    <script src="<?php echo base_url(); ?>public/js/spanish_dt.js"></script>
    <script src="<?php echo base_url(); ?>plugins/fileinput/fileinput.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>plugins/select2/select2.full.min.js" type="text/javascript"></script>

    <script src="https://cdn.datatables.net/buttons/2.4.1/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/2.4.1/js/buttons.colVis.min.js" type="text/javascript"></script>
    <!-- Plugin used-->
  </body>
</html>
