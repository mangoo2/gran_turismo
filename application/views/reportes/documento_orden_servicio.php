	<?php

	require_once('TCPDF4/tcpdf.php');
	//$ruta_base = $_SERVER['DOCUMENT_ROOT'];
	$this->load->helper('url');

	$englishDays = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
	$spanishDays = array('Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo');
	$englishMonths = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
	$spanishMonths = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
	$tipos_clientes = array('Potencial', 'Ocacional', 'Estandar', 'Bronce', 'Oro', 'Diamante');
	$cant_pasajeros = array('E', '4', '6', '10', '14', '20', '33', '47', '50');


	$GLOBALS['header'] = FCPATH . "public/img/orden_servicio/header.png";
	$GLOBALS['footer'] = FCPATH . "public/img/orden_servicio/footer.png";
	
	$GLOBALS['id_cont'] = $id_cont;
	$GLOBALS['folio_cont'] = $contratos->folio;
	$GLOBALS['folio_coti'] = $folio_coti;
	$GLOBALS['folio_oc'] = '';
	$GLOBALS['usuario'] = $usuario->nombre . ' ' . $usuario->apellido_p . ' ' . $usuario->apellido_m;
	$GLOBALS['fecha_hora_imp'] = $fecha_hora_imp;

	$empresa = $contratos->empresa;

	$cliente = $clientes->nombre . " " . $clientes->app . " " . $clientes->apm;
	$GLOBALS['cliente'] = $cliente;
	$telefono = $clientes->telefono;
	$telefono_2 = $clientes->telefono_2;

	$tipo_cliente = '';
	if (isset($clientes->tipo_cliente) && $clientes->tipo_cliente != '') {
		$tipo_cliente = $tipos_clientes[($clientes->tipo_cliente) - 1];
	}
	
	$estado = '';
	if (isset($estado_contrato) || $estado_contrato <= 0) {
		$estado = $estado_contrato;
	}
	$estadoD = $contratos->estado_destino;


	// Origen ---
	$origen = $contratos->lugar_origen;

	$origen_div = wordwrap($origen, 100, "\n", true);
	$origenes = explode("\n", $origen_div);
	$origenes[0] = isset($origenes[0]) ? $origenes[0] : '';
	$origenes[1] = isset($origenes[1]) ? $origenes[1] : '';
	$origenes[2] = isset($origenes[2]) ? $origenes[2] : '';


	$notas = $detalles->observaciones;
	//$notas = $contratos->observaciones;
	if ($notas == '') {
		$notas = '<br><br><br><br>';
	}


	$subtotal = 0;


	if (count($unidadAsig) > 0) {
		$subtotal = $unidadAsig->monto * $unidadAsig->cantidad;
	}

	$monto_anticipo = 0;
	if (isset($contratos->monto_anticipo)) {
		$monto_anticipo = $contratos->monto_anticipo;
	}

	$porc_anticipo = 0;
	if (isset($contratos->porc_anticipo)) {
		$porc_anticipo = $contratos->porc_anticipo;
	}

	$monto_descuento = 0;
	if (isset($contratos->porc_desc)) {
		$monto_descuento = $contratos->porc_desc;
	}


	$saldo = '';
	$saldo_letras = '';
	
	if($por_cobrar == 1){
		$saldo = $montoTotal - ($totalPagos + $monto_descuento + $monto_anticipo);
	}
	
	$saldo_format = number_format($saldo, 2, '.', ',');
	

	//--------------------------------------------------------->

	$fechaRegMayor = null;
	// ----- SALIDA -----
	$salida = new DateTime($contratos->fecha_salida);

	$newHoraSal = new DateTime($contratos->hora_salida);
	$newHoraSal->modify('-30 minutes');

	$hora_salida = $newHoraSal->format('H');
	$minuto_salida = $newHoraSal->format('i'); 

	$fecha_dia_s = $salida->format("j");
	$fecha_anio_s = $salida->format("Y");
	$fecha_mes_s = $salida->format("F");
	$fecha_mes_s = str_replace($englishMonths, $spanishMonths, $fecha_mes_s);

	$nombre_dia_s = $salida->format("l");
	$nombre_dia_s = str_replace($englishDays, $spanishDays, $nombre_dia_s);
	

	// -- REGRESO --
	$regreso = new DateTime($contratos->fecha_regreso);
	
	$hora_regreso = date('H', strtotime($contratos->hora_regreso));
	$minuto_regreso = date('i', strtotime($contratos->hora_regreso));

	$fecha_dia_r = $regreso->format("j");
	$fecha_anio_r = $regreso->format("Y");
	$fecha_mes_r = $regreso->format("F");
	$fecha_mes_r = str_replace($englishMonths, $spanishMonths, $fecha_mes_r);

	$nombre_dia_r = $regreso->format("l");
	$nombre_dia_r = str_replace($englishDays, $spanishDays, $nombre_dia_r);
	

	if ($fechaRegMayor === null || $salida->format('U') > $fechaRegMayor->format('U')) {
		$fechaRegMayor = $regreso;
	}


	if ($fechaRegMayor == null) {
		$redondo = ' ';
		$sencillo = 'X';
	} else {
		$redondo = 'X';
		$sencillo = ' ';
	}

	if ($salida !== null && $regreso !== null) {
		$diferencia = $salida->diff($regreso);
	} else {
		$salida = new DateTime('0000-00-00 00:00:00');
		$regreso = new DateTime('0000-00-00 00:00:00');
		$diferencia = $salida->diff($regreso);
	}

	$num_dias = 1;
	if ($diferencia->format('%a') > 0) {
		$num_dias = $diferencia->format('%a');
		$num_dias = $num_dias + 1;
	}
	//--------------------------------------------------------->


	//=======================================================================================
	class MYPDF extends TCPDF
	{

		//===========================================================
		var $Void = "";
		var $SP = " ";
		var $Dot = ".";
		var $Zero = "0";
		var $Neg = "Menos";
		function ValorEnLetras($x, $Moneda)
		{
			$s = "";
			$Ent = "";
			$Frc = "";
			$Signo = "";

			if (floatVal($x) < 0) {
				$Signo = $this->Neg . " ";
			} else {
				$Signo = "";
			}

			if (intval(number_format($x, 2, '.', '')) != $x) { //<- averiguar si tiene decimales 
				$s = number_format($x, 2, '.', '');
			} else {
				$s = number_format($x, 2, '.', '');
			}

			$Pto = strpos($s, $this->Dot);

			if ($Pto === false) {
				$Ent = $s;
				$Frc = $this->Void;
			} else {
				$Ent = substr($s, 0, $Pto);
				$Frc =  substr($s, $Pto + 1);
			}

			if ($Ent == $this->Zero || $Ent == $this->Void)
				$s = "Cero ";
			elseif (strlen($Ent) > 7) {
				$s = $this->SubValLetra(intval(substr($Ent, 0,  strlen($Ent) - 6))) .
					"Millones " . $this->SubValLetra(intval(substr($Ent, -6, 6)));
			} else {
				$s = $this->SubValLetra(intval($Ent));
			}

			if (substr($s, -9, 9) == "Millones " || substr($s, -7, 7) == "Millón ")
				$s = $s . "de ";

			$s = $s . $Moneda;
			if ($Moneda == 'MXN') {
				$abreviaturamoneda = 'M.N.';
			} else {
				$abreviaturamoneda = 'U.S.D';
			}

			if ($Frc != $this->Void) {
				$s = $s . " " . $Frc . "/100";
				//$s = $s . " " . $Frc . "/100"; 
			}
			$letrass = $Signo . $s . " " . $abreviaturamoneda;
			return ($Signo . $s . " " . $abreviaturamoneda);
		}
		function SubValLetra($numero)
		{
			$Ptr = "";
			$n = 0;
			$i = 0;
			$x = "";
			$Rtn = "";
			$Tem = "";

			$x = trim("$numero");
			$n = strlen($x);

			$Tem = $this->Void;
			$i = $n;

			while ($i > 0) {
				$Tem = $this->Parte(intval(substr($x, $n - $i, 1) .
					str_repeat($this->Zero, $i - 1)));
				if ($Tem != "Cero")
					$Rtn .= $Tem . $this->SP;
				$i = $i - 1;
			}


			//--------------------- GoSub FiltroMil ------------------------------ 
			$Rtn = str_replace(" Mil Mil", " Un Mil", $Rtn);
			while (1) {
				$Ptr = strpos($Rtn, "Mil ");
				if (!($Ptr === false)) {
					if (! (strpos($Rtn, "Mil ", $Ptr + 1) === false))
						$this->ReplaceStringFrom($Rtn, "Mil ", "", $Ptr);
					else
						break;
				} else break;
			}

			//--------------------- GoSub FiltroCiento ------------------------------ 
			$Ptr = -1;
			do {
				$Ptr = strpos($Rtn, "Cien ", $Ptr + 1);
				if (!($Ptr === false)) {
					$Tem = substr($Rtn, $Ptr + 5, 1);
					if ($Tem == "M" || $Tem == $this->Void);
					else
						$this->ReplaceStringFrom($Rtn, "Cien", "Ciento", $Ptr);
				}
			} while (!($Ptr === false));

			//--------------------- FiltroEspeciales ------------------------------ 
			$Rtn = str_replace("Diez Un", "Once", $Rtn);
			$Rtn = str_replace("Diez Dos", "Doce", $Rtn);
			$Rtn = str_replace("Diez Tres", "Trece", $Rtn);
			$Rtn = str_replace("Diez Cuatro", "Catorce", $Rtn);
			$Rtn = str_replace("Diez Cinco", "Quince", $Rtn);
			$Rtn = str_replace("Diez Seis", "Dieciseis", $Rtn);
			$Rtn = str_replace("Diez Siete", "Diecisiete", $Rtn);
			$Rtn = str_replace("Diez Ocho", "Dieciocho", $Rtn);
			$Rtn = str_replace("Diez Nueve", "Diecinueve", $Rtn);
			$Rtn = str_replace("Veinte Un", "Veintiun", $Rtn);
			$Rtn = str_replace("Veinte Dos", "Veintidos", $Rtn);
			$Rtn = str_replace("Veinte Tres", "Veintitres", $Rtn);
			$Rtn = str_replace("Veinte Cuatro", "Veinticuatro", $Rtn);
			$Rtn = str_replace("Veinte Cinco", "Veinticinco", $Rtn);
			$Rtn = str_replace("Veinte Seis", "Veintiseís", $Rtn);
			$Rtn = str_replace("Veinte Siete", "Veintisiete", $Rtn);
			$Rtn = str_replace("Veinte Ocho", "Veintiocho", $Rtn);
			$Rtn = str_replace("Veinte Nueve", "Veintinueve", $Rtn);

			//--------------------- FiltroUn ------------------------------ 
			if (substr($Rtn, 0, 1) == "M") $Rtn = "Un " . $Rtn;
			//--------------------- Adicionar Y ------------------------------ 
			for ($i = 65; $i <= 88; $i++) {
				if ($i != 77)
					$Rtn = str_replace("a " . Chr($i), "* y " . Chr($i), $Rtn);
			}
			$Rtn = str_replace("*", "a", $Rtn);
			return ($Rtn);
		}
		function ReplaceStringFrom(&$x, $OldWrd, $NewWrd, $Ptr)
		{
			$x = substr($x, 0, $Ptr)  . $NewWrd . substr($x, strlen($OldWrd) + $Ptr);
		}
		function Parte($x)
		{
			$Rtn = '';
			$t = '';
			$i = '';
			do {
				switch ($x) {
					case 0:
						$t = "Cero";
						break;
					case 1:
						$t = "Un";
						break;
					case 2:
						$t = "Dos";
						break;
					case 3:
						$t = "Tres";
						break;
					case 4:
						$t = "Cuatro";
						break;
					case 5:
						$t = "Cinco";
						break;
					case 6:
						$t = "Seis";
						break;
					case 7:
						$t = "Siete";
						break;
					case 8:
						$t = "Ocho";
						break;
					case 9:
						$t = "Nueve";
						break;
					case 10:
						$t = "Diez";
						break;
					case 20:
						$t = "Veinte";
						break;
					case 30:
						$t = "Treinta";
						break;
					case 40:
						$t = "Cuarenta";
						break;
					case 50:
						$t = "Cincuenta";
						break;
					case 60:
						$t = "Sesenta";
						break;
					case 70:
						$t = "Setenta";
						break;
					case 80:
						$t = "Ochenta";
						break;
					case 90:
						$t = "Noventa";
						break;
					case 100:
						$t = "Cien";
						break;
					case 200:
						$t = "Doscientos";
						break;
					case 300:
						$t = "Trescientos";
						break;
					case 400:
						$t = "Cuatrocientos";
						break;
					case 500:
						$t = "Quinientos";
						break;
					case 600:
						$t = "Seiscientos";
						break;
					case 700:
						$t = "Setecientos";
						break;
					case 800:
						$t = "Ochocientos";
						break;
					case 900:
						$t = "Novecientos";
						break;
					case 1000:
						$t = "Mil";
						break;
					case 1000000:
						$t = "Millón";
						break;
				}

				if ($t == $this->Void) {
					$i = floatval($i) + 1;
					$x = $x / 1000;
					if ($x == 0) $i = 0;
				} else
					break;
			} while ($i != 0);

			$Rtn = $t;
			switch ($i) {
				case 0:
					$t = $this->Void;
					break;
				case 1:
					$t = " Mil";
					break;
				case 2:
					$t = " Millones";
					break;
				case 3:
					$t = " Billones";
					break;
			}
			return ($Rtn . $t);
		}
		//===========================================================

		//Page header
		public function Header()
		{
			$this->Image($GLOBALS['header'], 8, 3, $this->getPageWidth() - 16, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
			$this->SetY(24.5);
			$this->SetX(-53); //-50 -> 0000
			$this->SetFont('times', 'B', 12);
			$this->SetTextColor(192, 0, 0);
			$this->Cell(0, 5, $GLOBALS['folio_cont'], 0, false, 'C', 0, '', 0, false, 'M', 'M');

			/*
			$this->SetY(19.5);
			$this->SetX(-82); //-50 -> 0000
			$this->SetFont('helvetica', 'BI', 12);
			$this->SetTextColor(51, 153, 255);
			$this->Cell(0, 5, $GLOBALS['folio_coti'], 0, false, 'C', 0, '', 0, false, 'M', 'M');
			*/

			$this->SetY(13.5);
			$this->SetX(-53); //-50 -> 0000
			$this->SetFont('helvetica', 'B', 14);
			$this->SetTextColor(40, 40, 248);
			$this->Cell(0, 5, $GLOBALS['folio_oc'], 0, false, 'C', 0, '', 0, false, 'M', 'M');
		}

		// Page footer
		public function Footer()
		{
			$html = '<p style="text-align:rigth; font-size:8.5px"> IMPRESO POR: ' . $GLOBALS['usuario'] . ' &nbsp;&nbsp;&nbsp; FECHA Y HORA: ' . $GLOBALS['fecha_hora_imp'] . '</p>';
			$this->writeHTML($html, true, false, true, false, '');

			/*
			$html = '<table width="100%" border="0" style="padding: 2px; font-size: 9.5px; text-align: justify; border:solid 5px black;">
								<thead>
									<tr>
										<td style="color:#ffffff; font-size: 12px; text-align: center; background-color:#000099;">
											<b>FIRMAS:</b>
										</td>
									</tr>
								</thead>

								<tbody>
									<tr>
										<td width="100%" style="color:#3399ff; text-align: center;">
											<b>¡QUE TENGAN UN EXCELENTE VIAJE!</b>
											<br><br>
										</td>
									</tr>

									<tr>
										<td width="10%"></td>
										<td width="30%" style="border-bottom:solid 1px black; text-align: center;">
											
										</td>
										<td width="20%"></td>
										<td width="30%" style="border-bottom:solid 1px black; text-align: center;">
											'.$GLOBALS['cliente'].'
										</td>
										<td width="10%"></td>
									</tr>
									
									<tr>
										<td width="10%"></td>
										<td width="30%" style="text-align: center;">
											NOMBRE Y FIRMA <br>OPERADOR ASIGNADO
										</td>
										<td width="20%"></td>
										<td width="30%" style="text-align: center;">
											NOMBRE Y FIRMA <br>CLIENTE
										</td>
										<td width="10%"></td>
									</tr>

									<tr>
										<td width="50%"></td>
										<td width="50%" style="text-align: center;">
											<br><br>
											IMPRESO POR: '.$GLOBALS['usuario'].' &nbsp;&nbsp;&nbsp; FECHA Y HORA: '.$GLOBALS['fecha_hora_imp'].'
										</td>
									</tr>
								</tbody>
							</table>';
							
							$this->writeHTML($html, true, false, true, false, '');
							*/
		}
	}
	$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Mangoo');
	$pdf->SetTitle('Orden De Servicio');
	$pdf->SetSubject('Orden De Servicio');
	$pdf->SetKeywords('Orden De Servicio');

	// set default header data
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

	// set header and footer fonts
	$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins('10', '30', '10'); //24
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
	$pdf->SetFooterMargin('5'); //46//40
	// set auto page breaks
	$pdf->SetAutoPageBreak(true, 5); //31//25//38
	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	$pdf->SetFont('dejavusans', '', 13);

	$height = $pdf->getPageHeight();
	$width = $pdf->getPageWidth();

	// add a page   style="width: 300px; height: 200px;"
	$pdf->AddPage('P', 'A4');

	$html = '
				<style type="text/css">
					.border_f{ border:solid 5px black; }
					.p_space{ font-size: 1px;}
				</style>

				<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 9.5px; text-align: justify;">
					<thead>
						<tr>
							<td class="border_f" style="color:#ffffff; font-size: 11px; text-align: center; background-color:#000099; font-weight: bold;">
								<b>DATOS DEL CLIENTE</b>
							</td>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td width="100%"><p class="p_space">&nbsp;</p></td>
						</tr>

						<tr>
							<td width="22%" style="font-weight: bold;">
								&nbsp;EMPRESA CONTRATANTE:
							</td>
							<td width="41%" style="border-bottom:solid 1px black;">
								' . $empresa . '
							</td>
							<td width="1%" ></td>
							<td width="23%" style="background-color:#c5d9f1; font-weight: bold;">
								TELÉFONO DE CONTACTO 1:
							</td>
							<td width="12%" style="border-bottom:solid 1px black;">
								' . $telefono . '
							</td>
							<td width="1%" ></td>
						</tr>

						<tr>
							<td width="64%" ></td>
							<td width="23%" style="background-color:#c5d9f1; font-size: 9.5px; text-align:right; color:#006ec0; font-weight: bold;">
								PRINCIPAL&nbsp;
							</td>
							<td width="13%" ></td>
						</tr>

						<tr>
							<td width="22%" style="font-weight: bold;">
								&nbsp;RESPONSABLE DEL VIAJE: 
							</td>
							<td width="41%" style="border-bottom:solid 1px black;">
								' . $cliente . '
							</td>
							<td width="1%"></td>
							<td width="23%" style="font-weight: bold;">
								TELÉFONO DE CONTACTO 2: 
							</td>
							<td width="12%" style="border-bottom:solid 1px black;">
								' . $telefono_2 . '
							</td>
							<td width="1%"></td>
						</tr>

						<tr>
							<td width="100%"><p class="p_space">&nbsp;</p></td>
						</tr>
					</tbody>
				</table>';


	$html .= '<br><br>
				<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 9.5px; text-align: justify;">
					<thead>
						<tr>
							<td style="color:#ffffff; font-size: 11px; text-align: center; background-color:#000099; font-weight: bold;">
								<b>DATOS DEL OPERADOR Y	VEHÍCULO</b>
							</td>
						</tr>
					</thead>
					<tbody>';

	if (count($choferAsig) > 0) {
		$choferVehiculo = '';
		$choferTipo = '0';
		$choferPlacas = '';

		foreach ($choferAsig as $key => $chofer) {

			if ($key == 0) {
				$choferVehiculo = $chofer->vehiculo;
				$choferTipo = $chofer->tipo;
				$choferPlacas = $chofer->placas;

				$html .= '
						<tr>
							<td width="20%" style="font-weight: bold;">
								&nbsp;OPERADOR ASIGNADO:
							</td>
							<td width="50%" style="border-bottom:solid 1px black;">
								' . $chofer->nombre . ' ' . $chofer->apellido_p . ' ' . $chofer->apellido_m . '
							</td>

							<td width="10%" style="font-weight: bold;">
								TELÉFONO:
							</td>
							<td width="19%" style="border-bottom:solid 1px black;">
								' . $chofer->telefono . '
							</td>
							<td width="1%" ></td>
						</tr>';

			}else{

			$html .= '
						<tr>
							<td width="19%" style="font-weight: bold;">
								&nbsp;OPERADOR ASIGNADO 
							</td>
							<td width="4%" style="font-weight: bold;">
								('. ($key + 1) .'):
							</td>
							<td width="47%" style="border-bottom:solid 1px black;">
								' . $chofer->nombre . ' ' . $chofer->apellido_p . ' ' . $chofer->apellido_m . '
							</td>

							<td width="10%" style="font-weight: bold;">
								TELÉFONO:
							</td>
							<td width="19%" style="border-bottom:solid 1px black;">
								' . $chofer->telefono . '
							</td>
							<td width="1%" ></td>
						</tr>';
			}

		}

		$html .= '
						<tr>
							<td width="10%" style="font-weight: bold;">
								&nbsp;VEHÍCULO:
							</td>
							<td width="40%" style="border-bottom:solid 1px black;">
								' . $choferVehiculo . '
							</td>

							<td width="21%" style="font-weight: bold;">
								NÚMERO DE PASAJEROS:
							</td>
							<td width="5%" style="border-bottom:solid 1px black; text-align: center;">
								' . $cant_pasajeros[$choferTipo] . '
							</td>

							<td width="8%" style="font-weight: bold;">
								PLACAS:
							</td>
							<td width="15%" style="border-bottom:solid 1px black; text-align: center;">
								' . $choferPlacas . '
							</td>
							<td width="1%" ></td>
						</tr>
						<tr>
							<td width="100%" style="border-bottom:solid 1px black;"><p class="p_space">&nbsp;</p></td>
						</tr>';
	}


	if (count($unidadAsig) > 0) {
		$idU = $unidadAsig->id;

		$choferIsset = array_filter($choferAsig, function ($chofer) use ($idU) {
			return $chofer->idUnidPros == $idU;
		});

		if (empty($choferIsset)) {
			$html .= '
						<tr>
							<td width="20%" style="font-weight: bold;">
								&nbsp;OPERADOR ASIGNADO:
							</td>
							<td width="50%" style="border-bottom:solid 1px black;">
								--- SIN ASIGNACIÓN ---
							</td>

							<td width="10%" style="font-weight: bold;">
								TELÉFONO:
							</td>
							<td width="19%" style="border-bottom:solid 1px black;">
								---
							</td>
							<td width="1%" ></td>
						</tr>

						<tr>
							<td width="10%" style="font-weight: bold;">
								&nbsp;VEHÍCULO:
							</td>
							<td width="40%" style="border-bottom:solid 1px black;">
								' . $unidadAsig->vehiculo . '
							</td>

							<td width="21%" style="font-weight: bold;">
								NÚMERO DE PASAJEROS:
							</td>
							<td width="5%" style="border-bottom:solid 1px black; text-align: center;">
								' . $cant_pasajeros[$unidadAsig->tipo] . '
							</td>

							<td width="8%" style="font-weight: bold;">
								PLACAS:
							</td>
							<td width="15%" style="border-bottom:solid 1px black; text-align: center;">
								' . $unidadAsig->placas . '
							</td>
							<td width="1%" ></td>
						</tr>
						<tr>
							<td width="100%" style="border-bottom:solid 1px black;"><p class="p_space">&nbsp;</p></td>
						</tr>';
		}
	} else {
		$html .= '
						<tr>
							<td width="20%" style="font-weight: bold;">
								&nbsp;OPERADOR ASIGNADO:
							</td>
							<td width="50%" style="border-bottom:solid 1px black;">
								--- SIN ASIGNACIÓN ---
							</td>

							<td width="10%" style="font-weight: bold;">
								TELÉFONO:
							</td>
							<td width="19%" style="border-bottom:solid 1px black;">
								---
							</td>
							<td width="1%" ></td>
						</tr>

						<tr>
							<td width="10%" style="font-weight: bold;">
								&nbsp;VEHÍCULO:
							</td>
							<td width="40%" style="border-bottom:solid 1px black;">
								--- SIN ASIGNACIÓN ---
							</td>

							<td width="21%" style="font-weight: bold;">
								NÚMERO DE PASAJEROS:
							</td>
							<td width="5%" style="border-bottom:solid 1px black; text-align: center;">
								---
							</td>

							<td width="8%" style="font-weight: bold;">
								PLACAS:
							</td>
							<td width="15%" style="border-bottom:solid 1px black; text-align: center;">
								---
							</td>
							<td width="1%" ></td>
						</tr>
						<tr>
							<td width="100%" style="border-bottom:solid 1px black;"><p class="p_space">&nbsp;</p></td>
						</tr>';
	}

	$html .= '</tbody>
				</table>';

	$html .= '<br><br>
							<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 9.5px; text-align: justify;">
								<thead>
									<tr>
										<td class="border_f" style="color:#ffffff; font-size: 11px; text-align: center; background-color:#000099; font-weight: bold;">
											<b>DATOS DEL SERVICIO</b>
										</td>
									</tr>
								</thead>
			
								<tbody>
									<tr>
										<td width="100%"><p class="p_space">&nbsp;</p></td>
									</tr>
			
									<tr>
										<td width="1%"></td>
										<td width="18%" style="text-align: center; background-color:#c5d9f1; font-weight: bold;">
											SALIDA
										</td>
										<td width="1%"></td>
										<td width="7%" style="font-weight: bold;">
											FECHA: 
										</td>
										<td width="50%" style="text-align: center; border-bottom:solid 1px black;">
											' . $nombre_dia_s . ', ' . $fecha_dia_s . ' de ' . $fecha_mes_s . ' de ' . $fecha_anio_s . '
										</td>
										<td width="6%" style="font-weight: bold;">
											HORA:
										</td>
										<td width="16%" style="text-align: center; border-bottom:solid 1px black;" >
											' . $hora_salida . ':' . $minuto_salida . ' Hrs.
										</td>
										<td width="1%"></td>
									</tr>
			
									<tr>
										<td width="100%"><p class="p_space">&nbsp;</p></td>
									</tr>
			
									<tr>
										<td width="1%"></td>
										<td width="18%" style="text-align: center; background-color:#c5d9f1; font-weight: bold;">
											REGRESO
										</td>
										<td width="1%"></td>
										<td width="7%" style="font-weight: bold;">
											FECHA: 
										</td>
										<td width="50%" style="text-align: center; border-bottom:solid 1px black;">
											' . $nombre_dia_r . ', ' . $fecha_dia_r . ' de ' . $fecha_mes_r . ' de ' . $fecha_anio_r . '
										</td>
										<td width="6%" style="font-weight: bold;">
											HORA:
										</td>
										<td width="16%" style="text-align: center; border-bottom:solid 1px black;" >
											' . $hora_regreso . ':' . $minuto_regreso . ' Hrs.
										</td>
										<td width="1%"></td>
									</tr>
			
									<tr>
										<td width="100%"><p class="p_space">&nbsp;</p></td>
									</tr>
			
									<tr>
										<td width="68%"></td>

										<td width="12%" style="font-weight: bold;">
											V. REDONDO:
										</td>

										<td width="5%" style="font-weight: bold; border:solid 1px black; text-align:center;">
											' . $redondo . '
										</td>
										<td width="1%"></td>

										<td width="6%" style="text-align: center; background-color:#c5d9f1; font-weight: bold; font-size: 9px;">
											NO. DE
										</td>
										<td width="1%"></td>
										<td width="6%"></td>

										<td width="1%"></td>
									</tr>

									<tr>
										<td width="1%"></td>
										<td width="15%" style="text-align: center; background-color:#c5d9f1; font-weight: bold;">
											ESTADO ORIGEN:
										</td>
										<td width="1%"></td>
										<td width="16%" style="border-bottom:solid 1px black;">
											' . $estado . '
										</td>
			
										<td width="1%"></td>
										<td width="16%" style="text-align: center; background-color:#c5d9f1; font-weight: bold;">
											ESTADO DESTINO:
										</td>
										<td width="1%"></td>
										<td width="16%" style="border-bottom:solid 1px black;">
											' . $estadoD . '
										</td>

										<td width="1%"></td>

										<td width="12%" style="font-weight: bold;">
											V. SENCILLO:
										</td>

										<td width="5%" style="font-weight: bold; border:solid 1px black; text-align:center;">
											' . $sencillo . '
										</td>
										<td width="1%"></td>

										<td width="6%" style="text-align: center; background-color:#c5d9f1; font-weight: bold; font-size: 9px;">
											DÍAS
										</td>
										<td width="1%"></td>
										<td width="6%" style="border-bottom:solid 1px black;text-align:center;">
											' . $num_dias . '
										</td>

										<td width="1%"></td>
									</tr>

			
									<tr>
										<td width="100%"><p class="p_space">&nbsp;</p></td>
									</tr>
									<tr>
										<td width="100%"><p class="p_space">&nbsp;</p></td>
									</tr>
			
									<tr>
										<td width="1%"></td>
										<td width="98%" style="text-align: center; font-size: 11px; background-color:#000099; border:solid 1px black; color:#ffffff; font-weight: bold;">
											PRESENTAR LA UNIDAD EN
										</td>
										<td width="1%"></td>
									</tr>
			
									<tr>
										<td width="1%"></td>
										<td width="98%" style="border:solid 1px black; text-align: center">
											' . $origenes[0] . '
										</td>
										<td width="1%"></td>
									</tr>
			
									<tr>
										<td width="1%"></td>
										<td width="98%" style="border:solid 1px black; text-align: center">
											' . $origenes[1] . '
										</td>
										<td width="1%"></td>
									</tr>
			
									<tr>
										<td width="1%"></td>
										<td width="98%" style="border:solid 1px black; text-align: center">
											' . $origenes[2] . '
										</td>
										<td width="1%"></td>
									</tr>
			
									<tr>
										<td width="100%"><p class="p_space">&nbsp;</p></td>
									</tr>
			
									<tr>
										<td width="1%"></td>
										<td width="98%" style="text-align: center; background-color:#000099; border:solid 1px black; color:#ffffff; font-weight: bold;">
											ITINERARIO
										</td>
										<td width="1%"></td>
									</tr>';


	if (count($destinos) > 0) {
		foreach ($destinos as $destino) {
			$html .= '<tr>
										<td width="1%"></td>
										<td width="98%" style="border:solid 1px black; text-align: center">
											' . $destino->lugar . '
										</td>
										<td width="1%"></td>
									</tr>';
		}
	} else {
		$html .= '<tr>
											<td width="1%"></td>
											<td width="98%" style="border:solid 1px black;"></td>
											<td width="1%"></td>
										</tr>
										<tr>
											<td width="1%"></td>
											<td width="98%" style="border:solid 1px black;"></td>
											<td width="1%"></td>
										</tr>
										<tr>
											<td width="1%"></td>
											<td width="98%" style="border:solid 1px black;"></td>
											<td width="1%"></td>
										</tr>';
	}


	$html .= 	'<tr>
										<td width="100%"><p class="p_space">&nbsp;</p></td>
									</tr>
			
									<tr>
										<td width="100%" style="color:#ee0000; text-align: center; font-size: 9px">
											<b>****TRASLADAR A LOS PASAJEROS A LOS LUGARES MENCIONADOS, <span style="color:#006ec0;;">FAVOR DE SER AMABLE Y TOLERANTE.****</span></b>
										</td>
									</tr>
			
									<tr>
										<td width="100%"><p class="p_space">&nbsp;</p></td>
									</tr>
			
								</tbody>
							</table>';


	$html .= '<br><br>
	<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 9.5px; text-align: justify;">
		<thead>
			<tr>
				<td class="border_f" style="color:#ffffff; font-size: 11px; text-align: center; background-color:#000099;">
					<b>NOTAS</b>
				</td>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td width="100%" style="color:#ee0000; font-size: 9.5px; text-align: center;">
					<b>NO INCLUYE PAGO DE ESTACIONAMIENTOS</b>
				</td>
			</tr>

			<tr>
				<td width="100%">
					<i>' . $notas . '</i>
				</td>
			</tr>

			<tr>
				<td width="100%"><p class="p_space">&nbsp;</p></td>
			</tr>
		</tbody>
	</table>';

	if($por_cobrar == 1){
		$saldo_letras =  $pdf->ValorEnLetras($saldo, 'MXN');
	}

	$html .= '<br><br>
				<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 9.5px; text-align: justify;">
					<thead>
						<tr>
							<td class="border_f" style="color:#ffffff; font-size: 11px; text-align: center; background-color:#000099;">
								<b>IMPORTE POR COBRAR</b>
							</td>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td width="20%" style="font-weight: bold;">
								&nbsp;IMPORTE POR COBRAR:
							</td>
							<td width="22%" style="border-bottom:solid 1px black;">
								<b>$</b> ' . $saldo_format . '
							</td>

							<td width="2%">
								&nbsp;(
							</td>
							<td width="53%" style="border-bottom:solid 1px black;">
								' . ($saldo_letras) . '
							</td>
							<td width="2%">
								)
							</td>
							<td width="1%" ></td>
						</tr>

						<tr>
							<td width="100%"><br></td>
						</tr>
					</tbody>
				</table>';

	$html .= '<br>
				<table class="" width="100%" border="0" style="padding: 2px; font-size: 9.5px; text-align: justify;">
					<tbody>
						<tr>
							<td width="100%">
								<img src="' . $GLOBALS['footer'] . '">
							</td>
						</tr>
					</tbody>
				</table>';

	$pdf->writeHTML($html, true, false, true, false, '');

	$pdf->IncludeJS('print(true);');
	//$pdf->Output('Orden_servicio.pdf', 'I');
	$pdf->Output(FCPATH . 'public/pdf/Orden_servicio_' . $id_cont . '.pdf', 'FI');
	//$pdf->Output(FCPATH.'public/pdf/Orden_servicio_'. $id_cont .'.pdf', 'I');