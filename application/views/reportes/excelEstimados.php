<?php
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename= estimacion_cotizacion".date('Ymd Gis').".xls");

$tipoCliente = array('---','Potencial','Ocacional','Estandar','Bronce','Oro','Diamante');
$tiposUnidad = array('---','AUTO 4 Plazas','MINIVAN 10 Plazas','VAN 14 Plazas','VAN 20 Plazas','VAN 33 Plazas','MIDIBUS 47 Plazas','AUTOBUS 47 Plazas','AUTOBUS 50 Plazas');
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th scope="col">Cotización</th>
            <th scope="col">Cliente</th>
            <th scope="col">Tipo Cliente</th>
            <th scope="col">Empresa</th>
            <th scope="col">RFC</th>
            <th scope="col">Teléfono</th>
            <th scope="col">Calle</th>
            <th scope="col">C.P.</th>
            <th scope="col">Ciudad</th>
            <th scope="col">Estado</th>
            <th scope="col">Email</th>
            <th scope="col">ID Cliente</th>
            <th scope="col">Lugar origen</th>
            <th scope="col">Destino</th>
            <th scope="col">No. de días</th>
            <th scope="col">Unidad(es)</th>
            <th scope="col">Cant.</th>
            <th scope="col">Asesor</th>
            <th scope="col">Total del viaje</th>
            <th scope="col">Obervaciones</th>
            <th scope="col">Kms recorridos (unidad)</th>
            <th scope="col">Kms recorridos (total)</th>
            <th scope="col">Costo combustible</th>
            <th scope="col">Total casetas</th>
            <th scope="col">Total otros</th>
            <th scope="col">Total combustible</th>
            <th scope="col">Sueldo chofer</th>
            <th scope="col">Utilidad %</th>
            <th scope="col">Gasto aproximado</th>
            <th scope="col">COTIZACION APROXIMADA</th>

        </tr>
    </thead>

    <tbody>
        <?php
        foreach ($cot as $c) {
            $id_cotizacion = 0;
            $unidad = "";
            $cantidad = "";
            $km_recorridos = "";
            $tipo = 0;
            if($c->tipo_cliente > 0){
                $tipo = $c->tipo_cliente;
            }

            $selEstado = $this->ModeloGeneral->getselectwhererow2('estados',array('EstadoId'=>$c->estado, 'activo'=>1));
            $fol_cot = "GT" . date("y") . "-000" . $c->id;
            $km_recorridos = "";
            $total_km_recorridos = 0;

            
            $f_sal = "";
            $h_sal = "";
            $f_reg = "";
            $h_reg = "";
            $destino = "";
            $tot_dias = "";


            $estimados = $this->ModeloGeneral->getselectwhererow2('cotizacion_estimados', array('id_cotizacion' => $c->id, "estatus" => 1));
            //log_message('error', 'estimados:' . json_encode($estimados));
            if (!empty($estimados)) {
                $id_cotizacion = $estimados->id;
            }

            //----------unidades--destinos
            $unidadesArray = $this->ModeloCotizaciones->getIdUnidades($c->id);
            $unid = $this->ModeloGeneral->getselectwhere2('cotizacion_unidades', array('id_estimados' => $id_cotizacion, 'id_cotizacion' => $c->id, "estatus" => 1));
            //log_message('error','unid:--> ' . json_encode($unid->result()) );
            //log_message('error','units:--> ' . json_encode($unidadesArray) );
            foreach ($unid->result() as $u) {
                $unidad .= $u->vehiculo . "<br>";
                $cantidad .= $u->cantidad . "<br>";
                $km_recorridos .= number_format($u->kms_reco, 2) . "<br>";
                $total_km_recorridos = $total_km_recorridos + $u->kms_reco;

                //$dest = $this->ModeloGeneral->getselectwhere2('destino_prospecto', array('id_cotizacion' => $c->id, "estatus" => 1));
                $dest = $this->ModeloCotizaciones->getDataDestinos($c->id);
                

                if (in_array(7, $unidadesArray)) {//$u->id_unidad

                    foreach ($dest as $d) {
                        if($d->unidad == 7){
                            $f_sal .= $d->fecha . "<br>";
                            $h_sal .= date('h:i A', strtotime($d->hora)) . "<br>";
                            $f_reg .= $d->fecha_regreso . "<br>";
                            $h_reg .= date('h:i A', strtotime($d->hora_regreso)) . "<br>";
                            $destino .= $d->lugar . "<br>";
        
                            $fecha_s = $d->fecha;
                            $sal = new DateTime($fecha_s);
                            $fecha_r = $d->fecha_regreso;
                            $reg = new DateTime($fecha_r);
                            $diferencia = $sal->diff($reg);
        
                            if ($diferencia->format('%a') == 0) {
                                $tot_dias .= 1 . "<br>";
                            } else {
                                $tot_dias .= intval($diferencia->format('%a')) . "<br>";
                            }
                        }
                        
                    }

                }
            }

            echo '
            <tr>
                <td >' . $fol_cot . '</td>
                <td >' . $c->cliente . '</td>
                <td >' . $tipoCliente[$tipo] . '</td>
                <td >' . ($c->empresa == "" ? "---" : $c->empresa) . '</td>
                <td >' . ($c->rfc == "" ? "---" : $c->rfc) . '</td>
                <td >' . ($c->telefono == "" ? "---" : $c->telefono) . '</td>
                <td >' . ($c->calle == "" ? "---" : $c->calle) . '</td>
                <td >' . ($c->cod_postal == "" ? "---" : $c->cod_postal) . '</td>
                <td >' . ($c->ciudad == "" ? "---" : $c->ciudad) . '</td>
                <td >' . $selEstado->Nombre . '</td>
                <td >' . ($c->correo == "" ? "---" : $c->correo) . '</td>
                <td >' . $c->idCliente . '</td>
                <td >' . ($c->lugar_origen == "" ? "---" : $c->lugar_origen) . '</td>  
                <td >' . ($destino == "" ? "---" : $destino) . '</td>
                <td >' . ($tot_dias == "" ? "---" : $tot_dias) . '</td>
                <td >' . ($unidad == "" ? "---" : $unidad) . '</td>
                <td >' . ($cantidad == "" ? "---" : $cantidad) . '</td>
                
                <td >' . $c->vendedor . '</td>
                <td >' . number_format($c->tot_unids, 2) . '</td>
    
                <td >' . $c->observaciones . '</td>

                <td >' . ($km_recorridos == "" ? 0 : $km_recorridos) . '</td>
                <td >' . number_format($total_km_recorridos, 2) . '</td>
                <td >' . number_format(isset($estimados->precio_gas) ? $estimados->precio_gas : 0, 2) . '</td>
                <td >' . number_format(isset($estimados->casetas) ? $estimados->casetas : 0, 2) . '</td>
                <td >' . number_format(isset($estimados->otros) ? $estimados->otros : 0, 2) . '</td>
                <td >' . number_format(isset($estimados->combustible) ? $estimados->combustible : 0, 2) . '</td>
                <td >' . number_format(isset($estimados->sueldo) ? $estimados->sueldo : 0, 2) . '</td>
                <td >' . (isset($estimados->utilidad) ? $estimados->utilidad : 0) . '% </td>
                <td >' . number_format(isset($estimados->gasto_aprox) ? $estimados->gasto_aprox : 0, 2) . '</td>
                <td >' . number_format(isset($estimados->cot_aprox) ? $estimados->cot_aprox : 0, 2) . '</td>
            </tr>';
        }
        ?>
    </tbody>
</table>