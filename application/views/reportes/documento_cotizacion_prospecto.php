<?php

require_once('TCPDF4/tcpdf.php');
//$ruta_base = $_SERVER['DOCUMENT_ROOT'];
$this->load->helper('url');
$englishDays = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
$spanishDays = array('Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo');
$englishMonths = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
$spanishMonths = array('Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');

$tiposUnidad = array(' ','AUTO','MINIVAN','VAN','VAN','VAN','MIDIBUS','AUTOBUS','AUTOBUS');
$capacidadUnidad = array(' ','4 Plazas','6 Plazas','10 Plazas','14 Plazas','20 Plazas','33 Plazas','47 Plazas','50 Plazas');

$fecha_dia = date('d');
$fecha_anio = date('Y');
$fecha_mes = date('m');
switch ($fecha_mes) {
	case 1:
			$nombre_mes = 'Enero';
			break;
	case 2:
			$nombre_mes = 'Febrero';
			break;
	case 3:
			$nombre_mes = 'Marzo';
			break;
	case 4:
			$nombre_mes = 'Abril';
			break;
	case 5:
			$nombre_mes = 'Mayo';
			break;
	case 6:
			$nombre_mes = 'Junio';
			break;
	case 7:
			$nombre_mes = 'Julio';
			break;
	case 8:
			$nombre_mes = 'Agosto';
			break;
	case 9:
			$nombre_mes = 'Septiembre';
			break;
	case 10:
			$nombre_mes = 'Octubre';
			break;
	case 11:
			$nombre_mes = 'Noviembre';
			break;
	case 12:
			$nombre_mes = 'Diciembre';
			break;
	default:
			$nombre_mes = ' --- ';
			break;
}

$GLOBALS['folio'] = $folio;

//- PROSPECTO ----------------->
	$id = $prospecto->id;
	$nombre = $prospecto->nombre;	
  $app = $prospecto->app;
  $apm = $prospecto->apm;
  $telefono = $prospecto->telefono;
  $correo = $prospecto->correo;
	$empresa = $prospecto->empresa;
  $lugar_origen = $prospecto->lugar_origen;
  $fecha_salida = $prospecto->fecha_salida;
	$hora_salida = date('H:i', strtotime($prospecto->hora_salida));
  $fecha_regreso = $prospecto->fecha_regreso;
  $hora_regreso = date('H:i', strtotime($prospecto->hora_regreso));
	$regreso = new DateTime($prospecto->fecha_regreso);
	$domicilio = $prospecto->direccion;


//Formato fecha----------------
$salida = new DateTime($fecha_salida);
$dia_salida = $salida->format("l");
$dia_salida = str_replace($englishDays, $spanishDays, $dia_salida);
$fecha_salida = $salida->format("j").'/'.$salida->format("M").'/'.$salida->format("Y");
$fecha_salida = str_replace($englishMonths, $spanishMonths, $fecha_salida);

$regreso = new DateTime($fecha_regreso);
$dia_regreso = $regreso->format("l");
$dia_regreso = str_replace($englishDays, $spanishDays, $dia_regreso);
$fecha_regreso = $regreso->format("j").'/'.$regreso->format("M").'/'.$regreso->format("Y");
$fecha_regreso = str_replace($englishMonths, $spanishMonths, $fecha_regreso);
//Formato fecha----------------

$diferencia = $salida->diff($regreso);
$dias = $diferencia->format('%a');

	$GLOBALS['logo'] = FCPATH . "public/img/cotizaciones/logo.png";

	$rutaImg = FCPATH . "public/uploads/unidades/";

	$GLOBALS['foot'] = FCPATH . "public/img/cotizaciones/footer.png";

	$pag1 = FCPATH . "public/img/cotizaciones/pag1.png";
	$pag2 = FCPATH . "public/img/cotizaciones/pag2.png";
	$pag3 = FCPATH . "public/img/cotizaciones/pag3.png";
	$pag4 = FCPATH . "public/img/cotizaciones/pag4.png";
	$pag5 = FCPATH . "public/img/cotizaciones/pag5.png";
	$pag6 = FCPATH . "public/img/cotizaciones/pag6.png";
	$pag7 = FCPATH . "public/img/cotizaciones/pag7.png";
	$pag8 = FCPATH . "public/img/cotizaciones/pag8.png";
	$pag9 = FCPATH . "public/img/cotizaciones/pag9.png";

	$GLOBALS['headerTable'] = true;


	if(isset($regreso)){
		$tipoViaje = 'Viaje Redondo';
	}else{
		$tipoViaje = 'Viaje Sencillo';
	}

	$nombrePersonal = $personal->nombre.' '.$personal->apellido_p.' '.$personal->apellido_m;
	$puesto = $personal->puesto;
//=======================================================================================
class MYPDF extends TCPDF
{

	//Page header
	public function Header()
	{
		
		if ($GLOBALS['headerTable'] && $this->PageNo() > 1) {
			$html = '
			<style type="text/css">
				.pspaces{ font-size: 7.2px;}
				.pspaces-2{ font-size: 2.5px;}
				.pspaces-t1{ font-size: 3.5px;}
				.pspaces-t2{ font-size: 2px;}
			</style>
			
			<table width="100%" border="0" RULES="rows" style="padding: 2px;" class="table table-striped">
				<tr>
					<td>
						<table>
								<tbody>
								<br><br><br><br><br><br><br>
									<tr style="background-color:#002060; color:white; font-size: 7.5px; text-align: center;">
										<td width="4%" border="1"><p class="pspaces-t1">&nbsp;</p> CANT. </td>
										<td width="8%" border="1"><p class="pspaces-t1">&nbsp;</p> UNIDAD </td>
										<td width="10%" border="1"><p class="pspaces-t1">&nbsp;</p> TIPO DE UNIDAD </td>
										<td width="4%" border="1"> NO: DE DÍAS </td>
										<td width="12%" border="1"><p class="pspaces-t1">&nbsp;</p> ORIGEN </td>
										<td width="8%" border="1"><p class="pspaces-t2">&nbsp;</p> FECHA DE SALIDA </td>
										<td width="6%" border="1"><p class="pspaces-t2">&nbsp;</p> HORA DE SALIDA </td>
										<td width="14%" border="1"><p class="pspaces-t1">&nbsp;</p> DESTINO Y/O ITINERARIO </td>
										<td width="8%" border="1"><p class="pspaces-t2">&nbsp;</p> FECHA DE REGRESO </td>
										<td width="6%" border="1"><p class="pspaces-t2">&nbsp;</p> HORA DE REGRESO </td>
										<td width="10%" border="1"><p class="pspaces-t2">&nbsp;</p> PRECIO UNITARIO </td>
										<td width="10%" border="1"><p class="pspaces-t1">&nbsp;</p> IMPORTE </td>
									</tr>
								<tbody>
						</table>
					</td>
				</tr>
			</table>
			';
			$this->writeHTML($html, true, false, true, false, '');
			return;
		}

		$html = '
			<table width="100%" border="0" RULES="rows" style="padding: 2px;" class="table table-striped">
				<tr>
					<td width="25%" style="color:black; text-align: left;">
						<img src="' . $GLOBALS['logo'] . '" >
					</td>
					<td width="35%"></td>
					<td width="40%" style="color:black; font-size: 10px; text-align: center;">
						<br><br><br>
						<table>
							<tbody>
								<tr>
									<td width="100%" border="1" style="background-color:#002060; color:white; font-size: 10px; text-align: center;"> <b><span style="font-size: 16px;">C</span>OTIZACIÓN</b> </td>
								</tr>
								<tr>
									<td width="50%" border="1" style="font-size: 10px; text-align: right;"> <b>F</b>OLIO DE PROPUESTA &nbsp; </td>
									<td width="50%" border="1" style="font-size: 9px; text-align: center;"> '.$GLOBALS['folio'].' </td>
								</tr>
								<tr>
									<td width="50%" border="1" style="font-size: 10px; text-align: right;"> <b>R</b>EVISIÓN &nbsp; </td>
									<td width="50%" border="1" style="font-size: 9px; text-align: center;"> - </td>
								</tr>
								<tr>
									<td width="50%" border="1" style="font-size: 10px; text-align: right;"> <b>T</b>IPO DE SERVICIO &nbsp; </td>
									<td width="50%" border="1" style="font-size: 9px; text-align: center;"> TRANSPORTE TURÍSTICO </td>
								</tr>
								<tr>
									<td width="50%" border="1" style="font-size: 10px; text-align: right;"> <b>P</b>ÁGINA &nbsp; </td>
									<td width="50%" border="1" style="font-size: 9px; text-align: center;"> '.$this->getAliasNumPage().' DE '.$this->getAliasNbPages().' </td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</table>
		';
		$this->writeHTML($html, true, false, true, false, '');
	}

	// Page footer
	public function Footer()
	{
		// Calcular la posición Y de la imagen
		$imageY = $this->getPageHeight() - 41;

		// Agregar la imagen al final de la página
		$this->Image($GLOBALS['foot'], 0, $imageY, $this->getPageWidth(), '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
	}
}
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo');
$pdf->SetTitle('Cotización');
$pdf->SetSubject('Cotización');
$pdf->SetKeywords('Cotización');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('8', '40', '8');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
$pdf->SetFooterMargin(45);
// set auto page breaks
$pdf->SetAutoPageBreak(true, 45);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 13);

$height = $pdf->getPageHeight();
$width = $pdf->getPageWidth();

// add a page   style="width: 300px; height: 200px;"
$pdf->AddPage('P', 'A4');

$html = '
			<style type="text/css">
				.pspaces{ font-size: 7.2px;}
				.pspaces-2{ font-size: 2.5px;}
				.pspaces-t1{ font-size: 3.5px;}
				.pspaces-t2{ font-size: 2px;}
			</style>

			<table width="100%" border="0" RULES="rows" style="padding: 2px;" class="table table-striped">
				<tr>
					<td width="20%"></td>

					<td width="60%" style="color:black; font-size: 10px; text-align: center;">
						<table width="100%">
							<tbody>
								<tr>
									<td width="40%" style="font-size: 10px; text-align: left; border-bottom:1px solid grey; border-right:1px solid grey;"> FECHA: </td>
									<td width="60%" style="font-size: 9px; text-align: left; border-bottom:1px solid grey;">  &nbsp; Puebla, Pue. a '.$fecha_dia.' de '.$nombre_mes.' de '.$fecha_anio.' </td>
								</tr>
								<tr>
									<td width="40%" style="font-size: 10px; text-align: left; border-bottom:1px solid grey; border-right:1px solid grey;"> COMPAÑÍA / EMPRESA: </td>
									<td width="60%" style="font-size: 9px; text-align: left; border-bottom:1px solid grey;">  &nbsp; '.$empresa.' </td>
								</tr>
								<tr>
									<td width="40%" style="font-size: 10px; text-align: left; border-bottom:1px solid grey; border-right:1px solid grey;"> CONTACTO: </td>
									<td width="60%" style="font-size: 9px; text-align: left; border-bottom:1px solid grey;">  &nbsp; '.$nombre.' '.$app.' '.$apm.' </td>
								</tr>
								<tr>
									<td width="40%" style="font-size: 10px; text-align: left; border-bottom:1px solid grey; border-right:1px solid grey;"> DOMICILIO: </td>
									<td width="60%" style="font-size: 9px; text-align: left; border-bottom:1px solid grey;">  &nbsp; '.$domicilio.' </td>
								</tr>
								<tr>
									<td width="40%" style="font-size: 10px; text-align: left; border-bottom:1px solid grey; border-right:1px solid grey;"> TELEFONO: </td>
									<td width="60%" style="font-size: 9px; text-align: left; border-bottom:1px solid grey;">  &nbsp; +52 '.$telefono.' </td>
								</tr>
								<tr>
									<td width="40%" style="font-size: 10px; text-align: left; border-bottom:1px solid grey; border-right:1px solid grey;"> E-MAIL: </td>
									<td width="60%" style="font-size: 9px; text-align: left; border-bottom:1px solid grey;">  &nbsp; '.$correo.' </td>
								</tr>
							</tbody>
						</table>
					</td>

					<td width="20%"></td>
				</tr>

				<tr>
					<td width="100%" style="color:black; text-align: justify; font-size: 9px;">
						Estimad@ '.$nombre.' '.$app.' '.$apm.',<br>
						'.$empresa.'<br>
						P&nbsp;R&nbsp;E&nbsp;S&nbsp;E&nbsp;N&nbsp;T&nbsp;E<br>
					</td>
				</tr>

				<tr>
					<td width="100%" style="font-size: 8px; color:black; text-align: justify;">
						<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
						Nos es muy grato dirigirnos a Usted para hacerle llegar un cordial saludo, asimismo agradecemos su interés al considerar a <span style="color:#0067b4;"><b>GRAND TURISMO EXPRESS</b></span> como proveedor
						para los servicios de transporte. A continuación, tenemos el gusto de presentarle nuestra Carta Propuesta para su revisión y aprobación.
						<br>
					</td>
				</tr>

				<tr>
					<td width="12%"></td>
					<td width="76%" style="font-size: 7px; color:black; text-align: justify; border:1px solid grey;">
						<span style="color:#002060;"><b>GRAND TURISMO EXPRESS: </b></span> <i>Somos una empresa comprometida y dedicada a proveer un excelente servicio integral en la logística
						de su transporte en sus diversas modalidades tales como transporte turístico, transporte de Personal, pasaje y arrendamiento de vehículos con operador. Mientras empresas
						como ustedes se dedican exclusivamente a su razón de negocio, nosotros le solucionamos la llegada Segura, Puntual y Cómoda de sus colaboradores y/o personal. 
						Además de cumplir con la función de llevarlos a donde desean, les brindaremos la comodidad y confiabilidad de llegar a su destino en las mejores manos. 
						Contamos con seguro de viajero, además de las unidades más cómodas y operadores con amplia experiencia, para que solamente se dedique a disfrutar del camino. 
						"Amplia experiencia, reconocimiento y satisfacción de nuestros clientes nos respaldan".</i>
					</td>
					<td width="12%"></td>
				</tr>

				<tr>
					<td width="100%" style="font-size: 8px; text-align: center;">
							<p class="pspaces-t2">&nbsp;</p>
							DESCRIPCIÓN DEL SERVICIO
					</td>
				</tr>
				<tr>
					<td>
						<table>
							<thead>
								<tr style="background-color:#002060; color:white; font-size: 7.5px; text-align: center;">
									<td width="4%" border="1"><p class="pspaces-t1">&nbsp;</p> CANT. </td>
									<td width="8%" border="1"><p class="pspaces-t1">&nbsp;</p> UNIDAD </td>
									<td width="10%" border="1"><p class="pspaces-t1">&nbsp;</p> TIPO DE UNIDAD </td>
									<td width="4%" border="1"> NO: DE DÍAS </td>
									<td width="12%" border="1"><p class="pspaces-t1">&nbsp;</p> ORIGEN </td>
									<td width="8%" border="1"><p class="pspaces-t2">&nbsp;</p> FECHA DE SALIDA </td>
									<td width="6%" border="1"><p class="pspaces-t2">&nbsp;</p> HORA DE SALIDA </td>
									<td width="14%" border="1"><p class="pspaces-t1">&nbsp;</p> DESTINO Y/O ITINERARIO </td>
									<td width="8%" border="1"><p class="pspaces-t2">&nbsp;</p> FECHA DE REGRESO </td>
									<td width="6%" border="1"><p class="pspaces-t2">&nbsp;</p> HORA DE REGRESO </td>
									<td width="10%" border="1"><p class="pspaces-t2">&nbsp;</p> PRECIO UNITARIO </td>
									<td width="10%" border="1"><p class="pspaces-t1">&nbsp;</p> IMPORTE </td>
								</tr>
							</thead>
							<tbody>';
foreach ($unidades as $unidad) {
	$html .= '
								<tr style="font-size: 7.5px; text-align: center;">
									<td width="4%" border="1"><p class="pspaces">&nbsp;</p> '.$unidad->cantidad.' </td>';

	if($unidad->img != ''){
		$html .= '		<td width="8%" border="1">
										<p style="font-size: 0.8px;">&nbsp;</p>
										<img src="'.$rutaImg.$unidad->img.'" height="43" width="52" >
									</td>';
	}else{
		$html .= '		<td width="8%" border="1">
										<p style="font-size: 35px;">&nbsp;</p>
									</td>';
	}

	$html .= '
									<td width="10%" border="1" >
										<p class="pspaces-t1">&nbsp;</p>
										<span style="color:#002060;"><b>'.$tiposUnidad[$unidad->tipo].' '.$unidad->marca.'</b></span><br>
										'.$capacidadUnidad[$unidad->tipo].'
									</td>
									<td width="4%" border="1"><p class="pspaces">&nbsp;</p> '.$dias.' </td>
									<td width="12%" border="1"><p class="pspaces-2">&nbsp;</p> '.$lugar_origen.' </td>
									<td width="8%" border="1"><p class="pspaces-t1">&nbsp;</p> '.$dia_salida.' <br> '.$fecha_salida.' </td> 
									<td width="6%" border="1"><p class="pspaces">&nbsp;</p> '.$hora_salida.' Hrs </td>
									<td width="14%" border="1"><p class="pspaces-2">&nbsp;</p> 
										<b>';
	
	foreach ($unidad->destinos as $destino) {
		$html .= $destino->lugar . ' - ' . $destino->lugar_hospeda.'<br>';
	}

	$html .=					'</b></td>
									<td width="8%" border="1"><p class="pspaces-t1">&nbsp;</p> '.$dia_regreso.' <br> '.$fecha_regreso.' </td>
									<td width="6%" border="1"><p class="pspaces">&nbsp;</p> '.$hora_regreso.' Hrs </td>
									<td width="10%" border="1"><p class="pspaces">&nbsp;</p> $'.number_format($unidad->monto, 2, '.', ',').'</td>
									<td width="10%" border="1"><p class="pspaces">&nbsp;</p> $'.number_format($unidad->monto * $unidad->cantidad, 2, '.', ',').'</td>
								</tr>';
}

$html .= '
							</tbody>
						</table>
					</td>
				</tr>
			</table>';
$pdf->writeHTML($html, true, false, true, false, '');

$GLOBALS['headerTable'] = false;

$html2 = '
			<table width="100%" border="0" RULES="rows" style="padding: 2px;" class="table table-striped">
				<tr>
					<td width="100%" style="font-size: 9px; color:black; text-align: justify;">
						<span style="color:#002060;"><u><b>NOTA:</b></u></span> TODOS NUESTROS PRECIOS <u>NO INCLUYEN I.V.A.</u><br>
						EN CASO DE REQUERIR FACTURA, SE APLICARÁ EL I.V.A. VIGENTE DEL COSTO DEL VIAJE.<br>
					</td>
				</tr>

				<tr>
					<td width="100%" style="font-size: 9px; color:black; text-align: justify;">
						1. <span style="color:#002060;"><b><u>EL SERVICIO DE TRANSPORTE INCLUYE:</u></b></span>
						<ul>
							<li>'.$tipoViaje.'</li>
							<li>Seguro de viajero a bordo de la unidad.</li>
							<li>Unidades certificadas por la SCT.</li>
							<li>Conductor Certificados por la SCT.</li>
							<li>Gastos de la unidad:
								<ul style="color: #404040;">
									<li><i>Combustible</i></li>
									<li><i>Gastos de Peaje.</i></li>
									<li><i>Mantenimientos de la unidad.</i></li>
									<li><i>Limpieza de la unidad.</i></li>
								</ul>
							</li>
							<li>Limpieza y sanitización con nebulización.</li>
							<li>Asistencia Telefónica 24/7.</li>
						</ul>
					</td>
				</tr>

				<tr>
					<td width="100%" style="font-size: 9px; color:black; text-align: justify;">
						2. <span style="color:#002060;"><b><u>CARACTERÍSTICAS DE NUESTRAS UNIDADES:</u></b></span>
						<ul>
							<li> Unidades turísticas con la capacidad requerida por la empresa.
								<ul style="color: #404040;">
									<li> <i>Asientos reclinables confort. </i></li>
									<li><i>Aire acondicionado </i></li>
									<li> <i>Conectores USB para carga de celular</i></li>
									<li> <i>Pantallas DVD</i></li>
									<li> <i>Equipo de sonido MP3</i></li>
									<li> <i>Cajuela y guarda equipaje </i></li>
									<li> <i>Velocidad controlada</i></li>
									<li> <i>WC (Sanitario) *Aplica únicamente para autobuses de 40, 47 y 50  plazas.</i></li>
								</ul>
							</li>
							<li> Excelentes condiciones mecánicas, de operación y de higiene garantizando la seguridad del personal.</li>
							<li> Las unidades cuentan con seguro vigente, que comprende seguro de vida e integridad física de los pasajeros, así como daños a terceros.</li>
							<li> Las unidades cuentan con placas autorizadas por la SCT, para el traslado del personal en carreteras federales.</li>
							<br>
						</ul>
					</td>
				</tr>

				<tr>
					<td width="100%" style="font-size: 9px; color:black; text-align: justify;">
						3. <span style="color:#002060;"><b><u>MEDIDAS DE HIGIENE Y PREVENCIÓN ANTE EL COVID-19</u></b></span><br>
						En GRAND TURISMO EXPRESS nos preocupamos por su seguridad y salud, es por ello que implementamos medidas de seguridad de
						acuerdo a la normativa de la Secretaría de Salud y Prevención en nuestras unidades para la protección de los usuarios, para eliminar
						riesgos y poder garantizar el bienestar de todos:
						<ul>
							<li> <span style="color:#5b9bd5;"><b>UNIDADES LIMPIAS: </b></span>Las unidades se limpian constantemente con cloro principalmente, pasamanos, pasillos, portabultos y lugares de donde los usuarios comúnmente se sostienen. Se realiza la limpieza al inicio y termino de cada traslado.</li>
							<li> <span style="color:#5b9bd5;"><b>SANITIZACIÓN:  </b></span>Se realiza la sanitización de las unidades semanalmente de acuerdo a los protocolos de la Secretaría de Salud. </li>
							<li> <span style="color:#5b9bd5;"><b>USO DE CUBREBOCAS: </b></span>Se refuerza el uso del cubrebocas durante el servicio.</li>
							<li> <span style="color:#5b9bd5;"><b>MEDICIÓN DE TEMPERATURA DE LOS USUARIOS: </b></span>Se incorpora el uso de un termómetro infrarrojo para medir la temperatura a cada usuario antes de abordar la unidad. </li>
							<li> <span style="color:#5b9bd5;"><b>GEL ANTIBACTERIAL: </b></span>Nuestras unidades están equipadas con dispensador de Gel antibacterial para el consumo de los usuarios y se les hace la recomendación de utilizarlo tanto en al ascenso de la unidad como en su descenso. </li>
							<li> <span style="color:#5b9bd5;"><b>CUBIERTA SANITIZANTE: </b></span>Se incorpora cubierta sanitizante para calzado de los usuarios al abordar (Tapete). </li>
							<br>
						</ul>
					</td>
				</tr>

				<tr>
					<td width="100%" style="font-size: 9px; color:black; text-align: justify;">
						4. <span style="color:#002060;"><b><u>TÉRMINOS Y CONDICIONES:</u></b></span><br>
						<ul>
							<li> Para contratación y/o reservación es necesario de un anticipo para garantizar los servicios. </li>
							<li> El monto restante del importe total, puede ser cubierto en pagos parciales y/o debe ser liquidado dos días antes, mínimo un día antes de los servicios programados.</li>
							<li> Se requiere firma de contrato para los servicios. </li>
							<li> Cualquier cambio será sujeto a revisión y negociación.</li>
							<li> Esta propuesta es de carácter informativo y tendrá una validez de 7 días hábiles a partir de su elaboración.</li>
							<li> Presupuesto sujeto a cambios sin previo aviso, a excepción de la firma de un convenio.</li>
							<li> Todas nuestras unidades están sujetas a disponibilidad, importante confirmar servicio y la firma del convenio. </li>
							<br>
						</ul>

						<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
						Así mismo, le informo que de vernos favorecidos con su preferencia, se solicita nos confirme el servicio, que contenga el itinerario completo, 
						domicilio especificado y hora del lugar de la cita, hora del lugar de regreso y nombre de la persona de responsable del servicio, número telefónico, 
						correo electrónico, copia de su IFE/INE o en caso de empresa RFC y comprobante domiciliario no mayor a tres meses de antigüedad.
						<br>
					</td>
				</tr>

				<tr>
					<td width="100%" style="font-size: 9px; color:black; text-align: justify;">
						5. <span style="color:#002060;"><b><u>FORMAS DE PAGO:</u></b></span><br>
						La forma de pago puede ser realizado por:
						<ul>
							<li> TRANSFERENCIA ELECTRÓNICA</li>
							<li> DEPÓSITO BANCARIO</li>
							<li> EN EFECTIVO. </li>
						</ul>

						<span>&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<b>* En caso de requerir factura, favor de anexar Cédula Fiscal y correo electrónico.</b><br>
						<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
						Sin otro particular, agradecemos sinceramente el interés y preferencia hacia Grand Turismo Express y le reiteramos nuestro compromiso de servirles con la calidad, puntualidad y profesionalismo que se merecen. Asimismo, quedamos de usted para atender a las necesidades específicas de los servicios requeridos.
						<br>
					</td>
				</tr>

				<tr>
					<td width="100%" style="font-size: 9px; color:black; text-align: justify;">
						&nbsp;&nbsp;Sinceramente,<br><br><br><br>
						<b>'.$nombrePersonal.'</b><br>
						<span style="font-size: 11px; color: #bf8f00;"><b>'.$puesto.'</b></span> <br>
						<span style="color: #002060;"><b>GRAND TURISMO EXPRESS</b></span> <br>
						<span style="color: #002060;"><b>Tel.</b></span> +52 222 220 03 28 <br>
						<span style="color: #002060;"><b>Móvil.</b></span> 222 359 00 08 <br>
						<span style="color: #002060;"><b>E-mail:</b></span> grandturismoexpress@outlook.com <br>
					</td>
				</tr>
			</table>';
$pdf->writeHTML($html2, true, false, true, false, '');

$pdf->SetAutoPageBreak(false, 0);
$pdf->setPrintHeader(false);

$pdf->AddPage('P', 'A4');
$pdf->Image($pag1, 12, 16, 185, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);

$pdf->AddPage('P', 'A4');
$pdf->Image($pag2, 0, 0, $width, $height, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);

$pdf->setPrintFooter(false);

$pdf->AddPage('L', 'A4');
$pdf->Image($pag3, 0, 0, $height, $width, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);

$pdf->AddPage('L', 'A4');
$pdf->Image($pag4, 0, 0, $height, $width, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);

$pdf->AddPage('L', 'A4');
$pdf->Image($pag5, 0, 0, $height, $width, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);

$pdf->AddPage('p', 'A4');
$pdf->Image($pag6, 0, 0, $width, $height, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);

$pdf->AddPage('L', 'A4');
$pdf->Image($pag7, 0, 0, $height, $width, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);

$pdf->AddPage('P', 'A4');
$pdf->Image($pag8, 0, 0, $width, $height, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);

$pdf->AddPage('L', 'A4');
$pdf->Image($pag9, 0, 0, $height, $width, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);


$pdf->IncludeJS('print(true);');
//$pdf->Output('Cotizacion.pdf', 'I');
$pdf->Output(FCPATH.'public/pdf/Cotizacion_'. $folio .'.pdf', 'I');
//$pdf->Output(FCPATH.'public/pdf/Cotizacion_'. 1 .'.pdf', 'F');
