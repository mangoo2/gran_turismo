<?php

header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=datos_contratos" . date('Ymd Gis') . ".xls");

?>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th scope="col">Contrato</th>
            <th scope="col">Folio</th>
            <th scope="col">Cotización</th>
            <th scope="col">Cliente</th>
            <th scope="col">Tipo Cliente</th>
            <th scope="col">Empresa</th>
            <th scope="col">RFC</th>
            <th scope="col">Teléfono</th>
            <th scope="col">Teléfono 2</th>
            <th scope="col">Calle</th>
            <th scope="col">C.P.</th>
            <th scope="col">Ciudad</th>
            <th scope="col">Estado</th>
            <th scope="col">Email</th>
            <th scope="col">ID Cliente</th>
            <!--<th scope="col">Fecha registro</th>-->
            <th scope="col">Lugar origen</th>
            <th scope="col">Fecha contrato</th>
            
            <th scope="col">Fecha salida</th>
            <th scope="col">Hora salida</th>
            <th scope="col">Fecha regreso</th>
            <th scope="col">Hora regreso</th>
            <th scope="col">No. de días</th>
            <th scope="col">Unidad(es)</th>
            <th scope="col">Cant.</th>
            <th scope="col">Destino</th>

            <th scope="col">Km inicial</th>
            <th scope="col">Km final</th>
            <th scope="col">Kms recorridos</th>

            <th scope="col">Operador</th>

            <th scope="col">Vendedor</th>
            <th scope="col">Total del viaje</th>
            <th scope="col">Anticipo</th>
            <th scope="col">Descuento</th>
            <th scope="col">Pagos recibidos</th>
            <th scope="col">Saldo</th>
            <th scope="col">Obervaciones</th>
            <th scope="col">Fecha gasto</th>
            <th scope="col">Tipo gasto</th>
            <th scope="col">Importe gasto</th>
            <th scope="col">Total gastos</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($con as $c) {
            $unidad = "";
            $cantidad = "";
            $chofer = "";
            $tipo = "";
            $unid = $this->ModeloContratos->getUnidadesContrato($c->id);
            

            //-------------------------------->
            $tabUnidades = '<table border="1"><tbody>';
            $tabCantidadU = '<table border="1"><tbody>';

            $tabGastosUF = '';
            $tabGastosUT = '';
            $tabGastosUI = '';

            $tabKmsUI = '<table border="1"><tbody>';
            $tabKmsUF = '<table border="1"><tbody>';
            $tabKmsUT = '<table border="1"><tbody>';

            $tabDestinos = '<table border="1"><tbody>';
            $destino = '';

            $tabOperadores = '<table border="1"><tbody>';
            $choferes = '';

            $gastos = $this->ModeloGeneral->getselectwhere2('contrato_gastos', array('id_contrato' => $c->id, "tipo >" => "0", "estatus" => 1));


            foreach ($unid->result() as $u) {
                $choferes = '';

                $dest = $this->ModeloGeneral->getselectwhere2('destino_prospecto', array('id_contrato' => $c->id, 'id_unidPros' => $u->id, "estatus" => 1));
                $operadores = $this->ModeloContratos->getChoferesUnidadDestino($c->id, $u->id);
                $gastosU = $this->ModeloGeneral->getselectwhere2('contrato_gastos', array('id_contrato' => $c->id,'id_unidad' => $u->unidad, "tipo >" => "0", "estatus" => 1));
                $KmsU = $this->ModeloGeneral->getselectwhere2('bitacora_revisiones', array('id_contrato' => $c->id, 'id_unidad' => $u->unidad, "estatus" => 1));


                $rowspan = $gastosU->num_rows() > 1 ? $gastosU->num_rows() : 1;

                $tabUnidades .= '
                    <tr style="vertical-align: middle;">
                        <td rowspan="' . $rowspan . '">
                            ' . $u->vehiculo . '
                        </td>
                    </tr>
                ';

                $tabCantidadU .= '
                    <tr style="vertical-align: middle;">
                        <td rowspan="' . $rowspan . '">
                            ' . $u->cantidad . '
                        </td>
                    </tr>
                ';


                if(count($operadores) > 0){
                    foreach ($operadores as $key => $op) {
                        $choferes .= $op->nombre . ' ' . $op->apellido_p . ' ' . $op->apellido_m;
                        if ($key < count($operadores) - 1) {
                            $choferes .= ' | ';
                        }
                    }
                }else{
                    $choferes .= '---SIN ASIGNACIÓN---';
                }

                $tabOperadores .= '
                    <tr style="vertical-align: middle;">
                        <td rowspan="' . $rowspan . '">
                            ' . $choferes . '
                        </td>
                    </tr>
                ';

                
                $destino = '';
                foreach ($dest->result() as $key => $d) {
                    $destino .= $d->lugar;
                    if ($key < ($dest->num_rows() - 1)) {
                        $destino .= ' | ';
                    }
                }

                $tabDestinos .= '
                    <tr style="vertical-align: middle;">
                        <td rowspan="' . $rowspan . '">
                            ' . $destino . '
                        </td>
                    </tr>
                ';


                $tabGastosUF .= '<table border="1"><tbody>
                    <tr style="vertical-align: middle;">
                        <td>';
                
                $tabGastosUT .= '<table border="1"><tbody>
                <tr style="vertical-align: middle;">
                    <td>';

                $tabGastosUI .= '<table border="1"><tbody>
                <tr style="vertical-align: middle;">
                    <td>';

                $total_gastos = 0;

                foreach ($gastosU->result() as $key => $gU) {
                    $tipo_gasto = "---";
                    if ($gU->tipo == "1") {
                        $tipo_gasto = "Caseta(s)";
                    } else if ($gU->tipo == "2") {
                        $tipo_gasto = "Combustible";
                    } else if ($gU->tipo == "3") {
                        $tipo_gasto = "Otros";
                    } else if ($gU->tipo == "4") {
                        $tipo_gasto = "Sueldos";
                    }

                    $tabGastosUF .= $gU->fecha;
                    $tabGastosUT .= $tipo_gasto ;
                    $tabGastosUI .= number_format($gU->importe, 2);
                    $total_gastos = ($total_gastos + $gU->importe);

                    
                    if ($key < ($gastosU->num_rows() - 1)) {
                        $tabGastosUF .= '<br>';
                        $tabGastosUT .= '<br>';
                        $tabGastosUI .= '<br>';
                    }
                }

                $tabGastosUF .= '</td></tr>
                            </tbody></table>';

                $tabGastosUT .= '</td></tr>
                </tbody></table>';

                $tabGastosUI .= '</td></tr>
                </tbody></table>';


                if($KmsU->num_rows() > 0){
                    foreach ($KmsU->result() as $KmsUn) {
                        $tabKmsUI .= '
                        <tr style="vertical-align: middle;">
                            <td rowspan="' . $rowspan . '">
                                ' . $KmsUn->km_ini . '
                            </td>
                        </tr>
                        ';
    
                        $tabKmsUF .= '
                        <tr style="vertical-align: middle;">
                            <td rowspan="' . $rowspan . '">
                                ' . $KmsUn->km_fin . '
                            </td>
                        </tr>
                        ';
    
                        $tabKmsUT .= '
                        <tr style="vertical-align: middle;">
                            <td rowspan="' . $rowspan . '">
                                ' . ($KmsUn->km_fin - $KmsUn->km_ini) . '
                            </td>
                        </tr>
                        ';
                    }
                }else{
                    $tabKmsUI .= '
                        <tr style="vertical-align: middle;">
                            <td rowspan="' . $rowspan . '">
                                ---
                            </td>
                        </tr>
                        ';
    
                        $tabKmsUF .= '
                        <tr style="vertical-align: middle;">
                            <td rowspan="' . $rowspan . '">
                                ---
                            </td>
                        </tr>
                        ';
    
                        $tabKmsUT .= '
                        <tr style="vertical-align: middle;">
                            <td rowspan="' . $rowspan . '">
                                ---
                            </td>
                        </tr>
                        ';
                }
                

                if ($rowspan > 1) {
                    for ($i = 0; $i < ($rowspan - 1); $i++) {
                        $tabUnidades .= '<tr></tr>';
                        $tabCantidadU .= '<tr></tr>';
                        $tabOperadores .= '<tr></tr>';
                        $tabDestinos .= '<tr></tr>';

                        $tabKmsUI .= '<tr></tr>';
                        $tabKmsUF .= '<tr></tr>';
                        $tabKmsUT .= '<tr></tr>';
                    }
                }
            }

            //-----------------------------Sin Unidades-------------------
            $choferesWU = '';
            $gastosWU = $this->ModeloGeneral->getselectwhere2('contrato_gastos', array('id_contrato' => $c->id,'id_unidad' => "0", "tipo >" => "0", "estatus" => 1));
            $operadoresWU = $this->ModeloContratos->getChoferesUnidadDestino($c->id, "0");
            $KmsWU = $this->ModeloGeneral->getselectwhere2('bitacora_revisiones', array('id_contrato' => $c->id, 'id_unidad' => "0", "estatus" => 1));
            //log_message('error','$contrato: '. json_encode($gastosWU->num_rows()));
            
            
            if($gastosWU->num_rows() > 0){
                $destWU = $this->ModeloGeneral->getselectwhere2('destino_prospecto', array('id_contrato' => $c->id, 'id_unidPros' => '0', "estatus" => 1));

                $rowspanW = $gastosWU->num_rows();

                $tabUnidades .= '
                    <tr style="vertical-align: middle;">
                        <td rowspan="' . $rowspanW . '">
                            ---SIN UNIDAD---
                        </td>
                    </tr>
                ';

                $tabCantidadU .= '
                    <tr style="vertical-align: middle;">
                        <td rowspan="' . $rowspanW . '">
                            ---
                        </td>
                    </tr>
                ';


                if(count($operadoresWU) > 0){
                    foreach ($operadoresWU as $key => $opWU) {
                        $choferesWU .= $opWU->nombre . ' ' . $opWU->apellido_p . ' ' . $opWU->apellido_m;
                        if ($key < count($operadoresWU) - 1) {
                            $choferesWU .= ' | ';
                        }
                    }
                }else{
                    $choferesWU .= '---SIN ASIGNACIÓN---';
                }

                $tabOperadores .= '
                    <tr style="vertical-align: middle;">
                        <td rowspan="' . $rowspanW . '">
                            ' . $choferesWU . '
                        </td>
                    </tr>
                ';



                $destinoW = '';
                foreach ($destWU->result() as $key => $dW) {
                    $destinoW .= $dW->lugar;
                    if ($key < ($destWU->num_rows() - 1)) {
                        $destinoW .= ' | ';
                    }
                }

                $tabDestinos .= '
                    <tr style="vertical-align: middle;">
                        <td rowspan="' . $rowspanW . '">
                            ' . $destinoW . '
                        </td>
                    </tr>
                ';



                $tabGastosUF .= '<table border="1"><tbody>
                    <tr style="vertical-align: middle;">
                        <td>';
                
                $tabGastosUT .= '<table border="1"><tbody>
                <tr style="vertical-align: middle;">
                    <td>';

                $tabGastosUI .= '<table border="1"><tbody>
                <tr style="vertical-align: middle;">
                    <td>';


                foreach ($gastosWU->result() as $key => $gWU) {
                    $tipo_gasto = "---";
                    if ($gWU->tipo == "1") {
                        $tipo_gasto = "Caseta(s)";
                    } else if ($gWU->tipo == "2") {
                        $tipo_gasto = "Combustible";
                    } else if ($gWU->tipo == "3") {
                        $tipo_gasto = "Otros";
                    } else if ($gWU->tipo == "4") {
                        $tipo_gasto = "Sueldos";
                    }

                    $tabGastosUF .= $gWU->fecha;
                    $tabGastosUT .= $tipo_gasto ;
                    $tabGastosUI .= number_format($gWU->importe, 2);
                    $total_gastos = ($total_gastos + $gWU->importe);

                    
                    if ($key < ($gastosWU->num_rows() - 1)) {
                        $tabGastosUF .= '<br>';
                        $tabGastosUT .= '<br>';
                        $tabGastosUI .= '<br>';
                    }
                }

                $tabGastosUF .= '</td></tr>
                            </tbody></table>';

                $tabGastosUT .= '</td></tr>
                </tbody></table>';

                $tabGastosUI .= '</td></tr>
                </tbody></table>';



                if($KmsWU->num_rows() > 0){
                    foreach ($KmsWU->result() as $KmsWUn) {
                        $tabKmsUI .= '
                        <tr style="vertical-align: middle;">
                            <td rowspan="' . $rowspanW . '">
                                ' . $KmsWUn->km_ini . '
                            </td>
                        </tr>
                        ';
    
                        $tabKmsUF .= '
                        <tr style="vertical-align: middle;">
                            <td rowspan="' . $rowspanW . '">
                                ' . $KmsWUn->km_fin . '
                            </td>
                        </tr>
                        ';
    
                        $tabKmsUT .= '
                        <tr style="vertical-align: middle;">
                            <td rowspan="' . $rowspanW . '">
                                ' . ($KmsWUn->km_fin - $KmsWUn->km_ini) . '
                            </td>
                        </tr>
                        ';
                    }
                }else{
                    $tabKmsUI .= '
                        <tr style="vertical-align: middle;">
                            <td rowspan="' . $rowspanW . '">
                                ---
                            </td>
                        </tr>
                        ';
    
                        $tabKmsUF .= '
                        <tr style="vertical-align: middle;">
                            <td rowspan="' . $rowspanW . '">
                                ---
                            </td>
                        </tr>
                        ';
    
                        $tabKmsUT .= '
                        <tr style="vertical-align: middle;">
                            <td rowspan="' . $rowspanW . '">
                                ---
                            </td>
                        </tr>
                        ';
                }




                if ($rowspanW > 1) {
                    for ($i = 0; $i < ($rowspan - 1); $i++) {
                        $tabUnidades .= '<tr></tr>';
                        $tabCantidadU .= '<tr></tr>';

                        $tabKmsUI .= '<tr></tr>';
                        $tabKmsUF .= '<tr></tr>';
                        $tabKmsUT .= '<tr></tr>';
                    }
                }
            }


            //-----------------------------Sin Unidades-------------------



            $tabUnidades .= '</tbody></table>';
            $tabCantidadU .= '</tbody></table>';
            $tabOperadores .= '</tbody></table>';
            $tabDestinos .= '</tbody></table>';


            $tabKmsUI .= '</tbody></table>';
            $tabKmsUF .= '</tbody></table>';
            $tabKmsUT .= '</tbody></table>';


            //--------------------------

            if ($c->tipo_cliente == "1") {
                $tipo = "Potencial";
            } else if ($c->tipo_cliente == "2") {
                $tipo = "Ocacional";
            } else if ($c->tipo_cliente == "3") {
                $tipo = "Estandar";
            } else if ($c->tipo_cliente == "4") {
                $tipo = "Bronce";
            } else if ($c->tipo_cliente == "5") {
                $tipo = "Oro";
            } else if ($c->tipo_cliente == "6") {
                $tipo = "Diamante";
            }


            $pagado = "";
            if ($c->liquidado == "1") {
                $pagado = "0";
            } else {
                $resta = $c->tot_unids - $c->monto_anticipo - $c->porc_desc - $c->tot_pagos;
                $pagado = number_format($resta, 2);
            }

            if ($c->id_cotizacion != 0) {
                $fol_cot = "T" . date("y") . "-0" . $c->id_cotizacion;
            } else {
                $fol_cot = "--";
            }


            $fecha_s = $c->fecha_salida;
            $sal = new DateTime($fecha_s);
            $fecha_r = $c->fecha_regreso;
            $reg = new DateTime($fecha_r);

            $diferencia = $sal->diff($reg);
            if ($diferencia->format('%a') == 0) {
                $tot_dias = 1;
            } else {
                $tot_dias = intval($diferencia->format('%a'));
            }


            echo '
            <tr style="vertical-align: middle;" >
                <td >' . $c->id . '</td>
                <td >' . $c->folio . '</td>
                <td >' . $fol_cot . '</td>
                <td >' . $c->cliente . '</td>
                <td >' . $tipo . '</td>
                <td >' . $c->empresa . '</td>
                <td >' . $c->rfc . '</td>
                <td >' . $c->telefono . '</td>
                <td >' . $c->telefono_2 . '</td>
                <td >' . $c->calle . '</td>
                <td >' . $c->cod_postal . '</td>
                <td >' . $c->ciudad . '</td>
                <td >' . $c->estado . '</td>
                <td >' . $c->correo . '</td>
                <td >' . $c->idCliente . '</td>
                <!--<td >' . $c->fecha_reg . '</td>-->
                <td >' . $c->lugar_origen . '</td>
                <td >' . $c->fecha_contrato . '</td>

                <td >' . $c->fecha_salida . '</td>
                <td >' . date('h:i A', strtotime($c->hora_salida)) . '</td>
                <td >' . $c->fecha_regreso . '</td>
                <td >' . date('h:i A', strtotime($c->hora_regreso)) . '</td>
                <td >' . $tot_dias . '</td>

                <td >' . $tabUnidades . '</td>
                <td >' . $tabCantidadU . '</td>
                <td >' . $tabDestinos . '</td>

                <td >' . $tabKmsUI . '</td>
                <td >' . $tabKmsUF . '</td>
                <td >' . $tabKmsUT . '</td>

                <td >' . $tabOperadores . '</td>

                <td >' . $c->vendedor . '</td>
                <td >' . number_format($c->tot_unids, 2) . '</td>
                <td >' . number_format($c->monto_anticipo, 2) . '</td>
                <td >' . number_format($c->porc_desc, 2) . '</td>
                <td >' . number_format($c->tot_pagos, 2) . '</td>
                <td >' . $pagado . '</td>

                <td >' . $c->observaciones . '</td>
                
                <td >' . $tabGastosUF . '</td>
                <td >' . $tabGastosUT . '</td>
                <td >' . $tabGastosUI . '</td>
                <td >' . number_format($total_gastos, 2) . '</td>
            <tr>';
        }
        ?>
    </tbody>
</table>