	<?php

	require_once('TCPDF4/tcpdf.php');
	//$ruta_base = $_SERVER['DOCUMENT_ROOT'];
	$this->load->helper('url');
//C:\xampp\htdocs\gran_turismo\public\img\comprobante_pago
$GLOBALS['header'] = FCPATH . "public/img/comprobante_pago/header.png";
$GLOBALS['footer'] = FCPATH . "public/img/comprobante_pago/footer.png";
$GLOBALS['folio_cont'] = $id;
$efectivo = $tipoCuenta == 1 ? 'X' : ''; 
$transferencia = $tipoCuenta != 1 ? 'X' : '';

$partesFecha = explode("-", $fecha);
// Obtener día, mes y año
$GLOBALS['dia'] = $partesFecha[2];
$GLOBALS['mes'] = $partesFecha[1];
$GLOBALS['anio'] = $partesFecha[0];

$pagado = 0;
$numIndex = 1;
foreach ($pagos->result() as $pago) {
	//[{"id":"88","id_contrato":"34","id_cuenta":"1","referencia":"Refe_1","monto":"100","fecha":"2023-10-06","file_pago":"pago_88_20231026183032.pdf","reg":"2023-10-26 18:30:32","id_usuario":"1","estatus":"1"},{"id":"89","id_contrato":"34","id_cuenta":"1","referencia":"Refe","monto":"120","fecha":"2023-10-01","file_pago":"pago_89_20231026183215.pdf","reg":"2023-10-26 18:32:15","id_usuario":"1","estatus":"1"},{"id":"90","id_contrato":"34","id_cuenta":"1","referencia":"0","monto":"123","fecha":"2023-10-05","file_pago":"pago_90_20231026183317.pdf","reg":"2023-10-26 18:33:17","id_usuario":"1","estatus":"1"},{"id":"91","id_contrato":"34","id_cuenta":"1","referencia":"Refe_1","monto":"100","fecha":"2023-10-17","file_pago":"","reg":"2023-10-26 18:34:03","id_usuario":"1","estatus":"1"}]
	$pagado = $pagado + $pago->monto;
	$numIndex++;
	$GLOBALS['folio_pago'] = $pago->id;
}

$GLOBALS['folio_pago']++;

$saldo = 0;
foreach ($costos->result() as $costo) {
	$saldo = $saldo + ($costo->cantidad * $costo->monto);
}

//$GLOBALS['usuario'] = '$usuario->nombre'.' '.'$usuario->apellido_p'.' '.'$usuario->apellido_m';
//$GLOBALS['fecha_hora_imp'] = '$fecha_hora_imp';

	//=======================================================================================
	class MYPDF extends TCPDF
	{
		//===========================================================
		var $Void = ""; 
		var $SP = " "; 
		var $Dot = "."; 
		var $Zero = "0"; 
		var $Neg = "Menos";
		function ValorEnLetras($x, $Moneda ){ 
			$s=""; 
			$Ent=""; 
			$Frc=""; 
			$Signo=""; 
					
			if(floatVal($x) < 0) {
				$Signo = $this->Neg . " "; 
			}else{
				$Signo = "";
			}
			
			if(intval(number_format($x,2,'.','') )!=$x){ //<- averiguar si tiene decimales 
				$s = number_format($x,2,'.',''); 
			}else{
				$s = number_format($x,2,'.',''); 
			}

			$Pto = strpos($s, $this->Dot); 
					
			if ($Pto === false) 
			{ 
				$Ent = $s; 
				$Frc = $this->Void; 
			} 
			else 
			{ 
				$Ent = substr($s, 0, $Pto ); 
				$Frc =  substr($s, $Pto+1); 
			} 

			if($Ent == $this->Zero || $Ent == $this->Void) 
				$s = "Cero "; 
			elseif( strlen($Ent) > 7) 
			{ 
				$s = $this->SubValLetra(intval( substr($Ent, 0,  strlen($Ent) - 6))) .  
							"Millones " . $this->SubValLetra(intval(substr($Ent,-6, 6))); 
			} 
			else 
			{ 
				$s = $this->SubValLetra(intval($Ent)); 
			} 

			if (substr($s,-9, 9) == "Millones " || substr($s,-7, 7) == "Millón ") 
				$s = $s . "de "; 

			$s = $s . $Moneda; 
			if ($Moneda=='MXN') {
				$abreviaturamoneda='M.N.';
			}else{
				$abreviaturamoneda='U.S.D';
			}

			if($Frc != $this->Void) 
			{ 
				$s = $s . " " . $Frc. "/100"; 
				//$s = $s . " " . $Frc . "/100"; 
			} 
			$letrass=$Signo . $s . " ".$abreviaturamoneda; 
			return ($Signo . $s . " ".$abreviaturamoneda);    
		} 
		function SubValLetra($numero) { 
				$Ptr=""; 
				$n=0; 
				$i=0; 
				$x =""; 
				$Rtn =""; 
				$Tem =""; 

				$x = trim("$numero"); 
				$n = strlen($x); 

				$Tem = $this->Void; 
				$i = $n; 
				
				while( $i > 0) 
				{ 
					$Tem = $this->Parte(intval(substr($x, $n - $i, 1).  
															str_repeat($this->Zero, $i - 1 ))); 
					If( $Tem != "Cero" ) 
							$Rtn .= $Tem . $this->SP; 
					$i = $i - 1; 
				} 

				
				//--------------------- GoSub FiltroMil ------------------------------ 
				$Rtn=str_replace(" Mil Mil", " Un Mil", $Rtn ); 
				while(1) 
				{ 
					$Ptr = strpos($Rtn, "Mil ");        
					If(!($Ptr===false)) 
					{ 
							If(! (strpos($Rtn, "Mil ",$Ptr + 1) === false )) 
								$this->ReplaceStringFrom($Rtn, "Mil ", "", $Ptr); 
							Else 
							break; 
					} 
					else break; 
				} 

				//--------------------- GoSub FiltroCiento ------------------------------ 
				$Ptr = -1; 
				do{ 
					$Ptr = strpos($Rtn, "Cien ", $Ptr+1); 
					if(!($Ptr===false)) 
					{ 
							$Tem = substr($Rtn, $Ptr + 5 ,1); 
							if( $Tem == "M" || $Tem == $this->Void) 
								; 
							else           
								$this->ReplaceStringFrom($Rtn, "Cien", "Ciento", $Ptr); 
					} 
				}while(!($Ptr === false)); 

				//--------------------- FiltroEspeciales ------------------------------ 
				$Rtn=str_replace("Diez Un", "Once", $Rtn ); 
				$Rtn=str_replace("Diez Dos", "Doce", $Rtn ); 
				$Rtn=str_replace("Diez Tres", "Trece", $Rtn ); 
				$Rtn=str_replace("Diez Cuatro", "Catorce", $Rtn ); 
				$Rtn=str_replace("Diez Cinco", "Quince", $Rtn ); 
				$Rtn=str_replace("Diez Seis", "Dieciseis", $Rtn ); 
				$Rtn=str_replace("Diez Siete", "Diecisiete", $Rtn ); 
				$Rtn=str_replace("Diez Ocho", "Dieciocho", $Rtn ); 
				$Rtn=str_replace("Diez Nueve", "Diecinueve", $Rtn ); 
				$Rtn=str_replace("Veinte Un", "Veintiun", $Rtn ); 
				$Rtn=str_replace("Veinte Dos", "Veintidos", $Rtn ); 
				$Rtn=str_replace("Veinte Tres", "Veintitres", $Rtn ); 
				$Rtn=str_replace("Veinte Cuatro", "Veinticuatro", $Rtn ); 
				$Rtn=str_replace("Veinte Cinco", "Veinticinco", $Rtn ); 
				$Rtn=str_replace("Veinte Seis", "Veintiseís", $Rtn ); 
				$Rtn=str_replace("Veinte Siete", "Veintisiete", $Rtn ); 
				$Rtn=str_replace("Veinte Ocho", "Veintiocho", $Rtn ); 
				$Rtn=str_replace("Veinte Nueve", "Veintinueve", $Rtn ); 

				//--------------------- FiltroUn ------------------------------ 
				If(substr($Rtn,0,1) == "M") $Rtn = "Un " . $Rtn; 
				//--------------------- Adicionar Y ------------------------------ 
				for($i=65; $i<=88; $i++) 
				{ 
					If($i != 77) 
						$Rtn=str_replace("a " . Chr($i), "* y " . Chr($i), $Rtn); 
				} 
				$Rtn=str_replace("*", "a" , $Rtn); 
				return($Rtn); 
		} 
		function ReplaceStringFrom(&$x, $OldWrd, $NewWrd, $Ptr) { 
			$x = substr($x, 0, $Ptr)  . $NewWrd . substr($x, strlen($OldWrd) + $Ptr); 
		} 
		function Parte($x) { 
				$Rtn=''; 
				$t=''; 
				$i=''; 
				Do 
				{ 
					switch($x) 
					{ 
						Case 0:  $t = "Cero";break; 
						Case 1:  $t = "Un";break; 
						Case 2:  $t = "Dos";break; 
						Case 3:  $t = "Tres";break; 
						Case 4:  $t = "Cuatro";break; 
						Case 5:  $t = "Cinco";break; 
						Case 6:  $t = "Seis";break; 
						Case 7:  $t = "Siete";break; 
						Case 8:  $t = "Ocho";break; 
						Case 9:  $t = "Nueve";break; 
						Case 10: $t = "Diez";break; 
						Case 20: $t = "Veinte";break; 
						Case 30: $t = "Treinta";break; 
						Case 40: $t = "Cuarenta";break; 
						Case 50: $t = "Cincuenta";break; 
						Case 60: $t = "Sesenta";break; 
						Case 70: $t = "Setenta";break; 
						Case 80: $t = "Ochenta";break; 
						Case 90: $t = "Noventa";break; 
						Case 100: $t = "Cien";break; 
						Case 200: $t = "Doscientos";break; 
						Case 300: $t = "Trescientos";break; 
						Case 400: $t = "Cuatrocientos";break; 
						Case 500: $t = "Quinientos";break; 
						Case 600: $t = "Seiscientos";break; 
						Case 700: $t = "Setecientos";break; 
						Case 800: $t = "Ochocientos";break; 
						Case 900: $t = "Novecientos";break; 
						Case 1000: $t = "Mil";break; 
						Case 1000000: $t = "Millón";break; 
					} 

					if($t == $this->Void) 
					{ 
						$i = floatval($i) + 1; 
						$x = $x / 1000; 
						if($x== 0) $i = 0; 
					} 
					else 
						break; 
								
				}while($i != 0); 
				
				$Rtn = $t; 
				switch($i) 
				{ 
					Case 0: $t = $this->Void;break; 
					Case 1: $t = " Mil";break; 
					Case 2: $t = " Millones";break; 
					Case 3: $t = " Billones";break; 
				} 
				return($Rtn . $t); 
		}  
	//===========================================================

		//Page header
		public function Header()
		{	
			$this->Image($GLOBALS['header'], 8, 3, $this->getPageWidth() - 16, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
			$this->SetY(12);
			$this->SetX(-70);//-50 -> 0000
			$this->SetFont('times', 'B', 18);
			$this->SetTextColor(192, 0, 0);
			$this->Cell(0, 5, $GLOBALS['folio_pago'], 0, false, 'C', 0, '', 0, false, 'M', 'M');

			$this->SetY(22);
			$this->SetX(-100);
			$this->SetFont('times', 'B', 12);
			$this->SetTextColor(0, 0, 153);
			$this->Cell(0, 5, $GLOBALS['dia'], 0, false, 'C', 0, '', 0, false, 'M', 'M');

			$this->SetY(22);
			$this->SetX(-65);
			$this->SetFont('times', 'B', 12);
			$this->SetTextColor(0, 0, 153);
			$this->Cell(0, 5, $GLOBALS['mes'], 0, false, 'C', 0, '', 0, false, 'M', 'M');

			$this->SetY(22);
			$this->SetX(-25);
			$this->SetFont('times', 'B', 12);
			$this->SetTextColor(0, 0, 153);
			$this->Cell(0, 5, $GLOBALS['anio'], 0, false, 'C', 0, '', 0, false, 'M', 'M');


		}

		// Page footer
		public function Footer()
		{
			//$html = '<p style="text-align:rigth; font-size:8.5px"> IMPRESO POR: '.$GLOBALS['usuario'].' &nbsp;&nbsp;&nbsp; FECHA Y HORA: '.$GLOBALS['fecha_hora_imp'].'</p>';
			//$this->writeHTML($html, true, false, true, false, '');
			$this->Image($GLOBALS['footer'], 8, $this->getPageHeight() - 18, $this->getPageWidth() - 16, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		}


	}
	$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Mangoo');
	$pdf->SetTitle('Comprobante de Pago');
	$pdf->SetSubject('Comprobante de Pago');
	$pdf->SetKeywords('Comprobante de Pago');

	// set default header data
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

	// set header and footer fonts
	$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins('15', '36', '15');
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
	$pdf->SetFooterMargin('5');//46//40
	// set auto page breaks
	$pdf->SetAutoPageBreak(true, 5);//31//25//38
	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	$pdf->SetFont('dejavusans', '', 13);

	$height = $pdf->getPageHeight();
	$width = $pdf->getPageWidth();

	// add a page   style="width: 300px; height: 200px;"
	$pdf->AddPage('L', 'A5');

	//'.($pdf->ValorEnLetras($saldo,'MXN')).'

	$html = '
				<style type="text/css">
					.border_f{ border:solid 1px #000099; padding-top: 8px; padding-bottom: 8px;}
					.p_space{ font-size: 2px;}
				</style>

				<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify; color: #000099;">
					<tbody>
						<tr>
							<td width="14%" >
								<b>RECIBIMOS DE:</b>
							</td>
							<td width="50%" >
								'.$cliente->nombre.' '.$cliente->app.' '.$cliente->apm.' 
							</td>
							<td width="10%" style="border-left:solid 1px #000099;">
								<b>EMPRESA:</b>
							</td>
							<td width="26%" >
								'.$cliente->empresa.'
							</td>
						</tr>
					</tbody>
				</table>';
	
	$html .= '
				<p class="p_space">&nbsp;</p>
				<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify; color: #000099;">
					<tbody>
						<tr>
							<td width="17%" >
								<b>LA CANTIDAD DE:</b>
							</td>
							<td width="58%">
								'.($pdf->ValorEnLetras($monto,'MXN')).'
							</td>
							<td width="3%" style="	center; border-bottom: solid 1px #000099; border-top: solid 1px #000099; border-left:solid 1px #000099; background-color: #deebf7;">
								<b><span style="font-size:13px;">$</span></b>
							</td>
							<td width="22%" style="border-bottom: solid 1px #000099; border-top: solid 1px #000099; border-right: solid 1px #000099; background-color: #deebf7;">
								'.number_format($monto, 2, '.', ',').'
							</td>
						</tr>
					</tbody>
				</table>';

	$html .= '
				<p class="p_space">&nbsp;</p>
				<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify; color: #000099;">
					<tbody>
						<tr>
							<td width="18%" >
								<b>POR CONCEPTO DE:</b>
							</td>
							<td width="57%">
								Pago a contrato 00'.$GLOBALS['folio_cont'].'
							</td>
							<td width="8%" style="border-left:solid 1px #000099;">
								<b><span style="font-size:8px;">FOLIO C.<br>INTERNO:</span></b>
							</td>
							<td width="17%">
								P-'.$GLOBALS['folio_pago'].' C-'.$GLOBALS['folio_cont'].'
							</td>
						</tr>
						<tr>
							<td width="100%" style="border-top: solid 1px #000099;">
								
							</td>
						</tr>
					</tbody>
				</table>';

	$html .= '
				<p class="p_space">&nbsp;</p>
				<table width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify; color: #000099;">
					<tbody>
						<tr>
							<td width="50%" >

								<table width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify; color: #000099;">
									<tbody>
										<tr>
											<td width="100%">
												<b>FORMA DE PAGO:</b>
											</td>
										</tr>
										<tr>
											<td width="5%" style="text-align: center;" class="border_f">
												<p class="p_space">&nbsp;</p>
												<b>'. $efectivo .'</b>
											</td>
											<td width="20%" class="border_f" style="text-align: center;">
												<p class="p_space">&nbsp;</p>
												<b>EFECTIVO</b>
											</td>
											<td width="2.5%">
											</td>

											<td width="5%" style="text-align: center;" class="border_f">
												<p class="p_space">&nbsp;</p>
												<b>'. $transferencia .'</b>
											</td>
											<td width="35%" class="border_f" style="text-align: center;">
												<b>TRANSFERENCIA<BR>INTERBANCARIA</b>
											</td>
											<td width="2.5%">
											</td>
											
											<td width="30%" class="border_f">
												<p class="p_space">&nbsp;</p>
												<b>CTA:</b> '.urldecode($cuenta).'
											</td>
										</tr>
										<tr>
											<td width="100%">
												<br><br><br>
											</td>
										</tr>

										<tr>
											<td width="20%">
											</td>
											<td width="60%" style="text-align: center; border-bottom: solid 1px #000099; " >
												'.$usuario->nombre.' '.$usuario->apellido_p.' '.$usuario->apellido_m.'
											</td>
											<td width="20%">
											</td>
										</tr>

										<tr>
											<td width="20%">
											</td>
											<td width="60%" style="text-align: center;" >
												<b>RECIBIDO POR:</b>
											</td>
											<td width="20%">
											</td>
										</tr>
									</tbody>
								</table>

							</td>
							<td width="50%">
								<table width="100%" class="border_f" border="0" style="padding: 2px; font-size: 10px; text-align: justify; color: #000099;">
									<tbody>
										
										<tr>
											<td width="40%" style="text-align: center; border-bottom: solid 1px #000099; border-right: solid 1px #000099; ">
												<b>SALDO ANTERIOR:</b>
											</td>
											<td width="4%" style="text-align: center; border-bottom: solid 1px #000099; border-top: solid 1px #000099; ">
												<b><span style="font-size:11px;">$</span></b>
											</td>
											<td width="56%" style="border-bottom: solid 1px #000099; border-right: solid 1px #000099;">
												'.number_format($pagado, 2, '.', ',').'
											</td>
										</tr>

										<tr>
											<td width="40%" style="text-align: center; border-bottom: solid 1px #000099; border-right: solid 1px #000099; ">
												<b>ANTICIPO NO. <u>&nbsp;'. $numIndex .'&nbsp;</u>:</b>
											</td>
											<td width="4%" style="text-align: center; border-bottom: solid 1px #000099;">
												<b><span style="font-size:11px;">$</span></b>
											</td>
											<td width="56%" style="border-bottom: solid 1px #000099; border-right: solid 1px #000099;">
												'.number_format($monto, 2, '.', ',').'
											</td>
										</tr>

										<tr>
											<td width="40%" style="text-align: center; border-bottom: solid 1px #000099; border-right: solid 1px #000099; ">
												<b>PAGADO A LA FECHA:</b>
											</td>
											<td width="4%" style="border-bottom: solid 1px #000099;">
												<b><span style="font-size:11px;">$</span></b>
											</td>
											<td width="56%" style="border-bottom: solid 1px #000099; border-right: solid 1px #000099;">
												'.number_format(($pagado + $monto), 2, '.', ',').'
											</td>
										</tr>

										<tr>
											<td width="40%" style="text-align: center; border-bottom: solid 1px #000099; border-right: solid 1px #000099; ">
												<b>SALDO ACTUAL:</b>
											</td>
											<td width="4%" style="border-bottom: solid 1px #000099;">
												<b><span style="font-size:11px;">$</span></b>
											</td>
											<td width="56%" style="border-bottom: solid 1px #000099; border-right: solid 1px #000099;">
												'.number_format(($saldo - ($pagado + $monto)), 2, '.', ',').'
											</td>
										</tr>
										
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>';
				



	$pdf->writeHTML($html, true, false, true, false, '');

	$pdf->IncludeJS('print(true);');
	//$pdf->Output('Orden_servicio.pdf', 'I');
	$pdf->Output(FCPATH.'public/pdf/Comprobante_pago_'. $id .'.pdf', 'FI');
	//$pdf->Output(FCPATH.'public/pdf/Orden_servicio_'. $id .'.pdf', 'I');