<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Reportes: Estados de Cuenta</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="mb-3 row">
              <div class="col-sm-4">
                <label class="col-sm-6 col-form-label">Cliente</label>
                <select class="form-control" id="id_cliente">
                </select>
              </div>
              <div class="col-sm-3">
                <label class="col-sm-6 col-form-label">Desde:</label>
                <input type="date" id="fechai" class="form-control">
              </div>
              <div class="col-sm-3">
                <label class="col-sm-6 col-form-label">Hasta:</label>
                <input type="date" id="fechaf" class="form-control">
              </div>

              <div class="col-md-1">
                <label class="col-sm-2 col-form-label" style="margin-top: 15px;"></label>
                <button id="search" type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
              </div>
              <div class="col-md-1">
                <label class="col-sm-2 col-form-label" style="margin-top: 15px;"></label>
                <button title="Exportar a Excel" id="export-excel" type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i></button>
              </div>
            </div>

            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table" id="table_data">
                  <thead>
                    <tr>
                      <th scope="col">Cliente</th>
                      <th scope="col">Contrato</th>
                      <th scope="col">Folio</th>
                      <th scope="col">Fecha registro</th>
                      <th scope="col">Fecha contrato</th>
                      <th scope="col">Total contrato</th>
                      <th scope="col">Total pagos</th>
                      <th scope="col">Restante</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
