	<?php

	require_once('TCPDF4/tcpdf.php');
	//$ruta_base = $_SERVER['DOCUMENT_ROOT'];
	$this->load->helper('url');

	$englishDays = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
	$spanishDays = array('Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo');
	$englishMonths = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
	$spanishMonths = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
	$spanishMonthsMin = array('Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');


	$GLOBALS['header'] = FCPATH . "public/img/relacion_gastos/header.png";
	$GLOBALS['folio_cont'] = $folio_cont;
	$GLOBALS['folio_coti'] = $folio_coti;
	$GLOBALS['folio_oc'] = '';
	$GLOBALS['usuario'] = $usuario->nombre.' '.$usuario->apellido_p.' '.$usuario->apellido_m;
	$GLOBALS['fecha_hora_imp'] = $fecha_hora_imp;

	$border_caseta = FCPATH . "public/img/relacion_gastos/borde_caseta.png";
	$border_combustible = FCPATH . "public/img/relacion_gastos/borde_combustible.png";
	$border_otros = FCPATH . "public/img/relacion_gastos/borde_otros.png";

	$empresa = $contratos->empresa;
	//$empresa = $clientes->empresa;

	$cliente = $clientes->nombre." ".$clientes->app." ".$clientes->apm;
	$GLOBALS['cliente'] = $cliente;
	$telefono = $clientes->telefono;


	$notas = '<br><br><br><br>';
	if(isset($obs->observaciones) && $notas != ''){
		$notas = $obs->observaciones;
	}

	$cant_recibida = '0';
	if(isset($obs->cant_recibida)){
		$cant_recibida = $obs->cant_recibida;
	}

/*
	$subtotal = 0;
	if (count($unidades) > 0) {
		foreach ($unidades->result() as $unidad) {
			$subtotal = $subtotal + ($unidad->monto * $unidad->cantidad);
		}
	}
*/


	$porc_anticipo = 0;
	if(isset($contratos->porc_anticipo)){
		$porc_anticipo = $contratos->porc_anticipo;	
	}

	$monto_anticipo = 0;
	if(isset($contratos->monto_anticipo)){
		$monto_anticipo = $contratos->monto_anticipo;
	}

	$monto_descuento = 0;
	if(isset($contratos->porc_desc)){
		$monto_descuento = $contratos->porc_desc;
	}

	$nombrePersonal ='';
	if(isset($personal)){
		$nombrePersonal = $personal->nombre.' '.$personal->apellido_p.' '.$personal->apellido_m;
	}

	$operadores = '';

	$total_caseta = 0;
	$total_combustible = 0;
	$total_otro = 0;

	//Registro
	$reg = substr($contratos->fecha_reg, 0, 10);
	$fecha_reg = new DateTime($reg);

	$dia_reg = $fecha_reg->format("j");
	$anio_reg = $fecha_reg->format("Y");
	$mes_reg = $fecha_reg->format("F");
	$mes_reg = str_replace($englishMonths, $spanishMonthsMin, $mes_reg);

	//pagoFecha
	$dia_pago = '--';
	$anio_pago = '--';
	$mes_pago = '--';

	if(isset($pagoFecha->fecha)){
		$fecha_pago = new DateTime($pagoFecha->fecha);

		$dia_pago = $fecha_pago->format("j");
		$anio_pago = $fecha_pago->format("Y");
		$mes_pago = $fecha_pago->format("F");
		$mes_pago = str_replace($englishMonths, $spanishMonthsMin, $mes_pago);
	}


	//=======================================================================================
	class MYPDF extends TCPDF
	{
		//Page header
		public function Header()
		{	
			$this->Image($GLOBALS['header'], 8, 3, $this->getPageWidth() - 16, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
			$this->SetY(19.3);
			$this->SetX(-36);//-50 -> 0000
			$this->SetFont('times', 'B', 12);
			$this->SetTextColor(192, 0, 0);
			$this->Cell(0, 5, $GLOBALS['folio_cont'], 0, false, 'C', 0, '', 0, false, 'M', 'M');

			$this->SetY(19.3);
			$this->SetX(-96);//-50 -> 0000
			$this->SetFont('helvetica', 'BI', 12);
			$this->SetTextColor(51, 153, 255);
			$this->Cell(0, 5, $GLOBALS['folio_coti'], 0, false, 'C', 0, '', 0, false, 'M', 'M');

			$this->SetY(10.5);
			$this->SetX(-69);//-50 -> 0000
			$this->SetFont('helvetica', 'B', 12);
			$this->SetTextColor(40, 40, 248);
			$this->Cell(0, 5, $GLOBALS['folio_oc'], 0, false, 'C', 0, '', 0, false, 'M', 'M');
		}

		// Page footer
		public function Footer()
		{
			$html = '<p style="text-align:rigth; font-size:8.5px"> IMPRESO POR: '.$GLOBALS['usuario'].' &nbsp;&nbsp;&nbsp; FECHA Y HORA: '.$GLOBALS['fecha_hora_imp'].'</p>';
			$this->writeHTML($html, true, false, true, false, '');
		}


	}
	$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Mangoo');
	$pdf->SetTitle('Relacion de gastos');
	$pdf->SetSubject('Relacion de gastos');
	$pdf->SetKeywords('Relacion de gastos');

	// set default header data
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

	// set header and footer fonts
	$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins('10', '26', '10');
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
	$pdf->SetFooterMargin('8');
	// set auto page breaks
	$pdf->SetAutoPageBreak(true, 12);
	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	$pdf->SetFont('dejavusans', '', 13);

	$height = $pdf->getPageHeight();
	$width = $pdf->getPageWidth();

	// add a page   style="width: 300px; height: 200px;"
	$pdf->AddPage('P', 'A4');

	$html = '
				<style type="text/css">
					.border_f{ border:solid 5px black; }
					.p_space{ font-size: 1px;}
					.title_borde{width: 20px; height: 50px;}
				</style>
				';

	$html .= '
				<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
					<thead>
						<tr>
							<td style="color:#ffffff; font-size: 12px; text-align: center; background-color:#7f7f7f;">
								<b>DATOS DEL OPERADOR ASIGNADO:</b>
							</td>
						</tr>
					</thead>
					<tbody>';

	if (count($choferes) > 0) {
		foreach ($choferes as $chofer) {
			$operadores .= $chofer->nombre.' '.$chofer->apellido_p.' '.$chofer->apellido_m.'<br>';
	$html .= '<tr>
							<td width="19%">
								&nbsp;OPERADOR ASIGNADO:
							</td>
							<td width="50%" style="border-bottom:solid 1px black;">
								'.$chofer->nombre.' '.$chofer->apellido_p.' '.$chofer->apellido_m.'
							</td>

							<td width="10%">
								TELEFONO:
							</td>
							<td width="20%" style="border-bottom:solid 1px black;">
								'.$chofer->telefono.'
							</td>
							<td width="1%" ></td>
						</tr>

						<tr>
							<td width="19%">
								&nbsp;VEHÍCULO ASIGNADO:
							</td>
							<td width="50%" style="border-bottom:solid 1px black;">
								'.$chofer->vehiculo.'
							</td>

							<td width="31%"></td>
						</tr>

						<tr>
							<td width="8%">
								&nbsp;MARCA:
							</td>
							<td width="30%" style="border-bottom:solid 1px black;">
								'.$chofer->marca.'
							</td>

							<td width="8%">
								MODELO:
							</td>
							<td width="23%" style="border-bottom:solid 1px black;">
								'.$chofer->modelo.'
							</td>

							<td width="7%">
								PLACAS:
							</td>
							<td width="23%" style="border-bottom:solid 1px black;">
								'.$chofer->placas.'
							</td>
							<td width="1%" ></td>
						</tr>
						<tr>
							<td width="100%" style="border-bottom:solid 1px black;"><p class="p_space">&nbsp;</p></td>
						</tr>';
		}
	}
	
	if(count($unidades) > 0){
		foreach ($unidades->result() as $unidad) {
			$idU = $unidad->id;

			$choferIsset = array_filter($choferes, function($chofer) use ($idU) {
				return $chofer->idUnidPros == $idU;
			});

			if (empty($choferIsset)) {
				
				$html .= '<tr>
								<td width="19%">
									&nbsp;OPERADOR ASIGNADO:
								</td>
								<td width="50%" style="border-bottom:solid 1px black;">
									--- SIN ASIGNACIÓN ---
								</td>

								<td width="10%">
									TELEFONO:
								</td>
								<td width="20%" style="border-bottom:solid 1px black;">
									---
								</td>
								<td width="1%" ></td>
							</tr>

							<tr>
								<td width="19%">
									&nbsp;VEHÍCULO ASIGNADO:
								</td>
								<td width="50%" style="border-bottom:solid 1px black;">
									'.$unidad->vehiculo.'
								</td>

								<td width="31%"></td>
							</tr>

							<tr>
								<td width="8%">
									&nbsp;MARCA:
								</td>
								<td width="30%" style="border-bottom:solid 1px black;">
									'.$unidad->marca.'
								</td>

								<td width="8%">
									MODELO:
								</td>
								<td width="23%" style="border-bottom:solid 1px black;">
									'.$unidad->modelo.'
								</td>

								<td width="7%">
									PLACAS:
								</td>
								<td width="23%" style="border-bottom:solid 1px black;">
									'.$unidad->placas.'
								</td>
								<td width="1%" ></td>
							</tr>
							<tr>
								<td width="100%" style="border-bottom:solid 1px black;"><p class="p_space">&nbsp;</p></td>
							</tr>';

			}

		}
	}else{
		$html .= '<tr>
								<td width="19%">
									&nbsp;OPERADOR ASIGNADO:
								</td>
								<td width="50%" style="border-bottom:solid 1px black;">
								</td>

								<td width="10%">
									TELEFONO:
								</td>
								<td width="20%" style="border-bottom:solid 1px black;">
								</td>
								<td width="1%" ></td>
							</tr>

							<tr>
								<td width="19%">
									&nbsp;VEHÍCULO ASIGNADO:
								</td>
								<td width="50%" style="border-bottom:solid 1px black;">
								</td>

								<td width="31%"></td>
							</tr>

							<tr>
								<td width="8%">
									&nbsp;MARCA:
								</td>
								<td width="30%" style="border-bottom:solid 1px black;">
								</td>

								<td width="8%">
									MODELO:
								</td>
								<td width="23%" style="border-bottom:solid 1px black;">
								</td>

								<td width="7%">
									PLACAS:
								</td>
								<td width="23%" style="border-bottom:solid 1px black;">
								</td>
								<td width="1%" ></td>
							</tr>';
	}


	$html .= '</tbody>
				</table>';


	$html .= '<br><br>
				<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
					<thead>
						<tr>
							<td class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#7f7f7f;">
								<b>DATOS DEL SERVICIO:</b>
							</td>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td width="100%"><p class="p_space">&nbsp;</p></td>
						</tr>

						<tr>
							<td width="1%"></td>
							<td width="98%" style="text-align: center; background-color:#d9d9d9; border:solid 1px black;">
								ITINERARIO:
							</td>
							<td width="1%"></td>
						</tr>';

	if (count($destinos) > 0) {
		$hora_sal = '';
		$fecha_sal = '';
		foreach ($destinos as $destino) {

			$fecha_s = $destino->fecha;
			$sal = new DateTime($fecha_s);
			$dia_sal = $sal->format("l");
			$dia_sal = str_replace($englishDays, $spanishDays, $dia_sal);


			$fecha_s_dia = $sal->format("j");
			$fecha_s_anio = $sal->format("Y");
			$fecha_s_mes = $sal->format("F");
			$fecha_s_mes = str_replace($englishMonths, $spanishMonths, $fecha_s_mes);

			$fecha_sal = $dia_sal . ', ' .$fecha_s_dia.' de '. $fecha_s_mes.' de '. $fecha_s_anio;
			$hora_sal = date('H:i', strtotime($destino->hora)).' Hrs';

			$fecha_s_dia = date('d', strtotime($contratos->fecha_regreso));
			$fecha_s_anio = date('Y', strtotime($contratos->fecha_regreso));
			$fecha_s_mes = date('m', strtotime($contratos->fecha_regreso));

			$html .='<tr>
							<td width="1%"></td>
							<td width="98%" style="border:solid 1px black; text-align: center">
								'.$destino->lugar.' - '.$fecha_sal.' - '.$hora_sal.' - '.$destino->lugar_hospeda.'
							</td>
							<td width="1%"></td>
						</tr>';
		}
	}else{
			$html .='<tr>
								<td width="1%"></td>
								<td width="98%" style="border:solid 1px black;"></td>
								<td width="1%"></td>
							</tr>
							<tr>
								<td width="1%"></td>
								<td width="98%" style="border:solid 1px black;"></td>
								<td width="1%"></td>
							</tr>
							<tr>
								<td width="1%"></td>
								<td width="98%" style="border:solid 1px black;"></td>
								<td width="1%"></td>
							</tr>';
	}

	$html .= 	'<tr>
							<td width="100%"><p class="p_space">&nbsp;</p></td>
						</tr>
					</tbody>
				</table>';
				


					$html .= '<br><br>
					<table width="100%" border="1" style="padding: 0.5px; font-size: 10px; text-align: justify;">
						<tbody>
							<tr>
								<td width="4%" style="background-color:#7f7f7f; text-align: center;">
									<img class="title_borde" src="'.$border_caseta.'">
								</td>
								<td width="96%">
								
									<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
										<thead>
											<tr>
												<td width="15%" style="text-align: center; background-color:#d9d9d9; border:solid 1px black;">
													FECHA
												</td>
												<td width="70%" style="text-align: center; background-color:#d9d9d9; border:solid 1px black;">
													DESCRIPCIÓN
												</td>
												<td width="15%" colspan="2" style="text-align: center; background-color:#d9d9d9; border:solid 1px black;">
													IMPORTE
												</td>
											</tr>
										</thead>
										<tbody>';

if (count($casetas->result()) > 0) {
	foreach ($casetas->result() as $caseta) {
		$total_caseta = $total_caseta + $caseta->importe;

		$fecha_caseta = new DateTime($caseta->fecha);
		$dia_caseta = $fecha_caseta->format("j");
		$anio_caseta = $fecha_caseta->format("Y");
		$mes_caseta = $fecha_caseta->format("F");
		$mes_caseta = str_replace($englishMonths, $spanishMonthsMin, $mes_caseta);

						$html .= '<tr>
												<td width="15%" style="text-align: center; border:solid 1px black;">
													'.$dia_caseta.'/'.$mes_caseta.'/'.$anio_caseta.'
												</td>
												<td width="70%" style="text-align: left; border:solid 1px black;">
													'.$caseta->descripcion.'
												</td>
												<td width="3%" style="text-align: center; border-bottom:solid 1px black;">
													$
												</td>
												<td width="12%" style="text-align: right; border-bottom:solid 1px black;">
													'.number_format($caseta->importe, 2, '.', ',').'
												</td>
											</tr>';
	}
}

						$html .= '<tr style="font-size: 12px;">
												<td width="52%">
												</td>
												<td width="33%" style="text-align: center;  background-color:#d9d9d9; border:solid 1px black;">
													<b>TOTAL DE CASETAS</b>
												</td>
												<td width="3%" style="text-align: center; border-bottom:solid 1px black;">
													$
												</td>
												<td width="12%" style="text-align: right; border-bottom:solid 1px black;">
													'.number_format($total_caseta, 2, '.', ',').'
												</td>
											</tr>
										</tbody>
									</table>

								</td>
							</tr>
						</tbody>
					</table>';


					
					$html .= '<br><br>
					<table width="100%" border="1" style="padding: 0.5px; font-size: 10px; text-align: justify;">
						<tbody>
							<tr>
								<td width="4%" style="background-color:#7f7f7f; text-align:center;">
									<img class="title_borde" src="'.$border_combustible.'">
								</td>
								<td width="96%">
								
									<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
										<thead>
											<tr>
												<td width="15%" style="text-align: center; background-color:#d9d9d9; border:solid 1px black;">
													FECHA
												</td>
												<td width="70%" style="text-align: center; background-color:#d9d9d9; border:solid 1px black;">
													DESCRIPCIÓN
												</td>
												<td width="15%" colspan="2" style="text-align: center; background-color:#d9d9d9; border:solid 1px black;">
													IMPORTE
												</td>
											</tr>
										</thead>
										<tbody>';

if (count($combustibles->result()) > 0) {
	foreach ($combustibles->result() as $combustible) {
		$total_combustible = $total_combustible + $combustible->importe;

		$fecha_combustible = new DateTime($combustible->fecha);
		$dia_combustible = $fecha_combustible->format("j");
		$anio_combustible = $fecha_combustible->format("Y");
		$mes_combustible = $fecha_combustible->format("F");
		$mes_combustible = str_replace($englishMonths, $spanishMonthsMin, $mes_combustible);

		$html .= '<tr>
												<td width="15%" style="text-align: center; border:solid 1px black;">
													'.$dia_combustible.'/'.$mes_combustible.'/'.$anio_combustible.'
												</td>
												<td width="70%" style="text-align: left; border:solid 1px black;">
													'.$combustible->descripcion.'
												</td>
												<td width="3%" style="text-align: center; border-bottom:solid 1px black;">
													$
												</td>
												<td width="12%" style="text-align: right; border-bottom:solid 1px black;">
													'.number_format($combustible->importe, 2, '.', ',').'
												</td>
											</tr>';
	}
}
										
						$html .= '<tr style="font-size: 12px;">
												<td width="52%">
												</td>
												<td width="33%" style="text-align: center;  background-color:#d9d9d9; border:solid 1px black;">
													<b>TOTAL DE COMBUSTIBLE</b>
												</td>
												<td width="3%" style="text-align: center; border-bottom:solid 1px black;">
													$
												</td>
												<td width="12%" style="text-align: right; border-bottom:solid 1px black;">
													'.number_format($total_combustible, 2, '.', ',').'
												</td>
											</tr>

										</tbody>
									</table>

								</td>
							</tr>
						</tbody>
					</table>';


					
					$html .= '<br><br>
					<table width="100%" border="1" style="padding: 0.5px; font-size: 10px; text-align: justify;">
						<tbody>
							<tr>
								<td width="4%" style="background-color:#7f7f7f; text-align:center;">
									<img class="title_borde" src="'.$border_otros.'">
								</td>
								<td width="96%">
								
									<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
										<thead>
											<tr>
												<td width="15%" style="text-align: center; background-color:#d9d9d9; border:solid 1px black;">
													FECHA
												</td>
												<td width="70%" style="text-align: center; background-color:#d9d9d9; border:solid 1px black;">
													DESCRIPCIÓN
												</td>
												<td width="15%" colspan="2" style="text-align: center; background-color:#d9d9d9; border:solid 1px black;">
													IMPORTE
												</td>
											</tr>
										</thead>
										<tbody>';

if (count($otros->result()) > 0) {
	foreach ($otros->result() as $otro) {
		$total_otro = $total_otro + $otro->importe;

		$fecha_otro = new DateTime($otro->fecha);
		$dia_otro = $fecha_otro->format("j");
		$anio_otro = $fecha_otro->format("Y");
		$mes_otro = $fecha_otro->format("F");
		$mes_otro = str_replace($englishMonths, $spanishMonthsMin, $mes_otro);

		$html .= '<tr>
												<td width="15%" style="text-align: center; border:solid 1px black;">
													'.$dia_otro.'/'.$mes_otro.'/'.$anio_otro.'
												</td>
												<td width="70%" style="text-align: left; border:solid 1px black;">
													'.$otro->descripcion.'
												</td>
												<td width="3%" style="text-align: center; border-bottom:solid 1px black;">
													$
												</td>
												<td width="12%" style="text-align: right; border-bottom:solid 1px black;">
													'.number_format($otro->importe, 2, '.', ',').'
												</td>
											</tr>';
	}
}
										
						$html .= '<tr style="font-size: 12px;">
												<td width="52%">
												</td>
												<td width="33%" style="text-align: center; background-color:#d9d9d9; border:solid 1px black;">
													<b>TOTAL DE OTROS</b>
												</td>
												<td width="3%" style="text-align: center; border-bottom:solid 1px black;">
													$
												</td>
												<td width="12%" style="text-align: right; border-bottom:solid 1px black;">
													'.number_format($total_otro, 2, '.', ',').'
												</td>
											</tr>

										</tbody>
									</table>

								</td>
							</tr>
						</tbody>
					</table>';
	
	



										
					$html .= '<br><br>
					<table width="100%" border="0" cellspacing="2" style="padding: 2px; font-size: 10px; text-align: justify;">
						<tbody>
							<tr>
								<td width="50%">

									<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
										<thead>
											<tr>
												<td class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#7f7f7f;">
													<b>OBSERVACIONES:</b>
												</td>
											</tr>
										</thead>
				
										<tbody>
											<tr>
												<td width="100%" >
													<i>'.$notas.'</i>
												</td>
											</tr>
										</tbody>
									</table>

								</td>

								<td width="50%">
									<table width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
										<thead>
											<tr>
												<td width="70%" class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#7f7f7f;">
													<b>TOTAL DE GASTOS (1)</b>
												</td>

												<td width="5%" style="background-color:#d9d9d9; font-size: 12px; text-align: center; border-bottom:solid 1px black; border-top:solid 1px black;">
													$
												</td>

												<td width="25%" style="background-color:#d9d9d9; font-size: 12px; text-align: right; border-bottom:solid 1px black; border-top:solid 1px black; border-right:solid 1px black; ">
													'.number_format(($total_caseta + $total_combustible + $total_otro), 2, '.', ',').'
												</td>
											</tr>


											<tr>
												<td></td>
											</tr>

											<tr>
												<td width="100%" colsapan="4" class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#7f7f7f;">
													<b>ANTICIPOS</b>
												</td>
											</tr>

											<tr>
												<td width="30%" class="border_f" style="text-align: center; background-color:#d9d9d9;">
													FECHA
												</td>
												<td width="40%" class="border_f" style="text-align: center; background-color:#d9d9d9;">
													DESCRIPCIÓN
												</td>
												<td width="30%" colspan="2" class="border_f" style="text-align: center; background-color:#d9d9d9;">
													IMPORTES
												</td>
											</tr>
										</thead>
				
										<tbody>
											<tr>
												<td width="30%" class="border_f" style="text-align: center;">
													
												</td>
												<td width="40%" class="border_f" style="text-align: center;">
													CLIENTE
												</td>
												<td width="5%" style="text-align: center; border-top:solid 1px black;">
													$
												</td>
												<td width="25%" style="text-align: right; border-top:solid 1px black; border-right:solid 1px black;">
													'.number_format("0", 2, '.', ',').'
												</td>
											</tr>
											<tr>
												<td width="30%" class="border_f" style="text-align: center;">
													
												</td>
												<td width="40%" class="border_f" style="text-align: center;">
													OFICINA
												</td>
												<td width="5%" style="text-align: center; border-top:solid 1px black;">
													$
												</td>
												<td width="25%" style="text-align: right; border-top:solid 1px black; border-right:solid 1px black;">
													'.number_format("0", 2, '.', ',').'
												</td>
											</tr>
											<tr>
												<td width="30%" class="border_f" style="text-align: center;">

												</td>
												<td width="40%" class="border_f" style="text-align: center;">
													DEPÓSITOS
												</td>
												<td width="5%" style="text-align: center; border-top:solid 1px black;">
													$
												</td>
												<td width="25%" style="text-align: right; border-top:solid 1px black; border-right:solid 1px black;">
													'.number_format("0", 2, '.', ',').'
												</td>
											</tr>
											<tr>
												<td width="30%" class="border_f" style="text-align: center;">
													
												</td>
												<td width="40%" class="border_f" style="text-align: center;">
													OTROS
												</td>
												<td width="5%" style="text-align: center; border-top:solid 1px black;">
													$
												</td>
												<td width="25%" style="text-align: right; border-top:solid 1px black; border-right:solid 1px black;">
													'.number_format("0", 2, '.', ',').'
												</td>
											</tr>

											<tr>
												<td width="30%" class="border_f" style="text-align: center;">
													'.$dia_reg.'/'.$mes_reg.'/'.$anio_reg.'
												</td>
												<td width="40%" class="border_f" style="text-align: center;">
													ANTICIPO
												</td>
												<td width="5%" style="text-align: center; border-top:solid 1px black;">
													$
												</td>
												<td width="25%" style="text-align: right; border-top:solid 1px black; border-right:solid 1px black;">
													'.number_format($monto_anticipo, 2, '.', ',').'
												</td>
											</tr>

											<tr>
												<td width="30%" class="border_f" style="text-align: center;">
													'.$dia_reg.'/'.$mes_reg.'/'.$anio_reg.'
												</td>
												<td width="40%" class="border_f" style="text-align: center;">
													DESCUENTOS
												</td>
												<td width="5%" style="text-align: center; border-top:solid 1px black;">
													$
												</td>
												<td width="25%" style="text-align: right; border-top:solid 1px black; border-right:solid 1px black;">
													'.number_format($monto_descuento, 2, '.', ',').'
												</td>
											</tr>

											<tr>
												<td width="30%" class="border_f" style="text-align: center;">
													'.$dia_pago.'/'.$mes_pago.'/'.$anio_pago.'
												</td>
												<td width="40%" class="border_f" style="text-align: center;">
													PAGOS
												</td>
												<td width="5%" style="text-align: center; border-top:solid 1px black;">
													$
												</td>
												<td width="25%" style="text-align: right; border-top:solid 1px black; border-right:solid 1px black;">
													'.number_format($pago, 2, '.', ',').'
												</td>
											</tr>

											<tr>
												<td width="70%" class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#7f7f7f;">
													<b>TOTAL DE ANTICIPOS (2)</b>
												</td>

												<td width="5%" style="background-color:#d9d9d9; font-size: 12px; text-align: center; border-bottom:solid 1px black; border-top:solid 1px black;">
													$
												</td>

												<td width="25%" style="background-color:#d9d9d9; font-size: 12px; text-align: right; border-bottom:solid 1px black; border-top:solid 1px black; border-right:solid 1px black; ">
													'.number_format((($monto_anticipo+$pago)-$monto_descuento), 2, '.', ',').'
												</td>
											</tr>
										</tbody>
									</table>
								
								</td>
							</tr>
						</tbody>
					</table>';



					$html .= '<br><br>
					<table width="100%" border="0" cellspacing="2" style="padding: 2px; font-size: 10px; text-align: justify;">
						<tbody>
							<tr>
								<td width="50%">

									<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align:center;">
										<tbody>
											<tr>
												<td width="5%"> </td>
												<td width="90%" style="font-size: 8px;">
													<br><br><br><br>
													<i>Recibí de Operadora Turística Grand Turismo Express S.A. de C.V. la cantidad <b>$ '.number_format($cant_recibida, 2, '.', ',').'</b></i>
													<br><br><br><br>
												</td>
												<td width="5%"> </td>
											</tr>
										</tbody>
									</table>

								</td>

								<td width="50%">
									<table width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
										<thead>
											<tr>
												<td width="70%" class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#7f7f7f;">
													<br><br><br>
													<b>NETO A FAVOR DE (1) (2)</b>
													<br><br><br>
												</td>

												<td width="5%" style="background-color:#d9d9d9; font-size: 12px; text-align: center; border-bottom:solid 1px black; border-top:solid 1px black;">
													<br><br><br>
													<b>$</b>
													<br><br><br>
												</td>

												<td width="25%" style="background-color:#d9d9d9; font-size: 12px; text-align: right; border-bottom:solid 1px black; border-top:solid 1px black; border-right:solid 1px black; ">
													<br><br><br>
													<b>'.number_format(((($monto_anticipo+$pago)-$monto_descuento)-($total_caseta+$total_combustible+$total_otro)), 2, '.', ',').'</b>
													<br><br><br>
												</td>
											</tr>
										</thead>
									</table>
								
								</td>
							</tr>
						</tbody>
					</table>';



					$html .= '<br><br>
					<table width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
						<thead>
							<tr>
								<td width="100%" class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#7f7f7f;">
									<b>NOMBRE DEL OPERADOR</b>
								</td>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td width="100%" class="border_f" style="text-align: left;">
									'.$operadores.'
								</td>
							</tr>
						</tbody>
					</table>';


					$html .= '<br><br>
					<table width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
						<thead>
							<tr>
								<td width="50%" class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#7f7f7f;">
									<b>AUTORIZADO POR:</b>
								</td>

								<td width="30%" class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#7f7f7f;">
									<b>FIRMA</b>
								</td>

								<td width="20%" class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#7f7f7f;">
									<b>FECHA</b>
								</td>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td width="50%" class="border_f" style="text-align: center;">
									'.$nombrePersonal.'
								</td>

								<td width="30%" class="border_f" style="text-align: center;">
									
								</td>

								<td width="20%" class="border_f" style="text-align: center;">
									
								</td>
							</tr>
						</tbody>
					</table>';



	$pdf->writeHTML($html, true, false, true, false, '');

	$pdf->IncludeJS('print(true);');
	//$pdf->Output('Orden_servicio.pdf', 'I');
	$pdf->Output(FCPATH.'public/pdf/Relacion_gastos_'. $folio_cont .'.pdf', 'FI');
	//$pdf->Output(FCPATH.'public/pdf/Orden_servicio_'. $id .'.pdf', 'I');