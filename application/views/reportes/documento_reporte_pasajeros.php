<?php

require_once('TCPDF4/tcpdf.php');
//$ruta_base = $_SERVER['DOCUMENT_ROOT'];
$this->load->helper('url');

$fecha_dia = date('d');
$fecha_anio = date('Y');
$fecha_mes = date('m');
switch ($fecha_mes) {
	case 1:
			$nombre_mes = 'Enero';
			break;
	case 2:
			$nombre_mes = 'Febrero';
			break;
	case 3:
			$nombre_mes = 'Marzo';
			break;
	case 4:
			$nombre_mes = 'Abril';
			break;
	case 5:
			$nombre_mes = 'Mayo';
			break;
	case 6:
			$nombre_mes = 'Junio';
			break;
	case 7:
			$nombre_mes = 'Julio';
			break;
	case 8:
			$nombre_mes = 'Agosto';
			break;
	case 9:
			$nombre_mes = 'Septiembre';
			break;
	case 10:
			$nombre_mes = 'Octubre';
			break;
	case 11:
			$nombre_mes = 'Noviembre';
			break;
	case 12:
			$nombre_mes = 'Diciembre';
			break;
	default:
			$nombre_mes = ' --- ';
			break;
}

$GLOBALS['idC'] = $idC;
$GLOBALS['folio'] = $contratos->folio;

//-Contrato------------------>
	//$telefono = $contratos->telefono;
	//$correo = $contratos->correo;
	$empresa = $contratos->empresa;
	//$domicilio = $contratos->direccion;
	//$domicilio = "Por confirmar";

//- PROSPECTO ----------------->
	$id = $prospecto->id;
	$nombre = $prospecto->nombre;	
  $app = $prospecto->app;
  $apm = $prospecto->apm;
  $telefono = $prospecto->telefono;
  $correo = $prospecto->correo;
	//$empresa = $prospecto->empresa;
	$direccion = $prospecto->direccion;


$GLOBALS['logo'] = FCPATH . "public/img/cotizaciones/logo.png";

$rutaImg = FCPATH . "public/uploads/unidades/";

$GLOBALS['foot'] = FCPATH . "public/img/cotizaciones/footer.png";

//=======================================================================================
class MYPDF extends TCPDF
{

	//Page header
	public function Header()
	{
		$html = '
			<table width="100%" border="0" RULES="rows" style="padding: 2px;" class="table table-striped">
				<tr>
					<td width="25%" style="color:black; text-align: left;">
						<img src="' . $GLOBALS['logo'] . '" >
					</td>
					<td width="35%"></td>
					<td width="40%" style="color:black; font-size: 10px; text-align: center;">
						<br><br><br>
						<table>
							<tbody>
								<tr>
									<td width="100%" border="1" style="background-color:#002060; color:white; font-size: 10px; text-align: center;"> <b><span style="font-size: 16px;">R</span>EPORTE DE PASAJEROS</b> </td>
								</tr>
								<tr>
									<td width="50%" border="1" style="font-size: 10px; text-align: right;"> <b>N</b>ÚMERO DE CONTRATO &nbsp; </td>
									<td width="50%" border="1" style="font-size: 9px; text-align: center;"> '.$GLOBALS['folio'].' </td>
								</tr>
								<tr>
									<td width="50%" border="1" style="font-size: 10px; text-align: right;"> <b>T</b>IPO DE SERVICIO &nbsp; </td>
									<td width="50%" border="1" style="font-size: 9px; text-align: center;"> TRANSPORTE TURÍSTICO </td>
								</tr>
								<tr>
									<td width="50%" border="1" style="font-size: 10px; text-align: right;"> <b>P</b>ÁGINA &nbsp; </td>
									<td width="50%" border="1" style="font-size: 9px; text-align: center;"> '.$this->getAliasNumPage().' DE '.$this->getAliasNbPages().' </td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</table>
		';
		$this->writeHTML($html, true, false, true, false, '');
	}

	// Page footer
	public function Footer()
	{
		// Calcular la posición Y de la imagen
		$imageY = $this->getPageHeight() - 41;

		// Agregar la imagen al final de la página
		$this->Image($GLOBALS['foot'], 0, $imageY, $this->getPageWidth(), '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
	}
}

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo');
$pdf->SetTitle('Reporte pasajeros');
$pdf->SetSubject('Reporte pasajeros');
$pdf->SetKeywords('Reporte pasajeros');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('8', '40', '8');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
$pdf->SetFooterMargin(45);
// set auto page breaks
$pdf->SetAutoPageBreak(true, 45);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 13);

$height = $pdf->getPageHeight();
$width = $pdf->getPageWidth();

// add a page   style="width: 300px; height: 200px;"
$pdf->AddPage('P', 'A4');

$html = '
			<style type="text/css">
				.pspaces{ font-size: 3px;}
			</style>

			<table width="100%" border="0" RULES="rows" style="padding: 2px;" class="table table-striped">
				<tr>
					<td width="20%"></td>

					<td width="60%" style="color:black; font-size: 10px; text-align: center;">
						<table width="100%">
							<tbody>
								<tr>
									<td width="40%" style="font-size: 10px; text-align: left; border-bottom:1px solid grey; border-right:1px solid grey;"> FECHA: </td>
									<td width="60%" style="font-size: 9px; text-align: left; border-bottom:1px solid grey;">  &nbsp; Puebla, Pue. a '.$fecha_dia.' de '.$nombre_mes.' de '.$fecha_anio.' </td>
								</tr>
								<tr>
									<td width="40%" style="font-size: 10px; text-align: left; border-bottom:1px solid grey; border-right:1px solid grey;"> COMPAÑÍA / EMPRESA: </td>
									<td width="60%" style="font-size: 9px; text-align: left; border-bottom:1px solid grey;">  &nbsp; '.$empresa.' </td>
								</tr>
								<tr>
									<td width="40%" style="font-size: 10px; text-align: left; border-bottom:1px solid grey; border-right:1px solid grey;"> CONTACTO: </td>
									<td width="60%" style="font-size: 9px; text-align: left; border-bottom:1px solid grey;">  &nbsp; '.$nombre.' '.$app.' '.$apm.' </td>
								</tr>
								<tr>
									<td width="40%" style="font-size: 10px; text-align: left; border-bottom:1px solid grey; border-right:1px solid grey;"> DOMICILIO: </td>
									<td width="60%" style="font-size: 9px; text-align: left; border-bottom:1px solid grey;">  &nbsp; '.$direccion.' </td>
								</tr>
								<tr>
									<td width="40%" style="font-size: 10px; text-align: left; border-bottom:1px solid grey; border-right:1px solid grey;"> TELEFONO: </td>
									<td width="60%" style="font-size: 9px; text-align: left; border-bottom:1px solid grey;">  &nbsp; +52 '.$telefono.' </td>
								</tr>
								<tr>
									<td width="40%" style="font-size: 10px; text-align: left; border-bottom:1px solid grey; border-right:1px solid grey;"> E-MAIL: </td>
									<td width="60%" style="font-size: 9px; text-align: left; border-bottom:1px solid grey;">  &nbsp; '.$correo.' </td>
								</tr>
							</tbody>
						</table>
					</td>

					<td width="20%"></td>
				</tr>

				<tr>
					<td width="100%" style="font-size: 8px; text-align: center;">
							<p class="pspaces-t2">&nbsp;</p>
							REPORTE DE PASAJEROS
					</td>
				</tr>
				<tr>
					<td>
						<table>
							<thead>
								<tr style="background-color:#002060; color:white; font-size: 7.5px; text-align: center;">
									<td width="5%" border="1"> CANT. </td>
									<td width="30%" border="1"> UNIDAD </td>
									<td width="65%" border="1"> PASAJEROS </td>
								</tr>
							</thead>
							<tbody>';
foreach ($unidades as $unidad) {
	$pasajeros = '';
	
	if($unidad->cant_pasajeros > 0) {
		$unidad->cant_pasajeros = $unidad->cant_pasajeros * $unidad->cantidad;
		$total_pasajeros = min($unidad->cant_pasajeros, count($unidad->pasajeros));
		for ($i = 0; $i < $total_pasajeros; $i++) {
			$pasajeros .= '&nbsp;&nbsp;&nbsp;#'.($i+1).': '.$unidad->pasajeros[$i]->pasajero.'<br>';
		}
	}

	$html .= '
								<tr style="font-size: 7.5px; text-align: center;">
									<td width="5%" border="1"><p class="pspaces">&nbsp;</p>'.$unidad->cantidad.' </td>
									<td width="30%" border="1"><p class="pspaces">&nbsp;</p>'.$unidad->vehiculo.' </td>
									<td width="65%" border="1" style="text-align: justify;"><p class="pspaces">&nbsp;</p>'.$pasajeros.' </td>
									</tr>';
}

$html .= '
							</tbody>
						</table>
					</td>
				</tr>
			</table>';
$pdf->writeHTML($html, true, false, true, false, '');


$pdf->IncludeJS('print(true);');
//$pdf->Output('Cotizacion.pdf', 'I');
$pdf->Output(FCPATH.'public/pdf/reporte_pasajeros_'. $GLOBALS['idC'] .'.pdf', 'I');
//$pdf->Output(FCPATH.'public/pdf/reporte_pasajeros_'. $GLOBALS['idC'] .'.pdf', 'F');
