	<?php

	require_once('TCPDF4/tcpdf.php');
	//$ruta_base = $_SERVER['DOCUMENT_ROOT'];
	$this->load->helper('url');

	$englishDays = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
	$spanishDays = array('Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo');
	$englishMonths = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
	$spanishMonths = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
	$spanishMonthsMin = array('Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');


	$GLOBALS['header'] = FCPATH . "public/img/bitacora_viaje/header.png";

	$GLOBALS['id_cont'] = $id_cont;
	$GLOBALS['folio_cont'] = $contratos->folio;
	$GLOBALS['folio_coti'] = $folio_coti;
	$GLOBALS['folio_oc'] = '';
	$GLOBALS['usuario'] = $usuario->nombre . ' ' . $usuario->apellido_p . ' ' . $usuario->apellido_m;
	$GLOBALS['fecha_hora_imp'] = $fecha_hora_imp;

	$bitacoraImg = FCPATH . "public/img/bitacora_viaje/bitacora.png";

	$empresa = $contratos->empresa;
	//$cliente = $clientes->nombre." ".$clientes->app." ".$clientes->apm;

	$unCheckImg = FCPATH . 'public/img/circleUncheck.png';
	$checkImg = FCPATH . 'public/img/circleCheck.png';


	$km_ini = 0;
	$km_fin = 0;
	$apto = 0;
	$revF_aceite = $unCheckImg;
	$revF_frenos = $unCheckImg;
	$revF_transmision = $unCheckImg;
	$revF_anticongelante = $unCheckImg;
	$revF_direccion = $unCheckImg;
	$revF_neumaticos = $unCheckImg;
	$revL_cortas = $unCheckImg;
	$revL_direc = $unCheckImg;
	$revL_tablero = $unCheckImg;
	$revL_reversa = $unCheckImg;
	$revL_largas = $unCheckImg;
	$revL_frenos = $unCheckImg;


	if (isset($bitacora_rev)) {
		$km_ini = $bitacora_rev->km_ini;
		$km_fin = $bitacora_rev->km_fin;
		$apto = $bitacora_rev->apto;
		$revF_aceite = $bitacora_rev->revF_aceite == 1 ? $checkImg : $unCheckImg;
		$revF_frenos = $bitacora_rev->revF_frenos == 1 ? $checkImg : $unCheckImg;
		$revF_transmision = $bitacora_rev->revF_transmision == 1 ? $checkImg : $unCheckImg;
		$revF_anticongelante = $bitacora_rev->revF_anticongelante == 1 ? $checkImg : $unCheckImg;
		$revF_direccion = $bitacora_rev->revF_direccion == 1 ? $checkImg : $unCheckImg;
		$revF_neumaticos = $bitacora_rev->revF_neumaticos == 1 ? $checkImg : $unCheckImg;
		$revL_cortas = $bitacora_rev->revL_cortas == 1 ? $checkImg : $unCheckImg;
		$revL_direc = $bitacora_rev->revL_direc == 1 ? $checkImg : $unCheckImg;
		$revL_tablero = $bitacora_rev->revL_tablero == 1 ? $checkImg : $unCheckImg;
		$revL_reversa = $bitacora_rev->revL_reversa == 1 ? $checkImg : $unCheckImg;
		$revL_largas = $bitacora_rev->revL_largas == 1 ? $checkImg : $unCheckImg;
		$revL_frenos = $bitacora_rev->revL_frenos == 1 ? $checkImg : $unCheckImg;
	}

	$lugar_origen = '';
	$estado_cont = '';
	if (isset($contratos)) {
		$lugar_origen = $contratos->lugar_origen;
		$estado_cont = $contratos->estado;
	}

	$km_total = $km_fin - $km_ini;

	$bit_row = array_fill(0, 96, 0);

	$fechaPDF = new DateTime($fecha_pdf);
	$diaPDF = $fechaPDF->format("j");
	$mesPDF = $fechaPDF->format("n");
	$anioPDF = $fechaPDF->format("Y");

	$licencias = array('','Tipo A','Tipo B','Tipo C','Tipo D','Tipo E','Tipo F');
	$choferIsset;
	$tiposUnidad = array('---', 'AUTO 4 Plazas', 'MINIVAN 10 Plazas', 'VAN14 Plazas', 'VAN 20 Plazas', 'VAN 33 Plazas', 'MIDIBUS 47 Plazas', 'AUTOBUS 47 Plazas', 'AUTOBUS 50 Plazas');

	//=======================================================================================
	class MYPDF extends TCPDF
	{
		//Page header
		public function Header()
		{
			$this->Image($GLOBALS['header'], 8, 3, $this->getPageWidth() - 16, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
			$this->SetY(19);
			$this->SetX(-55); //-50 -> 0000
			$this->SetFont('times', 'B', 12);
			$this->SetTextColor(192, 0, 0);
			$this->Cell(0, 5, $GLOBALS['folio_cont'], 0, false, 'C', 0, '', 0, false, 'M', 'M');
/*
			$this->SetY(19.3);
			$this->SetX(-80); //-50 -> 0000
			$this->SetFont('helvetica', 'BI', 12);
			$this->SetTextColor(51, 153, 255);
			$this->Cell(0, 5, $GLOBALS['folio_coti'], 0, false, 'C', 0, '', 0, false, 'M', 'M');
*/

			$this->SetY(10.2);
			$this->SetX(-55); //-50 -> 0000
			$this->SetFont('helvetica', 'B', 12);
			$this->SetTextColor(40, 40, 248);
			$this->Cell(0, 5, $GLOBALS['folio_oc'], 0, false, 'C', 0, '', 0, false, 'M', 'M');
		}

		// Page footer
		public function Footer()
		{
			$html = '<p style="text-align:rigth; font-size:8.5px"> IMPRESO POR: ' . $GLOBALS['usuario'] . ' &nbsp;&nbsp;&nbsp; FECHA Y HORA: ' . $GLOBALS['fecha_hora_imp'] . '</p>';
			$this->writeHTML($html, true, false, true, false, '');
		}
	}
	$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Mangoo');
	$pdf->SetTitle('Bitacora de viaje');
	$pdf->SetSubject('Bitacora de viaje');
	$pdf->SetKeywords('Bitacora de viaje');

	// set default header data
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

	// set header and footer fonts
	$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins('10', '26', '10');
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
	$pdf->SetFooterMargin('15'); //46//40
	// set auto page breaks
	$pdf->SetAutoPageBreak(true, 15); //31//25//38
	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	$pdf->SetFont('dejavusans', '', 13);

	$height = $pdf->getPageHeight();
	$width = $pdf->getPageWidth();

	// add a page   style="width: 300px; height: 200px;"
	$pdf->AddPage('P', 'A4');

	$html = '
		<style type="text/css">
			.border_f{ border: solid 5px black; }
			.border_l{ border-left: solid 5px black; }
			.border_r{ border-right: solid 5px black; }
			.border_t{ border-top: solid 5px black; }
			.border_b{ border-bottom: solid 5px black; }
			.p_space{ font-size: 1px;}
		</style>';

	$html .= '
		<table width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
			<thead>
				<tr>
					<td width="64%"></td>
					<td width="7%">
						<b>FECHA:</b>
					</td>
					<td width="8%" class="border_f" style="text-align: center; background-color:#c5d9f1;">
						' . $diaPDF . '
					</td>
					<td width="2%" style="text-align: center;"> / </td>
					<td width="8%" class="border_f" style="text-align: center; background-color:#c5d9f1;">
						' . $mesPDF . '
					</td>
					<td width="2%" style="text-align: center;"> / </td>
					<td width="9%" class="border_f" style="text-align: center; background-color:#c5d9f1;">
						' . $anioPDF . '
					</td>
				</tr>

				<tr>
					<td width="100%"><p class="p_space">&nbsp;</p></td>
				</tr>

			</tbody>
		</table>';

	/*
		<tr>
					<td width="50%" class="border_f" style="color:#ffffff; font-size: 11px; text-align: center; background-color:#558ed5;">
						<b>NOMBRE O RAZÓN SOCIAL DEL PERMISIONARIO</b>
					</td>
					<td width="50%" class="border_f" style="color:#ffffff; font-size: 11px; text-align: center; background-color:#558ed5;">
						<b>NOMBRE O RAZÓN SOCIAL DEL PERMISIONARIO</b>
					</td>
				</tr>
			</thead>

			<tbody>
				<tr>
					<td width="50%" class="border_f" >
						'.$empresa.'
					</td>
					<td width="50%" class="border_f" >
						'.$empresa.'
					</td>
				</tr>
		 */



	$html .= '
	<table width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
		<thead>
			<tr>
				<td width="100%" class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#558ed5;">
					<b>DATOS DEL VEHÍCULO</b>
				</td>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td width="20%" class="border_f" style="text-align: center; background-color:#c5d9f1;">
					<b>TIPO DE SERVICIO</b>
				</td>
				<td width="20%" class="border_f" style="text-align: center; background-color:#c5d9f1;">
					<b>NO. ECO</b>
				</td>
				<td width="20%" class="border_f" style="text-align: center; background-color:#c5d9f1;">
					<b>MARCA</b>
				</td>
				<td width="20%" class="border_f" style="text-align: center; background-color:#c5d9f1;">
					<b>MODELO</b>
				</td>
				<td width="20%" class="border_f" style="text-align: center; background-color:#c5d9f1;">
					<b>PLACAS</b>
				</td>
			</tr>';

	if (count($unidadAsig) > 0) {
		$idU = $unidadAsig->id;

		/*
		$choferIsset = array_filter($choferes, function ($chofer) use ($idU) {
			return $chofer->idUnidPros == $idU;
		});
*/

			$html .= '
						<tr>
							<td width="20%" class="border_f" style="text-align: center;">
								' . $tiposUnidad[$unidadAsig->tipo] . '
							</td>
							<td width="20%" class="border_f" style="text-align: center;">
								' . $unidadAsig->num_eco . '
							</td>
							<td width="20%" class="border_f" style="text-align: center;">
								' . $unidadAsig->marca . '
							</td>
							<td width="20%" class="border_f" style="text-align: center;">
								' . $unidadAsig->modelo . '
							</td>
							<td width="20%" class="border_f" style="text-align: center;">
								' . $unidadAsig->placas . '
							</td>
						</tr>';
	}

	$html .= '</tbody>
					</table>';


	$html .= '<br><br>
	<table width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
		<thead>
			<tr>
				<td width="100%" class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#558ed5;">
					<b>DATOS DEL OPERADOR ASIGNADO</b>
				</td>
			</tr>
		</thead>

		<tbody>';
	$html .= '<tr>
				<td width="25%" class="border_f" style="text-align: center; background-color:#c5d9f1;">
					<b>LICENCIA</b>
				</td>
				<td width="25%" class="border_f" style="text-align: center; background-color:#c5d9f1;">
					<b>VIGENCIA</b>
				</td>
				<td width="25%" class="border_f" style="text-align: center; background-color:#c5d9f1;">
					<b>EXPEDIENTE</b>
				</td>
				<td width="25%" class="border_f" style="text-align: center; background-color:#c5d9f1;">
					<b>VIGENCIA DE PÓLIZA</b>
				</td>
			</tr>';
			
	if (count($choferAsig) > 0) {
		foreach ($choferAsig as $chofer) {
			//$operadores .= $chofer->nombre . ' ' . $chofer->apellido_p . ' ' . $chofer->apellido_m . '<br>';
			if (empty($choferIsset)) {
				$html .= '
				<tr>
					<td width="25%" class="border_f" style="text-align: center;">
						'.$licencias[$chofer->tipo_licencia].'
					</td>
					<td width="25%" class="border_f" style="text-align: center;">
						'.$chofer->vigencia_licencia.'
					</td>
					<td width="25%" class="border_f" style="text-align: center;">
						'.$chofer->choferid.'
					</td>
					<td width="25%" class="border_f" style="text-align: center;">
						'.$chofer->vigencia_examen.'
					</td>
				</tr>';
			}else{
				$html .= '
					<tr>
						<td width="25%" class="border_f" style="text-align: center;">
						---
						</td>
						<td width="25%" class="border_f" style="text-align: center;">
						---
						</td>
						<td width="25%" class="border_f" style="text-align: center;">
						---
						</td>
						<td width="25%" class="border_f" style="text-align: center;">
						---
						</td>
					</tr>';
			}
		}
	}else{
		$html .= '
			<tr>
				<td width="25%" class="border_f" style="text-align: center;">
				---
				</td>
				<td width="25%" class="border_f" style="text-align: center;">
				---
				</td>
				<td width="25%" class="border_f" style="text-align: center;">
				---
				</td>
				<td width="25%" class="border_f" style="text-align: center;">
				---
				</td>
			</tr>';
	}


	$html .= '</tbody>
	</table>';



	/*

	$html .= '<br><br>
		<table class="border_f" width="100%" border="1" style="padding: 0.5px; font-size: 10px; text-align: justify;">
			<thead>
				<tr>
					<td class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#558ed5;">
						<b>ITINERARIO</b>
					</td>
				</tr>
			</thead>
			<tbody>';

	if (count($destinos) > 0) {
		foreach ($destinos as $dest) {
			$size = count($dest);
			$hora_sal = date('H:i', strtotime($dest[($size-1)]->hora)).' Hrs';
			$hora_reg = date('H:i', strtotime($dest[($size-1)]->hora_regreso)).' Hrs';
			$lugar_destino = $dest[($size-1)]->lugar;

			$html .= '
			<tr>
				<td width="100%"><p class="p_space">&nbsp;</p></td>
			</tr>

			<tr>
				<td width="20%" style="text-align: center; background-color:#c5d9f1; border:solid 1px black;">
					<b>ORIGEN</b>
				</td>
				<td width="54%" style="border:solid 1px black;">
					'.$lugar_origen.'
				</td>
				<td width="18%" style="text-align: center; background-color:#c5d9f1; border:solid 1px black;">
					<b>HORA DE SALIDA:</b>
				</td>
				<td width="8%" style="text-align: center; border:solid 1px black;">
					'.$hora_sal.'
				</td>
			</tr>

			<tr>
				<td width="20%" style="text-align: center; background-color:#c5d9f1; border-left:solid 1px black; border-right:solid 1px black;">
					<br><br>
					<b>PUNTOS INTERMEDIOS<br>(ITINERARIO)</b>
				</td>

				<td width="80%" style="border:solid 1px black;">
					<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
						<tbody>';

			if (count($dest) > 2) {
				foreach ($dest as $destino) {
						$html .= '
							<tr>
								<td width="100%" style="border:solid 1px black;">
									'.$destino->lugar.' - '.$destino->lugar_hospeda.'
								</td>
							</tr>
							';
				}
			}else if (count($dest) == 2) {
				foreach ($dest as $destino) {
						$html .= '
							<tr>
								<td width="100%" style="border:solid 1px black;">
									'.$destino->lugar.' - '.$destino->lugar_hospeda.'
								</td>
							</tr>
							';
				}
				$html .= '
							<tr>
								<td width="100%" style="border:solid 1px black;"></td>
							</tr>
							';
			}else if (count($dest) == 1 ) {
				foreach ($dest as $destino) {
						$html .= '
							<tr>
								<td width="100%" style="border:solid 1px black;">
									'.$destino->lugar.' - '.$destino->lugar_hospeda.'
								</td>
							</tr>
							';
				}
				$html .= '
							<tr>
								<td width="100%" style="border:solid 1px black;"></td>
							</tr>
							<tr>
								<td width="100%" style="border:solid 1px black;"></td>
							</tr>
							';
			}else if (count($dest) == 0 ) {
				$html .= '
							<tr>
								<td width="100%" style="border:solid 1px black;"></td>
							</tr>
							<tr>
								<td width="100%" style="border:solid 1px black;"></td>
							</tr>
							<tr>
								<td width="100%" style="border:solid 1px black;"></td>
							</tr>
							';
			}

				$html .= '
						</tbody>
					</table>
				</td>
			</tr>

					<tr>
						<td width="20%" class="border_f" style="text-align: center; background-color:#c5d9f1; border:solid 1px black;">
							<b>DESTINO</b>
						</td>
						<td width="54%" style="border:solid 1px black;">
							'.$lugar_destino.'
						</td>
						<td width="18%" style="text-align: center; background-color:#c5d9f1; border:solid 1px black;">
							<b>HORA DE REGRESO:</b>
						</td>
						<td width="8%" style="text-align: center; border:solid 1px black;">
							'.$hora_reg.'
						</td>
					</tr>';
			}
		}
	
	$html .= '
				<tr>
					<td width="100%"><p class="p_space">&nbsp;</p></td>
				</tr>
			</tbody>
		</table>';

*/


	$html .= '<br><br>
		<table width="100%" border="0" style="padding: 2px; font-size: 12.5px; text-align: justify;">
			<tbody>
				<tr>
					<td width="82%">
						<i>¿Me siento apto para realizar el viaje que me ha sido encomendado de forma segura?</i>
					</td>
					<td class="border_f" style="text-align: center;" width="3%">';

	if ($apto == 1) {
		$html .= 'X';
	}

	$html .= '
					</td>
					<td width="4%">
						SI
					</td>
					<td class="border_f" style="text-align: center;" width="3%">';
	if ($apto == 0) {
		$html .= 'X';
	}

	$html .= '
					</td>
					<td width="4%">
						NO
					</td>
					<td width="4%"></td>
				</tr>
			</tbody>
		</table>';

	$html .= '<br><br>
		<table width="100%" border="0" style="padding: 0px; font-size: 7px;">
			<thead>';

	if (count($dias) > 0) {
		foreach ($dias as $index => $dia) {
			$totalH = 0;
			$html .= '
				<tr>
					<th width="100%" style="font-size: 10px; text-align: center;">
						' . $dia->fecha . '
					</th>
				</tr>
				<tr>
					<th width="10.3%" style="font-size: 12px; text-align: center;">
						<b>DÍA ' . ($index + 1) . ':</b>
					</th>

					<th width="3.415%" style="font-size: 4.5px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>MEDIA<br>NOCHE</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>1</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>2</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>3</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>4</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>5</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>6</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>7</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>8</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>9</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>10</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>11</b>
					</th>

					<th width="3.415%" style="font-size: 4.5px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>MEDIO<br>DIA</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>1</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>2</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>3</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>4</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>5</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>6</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>7</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>8</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>9</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>10</b>
					</th>

					<th width="3.415%" style="font-size: 8px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>11</b>
					</th>

					<th width="1.7%" style="font-size: 8px; background-color: #3e3e3e;"></th>

					<th width="6%" style="font-size: 4.5px; text-align: center; color:#fff; background-color: #3e3e3e;">
						<p class="p_space">&nbsp;</p>
						<b>TOTAL<br>HORAS</b>
					</th>
				</tr>
			</thead>
		<tbody>';

			$html .= '
			<tr>
				<td width="4%" style="text-align:rigth;">
					<br><br>
					<b>1.</b>
				</td>
				<td width="8%" style="text-align:center;">
					<b>FUERA DE SERVICIO</b>
					<br>
					<span style="font-size:5px;">
						<b>OFF DUTY</b>
					</span>
				</td>

				<td width="82%">
					<table width="100%" border="0" style="padding: 2px; font-size: 4px;">
						<tbody>
							<tr class="bitacora_row1">';
			$bitacoraServ = $bit_row;
			$inicios = array();
			$finales = array();
			$totalHoras = 0;

			foreach ($dia->horas as $hora) {
				$horaIni = date('H', strtotime($hora->hora_ini));
				$minIni = date('i', strtotime($hora->hora_ini));
				$horaFin = date('H', strtotime($hora->hora_fin));
				$minFin = date('i', strtotime($hora->hora_fin));

				$horaIni = $horaIni * 4;
				$horaFin = $horaFin * 4;
				$minIni = round($minIni / 15);
				$minFin = round($minFin / 15);

				if ($hora->tipo == 1) {
					array_push($inicios, $horaIni + $minIni);
					array_push($finales, $horaFin + $minFin - 1);
				}

				foreach ($inicios as $index => $inicio) {
					$totalHoras = $finales[$index] - $inicio;
					for ($x = 0; $x <= ($totalHoras); $x++) {
						$bitacoraServ[($inicio + $x)] = 1;
					}
				}
			}

			if ($totalHoras != 0) {
				$totalHoras = ($totalHoras + 1) / 4;
			}

			$totalH += (float)$totalHoras;

			if ($totalHoras - floor($totalHoras) == 0) {
				$totalHoras = floor($totalHoras) . ':00 Hrs.';
			} else if ($totalHoras - floor($totalHoras) == 0.25) {
				$totalHoras = floor($totalHoras) . ':15 Hrs.';
			} else if ($totalHoras - floor($totalHoras) == 0.5) {
				$totalHoras = floor($totalHoras) . ':30 Hrs.';
			} else if ($totalHoras - floor($totalHoras) == 0.75) {
				$totalHoras = floor($totalHoras) . ':45 Hrs.';
			}

			foreach ($bitacoraServ as $bitacoraS) {
				if ($bitacoraS == 0) {
					$bussy = '';
				} else {
					$bussy = 'background-color: gray;';
				}

				$html .= '
									<td width="1.04%" style="' . $bussy . ' border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000;"></td>';
			}

			$html .= '
							</tr>
							<tr class="bitacora_row2">';
			for ($i = 0; $i < 24; $i++) {
				$html .= '
										<td width="1.04%" style="border-left: 1px solid #000;"></td>
										<td width="1.04%" style=""></td>
										<td width="1.04%" style="border-left: 1px solid #000;"></td>
										<td width="1.04%" style="border-right: 1px solid #000;"></td>';
			}

			$html .= '
							</tr>
							<tr class="bitacora_row3">';
			for ($i = 0; $i < 24; $i++) {
				$html .= '
										<td width="1.04%" style="border-bottom: 1px solid #000; border-left: 1px solid #000;"></td>
										<td width="1.04%" style="border-bottom: 1px solid #000; "></td>
										<td width="1.04%" style="border-bottom: 1px solid #000; "></td>
										<td width="1.04%" style="border-bottom: 1px solid #000; border-right: 1px solid #000;"></td>';
			}

			$html .= '
							</tr>
						</tbody>
					</table>
				</td>

				<td width="6%" style="text-align:center; border-bottom:0.5px solid #000;">
					<br><br>
					' . ($totalHoras) . '
				</td>
			</tr>';


			$html .= '
			<tr>
				<td width="4%" style="text-align:rigth;">
					<br><br>
					<b>2.</b>
				</td>
				<td width="8%" style="text-align:center;">
					<p class="p_space">&nbsp;</p>
					<b>DORMIDO</b>
					<br>
					<span style="font-size:5px;">
						<b>SLEEPER DERTH</b>
					</span>
				</td>

				<td width="82%">
					<table width="100%" border="0" style="padding: 2px; font-size: 4px;">
						<tbody>
							<tr class="bitacora_row1">';
			$bitacoraDorm = $bit_row;
			$inicios = array();
			$finales = array();
			$totalHoras = 0;

			foreach ($dia->horas as $hora) {
				$horaIni = date('H', strtotime($hora->hora_ini));
				$minIni = date('i', strtotime($hora->hora_ini));
				$horaFin = date('H', strtotime($hora->hora_fin));
				$minFin = date('i', strtotime($hora->hora_fin));

				$horaIni = $horaIni * 4;
				$horaFin = $horaFin * 4;
				$minIni = round($minIni / 15);
				$minFin = round($minFin / 15);

				if ($hora->tipo == 2) {
					array_push($inicios, $horaIni + $minIni);
					array_push($finales, $horaFin + $minFin - 1);
				}

				foreach ($inicios as $index => $inicio) {
					$totalHoras = $finales[$index] - $inicio;
					for ($x = 0; $x <= ($totalHoras); $x++) {
						$bitacoraDorm[($inicio + $x)] = 1;
					}
				}
			}

			if ($totalHoras != 0) {
				$totalHoras = ($totalHoras + 1) / 4;
			}

			$totalH += (float)$totalHoras;

			if ($totalHoras - floor($totalHoras) == 0) {
				$totalHoras = floor($totalHoras) . ':00 Hrs.';
			} else if ($totalHoras - floor($totalHoras) == 0.25) {
				$totalHoras = floor($totalHoras) . ':15 Hrs.';
			} else if ($totalHoras - floor($totalHoras) == 0.5) {
				$totalHoras = floor($totalHoras) . ':30 Hrs.';
			} else if ($totalHoras - floor($totalHoras) == 0.75) {
				$totalHoras = floor($totalHoras) . ':45 Hrs.';
			}

			foreach ($bitacoraDorm as $bitacoraD) {
				if ($bitacoraD == 0) {
					$bussy = '';
				} else {
					$bussy = 'background-color: gray;';
				}

				$html .= '
										<td width="1.04%" style="' . $bussy . ' border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000;"></td>
									';
			}

			$html .= '
							</tr>
							<tr class="bitacora_row2">';
			for ($i = 0; $i < 24; $i++) {
				$html .= '
										<td width="1.04%" style="border-left: 1px solid #000;"></td>
										<td width="1.04%" style=""></td>
										<td width="1.04%" style="border-left: 1px solid #000;"></td>
										<td width="1.04%" style="border-right: 1px solid #000;"></td>';
			}

			$html .= '
							</tr>
							<tr class="bitacora_row3">';
			for ($i = 0; $i < 24; $i++) {
				$html .= '
										<td width="1.04%" style="border-bottom: 1px solid #000; border-left: 1px solid #000;"></td>
										<td width="1.04%" style="border-bottom: 1px solid #000; "></td>
										<td width="1.04%" style="border-bottom: 1px solid #000; "></td>
										<td width="1.04%" style="border-bottom: 1px solid #000; border-right: 1px solid #000;"></td>';
			}

			$html .= '
							</tr>
						</tbody>
					</table>
				</td>

				<td width="6%" style="text-align:center; border-bottom:0.5px solid #000;">
					<br><br>
					' . ($totalHoras) . '
				</td>
			</tr>';

			$html .= '
						<tr>
							<td width="4%" style="text-align:rigth;">
								<br><br>
								<b>3.</b>
							</td>
							<td width="8%" style="text-align:center;">
								<p class="p_space">&nbsp;</p>
								<b>MANEJANDO</b>
								<br>
								<span style="font-size:5px;">
									<b>DRIVING</b>
								</span>
							</td>

							<td width="82%">
								<table width="100%" border="0" style="padding: 2px; font-size: 4px;">
									<tbody>
										<tr class="bitacora_row1">';

			$bitacoraMane = $bit_row;
			$inicios = array();
			$finales = array();
			$totalHoras = 0;

			foreach ($dia->horas as $hora) {
				$horaIni = date('H', strtotime($hora->hora_ini));
				$minIni = date('i', strtotime($hora->hora_ini));
				$horaFin = date('H', strtotime($hora->hora_fin));
				$minFin = date('i', strtotime($hora->hora_fin));

				$horaIni = $horaIni * 4;
				$horaFin = $horaFin * 4;
				$minIni = round($minIni / 15);
				$minFin = round($minFin / 15);

				if ($hora->tipo == 3) {
					array_push($inicios, $horaIni + $minIni);
					array_push($finales, $horaFin + $minFin - 1);
				}

				foreach ($inicios as $index => $inicio) {
					$totalHoras = $finales[$index] - $inicio;
					for ($x = 0; $x <= ($totalHoras); $x++) {
						//log_message('error', 'IN: '. ($inicio + $x));
						$bitacoraMane[($inicio + $x)] = 1;
					}
				}
			}
			
			if ($totalHoras != 0) {
				$totalHoras = ($totalHoras + 1) / 4;
			}

			$totalH += (float)$totalHoras;

			if ($totalHoras - floor($totalHoras) == 0) {
				$totalHoras = floor($totalHoras) . ':00 Hrs.';
			} else if ($totalHoras - floor($totalHoras) == 0.25) {
				$totalHoras = floor($totalHoras) . ':15 Hrs.';
			} else if ($totalHoras - floor($totalHoras) == 0.5) {
				$totalHoras = floor($totalHoras) . ':30 Hrs.';
			} else if ($totalHoras - floor($totalHoras) == 0.75) {
				$totalHoras = floor($totalHoras) . ':45 Hrs.';
			}

			foreach ($bitacoraMane as $bitacoraM) {
				if ($bitacoraM == 0) {
					$bussy = '';
				} else {
					$bussy = 'background-color: gray;';
				}

				$html .= '
												<td width="1.04%" style="' . $bussy . ' border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000;"></td>
											';
			}

			$html .= '</tr>
														<tr class="bitacora_row2">';
			for ($i = 0; $i < 24; $i++) {
				$html .= '
															<td width="1.04%" style="border-left: 1px solid #000;"></td>
															<td width="1.04%" style=""></td>
															<td width="1.04%" style="border-left: 1px solid #000;"></td>
															<td width="1.04%" style="border-right: 1px solid #000;"></td>
														';
			}

			$html .= '</tr>
														<tr class="bitacora_row3">';
			for ($i = 0; $i < 24; $i++) {
				$html .= '
															<td width="1.04%" style="border-bottom: 1px solid #000; border-left: 1px solid #000;"></td>
															<td width="1.04%" style="border-bottom: 1px solid #000; "></td>
															<td width="1.04%" style="border-bottom: 1px solid #000; "></td>
															<td width="1.04%" style="border-bottom: 1px solid #000; border-right: 1px solid #000;"></td>
														';
			}

			$html .= '</tr>
									</tbody>
								</table>
							</td>

							<td width="6%" style="text-align:center; border-bottom:0.5px solid #000;">
								<br><br>
								' . ($totalHoras) . '
							</td>
						</tr>


						<tr>
							<td width="4%" style="text-align:rigth;">
								<br><br>
								<b>4.</b>
							</td>
							<td width="8%" style="text-align:center;">
								<p class="p_space">&nbsp;</p>
								<b>SIN MANEJAR</b>
								<br>
								<span style="font-size:5px;">
									<b>ON DUTY</b>
								</span>
							</td>

							<td width="82%">
								<table width="100%" border="0" style="padding: 2px; font-size: 4px;">
									<tbody>
										<tr class="bitacora_row1">';

			$bitacoraSMane = $bit_row;
			$inicios = array();
			$finales = array();
			$totalHoras = 0;

			foreach ($dia->horas as $hora) {
				$horaIni = date('H', strtotime($hora->hora_ini));
				$minIni = date('i', strtotime($hora->hora_ini));
				$horaFin = date('H', strtotime($hora->hora_fin));
				$minFin = date('i', strtotime($hora->hora_fin));

				$horaIni = $horaIni * 4;
				$horaFin = $horaFin * 4;
				$minIni = round($minIni / 15);
				$minFin = round($minFin / 15);

				if ($hora->tipo == 4) {
					array_push($inicios, $horaIni + $minIni);
					array_push($finales, $horaFin + $minFin - 1);
				}

				foreach ($inicios as $index => $inicio) {
					$totalHoras = $finales[$index] - $inicio;
					for ($x = 0; $x <= ($totalHoras); $x++) {
						$bitacoraSMane[($inicio + $x)] = 1;
					}
				}
			}

			if ($totalHoras != 0) {
				$totalHoras = ($totalHoras + 1) / 4;
			}

			$totalH += (float)$totalHoras;

			if ($totalHoras - floor($totalHoras) == 0) {
				$totalHoras = floor($totalHoras) . ':00 Hrs.';
			} else if ($totalHoras - floor($totalHoras) == 0.25) {
				$totalHoras = floor($totalHoras) . ':15 Hrs.';
			} else if ($totalHoras - floor($totalHoras) == 0.5) {
				$totalHoras = floor($totalHoras) . ':30 Hrs.';
			} else if ($totalHoras - floor($totalHoras) == 0.75) {
				$totalHoras = floor($totalHoras) . ':45 Hrs.';
			}

			foreach ($bitacoraSMane as $bitacoraSM) {
				if ($bitacoraSM == 0) {
					$bussy = '';
				} else {
					$bussy = 'background-color: gray;';
				}

				$html .= '
												<td width="1.04%" style="' . $bussy . ' border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000;"></td>
											';
			}

			$html .= '</tr>
														<tr class="bitacora_row2">';
			for ($i = 0; $i < 24; $i++) {
				$html .= '
															<td width="1.04%" style="border-left: 1px solid #000;"></td>
															<td width="1.04%" style=""></td>
															<td width="1.04%" style="border-left: 1px solid #000;"></td>
															<td width="1.04%" style="border-right: 1px solid #000;"></td>
														';
			}

			$html .= '</tr>
														<tr class="bitacora_row3">';
			for ($i = 0; $i < 24; $i++) {
				$html .= '
															<td width="1.04%" style="border-bottom: 1px solid #000; border-left: 1px solid #000;"></td>
															<td width="1.04%" style="border-bottom: 1px solid #000; "></td>
															<td width="1.04%" style="border-bottom: 1px solid #000; "></td>
															<td width="1.04%" style="border-bottom: 1px solid #000; border-right: 1px solid #000;"></td>
														';
			}

			$html .= '</tr>
									</tbody>
								</table>
							</td>

							<td width="6%" style="text-align:center; border-bottom:0.5px solid #000;">
								<br><br>
								' . ($totalHoras) . '
							</td>
						</tr>


							<tr>
							<th width="10.3%" style="font-size: 12px;"></th>
		
							<th width="3.415%" style="font-size: 4.5px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>MEDIA<br>NOCHE</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>1</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>2</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>3</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>4</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>5</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>6</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>7</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>8</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>9</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>10</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>11</b>
							</th>
		
							<th width="3.415%" style="font-size: 4.5px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>MEDIO<br>DIA</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>1</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>2</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>3</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>4</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>5</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>6</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>7</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>8</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>9</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>10</b>
							</th>
		
							<th width="3.415%" style="font-size: 8px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>11</b>
							</th>
		
							<th width="1.7%" style="font-size: 8px;"></th>
		
							<th width="6%" style="font-size: 4.5px; text-align: center;">
								<p class="p_space">&nbsp;</p>
								<b>TOTAL<br>HORAS</b>
							</th>
						</tr>

						<tr>
							<td width="10%" style="font-size:10px; text-align:center;">
								<p class="p_space">&nbsp;</p><p class="p_space">&nbsp;</p>
								<b>RESUMEN</b>
								<br>
							</td>

							<td width="2%"></td>

							<td width="82%">
								<table width="100%" border="0" style="padding: 2px; font-size: 4px;">
									<tbody>
										<tr class="bitacora_row1">';

			$bitacoraResum = $bit_row;
			$inicios = array();
			$finales = array();
			$totalHoras = 0;

			foreach ($dia->horas as $hora) {
				$horaIni = date('H', strtotime($hora->hora_ini));
				$minIni = date('i', strtotime($hora->hora_ini));
				$horaFin = date('H', strtotime($hora->hora_fin));
				$minFin = date('i', strtotime($hora->hora_fin));

				$horaIni = $horaIni * 4;
				$horaFin = $horaFin * 4;
				$minIni = round($minIni / 15);
				$minFin = round($minFin / 15);

				array_push($inicios, $horaIni + $minIni);
				array_push($finales, $horaFin + $minFin - 1);

				foreach ($inicios as $index => $inicio) {
					$totalHoras = $finales[$index] - $inicio;
					for ($x = 0; $x <= ($totalHoras); $x++) {
						$bitacoraResum[($inicio + $x)] = 1;
					}
				}
			}

			if ($totalH - floor($totalH) == 0) {
				$totalH = floor($totalH) . ':00 Hrs.';
			} else if ($totalH - floor($totalH) == 0.25) {
				$totalH = floor($totalH) . ':15 Hrs.';
			} else if ($totalH - floor($totalH) == 0.5) {
				$totalH = floor($totalH) . ':30 Hrs.';
			} else if ($totalH - floor($totalH) == 0.75) {
				$totalH = floor($totalH) . ':45 Hrs.';
			}

			foreach ($bitacoraResum as $bitacoraR) {
				if ($bitacoraR == 0) {
					$bussy = '';
				} else {
					$bussy = 'background-color: gray;';
				}

				$html .= '
												<td width="1.04%" style="' . $bussy . ' border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000;"></td>
											';
			}

			$html .= '</tr>
														<tr class="bitacora_row2">';
			for ($i = 0; $i < 24; $i++) {
				$html .= '
															<td width="1.04%" style="border-left: 1px solid #000;"></td>
															<td width="1.04%" style=""></td>
															<td width="1.04%" style="border-left: 1px solid #000;"></td>
															<td width="1.04%" style="border-right: 1px solid #000;"></td>
														';
			}

			$html .= '</tr>
														<tr class="bitacora_row3">';
			for ($i = 0; $i < 24; $i++) {
				$html .= '
															<td width="1.04%" style="border-bottom: 1px solid #000; border-left: 1px solid #000;"></td>
															<td width="1.04%" style="border-bottom: 1px solid #000; "></td>
															<td width="1.04%" style="border-bottom: 1px solid #000; "></td>
															<td width="1.04%" style="border-bottom: 1px solid #000; border-right: 1px solid #000;"></td>
														';
			}

			$html .= '</tr>
									</tbody>
								</table>
							</td>

							<td width="6%" style="text-align:center; border-bottom:0.5px solid #000;">
								<br><br>
								' . ($totalH) . '
							</td>
						</tr>

					';
		}
	}
	$html .= '</tbody>
		</table>';


	$html .= '<br><br>
					<table width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
						<thead>
							<tr>
								<td width="100%" class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#558ed5;">
									<b>ACTIVIDADES AUXILIARES DEL OPERADOR</b>
								</td>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td width="18%" class="border_f" style="font-size: 6.5px; text-align: center; background-color:#c5d9f1; height: 15px; line-height: 15px;">
									<b>Revisión de niveles de fluidos</b>
								</td>
								<td width="2%" class="border_b" style="text-align: end; font-size: 8px; height: 15px; line-height: 15px;">
									<img src="' . $revF_aceite . '">
								</td>
								<td width="15%" class="border_b" style="font-size: 6px; text-align: start; height: 15px; line-height: 15px;">
									Aceite de motor
								</td>

								<td width="2%" class="border_b" style="text-align: end; font-size: 8px; height: 15px; line-height: 15px;">
									<img src="' . $revF_frenos . '">
								</td>
								<td width="15%" class="border_b" style="font-size: 6px; text-align: start; height: 15px; line-height: 15px;">
									Líquido de frenos 
								</td>

								<td width="2%" class="border_b" style="text-align: end; font-size: 8px; height: 15px; line-height: 15px;">
									<img src="' . $revF_anticongelante . '">
								</td>
								<td width="22%" class="border_b" style="font-size: 6px; text-align: start; height: 15px; line-height: 15px;">
									Nivel de anticongelante en radiador 
								</td>

								<td width="2%" class="border_b" style="text-align: end; font-size: 8px; height: 15px; line-height: 15px;">
									<img src="' . $revF_neumaticos . '">
								</td>
								<td width="22%" class="border_b border_r" style="font-size: 6px; text-align: start; height: 15px; line-height: 15px;">
									Presión de aire de neumáticos
								</td>

							</tr>

							<tr>
								<td width="18%" class="border_f" style="font-size: 6.5px; text-align: center; background-color:#c5d9f1;height: 15px; line-height: 15px;">
									<b>Revisión de luces</b>
								</td>
								<td width="2%" class="border_b" style="text-align: start; font-size: 8px;height: 15px; line-height: 15px;">
									<img src="' . $revL_cortas . '">
								</td>
								<td width="22%" class="border_b" style="font-size: 6px; text-align: start; height: 15px; line-height: 15px;">
									Luces cortas “cuartos” (traseras y delanteras) 
								</td>

								<td width="2%" class="border_b" style="text-align: start; font-size: 8px;height: 15px; line-height: 15px;">
									<img src="' . $revL_tablero . '">
								</td>
								<td width="9%" class="border_b" style="font-size: 6px; text-align: start; height: 15px; line-height: 15px;">
									Luces del tablero
								</td>

								<td width="2%" class="border_b" style="text-align: start; font-size: 8px;height: 15px; line-height: 15px;">
									<img src="' . $revL_largas . '">
								</td>
								<td width="13%" class="border_b" style="font-size: 6px; text-align: start; height: 15px; line-height: 15px;">
									Luces largas (altas y bajas) 
								</td>

								<td width="2%" class="border_b" style="text-align: start; font-size: 8px;height: 15px; line-height: 15px;">
									<img src="' . $revL_direc . '">
								</td>
								<td width="14%" class="border_b" style="font-size: 6px; text-align: start; height: 15px; line-height: 15px;">
									Direccionales e intermitentes
								</td>

								<td width="2%" class="border_b" style="text-align: start; font-size: 8px;height: 15px; line-height: 15px;">
									<img src="' . $revL_reversa . '">
								</td>
								<td width="5%" class="border_b" style="font-size: 6px; text-align: start; height: 15px; line-height: 15px;">
									Reversa
								</td>

								<td width="2%" class="border_b" style="text-align: start; font-size: 8px;height: 15px; line-height: 15px;">
									<img src="' . $revL_frenos . '">
								</td>
								<td width="7%" class="border_b border_r" style="font-size: 6px; text-align: start; height: 15px; line-height: 15px;">
									Rev. Frenos
								</td>
							</tr>
						</tbody>
					</table>';


	$html .= '<br><br>
					<table width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
						<thead>
							<tr>
								<td width="15%" class="border_f" style="text-align: center; background-color:#c5d9f1; height: 15px; line-height: 15px;">
									<b>KILOMETRAJE INICIAL</b>
								</td>
								<td width="18%" class="border_f" style="text-align: center; height: 15px; line-height: 25px;">
									' . $km_ini . '
								</td>
								<td width="15%" class="border_f" style="text-align: center; background-color:#c5d9f1; height: 15px; line-height: 15px;">
									<b>KILOMETRAJE FINAL</b>
								</td>
								<td width="18%" class="border_f" style="text-align: center; height: 15px; line-height: 25px;">
									' . $km_fin . '
								</td>
								<td width="15%" class="border_f" style="color:#ffffff; text-align: center; background-color:#558ed5; height: 15px; line-height: 15px;">
									<b>KILOMETROS RECORRIDOS</b>
								</td>
								<td width="19%" class="border_f" style="text-align: center; background-color:#c5d9f1; height: 15px; line-height: 25px;">
									' . $km_total . '
								</td>
							</tr>
						</thead>
					</table>';


	$html .= '<br><br>
					<table width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
						<thead>
							<tr>
								<td width="100%" class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#558ed5;">
									<b>FIRMAS</b>
								</td>
							</tr>
						</thead>

						<tbody>';

		if (count($choferAsig) > 0) {
			foreach ($choferAsig as $key => $chofer) {
				if ($key == 0) {
					$html .= '
							<tr>
								<td width="100%" style="border-left:solid 5px black; border-right:solid 5px black; "><p class="p_space">&nbsp;</p></td>
							</tr>
							<tr>
								<td width="12.5%" style="border-left:solid 5px black;"></td>

								<td width="25%" style="text-align: center; border-bottom:solid 5px black;">
									' . $chofer->nombre . ' ' . $chofer->apellido_p . ' ' . $chofer->apellido_m . '
								</td>

								<td width="25%"></td>

								<td width="25%" style="text-align: center; border-bottom:solid 5px black;">
									
								</td>

								<td width="12.5%" style="border-right:solid 5px black;"></td>
							</tr>

							<tr>
								<td width="12.5%" style="border-left:solid 5px black; "></td>

								<td width="25%" style="text-align: center;">
									NOMBRE Y FIRMA<br>
									DEL OPERADOR ASIGNADO
								</td>

								<td width="25%" style=""></td>

								<td width="25%" style="text-align: center;">
									NOMBRE Y FIRMA<br>
									GERENTE DE OPERACIONES
								</td>

								<td width="12.5%" style="border-right:solid 5px black;"></td>
							</tr>';
				}else{
					$html .= '
							<tr>
								<td width="100%" style="border-left:solid 5px black; border-right:solid 5px black; "><p class="p_space">&nbsp;</p></td>
							</tr>
							<tr>
								<td width="100%" style="border-left:solid 5px black; border-right:solid 5px black; "><p class="p_space">&nbsp;</p></td>
							</tr>
							<tr>
								<td width="100%" style="border-left:solid 5px black; border-right:solid 5px black; "><p class="p_space">&nbsp;</p></td>
							</tr>
							<tr>
								<td width="12.5%" style="border-left:solid 5px black;"></td>

								<td width="25%" style="text-align: center; border-bottom:solid 5px black;">
									' . $chofer->nombre . ' ' . $chofer->apellido_p . ' ' . $chofer->apellido_m . '
								</td>

								<td width="50%" style=""></td>

								<td width="12.5%" style="border-right:solid 5px black;"></td>
							</tr>

							<tr>
								<td width="12.5%" style="border-left:solid 5px black; "></td>

								<td width="25%" style="text-align: center;">
									NOMBRE Y FIRMA<br>
									DEL OPERADOR ASIGNADO
								</td>

								<td width="50%" style=""></td>

								<td width="12.5%" style="border-right:solid 5px black;"></td>
							</tr>';
				}
			
			}
		}

			$html .= '
							<tr>
								<td width="100%" style="border-left:solid 5px black; border-right:solid 5px black; border-bottom:solid 5px black;"></td>
							</tr>
						</tbody>
					</table>';



	$pdf->writeHTML($html, true, false, true, false, '');

	$pdf->IncludeJS('print(true);');
	//$pdf->Output('Orden_servicio.pdf', 'I');
	$pdf->Output(FCPATH . 'public/pdf/Bitacora_viaje_' . $id_cont . '.pdf', 'FI');
	//$pdf->Output(FCPATH.'public/pdf/Bitacora_viaje_'. $id_cont .'.pdf', 'I');
