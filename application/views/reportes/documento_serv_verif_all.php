<?php
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=reportes:unidades".date('Ymd Gis').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<style>
  .pspaces{ font-size: 0.5px; }
</style>
<br>

  <?php
    function redimensionar($src,$name){
      $imagen = $src; //Imagen original
      $imagenNueva = FCPATH.'public/uploads/unidades/verificaciones/redimen/'.$name; //Nueva imagen

      if(strtolower(pathinfo($name,PATHINFO_EXTENSION))=="jpg" || strtolower(pathinfo($name,PATHINFO_EXTENSION))=="jpeg" || strtolower(pathinfo($name,PATHINFO_EXTENSION))=="webp"){
          $imagen = imagecreatefromjpeg($imagen); 
      }else if(strtolower(pathinfo($name,PATHINFO_EXTENSION))=="png"){
        $imagen = imagecreatefrompng($imagen); 
      }
      $x = imagesx($imagen);
      $y = imagesy($imagen);
      
      $nAncho = 260;
      $nAlto = 180;

      $img = imagecreatetruecolor($nAncho, $nAlto);
      imagecopyresized($img, $imagen, 0, 0, 0, 0, $nAncho, $nAlto, $x, $y);
      
      imagejpeg($img, $imagenNueva);
      return $name;
    }

    $tipos = array('---','Físico mecánica','Contaminantes');
    $tipo_s = array('---','Mantenimiento Preventivo','Mantenimiento Correctivo');
    $uni = "";
    $serv = "";
    $verf = "";
    $tot_ser=0; $tot_verif=0;
    $unidad = $this->ModeloGeneral->getselectwhere2('unidades',array('estatus'=>1));
    foreach ($unidad->result() as $u) {  
      echo "<table border='1' width='100%'>
          <thead>
            <tr>
              <th colspan='9'>DATOS DEL VEHÍCULO</th>
            </tr>
            <tr>
              <th>Número Económico</th>
              <th>Vehículo</th>
              <th>Modelo</th>
              <th>Placas</th>
              <th>Placas Federales</th>
              <th>Serie</th>
              <th>Motor</th>
              <th>Próxima Verificación</th>
              <th>KM Actual</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>".$u->num_eco."</td>
              <td>".$u->vehiculo."</td>
              <td>".$u->modelo."</td>
              <td>".$u->placas."</td>
              <td>".$u->placas_fed."</td>
              <td>".$u->serie."</td>
              <td>".$u->motor."</td>
              <td>".($u->prox_ver != '0000-00-00' ? $u->prox_ver : '---Sin fecha asignada---') . "</td>
              <td>".$u->km_actual."</td>
            </tr>
          </tbody>
      </table>
      <table border='0' width='100%'>
        <thead>
         <tr><th><br></th></tr>
        </thead>
      </table>";

      $cont=0; $table=""; $body=""; $foot=""; $tot_ser=0;
      $servicios = $this->ModeloUnidades->getUnidadesServReporte($u->id,$fi,$ff);
      foreach ($servicios as $servicio) {
        $cont++;
        $tot_ser=$tot_ser+$servicio->importe;
          $table="<table border='1' width='100%''>
          <thead>
            <tr>
              <th colspan='6'>DATOS DE SERVICIOS</th>
            </tr>
            <tr>
              <th>#</th>
              <th>Fecha de Servicio</th>
              <th>Kilometraje</th>
              <th>Tipo</th>
              <th>Importe</th>
              <th>Comentarios</th>
            </tr>
          </thead>
          <tbody>";
            $body.="<tr>
              <td>".$cont."</td>
              <td>".$servicio->fecha."</td>
              <td>".$servicio->kilometraje."</td>
              <td>".$tipo_s[$servicio->tipo]."</td>
              <td>".number_format($servicio->importe)."</td>
              <td>".$servicio->comentarios."</td>
            </tr>
          </tbody>";
          $foot="<tfoot>
            <tr>
              <td colspan='4'></td>
              <td >".number_format($tot_ser,2)."</td>
              <td></td>
            </tr>
          </tfoot>
        </table>";
      }
      echo $table.$body.$foot.
      "<table border='0' width='100%'>
        <thead>
         <tr><th><br></th></tr>
        </thead>
      </table>";
      
      $cont2=0; $table2=""; $body2=""; $foot2=""; $tot_verif=0;
      $verificaciones = $this->ModeloUnidades->getUnidadesVeriReporte($u->id,$fi,$ff);
      foreach ($verificaciones as $verificacion) {
        $cont2++;
        $img = "";
        if($verificacion->img_verif!=""){
          $uri=base_url().'public/uploads/unidades/verificaciones/'.$verificacion->img_verif;
          $img_med=redimensionar($uri,$verificacion->img_verif);
          $img='<img src="'.base_url().'public/uploads/unidades/verificaciones/redimen/'.$img_med.'">';
        }
        $tot_verif=$tot_verif+$verificacion->importe;
        $table2="<table border='1' width='100%'>
          <thead>
            <tr>
              <th colspan='9'>DATOS DE VERIFICACIONES</th>
            </tr>
            <tr>
              <th>#</th>
              <th colspan='4'>Foto</th>
              <th>Número Económico</th>
              <th>Fecha de Verificación</th>
              <th>Tipo</th>
              <th>Importe</th>
              <th>Observaciones</th>
            </tr>
          </thead>";
          $body2.="<tbody>
            <tr>
              <td>".$cont2."</td>
              <td colspan='4' style='align-items: center; justify-content: center'><p class='pspaces'></p>
                <table>
                  <tr> <td colspan='4' width='100%'></td></tr>
                  <tr>
                    <td colspan='3' width='80%'>".$img."</td>
                    <td width='20%'></td>
                  </tr>
                </table><br><br><br><br><br><br><br><br><br>
              </td>
              <td>".$verificacion->num_eco."</td>
              <td>".$verificacion->fecha."</td>
              <td>".$tipos[$verificacion->tipo]."</td>
              <td>".number_format($verificacion->importe)."</td>
              <td>".$verificacion->observaciones."</td>
            </tr>
          </tbody>";
          $foot2="<tfoot>
            <tr>
              <td colspan='8'></td>
              <td >".number_format($tot_verif,2)."</td>
              <td ></td>
            </tr>
          </tfoot>
        </table>";
      }
      echo $table2.$body2.$foot2.
      "<table border='0' width='100%'>
        <thead>
         <tr><th><br></th></tr>
        </thead>
      </table>";

  } ?>
