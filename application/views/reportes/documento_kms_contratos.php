<?php
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=reporte_kms_unidades".date('Ymd Gis').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
  <?php

    $unidad = $this->ModeloUnidades->getKmsReporte($fi,$ff);
    log_message('error','Unidades: '.json_encode($unidad));
    echo "<table border='1' width='100%'>
          <thead>
            <tr>
              <th colspan='10'>DATOS DEL RECORRIDO(S)</th>
            </tr>
            <tr>
              <th>Número Económico</th>
              <th>Vehículo</th>
              <th>Contrato</th>
              <th>Folio</th>
              <th>Lugar</th>
              <th>Fecha salida</th>
              <th>Fecha regreso</th>
              <th>Km inicial</th>
              <th>Km final</th>
              <th>Kms recorridos</th>
            </tr>
          </thead>
          <tbody>";
    foreach ($unidad as $u) {  
      echo "
            <tr>
              <td>".$u->num_eco."</td>
              <td>".$u->vehiculo."</td>
              <td>".$u->id_contrato."</td>
              <td>".$u->folio."</td>
              <td>".$u->lugar."</td>
              <td>".$u->fecha_salida."</td>
              <td>".$u->fecha_regreso."</td>
              <td>".$u->km_ini."</td>
              <td>".$u->km_fin."</td>
              <td>".($u->km_fin-$u->km_ini)."</td>
            </tr>";
  } 
  echo "</tbody>
      </table>"


  ?>
