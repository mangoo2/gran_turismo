<?php
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename= estimacion_cotizacion".date('Ymd Gis').".xls");

$valorRespuesta = array('---','Muy mala','Mala','Regular','Buena','Muy buena');
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Cliente</th>
            <th scope="col">Contrato</th>
            <th scope="col">Fecha respuesta</th>
            <th scope="col">¿Cómo fue la atención proporcionada con su <b>ejecutivo de ventas</b>?</th>
            <th scope="col">¿Cómo fue la atención proporcionada por el operador (chofer) de su viaje?</th>
            <th scope="col">Por favor califique su nivel de satisfacción con el servicio brindado por el operador durante su servicio</th>
            <th scope="col">Basado en su experiencia global con <b>Gʀᴀɴᴅ Tᴜʀɪsᴍᴏ Exᴘʀᴇss</b> ¿Qué tanto nos recomendaría con un familiar o amigo?</th>
            <th scope="col">¿Qué recomendaría para mejorar nuestro servicio o tiene algún comentario o sugerencia para nosotros?</th>
            <th scope="col">En caso de tener alguna inconformidad con el servicio, favor de mencionarla.</th>
        </tr>
    </thead>

    <tbody>
        <?php
        foreach ($encuestas as $e) {
            echo '
            <tr>
                <td >' . $e->id . '</td>
                <td >' . $e->cliente . '</td>
                <td >' . $e->id_contrato . '</td>
                <td >' . ($e->reg == "0000-00-00 00:00:00" ? "---" : $e->reg) . '</td> 
                <td >' . $valorRespuesta[$e->pregunta1] . '</td>
                <td >' . $valorRespuesta[$e->pregunta2] . '</td>
                <td >' . $valorRespuesta[$e->pregunta3] . '</td>
                <td >' . $valorRespuesta[$e->pregunta4] . '</td>
                <td >' . $e->pregunta5 . '</td>
                <td >' . $e->pregunta6 . '</td>
            </tr>';
        }
        ?>
    </tbody>
</table>