<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=estado_cuenta".date('Ymd Gis').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th scope="col">Cliente</th>
            <th scope="col">Contrato</th>
            <th scope="col">Folio</th>
            <th scope="col">Fecha registro</th>
            <th scope="col">Fecha contrato</th>
            <th scope="col">Total contrato</th>
            <th scope="col">Pagos registrados</th>
            <th scope="col">Cuenta</th>
            <th scope="col">Fecha</th>
            <th scope="col">Referencia</th>
            <th scope="col">Descripción de referencia</th>
            <th scope="col">Total de pagos</th>
            <th scope="col">Restante</th>
        </tr>
    </thead>
    <tbody>
    	<?php $resta_fin=0; $pays_tot=0;
        foreach ($edos as $r) {
            $cuenta=""; $monto=""; $fecha=""; $referencia="";
            $getpays=$this->ModeloContratos->getPagosContrato($r->id);
            foreach ($getpays as $p) {
                $cuenta.=$p->cuenta."<br>";
                $monto.=number_format($p->monto,2)."<br>";
                $fecha.= ($p->fecha != '0000-00-00' ? $p->fecha : ' ' )."<br>";
                //$fecha.=date("d-m-Y H:i:s a", strtotime($p->fecha))."<br>";
                $referencia.=$p->referencia."<br>";
            }
            $totalc=($r->tot_unids-$r->tot_desc);
            $totalp=($r->tot_pays+$r->tot_antic);
            $resta=$r->tot_unids-$r->tot_desc-$r->tot_pays-$r->tot_antic;
            $resta_fin=$resta_fin+$resta;
            $pays_tot=$pays_tot+$totalp;
            echo '
            <tr>
                <td>'.$r->cliente.'</td>
                <td>'.$r->id.'</td>
                <td>'.$r->folio.'</td>
                <td>'.date("d-m-Y H:i:s a", strtotime($r->fecha_reg)).'</td>
                <td>'.($r->fecha_contrato != '0000-00-00' ? $r->fecha_contrato : ' ' ).'</td>
                <td>'.number_format($totalc,2).'</td>

                <td>'.$monto.'</td>
                <td>'.$cuenta.'</td>
                <td>'.$fecha.'</td>
                <td>'.$referencia.'</td>
                <td> </td>

                <td>'.number_format($totalp,2).'</td>
                <td>'.number_format($resta,2).'</td>
            </tr>';
        }
        ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="10"></td>
            <td><b><?php echo number_format($pays_tot,2); ?></b></td>
            <td><b><?php echo number_format($resta_fin,2); ?></b></td>
        </tr>
    </tfoot>
</table>
