<?php

require_once('TCPDF4/tcpdf.php');
//$ruta_base = $_SERVER['DOCUMENT_ROOT'];
$this->load->helper('url');

$englishDays = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
$spanishDays = array('Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo');
$englishMonths = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
$spanishMonths = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

$tiposUnidad = array('---', 'AUTO 4 Plazas', 'MINIVAN 10 Plazas', 'VAN14 Plazas', 'VAN 20 Plazas', 'VAN 33 Plazas', 'MIDIBUS 47 Plazas', 'AUTOBUS 47 Plazas', 'AUTOBUS 50 Plazas');
$tiposServicio = array(' ', 'TURÍSTICO');

$GLOBALS['header'] = FCPATH . "public/img/contratos/header.png";
$GLOBALS['foot'] = FCPATH . "public/img/contratos/foot.png";
$GLOBALS['ubicacion'] = FCPATH . "public/img/contratos/loc.png";
$GLOBALS['contacto'] = FCPATH . "public/img/contratos/tel.png";
$GLOBALS['web'] = FCPATH . "public/img/contratos/web.png";
$GLOBALS['whatsapp'] = FCPATH . "public/img/contratos/wha.png";
$GLOBALS['mail'] = FCPATH . "public/img/contratos/mail.png";
$GLOBALS['facebook'] = FCPATH . "public/img/contratos/face.png";

$GLOBALS['id'] = $idC;
$GLOBALS['folio'] = $contratos->folio;
$GLOBALS['folio_cot'] = $folio_cot;

$fecha_dia = date('d', strtotime($contratos->fecha_contrato));
$fecha_anio = date('Y', strtotime($contratos->fecha_contrato));
$fecha_mes = date('m', strtotime($contratos->fecha_contrato));

switch ($fecha_mes) {
	case 1:
		$nombre_mes = 'Enero';
		break;
	case 2:
		$nombre_mes = 'Febrero';
		break;
	case 3:
		$nombre_mes = 'Marzo';
		break;
	case 4:
		$nombre_mes = 'Abril';
		break;
	case 5:
		$nombre_mes = 'Mayo';
		break;
	case 6:
		$nombre_mes = 'Junio';
		break;
	case 7:
		$nombre_mes = 'Julio';
		break;
	case 8:
		$nombre_mes = 'Agosto';
		break;
	case 9:
		$nombre_mes = 'Septiembre';
		break;
	case 10:
		$nombre_mes = 'Octubre';
		break;
	case 11:
		$nombre_mes = 'Noviembre';
		break;
	case 12:
		$nombre_mes = 'Diciembre';
		break;
	default:
		$nombre_mes = ' --- ';
		break;
}

// destinos------------
$destino = '';
$fechaSalMenor = null;
$fechaRegMayor = null;

foreach ($destinos as $index => $data) {
	if ($index < (count($destinos) - 1)) {
		$destino .= $data->lugar . ' | ';
	} else {
		$destino .= $data->lugar;
	}
}


$fecha_s = $contratos->fecha_salida . ' ' . $contratos->hora_salida;
$sal = new DateTime($fecha_s);


$fecha_r = $contratos->fecha_regreso . ' ' . $contratos->hora_regreso;
$reg = new DateTime($fecha_r);

/*Salida*/
if ($sal === null) {
	$dia_sal = '';
	$mes_sal = '';
	$diaN_sal = '';
	$anio_sal = '';
	$hora_sal = '';
	$minutos_sal = '';
} else {
	$dia_sal = $sal->format("l");
	$dia_sal = str_replace($englishDays, $spanishDays, $dia_sal);

	$mes_sal = $sal->format("F");
	$mes_sal = str_replace($englishMonths, $spanishMonths, $mes_sal);

	$diaN_sal = $sal->format("j");

	$anio_sal = $sal->format("Y");

	$hora_sal = $sal->format("H");
	$minutos_sal = $sal->format("i");
}


/*Regreso*/
if ($reg === null) {
	$dia_reg = '';
	$mes_reg = '';
	$diaN_reg = '';
	$anio_reg = '';
	$hora_regreso = '';
	$minuto_regreso = '';
} else {
	$dia_reg = $reg->format("l");
	$dia_reg = str_replace($englishDays, $spanishDays, $dia_reg);

	$mes_reg = $reg->format("F");
	$mes_reg = str_replace($englishMonths, $spanishMonths, $mes_reg);

	$diaN_reg = $reg->format("j");

	$anio_reg = $reg->format("Y");

	$hora_regreso = $reg->format("H");
	$minuto_regreso = $reg->format("i");
}

if ($sal !== null && $reg !== null) {
	$diferencia = $sal->diff($reg);
} else {
	$sal = new DateTime('0000-00-00 00:00:00');
	$reg = new DateTime('0000-00-00 00:00:00');
	$diferencia = $sal->diff($reg);
}



$dias = "1";
if ($diferencia->format('%a') > 0) {
	$dias = $diferencia->format('%a');
	$dias = $dias + 1;
}


if ($fechaRegMayor === null || $sal->format('U') > $fechaRegMayor->format('U')) {
	$fechaRegMayor = $reg;
}

if ($fechaRegMayor == null) {
	$redondo = ' ';
	$sencillo = 'X';
} else {
	$redondo = 'X';
	$sencillo = ' ';
}

//$destino_div = wordwrap($destino, 58, "\n", true);
//$destino = "Parque Benito Juárez, Blvd. Héroes del 5 de Mayo, Carmen Huexotitla, 72534 Heroica Puebla de Zaragoza, Pue. - Puebla Avanza, Autopista México - Puebla 419, Cuarta Secc, 74169 Santa Ana Xalmimilulco, Pue. - Capilla del convento de las Capuchinas Sacramentarias del Purísimo Corazón de María, Col. de, Miguel Hidalgo 43, Tlalpan Centro I, Tlalpan, 14000 Ciudad de México, CDMX  Museo Universitario Arte Contemporáneo (MUAC), Av. Insurgentes Sur 3000, C.U., Coyoacán, 04510 Ciudad de México, CDMX - Biblioteca Central UNAM, Escolar S/N, C.U., Coyoacán, 04510 Ciudad de México, CDMX - Oasis Coyoacán, Av. Universidad 1770, Romero de Terreros, Coyoacán, 04310 Ciudad de México, CDMX - Museo Anahuacalli, Museo 150, San Pablo Tepetlapa, Coyoacán, 04620 Ciudad de México, CDMX - McDonald's Carretera México-Puebla, Autopista México - Puebla San Marcos Huixtoco, 56579 Ixtapaluca, Méx. - Parque Benito Juárez, Blvd. Héroes del 5 de Mayo, Carmen Huexotitla, 72534 Heroica Puebla de Zaragoza, Pue.";
$destino_div = wordwrap($destino, 70, "\n", true);
$dest = explode("\n", $destino_div);
/*
$destinoss = explode("\n", $destino_div);
$destinoss[0] = isset($destinoss[0]) ? $destinoss[0] : '';
$destinoss[1] = isset($destinoss[1]) ? $destinoss[1] : '';
$destinoss[2] = isset($destinoss[2]) ? $destinoss[2] : '';
$destinoss[3] = isset($destinoss[3]) ? $destinoss[3] : '';
$destinoss[4] = isset($destinoss[4]) ? $destinoss[4] : '';
*/

//Clientes----------------------------------
$GLOBALS['nombre'] = $clientes->nombre;
$GLOBALS['app']  = $clientes->app;
$GLOBALS['apm']  = $clientes->apm;
$telefono = $clientes->telefono;
$telefono_2 = $clientes->telefono_2;
$correo = $clientes->correo;
$rfc = $clientes->rfc;
$razon = $clientes->razon;
$cp = $clientes->cp;
$cod_post = $clientes->cod_postal;
//$estado = $estado_clientes->Nombre;
$colonia = $clientes->colonia;
$ciudad = $clientes->ciudad;
$calle = $clientes->calle;

//Contratos----------------------------->
$id = $contratos->id;
$lugar_origen = $contratos->lugar_origen;
$estado_destino = $contratos->estado_destino;

//Detalles------------
$calle_d = $detalles->calle;
$cp_d = $detalles->cp;
$colonia_d = $detalles->colonia;
$ciudad_d = $detalles->ciudad_municipio;
//$estado_d = $estado_detalles->Nombre;
$tipo_servicio_d = $tiposServicio[$detalles->tipo_servicio];

$tipo_unidad_d = 000;
$cantidad_unidades_d = 000;
$num_pasajeros_d = 000;
$importe_unidad_d = 000;

$observaciones_c = $contratos->observaciones;
$observaciones_d = $detalles->observaciones;

//observaciones-----------
//$observa_div = wordwrap($observaciones_d, 34, "\n", true);
$observa_div = wordwrap($observaciones_c, 34, "\n", true);
$observaciones = explode("\n", $observa_div);
$observaciones[0] = isset($observaciones[0]) ? $observaciones[0] : '';
$observaciones[1] = isset($observaciones[1]) ? $observaciones[1] : '';
$observaciones[2] = isset($observaciones[2]) ? $observaciones[2] : '';
$observaciones[3] = isset($observaciones[3]) ? $observaciones[3] : '';
$observaciones[4] = isset($observaciones[4]) ? $observaciones[4] : '';
$observaciones[5] = isset($observaciones[5]) ? $observaciones[5] : '';
$observaciones[6] = isset($observaciones[6]) ? $observaciones[6] : '';
$observaciones[7] = isset($observaciones[7]) ? $observaciones[7] : '';


$unidadesResult = $unidades->result();

$monto_anticipo = 0;
if (isset($contratos->monto_anticipo)) {
	$monto_anticipo = $contratos->monto_anticipo;
}

$porc_anticipo = 0;
if (isset($contratos->porc_anticipo)) {
	$porc_anticipo = $contratos->porc_anticipo;
}

$monto_descuento = 0;
if (isset($contratos->porc_desc)) {
	$monto_descuento = $contratos->porc_desc;
}

$ine = '';
if (isset($pagos->ine)) {
	$ine = $pagos->ine;
}

$fontSizeCiudad1 = 10;
if (strlen($ciudad) > 20) {
	$fontSizeCiudad1 = 8;
}


$fontSizeCiudad2 = 10;
if (strlen($ciudad_d) > 20) {
	$fontSizeCiudad2 = 8;
}

$subtotal = 0;

$GLOBALS['nombrePersonal'] = '';

if (isset($personal)) {
	$GLOBALS['nombrePersonal'] = $personal->nombre . ' ' . $personal->apellido_p . ' ' . $personal->apellido_m;
}


//---------------FONTSIZE-----------------
$SN1 = mb_strlen($calle, 'UTF-8');
switch (true) {
	case ($SN1 > 125):
		$fontSizeNum1 = "5px";
		break;
	case ($SN1 > 105):
		$fontSizeNum1 = "6px";
		break;
	case ($SN1 > 90):
		$fontSizeNum2 = "7px";
		break;
	case ($SN1 > 80):
		$fontSizeNum1 = "8px";
		break;
	default:
		$fontSizeNum1 = "9px";
		break;
}


$SN2 = mb_strlen($calle_d, 'UTF-8');
switch (true) {
	case ($SN2 > 125):
		$fontSizeNum2 = "5px";
		break;
	case ($SN2 > 105):
		$fontSizeNum2 = "6px";
		break;
	case ($SN2 > 90):
		$fontSizeNum2 = "7px";
		break;
	case ($SN2 > 80):
		$fontSizeNum2 = "8px";
		break;
	default:
		$fontSizeNum2 = "9px";
		break;
}


$SC1 = mb_strlen($colonia, 'UTF-8');
switch (true) {
	case ($SC1 > 50):
		$fontSizeCol1 = "5px";
		break;
	case ($SC1 > 40):
		$fontSizeCol1 = "6px";
		break;
	case ($SC1 > 30):
		$fontSizeCol1 = "7px";
		break;
	default:
		$fontSizeCol1 = "9px";
		break;
}


$SC2 = mb_strlen($colonia_d, 'UTF-8');
switch (true) {
	case ($SC2 > 50):
		$fontSizeCol2 = "5px";
		break;
	case ($SC2 > 40):
		$fontSizeCol2 = "6px";
		break;
	case ($SC2 > 30):
		$fontSizeCol2 = "7px";
		break;
	default:
		$fontSizeCol2 = "9px";
		break;
}



//---------------FONTSIZE-----------------


//=======================================================================================
class MYPDF extends TCPDF
{

	//Page header
	public function Header()
	{
		$this->Image($GLOBALS['header'], 0, 0, $this->getPageWidth(), '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		$this->SetY(22);
		$this->SetX(-44); //-50 -> 0000
		$this->SetFont('times', 'B', 12);
		$this->SetTextColor(192, 0, 0);
		$this->Cell(0, 5, $GLOBALS['folio'], 0, false, 'C', 0, '', 0, false, 'M', 'M');

		$this->SetY(27);
		$this->SetX(-44); //-50 -> 0000
		$this->Cell(0, 5, $GLOBALS['folio_cot'], 0, false, 'C', 0, '', 0, false, 'M', 'M');
	}

	// Page footer
	public function Footer()
	{

		$html = '
			<table width="100%" border="0" RULES="rows" style="color:#121e84; padding: 4px; font-size: 10px; text-align: justify;" class="table table-striped">
				<tr>
					<td width="10%"> 
					</td>';

		$html .= '
					<td width="50%" style="text-align: justify;">
						<table width="100%" border="0" RULES="rows" style="color:#121e84; padding: 2px; font-size: 10px; text-align: justify;" class="table table-striped">
							<tr>
								<td>
								</td>
							</tr>
							<tr>
								<td>
								</td>
							</tr>

							<tr>
								<td width="10%">
								</td>
			
								<td width="38%" style="font-size: 10px; text-align: center; border-bottom:solid 5px black; color: black;">
									<b>' . $GLOBALS['nombrePersonal'] . '</b>
								</td>
			
								<td width="4%">
								</td>
			
								<td width="38%" style="font-size: 10px; text-align: center; border-bottom:solid 5px black; color: black;">
									<b>' . $GLOBALS['nombre'] . ' ' . $GLOBALS['app'] . ' ' . $GLOBALS['apm'] . '</b>
								</td>
			
								<td width="10%">
								</td>
							</tr>
			
							<tr>
								<td width="10%">
								</td>
			
								<td width="38%" style="font-size: 8px; text-align: center;">
									<b>NOMBRE Y FIRMA DEL<br>
									EJECUTIVO DE VENTAS</b>
								</td>
			
								<td width="4%">
								</td>
			
								<td width="38%" style="font-size: 8px; text-align: center;">
									<b>NOMBRE Y FIRMA DE<br>
									CONFORMIDAD DEL CONTATANTE</b>
								</td>
								
								<td width="10%">
								</td>
							</tr>
						</table>
					</td>

					<td width="35%" style="text-align: right; font-size: 8.5px;">
						<b><i>GRAND TURISMO EXPRESS</i></b><br>
						Francisco Villa N° 32 (Planta Alta)<br>
						Col. Santa María La Rivera,<br>
						Puebla, Pue. C.P.72010
						<br><br>
						222 220 03 28 | 222 359 00 08
						<br><br>
						grandturismoexpress@outlook.com
						<br><br>
						Facebook: Grand Turismo Express
						<br><br>
						www.grandturismoexpress.com
					</td>

					<td width="5%">
						
					</td>
				</tr>
			</table>';

		$this->writeHTML($html, true, false, true, false, '');

		$this->Image($GLOBALS['contacto'], 153, $this->getPageHeight() - 25, 4.3, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);

		$this->Image($GLOBALS['ubicacion'], 195, $this->getPageHeight() - 31, 4.3, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		$this->Image($GLOBALS['whatsapp'], 195, $this->getPageHeight() - 25, 4.3, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		$this->Image($GLOBALS['mail'], 195, $this->getPageHeight() - 19, 4.3, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		$this->Image($GLOBALS['facebook'], 195, $this->getPageHeight() - 13, 4.3, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		$this->Image($GLOBALS['web'], 195, $this->getPageHeight() - 7, 4.3, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);

		// Calcular la posición Y de la imagen
		$imageY = $this->getPageHeight() - 41;
		// Agregar la imagen al final de la página
		$this->Image($GLOBALS['foot'], 0, $imageY, $this->getPageWidth(), '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
	}
}
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo');
$pdf->SetTitle('Contrato');
$pdf->SetSubject('Contrato');
$pdf->SetKeywords('Contrato');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('6', '40', '6');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
$pdf->SetFooterMargin('40'); //46//40
// set auto page breaks
$pdf->SetAutoPageBreak(true, 38); //31//25//38
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 13);

$height = $pdf->getPageHeight();
$width = $pdf->getPageWidth();

// add a page   style="width: 300px; height: 200px;"
$pdf->AddPage('P', 'A4');

// Establecer el ancho de línea
$pdf->SetLineWidth(1.5);
$pdf->SetDrawColor(18, 30, 132);
$pdf->Line(2, 42.4, 5, 42.4);
$pdf->Line(39.5, 42.4, 203.5, 42.4);

$pdf->SetLineWidth(0.2);
$pdf->Line(44.5, 43.7, 208, 43.7);

$html = '
			<table width="100%" border="0" RULES="rows" style="color:#121e84; padding: 2px; font-size: 10px; text-align: justify;" class="table table-striped">
				<tr>
					<td width="100%" style="font-size: 10px; text-align: justify;">
						<b>DATOS GENERALES:</b>
					</td>
				</tr>

				<tr>
					<td width="23%">
						Denominación/Razón social: 
					</td>
					<td width="48%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
						' . $razon . '
					</td>
					<td width="6%">
						&nbsp;&nbsp;RFC: 
					</td>
					<td width="23%" style="background-color: #ddebf7; border-bottom:solid 5px white; border-right:solid 5px #ddebf7;">
						' . $rfc . '
					</td>
				</tr>

				<tr>
					<td width="20%">
						Nombre del contratante:
					</td>
					<td width="51%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
						' . $GLOBALS['nombre'] . ' ' . $GLOBALS['app'] . ' ' . $GLOBALS['apm'] . '
					</td>
					<td width="8%">
						&nbsp;&nbsp;ID/INE: 
					</td>
					<td width="21%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
						' . $ine . '
					</td>
				</tr>

				<tr>
					<td width="14%">
						Calle y Número: 
					</td>
					<td width="57%" style="background-color: #ddebf7; border-bottom:solid 5px white; font-size:'.$fontSizeNum1.';">
						' . $calle . '
					</td>
					<td width="13%">
						&nbsp;&nbsp;Código Postal:
					</td>
					<td width="16%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
						' . $cod_post . '
					</td>
				</tr>

				<tr>
					<td width="8%">
						Colonia:
					</td>
					<td width="24%" style="background-color: #ddebf7; border-bottom:solid 5px white; font-size:'.$fontSizeCol1.';">
						' . $colonia . '
					</td>
					<td width="14%">
						&nbsp;&nbsp;Ciudad o Mpio: 
					</td>
					<td width="20%" style="background-color: #ddebf7; border-bottom:solid 5px white; font-size: ' . $fontSizeCiudad1 . 'px;">
						' . $ciudad . '
					</td>
					<td width="14%">
						&nbsp;&nbsp;Teléfono Fijo 1:
					</td>
					<td width="20%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
						' . $telefono . '
					</td>
				</tr>

				<tr>
					<td width="7%">
						Estado:
					</td>
					<td width="17%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
						' . $estado_clientes . '
					</td>
					<td width="8%">
						&nbsp;&nbsp;Correo:
					</td>
					<td width="32%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
						' . $correo . '
					</td>
					<td width="16%">
						&nbsp;&nbsp;Teléfono Móvil 2:
					</td>
					<td width="20%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
						' . $telefono_2 . '
					</td>
				</tr>
			</table>';

// Establecer el ancho de línea
$pdf->SetLineWidth(1.5);
$pdf->SetDrawColor(18, 30, 132);
$pdf->Line(2, 70.5, 5, 70.5);
$pdf->Line(53.5, 70.5, 203.5, 70.5);

$pdf->SetLineWidth(0.2);
$pdf->Line(58.5, 71.8, 208, 71.8);

$html .= '
			<table width="100%" border="0" RULES="rows" style="color:#121e84; padding: 2px; font-size: 10px; text-align: justify;" class="table table-striped">
				<tr>
					<td width="100%" style="font-size: 10px; text-align: justify;">
						<b>DESCRIPCIÓN DEL SERVICIO:</b>
					</td>
				</tr>

				<tr>
					<td width="100%" style="font-size: 9px; text-align: justify;">
						<b>*DIRECCIÓN DE SALIDA:</b> 
					</td>
				</tr>

				<tr>
					<td width="14%">
						Calle y Número: 
					</td>
					<td width="57%" style="background-color: #ddebf7; border-bottom:solid 5px white; font-size:'.$fontSizeNum2.';">
						' . $calle_d . '
					</td>
					<td width="13%">
						&nbsp;&nbsp;Código Postal:
					</td>
					<td width="16%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
						' . $cp_d . '
					</td>
				</tr>

				<tr>
					<td width="8%">
						Colonia:
					</td>
					<td width="24%" style="background-color: #ddebf7; border-bottom:solid 5px white; font-size:'.$fontSizeCol2.';">
						' . $colonia_d . '
					</td>
					<td width="14%">
						&nbsp;&nbsp;Ciudad o Mpio: 
					</td>
					<td width="20%" style="background-color: #ddebf7; border-bottom:solid 5px white; font-size: ' . $fontSizeCiudad2 . 'px;">
						' . $ciudad_d . '
					</td>
					<td width="8%">
						&nbsp;&nbsp;Estado: 
					</td>
					<td width="26%" style="background-color: #ddebf7; border-bottom:solid 5px white; border-right:solid 5px #ddebf7;">
						' . $estado_detalles . '
					</td>
				</tr>

				<tr>
					<td width="32%" style="font-size: 9px; text-align: justify;">
						<b>*TOTAL DE DIAS:</b>
					</td>
					<td width="68%" style="font-size: 9px; text-align: justify;">
						<b>*FECHA Y HORA DE SALIDA:</b>
					</td>
				</tr>

				<tr>
					<td width="11%" style="background-color: #ddebf7;">
						' . $dias . '
					</td>
					<td width="14%">
						&nbsp;Viaje <b>Redondo:</b>
					</td>
					<td width="4%" style="background-color: #ddebf7; border-bottom:solid 5px white;  text-align: center;">
						' . $redondo . '
					</td>
					<td width="5%">
						&nbsp;Día:
					</td>
					<td width="14%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
						' . $dia_sal . '
					</td>
					<td width="7%">
						&nbsp;Fecha:
					</td>
					<td width="4%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
						' . $diaN_sal . '
					</td>
					<td width="1%">
					</td>
					<td width="8%" style="background-color: #ddebf7; border-bottom:solid 5px white; font-size:8px">
						' . $mes_sal . '
					</td>
					<td width="1%">
					</td>
					<td width="6%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
						' . $anio_sal . '
					</td>
					<td width="1%">
					</td>
					<td width="6%">
						&nbsp;Hora:
					</td>
					<td width="6%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
						' . $hora_sal . '
					</td>
					<td width="1%">
					</td>
					<td width="6%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
						' . $minutos_sal . '
					</td>
					<td width="5%">
						&nbsp;<b>Hrs.</b>
					</td>
				</tr>

				<tr>
					<td width="11%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
					</td>
					<td width="14%">
						&nbsp;Viaje <b>Sencillo:</b>
					</td>
					<td width="4%" style="background-color: #ddebf7; border-bottom:solid 5px white; text-align: center;">
						' . $sencillo . '
					</td>
					<td width="3%">
					</td>
					<td width="68%" style="font-size: 9px; text-align: justify;">
						<b>*FECHA Y HORA DE REGRESO:</b>
					</td>
				</tr>

				<tr>
					<td width="16%" style="font-size: 9px; text-align: justify;">
						<b>*TIPO DE SERVICIO:</b>
					</td>
					<td width="13%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
						' . $tipo_servicio_d . '
					</td>
					<td width="5%">
						&nbsp;Día:
					</td>
					<td width="14%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
						' . $dia_reg . '
					</td>
					<td width="7%">
						&nbsp;Fecha:
					</td>
					<td width="4%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
						' . $diaN_reg . '
					</td>
					<td width="1%">
					</td>
					<td width="8%" style="background-color: #ddebf7; border-bottom:solid 5px white; font-size:8px">
						' . $mes_reg . '
					</td>
					<td width="1%">
					</td>
					<td width="6%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
						' . $anio_reg . '
					</td>
					<td width="1%">
					</td>
					<td width="6%">
						&nbsp;Hora:
					</td>
					<td width="6%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
						' . $hora_regreso . '
					</td>
					<td width="1%">
					</td>
					<td width="6%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
						' . $minuto_regreso . '
					</td>
					<td width="5%">
						&nbsp;<b>Hrs.</b>
					</td>
				</tr>
			</table>';

// Establecer el ancho de línea
$pdf->SetLineWidth(1.5);
$pdf->SetDrawColor(18, 30, 132);
$pdf->Line(2, 107, 5, 107);
$pdf->Line(27.5, 107, 203.5, 107);

$pdf->SetLineWidth(0.2);
$pdf->Line(32.5, 108.3, 208, 108.3);

/*
$html .= '
			<table width="100%" border="0" RULES="rows" style="color:#121e84; padding: 2px; font-size: 10px; text-align: justify;" class="table table-striped">
				<tr>
					<td width="100%" style="font-size: 10px; text-align: justify;">
						<b>ITINERARIO:</b>
					</td>
				</tr>

				<tr>
					<td width="50%" style="font-size: 9px; text-align: left; background-color: #bdd7ee; border-bottom:solid 5px white; border-right: solid 5px white;">
						<b>ESTADO DESTINO: </b>' . $estado_destino . '
					</td>
					<td width="50%" style="font-size: 9px; background-color: #ddebf7; border-bottom:solid 5px white; border-bottom:solid 5px white; border-right: solid 5px #bdd7ee;">
						' . $destinoss[0] . '
					</td>
				</tr>

				<tr>
					<td width="100%" style="font-size: 9px; background-color: #ddebf7; border-bottom:solid 5px white; border-bottom:solid 5px white; border-right: solid 5px #bdd7ee;">
						' . $destinoss[1] . ' ' . $destinoss[2] . '
					</td>
				</tr>

				<tr>
					<td width="100%" style="font-size: 9px; background-color: #ddebf7; border-bottom:solid 5px white; border-bottom:solid 5px white; border-right: solid 5px #bdd7ee;">
						' . $destinoss[3] . ' ' . $destinoss[4] . ' 
					</td>
				</tr>
			</table>';
			*/


$html .= '<table width="100%" border="0" RULES="rows" style="color:#121e84; padding: 2px; font-size: 10px; text-align: justify;" class="table table-striped">
				<tr>
					<td width="100%" style="font-size: 10px; text-align: justify;">
						<b>ITINERARIO:</b>
					</td>
				</tr>

				<tr>
					<td width="50%" style="font-size: 8px; text-align: left; background-color: #bdd7ee; border-bottom:solid 5px white; border-right: solid 5px white;">
						<b>ESTADO DESTINO: </b>' . $estado_destino . '
					</td>
					<td width="50%" style="font-size: 8px; background-color: #ddebf7; border-bottom:solid 5px white; border-bottom:solid 5px white; border-right: solid 5px #bdd7ee;">
						' . $dest[0] . '
					</td>
					
				</tr>';


for ($i = 0; $i < count($dest); $i++) {

	if ($i == 0) {

		if(count($dest) == 1 ){
			for ($j = ($i - count($dest)); $j < count($dest); $j++) {
				$html .= '
					<tr>
						<td width="100%" style="text-align: center; font-size: 8px; background-color: #ddebf7; border-bottom:solid 5px white; border-bottom:solid 5px white; border-right: solid 5px #bdd7ee;">
							
						</td>
					</tr>
				';
			}
		}
		continue;
	}


	if ($i % 2 != 0) {
		if (($i + 1) > count($dest)) {
			$html .= '
				<tr>
					<td width="100%" style="text-align: center; font-size: 8px; background-color: #ddebf7; border-bottom:solid 5px white; border-bottom:solid 5px white; border-right: solid 5px #bdd7ee;">
						' . $dest[$i] . '
					</td>
				</tr>
			';
		} else {
			$html .= '
				<tr>
					<td width="100%" style="text-align: center; font-size: 8px; background-color: #ddebf7; border-bottom:solid 5px white; border-bottom:solid 5px white; border-right: solid 5px #bdd7ee;">
						' . $dest[$i] . ' ' . $dest[$i + 1] . '
					</td>
				</tr>
			';
		}
	}

	if($i == 1){
		if (count($dest) == 2 || count($dest) == 3) {
				$html .= '
					<tr>
						<td width="100%" style="text-align: center; font-size: 8px; background-color: #ddebf7; border-bottom:solid 5px white; border-bottom:solid 5px white; border-right: solid 5px #bdd7ee;">
							
						</td>
					</tr>
				';
		}
	}

}

$html .= '</table>';

if (count($dest) <= 5) {
	// Establecer el ancho de línea
	$pdf->SetLineWidth(1.5);
	$pdf->SetDrawColor(18, 30, 132);
	$pdf->Line(2, 123.5, 5, 123.5);
	$pdf->Line(60.5, 123.5, 203.5, 123.5);

	$pdf->SetLineWidth(0.2);
	$pdf->Line(65.5, 124.8, 208, 124.8);
}

$html .= '
			<table width="100%" border="0" RULES="rows" style="color:#121e84; padding: 2px; font-size: 10px; text-align: justify;" class="table table-striped">
				<tr>
					<td width="100%" style="font-size: 10px; text-align: justify;">
						<b>DESCRIPCIÓN DE LAS UNIDADES:</b>
					</td>
				</tr>
			</table>';

if (count($unidadesResult) > 2) {
	foreach ($unidadesResult as $unidad) {
		$subtotal = $subtotal + ($unidad->monto * $unidad->cantidad);
		$html .= '
				<table width="100%" border="0" RULES="rows" style="color:#121e84; padding: 2px; font-size: 10px; text-align: justify;" class="table table-striped">
					<tr>
						<td width="7%">
							Unidad:
						</td>
						<td width="33%" style="background-color: #ddebf7; border-bottom:solid 5px white; font-size: 9px;">
							' . $unidad->vehiculo . '
						</td>

						<td width="14%">
							&nbsp;Cant. Unidades: 
						</td>
						<td width="3%" style="background-color: #ddebf7; border-bottom:solid 5px white; border-right:solid 5px #ddebf7;">
							' . $unidad->cantidad . '
						</td>

						<td width="14%">
							&nbsp;Cant. Pasajeros:
						</td>
						<td width="3%" style="background-color: #ddebf7; border-bottom:solid 5px white; border-right:solid 5px #ddebf7;">
							' . $unidad->cant_pasajeros . '
						</td>

						<td width="14%">
							&nbsp;Importe Unidad:
						</td>
						<td width="2%" style="background-color: #ddebf7; border-bottom:solid 5px white; border-right:solid 5px #ddebf7;">
							$
						</td>
						<td width="10%" style="background-color: #ddebf7; border-bottom:solid 5px white; border-right:solid 5px #ddebf7; text-align: right;">
							' . number_format($unidad->monto, 2, '.', ',') . '
						</td>
					</tr>
				</table>';
	}
} else {

	for ($i = 0; $i < 2; $i++) {
		if (!isset($unidadesResult[$i])) {
			$unidadesResult[$i] = [
				"cantidad" => "0",
				"cant_pasajeros" => "0",
				"monto" => "0",
				"marca" => "",
				"vehiculo" => "",
				"tipo" => "0",
			];

			$unidadesResult[$i] = json_decode(json_encode($unidadesResult[$i]), false);
		}

		$unidad = $unidadesResult[$i];

		$subtotal = $subtotal + ($unidad->monto * $unidad->cantidad);

		$html .= '
				<table width="100%" border="0" RULES="rows" style="color:#121e84; padding: 2px; font-size: 10px; text-align: left;" class="table table-striped">
					<tr>
						<td width="7%">
							Unidad:
						</td>
						<td width="33%" style="background-color: #ddebf7; border-bottom:solid 5px white; font-size: 8.5px;">
							' . $unidad->vehiculo . '
						</td>

						<td width="14%">
							&nbsp;Cant. Unidades: 
						</td>
						<td width="3%" style="background-color: #ddebf7; border-bottom:solid 5px white; border-right:solid 5px #ddebf7;">
							' . $unidad->cantidad . '
						</td>

						<td width="14%">
							&nbsp;Cant. Pasajeros:
						</td>
						<td width="3%" style="background-color: #ddebf7; border-bottom:solid 5px white; border-right:solid 5px #ddebf7;">
							' . $unidad->cant_pasajeros . '
						</td>

						<td width="14%">
							&nbsp;Importe Unidad:
						</td>
						<td width="2%" style="background-color: #ddebf7; border-bottom:solid 5px white; border-right:solid 5px #ddebf7;">
							$
						</td>
						<td width="10%" style="background-color: #ddebf7; border-bottom:solid 5px white; border-right:solid 5px #ddebf7; text-align: right;">
							' . number_format($unidad->monto, 2, '.', ',') . '
						</td>
					</tr>
				</table>';
	}

	if (count($dest) <= 5) {
		// Establecer el ancho de línea
		$pdf->SetLineWidth(1.5);
		$pdf->SetDrawColor(18, 30, 132);
		$pdf->Line(2, 137.5, 5, 137.5);
		$pdf->Line(50.9, 137.5, 203.5, 137.5);

		$pdf->SetLineWidth(0.2);
		$pdf->Line(55.9, 138.8, 208, 138.8);
	}
}

$html .= '
			<table width="100%" border="0" RULES="rows" style="color:#121e84; padding: 2px; font-size: 10px; text-align: justify;" class="table table-striped">
				<tr>
					<td width="100%" style="font-size: 10px; text-align: justify;">
						<b>CLÁUSULAS DEL SERVICIO:</b>
					</td>
				</tr>

				<tr>
					<td width="70%" style="font-size: 7.5px; text-align: justify;">
						1. En caso de requerir factura se aplicará el I.V.A vigente del costo total del viaje.<br>
						2. Para contratar la unidad se necesita un anticipo mínimo del 25% del total del costo del viaje y faltando tres días para realizarse el servicio, el Contratante deberá acudir a las oficinas de dicha empresa a liquidar el Total. (El pago se efectuará en efectivo y en moneda nacional). 3. Toda cancelación realizada con 15 días o el mismo día de salida tienen un cargo del 100%. Así mismo, los anticipos quedarán en la empresa transportista como indemnización por recesión del contrato celebrado y deberá cubrir el restante dependiendo la penalización del 35% o 100% del importe total del contrato.<br>
						4. El cliente se hace responsable de la integridad interior y exterior del autobús supervisando que sus pasajeros no causen daños y perjuicios. Y en caso contrario, el contratante se compromete a cubrir los gastos de reparación de los daños ocasionados por sus acompañantes de viaje.<br>
						5. La unidad únicamente hará el ascenso y descenso de pasaje en la dirección acordada.<br>
						6. Todo recorrido que no esté descrito en el itinerario tendrá un costo adicional. Esta cláusula aplica solo que el autobús esté disponible y no esté contratado para un servicio posterior.<br>
						7. Cada hora extra utilizada después de la terminación de este contrato (*Hora de llegada después al punto de origen) tendrá un costo extra de $1,000 pesos.<br>
						8. En caso de alguna avería en la unidad se le enviará una unidad sustituida dependiendo de la situación y se le repondrá el tiempo que se pierda, pero la empresa no tendrá responsabilidad por incumplimiento.<br>
						9. Por su seguridad No se permite el sobrecupo en la unidad. El Seguro de Viaje solo cubre el total de asientos acordado, así mismo los niños mayores de 3 años deberán ocupar asiento.<br>
						10. Todas las unidades no transitan por carreteras o caminos de terracería.<br>
						11. El cliente tendrá que revisar las unidades al finalizar el servicio, ya que la empresa no se hace responsable por objetos olvidados dentro de ellas.<br>
						12. Manifiesta el contratante que es responsable de los objetos que ellos transportan por lo que se les prohíbe terminantemente transportar enervantes, estupefacientes, contrabando y materiales peligrosos dentro de nuestras unidades, así como personas ilegales.<br>
						13. Cualquier incumplimiento a las condiciones, genera la cancelación del mismo sin rembolso.<br>
						14. <b>NO INCLUYEN PAGO DE ESTACIONAMIENTOS.</b>
					</td>
					<td width="1%">
					</td>
					<td width="29%">
						<table width="100%" border="0" RULES="rows" style="color:#121e84; padding: 2px; font-size: 10px; text-align: justify;" class="table table-striped">
							<tr>
								<td width="62%" style="text-align: right;">
									Subtotal:
								</td>
								<td width="6%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
									$
								</td>
								<td width="32%" style="background-color: #ddebf7; border-bottom:solid 5px white; text-align: right;">
									' . number_format(($subtotal - $monto_descuento), 2, '.', ',') . '
								</td>
							</tr>

							<tr>
								<td>
								</td>
							</tr>

							<tr>
								<td>
								</td>
							</tr>

							<tr>
								<td width="62%" style="text-align: right;">
									<b>IMPORTE TOTAL:</b>
								</td>
								<td width="6%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
									$
								</td>
								<td width="32%" style="background-color: #ddebf7; border-bottom:solid 5px white; text-align: right;">
									' . number_format(($subtotal - $monto_descuento), 2, '.', ',') . '
								</td>
							</tr>

							<tr>
								<td width="62%" style="text-align: right;">
									<b>ANTICIPO 1:</b>
								</td>
								<td width="6%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
									$
								</td>
								<td width="32%" style="background-color: #ddebf7; border-bottom:solid 5px white; text-align: right;">
									' . number_format($monto_anticipo, 2, '.', ',') . '
								</td>
							</tr>

							<tr>
								<td width="62%" style="text-align: right;">
									<b>SALDO A LA FECHA:</b>
								</td>
								<td width="6%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
									$
								</td>
								<td width="32%" style="background-color: #ddebf7; border-bottom:solid 5px white; text-align: right;">
									' . number_format((($subtotal - $monto_descuento) - $monto_anticipo) - $pago, 2, '.', ',') . '
								</td>
							</tr>

							<tr>
								<td>
								</td>
							</tr>

							<tr>
								<td width="100%" style="text-align: center;">
									<b>OBSERVACIONES ADICIONALES:</b>
								</td>
							</tr>

							<tr>
								<td width="100%" style="font-size: 7.5px; background-color: #ddebf7; border-bottom:solid 5px white;">
									' . $observaciones[0] . '
								</td>
							</tr>
							<tr>
								<td width="100%" style="font-size: 7.5px; background-color: #ddebf7; border-bottom:solid 5px white;">
									' . $observaciones[1] . '
								</td>
							</tr>
							<tr>
								<td width="100%" style="font-size: 7.5px; background-color: #ddebf7; border-bottom:solid 5px white;">
									' . $observaciones[2] . '
								</td>
							</tr>
							<tr>
								<td width="100%" style="font-size: 7.5px; background-color: #ddebf7; border-bottom:solid 5px white;">
									' . $observaciones[3] . '
								</td>
							</tr>
							<tr>
								<td width="100%" style="font-size: 7.5px; background-color: #ddebf7; border-bottom:solid 5px white;">
									' . $observaciones[4] . '
								</td>
							</tr>
							<tr>
								<td width="100%" style="font-size: 7.5px; background-color: #ddebf7; border-bottom:solid 5px white;">
									' . $observaciones[5] . '
								</td>
							</tr>
							<tr>
								<td width="100%" style="font-size: 7.5px; background-color: #ddebf7; border-bottom:solid 5px white;">
									' . $observaciones[6] . '
								</td>
							</tr>	
						</table>
					</td>
				</tr>
				
				<tr>
					<td>
					</td>
				</tr>

				<tr>
					<td width="52%">
						Se celebra el presente contrato en la ciudad de Puebla, Pue. el día 
					</td>
					<td width="6%" style="background-color: #ddebf7; border-bottom:solid 5px white;">
						' . $fecha_dia . '
					</td>
					<td width="4%">
						&nbsp;de
					</td>
					<td width="24%" style="background-color: #ddebf7; border-bottom:solid 5px white; border-right:solid 5px #ddebf7;">
						' . $nombre_mes . '
					</td>
					<td width="4%">
						&nbsp;de
					</td>
					<td width="10%" style="background-color: #ddebf7; border-bottom:solid 5px white; border-right:solid 5px #ddebf7;">
						' . $fecha_anio . '
					</td>
				</tr>
			</table>';

$pdf->writeHTML($html, true, false, true, false, '');

$pdf->IncludeJS('print(true);');
//$pdf->Output('Contrato.pdf', 'I');
$pdf->Output(FCPATH . 'public/pdf/Contrato_' . $GLOBALS['id'] . '.pdf', 'FI');
//$pdf->Output(FCPATH.'public/pdf/Contrato_'. $GLOBALS['id'] .'.pdf', 'I');