<?php
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=Servicios_verificaciones".date('Ymd Gis').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<style>
  .pspaces{ font-size: 0.5px; }
</style>
<br>
<table border="1" id="unidad" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th colspan="9">DATOS DEL VEHÍCULO</th>
    </tr>
    <tr>
      <th>Número Económico</th>
      <th>Vehículo</th>
      <th>Modelo</th>
      <th>Placas</th>
      <th>Placas Federales</th>
      <th>Serie</th>
      <th>Motor</th>
      <th>Próxima Verificación</th>
      <th>KM Actual</th>
    </tr>
  </thead>
  <tbody>
    <?php 
      echo $uni;
    ?>
  </tbody>
</table>

<?php if($type == 1 || $type == 0){ ?>
  <br>
  <br>
  <br>
  <table border="1" id="serv" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
      <tr>
        <th colspan="6">DATOS DE SERVICIOS</th>
      </tr>
      <tr>
        <th>#</th>
        <th>Fecha de Servicio</th>
        <th>Kilometraje</th>
        <th>Tipo</th>
        <th>Importe</th>
        <th>Comentarios</th>
      </tr>
    </thead>
    <tbody>
      <?php 
        echo $serv;
      ?>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="4"></td>
        <td ><?php echo number_format($tot_ser,2); ?></td>
        <td></td>
      </tr>
    </tfoot>
  </table>
<?php } ?>


<?php if($type == 2 || $type == 0){ ?>
  <br>
  <br>
  <br>
  <table border="1" id="verf" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
      <tr>
        <th colspan="9">DATOS DE VERIFICACIONES</th>
      </tr>
      <tr>
        <th>#</th>
        <th colspan="4">Foto</th>
        <th>Fecha de Verificación</th>
        <th>Tipo</th>
        <th>Importe</th>
        <th>Observaciones</th>
      </tr>
    </thead>
    <tbody>
      <?php 
        echo $verf;
      ?>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="7"></td>
        <td ><?php echo number_format($tot_verif,2); ?></td>
        <td ></td>
      </tr>
    </tfoot>
  </table>
<?php } ?>