	<?php

	require_once('TCPDF4/tcpdf.php');
	//$ruta_base = $_SERVER['DOCUMENT_ROOT'];
	$this->load->helper('url');

	$englishDays = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
	$spanishDays = array('Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo');
	$englishMonths = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
	$spanishMonths = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
	$tipos_clientes = array('Potencial', 'Ocacional', 'Estandar', 'Bronce', 'Oro', 'Diamante');


	$GLOBALS['header'] = FCPATH . "public/img/orden_servicio/header.png";
	$GLOBALS['folio_cont'] = $folio_cont;
	$GLOBALS['folio_coti'] = $folio_coti;
	$GLOBALS['folio_oc'] = '';
	$GLOBALS['usuario'] = $usuario->nombre.' '.$usuario->apellido_p.' '.$usuario->apellido_m;
	$GLOBALS['fecha_hora_imp'] = $fecha_hora_imp;

	$empresa = $contratos->empresa;
	//$empresa = $clientes->empresa;

	$cliente = $clientes->nombre." ".$clientes->app." ".$clientes->apm;
	$GLOBALS['cliente'] = $cliente;
	$telefono = $clientes->telefono;
	
	$tipo_cliente = '';
	if(isset($clientes->tipo_cliente) && $clientes->tipo_cliente != ''){
		$tipo_cliente = $tipos_clientes[($clientes->tipo_cliente)-1];
	}


	/*
	// -- SALIDA --
	$salida = new DateTime($contratos->fecha_salida);

	$hora_salida = date('H', strtotime($contratos->hora_salida));
	$minuto_salida = date('i', strtotime($contratos->hora_salida));

	$fecha_dia_s = $salida->format("j");
	$fecha_anio_s = $salida->format("Y");
	$fecha_mes_s = $salida->format("F");
	$fecha_mes_s = str_replace($englishMonths, $spanishMonths, $fecha_mes_s);

	$nombre_dia_s = $salida->format("l");
	$nombre_dia_s = str_replace($englishDays, $spanishDays, $nombre_dia_s);


	// -- REGRESO --
	$regreso = new DateTime($contratos->fecha_regreso);
	$hora_regreso = date('H', strtotime($contratos->hora_regreso));
	$minuto_regreso = date('i', strtotime($contratos->hora_regreso));

	$fecha_dia_r = $regreso->format("j");
	$fecha_anio_r = $regreso->format("Y");
	$fecha_mes_r = $regreso->format("F");
	$fecha_mes_r = str_replace($englishMonths, $spanishMonths, $fecha_mes_r);

	$nombre_dia_r = $regreso->format("l");
	$nombre_dia_r = str_replace($englishDays, $spanishDays, $nombre_dia_r);
	// -----


	$diferencia = $salida->diff($regreso);
	$num_dias = $diferencia->format('%a');
	if($num_dias == 0){
		$num_dias = 1;
	}
*/

	$num_dias = 1;

	$estado = '';
	if(isset($estado_contrato) || $estado_contrato <= 0){
		$estado = $estado_contrato;
	}

	// Origen ---
	$origen = $contratos->lugar_origen;

	$origen_div = wordwrap($origen, 100, "\n", true);
	$origenes = explode("\n", $origen_div);
	$origenes[0] = isset($origenes[0])? $origenes[0] : '';
	$origenes[1] = isset($origenes[1])? $origenes[1] : '';
	$origenes[2] = isset($origenes[2])? $origenes[2] : '';


	$notas = $detalles->observaciones;
	if($notas == ''){
		$notas = '<br><br><br><br>';
	}


	$subtotal = 0;

	if (count($unidades) > 0) {
		foreach ($unidades->result() as $unidad) {
			$subtotal = $subtotal + ($unidad->monto * $unidad->cantidad);
		}
	}


	$monto_anticipo = 0;
	if(isset($contratos->monto_anticipo)){
		$monto_anticipo = $contratos->monto_anticipo;
	}

	$porc_anticipo = 0;
	if(isset($contratos->porc_anticipo)){
		$porc_anticipo = $contratos->porc_anticipo;	
	}

	$monto_descuento = 0;
	if(isset($contratos->porc_desc)){
		$monto_descuento = $contratos->porc_desc;
	}

	//$saldo = number_format((($subtotal - $monto_descuento) - $monto_anticipo)-$pago, 2, '.', ',');
	$saldo = (($subtotal - $monto_descuento) - $monto_anticipo)-$pago;
	$saldo_format = number_format($saldo, 2, '.', ',');

	//=======================================================================================
	class MYPDF extends TCPDF
	{

		//===========================================================
		var $Void = ""; 
		var $SP = " "; 
		var $Dot = "."; 
		var $Zero = "0"; 
		var $Neg = "Menos";
		function ValorEnLetras($x, $Moneda ){ 
			$s=""; 
			$Ent=""; 
			$Frc=""; 
			$Signo=""; 
					
			if(floatVal($x) < 0) {
				$Signo = $this->Neg . " "; 
			}else{
				$Signo = "";
			}
			
			if(intval(number_format($x,2,'.','') )!=$x){ //<- averiguar si tiene decimales 
				$s = number_format($x,2,'.',''); 
			}else{
				$s = number_format($x,2,'.',''); 
			}

			$Pto = strpos($s, $this->Dot); 
					
			if ($Pto === false) 
			{ 
				$Ent = $s; 
				$Frc = $this->Void; 
			} 
			else 
			{ 
				$Ent = substr($s, 0, $Pto ); 
				$Frc =  substr($s, $Pto+1); 
			} 

			if($Ent == $this->Zero || $Ent == $this->Void) 
				$s = "Cero "; 
			elseif( strlen($Ent) > 7) 
			{ 
				$s = $this->SubValLetra(intval( substr($Ent, 0,  strlen($Ent) - 6))) .  
							"Millones " . $this->SubValLetra(intval(substr($Ent,-6, 6))); 
			} 
			else 
			{ 
				$s = $this->SubValLetra(intval($Ent)); 
			} 

			if (substr($s,-9, 9) == "Millones " || substr($s,-7, 7) == "Millón ") 
				$s = $s . "de "; 

			$s = $s . $Moneda; 
			if ($Moneda=='MXN') {
				$abreviaturamoneda='M.N.';
			}else{
				$abreviaturamoneda='U.S.D';
			}

			if($Frc != $this->Void) 
			{ 
				$s = $s . " " . $Frc. "/100"; 
				//$s = $s . " " . $Frc . "/100"; 
			} 
			$letrass=$Signo . $s . " ".$abreviaturamoneda; 
			return ($Signo . $s . " ".$abreviaturamoneda);    
		} 
		function SubValLetra($numero) { 
				$Ptr=""; 
				$n=0; 
				$i=0; 
				$x =""; 
				$Rtn =""; 
				$Tem =""; 

				$x = trim("$numero"); 
				$n = strlen($x); 

				$Tem = $this->Void; 
				$i = $n; 
				
				while( $i > 0) 
				{ 
					$Tem = $this->Parte(intval(substr($x, $n - $i, 1).  
															str_repeat($this->Zero, $i - 1 ))); 
					If( $Tem != "Cero" ) 
							$Rtn .= $Tem . $this->SP; 
					$i = $i - 1; 
				} 

				
				//--------------------- GoSub FiltroMil ------------------------------ 
				$Rtn=str_replace(" Mil Mil", " Un Mil", $Rtn ); 
				while(1) 
				{ 
					$Ptr = strpos($Rtn, "Mil ");        
					If(!($Ptr===false)) 
					{ 
							If(! (strpos($Rtn, "Mil ",$Ptr + 1) === false )) 
								$this->ReplaceStringFrom($Rtn, "Mil ", "", $Ptr); 
							Else 
							break; 
					} 
					else break; 
				} 

				//--------------------- GoSub FiltroCiento ------------------------------ 
				$Ptr = -1; 
				do{ 
					$Ptr = strpos($Rtn, "Cien ", $Ptr+1); 
					if(!($Ptr===false)) 
					{ 
							$Tem = substr($Rtn, $Ptr + 5 ,1); 
							if( $Tem == "M" || $Tem == $this->Void) 
								; 
							else           
								$this->ReplaceStringFrom($Rtn, "Cien", "Ciento", $Ptr); 
					} 
				}while(!($Ptr === false)); 

				//--------------------- FiltroEspeciales ------------------------------ 
				$Rtn=str_replace("Diez Un", "Once", $Rtn ); 
				$Rtn=str_replace("Diez Dos", "Doce", $Rtn ); 
				$Rtn=str_replace("Diez Tres", "Trece", $Rtn ); 
				$Rtn=str_replace("Diez Cuatro", "Catorce", $Rtn ); 
				$Rtn=str_replace("Diez Cinco", "Quince", $Rtn ); 
				$Rtn=str_replace("Diez Seis", "Dieciseis", $Rtn ); 
				$Rtn=str_replace("Diez Siete", "Diecisiete", $Rtn ); 
				$Rtn=str_replace("Diez Ocho", "Dieciocho", $Rtn ); 
				$Rtn=str_replace("Diez Nueve", "Diecinueve", $Rtn ); 
				$Rtn=str_replace("Veinte Un", "Veintiun", $Rtn ); 
				$Rtn=str_replace("Veinte Dos", "Veintidos", $Rtn ); 
				$Rtn=str_replace("Veinte Tres", "Veintitres", $Rtn ); 
				$Rtn=str_replace("Veinte Cuatro", "Veinticuatro", $Rtn ); 
				$Rtn=str_replace("Veinte Cinco", "Veinticinco", $Rtn ); 
				$Rtn=str_replace("Veinte Seis", "Veintiseís", $Rtn ); 
				$Rtn=str_replace("Veinte Siete", "Veintisiete", $Rtn ); 
				$Rtn=str_replace("Veinte Ocho", "Veintiocho", $Rtn ); 
				$Rtn=str_replace("Veinte Nueve", "Veintinueve", $Rtn ); 

				//--------------------- FiltroUn ------------------------------ 
				If(substr($Rtn,0,1) == "M") $Rtn = "Un " . $Rtn; 
				//--------------------- Adicionar Y ------------------------------ 
				for($i=65; $i<=88; $i++) 
				{ 
					If($i != 77) 
						$Rtn=str_replace("a " . Chr($i), "* y " . Chr($i), $Rtn); 
				} 
				$Rtn=str_replace("*", "a" , $Rtn); 
				return($Rtn); 
		} 
		function ReplaceStringFrom(&$x, $OldWrd, $NewWrd, $Ptr) { 
			$x = substr($x, 0, $Ptr)  . $NewWrd . substr($x, strlen($OldWrd) + $Ptr); 
		} 
		function Parte($x) { 
				$Rtn=''; 
				$t=''; 
				$i=''; 
				Do 
				{ 
					switch($x) 
					{ 
						Case 0:  $t = "Cero";break; 
						Case 1:  $t = "Un";break; 
						Case 2:  $t = "Dos";break; 
						Case 3:  $t = "Tres";break; 
						Case 4:  $t = "Cuatro";break; 
						Case 5:  $t = "Cinco";break; 
						Case 6:  $t = "Seis";break; 
						Case 7:  $t = "Siete";break; 
						Case 8:  $t = "Ocho";break; 
						Case 9:  $t = "Nueve";break; 
						Case 10: $t = "Diez";break; 
						Case 20: $t = "Veinte";break; 
						Case 30: $t = "Treinta";break; 
						Case 40: $t = "Cuarenta";break; 
						Case 50: $t = "Cincuenta";break; 
						Case 60: $t = "Sesenta";break; 
						Case 70: $t = "Setenta";break; 
						Case 80: $t = "Ochenta";break; 
						Case 90: $t = "Noventa";break; 
						Case 100: $t = "Cien";break; 
						Case 200: $t = "Doscientos";break; 
						Case 300: $t = "Trescientos";break; 
						Case 400: $t = "Cuatrocientos";break; 
						Case 500: $t = "Quinientos";break; 
						Case 600: $t = "Seiscientos";break; 
						Case 700: $t = "Setecientos";break; 
						Case 800: $t = "Ochocientos";break; 
						Case 900: $t = "Novecientos";break; 
						Case 1000: $t = "Mil";break; 
						Case 1000000: $t = "Millón";break; 
					} 

					if($t == $this->Void) 
					{ 
						$i = floatval($i) + 1; 
						$x = $x / 1000; 
						if($x== 0) $i = 0; 
					} 
					else 
						break; 
								
				}while($i != 0); 
				
				$Rtn = $t; 
				switch($i) 
				{ 
					Case 0: $t = $this->Void;break; 
					Case 1: $t = " Mil";break; 
					Case 2: $t = " Millones";break; 
					Case 3: $t = " Billones";break; 
				} 
				return($Rtn . $t); 
		}  
	//===========================================================

		//Page header
		public function Header()
		{	
			$this->Image($GLOBALS['header'], 8, 3, $this->getPageWidth() - 16, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
			$this->SetY(19.5);
			$this->SetX(-36);//-50 -> 0000
			$this->SetFont('times', 'B', 12);
			$this->SetTextColor(192, 0, 0);
			$this->Cell(0, 5, $GLOBALS['folio_cont'], 0, false, 'C', 0, '', 0, false, 'M', 'M');

			$this->SetY(19.5);
			$this->SetX(-82);//-50 -> 0000
			$this->SetFont('helvetica', 'BI', 12);
			$this->SetTextColor(51, 153, 255);
			$this->Cell(0, 5, $GLOBALS['folio_coti'], 0, false, 'C', 0, '', 0, false, 'M', 'M');

			$this->SetY(10.5);
			$this->SetX(-60);//-50 -> 0000
			$this->SetFont('helvetica', 'B', 12);
			$this->SetTextColor(40, 40, 248);
			$this->Cell(0, 5, $GLOBALS['folio_oc'], 0, false, 'C', 0, '', 0, false, 'M', 'M');
		}

		// Page footer
		public function Footer()
		{
			$html = '<p style="text-align:rigth; font-size:8.5px"> IMPRESO POR: '.$GLOBALS['usuario'].' &nbsp;&nbsp;&nbsp; FECHA Y HORA: '.$GLOBALS['fecha_hora_imp'].'</p>';
			$this->writeHTML($html, true, false, true, false, '');

			/*
			$html = '<table width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify; border:solid 5px black;">
								<thead>
									<tr>
										<td style="color:#ffffff; font-size: 12px; text-align: center; background-color:#7f7f7f;">
											<b>FIRMAS:</b>
										</td>
									</tr>
								</thead>

								<tbody>
									<tr>
										<td width="100%" style="color:#3399ff; text-align: center;">
											<b>¡QUE TENGAN UN EXCELENTE VIAJE!</b>
											<br><br>
										</td>
									</tr>

									<tr>
										<td width="10%"></td>
										<td width="30%" style="border-bottom:solid 1px black; text-align: center;">
											
										</td>
										<td width="20%"></td>
										<td width="30%" style="border-bottom:solid 1px black; text-align: center;">
											'.$GLOBALS['cliente'].'
										</td>
										<td width="10%"></td>
									</tr>
									
									<tr>
										<td width="10%"></td>
										<td width="30%" style="text-align: center;">
											NOMBRE Y FIRMA <br>OPERADOR ASIGNADO
										</td>
										<td width="20%"></td>
										<td width="30%" style="text-align: center;">
											NOMBRE Y FIRMA <br>CLIENTE
										</td>
										<td width="10%"></td>
									</tr>

									<tr>
										<td width="50%"></td>
										<td width="50%" style="text-align: center;">
											<br><br>
											IMPRESO POR: '.$GLOBALS['usuario'].' &nbsp;&nbsp;&nbsp; FECHA Y HORA: '.$GLOBALS['fecha_hora_imp'].'
										</td>
									</tr>
								</tbody>
							</table>';
							
							$this->writeHTML($html, true, false, true, false, '');
							*/
		}


	}
	$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Mangoo');
	$pdf->SetTitle('Orden De Servicio');
	$pdf->SetSubject('Orden De Servicio');
	$pdf->SetKeywords('Orden De Servicio');

	// set default header data
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

	// set header and footer fonts
	$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins('10', '24', '10');
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
	$pdf->SetFooterMargin('5');//46//40
	// set auto page breaks
	$pdf->SetAutoPageBreak(true, 5);//31//25//38
	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	$pdf->SetFont('dejavusans', '', 13);

	$height = $pdf->getPageHeight();
	$width = $pdf->getPageWidth();

	// add a page   style="width: 300px; height: 200px;"
	$pdf->AddPage('P', 'A4');

	$html = '
				<style type="text/css">
					.border_f{ border:solid 5px black; }
					.p_space{ font-size: 1px;}
				</style>

				<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
					<thead>
						<tr>
							<td class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#7f7f7f;">
								<b>DATOS DEL CLIENTE:</b>
							</td>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td width="21%">
								&nbsp;EMPRESA CONTRATANTE:
							</td>
							<td width="78%" style="border-bottom:solid 1px black;">
								'.$empresa.'
							</td>
							<td width="1%" ></td>
						</tr>

						<tr>
							<td width="21%">
								&nbsp;RESPONSABLE DEL VIAJE: 
							</td>
							<td width="78%" style="border-bottom:solid 1px black;">
								'.$cliente.'
							</td>
							<td width="1%" ></td>
						</tr>

						<tr>
							<td width="15%">
								&nbsp;TIPO DE CLIENTE:
							</td>
							<td width="35%" style="border-bottom:solid 1px black;">
								'.$tipo_cliente.'
							</td>

							<td width="23%">
								TELEFONO DE CONTACTO 1: 
							</td>
							<td width="26%" style="border-bottom:solid 1px black;">
								'.$telefono.'
							</td>
							<td width="1%" ></td>
						</tr>

						<tr>
							<td width="50%"></td>
							
							<td width="23%">
								TELEFONO DE CONTACTO 2: 
							</td>
							<td width="26%" style="border-bottom:solid 1px black;">
								
							</td>
							<td width="1%"></td>
						</tr>

						<tr>
							<td width="100%"><p class="p_space">&nbsp;</p></td>
						</tr>
					</tbody>
				</table>';

	if (count($destinos) > 0) {
		$size = count($destinos);
		// ----- SALIDA -----
		$salida = new DateTime($destinos[0]->fecha);
		$hora_salida = date('H', strtotime($destinos[0]->hora));
		$minuto_salida = date('i', strtotime($destinos[0]->hora));

		$fecha_dia_s = $salida->format("j");
		$fecha_anio_s = $salida->format("Y");
		$fecha_mes_s = $salida->format("F");
		$fecha_mes_s = str_replace($englishMonths, $spanishMonths, $fecha_mes_s);

		$nombre_dia_s = $salida->format("l");
		$nombre_dia_s = str_replace($englishDays, $spanishDays, $nombre_dia_s);

		// -- REGRESO --
		$regreso = new DateTime($destinos[($size-1)]->fecha_regreso);
		$hora_regreso = date('H', strtotime($destinos[($size-1)]->hora_regreso));
		$minuto_regreso = date('i', strtotime($destinos[($size-1)]->hora_regreso));

		$fecha_dia_r = $regreso->format("j");
		$fecha_anio_r = $regreso->format("Y");
		$fecha_mes_r = $regreso->format("F");
		$fecha_mes_r = str_replace($englishMonths, $spanishMonths, $fecha_mes_r);

		$nombre_dia_r = $regreso->format("l");
		$nombre_dia_r = str_replace($englishDays, $spanishDays, $nombre_dia_r);


		$diferencia = $salida->diff($regreso);
		$num_dias = $diferencia->format('%a');
		if($num_dias == 0){
			$num_dias = 1;
		}
	}

	$html .= '<br><br>
				<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
					<thead>
						<tr>
							<td class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#7f7f7f;">
								<b>DATOS DEL SERVICIO:</b>
							</td>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td width="100%"><p class="p_space">&nbsp;</p></td>
						</tr>

						<tr>
							<td width="1%"></td>
							<td width="18%" style="text-align: center; background-color:#d9d9d9; ">
								SALIDA
							</td>
							<td width="1%"></td>
							<td width="7%">
								FECHA: 
							</td>
							<td width="50%" style="text-align: center; border-bottom:solid 1px black;">
								'.$nombre_dia_s.', '.$fecha_dia_s.' de '.$fecha_mes_s.' de '.$fecha_anio_s.'
							</td>
							<td width="6%">
								HORA:
							</td>
							<td width="16%" style="text-align: center; border-bottom:solid 1px black;" >
								'.$hora_salida.':'.$minuto_salida.' Hrs.
							</td>
							<td width="1%"></td>
						</tr>

						<tr>
							<td width="100%"><p class="p_space">&nbsp;</p></td>
						</tr>

						<tr>
							<td width="1%"></td>
							<td width="18%" style="text-align: center; background-color:#d9d9d9; ">
								REGRESO
							</td>
							<td width="1%"></td>
							<td width="7%">
								FECHA: 
							</td>
							<td width="50%" style="text-align: center; border-bottom:solid 1px black;">
								'.$nombre_dia_r.', '.$fecha_dia_r.' de '.$fecha_mes_r.' de '.$fecha_anio_r.'
							</td>
							<td width="6%">
								HORA:
							</td>
							<td width="16%" style="text-align: center; border-bottom:solid 1px black;" >
								'.$hora_regreso.':'.$minuto_regreso.' Hrs.
							</td>
							<td width="1%"></td>
						</tr>

						<tr>
							<td width="100%"><p class="p_space">&nbsp;</p></td>
						</tr>

						<tr>
							<td width="1%"></td>
							<td width="18%" style="text-align: center; background-color:#d9d9d9; ">
								NO. DE DÍAS
							</td>
							<td width="1%"></td>
							<td width="13%" style="border-bottom:solid 1px black;">
								'.$num_dias.'
							</td>

							<td width="1%"></td>
							<td width="15%" style="text-align: center; background-color:#d9d9d9; ">
								ESTADO DESTINO
							</td>
							<td width="1%"></td>
							<td width="16%" style="border-bottom:solid 1px black;">
								'.$estado.'
							</td>

							<td width="1%"></td>
							<td width="15%" style="text-align: center; background-color:#d9d9d9; ">
								ESTADO DESTINO
							</td>
							<td width="1%"></td>
							<td width="16%" style="border-bottom:solid 1px black;">

							</td>
							<td width="1%"></td>
						</tr>

						<tr>
							<td width="100%"><p class="p_space">&nbsp;</p></td>
						</tr>

						<tr>
							<td width="1%"></td>
							<td width="98%" style="text-align: center; background-color:#d9d9d9; border:solid 1px black;">
								DIRECCIÓN DE ORIGEN:
							</td>
							<td width="1%"></td>
						</tr>

						<tr>
							<td width="1%"></td>
							<td width="98%" style="border:solid 1px black; text-align: center">
								'.$origenes[0].'
							</td>
							<td width="1%"></td>
						</tr>

						<tr>
							<td width="1%"></td>
							<td width="98%" style="border:solid 1px black; text-align: center">
								'.$origenes[1].'
							</td>
							<td width="1%"></td>
						</tr>

						<tr>
							<td width="1%"></td>
							<td width="98%" style="border:solid 1px black; text-align: center">
								'.$origenes[2].'
							</td>
							<td width="1%"></td>
						</tr>

						<tr>
							<td width="100%"><p class="p_space">&nbsp;</p></td>
						</tr>

						<tr>
							<td width="1%"></td>
							<td width="98%" style="text-align: center; background-color:#d9d9d9; border:solid 1px black;">
								ITINERARIO:
							</td>
							<td width="1%"></td>
						</tr>';




	if (count($destinos) > 0) {
		$hora_sal = '';
		$fecha_sal = '';
		foreach ($destinos as $destino) {

			$fecha_s = $destino->fecha;
			$sal = new DateTime($fecha_s);
			$dia_sal = $sal->format("l");
			$dia_sal = str_replace($englishDays, $spanishDays, $dia_sal);

			$fecha_s_dia = $sal->format("j");
			$fecha_s_anio = $sal->format("Y");
			$fecha_s_mes = $sal->format("F");
			$fecha_s_mes = str_replace($englishMonths, $spanishMonths, $fecha_s_mes);

			$fecha_sal = $dia_sal . ', ' .$fecha_s_dia.' de '. $fecha_s_mes.' de '. $fecha_s_anio;
			$hora_sal = date('H:i', strtotime($destino->hora)).' Hrs';

			$fecha_s_dia = date('d', strtotime($contratos->fecha_regreso));
			$fecha_s_anio = date('Y', strtotime($contratos->fecha_regreso));
			$fecha_s_mes = date('m', strtotime($contratos->fecha_regreso));

			$html .='<tr>
							<td width="1%"></td>
							<td width="98%" style="border:solid 1px black; text-align: center">
								'.$destino->lugar.' - '.$fecha_sal.' - '.$hora_sal.' - '.$destino->lugar_hospeda.'
							</td>
							<td width="1%"></td>
						</tr>';
		}
	}else{
			$html .='<tr>
								<td width="1%"></td>
								<td width="98%" style="border:solid 1px black;"></td>
								<td width="1%"></td>
							</tr>
							<tr>
								<td width="1%"></td>
								<td width="98%" style="border:solid 1px black;"></td>
								<td width="1%"></td>
							</tr>
							<tr>
								<td width="1%"></td>
								<td width="98%" style="border:solid 1px black;"></td>
								<td width="1%"></td>
							</tr>';
	}


	$html .= 	'<tr>
							<td width="100%"><p class="p_space">&nbsp;</p></td>
						</tr>

						<tr>
							<td width="100%" style="color:#2828f8; text-align: center; font-size: 9px">
								<b>****TRASLADAR A LOS PASAJEROS A LOS LUGARES MENCIONADOS, <span style="color:#00b050;">FAVOR DE SER AMABLE Y TOLERANTE.****</span></b>
							</td>
						</tr>

						<tr>
							<td width="100%"><p class="p_space">&nbsp;</p></td>
						</tr>

					</tbody>
				</table>';


	$html .= '<br><br>
				<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
					<thead>
						<tr>
							<td style="color:#ffffff; font-size: 12px; text-align: center; background-color:#7f7f7f;">
								<b>DATOS DEL OPERADOR ASIGNADO / DATOS DEL VEHÍCULO:</b>
							</td>
						</tr>
					</thead>
					<tbody>';

	if (count($choferes) > 0) {
		foreach ($choferes as $chofer) {
			$operadores .= $chofer->nombre.' '.$chofer->apellido_p.' '.$chofer->apellido_m.'<br>';
	$html .= '<tr>
							<td width="19%">
								&nbsp;OPERADOR ASIGNADO:
							</td>
							<td width="50%" style="border-bottom:solid 1px black;">
								'.$chofer->nombre.' '.$chofer->apellido_p.' '.$chofer->apellido_m.'
							</td>

							<td width="10%">
								TELEFONO:
							</td>
							<td width="20%" style="border-bottom:solid 1px black;">
								'.$chofer->telefono.'
							</td>
							<td width="1%" ></td>
						</tr>

						<tr>
							<td width="19%">
								&nbsp;VEHÍCULO ASIGNADO:
							</td>
							<td width="50%" style="border-bottom:solid 1px black;">
								'.$chofer->vehiculo.'
							</td>

							<td width="31%"></td>
						</tr>

						<tr>
							<td width="8%">
								&nbsp;MARCA:
							</td>
							<td width="30%" style="border-bottom:solid 1px black;">
								'.$chofer->marca.'
							</td>

							<td width="8%">
								MODELO:
							</td>
							<td width="23%" style="border-bottom:solid 1px black;">
								'.$chofer->modelo.'
							</td>

							<td width="7%">
								PLACAS:
							</td>
							<td width="23%" style="border-bottom:solid 1px black;">
								'.$chofer->placas.'
							</td>
							<td width="1%" ></td>
						</tr>
						<tr>
							<td width="100%" style="border-bottom:solid 1px black;"><p class="p_space">&nbsp;</p></td>
						</tr>';
		}
	}

	if(count($unidades) > 0){
		foreach ($unidades->result() as $unidad) {
			$idU = $unidad->id;

			$choferIsset = array_filter($choferes, function($chofer) use ($idU) {
				return $chofer->idUnidPros == $idU;
			});

			if (empty($choferIsset)) {
				
				$html .= '<tr>
								<td width="19%">
									&nbsp;OPERADOR ASIGNADO:
								</td>
								<td width="50%" style="border-bottom:solid 1px black;">
									--- SIN ASIGNACIÓN ---
								</td>

								<td width="10%">
									TELEFONO:
								</td>
								<td width="20%" style="border-bottom:solid 1px black;">
									---
								</td>
								<td width="1%" ></td>
							</tr>

							<tr>
								<td width="19%">
									&nbsp;VEHÍCULO ASIGNADO:
								</td>
								<td width="50%" style="border-bottom:solid 1px black;">
									'.$unidad->vehiculo.'
								</td>

								<td width="31%"></td>
							</tr>

							<tr>
								<td width="8%">
									&nbsp;MARCA:
								</td>
								<td width="30%" style="border-bottom:solid 1px black;">
									'.$unidad->marca.'
								</td>

								<td width="8%">
									MODELO:
								</td>
								<td width="23%" style="border-bottom:solid 1px black;">
									'.$unidad->modelo.'
								</td>

								<td width="7%">
									PLACAS:
								</td>
								<td width="23%" style="border-bottom:solid 1px black;">
									'.$unidad->placas.'
								</td>
								<td width="1%" ></td>
							</tr>
							<tr>
								<td width="100%" style="border-bottom:solid 1px black;"><p class="p_space">&nbsp;</p></td>
							</tr>';

			}

		}
	}else{
		$html .= '<tr>
								<td width="19%">
									&nbsp;OPERADOR ASIGNADO:
								</td>
								<td width="50%" style="border-bottom:solid 1px black;">
								</td>

								<td width="10%">
									TELEFONO:
								</td>
								<td width="20%" style="border-bottom:solid 1px black;">
								</td>
								<td width="1%" ></td>
							</tr>

							<tr>
								<td width="19%">
									&nbsp;VEHÍCULO ASIGNADO:
								</td>
								<td width="50%" style="border-bottom:solid 1px black;">
								</td>

								<td width="31%"></td>
							</tr>

							<tr>
								<td width="8%">
									&nbsp;MARCA:
								</td>
								<td width="30%" style="border-bottom:solid 1px black;">
								</td>

								<td width="8%">
									MODELO:
								</td>
								<td width="23%" style="border-bottom:solid 1px black;">
								</td>

								<td width="7%">
									PLACAS:
								</td>
								<td width="23%" style="border-bottom:solid 1px black;">
								</td>
								<td width="1%" ></td>
							</tr>';
	}


	$html .= '</tbody>
				</table>';

	$html .= '<br><br>
				<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 6px; text-align: justify;">
					<thead>
						<tr>
							<td class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#7f7f7f;">
								<b>CONDICIONES DE CONTRATACIÓN:</b>
							</td>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td width="100%">
								<b>CONDICIONES DE CONTRATACION</b><br>
								1.-TODOS LOS NIÑOS DE 5 AÑOS O MAYORES FORMAN PARTE DEL CUPO DEL AUTOBUS, Y SOLAMENTE SE AUTORIZA A 3 NIÑOS MENORES DE 5 AÑOS A VIAJAR ASEGURADOS EN EXCESO AL CUPO AUTORIZADO.<br>
								2.-QUEDA PROHIBIDO INGERIR BEBIDAS ALCOHOLICAS Y FUMAR DENTRO DEL AUTOBUS (REGLAMENTO DE VIAS GENERALES DE COMUNICACION).<br>
								ASI COMO HACER PARADAS Y/O RECORRIDOS EXTRAORDINARIOS AL ITINERARIO, QUE NO ESTEN PACTADAS EN EL CONTRATO.<br>
								3.-LA EMPRESA NO SE HACE RESPONSABLE DE OBJETOS Y VALORES OLVIDADOS DENTRO DEL VEHÍCULO.<br>
								4.-EL CLIENTE SE OBLIGA A CUMPLIR CON LOS HORARIOS PACTADOS O PAGAR LA DIFERENCIA DEL TIEMPO QUE SE EXCEDA.<br>
								5.-LAS CANCELACIONES O MODIFICACIONES DEBERAN REPORTARSE 48 HORAS ANTES DE LA SALIDA, SOLICITANDO EL NUMERO DE CLAVE DEL REPORTE, SIN EL CUAL NO SE ACEPTARA NINGUNA RECLAMACION Y SE HARAN LOS CARGOS CORRESPONDIENTES.<br>
								6.-EN CASO DE ATRASO O INCUMPLIMIENTO DE HORARIOS PACTADOS DERIVADOS DE PROBLEMAS DE TRANSITO O DESCOMPOSTURAS DEL VEHICULO, LA EMPRESA SE OBLIGA EXCLUSIVAMENTE, AL ENVIO DE OTRA UNIDAD PARA EL CUMPLIMIENTO DEL SERVICO.
							</td>
						</tr>

						<tr>
							<td width="100%" style="color:#ee0000; font-size: 10px; text-align: center;">
								<b>NO INCLUYE PAGO DE ESTACIONAMIENTOS</b>
							</td>
						</tr>
					</tbody>
				</table>';


	$html .= '<br><br>
				<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
					<thead>
						<tr>
							<td class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#7f7f7f;">
								<b>IMPORTE POR COBRAR:</b>
							</td>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td width="17%">
								&nbsp;SALDO A LA FECHA:
							</td>
							<td width="22%" style="border-bottom:solid 1px black;">
								<b>$</b> '.$saldo_format.'
							</td>

							<td width="2%">
								&nbsp;(
							</td>
							<td width="56%" style="border-bottom:solid 1px black;">
								'.($pdf->ValorEnLetras($saldo,'MXN')).'
							</td>
							<td width="2%">
								)
							</td>
							<td width="1%" ></td>
						</tr>

						<tr>
							<td width="100%"><br></td>
						</tr>
					</tbody>
				</table>';


	$html .= '<br><br>
				<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
					<thead>
						<tr>
							<td class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#7f7f7f;">
								<b>NOTAS:</b>
							</td>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td width="100%">
								<i>'.$notas.'</i>
							</td>
						</tr>

						<tr>
							<td width="100%"><p class="p_space">&nbsp;</p></td>
						</tr>
					</tbody>
				</table>';


	$html .= '<br><br>
				<table width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify; border:solid 5px black;">
					<thead>
						<tr>
							<td style="color:#ffffff; font-size: 12px; text-align: center; background-color:#7f7f7f;">
								<b>FIRMAS:</b>
							</td>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td width="100%" style="color:#3399ff; text-align: center;">
								<b>¡QUE TENGAN UN EXCELENTE VIAJE!</b>
								<br>
							</td>
						</tr>

						<tr>
							<td width="10%"></td>
							<td width="30%" style="border-bottom:solid 1px black; text-align: center;">
								
							</td>
							<td width="20%"></td>
							<td width="30%" style="border-bottom:solid 1px black; text-align: center;">
								'.$GLOBALS['cliente'].'
							</td>
							<td width="10%"></td>
						</tr>
						
						<tr>
							<td width="10%"></td>
							<td width="30%" style="text-align: center;">
								NOMBRE Y FIRMA <br>OPERADOR ASIGNADO
							</td>
							<td width="20%"></td>
							<td width="30%" style="text-align: center;">
								NOMBRE Y FIRMA <br>CLIENTE
							</td>
							<td width="10%"></td>
						</tr>
					</tbody>
				</table>';


	$pdf->writeHTML($html, true, false, true, false, '');

	$pdf->IncludeJS('print(true);');
	//$pdf->Output('Orden_servicio.pdf', 'I');
	$pdf->Output(FCPATH.'public/pdf/Orden_servicio_'. $folio_cont .'.pdf', 'FI');
	//$pdf->Output(FCPATH.'public/pdf/Orden_servicio_'. $id .'.pdf', 'I');