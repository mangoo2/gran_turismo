<?php

require_once('TCPDF4/tcpdf.php');
//$ruta_base = $_SERVER['DOCUMENT_ROOT'];
$this->load->helper('url');

$GLOBALS['logo'] = FCPATH . "public/img/cotizaciones/logo.png";
$GLOBALS['foot'] = FCPATH . "public/img/cotizaciones/footer.png";
$GLOBALS['headerTable'] = true;

//=======================================================================================
class MYPDF extends TCPDF
{
	public function Header()
	{
		$html = '
			<table width="100%" border="0" style="padding: 2px; font-size: 10px; color:black; text-align: justify;">
				<tr>
					<td width="25%">
						<img width="140px" src="' . $GLOBALS['logo'] . '" >
					</td>
					<td width="35%"></td>
					<td width="40%">
						<br><br><br>
						<table>
							<thead>
								<tr>
									<td width="100%" border="1" style="background-color:#002060; color:white; text-align: center;"> <b><span style="font-size: 16px;">CONTRATOS</span></b><br> </td>
								</tr>
							</thead>
							<tbody>
							
								<tr>
									<td width="50%" border="1" style="text-align: right;"> <b>P</b>ÁGINA &nbsp; </td>
									<td width="50%" border="1" style="font-size: 9px; text-align: center;"> '.$this->getAliasNumPage().' DE '.$this->getAliasNbPages().' </td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</table>';
		$this->writeHTML($html, true, false, true, false, '');
	}

	// Page footer
	public function Footer()
	{
		// Calcular la posición Y de la imagen
		/*$imageY = $this->getPageHeight() - 58;

		// Agregar la imagen al final de la página
		$this->Image($GLOBALS['foot'], 0, $imageY, $this->getPageWidth(), '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);*/
	}
}
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo');
$pdf->SetTitle('LISTA DE CONTRATOS');
$pdf->SetSubject('Contratos');
$pdf->SetKeywords('Contratos');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('7', '34', '7');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
$pdf->SetFooterMargin(21);
// set auto page breaks
$pdf->SetAutoPageBreak(true, 20);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 13);

$height = $pdf->getPageHeight();
$width = $pdf->getPageWidth();

// add a page   style="width: 300px; height: 200px;"
$pdf->AddPage('L', 'A4');

	$html = '
			<style type="text/css">
				.p_space{ font-size: 7.2px;}
				.p_space-2{ font-size: 2.5px;}
				.p_space-t1{ font-size: 3.5px;}
				.p_space-t2{ font-size: 2px;}
			</style>

			<table width="100%" border="1" style="padding: 2px; font-size: 10px; color:black; text-align: justify;">
				<thead>
					<tr style="background-color:#002060; color:white; font-size: 7.5px; text-align: center;">
						<th width="2%">#</th>
						<th width="4%">Folio</th>
                        <th width="5%">Cliente</th>
                        <th width="4%">Tipo</th>
                        <th width="4%">Empresa</th>
                        <th width="4%">Fecha registro</th>
                        <th width="5%">Lugar origen</th>
                        <th width="4%">Fecha contrato</th>
                        <th width="4%">Fecha salida</th>
                        <th width="5%">Unidad(es)</th>
                        <th width="8%">Destino</th>
                        <th width="3%">No.<br> días</th>
                        <th width="4%">Teléfono</th>
                        <th width="4%">Teléfono 2</th>
                        <th width="5%">Email</th>
                        <th width="4%">Total</th>
                        <th width="4%">Anticipo</th>
                        <th width="3%">Desc.</th>
                        <th width="5%">Pagado</th>
                        <th width="5%">Vendedor</th>
                        <th width="4%">Prioridad</th>
                        <th width="4%">Estatus</th>
                        <th width="6%">Obervaciones</th>
					</tr>
				</thead>
				<tbody>';

		foreach ($con as $c) {
            $tipo="";
            $unid = $this->ModeloContratos->getUnidadesContrato($c->id);

            $tabUnidades = '<table border="0"><tbody>';
            $tabDestinos = '<table border="0"><tbody>';

            foreach($unid->result() as $u){
                $dest = $this->ModeloGeneral->getselectwhere2('destino_prospecto', array('id_contrato' => $c->id, 'id_unidPros' => $u->id, "estatus" => 1));
                log_message('error','Dest: '.json_encode($dest->result()));

                $rowspan = $unid->num_rows() > 1 ? $unid->num_rows() : 1;

                $tabUnidades .= '
                    <tr>
                        <td rowspan="' . $rowspan . '">
                            ' . $u->vehiculo . '
                        </td>
                    </tr>
                ';

                $destino = '';
                foreach ($dest->result() as $key => $d) {
                    $destino .= $d->lugar;
                    if ($key < ($dest->num_rows() - 1)) {
                        $destino .= ' | ';
                    }
                }

                $tabDestinos .= '
                    <tr style="vertical-align: middle;">
                        <td rowspan="' . $rowspan . '">
                            ' . $destino . '
                        </td>
                    </tr>
                ';

                if ($rowspan > 1) {
                    for ($i = 0; $i < ($rowspan - 1); $i++) {
                        $tabUnidades .= '<tr><td class="p_space-t2"></td></tr>';
                        $tabDestinos .= '<tr><td class="p_space-t2"></td></tr>';
                    }
                }
            }

            $tabUnidades .= '</tbody></table>';
            $tabDestinos .= '</tbody></table>';




            log_message('error','tabUnidades: '.json_encode($tabUnidades));
            log_message('error','tabDestinos: '.json_encode($tabDestinos));

            if($c->tipo=="0"){
                $tipo="Prospecto";
            }
            if($c->tipo_cliente=="1"){
                $tipo="Potencial";
            }else if($c->tipo_cliente=="2"){
                $tipo="Ocacional";
            }else if($c->tipo_cliente=="3"){
                $tipo="Estandar";
            }else if($c->tipo_cliente=="4"){
                $tipo="Bronce";
            }else if($c->tipo_cliente=="5"){
                $tipo="Oro";
            }else if($c->tipo_cliente=="6"){
                $tipo="Diamante";
            }

            $prioridad="";
            if($c->prioridad=="1"){
                $prioridad="<span class='btn btn-danger'>Alta</span>";
            }else if($c->prioridad=="2"){
                $prioridad="<span class='btn btn-warning'>Media</span>";
            }else if($c->prioridad=="3"){
                $prioridad="<span class='btn btn-info'>Baja</span>";
            }

            if($c->seguimiento=="1"){
                $seguimiento="<span class='btn btn-light'>Creada</span>";
            }else if($c->seguimiento=="2"){
                $seguimiento="<span class='btn btn-info'>En proceso</span>";
            }else if($c->seguimiento=="3"){
                $seguimiento="<span class='btn btn-warning'>Revisada</span>";
            }else if($c->seguimiento=="4"){
                $seguimiento="<span class='btn btn-success'>Autorizada</span>";
            }else if($c->seguimiento=="5"){
                $seguimiento="<span class='btn btn-secondary'>Expirada</span>";
            }else if($c->seguimiento=="6"){
                $seguimiento="<span class='btn btn-danger'>Rechazada</span>";
            }else if($c->seguimiento=="7"){
                $seguimiento="<span class='btn btn-primary'>Enviada</span>";
            }

            $pagado="";
            if($c->liquidado=="1"){
                $pagado="Pagado";
            }else{
                $resta=$c->tot_unids-$c->monto_anticipo-$c->porc_desc-$c->tot_pagos;
                $pagado=number_format($resta,2)." Pendiente";
            }


            $fecha_s = $c->fecha_salida;
            $sal = new DateTime($fecha_s);
            $fecha_r = $c->fecha_regreso;
            $reg = new DateTime($fecha_r);

            $diferencia = $sal->diff($reg);
            if ($diferencia->format('%a') == 0) {
                $tot_dias = 1;
            } else {
                $tot_dias = intval($diferencia->format('%a'));
            }

            $html .='
            <tr style="font-size: 6px; vertical-align: middle; text-align: center;">
                <td width="2%">'.$c->id.'</td>
                <td width="4%">'.$c->folio.'</td>
                <td width="5%">'.$c->cliente.'</td>
                <td width="4%">'.$tipo.'</td>
                <td width="4%">'.$c->empresa.'</td>
                <td width="4%">'.$c->fecha_reg.'</td>
                <td width="5%">'.$c->lugar_origen.'</td>
                <td width="4%">'.$c->fecha_contrato.'</td>
                <td width="4%">'.$c->fecha_salida.'</td>
                <td width="5%">'.$tabUnidades.'</td>
                <td width="8%">'.$tabDestinos.'</td>
                <td width="3%">'.$tot_dias.'</td>
                <td width="4%">'.$c->telefono.'</td>
                <td width="4%">'.$c->telefono_2.'</td>
                <td width="5%">'.$c->correo.'</td>
                <td width="4%">'.number_format($c->tot_unids,2).'</td>
                <td width="4%">'.number_format($c->monto_anticipo,2).'</td>
                <td width="3%">'.number_format($c->porc_desc,2).'</td>
                <td width="5%">'.$pagado.'</td>
                <td width="5%">'.$c->vendedor.'</td>
                <td width="4%">'.$prioridad.'</td>
                <td width="4%">'.$seguimiento.'</td>
                <td width="6%">'.$c->observaciones.'</td>
            </tr>';
        }
    $html.='</tbody>
		</table>';

$pdf->writeHTML($html, true, false, true, false, '');

$pdf->SetAutoPageBreak(false, 0);
$pdf->setPrintHeader(false);

$pdf->IncludeJS('print(true);');
$pdf->Output('listaContratos.pdf', 'I');
