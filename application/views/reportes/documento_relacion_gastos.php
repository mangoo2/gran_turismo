	<?php

	require_once('TCPDF4/tcpdf.php');
	//$ruta_base = $_SERVER['DOCUMENT_ROOT'];
	$this->load->helper('url');

	$englishDays = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
	$spanishDays = array('Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo');
	$englishMonths = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
	$spanishMonths = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
	$spanishMonthsMin = array('Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');


	$GLOBALS['header'] = FCPATH . "public/img/relacion_gastos/header.png";
	$empresa = $contratos->empresa;
	$GLOBALS['id_cont'] = $id_cont;
	$GLOBALS['folio_cont'] = $contratos->folio;
	$GLOBALS['folio_coti'] = $folio_coti;
	$GLOBALS['folio_oc'] = '';
	$GLOBALS['usuario'] = $usuario->nombre . ' ' . $usuario->apellido_p . ' ' . $usuario->apellido_m;
	$GLOBALS['fecha_hora_imp'] = $fecha_hora_imp;

	$border_caseta = FCPATH . "public/img/relacion_gastos/borde_caseta.png";
	$border_combustible = FCPATH . "public/img/relacion_gastos/borde_combustible.png";
	$border_otros = FCPATH . "public/img/relacion_gastos/borde_otros.png";
	$border_sueldos = FCPATH . "public/img/relacion_gastos/borde_sueldos.png";
	$border_anticipos = FCPATH . "public/img/relacion_gastos/borde_anticipos.png";

	$empresa = $contratos->empresa;

	$cliente = $clientes->nombre . " " . $clientes->app . " " . $clientes->apm;
	$GLOBALS['cliente'] = $cliente;
	$telefono = $clientes->telefono;


	$notas = '<br><br><br><br>';
	if (isset($obs->observaciones) && $notas != '') {
		$notas = $obs->observaciones;
	}

	$cant_recibida = '0';
	if (isset($obs->cant_recibida)) {
		$cant_recibida = $obs->cant_recibida;
	}
	
	$fechaCantRecibida = '';
	if(isset($obs->reg)){

		$fechaCR = new DateTime($obs->reg);
		$mesCR = str_replace($englishMonths, $spanishMonthsMin, $fechaCR->format("F"));
		$fechaCantRecibida = $fechaCR->format("j") . '/' . $mesCR . '/' . $fechaCR->format("Y");
	}


	$porc_anticipo = 0;
	if (isset($contratos->porc_anticipo)) {
		$porc_anticipo = $contratos->porc_anticipo;
	}

	$monto_anticipo = 0;
	if (isset($contratos->monto_anticipo)) {
		$monto_anticipo = $contratos->monto_anticipo;
	}

	$monto_descuento = 0;
	if (isset($contratos->porc_desc)) {
		$monto_descuento = $contratos->porc_desc;
	}

	$porCobrar = $montoTotal - ($totalPagos + $monto_descuento + $monto_anticipo);

	$fechaCC = new DateTime(date('Y-m-d'));
	$mesCC = str_replace($englishMonths, $spanishMonthsMin, $fechaCC->format("F"));
	$fechaCobrarCliente = $fechaCC->format("j") . '/' . $mesCC . '/' . $fechaCC->format("Y");


	$nombrePersonal = '';
	if (isset($personal)) {
		$nombrePersonal = $personal->nombre . ' ' . $personal->apellido_p . ' ' . $personal->apellido_m;
	}

	$operadores = '';

	$total_gastos = 0;
	$total_caseta = 0;
	$total_combustible = 0;
	$total_otro = 0;
	$total_sueldo = 0;

	//Registro
	$reg = substr($contratos->fecha_reg, 0, 10);
	$fecha_reg = new DateTime($reg);

	$dia_reg = $fecha_reg->format("j");
	$anio_reg = $fecha_reg->format("Y");
	$mes_reg = $fecha_reg->format("F");
	$mes_reg = str_replace($englishMonths, $spanishMonthsMin, $mes_reg);


	//=======================================================================================
	class MYPDF extends TCPDF
	{
		//Page header
		public function Header()
		{
			$this->Image($GLOBALS['header'], 8, 3, $this->getPageWidth() - 16, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
			$this->SetY(28);
			$this->SetX(-70); //-50 -> 0000
			$this->SetFont('times', 'B', 14);
			$this->SetTextColor(192, 0, 0);
			$this->Cell(0, 5, $GLOBALS['folio_cont'], 0, false, 'C', 0, '', 0, false, 'M', 'M');

			$this->SetY(13.8);
			$this->SetX(-70); //-50 -> 0000
			$this->SetFont('helvetica', 'B', 14);
			$this->SetTextColor(40, 40, 248);
			$this->Cell(0, 5, $GLOBALS['folio_oc'], 0, false, 'C', 0, '', 0, false, 'M', 'M');
		}

		// Page footer
		public function Footer()
		{
			$html = '<p style="text-align:rigth; font-size:8.5px"> IMPRESO POR: ' . $GLOBALS['usuario'] . ' &nbsp;&nbsp;&nbsp; FECHA Y HORA: ' . $GLOBALS['fecha_hora_imp'] . '</p>';
			$this->writeHTML($html, true, false, true, false, '');
		}
	}
	$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Mangoo');
	$pdf->SetTitle('Relacion de gastos');
	$pdf->SetSubject('Relacion de gastos');
	$pdf->SetKeywords('Relacion de gastos');

	// set default header data
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

	// set header and footer fonts
	$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins('10', '34', '10');
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
	$pdf->SetFooterMargin('8');
	// set auto page breaks
	$pdf->SetAutoPageBreak(true, 12);
	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	$pdf->SetFont('dejavusans', '', 13);

	$height = $pdf->getPageHeight();
	$width = $pdf->getPageWidth();

	// add a page   style="width: 300px; height: 200px;"
	$pdf->AddPage('P', 'A4');

	$html = '
				<style type="text/css">
					.border_f{ border:solid 5px black; }
					.p_space{ font-size: 1px;}
					.title_borde{width: 20px; height: 50px;}
				</style>
				';

	$html .= '
				<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
					<thead>
						<tr>
							<td style="color:#ffffff; font-size: 12px; text-align: center; background-color:#558ed5;">
								<b>DATOS DEL OPERADOR ASIGNADO</b>
							</td>
						</tr>
					</thead>
					<tbody>';

	if (count($choferAsig) > 0) {
		foreach ($choferAsig as $key => $chofer) {
			if ($key == 0) {
				$montoEntregado = number_format($cant_recibida, 2, '.', ',');
			}

			if (count($choferAsig) <= ($key+1)) {
				$operadores .= $chofer->nombre.' '.$chofer->apellido_p.' '.$chofer->apellido_m;
			}else{
				$operadores .= $chofer->nombre.' '.$chofer->apellido_p.' '.$chofer->apellido_m.'<br>';
			}

			$html .= '
						<tr>
							<td width="19%">
								&nbsp;OPERADOR ASIGNADO:
							</td>
							<td width="80%" style="border-bottom:solid 1px black;">
								' . $chofer->nombre . ' ' . $chofer->apellido_p . ' ' . $chofer->apellido_m . '
							</td>
							<td width="1%" ></td>
						</tr>

						<tr>
							<td width="100%"><p class="p_space">&nbsp;</p></td>
						</tr>';
		}
	}

	if (count($unidadAsig) > 0) {
		$idU = $unidadAsig->id;

		$choferIsset = array_filter($choferAsig, function ($chofer) use ($idU) {
			return $chofer->idUnidPros == $idU;
		});

		if (empty($choferIsset)) {
			$html .= '
						<tr>
							<td width="19%">
								&nbsp;OPERADOR ASIGNADO:
							</td>
							<td width="80%" style="border-bottom:solid 1px black;">
								--- SIN ASIGNACIÓN ---
							</td>
							<td width="1%" ></td>
						</tr>

						<tr>
							<td width="100%"><p class="p_space">&nbsp;</p></td>
						</tr>';
		}

		$html .= '
						<tr>
							<td width="24%"></td>
							<td width="20%" style="font-size:9px; background-color:#c5d9f1; text-align: rigth;">
								IMPORTE ENTREGADO PARA
							</td>

							<td width="34%"></td>

							<td width="7%" style="font-size:8px;">
								FIRMA:
							</td>
							<td width="14%"></td>
							<td width="1%" ></td>
						</tr>

						<tr>
							<td width="1%"></td>
							<td width="8%">NO. ECO.</td>
							<td width="14%" style="border-bottom:solid 1px black;">
								' . $unidadAsig->num_eco . '
							</td>
							<td width="1%"></td>
							<td width="20%" style="font-size:9px; background-color:#c5d9f1; text-align: rigth;">
								GASTOS A CONDUCTOR:
							</td>
							<td width="2%" style="border-bottom:solid 1px black; font-size:10px">
								<b>$</b>
							</td>
							<td width="31%" style="border-bottom:solid 1px black;">
								' .number_format($cant_recibida, 2, '.', ','). '
							</td>
							<td width="1%" ></td>
							<td width="21%" style="border-bottom:solid 1px black;"></td>
							<td width="1%" ></td>
						</tr>
						<tr>
							<td width="100%" style="border-bottom:solid 1px black;"><p class="p_space">&nbsp;</p></td>
						</tr>';

	} else {
		$html .= '
					<tr>
						<td width="19%">
							&nbsp;OPERADOR ASIGNADO:
						</td>
						<td width="80%" style="border-bottom:solid 1px black;">
							--- SIN ASIGNACIÓN ---
						</td>
						<td width="1%" ></td>
					</tr>

					<tr>
						<td width="100%"><p class="p_space">&nbsp;</p></td>
					</tr>';

					$html .= '
						<tr>
							<td width="24%"></td>
							<td width="20%" style="font-size:9px; background-color:#c5d9f1; text-align: rigth;">
								IMPORTE ENTREGADO PARA
							</td>

							<td width="34%"></td>

							<td width="7%" style="font-size:8px;">
								FIRMA:
							</td>
							<td width="14%"></td>
							<td width="1%" ></td>
						</tr>

						<tr>
							<td width="1%"></td>
							<td width="8%">NO. ECO.</td>
							<td width="14%" style="border-bottom:solid 1px black;"></td>
							<td width="1%"></td>
							<td width="20%" style="font-size:9px; background-color:#c5d9f1; text-align: rigth;">
								GASTOS A CONDUCTOR:
							</td>
							<td width="2%" style="border-bottom:solid 1px black; font-size:10px">
								<b>$</b>
							</td>
							<td width="31%" style="border-bottom:solid 1px black;">
								---
							</td>
							<td width="1%" ></td>
							<td width="21%" style="border-bottom:solid 1px black;"></td>
							<td width="1%" ></td>
						</tr>
						<tr>
							<td width="100%" style="border-bottom:solid 1px black;"><p class="p_space">&nbsp;</p></td>
						</tr>';
	}


	$html .= '</tbody>
				</table>';


	$html .= '<br><br>
					<table width="100%" border="1" style="padding: 0.5px; font-size: 10px; text-align: justify;">
						<tbody>
							<tr>
								<td width="4%" style="background-color:#558ed5; text-align: center;">
									<img class="title_borde" src="' . $border_caseta . '">
								</td>
								<td width="96%">
								
									<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
										<thead>
											<tr>
												<td width="15%" style="text-align: center; background-color:#c5d9f1; border:solid 1px black;">
													FECHA
												</td>
												<td width="70%" style="text-align: center; background-color:#c5d9f1; border:solid 1px black;">
													DESCRIPCIÓN
												</td>
												<td width="15%" colspan="2" style="text-align: center; background-color:#c5d9f1; border:solid 1px black;">
													IMPORTE
												</td>
											</tr>
										</thead>
										<tbody>';

	if (count($casetas->result()) > 0) {
		foreach ($casetas->result() as $caseta) {
			$total_caseta = $total_caseta + $caseta->importe;

			$fecha_caseta = new DateTime($caseta->fecha);
			$dia_caseta = $fecha_caseta->format("j");
			$anio_caseta = $fecha_caseta->format("Y");
			$mes_caseta = $fecha_caseta->format("F");
			$mes_caseta = str_replace($englishMonths, $spanishMonthsMin, $mes_caseta);

			$html .= '<tr>
												<td width="15%" style="text-align: center; border:solid 1px black;">
													' . $dia_caseta . '/' . $mes_caseta . '/' . $anio_caseta . '
												</td>
												<td width="70%" style="text-align: left; border:solid 1px black;">
													' . $caseta->descripcion . '
												</td>
												<td width="3%" style="text-align: center; border-bottom:solid 1px black;">
													$
												</td>
												<td width="12%" style="text-align: right; border-bottom:solid 1px black;">
													' . number_format($caseta->importe, 2, '.', ',') . '
												</td>
											</tr>';
		}
		$total_gastos = $total_gastos + $total_caseta;
	}

	$html .= '<tr style="font-size: 12px;">
												<td width="52%">
												</td>
												<td width="33%" style="text-align: center;  background-color:#8db4e2; border:solid 1px black;">
													<b>TOTAL DE CASETAS</b>
												</td>
												<td width="3%" style="text-align: center; border-bottom:solid 1px black;">
													$
												</td>
												<td width="12%" style="text-align: right; border-bottom:solid 1px black;">
													' . number_format($total_caseta, 2, '.', ',') . '
												</td>
											</tr>
										</tbody>
									</table>

								</td>
							</tr>
						</tbody>
					</table>';



	$html .= '<br><br>
					<table width="100%" border="1" style="padding: 0.5px; font-size: 10px; text-align: justify;">
						<tbody>
							<tr>
								<td width="4%" style="background-color:#558ed5; text-align:center;">
									<img class="title_borde" src="' . $border_combustible . '">
								</td>
								<td width="96%">
								
									<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
										<thead>
											<tr>
												<td width="15%" style="text-align: center; background-color:#c5d9f1; border:solid 1px black;">
													FECHA
												</td>
												<td width="70%" style="text-align: center; background-color:#c5d9f1; border:solid 1px black;">
													DESCRIPCIÓN
												</td>
												<td width="15%" colspan="2" style="text-align: center; background-color:#c5d9f1; border:solid 1px black;">
													IMPORTE
												</td>
											</tr>
										</thead>
										<tbody>';

	if (count($combustibles->result()) > 0) {
		foreach ($combustibles->result() as $combustible) {
			$total_combustible = $total_combustible + $combustible->importe;

			$fecha_combustible = new DateTime($combustible->fecha);
			$dia_combustible = $fecha_combustible->format("j");
			$anio_combustible = $fecha_combustible->format("Y");
			$mes_combustible = $fecha_combustible->format("F");
			$mes_combustible = str_replace($englishMonths, $spanishMonthsMin, $mes_combustible);

			$html .= '<tr>
												<td width="15%" style="text-align: center; border:solid 1px black;">
													' . $dia_combustible . '/' . $mes_combustible . '/' . $anio_combustible . '
												</td>
												<td width="70%" style="text-align: left; border:solid 1px black;">
													' . $combustible->descripcion . '
												</td>
												<td width="3%" style="text-align: center; border-bottom:solid 1px black;">
													$
												</td>
												<td width="12%" style="text-align: right; border-bottom:solid 1px black;">
													' . number_format($combustible->importe, 2, '.', ',') . '
												</td>
											</tr>';
		}
		$total_gastos = $total_gastos + $total_combustible;
	}

	$html .= '<tr style="font-size: 12px;">
												<td width="52%">
												</td>
												<td width="33%" style="text-align: center;  background-color:#8db4e2; border:solid 1px black;">
													<b>TOTAL DE COMBUSTIBLE</b>
												</td>
												<td width="3%" style="text-align: center; border-bottom:solid 1px black;">
													$
												</td>
												<td width="12%" style="text-align: right; border-bottom:solid 1px black;">
													' . number_format($total_combustible, 2, '.', ',') . '
												</td>
											</tr>

										</tbody>
									</table>

								</td>
							</tr>
						</tbody>
					</table>';




	$html .= '<br><br>
					<table width="100%" border="1" style="padding: 0.5px; font-size: 10px; text-align: justify;">
						<tbody>
							<tr>
								<td width="4%" style="background-color:#558ed5; text-align:center;">
									<img class="title_borde" src="' . $border_otros . '">
								</td>
								<td width="96%">
								
									<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
										<thead>
											<tr>
												<td width="15%" style="text-align: center; background-color:#c5d9f1; border:solid 1px black;">
													FECHA
												</td>
												<td width="70%" style="text-align: center; background-color:#c5d9f1; border:solid 1px black;">
													DESCRIPCIÓN
												</td>
												<td width="15%" colspan="2" style="text-align: center; background-color:#c5d9f1; border:solid 1px black;">
													IMPORTE
												</td>
											</tr>
										</thead>
										<tbody>';

	if (count($otros->result()) > 0) {
		foreach ($otros->result() as $otro) {
			$total_otro = $total_otro + $otro->importe;

			$fecha_otro = new DateTime($otro->fecha);
			$dia_otro = $fecha_otro->format("j");
			$anio_otro = $fecha_otro->format("Y");
			$mes_otro = $fecha_otro->format("F");
			$mes_otro = str_replace($englishMonths, $spanishMonthsMin, $mes_otro);

			$html .= '<tr>
												<td width="15%" style="text-align: center; border:solid 1px black;">
													' . $dia_otro . '/' . $mes_otro . '/' . $anio_otro . '
												</td>
												<td width="70%" style="text-align: left; border:solid 1px black;">
													' . $otro->descripcion . '
												</td>
												<td width="3%" style="text-align: center; border-bottom:solid 1px black;">
													$
												</td>
												<td width="12%" style="text-align: right; border-bottom:solid 1px black;">
													' . number_format($otro->importe, 2, '.', ',') . '
												</td>
											</tr>';
		}
		$total_gastos = $total_gastos + $total_otro;
	}

	$html .= '<tr style="font-size: 12px;">
												<td width="52%">
												</td>
												<td width="33%" style="text-align: center; background-color:#8db4e2; border:solid 1px black;">
													<b>TOTAL DE OTROS</b>
												</td>
												<td width="3%" style="text-align: center; border-bottom:solid 1px black;">
													$
												</td>
												<td width="12%" style="text-align: right; border-bottom:solid 1px black;">
													' . number_format($total_otro, 2, '.', ',') . '
												</td>
											</tr>

										</tbody>
									</table>

								</td>
							</tr>
						</tbody>
					</table>';





	$html .= '<br><br>
					<table width="100%" border="1" style="padding: 0.5px; font-size: 10px; text-align: justify;">
						<tbody>
							<tr>
								<td width="4%" style="background-color:#558ed5; text-align:center;">
									<img class="title_borde" src="' . $border_sueldos . '">
								</td>
								<td width="96%">
								
									<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
										<thead>
											<tr>
												<td width="15%" style="text-align: center; background-color:#c5d9f1; border:solid 1px black;">
													FECHA
												</td>
												<td width="70%" style="text-align: center; background-color:#c5d9f1; border:solid 1px black;">
													DESCRIPCIÓN
												</td>
												<td width="15%" colspan="2" style="text-align: center; background-color:#c5d9f1; border:solid 1px black;">
													IMPORTE
												</td>
											</tr>
										</thead>
										<tbody>';

	if (count($sueldos->result()) > 0) {
		foreach ($sueldos->result() as $sueldo) {
			$total_sueldo = $total_sueldo + $sueldo->importe;

			$fecha_sueldo = new DateTime($sueldo->fecha);
			$dia_sueldo = $fecha_sueldo->format("j");
			$anio_sueldo = $fecha_sueldo->format("Y");
			$mes_sueldo = $fecha_sueldo->format("F");
			$mes_sueldo = str_replace($englishMonths, $spanishMonthsMin, $mes_sueldo);

			$html .= '<tr>
												<td width="15%" style="text-align: center; border:solid 1px black;">
													' . $dia_sueldo . '/' . $mes_sueldo . '/' . $anio_sueldo . '
												</td>
												<td width="70%" style="text-align: left; border:solid 1px black;">
													' . $sueldo->descripcion . '
												</td>
												<td width="3%" style="text-align: center; border-bottom:solid 1px black;">
													$
												</td>
												<td width="12%" style="text-align: right; border-bottom:solid 1px black;">
													' . number_format($sueldo->importe, 2, '.', ',') . '
												</td>
											</tr>';
		}
		$total_gastos = $total_gastos + $total_sueldo;
	}

	$html .= '<tr style="font-size: 12px;">
												<td width="52%">
												</td>
												<td width="33%" style="text-align: center; background-color:#8db4e2; border:solid 1px black;">
													<b>TOTAL DE SUELDOS</b>
												</td>
												<td width="3%" style="text-align: center; border-bottom:solid 1px black;">
													$
												</td>
												<td width="12%" style="text-align: right; border-bottom:solid 1px black;">
													' . number_format($total_sueldo, 2, '.', ',') . '
												</td>
											</tr>

										</tbody>
									</table>

								</td>
							</tr>
						</tbody>
					</table>';


	$html .= '<br><br>
					<table width="100%" border="0" cellspacing="2" style="padding: 0.5px; font-size: 10px; text-align: justify;">
						<tbody>
							<tr>

								<td width="50%">
								
									<table width="100%" border="1" style="padding: 0.5px; font-size: 10px; text-align: justify;">
										<tbody>
											<tr>
												<td width="8%" style="background-color:#558ed5; text-align: center;">
													<img class="title_borde" src="' . $border_anticipos . '">
												</td>
												<td width="92%">

													<table width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
														<thead>
															<tr>
																<td width="30%" class="border_f" style="text-align: center; background-color:#c5d9f1;">
																	FECHA
																</td>
																<td width="40%" class="border_f" style="text-align: center; background-color:#c5d9f1;">
																	DESCRIPCIÓN
																</td>
																<td width="30%" colspan="2" class="border_f" style="text-align: center; background-color:#c5d9f1;">
																	IMPORTES
																</td>
															</tr>
														</thead>
								
														<tbody>
															<tr>
																<td width="30%" class="border_f" style="text-align: center;">
																	'.$fechaCobrarCliente.'
																</td>
																<td width="40%" class="border_f" style="text-align: center;">
																	POR COBRAR A CLIENTE
																</td>
																<td width="5%" style="text-align: center; border-top:solid 1px black;">
																	$
																</td>
																<td width="25%" style="text-align: right; border-top:solid 1px black; border-right:solid 1px black;">
																	' . number_format($porCobrar, 2, '.', ',') . '
																</td>
															</tr>
															<tr>
																<td width="30%" class="border_f" style="text-align: center;">
																	'.$fechaCantRecibida.'
																</td>
																<td width="40%" class="border_f" style="text-align: center;">
																	GASTOS ENTREGADOS EN OFICINA
																</td>
																<td width="5%" style="text-align: center; border-top:solid 1px black;">
																	$
																</td>
																<td width="25%" style="text-align: right; border-top:solid 1px black; border-right:solid 1px black;">
																	' . number_format($cant_recibida, 2, '.', ',') . '
																</td>
															</tr>
															<tr>
																<td width="30%" class="border_f" style="text-align: center;">

																</td>
																<td width="40%" class="border_f" style="text-align: center;">
																	DEPÓSITOS
																</td>
																<td width="5%" style="text-align: center; border-top:solid 1px black;">
																	
																</td>
																<td width="25%" style="text-align: right; border-top:solid 1px black; border-right:solid 1px black;">
																	
																</td>
															</tr>
															<tr>
																<td width="30%" class="border_f" style="text-align: center;">
																	
																</td>
																<td width="40%" class="border_f" style="text-align: center;">
																	OTROS
																</td>
																<td width="5%" style="text-align: center; border-top:solid 1px black;">
																	
																</td>
																<td width="25%" style="text-align: right; border-top:solid 1px black; border-right:solid 1px black;">
																	
																</td>
															</tr>

															<tr>
																<td width="70%" class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#558ed5;">
																	<b>TOTAL DE ANTICIPOS</b>
																</td>

																<td width="5%" style="background-color:#c5d9f1; font-size: 12px; text-align: center; border-bottom:solid 1px black; border-top:solid 1px black;">
																	$
																</td>

																<td width="25%" style="background-color:#c5d9f1; font-size: 12px; text-align: right; border-bottom:solid 1px black; border-top:solid 1px black; border-right:solid 1px black; ">
																	' . number_format(($porCobrar + $cant_recibida), 2, '.', ',') . '
																</td>
															</tr>
														</tbody>
													</table>

												</td>
											</tr>
										</tbody>
									</table>

									<br><br>
									<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align:justify;">
										<tbody>
											<tr>
												<td width="2%"> </td>
												<td width="96%" style="font-size: 9px;">
													<br><br>
													<i>Recibí de <b>Operadora Turística Grand Turismo Express S.A. de C.V.</b> 
														la cantidad manifestada en el Renglón de Sueldo por concepto de pago total 
														y finiquito de todas y cada una de las prestaciones a que tengo derecho 
														derivadas de mi contrato de trabajo y de la ley tales como salario, 
														comisiones, aguinaldos, vacaciones, prima vacacional, prima de antigüedad, 
														por lo que no tengo acción pendiente de ejercitar en su contra.
													</i>
													<br>
												</td>
												<td width="2%"> </td>
											</tr>
										</tbody>
									</table>

								</td>

								<td width="50%">

									<table width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
										<tbody>
											<tr>
												<td width="70%" class="border_f" style="color:#ffffff; font-size: 12px; text-align: rigth; background-color:#558ed5;">
													IMPORTE TOTAL RECIBIDO DEL VIAJE
												</td>
												<td width="5%" style="text-align: center; border-top:solid 5px black; background-color:#c5d9f1;">
													$
												</td>
												<td width="25%" style="text-align: right; border-top:solid 5px black; border-right:solid 5px black; background-color:#c5d9f1;">
													' . number_format(($porCobrar + $cant_recibida), 2, '.', ',') . '
												</td>
											</tr>
											<tr>
												<td width="70%" class="border_f" style="color:#ffffff; font-size: 12px; text-align: rigth; background-color:#8eb4e3;">
													IMPORTE TOTAL DE GASTOS
												</td>
												<td width="5%" style="text-align: center; border-top:solid 5px black; background-color:#c5d9f1;">
													$
												</td>
												<td width="25%" style="text-align: right; border-top:solid 5px black; border-right:solid 5px black; background-color:#c5d9f1;">
													' . number_format($total_gastos, 2, '.', ',') . '
												</td>
											</tr>
											<tr>
												<td width="70%" class="border_f" style="color:#ffffff; font-size: 12px; text-align: rigth; background-color:#000099;">
													IMPORTE TOTAL
												</td>
												<td width="5%" style="text-align: center; border-top:solid 5px black; border-bottom:solid 5px black; background-color:#c5d9f1;">
													$
												</td>
												<td width="25%" style="text-align: right; border-top:solid 5px black; border-right:solid 5px black; border-bottom:solid 5px black; background-color:#c5d9f1;">
													' . number_format(($porCobrar + $cant_recibida) - $total_gastos, 2, '.', ',') . '
												</td>
											</tr>

										</tbody>
									</table>

										<br><br>

									<table class="border_f" width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
										<thead>
											<tr>
												<td class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#558ed5;">
													<b>OBSERVACIONES:</b>
												</td>
											</tr>
										</thead>
				
										<tbody>
											<tr>
												<td width="100%"  height="157px">
													<i>' . $notas . '</i>
												</td>
											</tr>
										</tbody>
									</table>

								</td>

							</tr>
						</tbody>
					</table>';

	$html .= '<br>
					<table width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
						<thead>
							<tr>
								<td width="50%" class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#558ed5;">
									<b>NOMBRE DEL OPERADOR</b>
								</td>
								
								<td width="30%" class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#558ed5;">
									<b>FIRMA</b>
								</td>

								<td width="20%" class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#558ed5;">
									<b>FECHA</b>
								</td>

							</tr>
						</thead>

						<tbody>
							<tr>
								<td width="50%" class="border_f" style="text-align: center;">
									' . $operadores . '
								</td>

								<td width="30%" class="border_f" style="text-align: center;">
								</td>

								<td width="20%" class="border_f" style="text-align: center;">
								</td>
							</tr>
						</tbody>
					</table>';


	$html .= '<br><br>
					<table width="100%" border="0" style="padding: 2px; font-size: 10px; text-align: justify;">
						<thead>
							<tr>
								<td width="50%" class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#558ed5;">
									<b>AUTORIZADO POR:</b>
								</td>

								<td width="30%" class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#558ed5;">
									<b>FIRMA</b>
								</td>

								<td width="20%" class="border_f" style="color:#ffffff; font-size: 12px; text-align: center; background-color:#558ed5;">
									<b>FECHA</b>
								</td>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td width="50%" class="border_f" style="text-align: center;">
									' . $nombrePersonal . '
								</td>

								<td width="30%" class="border_f" style="text-align: center;">
									
								</td>

								<td width="20%" class="border_f" style="text-align: center;">
									
								</td>
							</tr>
						</tbody>
					</table>';
					

	$pdf->writeHTML($html, true, false, true, false, '');

	$pdf->IncludeJS('print(true);');
	//$pdf->Output('Orden_servicio.pdf', 'I');
	$pdf->Output(FCPATH . 'public/pdf/Relacion_gastos_' . $id_cont . '.pdf', 'FI');
	//$pdf->Output(FCPATH.'public/pdf/Orden_servicio_'. $id_cont .'.pdf', 'I');