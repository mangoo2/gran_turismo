<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Listado de encuestas</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            
            <div class="mb-3 row">
              <div class="col-md-10">
              </div>
              <div class="col-md-2">
                <button title="Exportar a Excel" id="export-excel" type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i></button>
              </div>
            </div>
            
            <div class="col-12">
              <div class="table-responsive">
                <table class="table" id="table_data">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">¿Cómo fue la atención proporcionada con su <b>ejecutivo de ventas</b>?</th>
                      <th scope="col">¿Cómo fue la atención proporcionada por el operador (chofer) de su viaje?</th>
                      <th scope="col">Por favor califique su nivel de satisfacción con el servicio brindado por el operador durante su servicio</th>
                      <th scope="col">Basado en su experiencia global con <b>Gʀᴀɴᴅ Tᴜʀɪsᴍᴏ Exᴘʀᴇss</b> ¿Qué tanto nos recomendaría con un familiar o amigo?</th>
                      <th scope="col">¿Qué recomendaría para mejorar nuestro servicio o tiene algún comentario o sugerencia para nosotros?</th>
                      <th scope="col">En caso de tener alguna inconformidad con el servicio, favor de mencionarla.</th>
                      <th scope="col">Datos</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!------------------------------------------------>
<div class="modal fade" id="modal_respuesta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Pregunta X</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <!---->
              <div class="card-body">
                <div class="mb-3 row">
                  <label class="col-sm-12 col-form-label" id="quest">Pregunta x:</label>
                  <textarea class="col-sm-12 " id="textareaR" readonly>This text area is non-editable.</textarea>
                </div>
              </div>
              <!---->
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!------------------------------------------------>
<div class="modal fade" id="modal_infoR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel2">Información encuesta</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <!---->
              <div class="card-body">
                <div class="mb-3 row">
                  <input class="form-control" type="hidden" id="idEncuestaR"  value="0" readonly>
                  
                  <label class="col-sm-4 col-form-label">Cliente:</label>
                  <div class="col-sm-8">
                    <input class="form-control" type="text" id="clienteR" readonly>
                  </div>

                  <label class="col-sm-4 col-form-label">Contrato:</label>
                  <div class="col-sm-8">
                    <input class="form-control" type="text" id="contratoR" readonly>
                  </div>

                  <label class="col-sm-4 col-form-label">Fecha de respuesta:</label>
                  <div class="col-sm-8">
                    <input class="form-control" type="text" id="creacionR" readonly>
                  </div>
                  
                </div>
              </div>
              <!---->
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!------------------------------------------------>