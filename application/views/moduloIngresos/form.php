<div class="container-fluid">
	<div class="page-header">
		<div class="row">
			<div class="col-sm-6">
				<h3><?php echo $title_heades; ?></h3>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<form class="form" method="post" role="form" id="form_data"><!---FORM--->
					<!------------------->
					<input type="hidden" name="id" id="idModulo" value="<?php echo isset($idMD) ? $idMD : '0'; ?>">
					<!------------------->
					<div class="card-body">
						<div class="row">
							<div class="col">
								<div class="mb-3 row">
									<label class="col-sm-2 col-form-label">Concepto</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" name="concepto" value="<?php echo isset($idMD) ? $concepto : ''; ?>">
									</div>

									<label class="col-sm-2 col-form-label">Beneficiario</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" name="responsable" value="<?php echo isset($idMD) ? $responsable : ''; ?>">
                    <!--
										<select class="form-control" name="responsable" id="personal">
                      <?php /* foreach ($personal->result() as $p) {
                        if(isset($idMD) && $p->personalId == $responsable)
                          $sel="selected";
                        else
                          $sel="";

                        echo '<option '.$sel.' value="'.$p->personalId.'">'.$p->nombre.' '.$p->apellido_p.' '.$p->apellido_m.'</option>';
                      } */ ?>
                    </select>
										-->
                  </div>
								</div>
								<div class="mb-3 row">
									<label class="col-sm-2 col-form-label">Folio</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" name="folio" value="<?php echo isset($idMD) ? $folio : ''; ?>">
									</div>

									<label class="col-sm-2 col-form-label">Descripción</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" name="descripcion" value="<?php echo isset($idMD) ? $descripcion : ''; ?>">
									</div>
								</div>

								<div class="mb-3 row">
									<label class="col-sm-2 col-form-label">Monto $</label>
									<div class="col-sm-4">
										<input class="form-control" type="number" name="monto" value="<?php echo isset($idMD) ? $monto : ''; ?>">
									</div>

									<label class="col-sm-2 col-form-label">Fecha</label>
									<div class="col-sm-4">
										<input class="form-control" type="date" name="fecha" value="<?php echo isset($idMD) ? $fecha : ''; ?>">
									</div>
								</div>

							</div>
						</div>
					</div>
				</form>
				<div class="card-footer">
					<div class="col-sm-9">
						<button class="btn btn-primary btn_form" type="button" onclick="save_form()"><?php echo $title_save; ?></button>
						<a href="<?php echo base_url() ?>Modulo_ingresos" class="btn btn-light">Regresar</a>
					</div>
				</div>
				<!---->
			</div>
		</div>
	</div>
</div>