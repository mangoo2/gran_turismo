<div class="container-fluid">
	<div class="page-header">
		<div class="row">
			<div class="col-sm-6">
				<h3>Corte de caja chica</h3>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="mb-4 row">
							<div class="col-sm-4">
								<label class="col-sm-6 col-form-label">Desde:</label>
								<input type="date" id="fecha_ini" class="form-control">
							</div>
							<div class="col-sm-4">
								<label class="col-sm-6 col-form-label">Hasta:</label>
								<input type="date" id="fecha_fin" class="form-control">
							</div>
							<div class="col-md-1 pt-2">
								<label class="col-sm-2 col-form-label" style="margin-top: 11px;"></label>
								<button id="search" type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
							</div>
							<div class="col-md-1 pt-2">
								<label class="col-sm-2 col-form-label" style="margin-top: 11px;"></label>
								<button title="Exportar a Excel" id="export_excel" type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i></button>
							</div>
						</div>

						<div class="col-12 mb-3">
							<div class="table-responsive">
								<hr>
								<h4>Listado de movimientos: </h4>
								<table class="table" id="table_data">
									<thead>
										<tr>
											<!--<th scope="col">ID</th>-->
											<th scope="col">#</th>
											<th scope="col">Folio</th>
											<th scope="col">Fecha</th>
											<th scope="col">Concepto</th>
											<th scope="col">Descripción</th>
											<th scope="col">Beneficiario</th>
											<th scope="col">Ingreso</th>
											<th scope="col">Egreso</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>

						<div class="col-6 mb-3">
							<div class="table-responsive">
								<hr>
								<h4>Montos Totales</h4>
								<div id="cont_totales">
									<table class="table" id="table_totales" style="width:100%;">
										<tbody>
											<tr>
												<td scope="col">Total Ingresos:</td>
												<td scope="col" style="text-align: right;"><b>$ ---</b></td>
											</tr>
											<tr>											
												<td scope="col">Total Gastos:</td>
												<td scope="col" style="text-align: right;"><b>$ ---</b></td>
											</tr>
											<tr>
												<td scope="col" style="font-size: 115%">Total Final:</td>
												<td scope="col" style="text-align: right; font-size: 115%"><b>$ ---</b></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>