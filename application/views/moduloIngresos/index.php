<div class="container-fluid">
	<div class="page-header">
		<div class="row">
			<div class="col-sm-6">
				<h3>Módulo de ingresos</h3>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-sm-3">
							<label class="col-sm-6 col-form-label">Desde:</label>
							<input type="date" id="fechaIni" class="form-control">
						</div>
						<div class="col-sm-3">
							<label class="col-sm-6 col-form-label">Hasta:</label>
							<input type="date" id="fechaFin" class="form-control">
						</div>
						<div class="col-sm-2">
							<label class="col-sm-2 col-form-label" style="margin-top: 40px;"></label>
							<button id="search" type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
						</div>
						
						<div class="col-sm-2">
							<label class="col-sm-2 col-form-label" style="margin-top: 15px;"></label>
							<a class="btn btn-primary" href="<?php echo base_url() ?>Modulo_ingresos/registro">Nuevo ingreso</a>
						</div>

						<div class="col-sm-2">
							<label class="col-sm-2 col-form-label" style="margin-top: 15px;"></label>
							<a class="btn btn-primary" href="<?php echo base_url() ?>Modulo_ingresos/corteCajaChica">Corte caja chica</a>
						</div>

						<div class="col-12 mt-4">
							<div class="table-responsive">
								<table class="table" id="table_data">
									<thead>
										<tr>
											<th scope="col">#</th>
											<th scope="col">Folio</th>
											<th scope="col">Concepto de ingreso</th>
											<th scope="col">Monto</th>
											<th scope="col">Fecha</th>
											<th scope="col">Responsable</th>
											<th scope="col">Usuario</th>
											<th scope="col">Acciones</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>