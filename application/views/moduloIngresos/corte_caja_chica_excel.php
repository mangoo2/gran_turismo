<?php
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=corte_caja_chica".date('Ymd Gis').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />


<table border="1" id="tabla-ingreso" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th colspan="8">
                <h4>Listado de movimientos</h4>
            </th>
        </tr>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Folio</th>
            <th scope="col">Fecha</th>
            <th scope="col">Concepto</th>
            <th scope="col">Descripción</th>
            <th scope="col">Beneficiario</th>
            <th scope="col">Ingreso $</th>
            <th scope="col">Egreso $</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($table_movimientos->result() as $count => $t_m) {
            $montoI = '---';
            $montoE = '---';
            if($t_m->tipo == "Ingreso"){
                $montoI = number_format($t_m->monto, 2);
            }

            if($t_m->tipo == "Gasto"){
                $montoE = number_format($t_m->monto, 2);
            }

            echo '<tr>
                <td >' . ($count + 1) . '</td>
                <td >' . $t_m->folio . '</td>
                <td >' . $t_m->fecha . '</td>
                <td >' . $t_m->concepto . '</td>
                <td >' . $t_m->descripcion . '</td>
                <td >' . $t_m->responsable . '</td>
                <td style="color: green;">' . $montoI . '</td>
                <td style="color: red;">-' . $montoE . '</td>
            </tr>';
        }
        ?>

        <?php
        echo '
            <tr></tr>
            <tr>
                <td>Total Ingresos:</td>
                <td style="text-align: right; color: green;"><b>$ ' . number_format($table_totales->totalIngresos, 2) . '</b></td>
            </tr>
            <tr>
                <td>Total Egresos:</td>
                <td style="text-align: right; color: red;"><b>-$ ' . number_format($table_totales->totalEgresos, 2) . '</b></td>
            </tr>
            <tr>
                <td>Total General:</td>
                <td style="text-align: right;"><b>$ ' . number_format($table_totales->totalFinal, 2) . '</b></td>
            </tr>
            ';
        ?>
    </tbody>
</table>