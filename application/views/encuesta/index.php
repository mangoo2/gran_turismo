<style type="text/css">
	.btn-fau {
		color: white;
		background-color: #00319e;
	}

	.btn:hover {
		color: #ffffff;
		text-decoration: none;
	}

	.radio.radio-accent.radio-success>span:after {
		background-color: #ffffff;
	}

	.radio.radio-accent.radio-success>input:checked~span {
		border-color: #ffffff;
	}

	.radio.radio-accent.radio-success>input:checked~span:after {
		background-color: #ffffff;
	}

	.btn_color:hover {
		background: #ff000069;
	}

	.btn_color:hover {
		background: #1f378e !important;
	}

	/* El usuario esta sobre el enlace */

	.btn_color:active {
		background: #1f378e !important;
	}

	.switch.switch-outline.switch-success input:checked~span:before {
		border: 2px solid #ffffff;
		background-color: transparent;
	}

	.switch.switch-outline.switch-success input:checked~span:after {
		color: #00319e;
		background-color: #ffffff;
	}

	.form-control {
		display: block;
		width: 100%;
		height: calc(1.5em + 1.3rem + 2px);
		padding: 0.65rem 1rem;
		font-size: 1rem;
		font-weight: 400;
		line-height: 1.5;
		color: white;
		background-color: #ffffff21;
		background-clip: padding-box;
		border: 1px solid #E4E6EF;
		border-radius: 0.42rem;
		-webkit-box-shadow: none;
		box-shadow: none;
		-webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
		transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
		transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
		transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
	}

	.form-control:focus {
		color: white;
		background-color: #ffffff00;
		border-color: #00319e;
		outline: 0;
	}

	.entrella_label1 {
		color: #00319e;
		font-size: 20px;
	}

	.entrella_label2 {
		color: #00319e;
		font-size: 20px;
	}

	.entrella_label3 {
		color: #00319e;
		font-size: 20px;
	}

	.entrella_label4 {
		color: #00319e;
		font-size: 20px;
	}

	.entrella_label5 {
		color: #00319e;
		font-size: 20px;
		/*border: 2px #000000 solid;*/
		/*background: #d9dbe8;*/
	}
/*
	.text_label {
		color: #ffffff80;
		font-size: 130px;
	}
*/

	.estrella_input[type="radio"] {
		display: none;
	}

	.clasificacion {
		direction: rtl;
		/* right to left */
		unicode-bidi: bidi-override;
		/* bidi de bidireccional */
	}

	.entrella_label1:hover {
		color: #00319e;
		font-size: 24px;
	}

	.entrella_label1:hover~label {
		color: #00319e;
	}

	.entrella_label2:hover {
		color: #00319e;
		font-size: 24px;
	}

	.entrella_label2:hover~label {
		color: #00319e;
	}

	.entrella_label3:hover {
		color: #00319e;
		font-size: 24px;
	}

	.entrella_label3:hover~label {
		color: #00319e;
	}

	.entrella_label4:hover {
		color: #00319e;
		font-size: 24px;
	}

	.entrella_label4:hover~label {
		color: #00319e;
	}

	.entrella_label5:hover {
		color: #00319e;
		font-size: 24px;
		/*border: 4px #000000 solid;*/
		/*background: #f2f2f2;*/
	}

	.entrella_label5:hover~label {
		color: #00319e;
	}

	.estrella_input[type="radio"]:checked~label {
		color: #00319e;
	}

	@media all and (max-width: 680px) {
		.tnm_btn0 {
			width: 80% !important
		}

		.tnm_btn {
			width: 80% !important
		}

		.tnm_btn3 {
			width: 83% !important;
		}

		.btn_iniciar {
			width: 100% !important;
			font-size: 16px !important;
		}

		.img_ryd {
			width: 40% !important;
		}

		.letra_ryd {
			font-size: 38px !important;
		}

		.entrella_label1 {
			color: #ffffff80;
			font-size: 30px !important;
			;
		}

		.entrella_label2 {
			color: #ffffff80;
			font-size: 35px !important;
			;
		}

		.entrella_label3 {
			color: #ffffff80;
			font-size: 40px !important;
			;
		}

		.entrella_label4 {
			color: #ffffff80;
			font-size: 45px !important;
			;
		}

		.entrella_label5 {
			color: #ffffff80;
			font-size: 50px !important;
			;
		}
	}

	/*------------*/


	.background-image {
		height: 100vh;
		width: 100vw;
		background-size: cover;
		background-position: center bottom;
		background-repeat: no-repeat;
	}

	.preg_text{
		height: 150px;
		color: #00319e;
	}

	.preg_text:focus {
      color: #00319e;
    }

</style>

<div class="d-flex background-image flex-row" style="background-image: url(<?php echo base_url(); ?>public/img/encuesta/fondoGT.jpg);">
	<div class="preg_0 tnm_btn0" style="background: #ffffffd9;border-radius: 18px; margin: auto; box-shadow: 0 8px 8px 0 rgb(255 255 255);box-shadow: 16px 21px #1f378e96;">
		<div class="text-center overflow-hidden py-2 px-3">
			<div class="row">
				<input type="hidden" id="id_cliente" value="<?php if(isset($idCli)) echo $idCli; else echo "0"; ?>">
				<input type="hidden" id="id_contrato" value="<?php if(isset($idCon)) echo $idCon; else echo "0"; ?>">
				<div class="col-lg-12">
					<h1 style="color: #00319e;"><b style="font-size: 55px;">Gʀᴀɴᴅ Tᴜʀɪsᴍᴏ Exᴘʀᴇss</b></h1>
					<h1 style="color: black;">Ayúdanos a Mejorar</h1>
					<br>
					<h3 style="color: black;">Nos encantaría escucharte y saber que tan satisfecho estás.</h3>
					<img style="width: 8%;" src="<?php echo base_url() ?>public/img/encuesta/sonrisa.png">
					<h3 style="color: black;">Somos muy afortunados por tu participación.</h3>
					<br><br>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-12" align="center">
					<button style="width: 48%; border: white 5px double; font-size: 18px;" class="btn_iniciar btn btn-fau font-weight-bold btn-pill mr-2" onclick="siguiente()">
						<img style="width: 10%; filter: invert(1) sepia(0) hue-rotate(20deg) saturate(975%);" src="<?php echo base_url() ?>public/img/encuesta/doble_flecha.png"> &nbsp;&nbsp; Comenzar encuesta
					</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Pregunta 1 -->
	<div class="preg_1 " style="display: none; background: #ffffffd9;border-radius: 18px; margin: auto; box-shadow: 0 8px 8px 0 rgb(255 255 255);box-shadow: 16px 21px #1f378e96;">
		<div class="text-center overflow-hidden py-3 px-3">
			<div class="row">
				<div class="col-lg-12">
					<h1 style="color: #00319e;">¿Cómo fue la atención proporcionada con su <b>ejecutivo de ventas</b>?</h1>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-12 col-form-label" align="center">
					<p class="clasificacion ">

						<input class="estrella_input" id="p1_radio1" type="radio" name="pregunta1" value="5" onclick="siguiente()">
						<label class="entrella_label5" for="p1_radio1">
							<img src="<?php echo base_url() ?>public/img/encuesta/mbien.png">
							<br>
							<label>Muy buena</label>
						</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

						<input class="estrella_input" id="p1_radio2" type="radio" name="pregunta1" value="4" onclick="siguiente()">
						<label class="entrella_label4" for="p1_radio2">
							<img src="<?php echo base_url() ?>public/img/encuesta/bien.png">
							<br>
							<label>Buena</label>
						</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

						<input class="estrella_input" id="p1_radio3" type="radio" name="pregunta1" value="3" onclick="siguiente()">
						<label class="entrella_label3" for="p1_radio3">
							<img src="<?php echo base_url() ?>public/img/encuesta/neutro.png">
							<br>
							<label>Neutral</label>
						</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

						<input class="estrella_input" id="p1_radio4" type="radio" name="pregunta1" value="2" onclick="siguiente()">
						<label class="entrella_label2" for="p1_radio4">
							<img src="<?php echo base_url() ?>public/img/encuesta/mal.png">
							<br>
							<label>Mala</label>
						</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						
						<input class="estrella_input" id="p1_radio5" type="radio" name="pregunta1" value="1" onclick="siguiente()">
						<label class="entrella_label1" for="p1_radio5">
							<img src="<?php echo base_url() ?>public/img/encuesta/mmal.png">
							<br>
							<label>Muy mala</label>
						</label>

					</p>
				</div>
			</div>
		</div>
	</div>


	<!-- Pregunta 2 -->
	<div class="preg_2 " style="display: none; background: #ffffffd9;border-radius: 18px; margin: auto; box-shadow: 0 8px 8px 0 rgb(255 255 255);box-shadow: 16px 21px #1f378e96;">
		<div class="text-center overflow-hidden py-3 px-3">
			<div class="row">
				<div class="col-lg-12">
					<h1 style="color: #00319e;">¿Cómo fue la atención proporcionada por el operador (chofer) de su viaje?</h1>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-12 col-form-label" align="center">
					<p class="clasificacion ">

						<input class="estrella_input" id="p2_radio1" type="radio" name="pregunta2" value="5" onclick="siguiente()">
						<label class="entrella_label5" for="p2_radio1">
							<img src="<?php echo base_url() ?>public/img/encuesta/mbien.png">
							<br>
							<label>Muy buena</label>
						</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

						<input class="estrella_input" id="p2_radio2" type="radio" name="pregunta2" value="4" onclick="siguiente()">
						<label class="entrella_label4" for="p2_radio2">
							<img src="<?php echo base_url() ?>public/img/encuesta/bien.png">
							<br>
							<label>Buena</label>
						</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

						<input class="estrella_input" id="p2_radio3" type="radio" name="pregunta2" value="3" onclick="siguiente()">
						<label class="entrella_label3" for="p2_radio3">
							<img src="<?php echo base_url() ?>public/img/encuesta/neutro.png">
							<br>
							<label>Neutral</label>
						</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

						<input class="estrella_input" id="p2_radio4" type="radio" name="pregunta2" value="2" onclick="siguiente()">
						<label class="entrella_label2" for="p2_radio4">
							<img src="<?php echo base_url() ?>public/img/encuesta/mal.png">
							<br>
							<label>Mala</label>
						</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						
						<input class="estrella_input" id="p2_radio5" type="radio" name="pregunta2" value="1" onclick="siguiente()">
						<label class="entrella_label1" for="p2_radio5">
							<img src="<?php echo base_url() ?>public/img/encuesta/mmal.png">
							<br>
							<label>Muy mala</label>
						</label>
						
					</p>
				</div>
			</div>
		</div>
	</div>


	<!-- Pregunta 3 -->
	<div class="preg_3 " style="display: none; background: #ffffffd9;border-radius: 18px; margin: auto; box-shadow: 0 8px 8px 0 rgb(255 255 255);box-shadow: 16px 21px #1f378e96;">
		<div class="text-center overflow-hidden py-3 px-3">
			<div class="row">
				<div class="col-lg-12">
					<h1 style="color: #00319e;">Por favor califique su nivel de satisfacción con el servicio brindado por el operador durante su servicio</h1>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-12 col-form-label" align="center">
					<p class="clasificacion ">

						<input class="estrella_input" id="p3_radio1" type="radio" name="pregunta3" value="5" onclick="siguiente()">
						<label class="entrella_label5" for="p3_radio1">
							<img src="<?php echo base_url() ?>public/img/encuesta/mbien.png">
							<br>
							<label>Muy buena</label>
						</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

						<input class="estrella_input" id="p3_radio2" type="radio" name="pregunta3" value="4" onclick="siguiente()">
						<label class="entrella_label4" for="p3_radio2">
							<img src="<?php echo base_url() ?>public/img/encuesta/bien.png">
							<br>
							<label>Buena</label>
						</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

						<input class="estrella_input" id="p3_radio3" type="radio" name="pregunta3" value="3" onclick="siguiente()">
						<label class="entrella_label3" for="p3_radio3">
							<img src="<?php echo base_url() ?>public/img/encuesta/neutro.png">
							<br>
							<label>Neutral</label>
						</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

						<input class="estrella_input" id="p3_radio4" type="radio" name="pregunta3" value="2" onclick="siguiente()">
						<label class="entrella_label2" for="p3_radio4">
							<img src="<?php echo base_url() ?>public/img/encuesta/mal.png">
							<br>
							<label>Mala</label>
						</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						
						<input class="estrella_input" id="p3_radio5" type="radio" name="pregunta3" value="1" onclick="siguiente()">
						<label class="entrella_label1" for="p3_radio5">
							<img src="<?php echo base_url() ?>public/img/encuesta/mmal.png">
							<br>
							<label>Muy mala</label>
						</label>
						
					</p>
				</div>
			</div>
		</div>
	</div>


	<!-- Pregunta 4 -->
	<div class="preg_4 " style="display: none; background: #ffffffd9;border-radius: 18px; margin: auto; box-shadow: 0 8px 8px 0 rgb(255 255 255);box-shadow: 16px 21px #1f378e96;">
		<div class="text-center overflow-hidden py-3 px-3">
			<div class="row">
				<div class="col-lg-12">
					<h1 style="color: #00319e;">Basado en su experiencia global con <b>Gʀᴀɴᴅ Tᴜʀɪsᴍᴏ Exᴘʀᴇss</b> ¿Qué tanto nos recomendaría con un familiar o amigo?</h1>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-12 col-form-label" align="center">
					<p class="clasificacion ">

						<input class="estrella_input" id="p4_radio1" type="radio" name="pregunta4" value="5" onclick="siguiente()">
						<label class="entrella_label5" for="p4_radio1">
							<img src="<?php echo base_url() ?>public/img/encuesta/mbien.png">
							<br>
							<label>Muy buena</label>
						</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

						<input class="estrella_input" id="p4_radio2" type="radio" name="pregunta4" value="4" onclick="siguiente()">
						<label class="entrella_label4" for="p4_radio2">
							<img src="<?php echo base_url() ?>public/img/encuesta/bien.png">
							<br>
							<label>Buena</label>
						</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

						<input class="estrella_input" id="p4_radio3" type="radio" name="pregunta4" value="3" onclick="siguiente()">
						<label class="entrella_label3" for="p4_radio3">
							<img src="<?php echo base_url() ?>public/img/encuesta/neutro.png">
							<br>
							<label>Neutral</label>
						</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

						<input class="estrella_input" id="p4_radio4" type="radio" name="pregunta4" value="2" onclick="siguiente()">
						<label class="entrella_label2" for="p4_radio4">
							<img src="<?php echo base_url() ?>public/img/encuesta/mal.png">
							<br>
							<label>Mala</label>
						</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						
						<input class="estrella_input" id="p4_radio5" type="radio" name="pregunta4" value="1" onclick="siguiente()">
						<label class="entrella_label1" for="p4_radio5">
							<img src="<?php echo base_url() ?>public/img/encuesta/mmal.png">
							<br>
							<label>Muy mala</label>
						</label>
						
					</p>
				</div>
			</div>
		</div>
	</div>


	<!-- Pregunta 5 -->
	<div class="preg_5 " style="display: none; background: #ffffffd9;border-radius: 18px; margin: auto; box-shadow: 0 8px 8px 0 rgb(255 255 255);box-shadow: 16px 21px #1f378e96;">
		<div class="text-center overflow-hidden py-3 px-3">
			<div class="row">
				<div class="col-lg-12">
					<h1 style="color: #00319e;">¿Qué recomendaría para mejorar nuestro servicio o tiene algún comentario o sugerencia para nosotros?</h1>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-12 col-form-label" align="center">
					<textarea type="text" class="form-control preg_text" rows="4" id="pregunta5" name="pregunta5" placeholder="Escribe aquí..."></textarea>
					<br>
					<button style="width: 48%; border: white 5px double; font-size: 18px;" class="btn_iniciar btn btn-fau font-weight-bold btn-pill mr-2" onclick="siguiente()"> Aceptar</button>
				</div>
			</div>
		</div>
	</div>


		<!-- Pregunta 6 -->
		<div class="preg_6 " style="display: none; background: #ffffffd9;border-radius: 18px; margin: auto; box-shadow: 0 8px 8px 0 rgb(255 255 255);box-shadow: 16px 21px #1f378e96;">
		<div class="text-center overflow-hidden py-3 px-3">
			<div class="row">
				<div class="col-lg-12">
					<h1 style="color: #00319e;">En caso de tener alguna inconformidad con el servicio, favor de mencionarla.</h1>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-12 col-form-label" align="center">
					<textarea type="text" class="form-control preg_text" rows="4" id="pregunta6" name="pregunta6" placeholder="Escribe aquí..."></textarea>
					<br>
					<button style="width: 48%; border: white 5px double; font-size: 18px;" class="btn_iniciar btn btn-fau font-weight-bold btn-pill mr-2" onclick="guardar_registro()"> Aceptar</button>
				</div>
			</div>
		</div>
	</div>


	<!-- Pregunta 7 -->
	<div class="preg_7 " style="display: none; background: #ffffffd9;border-radius: 18px; margin: auto; box-shadow: 0 8px 8px 0 rgb(255 255 255);box-shadow: 16px 21px #1f378e96;">
		<div class="text-center overflow-hidden py-3 px-3">
			<div class="row">
				<div class="col-lg-12">
					<h1 style="color: black;">Gracias por tomarte el tiempo en está pequeña encuesta</h1>
					<img style="width: 12%;" src="<?php echo base_url() ?>public/img/encuesta/guino.png">
					<br>
					<br>
					<h2 style="color: black;">Encuesta finalizada</h2>
					<br>
					<button style="width: 48%; border: white 5px double; font-size: 18px;" class="btn_iniciar btn btn-fau font-weight-bold btn-pill mr-2" onclick="redireccionar()"> Aceptar</button>
				</div>
			</div>
		</div>
	</div>
</div>