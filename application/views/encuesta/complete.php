<style type="text/css">
	.btn-fau {
		color: white;
		background-color: #00319e;
	}

	.btn:hover {
		color: #ffffff;
		text-decoration: none;
	}

	.background-image {
		height: 100vh;
		width: 100vw;
		background-size: cover;
		background-position: center bottom;
		background-repeat: no-repeat;
	}
</style>

<div class="d-flex background-image flex-row" style="background-image: url(<?php echo base_url(); ?>public/img/encuesta/fondoGT.jpg);">
	<div class="preg_0 tnm_btn0" style="background: #ffffffb3;border-radius: 18px; margin: auto; box-shadow: 0 8px 8px 0 rgb(255 255 255);box-shadow: 16px 21px #1f378e96;">
		<div class="text-center overflow-hidden py-3 px-3">
			<div class="row">
				<div class="col-lg-12">
					<h1 style="color: black;">Gracias por tomarte el tiempo en está pequeña encuesta</h1>
					<img style="width: 12%;" src="<?php echo base_url() ?>public/img/encuesta/guino.png">
					<br>
					<br>
					<h2 style="color: black;">Encuesta finalizada</h2>
					<br>
					<button style="width: 48%; border: white 5px double; font-size: 18px;" class="btn_iniciar btn btn-fau font-weight-bold btn-pill mr-2" onclick="redireccionar()"> Aceptar</button>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	function redireccionar() {
		window.location.href = "https://www.grandturismoexpress.com/";
	}
</script>