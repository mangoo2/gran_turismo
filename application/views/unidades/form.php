<style type="text/css">
	.kv-file-upload,.fileinput-remove-button{
    display: none;
  }
  .table_det{
    font-size: 11px !important;
  }
  .table_det input,textarea{
    font-size: 11px !important;
  }
  .table_det select{
    font-size: 11px !important;
  }
</style>
<!-- latest jquery-->
<script src="<?php echo base_url(); ?>assets/js/jquery-3.5.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/fileinput/fileinput.min.css">
<script src="<?php echo base_url(); ?>plugins/fileinput/fileinput.js" type="text/javascript"></script>
<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3><?php echo $tittle; ?> Unidad</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <!---->
        <form class="form" method="post" role="form" id="form_data">
          <input type="hidden" name="id" id="idcliente" value="<?php if(isset($u)) echo $u->id; else echo "0"; ?>">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Número económico</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" name="num_eco" value="<?php if(isset($u)) echo $u->num_eco; ?>">
                  </div>
                  <label class="col-sm-2 col-form-label">Tipo Permiso</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="tipo_perm" id="tipo_perm">
                      <option <?php if(isset($u) && $u->tipo_perm=="1") echo "selected"; ?> value="1">Federal</option>
                      <option <?php if(isset($u) && $u->tipo_perm=="2") echo "selected"; ?> value="2">Federal/Particular</option>
                      <option <?php if(isset($u) && $u->tipo_perm=="3") echo "selected"; ?> value="3">Particular</option>
                      <option <?php if(isset($u) && $u->tipo_perm=="4") echo "selected"; ?> value="4">Federal/Paquetería</option>
                    </select>
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Vehículo</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" name="vehiculo" value="<?php if(isset($u)) echo $u->vehiculo; ?>">
                  </div>
                  <label class="col-sm-2 col-form-label">Placas Particulares</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" name="placas" value="<?php if(isset($u)) echo $u->placas; ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Placas Federales</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" name="placas_fed" value="<?php if(isset($u)) echo $u->placas_fed; ?>">
                  </div>
                  <label class="col-sm-2 col-form-label">Serie</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" name="serie" value="<?php if(isset($u)) echo $u->serie; ?>">
                  </div>
                </div>

                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Motor</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" name="motor" value="<?php if(isset($u)) echo $u->motor; ?>">
                  </div>
                  <label class="col-sm-2 col-form-label">Marca</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" name="marca" value="<?php if(isset($u)) echo $u->marca; ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Modelo</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" name="modelo" value="<?php if(isset($u)) echo $u->modelo; ?>">
                  </div>
                  <label class="col-sm-2 col-form-label">Tipo</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="tipo" id="tipo">
                      <option <?php if(isset($u) && $u->tipo=="1") echo "selected"; ?> value="1">Auto (4 pasajeros)</option>
                      <option <?php if(isset($u) && $u->tipo=="2") echo "selected"; ?> value="2">Minivan (6 pasajeros)</option>
                      <option <?php if(isset($u) && $u->tipo=="3") echo "selected"; ?> value="3">Van (10 pasajeros)</option>
                      <option <?php if(isset($u) && $u->tipo=="4") echo "selected"; ?> value="4">Van (14 pasajeros)</option>
                      <option <?php if(isset($u) && $u->tipo=="5") echo "selected"; ?> value="5">Van (20 pasajeros)</option>
                      <option <?php if(isset($u) && $u->tipo=="6") echo "selected"; ?> value="6">Midibus (33 pasajeros)</option>
                      <option <?php if(isset($u) && $u->tipo=="7") echo "selected"; ?> value="7">Autobus (47 pasajeros)</option>
                      <option <?php if(isset($u) && $u->tipo=="8") echo "selected"; ?> value="8">Autobus (50 pasajeros)</option>
                    </select>
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Póliza de seguro</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" name="poliza_seg" value="<?php if(isset($u)) echo $u->poliza_seg; ?>">
                  </div>
                  <label class="col-sm-2 col-form-label">Vencimiento de póliza</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="date" name="vencimiento_poliza" value="<?php if(isset($u)) echo $u->vencimiento_poliza; ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Próxima verificación</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="date" name="prox_ver" value="<?php if(isset($u)) echo $u->prox_ver; ?>">
                  </div>
                  <label class="col-sm-2 col-form-label">Control Vehicular</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="date" name="ctrl_vehicular" value="<?php if(isset($u)) echo $u->ctrl_vehicular; ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Kilometraje actual</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="number" name="km_actual" value="<?php if(isset($u)) echo $u->km_actual; ?>">
                  </div>

                  <label class="col-sm-2 col-form-label">Rendimiento por litro</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="number" name="rendimiento" value="<?php if(isset($u)) echo $u->rendimiento; ?>">
                  </div>
                </div>

                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Color</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="color" name="color" value="<?php if(isset($u)) echo $u->color; ?>">
                  </div>
                </div>

                <div class="mb-3 row ">
                  <div class="col-md-4 mx-auto">
                    <label class="col-sm-12 col-form-label">Poliza de seguro (PDF)</label>
                    <input class="form-control" type="hidden" id="img_poliza_aux" value="<?php if(isset($u)) echo $u->img_poliza; ?>">
                    <input class="form-control" type="file" name="img_poliza" id="img_poliza">
                  </div>
                  <div class="col-md-4 mx-auto">
                    <label class="col-sm-12 col-form-label">Imagen de la unidad</label>
                    <input class="form-control" type="hidden" id="img_aux" value="<?php if(isset($u)) echo $u->img; ?>">
                    <input class="form-control" type="file" name="img" id="img">
                  </div>
                </div>

                <!-------------------------------------------------------------------------->
                <div class="col-md-12 mt-5">
                  <h4 class="form-section">
                    Servicios 
                    <a class="btn btn-primary" onclick="export_excel(<?php if(isset($u)) echo $u->id; else echo '0'; ?>,1)"><i class="fa fa-file-excel-o" style="font-size: 20px;"></i> Exportar</a>
                  </h4> 
                  <hr>
                  <table class="table_det" id="table_servs" width="100%">
                    <tbody>
                      <tr id="tr_servs">
                        <td width="15%">
                          <input type="hidden" id="id" value="0">
                          <div class="form-group">
                            <label class="col-form-label">Fecha de Servicio</label>
                            <div class="controls">
                              <input type="date" id="fecha" class="form-control form-control-smr" value="">
                            </div>
                          </div>
                        </td>
                        <td width="15%">
                          <div class="form-group">
                            <label class="col-form-label">Kilometraje</label>
                            <div class="controls">
                              <input type="text" id="kilometraje" class="form-control form-control-sm" value="">
                            </div>
                          </div>
                        </td>
                        <td width="15%">
                          <div class="form-group">
                            <label class="col-form-label">Importe</label>
                            <div class="controls">
                              <input type="number" id="importe" class="form-control form-control-sm" value="">
                            </div>
                          </div>
                        </td>
                        <td width="15%">
                          <div class="form-group">
                            <label class="col-form-label">Tipo</label>
                            <div class="controls">
                              <select class="form-control" id="tipo">
                                <option value="1">Mant. Preventivo </option>
                                <option value="2">Mant. Correctivo </option>
                              </select>
                            </div>
                          </div>
                        </td>
                        <td width="35%">
                          <div class="form-group">
                            <label class="col-form-label">Comentarios</label>
                            <div class="controls">
                              <textarea type="text" rows="2" id="comentarios" class="form-control form-control-sm" value=""></textarea>
                            </div>
                          </div>
                        </td>
                        <td width="5%">
                          <span style="color: transparent;">add</span>
                          <button type="button" class="btn btn-primary" title="Agregar nuevo servicio" onclick="addServ()"><i id="btn_plus" class="fa fa-plus" aria-hidden="true"></i></button>
                        </td>
                      </tr>
                      <?php if (isset($serv)) {
                        foreach ($serv->result() as $d) { ?>
                        <tr id="<?php echo 'tr_each_'.$d->id ?>">
                        <td width="15%">
                          <input type="hidden" id="id" value="<?php echo $d->id; ?>">
                          <div class="form-group">
                            <label class="col-form-label">Fecha de Servicio</label>
                            <div class="controls">
                              <input type="date" id="fecha" class="form-control form-control-smr" value="<?php echo $d->fecha; ?>">
                            </div>
                          </div>
                        </td>
                        <td width="15%">
                          <div class="form-group">
                            <label class="col-form-label">Kilometraje</label>
                            <div class="controls">
                              <input type="text" id="kilometraje" class="form-control form-control-sm" value="<?php echo $d->kilometraje; ?>">
                            </div>
                          </div>
                        </td>
                        <td width="15%">
                          <div class="form-group">
                            <label class="col-form-label">Importe</label>
                            <div class="controls">
                              <input type="number" id="importe" class="form-control form-control-sm" value="<?php echo $d->importe; ?>">
                            </div>
                          </div>
                        </td>
                        <td width="15%">
                          <div class="form-group">
                            <label class="col-form-label">Tipo</label>
                            <div class="controls">
                              <select class="form-control" id="tipo">
                                <option <?php if($d->tipo==1) echo "selected"; ?> value="1">Mant. Preventivo </option>
                                <option <?php if($d->tipo==2) echo "selected"; ?> value="2">Mant. Correctivo </option>
                              </select>
                            </div>
                          </div>
                        </td>
                        <td width="35%">
                          <div class="form-group">
                            <label class="col-form-label">Comentarios</label>
                            <div class="controls">
                              <textarea type="time" id="comentarios" class="form-control form-control-sm" value="<?php echo $d->comentarios; ?>"><?php echo $d->comentarios; ?></textarea>
                            </div>
                          </div>
                        </td>
                        <td width="5%">
                          <span class="required" style="color: transparent;">dele</span>
                          <a class="btn btn-danger" title="Eliminar contacto" onclick="eliminarServ(<?php echo $d->id; ?>)"><i class="fa fa-minus" aria-hidden="true"></i></a>
                        </td>
                      </tr>
                      <?php }
                    } ?>
                    </tbody>
                  </table>
                </div>
                <!-------------------------------------------------------------------------->
                <div class="col-md-12 mt-5">
                  <h4 class="form-section">
                    Verificación 
                    <a class="btn btn-primary" onclick="export_excel(<?php if(isset($u)) echo $u->id; else echo '0'; ?>,2)"><i class="fa fa-file-excel-o" style="font-size: 20px;"></i> Exportar</a>
                  </h4> 
                  <hr>
                  <table class="table_det" id="table_verif" width="100%">
                    <tbody id="body_verif">
                      <tr id="tr_verif">
                        <td width="15%">
                          <!--<div class="col-md-4 mx-auto">
                            <label class="col-sm-12 col-form-label">Imagen</label>
                            <input class="form-control" type="file" name="img" id="img">
                          </div>-->
                        </td>
                        <td width="15%">
                          <input type="hidden" id="id" value="0">
                          <div class="form-group">
                            <label class="col-form-label">Fecha de Verificación</label>
                            <div class="controls">
                              <input type="date" id="fecha" class="form-control form-control-smr" value="">
                            </div>
                          </div>
                        </td>
                        <td width="15%">
                          <div class="form-group">
                            <label class="col-form-label">Tipo</label>
                            <div class="controls">
                              <select class="form-control" id="tipo">
                                <!--option disabled selected value="0">Selecciona una opción</option-->
                                <option value="1">Físico mecánica </option>
                                <option value="2">Contaminantes </option>
                              </select>
                            </div>
                          </div>
                        </td>
                        <td width="15%">
                          <div class="form-group">
                            <label class="col-form-label">Importe</label>
                            <div class="controls">
                              <input type="number" id="importe" class="form-control form-control-sm" value="">
                            </div>
                          </div>
                        </td>
                        <td width="35%">
                          <div class="form-group">
                            <label class="col-form-label">Observaciones</label>
                            <div class="controls">
                              <textarea type="text" rows="2" id="observaciones" class="form-control form-control-sm" value=""></textarea>
                            </div>
                          </div>
                        </td>
                        <td width="5%">
                          <span style="color: transparent;">add</span>
                          <button type="button" class="btn btn-primary" title="Agregar nuevo servicio" onclick="addVerif()"><i id="btn_plus" class="fa fa-plus" aria-hidden="true"></i></button>
                        </td>
                      </tr>
                      <?php /*if (isset($verif)) {
                        foreach ($verif->result() as $v) { 
                          imgVerificacion($v->id,"'".$v->img_verif."'");  */ ?>
                          <!--<script type="text/javascript"> imgVerificacion(<?php //echo $v->id; ?>,"<?php //echo $v->img_verif; ?>"); </script>-->
                          <!--<tr id="<?php //echo 'tr_each_verif_'.$v->id ?>">
                            <td width="15%">
                              <div class="col-md-4 mx-auto">
                                <label class="col-sm-12 col-form-label">Imagen</label>
                                <input width="50%" onchange="cargarDoc(<?php //echo $v->id; ?>,this.value,this.id,this.name,this.files[0].size)" class="doc<?php //echo '_'.$v->id; ?>" id="img_verif_<?php //echo $v->id; ?>" name="<?php //echo $v->id; ?>" type="file">
                              </div>
                            </td>
                            <td width="15%">
                              <input type="hidden" id="id" value="<?php //echo $v->id; ?>">
                              <div class="form-group">
                                <label class="col-form-label">Fecha de Verificación</label>
                                <div class="controls">
                                  <input type="date" id="fecha" class="form-control form-control-smr" value="<?php //echo $v->fecha; ?>">
                                </div>
                              </div>
                            </td>
                            <td width="15%">
                              <div class="form-group">
                                <label class="col-form-label">Tipo</label>
                                <div class="controls">
                                  <select class="form-control" id="tipo">
                                    <option <?php //if(isset($v) && $v->tipo=="1") echo "selected"; ?> value="1">Físico mecánica </option>
                                    <option <?php //if(isset($v) && $v->tipo=="2") echo "selected"; ?> value="2">Contaminantes </option>
                                  </select>
                                </div>
                              </div>
                            </td>
                            <td width="15%">
                              <div class="form-group">
                                <label class="col-form-label">Importe</label>
                                <div class="controls">
                                  <input type="number" id="importe" class="form-control form-control-sm" value="<?php // echo $v->importe; ?>">
                                </div>
                              </div>
                            </td>
                            <td width="35%">
                              <div class="form-group">
                                <label class="col-form-label">Observaciones</label>
                                <div class="controls">
                                  <textarea type="time" id="observaciones" class="form-control form-control-sm" value="<?php // echo $v->observaciones; ?>"><?php //echo $v->observaciones; ?></textarea>
                                </div>
                              </div>
                            </td>
                            <td width="5%">
                              <span class="required" style="color: transparent;">dele</span>
                              <a class="btn btn-danger" title="Eliminar contacto" onclick="eliminarVerif(<?php // echo $v->id; ?>)"><i class="fa fa-minus" aria-hidden="true"></i></a>
                            </td>
                          </tr>-->
                      <?php /* }
                    } */ ?>
                    </tbody>
                  </table>
                </div>
                <!-------------------------------------------------------------------------->
              </div>
            </div>
          </div>
        </form>  
          <div class="card-footer">
            <div class="col-sm-9">
              <button class="btn btn-primary" type="button" onclick="add_form()">Guardar datos</button>
              <a href="<?php echo base_url()?>Unidades" class="btn btn-light">Regresar</a>
            </div>
          </div>
        <!---->
      </div>
    </div>
  </div>
</div>


<!------------------------------------------------>

<div class="modal fade" id="modal_carga_evidencia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Cargar evidencia</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <!---->
              <div class="card-body">
                <div class="mb-3 row" id="eviencia_contenedor">
                </div>
              </div>
              <!---->
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light btn-block" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!------------------------------------------------>