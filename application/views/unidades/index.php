<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Listado de unidades</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="mb-3 row">
              <div class="col-sm-3">
                <label class="col-sm-6 col-form-label">Desde:</label>
                <input type="date" id="fechai" class="form-control">
              </div>
              <div class="col-sm-3">
                <label class="col-sm-6 col-form-label">Hasta:</label>
                <input type="date" id="fechaf" class="form-control">
              </div>
              <div class="col-sm-1"></div>
              <div class="col-sm-2">
                <button title="Exportar a Excel" id="export-excel" type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i> Servicios</button>
              </div>
              <div class="col-sm-3">
                <button title="Exportar a Excel" id="export-excel-kms" type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i> Kms recorridos</button>
              </div>
            </div>
            <div class="col-12">
              <a class="btn btn-primary" href="<?php echo base_url() ?>Unidades/registro">Nueva</a><br><br>
            </div>
            <div class="col-12">
              <div class="table-responsive">
                <table class="table" id="table_data">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Número Económico</th>
                      <th scope="col">Vehículo</th>
                      <th scope="col">Tipo</th>
                      <th scope="col">Placas</th>
                      <th scope="col">Placas Federales</th>
                      <th scope="col">Marca</th>
                      <th scope="col">Modelo</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>