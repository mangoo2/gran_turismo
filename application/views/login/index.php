<section>
    <div class="container-fluid">
      <div class="row">
        <div class="col-xl-7"><img class="bg-img-cover " src="<?php echo base_url(); ?>public/img/login4.jpg" alt="looginpage"></div>  
        <div class="col-xl-5 p-0">
          <div class="login-card">
            <form class="theme-form login-form" id="login-form">
              <h4>Login</h4>
              <h6>Bienvenido! Ingresa tus datos para accesar a sistema.</h6>
              <div class="form-group">
                <label>Usuario</label>
                <div class="input-group"><span class="input-group-text"><i class="icon-email"></i></span>
                  <input class="form-control" type="text" name="txtUsuario" id="txtUsuario" required="" placeholder="Tu usuario">
                </div>
              </div>
              <div class="form-group">
                <label>Contraseña</label>
                <div class="input-group"><span class="input-group-text"><i class="icon-lock"></i></span>
                  <input class="form-control" type="password" name="txtPass" id="txtPass" required="" placeholder="*********">
                  <div class="show-hide"><span class="show">                         </span></div>
                </div>
              </div>
              <div class="form-group">
                <div class="checkbox">
                  <input id="checkbox1" type="checkbox">
                  <label class="text-muted" for="checkbox1">Recordarme</label>
                </div>
              </div>
              <div class="form-group">
                <button class="btn btn-primary btn-block" type="submit" id="login-submit"> Iniciar sesión </button>
              </div>
            </form>
            <div class="alert bg-pink" id="error" style="display: none">
              <i class="material-icons ">error</i> <strong>Error!</strong> El nombre de usuario y/o contraseña son incorrectos 
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>