<style type="text/css">
	.kv-file-upload, .fileinput-remove-button {
		display: none;
	}
</style>

<div class="container-fluid">
	<div class="page-header">
		<div class="row">
			<div class="col-sm-6">
				<h3><?php echo $title_heades; ?></h3>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<form class="form" method="post" role="form" id="form_data"><!---FORM--->
					<!------------------->
					<input type="hidden" name="id" id="idcliente" value="<?php echo isset($idC) ? $idC : '0'; ?>">
					<!------------------->
					<div class="card-body">
						<div class="row">
							<div class="col">
								<div class="mb-3 row">
									<label class="col-sm-2 col-form-label">Nombre(s)</label>
									<div class="col-sm-10">
										<input class="form-control" type="text" name="nombre" value="<?php echo isset($idC) ? $nombre : ''; ?>">
									</div>
								</div>

								<div class="mb-3 row">
									<label class="col-sm-2 col-form-label">Apellido Paterno</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" name="apellido_p" value="<?php echo isset($idC) ? $apellido_p : ''; ?>">
									</div>
									<label class="col-sm-2 col-form-label">Apellido Materno</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" name="apellido_m" value="<?php echo isset($idC) ? $apellido_m : ''; ?>">
									</div>
								</div>

								<div class="mb-3 row">
									<label class="col-sm-2 col-form-label">Fecha ingreso</label>
									<div class="col-sm-4">
										<input class="form-control" type="date" name="fecha_ingreso" value="<?php echo isset($idC) ? $fecha_ingreso : ''; ?>">
									</div>
									<label class="col-sm-2 col-form-label">Telefono</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" name="telefono" pattern="\d*" maxlength="12" value="<?php echo isset($idC) ? $telefono : ''; ?>">
									</div>
								</div>

								<div class="mb-3 row">
									<label class="col-sm-2 col-form-label">Dirección</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" name="direccion" value="<?php echo isset($idC) ? $direccion : ''; ?>">
									</div>
								</div>

								<!-------------------------------------------------------------------------->
								<hr>
								<div class="mb-3 row">
								
									<label class="col-sm-2 col-form-label">Usuario</label>
									<div class="col-sm-4">
										<input class="form-control" type="hidden" id="idusuario" name="idusuario" value="<?php echo isset($idC)? $idusuario : 0; ?>">
										<input class="form-control" type="text" id="usuario" name="usuario" value="<?php echo isset($idC)? $usuario : ''; ?>">
									</div>
									<label class="col-sm-2 col-form-label">Contraseña</label>
									<div class="col-sm-4">
										<input class="form-control" type="password" name="contrasena" value="<?php echo isset($idC)? $contrasena : ''; ?>">
									</div>
								</div>

								<!-------------------------------------------------------------------------->
								<hr>
								<div class="col-md-12 mt-5">
									<h4 class="form-section">Documentación</h4>
								</div>

								<div class="mb-3 row ">
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-4 mx-auto">
											<input type="hidden" id="idDocIden" value="<?php echo isset($idC) && isset($idDIden) ? $idDIden : '0'; ?>">
											<h4>Identificación Oficial</h4>
											<hr>
											<input class="form-control" type="hidden" id="iden_aux" value="<?php echo isset($idC) && isset($identificacion) ? $identificacion : ''; ?>">
											<input class="form-control" type="file" name="file" id="identificacion">
										</div>

										<div class="col-md-4 mx-auto">
											<input type="hidden" id="idDocComp" value="<?php echo isset($idC) && isset($idDComp) ? $idDComp : '0'; ?>">
											<h4>Comprobante de Domicilio</h4>
											<hr>
											<input class="form-control" type="hidden" id="comp_aux" value="<?php echo isset($idC) && isset($comprobante) ? $comprobante : ''; ?>">
											<input class="form-control" type="file" name="file" id="comprobante">
										</div>
									</div>


									<div class="col-md-12 form-group d-flex">
										<div class="col-md-4 mx-auto">
											<input type="hidden" id="idDocExa" value="<?php echo isset($idC) && isset($idDExa) ? $idDExa : '0'; ?>">
											<h4>Examen Médico</h4>
											<hr>
											<input class="form-control" type="hidden" id="exa_aux" value="<?php echo isset($idC) && isset($examen) ? $examen : ''; ?>">
											<input class="form-control" type="file" name="file" id="examen">
										</div>

										<div class="col-md-4 mx-auto">
											<input type="hidden" id="idDocLic" value="<?php echo isset($idC) && isset($idDLic) ? $idDLic : '0'; ?>">
											<h4>Licencia de Manejo</h4>
											<hr>
											<input class="form-control" type="hidden" id="lic_aux" value="<?php echo isset($idC) && isset($licencia) ? $licencia : ''; ?>">
											<input class="form-control" type="file" name="file" id="licencia">
										</div>
									</div>

									<div class="col-md-12 form-group d-flex">
										<div class="col-md-4 mx-auto">
											<label class="col-sm-12 col-form-label">Fecha de vencimiento (examen médico)</label>
											<input class="form-control" type="date" name="vigencia_examen" id="vigencia_exa" value="<?php echo isset($idC) ? $vigencia_exa : ''; ?>">
										</div>

										<div class="col-md-4 mx-auto">
											<label class="col-sm-12 col-form-label">Fecha de vencimiento (licencia de manejo)</label>
											<input class="form-control" type="date" name="vigencia_licencia" id="vigencia_lic" value="<?php echo isset($idC) ? $vigencia_lic : ''; ?>">
										</div>
									</div>

									<div class="col-md-12 form-group d-flex">
										<div class="col-md-4 mx-auto">
										</div>

										<div class="col-md-4 mx-auto">
											<label class="col-sm-12 col-form-label">Tipo de licencia</label>
											<select class="form-control" name="tipo_licencia" id="tipo_lic">
												<option <?php echo isset($idC) && $tipo_lic == 1 ? 'selected' : ''; ?> value="1">Tipo A</option>
												<option <?php echo isset($idC) && $tipo_lic == 2 ? 'selected' : ''; ?> value="2">Tipo B</option>
												<option <?php echo isset($idC) && $tipo_lic == 3 ? 'selected' : ''; ?> value="3">Tipo C</option>
												<option <?php echo isset($idC) && $tipo_lic == 4 ? 'selected' : ''; ?> value="4">Tipo D</option>
												<option <?php echo isset($idC) && $tipo_lic == 5 ? 'selected' : ''; ?> value="5">Tipo E</option>
												<option <?php echo isset($idC) && $tipo_lic == 6 ? 'selected' : ''; ?> value="6">Tipo F</option>
											</select>
										</div>
									</div>


								</div>

							</div>
						</div>
					</div>
				</form>
					<div class="card-footer">
						<div class="col-sm-9">
						<button class="btn btn-primary btn_form" type="button" onclick="save_form()"><?php echo $title_save; ?></button>
						<a href="<?php echo base_url() ?>Choferes" class="btn btn-light">Regresar</a>
					</div>
				</div>
				<!---->
			</div>
		</div>
	</div>
</div>