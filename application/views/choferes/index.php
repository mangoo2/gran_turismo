<div class="container-fluid">
	<div class="page-header">
		<div class="row">
			<div class="col-sm-6">
				<h3>Listado de choferes</h3>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-12">
							<a class="btn btn-primary" href="<?php echo base_url() ?>Choferes/registro">Nuevo chofer</a><br><br>
						</div>
						<div class="col-12">
							<div class="table-responsive">
								<table class="table" id="table_data">
									<thead>
										<tr>
											<th scope="col">#</th>
											<th scope="col">Nombre</th>
											<th scope="col">Teléfono</th>
											<th scope="col">Fecha ingreso</th>
											<th scope="col">Acciones</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>