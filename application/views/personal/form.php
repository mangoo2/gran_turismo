
<style type="text/css">
	.kv-file-upload,.fileinput-remove-button{
    display: none;
  }
</style>

<div class="container-fluid">
	<div class="page-header">
		<div class="row">
			<div class="col-sm-6">
				<h3><?php echo $title_heades; ?></h3>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<form class="form" method="post" role="form" id="form_data"><!---FORM--->
					<!------------------->
					<input type="hidden" name="id" id="idcliente" value="<?php echo isset($idP)? $idP : '0'; ?>">
					<!------------------->
					<div class="card-body">
						<div class="row">
							<div class="col">
								<div class="mb-3 row">
									<label class="col-sm-2 col-form-label">Nombre(s)</label>
									<div class="col-sm-10">
										<input class="form-control" type="text" name="nombre" value="<?php echo isset($idP)? $nombre : ''; ?>">
									</div>
								</div>

								<div class="mb-3 row">
									<label class="col-sm-2 col-form-label">Apellido Paterno</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" name="apellido_p" value="<?php echo isset($idP)? $apellido_p : ''; ?>">
									</div>
									<label class="col-sm-2 col-form-label">Apellido Materno</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" name="apellido_m" value="<?php echo isset($idP)? $apellido_m : ''; ?>">
									</div>
								</div>

								<div class="mb-3 row">
									<label class="col-sm-2 col-form-label">Fecha ingreso</label>
									<div class="col-sm-4">
										<input class="form-control" type="date" name="fecha_ingreso" value="<?php echo isset($idP)? $fecha_ingreso : ''; ?>">
									</div>
									<label class="col-sm-2 col-form-label">Puesto</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" name="puesto" value="<?php echo isset($idP)? $puesto : ''; ?>">
									</div>
								</div>

								<div class="mb-3 row">
									<label class="col-sm-2 col-form-label">Telefono</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" name="telefono" pattern="\d*" maxlength="12" value="<?php echo isset($idP)? $telefono : ''; ?>">
									</div>
									<label class="col-sm-2 col-form-label">Dirección</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" name="direccion" value="<?php echo isset($idP)? $direccion : ''; ?>">
									</div>
								</div>

								<!-------------------------------------------------------------------------->
								<hr>
								<div class="mb-3 row">
								
									<label class="col-sm-2 col-form-label">Usuario</label>
									<div class="col-sm-4">
										<input class="form-control" type="hidden" id="idusuario" name="idusuario" value="<?php echo isset($idP)? $idusuario : 0; ?>">
										<input class="form-control" type="text" id="usuario" name="usuario" value="<?php echo isset($idP)? $usuario : ''; ?>">
									</div>
									<label class="col-sm-2 col-form-label">Contraseña</label>
									<div class="col-sm-4">
										<input class="form-control" type="password" name="contrasena" value="<?php echo isset($idP)? $contrasena : ''; ?>">
									</div>
								</div>
								<div class="mb-3 row">
									<label class="col-sm-2 col-form-label">Perfil</label>
									<div class="col-sm-4">
										<select class="form-control" name="perfilId">
											<?php foreach ($perfiles as $perfil) {
												$sel = "";
												if (isset($idP) && $perfilId == $perfil->perfilId) {
													$sel = "selected";
												}
												echo "<option value='$perfil->perfilId' $sel> $perfil->nombre</option>";
											} ?>
										</select>
									</div>

								</div>

								<!-------------------------------------------------------------------------->
								<hr>
								<div class="col-md-12 mt-5">
									<h4 class="form-section">Documentación</h4>
								</div>

								<div class="mb-3 row ">
                  <div class="col-md-12 form-group d-flex">
										
                    <div class="col-md-4 mx-auto">
											<input type="hidden" id="idDocIden" value="<?php echo isset($idP) && isset($idDIden)? $idDIden : '0'; ?>">
                      <h4>Identificación Oficial</h4><hr>
                      <input class="form-control" type="hidden" id="iden_aux" value="<?php echo isset($idP) && isset($identificacion)? $identificacion : ''; ?>">
                      <input class="form-control" type="file" name="file" id="identificacion">
                    </div>
                  
                    <div class="col-md-4 mx-auto">
											<input type="hidden" id="idDocComp" value="<?php echo isset($idP) && isset($idDComp)? $idDComp : '0'; ?>">
                      <h4>Comprobante de Domicilio</h4><hr>
                      <input class="form-control" type="hidden" id="comp_aux" value="<?php echo isset($idP) && isset($comprobante)? $comprobante : ''; ?>">
                      <input class="form-control" type="file" name="file" id="comprobante">
                    </div>
                  </div>
                </div>

							</div>
						</div>
					</div>
				</form>
				<div class="card-footer">
					<div class="col-sm-9">
						<button class="btn btn-primary btn_form" type="button" onclick="save_form()"><?php echo $title_save; ?></button>
						<a href="<?php echo base_url() ?>Personal" class="btn btn-light">Regresar</a>
					</div>
				</div>
				<!---->
			</div>
		</div>
	</div>
</div>